<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TalkdeskGainsightSetting</label>
    <protected>false</protected>
    <values>
        <field>Gainsight_URL_Base__c</field>
        <value xsi:type="xsd:string">https://servicetitan--jbcxm.na80.visual.force.com/apex/JBCXM__customersuccess360?cid=</value>
    </values>
    <values>
        <field>activityTypeId__c</field>
        <value xsi:type="xsd:string">e88358cb-d087-4bf4-a04f-c7da950a5da2</value>
    </values>
    <values>
        <field>appOrgId__c</field>
        <value xsi:type="xsd:string">00D1a000000afFxEAI</value>
    </values>
    <values>
        <field>appSessionID__c</field>
        <value xsi:type="xsd:string">eyJhbGciOiJIUzM4NCIsInppcCI6IkRFRiJ9.eNqsV21z4jYQ_itXf2qn4T0QyMzNVGCHkBAI2CEvpeMRsmwLbMmxZDhyk_9e2TLgcLm7aXp882r3We3bs-KrFsFtwKCjnWtf51qMnxPMxcCZa-c0CYKTuQb5lqLpt3LBVpha2whL0Vy7N7pzLdVOhG-lJwdFEgHHiTHnBVtMIVVoc-2s1kGOU2uW0GJRL5022qeldr1TLbUbzZpbrzdO2203w8ZfBI4pDEDBx1yrVvUarGY_6F58-Q1MJmDSr6681UJvXTcbz5ZNHi6f-pa-wb51xRb37gVBa_2W3PnL2fWGJZMmRWbv8YGPcDfcTFov3DkzuXMdim4grmbDZAXqT2I5iasW2txmV0k4jgcOpoKIrbzF33MNsbDsQUI58XxR5igmi0WAyxwGmLssRrhMqLp-2dzL7t7AnMgCEJUTX4iIn1cqAfMILYJINxXiVI6CNsBAipq5qO7HTggA2N-UwlBVCfrJlvp_SdGaICy9QpoiZooR5HzD4kKFcQhJ8FM7QUL8xCg-2EU-EyyttownIkgkMX4TVBGmVEJlCttVCSfTQ0X5EGYUM5cEOENLo6tcKH9-Ei7o7mb_F9Oaa69pluIgv7CsoISQpYmDQibS0kUx4d8PpBxuj-uUH_OKyWBUQZWvaxxzwujrcfmyuEIsoAMF_LiH8CceIhgLiuOPO0h-4kAOufgQehp3ZX0Az9A4WywxEvyXIO7AFLQks3j7S3AzpEoePZJ98ktQFZSCzVv2v-C-RwVpl0MkyDpFEnGC5XcAqZdAT2Fjat-ZmceAIRgcCxOBxq7LsQqwVG-3Mwd70mcxEQQXOB4ixBIqRowaXyIS45RYXBhw_OZsyNDqzRGSmikhys93TeXmkLxalKQMZ-e8-R0SZLEHKXmBQmb3oHpMoZkqJWhl7wkTULm5Np8uU_r79Lup0m6laf8j03YIjwK4fd9ALa2URG1ZWeKS7Np57l0Sc3Fsp9IPiwcHpCxOsVu4pgVGOpjqB5OQOZkPW3aT0qlXa51S9bRUa1n1xnm1fl5tPMlO-Ge_gdNNOqAuU9QnszT4YW4-traV1WgXUDGJ-dxELBZTDJ0hCUnaX03pNyWAZOEwmT6154s9v--6GQyIo3Zwo5UZKW_7d8ntdKzf9azBeKRsoiggKOsDEwtBqLejfRUUrPm7wMHEGwJwn5ltMF6ZQtInH6vLmHcy-Y-7snCDOrpMOuD3UjGPdNeeu_2Ypw2EshUQrAwZtwH1cJDOjFTz-ExNf6bVKtfL-WwhSRD8Gm-5emcs4RqWE0GC8pBQOTqXkPs3MFKvhxDGKyyY1M5QFPDuUWKknbg78h6W3aVhGpP27HkW8eCBds967uzPRPQc0KuQGrtyRhPc9l-wF86ugdHuP4KLFmF-gu67F6YOX-5RoAPzcuks262XUSd-BN7nz3l7Ua7254GyZOYL76OUpda1PETk0HdiTwlLLnCXeN9E_iZm4wYMhrY1tvtgMDIH_UvLNkagOzT0DC0dt8zLcAz0VM280Ht2F1i9S9scPBkloLgoH5hMVTZNzzBNezjuXdvGzBhZ9vRuaCiVH4JddXsPN7bdS7hgstDpbNk2ygybCjsFMu2bQX8KVFcqzCxrGVfk83EbszVx8m1tGiO9Px3o-bw43CeuMPZMmFOKWnH9GEa-Kd_shR6kMCf-nSCi0bdE6uBF4hlvxdnaiMihPrs3nnzUjXaoxX8ER4z9qp3Id3ukndeazeZpq37WaZ5oBIpc0OzUOs3XfwEAAP__.UC6QFoDnhC3fdkBRhYxnSHpMt6y8TjhC1gKME8bsWbxx6t_cTRxNeyyOpskbbxWg</value>
    </values>
    <values>
        <field>appUserID__c</field>
        <value xsi:type="xsd:string">0051a000002hrdmAAA</value>
    </values>
    <values>
        <field>contentType__c</field>
        <value xsi:type="xsd:string">application/json</value>
    </values>
    <values>
        <field>csOrgURL__c</field>
        <value xsi:type="xsd:string">https://servicetitan--c.na80.content.force.com/profilephoto/005/T</value>
    </values>
    <values>
        <field>gsServiceURL__c</field>
        <value xsi:type="xsd:string">https://scribble.gainsight.com/ant/activity</value>
    </values>
</CustomMetadata>
