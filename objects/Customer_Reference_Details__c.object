<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Customer reference details common to Reference Request and Customer Reference Program object</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Accounting_Software__c</fullName>
        <description>Accounting software, Reference knows</description>
        <externalId>false</externalId>
        <label>Accounting Software</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Quickbooks Online</fullName>
                    <default>false</default>
                    <label>Quickbooks Online</label>
                </value>
                <value>
                    <fullName>Quickbooks Desktop</fullName>
                    <default>false</default>
                    <label>Quickbooks Desktop</label>
                </value>
                <value>
                    <fullName>Intacct</fullName>
                    <default>false</default>
                    <label>Intacct</label>
                </value>
                <value>
                    <fullName>Sage</fullName>
                    <default>false</default>
                    <label>Sage</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Unknown</fullName>
                    <default>false</default>
                    <label>Unknown</label>
                </value>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>Intacct; None</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Intacct; None</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Activity_Types__c</fullName>
        <description>Different type of activities, reference can take part</description>
        <externalId>false</externalId>
        <label>Activity Types</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Prospect Calls</fullName>
                    <default>false</default>
                    <label>Prospect Calls</label>
                </value>
                <value>
                    <fullName>Prospect Onsites</fullName>
                    <default>false</default>
                    <label>Prospect Onsites</label>
                </value>
                <value>
                    <fullName>Peer Calls</fullName>
                    <default>false</default>
                    <label>Peer Calls</label>
                </value>
                <value>
                    <fullName>Peer Onsites</fullName>
                    <default>false</default>
                    <label>Peer Onsites</label>
                </value>
                <value>
                    <fullName>Written Testimonial</fullName>
                    <default>false</default>
                    <label>Written Testimonial</label>
                </value>
                <value>
                    <fullName>Video Testimonial</fullName>
                    <default>false</default>
                    <label>Video Testimonial</label>
                </value>
                <value>
                    <fullName>Case Studies</fullName>
                    <default>false</default>
                    <label>Case Studies</label>
                </value>
                <value>
                    <fullName>Speaking Engagements</fullName>
                    <default>false</default>
                    <label>Speaking Engagements</label>
                </value>
                <value>
                    <fullName>PR Activities</fullName>
                    <default>false</default>
                    <label>PR Activities</label>
                </value>
                <value>
                    <fullName>Product Interviews</fullName>
                    <default>false</default>
                    <label>Product Interviews</label>
                </value>
                <value>
                    <fullName>Product Betas</fullName>
                    <default>false</default>
                    <label>Product Betas</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Competitor_Comments__c</fullName>
        <description>Comment about the reference from other competitors</description>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Competitor Comments</label>
        <length>550</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>GPS_Integration__c</fullName>
        <description>Type of GPS integration</description>
        <externalId>false</externalId>
        <label>GPS Integration</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>Native</fullName>
                    <default>false</default>
                    <label>Native</label>
                </value>
                <value>
                    <fullName>ClearPath</fullName>
                    <default>false</default>
                    <label>ClearPath</label>
                </value>
                <value>
                    <fullName>Fleet Locate</fullName>
                    <default>false</default>
                    <label>Fleet Locate</label>
                </value>
                <value>
                    <fullName>Fleetmatics</fullName>
                    <default>false</default>
                    <label>Fleetmatics</label>
                </value>
                <value>
                    <fullName>GPS Insight</fullName>
                    <default>false</default>
                    <label>GPS Insight</label>
                </value>
                <value>
                    <fullName>GPSTrackIt</fullName>
                    <default>false</default>
                    <label>GPSTrackIt</label>
                </value>
                <value>
                    <fullName>Mobile Tracking and Consulting</fullName>
                    <default>false</default>
                    <label>Mobile Tracking and Consulting</label>
                </value>
                <value>
                    <fullName>Network Fleet</fullName>
                    <default>false</default>
                    <label>Network Fleet</label>
                </value>
                <value>
                    <fullName>SageQuest</fullName>
                    <default>false</default>
                    <label>SageQuest</label>
                </value>
                <value>
                    <fullName>Teletrac</fullName>
                    <default>false</default>
                    <label>Teletrac</label>
                </value>
                <value>
                    <fullName>TomTom</fullName>
                    <default>false</default>
                    <label>TomTom</label>
                </value>
                <value>
                    <fullName>TrackNet</fullName>
                    <default>false</default>
                    <label>TrackNet</label>
                </value>
                <value>
                    <fullName>Trimble</fullName>
                    <default>false</default>
                    <label>Trimble</label>
                </value>
                <value>
                    <fullName>US Fleet Tracking</fullName>
                    <default>false</default>
                    <label>US Fleet Tracking</label>
                </value>
                <value>
                    <fullName>Venture GPS</fullName>
                    <default>false</default>
                    <label>Venture GPS</label>
                </value>
                <value>
                    <fullName>Xora</fullName>
                    <default>false</default>
                    <label>Xora</label>
                </value>
                <value>
                    <fullName>Zubie</fullName>
                    <default>false</default>
                    <label>Zubie</label>
                </value>
                <value>
                    <fullName>Unknown</fullName>
                    <default>false</default>
                    <label>Unknown</label>
                </value>
                <value>
                    <fullName>NexTraq</fullName>
                    <default>false</default>
                    <label>NexTraq</label>
                </value>
                <value>
                    <fullName>Reveal</fullName>
                    <default>false</default>
                    <label>Reveal</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Field Locate</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Field Locate</label>
                </value>
                <value>
                    <fullName>NexTraq; Reveal</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>NexTraq; Reveal</label>
                </value>
                <value>
                    <fullName>rackNet</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>rackNet</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Goals_Comments__c</fullName>
        <description>Comments about goal objective of reference</description>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Goals Comments</label>
        <length>550</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Goals_Realized__c</fullName>
        <description>List of goals available for realization</description>
        <externalId>false</externalId>
        <label>Goals Realized</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Increased Efficiency / Streamlined Operations</fullName>
                    <default>false</default>
                    <label>Increased Efficiency / Streamlined Operations</label>
                </value>
                <value>
                    <fullName>Increased Revenue / Average Ticket</fullName>
                    <default>false</default>
                    <label>Increased Revenue / Average Ticket</label>
                </value>
                <value>
                    <fullName>Improved Reporting</fullName>
                    <default>false</default>
                    <label>Improved Reporting</label>
                </value>
                <value>
                    <fullName>Improved Sales Processes</fullName>
                    <default>false</default>
                    <label>Improved Sales Processes</label>
                </value>
                <value>
                    <fullName>More Effective Marketing / Lead Generation</fullName>
                    <default>false</default>
                    <label>More Effective Marketing / Lead Generation</label>
                </value>
                <value>
                    <fullName>Improved Customer Experience</fullName>
                    <default>false</default>
                    <label>Improved Customer Experience</label>
                </value>
                <value>
                    <fullName>Decreased Overhead</fullName>
                    <default>false</default>
                    <label>Decreased Overhead</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Increased Efficiency / Strealined Operations</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Increased Efficiency / Strealined Operations</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>High_Product_Usage__c</fullName>
        <description>Production area for which reference can be usable</description>
        <externalId>false</externalId>
        <label>High Product Usage</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Mobile App</fullName>
                    <default>false</default>
                    <label>Mobile App</label>
                </value>
                <value>
                    <fullName>Dispatching / Scheduling</fullName>
                    <default>false</default>
                    <label>Dispatching / Scheduling</label>
                </value>
                <value>
                    <fullName>Memberships</fullName>
                    <default>false</default>
                    <label>Memberships</label>
                </value>
                <value>
                    <fullName>Accounting</fullName>
                    <default>false</default>
                    <label>Accounting</label>
                </value>
                <value>
                    <fullName>Inventory / Purchasing</fullName>
                    <default>false</default>
                    <label>Inventory / Purchasing</label>
                </value>
                <value>
                    <fullName>Reporting / Dashboards</fullName>
                    <default>false</default>
                    <label>Reporting / Dashboards</label>
                </value>
                <value>
                    <fullName>Payroll</fullName>
                    <default>false</default>
                    <label>Payroll</label>
                </value>
                <value>
                    <fullName>Pricebook</fullName>
                    <default>false</default>
                    <label>Pricebook</label>
                </value>
                <value>
                    <fullName>Projects</fullName>
                    <default>false</default>
                    <label>Projects</label>
                </value>
                <value>
                    <fullName>Call Center Management / Phone Porting</fullName>
                    <default>false</default>
                    <label>Call Center Management / Phone Porting</label>
                </value>
                <value>
                    <fullName>Customer Communication</fullName>
                    <default>false</default>
                    <label>Customer Communication</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Integration_Comments__c</fullName>
        <description>Comments by the Integrator.</description>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Integration Comments</label>
        <length>550</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Nomination_Comments__c</fullName>
        <description>Comment about the reference at the time of nomination</description>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Nomination Comments</label>
        <length>550</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Positive_Product_Experience__c</fullName>
        <description>Production area, reference has positive experience in</description>
        <externalId>false</externalId>
        <label>Positive Product Experience</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Mobile App</fullName>
                    <default>false</default>
                    <label>Mobile App</label>
                </value>
                <value>
                    <fullName>Dispatching / Scheduling</fullName>
                    <default>false</default>
                    <label>Dispatching / Scheduling</label>
                </value>
                <value>
                    <fullName>Memberships</fullName>
                    <default>false</default>
                    <label>Memberships</label>
                </value>
                <value>
                    <fullName>Accounting</fullName>
                    <default>false</default>
                    <label>Accounting</label>
                </value>
                <value>
                    <fullName>Inventory / Purchasing</fullName>
                    <default>false</default>
                    <label>Inventory / Purchasing</label>
                </value>
                <value>
                    <fullName>Reporting / Dashboards</fullName>
                    <default>false</default>
                    <label>Reporting / Dashboards</label>
                </value>
                <value>
                    <fullName>Payroll</fullName>
                    <default>false</default>
                    <label>Payroll</label>
                </value>
                <value>
                    <fullName>Pricebook</fullName>
                    <default>false</default>
                    <label>Pricebook</label>
                </value>
                <value>
                    <fullName>Projects</fullName>
                    <default>false</default>
                    <label>Projects</label>
                </value>
                <value>
                    <fullName>Call Center Management / Phone Porting</fullName>
                    <default>false</default>
                    <label>Call Center Management / Phone Porting</label>
                </value>
                <value>
                    <fullName>Customer Communication</fullName>
                    <default>false</default>
                    <label>Customer Communication</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Pricebook__c</fullName>
        <description>List of pricebooks available for use</description>
        <externalId>false</externalId>
        <label>Pricebook</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>Custom</fullName>
                    <default>false</default>
                    <label>Custom</label>
                </value>
                <value>
                    <fullName>Maio</fullName>
                    <default>false</default>
                    <label>Maio</label>
                </value>
                <value>
                    <fullName>NSPG</fullName>
                    <default>false</default>
                    <label>NSPG</label>
                </value>
                <value>
                    <fullName>New Flat Rate</fullName>
                    <default>false</default>
                    <label>New Flat Rate</label>
                </value>
                <value>
                    <fullName>Profit Rhino</fullName>
                    <default>false</default>
                    <label>Profit Rhino</label>
                </value>
                <value>
                    <fullName>Quality Service Plumbing</fullName>
                    <default>false</default>
                    <label>Quality Service Plumbing</label>
                </value>
                <value>
                    <fullName>Straight Forward Pricing (SGI)</fullName>
                    <default>false</default>
                    <label>Straight Forward Pricing (SGI)</label>
                </value>
                <value>
                    <fullName>The Best Flate Rate</fullName>
                    <default>false</default>
                    <label>The Best Flate Rate</label>
                </value>
                <value>
                    <fullName>Lennox</fullName>
                    <default>false</default>
                    <label>Lennox</label>
                </value>
                <value>
                    <fullName>Rheem</fullName>
                    <default>false</default>
                    <label>Rheem</label>
                </value>
                <value>
                    <fullName>Aprilaire</fullName>
                    <default>false</default>
                    <label>Aprilaire</label>
                </value>
                <value>
                    <fullName>SolaceAire</fullName>
                    <default>false</default>
                    <label>SolaceAire</label>
                </value>
                <value>
                    <fullName>Unknown</fullName>
                    <default>false</default>
                    <label>Unknown</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Proft Rhino</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Proft Rhino</label>
                </value>
                <value>
                    <fullName>Straight Forward Pricng (SGI)</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Straight Forward Pricng (SGI)</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Product_Comments__c</fullName>
        <description>Comments about the products</description>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Product Comments</label>
        <length>550</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Public_Use_of_Name__c</fullName>
        <description>From Torch Network Nomination Request or From Success Story</description>
        <externalId>false</externalId>
        <label>Public Use of Name</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                    <label>Yes</label>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Third_Party_Integrations__c</fullName>
        <description>Services for which reference has other partners</description>
        <externalId>false</externalId>
        <label>Third Party Integrations</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>All Orders</fullName>
                    <default>false</default>
                    <label>All Orders</label>
                </value>
                <value>
                    <fullName>AsktheSeal</fullName>
                    <default>false</default>
                    <label>AsktheSeal</label>
                </value>
                <value>
                    <fullName>BirdEye</fullName>
                    <default>false</default>
                    <label>BirdEye</label>
                </value>
                <value>
                    <fullName>Broadly</fullName>
                    <default>false</default>
                    <label>Broadly</label>
                </value>
                <value>
                    <fullName>Customer Lobby</fullName>
                    <default>false</default>
                    <label>Customer Lobby</label>
                </value>
                <value>
                    <fullName>Freshlime</fullName>
                    <default>false</default>
                    <label>Freshlime</label>
                </value>
                <value>
                    <fullName>HomeAdvisor</fullName>
                    <default>false</default>
                    <label>HomeAdvisor</label>
                </value>
                <value>
                    <fullName>LeadsNearby</fullName>
                    <default>false</default>
                    <label>LeadsNearby</label>
                </value>
                <value>
                    <fullName>Nationwide Inbound</fullName>
                    <default>false</default>
                    <label>Nationwide Inbound</label>
                </value>
                <value>
                    <fullName>NearbyNow</fullName>
                    <default>false</default>
                    <label>NearbyNow</label>
                </value>
                <value>
                    <fullName>OPX/TPJ</fullName>
                    <default>false</default>
                    <label>OPX/TPJ</label>
                </value>
                <value>
                    <fullName>Plumber SEO</fullName>
                    <default>false</default>
                    <label>Plumber SEO</label>
                </value>
                <value>
                    <fullName>Podium</fullName>
                    <default>false</default>
                    <label>Podium</label>
                </value>
                <value>
                    <fullName>PulseM</fullName>
                    <default>false</default>
                    <label>PulseM</label>
                </value>
                <value>
                    <fullName>Reputation Lobby</fullName>
                    <default>false</default>
                    <label>Reputation Lobby</label>
                </value>
                <value>
                    <fullName>ReviewBuzz</fullName>
                    <default>false</default>
                    <label>ReviewBuzz</label>
                </value>
                <value>
                    <fullName>VitalStorm</fullName>
                    <default>false</default>
                    <label>VitalStorm</label>
                </value>
                <value>
                    <fullName>Yelp</fullName>
                    <default>false</default>
                    <label>Yelp</label>
                </value>
                <value>
                    <fullName>Zoho Reports</fullName>
                    <default>false</default>
                    <label>Zoho Reports</label>
                </value>
                <value>
                    <fullName>Unknown</fullName>
                    <default>false</default>
                    <label>Unknown</label>
                </value>
                <value>
                    <fullName>None</fullName>
                    <default>false</default>
                    <label>None</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>LeadsNearb</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>LeadsNearb</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <label>Customer Reference Details</label>
    <nameField>
        <displayFormat>CR Details-{0000}</displayFormat>
        <label>Customer Reference Details Id</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Customer Reference Details</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
