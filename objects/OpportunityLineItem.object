<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>AddProduct</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>ChoosePricebook</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>EditAllProduct</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Actual_Live_Date_Mobile__c</fullName>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Actual Live Date (Mobile)</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Add_On_Product__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field will be used to Identify whether the Product added is Add-on or not, and this is used in Attach Rate calculation Jira User Story number #SE-2930</description>
        <externalId>false</externalId>
        <label>Add On Product</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Asset__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Asset</label>
        <referenceTo>Asset</referenceTo>
        <relationshipLabel>Opportunity Product</relationshipLabel>
        <relationshipName>Opportunity_Product</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Basis_Points_BPS__c</fullName>
        <externalId>false</externalId>
        <label>Basis Points (BPS)</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Current_Projected_Live_Date_Mobile__c</fullName>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Current Projected Live Date (Mobile)</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Current_Projected_Live_Date_Office__c</fullName>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Current Projected Live Date (Office)</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Custom_Package__c</fullName>
        <description>This field is used to identify whether the selected Product Package is Custom or not.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is used to identify whether the selected Product Package is Custom or not.</inlineHelpText>
        <label>Custom Package</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                    <label>Yes</label>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Description</fullName>
    </fields>
    <fields>
        <fullName>Discount</fullName>
    </fields>
    <fields>
        <fullName>ExpectedBillCMRR__c</fullName>
        <description>If Expected bill techs is NULL = (Quantity)*(SalesPrice) 
ELSE (SalesPrice)*( Expected Bill Techs)</description>
        <externalId>false</externalId>
        <formula>IF(
  OR(
   (ISNULL(ExpectedBillTechs__c)= TRUE &amp;&amp; Managed_Tech__c=TRUE),
    ExpectedBillTechs__c =0 
    ), 
(Quantity*UnitPrice), 
IF(
  (ISNULL(ExpectedBillTechs__c)= FALSE &amp;&amp; Managed_Tech__c=TRUE),
  (UnitPrice*ExpectedBillTechs__c),
    NULL
  )
)</formula>
        <inlineHelpText>If Expected bill techs is NULL = (Quantity)*(SalesPrice) 
ELSE (SalesPrice)*( Expected Bill Techs)</inlineHelpText>
        <label>Expected Bill CMRR</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExpectedBillTechs__c</fullName>
        <description>(Activated Number of Techs) Expected number of techs at first Bill. Editable by OB Managers only</description>
        <externalId>false</externalId>
        <inlineHelpText>(Activated Number of Techs) Expected number of techs at first Bill. Editable by OB Managers only</inlineHelpText>
        <label>Activated Number of Techs</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>First_Year_ARR__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Recurring_Revenue__c = TRUE,
IF(ISBLANK(Number_of_Free_Months__c), TotalPrice * 12,  TotalPrice *  (12 -  Number_of_Free_Months__c )),0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>First Year ARR</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>GSP__c</fullName>
        <description>GSP Calculated off of Sales/Onboarding SLA doc (Discounted GSP)</description>
        <externalId>false</externalId>
        <formula>IF( AND(OR(PricebookEntry.Product2.Id=&quot;01t1a000000klbf&quot;, PricebookEntry.Product2.Id=&quot;01t1a000000h4z0&quot;), Projected_Quantity__c &lt;=4 ) , 1495.00, 
IF( AND(OR(PricebookEntry.Product2.Id=&quot;01t1a000000klbf&quot;, PricebookEntry.Product2.Id=&quot;01t1a000000h4z0&quot;), Projected_Quantity__c &gt;=5, Projected_Quantity__c &lt;=9) , 2495.00, 
IF( AND(OR(PricebookEntry.Product2.Id=&quot;01t1a000000klbf&quot;, PricebookEntry.Product2.Id=&quot;01t1a000000h4z0&quot;), Projected_Quantity__c &gt;=10, Projected_Quantity__c &lt;=25) , 3495.00, 
IF( AND(OR(PricebookEntry.Product2.Id=&quot;01t1a000000klbf&quot;, PricebookEntry.Product2.Id=&quot;01t1a000000h4z0&quot;), Projected_Quantity__c &gt;=26, Projected_Quantity__c &lt;=50) , 4995.00, 
IF( AND(OR(PricebookEntry.Product2.Id=&quot;01t1a000000klbf&quot;, PricebookEntry.Product2.Id=&quot;01t1a000000h4z0&quot;), Projected_Quantity__c &gt;=51, Projected_Quantity__c &lt;=99) , 6995.00, 
IF( AND(OR(PricebookEntry.Product2.Id=&quot;01t1a000000klbf&quot;, PricebookEntry.Product2.Id=&quot;01t1a000000h4z0&quot;), Projected_Quantity__c &gt;=100) , 9995.00, 
0.00))))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>GSP Calculated off of Sales/Onboarding SLA doc (Discounted GSP)</inlineHelpText>
        <label>GSP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Initial_Go_Live_Date__c</fullName>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <inlineHelpText>Updating the Initial Live Date will autopopulate the Current Projected Live Date field with the same value.</inlineHelpText>
        <label>Initial Live Date (Office)</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Initial_Live_Date_Mobile__c</fullName>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Initial Live Date (Mobile)</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ListPrice</fullName>
    </fields>
    <fields>
        <fullName>MRR__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Recurring_Revenue__c = TRUE,  TotalPrice ,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Actual MRR</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Managed_Tech__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Managed Tech</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Number_of_Free_Months__c</fullName>
        <externalId>false</externalId>
        <label>Number of Free Months</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Office_User_Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Office User Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>On_going_ARR__c</fullName>
        <externalId>false</externalId>
        <formula>IF(  Recurring_Revenue__c = TRUE, TotalPrice * 12, 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>On-going ARR</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Onboarding__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This filed looks up the Onboarding record, this field is created during Platform Data Model revamp, to create asset records when Onboarding went Live</description>
        <externalId>false</externalId>
        <label>Onboarding</label>
        <referenceTo>Onboarding__c</referenceTo>
        <relationshipLabel>Opportunity Product</relationshipLabel>
        <relationshipName>Opportunity_Product</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OpportunityId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Previous_Processor_BPS__c</fullName>
        <externalId>false</externalId>
        <label>Previous Processor BPS</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Product2Id</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductCode</fullName>
    </fields>
    <fields>
        <fullName>Product_Name__c</fullName>
        <description>Displays Product Name. Used for reporting and email alerts</description>
        <externalId>false</externalId>
        <formula>PricebookEntry.Product2.Name</formula>
        <label>Product Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_is_Live__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Product is Live</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Projected_MRR__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Recurring_Revenue__c = TRUE,  UnitPrice * Projected_Quantity__c ,0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CMRR (Projected MRR)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Projected_Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Sales Projected Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Projected_v_Actual_Go_Live__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISBLANK( ServiceDate ),0, ServiceDate -  Initial_Go_Live_Date__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Projected v Actual Go Live</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Projected_v_Actual_MRR__c</fullName>
        <externalId>false</externalId>
        <formula>Projected_MRR__c - MRR__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Projected v Actual MRR</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Quantity</fullName>
    </fields>
    <fields>
        <fullName>Recurring_Revenue__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Recurring Revenue</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ServiceDate</fullName>
    </fields>
    <fields>
        <fullName>Service_Date__c</fullName>
        <externalId>false</externalId>
        <formula>ServiceDate</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Subtotal</fullName>
    </fields>
    <fields>
        <fullName>Tech_Count__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Quantity &gt; Office_User_Quantity__c, Quantity - Office_User_Quantity__c, Office_User_Quantity__c  - Quantity)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tech Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tenant_Name__c</fullName>
        <description>This is a hidden formula field to generate the Platform boarding naming convention</description>
        <externalId>false</externalId>
        <formula>Opportunity.Account.Tenant_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tenant Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalPrice</fullName>
    </fields>
    <fields>
        <fullName>Undiscounted_CMRR__c</fullName>
        <description>Based on Post 12 month pricing
# of Managed Techs	Starting Price	1st 12 Months Price Special	Post 12 Months Price
3	9	$295	$215	$235
10	19	$295	$205	$225
20	29	$295	$195	$215
30	39	$295	$185	$205
40	49	$295	$175	$195
50	74	$295	$165	$185
75	99	$295	$155	$175
100	149	$295	$145	$165
150	249	$295	$135	$155
250	499	$295	$125	$145
500	999	$295	$115	$135</description>
        <externalId>false</externalId>
        <formula>IF(
AND(  Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &lt;=9), (235*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=10, Projected_Quantity__c &lt;=19), (225*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=20, Projected_Quantity__c &lt;=29), (215*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=30, Projected_Quantity__c &lt;=39), (205*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=40, Projected_Quantity__c &lt;=49), (195*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=50, Projected_Quantity__c &lt;=74), (185*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=75, Projected_Quantity__c &lt;=99), (175*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=100, Projected_Quantity__c &lt;=149), (165*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=150, Projected_Quantity__c &lt;=249), (155*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=250, Projected_Quantity__c &lt;=499), (145*Projected_Quantity__c),
IF (AND( Managed_Tech__c = TRUE, Recurring_Revenue__c = TRUE, Projected_Quantity__c &gt;=5000, Projected_Quantity__c &lt;=999), (135*Projected_Quantity__c),
0)))))))))))</formula>
        <label>Undiscounted CMRR</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Unique_Product_ID__c</fullName>
        <encryptionScheme>None</encryptionScheme>
        <externalId>false</externalId>
        <label>Unique Product ID</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPrice</fullName>
    </fields>
    <fields>
        <fullName>Vendor__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Vendor</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.Name</field>
                <operation>equals</operation>
                <value>Partner</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Opportunity Product</relationshipLabel>
        <relationshipName>Opportunity_Product</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>mavenlink__ML_Sent_to_Mavenlink__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Item Sent To Mavenlink</label>
        <type>Checkbox</type>
    </fields>
    <validationRules>
        <fullName>Custom_Package_is_Required</fullName>
        <active>true</active>
        <description>Custom Package is required for these Products Growth, Pro, Starter</description>
        <errorConditionFormula>AND 
( 
Opportunity.RecordType.Name = &apos;Sales&apos;,
$Permission.NoAccessForCorporatePackages, 
 
OR 
( 
Product2.Name = &apos;Starter&apos;, 
Product2.Name = &apos;Growth&apos;, 
Product2.Name = &apos;Pro&apos; 
),
NOT
(
OR
(
TEXT(Custom_Package__c)= &apos;YES&apos;,
TEXT(Custom_Package__c) = &apos;NO&apos;
)
)
)</errorConditionFormula>
        <errorDisplayField>Custom_Package__c</errorDisplayField>
        <errorMessage>You must select  either &quot;YES&quot; or &quot;NO&quot; in Custom Package field, for the Product you selected.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Edit_Price_in_Closed_WonOrLost_Stages</fullName>
        <active>true</active>
        <description>Sales Price will be editable by SysAdmins only when the Opportunity changes to a Closed Won/Lost status</description>
        <errorConditionFormula>AND(
ISCHANGED( UnitPrice ) ,
OR(
  
  ISPICKVAL(Opportunity.StageName , &quot;Pending (Onboarding)&quot;), 
  ISPICKVAL(Opportunity.StageName , &quot;Paused&quot;),
  ISPICKVAL(Opportunity.StageName , &quot;Closed Lost&quot;) 
),
Not($User.ProfileId = &quot;00e1a000000tbPn&quot;),
NOT($User.ProfileId = &quot;00e1a000000Ix82&quot;)
)</errorConditionFormula>
        <errorMessage>Sales Price is only editable by SysAdmins only when the Stage is a Closed Won status.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Edit_Quantity_in_Closed_Won_Stages</fullName>
        <active>true</active>
        <description>Quantity will be editable by SysAdmins and Onboarders when the Opportunity changes to a Closed Won status</description>
        <errorConditionFormula>AND( 
ISCHANGED( Quantity ) , 
OR( 
ISPICKVAL(Opportunity.StageName , &quot;Closed Won&quot;), 
ISPICKVAL(Opportunity.StageName , &quot;Pending (Onboarding)&quot;), 
ISPICKVAL(Opportunity.StageName , &quot;Live&quot;), 
ISPICKVAL(Opportunity.StageName , &quot;Success&quot;), 
ISPICKVAL(Opportunity.StageName , &quot;Paused&quot;) 
), 
Not($Permission.Edit_by_Onb_and_SysAdmins) 
)</errorConditionFormula>
        <errorMessage>Quantity is only editable by SysAdmins and Onboarders when the Stage is a Closed Won status.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Expected_Bill_Tech_Restriction</fullName>
        <active>true</active>
        <description>Expected Bill Tech may be edited by OB Managers only</description>
        <errorConditionFormula>AND(
	ISCHANGED(ExpectedBillTechs__c),
	NOT(
		$UserRole.Name = $Label.Role_Name_OB_MM_Manager ||
		$UserRole.Name = $Label.Role_Name_OB_SMB_Manager || 
		$UserRole.Name = $Label.Role_Name_OB_Ent_Manager || 
		$UserRole.Name = $Label.Role_Name_OB_Ent_Rep ||
		$UserRole.Name = $Label.Role_Name_Onboarding_Manager ||
		$UserRole.Name = $Label.Role_Name_Data_Manager || 
		$UserRole.Name = $Label.Role_Name_Customer_Success_Manager ||   
                $UserRole.Name = $Label.Role_Name_VP_of_Customer_Success ||

		($User.ProfileId=$Label.SysAdmin_Profile_ID)
	)
)</errorConditionFormula>
        <errorDisplayField>ExpectedBillTechs__c</errorDisplayField>
        <errorMessage>Expected Bill Techs may only be edited by an Onboarding Manager.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>HandOff_Requirement</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
ISCHANGED( Initial_Go_Live_Date__c ),
ISBLANK(PRIORVALUE( Initial_Go_Live_Date__c )),
ISPICKVAL( Opportunity.StageName, &quot;Onboarding&quot;),
OR(
ISBLANK( Opportunity.Account.Primary_Contact__c),
ISBLANK( Opportunity.Account.Primary_Contact_Email__c ),
ISBLANK( Opportunity.Account.PrimaryContactFirstName__c)
)
)</errorConditionFormula>
        <errorMessage>Before updating the Current Live Date (Office) field you must assign a Primary Contact to the Account with a valid email.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_Access_for_Custom_Packaging</fullName>
        <active>true</active>
        <description>This validation rule is to restrict the users from the selection of Corporate Packing Products. Jira#SE-3325</description>
        <errorConditionFormula>OR
(

AND
(
Opportunity.RecordType.Name = &apos;Sales&apos;, 
ISNEW(), 
OR 
( 
Product2.Name = &apos;Starter&apos;, 
Product2.Name = &apos;Growth&apos;, 
Product2.Name = &apos;Pro&apos; 
),
NOT
(
$Permission.NoAccessForCorporatePackages
)
),

AND 
( 
Opportunity.RecordType.Name = &apos;Sales&apos;, 

NOT 
( 
OR 
( 
Product2.Name = &apos;Starter&apos;, 
Product2.Name = &apos;Growth&apos;, 
Product2.Name = &apos;Pro&apos; 
) 
), 
OR(
ISCHANGED(Custom_Package__c),
AND
(
ISNEW(),
OR
(
TEXT(Custom_Package__c) = &apos;YES&apos;,
TEXT(Custom_Package__c) = &apos;NO&apos;
)
)

)
)
)</errorConditionFormula>
        <errorMessage>You can not select this &quot;Product&quot; or &quot;Custom Packing&quot; field.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>No_Editing_of_Qty_in_Closed_Lost_Stages</fullName>
        <active>true</active>
        <description>Quantity will be editable by SysAdmin only when stage is Closed Lost</description>
        <errorConditionFormula>AND(
 ISCHANGED( Projected_Quantity__c ), 
OR(
  ISPICKVAL(Opportunity.StageName , &quot;Closed Lost&quot;)
),
Not( $User.ProfileId = &quot;00e1a000000tbPn&quot;)
)</errorConditionFormula>
        <errorMessage>Quantity is only editable by SysAdmin only when stage is Closed Lost.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>OfficeUsersCannotBeGreaterThanQuantity</fullName>
        <active>true</active>
        <description>Officer users can not be more than Quantity</description>
        <errorConditionFormula>Quantity &lt;= Office_User_Quantity__c</errorConditionFormula>
        <errorDisplayField>Office_User_Quantity__c</errorDisplayField>
        <errorMessage>Office users quantity can not be more than Quantity</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Office_User_Qty_Not_To_Be_Selected</fullName>
        <active>true</active>
        <description>You Can not add Office User Quantity for this Product (Jira User Story #SE-1776)</description>
        <errorConditionFormula>OR(
  AND( 
   NOT(ISBLANK(Office_User_Quantity__c)), 
   Product2.Used_for_CX_Level_Calculation__c == FALSE, 
     Product2.Only_Managed_Tech__c == FALSE 
),
AND( 
   NOT(ISBLANK(Office_User_Quantity__c)), 
   Product2.Used_for_CX_Level_Calculation__c == TRUE, 
     Product2.Only_Managed_Tech__c == TRUE 
 )
)</errorConditionFormula>
        <errorDisplayField>Office_User_Quantity__c</errorDisplayField>
        <errorMessage>Office User Quantity is not available for this product</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Office_User_Quantity_Need_To_be_Selected</fullName>
        <active>true</active>
        <description>If Product.Used for CX Level Calculation = true and Product.Only Managed Tech = true then user has to enter value in OPP Product.Office User Quantity</description>
        <errorConditionFormula>AND( 
ISBLANK(Office_User_Quantity__c),
Product2.Used_for_CX_Level_Calculation__c == TRUE,  
Product2.Only_Managed_Tech__c == False
)</errorConditionFormula>
        <errorDisplayField>Office_User_Quantity__c</errorDisplayField>
        <errorMessage>You must have Office User Quantity for this Product</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Only_Non_Core_Products_can_be_added</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
Opportunity.RecordType.Name =&apos;Upsell&apos;,
    OR(Product2.Core_Product__c = true,
    NOT(ISPICKVAL(Product2.Product_Type__c, &apos;Add-on&apos;)))
)</errorConditionFormula>
        <errorMessage>Only Non Core Products with Product Type as &quot;Add-On&quot;  can be added to Upsell Opportunity.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Stage_is_Closed_Won_Not_Edit_Quantity</fullName>
        <active>true</active>
        <description>When Opportunity Stage is Closed Won then can&apos;t change the quantity.</description>
        <errorConditionFormula>AND(ISPICKVAL(Opportunity.StageName, &quot;Closed Won&quot;), 
ISCHANGED(Quantity), 
Managed_Tech__c,
NOT( $User.ProfileId = $Label.SysAdmin_Profile_ID ))</errorConditionFormula>
        <errorDisplayField>Quantity</errorDisplayField>
        <errorMessage>You can not change the quantity of closed won opportunity.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Features</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Features</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>window.open(&quot;https://servicetitan.my.salesforce.com//apex/productOptions?id={!Opportunity.Id}&quot;,&quot;_blank&quot;)</url>
    </webLinks>
</CustomObject>
