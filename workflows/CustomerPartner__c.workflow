<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Association_Weight</fullName>
        <field>Association_Weight__c</field>
        <formula>VALUE(TEXT(Partner__r.Association_Weight__c))</formula>
        <name>Association Weight</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Customer Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Customer Partner Record Type</fullName>
        <actions>
            <name>Customer_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
 ISBLANK(Lead__c) ,
 RecordTypeId &lt;&gt; &quot;0121a000000EhW9&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Weight</fullName>
        <actions>
            <name>Association_Weight</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK( TEXT(Partner__r.Association_Weight__c) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
