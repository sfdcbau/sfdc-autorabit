<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>MSA_Error_for_Existing_Leads</fullName>
        <description>MSA Error for Existing Leads</description>
        <protected>false</protected>
        <recipients>
            <recipient>ihernandez@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tgaddam@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales/MSA_Lead_Template_HTML</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Acitivity_Type</fullName>
        <field>Type</field>
        <literalValue>Auto MDR Call</literalValue>
        <name>Update Acitivity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_ActivityType2_To_ScheduledDiscCal</fullName>
        <field>Activity_Type_2__c</field>
        <formula>TEXT(Type)</formula>
        <name>Update ActivityType2 To ScheduledDiscCal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activity_Type_2</fullName>
        <field>Activity_Type_2__c</field>
        <formula>&quot;Auto MDR Call&quot;</formula>
        <name>Update Activity Type 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Created_By_Role_Event</fullName>
        <field>CreatedByRole__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update Created By Role - Event</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Subject_for_Schedule_Disco</fullName>
        <field>Subject</field>
        <formula>&quot;Scheduled Discovery Call&quot;</formula>
        <name>Update Event Subject for Schedule Disco</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Type_To_SchedDiscCall</fullName>
        <field>Type</field>
        <literalValue>Scheduled Discovery Call</literalValue>
        <name>Update Event Type To SchedDiscCall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Meeting_Outcome</fullName>
        <field>Meeting_Outcome__c</field>
        <literalValue>Prospect No Show</literalValue>
        <name>Update Meeting Outcome</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Created By Role - Event</fullName>
        <actions>
            <name>Update_Created_By_Role_Event</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the activity is created the users role is copied to the Created By Role field</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Event Subject for Schedule Disco</fullName>
        <actions>
            <name>Update_Event_Subject_for_Schedule_Disco</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Type</field>
            <operation>equals</operation>
            <value>Scheduled Discovery Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Meeting_Type_CP__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Event Type For Chili Piper Events - Auto MDR Call</fullName>
        <actions>
            <name>Update_Acitivity_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Activity_Type_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Meeting_Type_CP__c</field>
            <operation>equals</operation>
            <value>Auto MDR Call</value>
        </criteriaItems>
        <description>When a Auto MDR Call is scheduled and the Meeting Type_CP is Auto MDR Call update the Event &quot;Type&quot; field to Auto MDR Call</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Event Type For Chili Piper Events - Scheduled Disco</fullName>
        <actions>
            <name>Update_ActivityType2_To_ScheduledDiscCal</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Event_Type_To_SchedDiscCall</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Meeting_Type_CP__c</field>
            <operation>equals</operation>
            <value>Discovery Call</value>
        </criteriaItems>
        <description>When a Discovery Call is scheduled and the Meeting Type_CP is Discovery Call update the Event &quot;Type&quot; field to Scheduled Discovery Call</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Meeting Outcome for No Shows</fullName>
        <actions>
            <name>Update_Meeting_Outcome</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Event.Meeting_Outcome__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Event.No_Show_CP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Type</field>
            <operation>equals</operation>
            <value>Scheduled Discovery Call</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
