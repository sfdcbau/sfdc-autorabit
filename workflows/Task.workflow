<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Discovery_Call_Scheduled_Alert</fullName>
        <description>Discovery Call Scheduled Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Discovery_Call_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Follow_Up_Completed_Email_Alert</fullName>
        <description>Rejection Follow Up Completed Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/RejectionFollowUpResolution</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Follow_Up_UnableToConnect</fullName>
        <description>Rejection Follow Up_Unable To Connect</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/RejectionFollowUpUnableToConnect</template>
    </alerts>
    <fieldUpdates>
        <fullName>Copy_Task_Type_to_ActivityType2</fullName>
        <field>Activity_Type_2__c</field>
        <formula>Text(Type)</formula>
        <name>Copy Task Type to ActivityType2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Created_By_Role_Task</fullName>
        <field>CreatedByRole__c</field>
        <formula>$UserRole.Name</formula>
        <name>Update Created By Role - Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_short_comments</fullName>
        <field>Short_Comments__c</field>
        <formula>LEFT( Description,200)</formula>
        <name>Update short comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Task Type to ActivityType2</fullName>
        <actions>
            <name>Copy_Task_Type_to_ActivityType2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When a new task is created/edited copy the Task Type to Activity Type 2 in order to give access to that data to Custom Report Types</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Created By Role - Task</fullName>
        <actions>
            <name>Update_Created_By_Role_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Username</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the activity is created the users role is copied to the Created By Role field</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Discovery Call Alert</fullName>
        <actions>
            <name>Discovery_Call_Scheduled_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Scheduled Discovery Call</value>
        </criteriaItems>
        <description>Alert to Jo and AE when a Discovery Call is scheduled by the SDR/MRR</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task - Update Short Comments</fullName>
        <actions>
            <name>Update_short_comments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
