<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Admin_Bootcamp_1_Week_Reminder</fullName>
        <description>Admin Bootcamp 1 Week Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementationbootcamp@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Bootcamp_Glendale_1_week_out</template>
    </alerts>
    <alerts>
        <fullName>Admin_Bootcamp_3_Week_Reminder</fullName>
        <description>Admin Bootcamp 3 Week Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementationbootcamp@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Bootcamp_Glendale_3_weeks_out</template>
    </alerts>
    <alerts>
        <fullName>Admin_Bootcamp_Follow_Up_Email</fullName>
        <description>Admin Bootcamp Follow Up Email</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementationbootcamp@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Bootcamp_Glendale_Post_Event</template>
    </alerts>
    <alerts>
        <fullName>Admin_Bootcamp_Registration_Accepted</fullName>
        <description>Admin Bootcamp Registration Accepted</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementationbootcamp@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Accepted_Bootcamp</template>
    </alerts>
    <alerts>
        <fullName>Alert_for_Customer_Success_Manager</fullName>
        <description>Alert for Customer Success Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Success_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Template_for_Customer_Manager</template>
    </alerts>
    <alerts>
        <fullName>Atlanta_1_Week_Out</fullName>
        <description>Atlanta 1 Week Out</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Atlanta_1_week_out</template>
    </alerts>
    <alerts>
        <fullName>Atlanta_5_weeks_out</fullName>
        <description>Atlanta 5 weeks out</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Atlanta_5_weeks_out</template>
    </alerts>
    <alerts>
        <fullName>Atlanta_Post_Training_Follow_Up</fullName>
        <description>Atlanta Post Training Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Post_Event_Follow_up</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Onboarder</fullName>
        <description>Email Alert for Onboarder</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarder_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Template_for_Onboarder</template>
    </alerts>
    <alerts>
        <fullName>Glendale_1_week_out_Email_Reminder</fullName>
        <description>Glendale 1 week out Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Glendale_1_week_out</template>
    </alerts>
    <alerts>
        <fullName>Glendale_5_weeks_out_Email_Reminder</fullName>
        <description>Glendale 5 weeks out Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Glendale_5_weeks_out</template>
    </alerts>
    <alerts>
        <fullName>Power_User_Post_Training_Follow_Up</fullName>
        <description>Power User Post Training Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Post_Event_Follow_up</template>
    </alerts>
    <alerts>
        <fullName>Power_User_Registration_Accepted</fullName>
        <description>Power User Registration Accepted</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>powerusertraining@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Training_Team/Power_User_Accepted</template>
    </alerts>
    <fieldUpdates>
        <fullName>FU_Completed_Date</fullName>
        <field>Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Completed Date</fullName>
        <actions>
            <name>FU_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CustomerTraining__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Paid Training Event</value>
        </criteriaItems>
        <criteriaItems>
            <field>CustomerTraining__c.Training_Type__c</field>
            <operation>equals</operation>
            <value>Power User</value>
        </criteriaItems>
        <criteriaItems>
            <field>CustomerTraining__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Automatically populates the completed date field when the power user record type customer training is completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
