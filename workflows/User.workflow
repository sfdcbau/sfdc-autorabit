<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Nickname</fullName>
        <field>CommunityNickname</field>
        <formula>LEFT(FirstName &amp; &quot; &quot; &amp; LastName &amp; &quot; (&quot; &amp; IF(ISBLANK( Contact.Account.Name ), &quot;ServiceTitan&quot;, Contact.Account.Name), 39) &amp; &quot;)&quot;</formula>
        <name>Nickname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Nickname</fullName>
        <actions>
            <name>Nickname</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>1/1/2015</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
