<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Churn_Alert_CustPartners_Onboarding</fullName>
        <description>Churn Alert_CustPartners_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>lgildea@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert_For_ST_Billing_Onboarding</fullName>
        <ccEmails>billing@servicetitan.com</ccEmails>
        <description>Churn Alert For ST Billing_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert_STBilling</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert_Onboarding</fullName>
        <ccEmails>csops@servicetitan.com</ccEmails>
        <ccEmails>platforminternal@servicetitan.com</ccEmails>
        <description>Churn Alert_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>bnorton@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jculley@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlaino@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Customer_is_LIVE_Sales_Email</fullName>
        <description>Customer is LIVE - Sales Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gainsight@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Notification_of_Go_Live</template>
    </alerts>
    <alerts>
        <fullName>Merger_Acquisition_Alert_to_CS_Onboarding</fullName>
        <ccEmails>csops@servicetitan.com</ccEmails>
        <description>Merger/Acquisition Alert to CS Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/MergerAcquisition_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Financing_Hand_Off_Alert</fullName>
        <ccEmails>financing@servicetitan.com</ccEmails>
        <description>Onboarding-Financing Hand Off Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/IMP_Financing_Hand_Off_Email</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Payments_Introduction_Alert</fullName>
        <ccEmails>paymentsetup@servicetitan.com</ccEmails>
        <description>Onboarding-Payments Introduction Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/ST_Payments_Introduction</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Status_Set_to_Out_for_Signature</fullName>
        <ccEmails>paymentsetup@servicetitan.com</ccEmails>
        <description>Onboarding Status Set to Out for Signature</description>
        <protected>false</protected>
        <recipients>
            <recipient>jjuarez@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Onboarding/Onboarding_Status_for_CC_Check_is_Out_for_Signature</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert_CustPartners_Onboarding</fullName>
        <description>Rejection Alert_CustPartners_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>lgildea@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert_Onboarding</fullName>
        <ccEmails>csops@servicetitan.com</ccEmails>
        <ccEmails>platforminternal@servicetitan.com</ccEmails>
        <description>Rejection Alert_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ezevin@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hlim@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jamesgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jlaino@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert_Product_Onboarding</fullName>
        <description>Rejection Alert_Product_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>avartazarian@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>ST_Payments_Unsuccessful_Contact_Alert</fullName>
        <ccEmails>payments@servicetitan.com, sysadmin@servicetitan.com</ccEmails>
        <description>ST Payments_Unsuccessful Contact Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/ST_Payments_UnsuccessfulContact</template>
    </alerts>
    <fieldUpdates>
        <fullName>Anticipated_autoupdate_onboarding</fullName>
        <field>Anticipated_Success_Transition_Date__c</field>
        <formula>IF(
  ISBLANK(Live_Date__c), 
  (Projected_Go_Live_Date__c + 21),
  (Live_Date__c + 21)
)</formula>
        <name>Anticipated success transition date upda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Date</fullName>
        <field>ApprovedDate__c</field>
        <formula>TODAY()</formula>
        <name>Set Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Live_Date</fullName>
        <field>Live_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Live Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Out_for_Signature_Date</fullName>
        <field>OutforSignatureDate__c</field>
        <formula>TODAY()</formula>
        <name>Set Out for Signature Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_ST_Live_Date</fullName>
        <field>PendingSTLiveDate__c</field>
        <formula>TODAY()</formula>
        <name>Set Pending ST Live Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Underwriting_Date</fullName>
        <field>PendingUnderwritingDate__c</field>
        <formula>TODAY()</formula>
        <name>Set Pending Underwriting Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Current_Projected_go_Live</fullName>
        <field>Projected_Go_Live_Date__c</field>
        <formula>Initial_Live_Date_Office__c</formula>
        <name>Update Current Projected go Live</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto Pop Anticipated Success Trans%2E Date</fullName>
        <actions>
            <name>Anticipated_autoupdate_onboarding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever &quot;Actual Live Date (Office)&quot; is entered then the Anticipated Success Transition Date will auto-update to be 21 days later. This field is still editable and will allow user to the date back if there are outstanding issues</description>
        <formula>OR(
  ISCHANGED(Live_Date__c),
  ISCHANGED(Projected_Go_Live_Date__c)
)
/*
previous formula
NOT(ISBLANK(Live_Date__c))
*/</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Approved Date-Onboarding</fullName>
        <actions>
            <name>Set_Approved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Onboarding__c.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Set Approved Date based on when Onboarding Status is set to &quot;Approved&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Live Date-Onboarding</fullName>
        <actions>
            <name>Set_Live_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Onboarding__c.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Live</value>
        </criteriaItems>
        <description>Set Live date based on when Onboarding Status is set to &quot;Live&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Out for Signature Date-Onboarding</fullName>
        <actions>
            <name>Set_Out_for_Signature_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Onboarding__c.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Out for Signature</value>
        </criteriaItems>
        <description>Set Out for Signature Date based on when Onboarding Status is set to &quot;Out for Signature&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Pending ST Live Date-Onboarding</fullName>
        <actions>
            <name>Set_Pending_ST_Live_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Onboarding__c.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Pending ST Live</value>
        </criteriaItems>
        <description>Set Pending ST Live Date based on when Onboarding Status is set to &quot;Pending ST Live&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Pending Underwriting Date-Onboarding</fullName>
        <actions>
            <name>Set_Pending_Underwriting_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Onboarding__c.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Pending Underwriting</value>
        </criteriaItems>
        <description>Set Pending Underwriting Date based on when Onboarding Status is set to &quot;Pending Underwriting&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Initial %26 Projected live dates %28office%29</fullName>
        <actions>
            <name>Update_Current_Projected_go_Live</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Onboarding__c.Initial_Live_Date_Office__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Onboarding__c.Projected_Go_Live_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When Initial Live Date (office) is populated, Current Projected Live Date (office) will auto-populate with the same date to prevent the user from needing to perform a double-entry
TAR-6</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On-boarding Status is Out for Signature</fullName>
        <actions>
            <name>Onboarding_Status_Set_to_Out_for_Signature</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Onboarding__c.Onboarding_Status__c</field>
            <operation>equals</operation>
            <value>Out for Signature</value>
        </criteriaItems>
        <description>Send Email Notification when Onboarding Status is set to &apos;Out or Signature&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
