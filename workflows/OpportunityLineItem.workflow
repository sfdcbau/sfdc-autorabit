<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Curr_Proj_Live_Date_Office_update_notification</fullName>
        <description>Curr Proj Live Date (Office) update notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Change_to_current_projected_live_date</template>
    </alerts>
    <alerts>
        <fullName>ProdQuantityLessThanProjected</fullName>
        <description>ProdQuantityLessThanProjected</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jphillips@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/ProdQuantityLessThanProjectedAlert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Current_live_date_auto_mobile</fullName>
        <description>Current live date auto populate from initial (mobile)</description>
        <field>Current_Projected_Live_Date_Mobile__c</field>
        <formula>Initial_Live_Date_Mobile__c</formula>
        <name>Current live date auto mobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Current_live_date_auto_populate_from_ini</fullName>
        <description>Current live date auto populate from initial (office)</description>
        <field>Current_Projected_Live_Date_Office__c</field>
        <formula>Initial_Go_Live_Date__c</formula>
        <name>Current live date auto populate from ini</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Go_Live_Date</fullName>
        <field>Initial_Go_Live_Date__c</field>
        <formula>ServiceDate</formula>
        <name>Go Live Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MarkProductIsLiveTrue</fullName>
        <field>Product_is_Live__c</field>
        <literalValue>1</literalValue>
        <name>MarkProductIsLiveTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_is_Live</fullName>
        <field>Product_is_Live__c</field>
        <literalValue>1</literalValue>
        <name>Product is Live</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quantity</fullName>
        <field>Projected_Quantity__c</field>
        <formula>Quantity</formula>
        <name>Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quantity_copies_to_Sales_Projected_Qty</fullName>
        <field>Projected_Quantity__c</field>
        <formula>Quantity</formula>
        <name>Quantity copies to Sales Projected Qty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recurring_Revenue</fullName>
        <field>Recurring_Revenue__c</field>
        <literalValue>1</literalValue>
        <name>Recurring Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quantity_when_Sales_Proj_Qty_Upda</fullName>
        <description>Update the Quantity field whenever the Sales Projected Quantity field is updated.</description>
        <field>Quantity</field>
        <formula>Projected_Quantity__c</formula>
        <name>Update Quantity when Sales Proj Qty Upda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>live_date_office</fullName>
        <field>Initial_Go_Live_Date__c</field>
        <formula>Current_Projected_Live_Date_Office__c</formula>
        <name>live date (office)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>managed_tech</fullName>
        <field>Managed_Tech__c</field>
        <literalValue>1</literalValue>
        <name>managed tech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Current live date auto populate from initial %28mobile%29</fullName>
        <actions>
            <name>Current_live_date_auto_mobile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Initial_Live_Date_Mobile__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Current_Projected_Live_Date_Mobile__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Current live date auto populate from initial (mobile)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Current live date auto populate from initial %28office%29</fullName>
        <actions>
            <name>Current_live_date_auto_populate_from_ini</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Initial_Go_Live_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Current_Projected_Live_Date_Office__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Current live date auto populate from initial (office)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Go Live Date</fullName>
        <actions>
            <name>Go_Live_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.ServiceDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Initial_Go_Live_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Product is Live</fullName>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.ServiceDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Family_Sub_Type__c</field>
            <operation>equals</operation>
            <value>User Fees</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Product_is_Live</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>OpportunityLineItem.ServiceDate</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ProductQuantityReductionAlert</fullName>
        <actions>
            <name>ProdQuantityLessThanProjected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert to be sent to Sales Rep when an Onboarder enters a quantity lower than the projected quantity on an item.</description>
        <formula>Quantity  &lt;  Projected_Quantity__c &amp;&amp; Opportunity.RecordTypeId=$Label.RecordTypeID_SMBSales_Opp</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Proj Live Date Changed</fullName>
        <actions>
            <name>Curr_Proj_Live_Date_Office_update_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>DOES NOT WORK YET, CRITERIA TO BE FIXED. Notify User each time the Projected Live Date is changed. But not when its first entered</description>
        <formula>AND((Current_Projected_Live_Date_Office__c &lt;&gt; NULL),(Current_Projected_Live_Date_Office__c &lt;&gt;  Initial_Go_Live_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quantity copies to Sales Projected Qty</fullName>
        <actions>
            <name>Quantity_copies_to_Sales_Projected_Qty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 )</booleanFilter>
        <criteriaItems>
            <field>OpportunityLineItem.Quantity</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Stage Zero,Discovery Call,Q&amp;A / DEMO,Pricing / ROI Assessment,Negotiation / Pending,Prospect,Pricing / Negotiation,Proposal Sent,SE Vetting,CC Processed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales,Upsell</value>
        </criteriaItems>
        <description>Quantity is copied to Sales Projected Quantity when Product is created/edited AND the Stage is one of the Open Stages</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Quantity copies to Sales Projected Quantity</fullName>
        <actions>
            <name>Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Quantity</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Projected_Quantity__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>NO LONGER NEEDED? - Quantity is copied to Sales Projected Quantity when Product is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Recurring Revenue</fullName>
        <actions>
            <name>Recurring_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Recurring_Revenue__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Family</field>
            <operation>equals</operation>
            <value>Recurring Revenue</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Quantity when Sales Proj Qty Updated</fullName>
        <actions>
            <name>Update_Quantity_when_Sales_Proj_Qty_Upda</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Quantity field whenever the Sales Projected Quantity field is updated.</description>
        <formula>ISCHANGED(Projected_Quantity__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>managed tech</fullName>
        <actions>
            <name>managed_tech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Core_Product__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Managed_Tech__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
