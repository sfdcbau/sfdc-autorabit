<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ABS_of_Est_Techs_when_below_0</fullName>
        <field>No_of_Technicians__c</field>
        <formula>ABS(No_of_Technicians__c)</formula>
        <name>ABS of Est Techs when below 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ABS_of_Residential_when_below_0</fullName>
        <field>Residential__c</field>
        <formula>ABS( Residential__c )</formula>
        <name>ABS of %Residential when below 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_is_Customer</fullName>
        <field>Account_Record_Type__c</field>
        <literalValue>Customer</literalValue>
        <name>Account is Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_is_partner</fullName>
        <field>Account_Record_Type__c</field>
        <literalValue>Partner</literalValue>
        <name>Account is partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Data_Com_Lead_Source</fullName>
        <description>Change Data.com Lead Source from Jigsaw to Sales - Lead Lists</description>
        <field>LeadSource</field>
        <literalValue>Sales - Lead Lists</literalValue>
        <name>Change Data.Com Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Olark_Chat_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>Mar - Website Chat</literalValue>
        <name>Change Olark Chat Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disqualified_Date</fullName>
        <field>Disqualified_Date__c</field>
        <formula>TODAY()</formula>
        <name>Disqualified Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Created_Date</fullName>
        <field>Lead_Created_Date__c</field>
        <formula>DateValue( CreatedDate )</formula>
        <name>Lead Created Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Round_Robin</fullName>
        <field>LCA__Assign_using_active_assignment_rules__c</field>
        <literalValue>1</literalValue>
        <name>Lead Round Robin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MQL_Date_Update</fullName>
        <field>MQL_Date__c</field>
        <formula>Today()</formula>
        <name>MQL Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Multiply_Residential_By_1_when_below_0</fullName>
        <field>Residential__c</field>
        <formula>ABS(Residential__c)</formula>
        <name>Multiply %Residential By -1 when below 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Needs_De_Duping</fullName>
        <field>Need_De_Duping__c</field>
        <literalValue>1</literalValue>
        <name>Needs De-Duping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_is_Sales</fullName>
        <field>Opportunity_Record_Type__c</field>
        <literalValue>Sales</literalValue>
        <name>Opportunity is Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_is_biz_dev</fullName>
        <field>Opportunity_Record_Type__c</field>
        <literalValue>Biz Dev</literalValue>
        <name>Opportunity is biz dev</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQL_Date</fullName>
        <field>SQL_Date__c</field>
        <formula>TODAY()</formula>
        <name>SQL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TQL_Date</fullName>
        <field>TQL_Date__c</field>
        <formula>TODAY()</formula>
        <name>TQL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SAL_Date</fullName>
        <field>SAL_Date__c</field>
        <formula>Today()</formula>
        <name>Update SAL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ABS of Est Techs OR %25 Residential when below 0</fullName>
        <actions>
            <name>ABS_of_Est_Techs_when_below_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ABS_of_Residential_when_below_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Take absolute value of Est Tech Field whenever it&apos;s changed</description>
        <formula>OR( 
ISCHANGED(  No_of_Technicians__c  ), 
ISCHANGED( Residential__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Data%2ECom Lead Source</fullName>
        <actions>
            <name>Change_Data_Com_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>Jigsaw</value>
        </criteriaItems>
        <description>Change Data.com Lead Source from Jigsaw to Lead Lists</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Olark Chat Lead Source</fullName>
        <actions>
            <name>Change_Olark_Chat_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>Live Chat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>Drift</value>
        </criteriaItems>
        <description>Change Olark Chat Lead Source from Live Chat to Mar - Website Chat</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Check Needs De-Duping</fullName>
        <actions>
            <name>Needs_De_Duping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Need_De_Duping__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Disqualified Date</fullName>
        <actions>
            <name>Disqualified_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Disqualified,Nurture</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Disqualified_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Biz Dev</fullName>
        <actions>
            <name>Account_is_partner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_is_biz_dev</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Biz Dev</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Created Date</fullName>
        <actions>
            <name>Lead_Created_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Created_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Sales</fullName>
        <actions>
            <name>Account_is_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_is_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Multiply %25Residential By -1 when below 0</fullName>
        <actions>
            <name>Multiply_Residential_By_1_when_below_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Residential__c</field>
            <operation>lessThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Round Robin Eligible</fullName>
        <actions>
            <name>Lead_Round_Robin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
            <value>Referral</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update MQL Date</fullName>
        <actions>
            <name>MQL_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>MQL</value>
        </criteriaItems>
        <description>Updates the MQL date field when the Lead Status field = MQL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SAL Date</fullName>
        <actions>
            <name>Update_SAL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>SAL</value>
        </criteriaItems>
        <description>Updates the SAL date field when the Lead Status field = SAL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update SQL Date</fullName>
        <actions>
            <name>SQL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>SQL</value>
        </criteriaItems>
        <description>Updates the SQL date field when the Lead Status = SQL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update TQL Date</fullName>
        <actions>
            <name>TQL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>TQL</value>
        </criteriaItems>
        <description>Updates the TQL date field when the Lead Status = TQL</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
