<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Approval_Stage_3_Approved</fullName>
        <description>Case Approval: Stage 3 (Final) - Approved</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_Approval_Stage_3_Approved</template>
    </alerts>
    <alerts>
        <fullName>Case_Approval_Step_Rejected</fullName>
        <description>Case Approval: Step Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Case_Re_Opened_Alert</fullName>
        <description>Case Re-Opened Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_ReOpened_Alert</template>
    </alerts>
    <alerts>
        <fullName>Case_Re_Opened_Data_Enrichment_Uploads</fullName>
        <description>Case Re-Opened - Data Enrichment Uploads</description>
        <protected>false</protected>
        <recipients>
            <recipient>emcbride@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_ReOpened_Alert</template>
    </alerts>
    <alerts>
        <fullName>Case_Re_Opened_SEProdGapResolution</fullName>
        <description>Case Re-Opened - SE Prod Gap Resolution</description>
        <protected>false</protected>
        <recipients>
            <recipient>dhall@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_ReOpened_Alert_SEprodgapresolution</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Alert</fullName>
        <description>New Case Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/New_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Alert_Data_Enrichment</fullName>
        <description>New Case Alert - Data Enrichment</description>
        <protected>false</protected>
        <recipients>
            <recipient>emcbride@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/New_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Alert_Onboarding_Live_Account</fullName>
        <description>New Case Alert - Onboarding/Live Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/New_Case_Alert_OnboardingLive</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Alert_SEproductgapresolution</fullName>
        <description>New Case Alert - SE Prod Gap Resolution</description>
        <protected>false</protected>
        <recipients>
            <recipient>dhall@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/New_SECase_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_Case_Alert_SalesOps</fullName>
        <description>New Case Alert - SalesOps</description>
        <protected>false</protected>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/New_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_Data_Case_Alert</fullName>
        <description>New Data Case Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>clee@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/New_Case_Alert</template>
    </alerts>
    <alerts>
        <fullName>Pending_notification_24_hours</fullName>
        <description>Pending notification 24 hours</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Pending_notification_24_hours</template>
    </alerts>
    <alerts>
        <fullName>Pending_notification_5_days</fullName>
        <description>Pending notification 5 days</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Pending_Notification_5_Days</template>
    </alerts>
    <alerts>
        <fullName>Premium_Case_Created</fullName>
        <ccEmails>premiumsupport@servicetitan.com</ccEmails>
        <description>Premium Case Created</description>
        <protected>false</protected>
        <recipients>
            <recipient>dkabir@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Premium_Support_Notification</template>
    </alerts>
    <alerts>
        <fullName>Quick_Response_Email</fullName>
        <description>Quick Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/Quick_Response</template>
    </alerts>
    <alerts>
        <fullName>ST_Send_Email_To_Onboarder</fullName>
        <description>ST Send Email To Onboarder</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarder_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/ST_Onboarder_Support_Case_Notification</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Rejection_15_to_24</fullName>
        <description>Sales Reference Rejection - 15 to 24</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Rejection_25</fullName>
        <description>Sales Reference Rejection - 25+</description>
        <protected>false</protected>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Rejection_5_to_14</fullName>
        <description>Sales Reference Rejection - 5 to 14</description>
        <protected>false</protected>
        <recipients>
            <recipient>bnorton@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Request_15_to_24</fullName>
        <description>Sales Reference Request - 15 to 24</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Case_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Request_25</fullName>
        <description>Sales Reference Request - 25+</description>
        <protected>false</protected>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Case_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Request_5_to_14</fullName>
        <description>Sales Reference Request - 5 to 14</description>
        <protected>false</protected>
        <recipients>
            <recipient>bnorton@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Case_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Sales_Reference_Request_less_than_5</fullName>
        <description>Sales Reference Request - less than 5</description>
        <protected>false</protected>
        <recipients>
            <recipient>jculley@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>szaki@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Customer_Referral_Case_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Telecom_Closed_Autoreply</fullName>
        <description>Telecom_Closed Autoreply</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>port@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Telecom/Automatic_Email_3_Closed_Telecom_Case</template>
    </alerts>
    <alerts>
        <fullName>Telecom_Pre_Call_Autoreply</fullName>
        <description>Telecom_Pre-Call Autoreply</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>port@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Telecom/Automatic_Reply_1_Pre_Integration_Call</template>
    </alerts>
    <alerts>
        <fullName>Telecom_Solved_Autoreply</fullName>
        <description>Telecom_Solved Autoreply</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>port@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Telecom/Automatic_Reply_2_Solved_Telecom_Case</template>
    </alerts>
    <alerts>
        <fullName>Template_Review_Approved</fullName>
        <description>Template Review - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_Approval_Stage_1_Approved</template>
    </alerts>
    <alerts>
        <fullName>Upload_Approved</fullName>
        <description>Upload Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Assigned_To__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Case_Approval_Stage_2_Approved</template>
    </alerts>
    <alerts>
        <fullName>vCase_Closed</fullName>
        <description>vCase Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support/vCase_Closed</template>
    </alerts>
    <alerts>
        <fullName>vEmail_Public_Comment_to_Case_Contact</fullName>
        <description>vEmail Public Comment to Case Contact</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/vPublic_Case_Comment_to_Customer</template>
    </alerts>
    <alerts>
        <fullName>vSurvey_Sent</fullName>
        <description>vSurvey Sent</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Closed_Survey_Rate</template>
    </alerts>
    <fieldUpdates>
        <fullName>Add_Onboarder_Email_To_Case</fullName>
        <field>Onboarder_Email__c</field>
        <formula>Account.Onboarder__r.Email</formula>
        <name>Add Onboarder Email To Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_2</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_3</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Admin_to_ST_Cases</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>thenry@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Admin to ST Cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Case_to_Ivan</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>ihernandez@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Case to Ivan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Data_Cases_to_Ivan</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>ihernandez@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Data Cases to Ivan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Sales_Reference_to_Brendon</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>bnorton@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Sales Reference to Brendon</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Sales_Reference_to_Casey</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>chackett@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Sales Reference to Casey</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Sales_Reference_to_Jackson</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>jculley@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Sales Reference to Jackson</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Sales_Reference_to_Jeremy</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>jgreen@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Sales Reference to Jeremy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Sales_Reference_to_Jeremy1</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>jgreen@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Sales Reference to Jeremy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Admin</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>thenry@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign to Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Approval_Rejected</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Approval: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Approval_Rejected2</fullName>
        <field>Status</field>
        <literalValue>Match File Creation/Pending Stage 2 Review</literalValue>
        <name>Case Approval: Step 2 Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Approval_Stage_1_Approved</fullName>
        <field>Status</field>
        <literalValue>Match File Creation/Pending Stage 2 Review</literalValue>
        <name>Case Approval: Stage 1 - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Approval_Stage_2_Approved</fullName>
        <field>Status</field>
        <literalValue>Upload Approved/In Progress-Pending Final Review</literalValue>
        <name>Case Approval: Stage 2 - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Reopen_Date</fullName>
        <field>Reopen_Date__c</field>
        <formula>Now()</formula>
        <name>Case - Set Reopen Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_One_Touch</fullName>
        <field>One_Touch__c</field>
        <literalValue>1</literalValue>
        <name>Check One Touch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Case_Description_to_Details</fullName>
        <field>Case_Detail__c</field>
        <formula>Description</formula>
        <name>Copy Case Description to Details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_Received</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Final Approval Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Approval_Rejected</fullName>
        <field>Status</field>
        <literalValue>Upload Approved/In Progress-Pending Final Review</literalValue>
        <name>Final Approval Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Final_Review_Completed</fullName>
        <field>Final_Review_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Final Review Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Match_Review_Completed</fullName>
        <field>Match_Review_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Match Review Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_2</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_3</fullName>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_2</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_3</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopened_Yes</fullName>
        <field>Reopened__c</field>
        <literalValue>1</literalValue>
        <name>Reopened Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Survey_Received</fullName>
        <field>Survey_Response__c</field>
        <literalValue>1</literalValue>
        <name>Set Survey Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval_2</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval_3</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Template_Review_Completed</fullName>
        <field>Template_Review_Completed__c</field>
        <literalValue>1</literalValue>
        <name>Template Review Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_To_to_Sales_Engineer</fullName>
        <field>Assigned_To__c</field>
        <lookupValue>dhall@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update Assigned To to Sales Engineer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Reason_to_Duplicates</fullName>
        <field>Reason</field>
        <literalValue>Duplicates Found</literalValue>
        <name>Update Case Reason to Duplicates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Reason_to_New_App_Request</fullName>
        <field>Reason</field>
        <literalValue>New App/Integration Request</literalValue>
        <name>Update Case Reason to New App Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vCase_Assigned</fullName>
        <field>Assigned_At__c</field>
        <formula>Now()</formula>
        <name>vCase Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vCase_Channel_is_Web</fullName>
        <field>Origin</field>
        <literalValue>Web</literalValue>
        <name>vCase Channel is Web</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vCase_Owner_is_Unassigned</fullName>
        <field>OwnerId</field>
        <lookupValue>Unassigned_Cases</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>vCase Owner is Unassigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vCase_Solved_Date</fullName>
        <field>Solved_At_new__c</field>
        <formula>Now()</formula>
        <name>vCase Solved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vChange_to_Port</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Port</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>vChange to Port</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vEmail_on_Closed_Case_False</fullName>
        <field>Email_on_Closed_Case__c</field>
        <literalValue>0</literalValue>
        <name>vEmail on Closed Case False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vOne_Touch_Uncheck</fullName>
        <field>One_Touch__c</field>
        <literalValue>0</literalValue>
        <name>vOne Touch Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vStatus_is_Open</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>vStatus is Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vStatus_is_Solved</fullName>
        <field>Status</field>
        <literalValue>Solved</literalValue>
        <name>vStatus is Solved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vSurvey_Sent</fullName>
        <field>Survey_Date_Sent__c</field>
        <formula>Today()</formula>
        <name>vSurvey Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vTelecom_Closed</fullName>
        <field>Status</field>
        <literalValue>Solved</literalValue>
        <name>vTelecom Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>vUncheck_Case_Comment</fullName>
        <field>CommentAdded__c</field>
        <literalValue>0</literalValue>
        <name>vUncheck Case Comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Add Onboarder Email To Case</fullName>
        <actions>
            <name>New_Case_Alert_Onboarding_Live_Account</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Add_Onboarder_Email_To_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Onboarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Live</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Admin to ST Cases</fullName>
        <actions>
            <name>Assign_Admin_to_ST_Cases</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>New App/Integration Request,New Automation or Alert Request,New Field(s) Request,New Validation Rule Request,Automation or Alert Adjustment,Data Import Request,Data Question or Issue,Error Message,Field Update,Report/Dashboard Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Validation Rule Adjustment</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Assign Case to Ivan</fullName>
        <actions>
            <name>Assign_Case_to_Ivan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>Sales Manager,Sales Development Representative,Sales Rep,Marketing Development Representative,Business Development Representative,Business Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notEqual</operation>
            <value>Duplicates Found</value>
        </criteriaItems>
        <description>Assign sales Cases to Ivan</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Data Cases to Ivan</fullName>
        <actions>
            <name>Assign_Data_Cases_to_Ivan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Data Import Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>Sales Manager,Sales Development Representative,Sales Rep</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Alias</field>
            <operation>notEqual</operation>
            <value>ihern</value>
        </criteriaItems>
        <description>Assign data cases to Ivan if he is not the case creator.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Non-Sales Cases to Admin</fullName>
        <actions>
            <name>Assign_to_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 or 3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>notEqual</operation>
            <value>Sales Manager,Sales Development Representative,Sales Rep,Marketing Development Representative,Business Development Representative,Business Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Duplicates Found</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Admin Requests</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Assign Sales Engineer to SE Prod Gap Resolution Case</fullName>
        <actions>
            <name>Update_Assigned_To_to_Sales_Engineer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SE Prod Gap Resolution</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Re-Opened Alert</fullName>
        <actions>
            <name>Case_Re_Opened_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Re-Opened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>New App/Integration Request,New Automation or Alert Request,New Field(s) Request,New Validation Rule Request,Automation or Alert Adjustment,Data Import Request,Data Question or Issue,Error Message,Field Update,Report/Dashboard Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Validation Rule Adjustment,Duplicates Found,Improvement Suggestion,Admin Requests</value>
        </criteriaItems>
        <description>Alert SysAdmin when Case has been Re-Opened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Re-Opened Alert - Data Enrichment Uploads</fullName>
        <actions>
            <name>Case_Re_Opened_Data_Enrichment_Uploads</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Re-Opened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Data Enrichment Upload Request</value>
        </criteriaItems>
        <description>Alert Sales Ops Manager when Case has been Re-Opened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Re-Opened Alert - SE Prod Gap Resolution</fullName>
        <actions>
            <name>Case_Re_Opened_SEProdGapResolution</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Re-Opened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SE Prod Gap Resolution</value>
        </criteriaItems>
        <description>Alert Sales Engineer when Case has been re-opened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Duplicate Email To Case</fullName>
        <actions>
            <name>Copy_Case_Description_to_Details</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Reason_to_Duplicates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.ToAddress</field>
            <operation>equals</operation>
            <value>duplicates@servicetitan.com</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Case Alert</fullName>
        <actions>
            <name>New_Case_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>New App/Integration Request,New Automation or Alert Request,New Field(s) Request,New Custom Object,New Validation Rule Request,Automation or Alert Adjustment,Data Import Request,Data Question or Issue,Error Message,Field Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Report/Dashboard Request,Validation Rule Adjustment,Duplicates Found,Improvement Suggestion,Admin Requests</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Case Alert - Data Enrichment Upload</fullName>
        <actions>
            <name>New_Case_Alert_Data_Enrichment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Data Enrichment Upload Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Case Alert - SE Prod Gap Resolution</fullName>
        <actions>
            <name>New_Case_Alert_SEproductgapresolution</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SE Prod Gap Resolution</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Case Alert - SalesOps</fullName>
        <actions>
            <name>New_Case_Alert_SalesOps</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 4 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>New App/Integration Request,New Automation or Alert Request,New Field(s) Request,New Validation Rule Request,Automation or Alert Adjustment,Data Import Request,Data Question or Issue,Error Message,Field Update,Report/Dashboard Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Validation Rule Adjustment</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Department</field>
            <operation>equals</operation>
            <value>SalesOps</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Data Case Alert</fullName>
        <actions>
            <name>New_Data_Case_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>equals</operation>
            <value>Data Question or Issue</value>
        </criteriaItems>
        <description>Notifies both Trina and Chris for data alerts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sales Reference Request - 15 to 24</fullName>
        <actions>
            <name>Sales_Reference_Request_15_to_24</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Sales_Reference_to_Casey</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Technician_Count__c</field>
            <operation>lessOrEqual</operation>
            <value>24</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Customer Reference Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Technician_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>15</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Reference Request - 25%2B</fullName>
        <actions>
            <name>Sales_Reference_Request_25</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Sales_Reference_to_Jeremy1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Technician_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Customer Reference Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Reference Request - 5 to 14</fullName>
        <actions>
            <name>Sales_Reference_Request_5_to_14</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Sales_Reference_to_Brendon</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Technician_Count__c</field>
            <operation>lessOrEqual</operation>
            <value>14</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Customer Reference Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Technician_Count__c</field>
            <operation>greaterOrEqual</operation>
            <value>5</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Sales Reference Request - less than 5</fullName>
        <actions>
            <name>Sales_Reference_Request_less_than_5</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Assign_Sales_Reference_to_Jackson</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Technician_Count__c</field>
            <operation>lessThan</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Customer Reference Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Software Request Email To Case</fullName>
        <actions>
            <name>Copy_Case_Description_to_Details</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Reason_to_New_App_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Software Request: Form submissions detected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vCase Assigned</fullName>
        <actions>
            <name>vCase_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>vStatus_is_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
 ISNULL(Assigned_At__c),
 ISPICKVAL(Status ,&quot;New&quot;),
CONTAINS( Owner:User.UserRole.Name , &quot;Support&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vCase Reopened</fullName>
        <actions>
            <name>Case_Set_Reopen_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reopened_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tracking the data a case was reopened.</description>
        <formula>AND(ISCHANGED(IsClosed),
IsClosed = FALSE,
OR(Reopen_Date__c = NULL,ISBLANK(Reopen_Date__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>vCase Solved</fullName>
        <actions>
            <name>vCase_Solved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vChange to Port</fullName>
        <actions>
            <name>vChange_to_Port</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If case record type = customer support &amp; owner = telecom queue, change to port record type</description>
        <formula>AND(
 Owner:Queue.Id = &quot;00G1a000001HhZC&quot;,
 RecordTypeId = &quot;0121a000000RL7Y&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vClosed Pending Tickets</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SFDC Cases</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_Case</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>168</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vCommunity Case</fullName>
        <actions>
            <name>vCase_Channel_is_Web</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>vCase_Owner_is_Unassigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(CreatedBy.Profile.Name, &quot;Community&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>vEmail on Closed Case</fullName>
        <actions>
            <name>vCase_Closed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>vEmail_on_Closed_Case_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Email_on_Closed_Case__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vOne Touch</fullName>
        <actions>
            <name>Check_One_Touch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Support,TDG,Port</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Solved,Closed</value>
        </criteriaItems>
        <description>Check One Touch upon case creation.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>vOne Touch Uncheck</fullName>
        <actions>
            <name>vOne_Touch_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Support,TDG,Port</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New,Solved,Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vPending notifications</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SFDC Cases,SFDC Cases-ST Internal,Port</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Pending_notification_5_days</name>
                <type>Alert</type>
            </actions>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Pending_notification_24_hours</name>
                <type>Alert</type>
            </actions>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vPending to Solved</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SFDC Cases</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>vStatus_is_Solved</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>96</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vPending to Solved2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>vStatus_is_Solved</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vQuick Response</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 5 AND 6 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SFDC Cases-ST Internal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BusinessHoursId</field>
            <operation>equals</operation>
            <value>Support Hours</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.VictorOps__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Quick_Response_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vSend Public Comment to Customer</fullName>
        <actions>
            <name>vEmail_Public_Comment_to_Case_Contact</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>vUncheck_Case_Comment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comment_Email_sent_to_Case_Contact</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CommentAdded__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vSend Survey - chat or phone</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Survey_Date_Sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Phone,Chat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Rate__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Root_Cause__c</field>
            <operation>notEqual</operation>
            <value>Ticket Workflow</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Issue_Type__c</field>
            <operation>notEqual</operation>
            <value>Unknown - Need Info</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Component__c</field>
            <operation>notEqual</operation>
            <value>Infra</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>vSurvey_Sent</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>vSurvey_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vSend Survey - email or web</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Survey_Date_Sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Root_Cause__c</field>
            <operation>notEqual</operation>
            <value>Ticket Workflow</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Issue_Type__c</field>
            <operation>notEqual</operation>
            <value>Unknown - Need Info</value>
        </criteriaItems>
        <description>Rule deactivated 1/18/2019 by AH. Taken over by new cloned rule &quot;vSend Survey - email or web v2&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>vSurvey_Sent</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>vSurvey_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>20</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vSend Survey - email or web v2</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Customer Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Survey_Date_Sent__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email,Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Root_Cause__c</field>
            <operation>notEqual</operation>
            <value>Ticket Workflow</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Issue_Type__c</field>
            <operation>notEqual</operation>
            <value>Unknown - Need Info</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Component__c</field>
            <operation>notEqual</operation>
            <value>Infra</value>
        </criteriaItems>
        <description>Changing Rule to send out survey 2 hours after and not 20 hours</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>vSurvey_Sent</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>vSurvey_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vSolved to Closed Tickets</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SFDC Cases</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_Case</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>144</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>vSurvey Response</fullName>
        <actions>
            <name>Set_Survey_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Rate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Final_Resolution_Rate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>vTelecom Closed</fullName>
        <actions>
            <name>vTelecom_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
RecordTypeId = &quot;0121a000000RL7i&quot;, 
SuppliedEmail = &quot;dashboard-updatenotification@bandwidth.com&quot;, 
TEXT(Status) &lt;&gt; &quot;Solved&quot;, 
TEXT(Status) &lt;&gt; &quot;Closed&quot;, 
OR((CONTAINS( Subject , &quot;Cancelled&quot;)), 
(CONTAINS( Subject , &quot;Pending_documents&quot;)), 
(CONTAINS( Subject , &quot;Submitted&quot;)), 
(CONTAINS( Subject , &quot;Requested_cancel&quot;))) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>vTelecom solved</fullName>
        <actions>
            <name>vStatus_is_Solved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
 RecordTypeId = &quot;0121a000000RL7i&quot;,
 SuppliedEmail = &quot;dashboard-updatenotification@bandwidth.com&quot;,
 TEXT(Status) &lt;&gt; &quot;Solved&quot;,
 TEXT(Status) &lt;&gt; &quot;Closed&quot;,
NOT(CONTAINS(  Subject , &quot;Port&quot;)),
NOT(CONTAINS(  Subject , &quot;Note&quot;)),
NOT(CONTAINS(  Subject , &quot;FOC&quot;)),
NOT(CONTAINS(  Subject , &quot;EXCEPTION&quot;)),
NOT(CONTAINS(  Subject , &quot;CANCELLED&quot;)),
NOT(CONTAINS(  Subject , &quot;REQUESTED_CANCEL&quot;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Comment_Email_sent_to_Case_Contact</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Comment Email sent to Case Contact</subject>
    </tasks>
</Workflow>
