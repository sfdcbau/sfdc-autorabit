<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ReferenceRequestMaximumReached</fullName>
        <description>ReferenceRequestMaximumReached</description>
        <protected>false</protected>
        <recipients>
            <recipient>CRM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CRP_Email_Templates/CRPRecordActivitiesAvailableEqualsZero</template>
    </alerts>
    <alerts>
        <fullName>StatusChangeEmailAlertToCRPNominator</fullName>
        <description>StatusChangeEmailAlertToCRPNominator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CRP_Email_Templates/CRPRecordStatusChange</template>
    </alerts>
</Workflow>
