<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ABS_of_Est_Techs_when_below_0</fullName>
        <field>Estimated_Total_Potential_Techs__c</field>
        <formula>ABS( Estimated_Total_Potential_Techs__c )</formula>
        <name>ABS of Est Techs when below 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ABS_of_PercentResidential_when_below_0</fullName>
        <field>ResidentialPercentage__c</field>
        <formula>ABS( ResidentialPercentage__c )</formula>
        <name>ABS of %Residential when below 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Status_to_No_Show</fullName>
        <field>Status__c</field>
        <literalValue>No Show</literalValue>
        <name>Contact Status to No Show</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CopyAcctPhoneToContactAcctPhoneWhenBlank</fullName>
        <field>AccountPhone__c</field>
        <formula>Account.Phone</formula>
        <name>CopyAcctPhoneToContactAcctPhoneWhenBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDupliContact</fullName>
        <field>DupliContact__c</field>
        <formula>Account.X18_digit_ID__c +  Account.Name</formula>
        <name>UpdateDupliContact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ABS of Est Techs OR %25 Residential when below 0</fullName>
        <actions>
            <name>ABS_of_Est_Techs_when_below_0</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ABS_of_PercentResidential_when_below_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
ISCHANGED( Estimated_Total_Potential_Techs__c ),
ISCHANGED(  ResidentialPercentage__c  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact Status to No Show</fullName>
        <actions>
            <name>Contact_Status_to_No_Show</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.No_Show_CP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Copy Account Phone to Contact when Blank</fullName>
        <actions>
            <name>CopyAcctPhoneToContactAcctPhoneWhenBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Phone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AccountPhone__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UpdateDuplicateContact</fullName>
        <actions>
            <name>UpdateDupliContact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.X18_digit_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>UpdateDuplicateContact</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
