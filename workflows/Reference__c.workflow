<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reference_Completion_Reminder_for_AE</fullName>
        <description>Reference Completion Reminder for AE</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/Reference_Completion_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Asked_In_Last_30_Days</fullName>
        <field>Asked_In_Last_30_Days__c</field>
        <literalValue>1</literalValue>
        <name>Update Asked In Last 30 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Asked_In_to_No_When_Not_In_Last_3</fullName>
        <description>Update the &quot;Asked In Last 30 Days&quot; checkbox to False when not in the last 30 days</description>
        <field>Asked_In_Last_30_Days__c</field>
        <literalValue>0</literalValue>
        <name>Update Asked In to No When Not In Last 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_In_Last_30_Days</fullName>
        <field>Completed_In_Last_30_Days__c</field>
        <literalValue>1</literalValue>
        <name>Update Completed In Last 30 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_In_Last_30_Days_when_da</fullName>
        <field>Completed_In_Last_30_Days__c</field>
        <literalValue>1</literalValue>
        <name>Update Completed In Last 30 Days when da</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Reference_Asked_On_Date</fullName>
        <field>Last_Asked_Reference_On__c</field>
        <formula>TODAY()</formula>
        <name>Update Last Reference Asked On Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account_Reference__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>When_Completed_On_NOT_in_Last_30_uncheck</fullName>
        <field>Completed_In_Last_30_Days__c</field>
        <literalValue>0</literalValue>
        <name>When Completed On NOT in Last 30 uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Asked In Last 30 Days</fullName>
        <actions>
            <name>Update_Asked_In_Last_30_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.Asked_In_Last_30_Days_formula__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Asked In to No When Not In Last 30 Days</fullName>
        <actions>
            <name>Update_Asked_In_to_No_When_Not_In_Last_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.Asked_In_Last_30_Days_formula__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Completed In Last 30 Days when date Not in 30 Days</fullName>
        <actions>
            <name>When_Completed_On_NOT_in_Last_30_uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.CompletedOn__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Reference__c.Completed_On_Date_InLast_30_Days_Formula__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Completed In Last 30 Days when date added</fullName>
        <actions>
            <name>Update_Completed_In_Last_30_Days_when_da</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.CompletedOn__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Reference__c.Completed_On_Date_InLast_30_Days_Formula__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Reference Asked On Date</fullName>
        <actions>
            <name>Update_Last_Reference_Asked_On_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reference__c.Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
