<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <knowledgePublishes>
        <fullName>Publish</fullName>
        <action>PublishAsNew</action>
        <label>Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>vDate to Publish</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Knowledge__kav.PublishStatus</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Knowledge__kav.Date_to_Publish_Article__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Publish</name>
                <type>KnowledgePublish</type>
            </actions>
            <offsetFromField>Knowledge__kav.Date_to_Publish_Article__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
