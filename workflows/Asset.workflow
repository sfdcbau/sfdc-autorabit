<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Notification_to_Owner</fullName>
        <description>Send Notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Send_Noti_on_Asset_Creation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Churn_Date</fullName>
        <field>Churn_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Churn Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Churn_Date_Asset</fullName>
        <field>Churn_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Churn Date-Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Deactivated_by_Partner_Date</fullName>
        <field>Deactivated_by_Partner_Date_Asset__c</field>
        <formula>TODAY()</formula>
        <name>Set Deactivated by Partner Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Deactivated_by_Partner_Date_Asset</fullName>
        <field>Deactivated_by_Partner_Date_Asset__c</field>
        <formula>TODAY()</formula>
        <name>Set Deactivated by Partner Date-Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Denied_by_Partner_Date</fullName>
        <field>Denied_by_Partner_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Denied by Partner Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Denied_by_Partner_Date_Asset</fullName>
        <field>Denied_by_Partner_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Denied by Partner Date-Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Live_Date_Asset</fullName>
        <field>Asset_Live_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Live Date-Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Live_Using_Date</fullName>
        <field>Live_Using_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Live &amp; Using Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Onboarding_Start_Date_Asset</fullName>
        <field>Onboarding_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Onboarding Start Date-Asset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Autopopulate Churn Date-Asset</fullName>
        <actions>
            <name>Set_Churn_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Inactive_Reason__c</field>
            <operation>equals</operation>
            <value>Platform Churn,ST Churn</value>
        </criteriaItems>
        <description>Set date based on when Inactive Reason is set to &quot;Platform Churn&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Deactivated by Partner Date-Asset</fullName>
        <actions>
            <name>Set_Deactivated_by_Partner_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Inactive_Reason__c</field>
            <operation>equals</operation>
            <value>Deactivated by Partner</value>
        </criteriaItems>
        <description>Set date based on Inactive Reason</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Denied by Partner Date-Asset</fullName>
        <actions>
            <name>Set_Denied_by_Partner_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.Inactive_Reason__c</field>
            <operation>equals</operation>
            <value>Denied by Partner</value>
        </criteriaItems>
        <description>Set date based on Inactive Reason</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Live %26 Using Date-Asset</fullName>
        <actions>
            <name>Set_Live_Using_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Live &amp; Using</value>
        </criteriaItems>
        <description>Set Live &amp; Using date based on when Asset Status is set to &quot;Live &amp; Using&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Live Date-Asset</fullName>
        <actions>
            <name>Set_Live_Date_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Live</value>
        </criteriaItems>
        <description>Set Live date based on when Asset Status is set to &quot;Live&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Autopopulate Onboarding Start Date-Asset</fullName>
        <actions>
            <name>Set_Onboarding_Start_Date_Asset</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Onboarding</value>
        </criteriaItems>
        <description>Set date based on when Asset Status is set to &quot;Onboarding&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Asset Create Notification</fullName>
        <actions>
            <name>Send_Notification_to_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Status</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Asset.RecordTypeId</field>
            <operation>equals</operation>
            <value>Upsell</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
