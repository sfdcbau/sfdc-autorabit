<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AE_Discovery_Call_No_Show</fullName>
        <description>AE Discovery Call No Show</description>
        <protected>false</protected>
        <recipients>
            <field>SDR_MDR_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/AE_Discovery_No_Show</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert_CustPartners_Opp</fullName>
        <description>Churn Alert_CustPartners_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>lgildea@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert_For_ST_Billing_Opp</fullName>
        <ccEmails>billing@servicetitan.com</ccEmails>
        <description>Churn Alert For ST Billing_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert_STBilling</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert_Opp</fullName>
        <ccEmails>csops@servicetitan.com</ccEmails>
        <description>Churn Alert_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhabel@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bnorton@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jculley@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oalomar@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Closed_Deal_Email</fullName>
        <description>Closed Deal Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kdorsey@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mlui@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>snunez@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zkahn@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>SDR_MDR_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Deal_Email</template>
    </alerts>
    <alerts>
        <fullName>DQ_Rationale_Email_Send</fullName>
        <description>DQ Rationale Email Send</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jferguson@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mlui@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>molson@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>snunez@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tdegolia@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zkahn@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>SDR_MDR_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales/DQ_Rationale_Email_send</template>
    </alerts>
    <alerts>
        <fullName>Ent_MM_Customer_Welcome_Email_Alert</fullName>
        <description>Ent/MM Customer Welcome Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarding_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/Ent_MM_Customer_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>Ent_MM_Welcome_Email</fullName>
        <description>Ent/MM Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarding_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Onboarding/Ent_MM_Customer_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>Merger_Acquisition_Alert_to_CS_Ops</fullName>
        <ccEmails>csops@servicetitan.com</ccEmails>
        <description>Merger/Acquisition Alert to CS Ops</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/MergerAcquisition_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>New_Markets_Won_Opp_Alert</fullName>
        <ccEmails>newmarkets@servicetitan.com</ccEmails>
        <description>Alerts new markets team when an opportunity is closed won</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/New_Markets_Won_Opp_Alert</template>
    </alerts>
    <alerts>
        <fullName>OB_Academy_Email_Alert</fullName>
        <description>OB Academy Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/OB_Academy_Email</template>
    </alerts>
    <alerts>
        <fullName>OB_Marketing_Email_Alert</fullName>
        <description>OB Marketing Email_Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/OB_Marketing_Email</template>
    </alerts>
    <alerts>
        <fullName>OB_Reminder_Email_Alert</fullName>
        <description>OB Reminder Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/OB_Reminder_Email</template>
    </alerts>
    <alerts>
        <fullName>OB_Reminder_Email_Alert_New</fullName>
        <description>OB Reminder Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarding_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Onboarding/OB_Reminder_Email</template>
    </alerts>
    <alerts>
        <fullName>OB_Self_Setup_Email_Alert</fullName>
        <description>OB Self Setup Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarding_POC__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/OB_Welcome_Setup_Email</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Owner_and_their_Manager_Alert</fullName>
        <description>Opportunity Owner and their Manager Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jferguson@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>zkahn@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Send_Email_to_Oppt_Owner_and_their_Manager</template>
    </alerts>
    <alerts>
        <fullName>Payments_Opportunity_Email_Notification</fullName>
        <description>Payments Opportunity Email Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Opportunity_Go_Live_Date_Notification</template>
    </alerts>
    <alerts>
        <fullName>Pending_Onboarding_Alert</fullName>
        <description>Pending (Onboarding) Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>jjones@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/PendingOnboardingAlert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert_CustPartners_Opp</fullName>
        <description>Rejection Alert_CustPartners_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>lgildea@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert_Opp</fullName>
        <description>Rejection Alert_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mdegroot@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert_Product_Opp</fullName>
        <description>Rejection Alert_Product_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>avartazarian@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hahluwalia@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Send_Mail_to_Payment_Support</fullName>
        <ccEmails>skaurav@servicetitan.com,jchu@servicetitan.com</ccEmails>
        <description>Send Mail to Payment Support</description>
        <protected>false</protected>
        <recipients>
            <recipient>pnethikunta@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ST_PaymentSupportEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>Stage_is_onboarding</fullName>
        <description>Stage is onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>egoumas@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hlim@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Stage_is_onboarding</template>
    </alerts>
    <alerts>
        <fullName>WorldPay_Alert_ProjLiveDate_Opp</fullName>
        <ccEmails>Perry.Pokrandt@worldpay.us</ccEmails>
        <description>WorldPay Alert_ProjLiveDate_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/CurrProjLiveDateAlert</template>
    </alerts>
    <alerts>
        <fullName>WorldPay_Alert_for_Perry_Opp</fullName>
        <ccEmails>Perry.Pokrandt@worldpay.us</ccEmails>
        <description>WorldPay Alert for Perry_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/WorldPay_Alert</template>
    </alerts>
    <alerts>
        <fullName>WorldPay_Alert_for_Perry_wSuccessRep_Opp</fullName>
        <ccEmails>Perry.Pokrandt@worldpay.us</ccEmails>
        <description>WorldPay Alert for Perry w/SuccessRep_Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/WorldPay_Alert_W_SuccessRep</template>
    </alerts>
    <fieldUpdates>
        <fullName>Bizible_Opp_Amount</fullName>
        <field>bizible2__Bizible_Opportunity_Amount__c</field>
        <formula>Total_Projected_MRR__c</formula>
        <name>Bizible Opp Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Discovery_Call_Stage_Date_Update</fullName>
        <description>Update the Discovery Call Stage Date field when Opportunity hits this stage</description>
        <field>Discovery_Call_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Discovery Call Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_AE_Probability</fullName>
        <field>Initial_AE_Probability__c</field>
        <formula>VALUE(TEXT( AE_Probability__c ))/100</formula>
        <name>Initial AE Probability</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_Close_Date</fullName>
        <field>Initial_Close_Date__c</field>
        <formula>CloseDate</formula>
        <name>Initial Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Negotiation_Stage_Date_Update</fullName>
        <field>Negotiation_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Negotiation Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_3</fullName>
        <field>Health_Score__c</field>
        <literalValue>3 - Happy but burdened by things</literalValue>
        <name>OB - 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_4</fullName>
        <field>Health_Score__c</field>
        <literalValue>4 - Very happy but not evangelist</literalValue>
        <name>OB - 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_5</fullName>
        <field>Health_Score__c</field>
        <literalValue>5 - Evangelist</literalValue>
        <name>OB - 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Cancel</fullName>
        <field>Health_Score__c</field>
        <literalValue>0 - Wants to cancel</literalValue>
        <name>OB - Cancel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Code_Orange</fullName>
        <field>Health_Score__c</field>
        <literalValue>2 - Code Orange</literalValue>
        <name>OB - Code Orange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OB_Code_Red</fullName>
        <field>Health_Score__c</field>
        <literalValue>1 - Code Red</literalValue>
        <name>OB - Code Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OnboarderUpdate</fullName>
        <field>OnboarderOnOpp__c</field>
        <formula>Account.Onboarder__c</formula>
        <name>OnboarderUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Onboarding_Disengaged_Stage_Date_Update</fullName>
        <field>OnboardingDisengagedStageDate__c</field>
        <formula>TODAY()</formula>
        <name>Onboarding Disengaged Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_field_Risk_Level_Automation</fullName>
        <description>If % Residential on the account is &lt;90 AND Managed Tech Quantity is greater than or equal to 30, then it should automatically be flipped to 4.</description>
        <field>Risk_Level__c</field>
        <literalValue>4</literalValue>
        <name>Opportunity field Risk Level Automation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pricing_Stage_Date_Update</fullName>
        <field>Pricing_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Pricing Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Q_A_Stage_Date_Update</fullName>
        <description>Update the Q&amp;A / DEMO Stage Date field when Opportunity hits this stage</description>
        <field>Q_A_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Q&amp;A Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Disco_Stage_Date</fullName>
        <field>Discovery_Call_Stage_Date__c</field>
        <name>Remove Disco Stage Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stage_Zero_Stage_Date_Update</fullName>
        <field>Stage_Zero_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Stage Zero Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Success_Stage_Date</fullName>
        <field>Success_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Success Stage Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePushedToPipe</fullName>
        <field>Pushed_to_Pipe__c</field>
        <literalValue>Updated</literalValue>
        <name>UpdatePushedToPipe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Grace_Period_End_Date</fullName>
        <description>Stamps grace period end date on Account when Opp is flipped to Onboarding.</description>
        <field>GracePeriodEndDate__c</field>
        <formula>Grace_Period_End_Date__c</formula>
        <name>Update Account Grace Period End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Booked_Meeting_Date_Time</fullName>
        <field>LastBookedMeetingDateTime__c</field>
        <formula>NOW()</formula>
        <name>Update Booked Meeting Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Date</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Update Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disco_Date_with_Close_Date</fullName>
        <description>Update Disco Date with Close Date</description>
        <field>Discovery_Call_Stage_Date__c</field>
        <formula>CloseDate</formula>
        <name>Update Disco Date with Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Onboarding_Vetting_Date_Field</fullName>
        <field>OnboardingVettingDate__c</field>
        <formula>TODAY()</formula>
        <name>Update Onboarding Vetting Date Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Grace_Period_End_Date</fullName>
        <description>Stamps grace period end date on Opp when Opp is flipped to Onboarding.</description>
        <field>GracePeriodEndDate__c</field>
        <formula>Grace_Period_End_Date__c</formula>
        <name>Update Opportunity Grace Period End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_Email_Field</fullName>
        <description>Update Update Oppty Email Field when Meets Criteria for ST</description>
        <field>OpptyEmailUpdate__c</field>
        <literalValue>0</literalValue>
        <name>Update Oppty Email Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Won_Stage_Date</fullName>
        <field>Won_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Won Stage Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Write_Off_Stage_Date_Update</fullName>
        <field>Write_Off_Stage_Date__c</field>
        <formula>Today()</formula>
        <name>Write Off Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Backdate Disco Date when Unsellable to Sellable</fullName>
        <actions>
            <name>Update_Disco_Date_with_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Backdate Disco Date when Unsellable to Sellable using the close date</description>
        <formula>RecordTypeId=$Label.RecordTypeID_SMBSales_Opp &amp;&amp;  Sellable_vs_Unsellable__c = &quot;Sellable&quot; &amp;&amp; Discovery_Call_Stage_Date__c = NULL &amp;&amp; ISPICKVAL(StageName, &quot;Closed Lost&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clear Discovery Call Stage Date for Unsellable Opps</fullName>
        <actions>
            <name>Remove_Disco_Stage_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Discovery_Call_Stage_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sellable_vs_Unsellable__c</field>
            <operation>equals</operation>
            <value>Unsellable</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Whenever an Opp lands in an Unsellable and Unknown disposition make sure to clear the Discovery Call Stage Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Deal Email</fullName>
        <actions>
            <name>Closed_Deal_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>test account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Lost Date</fullName>
        <actions>
            <name>Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Update opportunity Close Date when opportunity stage = Closed Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Discovery Call Stage Date Update %2B Initial Close Date%2FProbability</fullName>
        <actions>
            <name>Discovery_Call_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Initial_AE_Probability</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Initial_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Discovery Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Update the Discovery Call Stage Date, Initial Close Date, Initial AE Probability fields when Opportunity hits this stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Discovery Call Stage Date_Platform</fullName>
        <actions>
            <name>Discovery_Call_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Discovery Call</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ServiceTitan Payments,Integrated Financing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ent%2FMM Closed Won Welcome Email</fullName>
        <active>true</active>
        <formula>(ISPICKVAL(StageName , &apos;Onboarding&apos;)  ||  ISPICKVAL(StageName , &apos;Closed Won&apos;) ) &amp;&amp;  NOT( ISNULL( CloseDate ) )  &amp;&amp;  NOT( ISNULL(Onboarding_POC__c ) )  &amp;&amp;  RecordType.Name = &apos;Sales&apos;    &amp;&amp;  (ISPICKVAL(Account.Account_Level__c  , &apos;MidMarket&apos;)  ||  ISPICKVAL(Account.Account_Level__c  , &apos;Enterprise&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Ent_MM_Welcome_Email</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Grace Period End Date Stamp</fullName>
        <actions>
            <name>Update_Account_Grace_Period_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opportunity_Grace_Period_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Stamps the grace period end date when the opportunity is updated to &quot;Onboarding&quot;</description>
        <formula>RecordTypeId=$Label.RecordTypeID_SMBSales_Opp &amp;&amp; IsWon=TRUE &amp;&amp; Estimated_Total_Potential_Technicians__c&lt;=24 &amp;&amp; NOT(ISPICKVAL(Account.Account_Level__c, &quot;Strategic&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Insert Primary Contact</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.PrimaryContactFirstName__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Negotiation Stage Date Update</fullName>
        <actions>
            <name>Negotiation_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation / Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales,ServiceTitan Payments,Integrated Financing</value>
        </criteriaItems>
        <description>Update the Negotiation Stage Date field when Opportunity hits this stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OB Reminder Email</fullName>
        <active>true</active>
        <formula>OBDiscoveryCallDate__c  &gt;  CloseDate  + 5  &amp;&amp; NOT( ISNULL(OBDiscoveryCallDate__c))   &amp;&amp;  NOT( ISNULL( CloseDate )) &amp;&amp;  Disable_OB_Disco_Reminder_Email__c  = False &amp;&amp;    (ISPICKVAL(Account.Account_Level__c, &apos;1 to Many&apos;) ||  ISPICKVAL(Account.Account_Level__c, &apos;Emerging&apos;) )  &amp;&amp;  RecordType.Name  = &apos;Sales&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>OB_Reminder_Email_Alert_New</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.OBDiscoveryCallDate__c</offsetFromField>
            <timeLength>-2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OB Self Setup Email %2F OB Academy Email Current</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Level__c</field>
            <operation>equals</operation>
            <value>OneToMany,Emerging</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Sends Self Setup Email to customer for 1:M and Emerging 2 hours after opp close date and Academy Email 5 days after</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>OB_Self_Setup_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Onboarding Disengaged Stage Date Update</fullName>
        <actions>
            <name>Onboarding_Disengaged_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Onboarding Disengaged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Update the Onboarding Disengaged field when Opportunity hits this stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding Vetting Stage Date Update</fullName>
        <actions>
            <name>Update_Onboarding_Vetting_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>SE Vetting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Update the SE Vetting Date field when Opportunity hits this stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Risk Level Automation</fullName>
        <actions>
            <name>Opportunity_field_Risk_Level_Automation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.Residential__c</field>
            <operation>lessThan</operation>
            <value>90</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Managed_Tech_Quantity__c</field>
            <operation>greaterOrEqual</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Residential__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If % Residential on the account is &lt;90 AND Managed Tech Quantity is greater than or equal to 30, then it should automatically be flipped to 4</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Payments Opp Notification</fullName>
        <active>true</active>
        <description>This will send and email alert to Opp owner if (Sales_OB_Projected_Go_Live_Date__c -  TODAY()) &gt; 14). Jira User Story SE-915</description>
        <formula>AND( $RecordType.Name = &apos;ServiceTitan Payments&apos;,  ISPICKVAL(StageName, &apos;Prospect&apos;), (Sales_OB_Projected_Go_Live_Date__c -  TODAY()) &gt; 14)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Payments_Opportunity_Email_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Sales_OB_Projected_Go_Live_Date__c</offsetFromField>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Pricing Stage Date Update</fullName>
        <actions>
            <name>Pricing_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Pricing/ROI Assessment Stage Date field when Opportunity hits this stage</description>
        <formula>RecordTypeId=$Label.RecordTypeID_SMBSales_Opp &amp;&amp;  ISPICKVAL( StageName , &quot;Pricing / ROI Assessment&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PushedToPipeUpdate</fullName>
        <actions>
            <name>UpdatePushedToPipe</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update PushedToPipe Field on Opportunity When Event Type is Scheduled Discovery Call and End Date Time is More is than 72 Hours.</description>
        <formula>ISCHANGED(StageName) &amp;&amp; TEXT(PRIORVALUE(StageName))=&apos;Stage Zero&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Q%26A Stage Date Update</fullName>
        <actions>
            <name>Q_A_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Q&amp;A / DEMO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Update the Q&amp;A /DEMO Stage Date field when Opportunity hits this stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ST_SendMailToPaymentSupport</fullName>
        <actions>
            <name>Send_Mail_to_Payment_Support</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Stored_Tokens_Import__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ServiceTitan Payments</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Sales email LIVE</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Live</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.X18_digit_ID__c</field>
            <operation>notEqual</operation>
            <value>0011a00000Hov1yAAB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Automatically sends an email to entire sales team notifying that a company went live</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stage Zero Stage Date Update</fullName>
        <actions>
            <name>Stage_Zero_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Booked_Meeting_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Stage Zero Stage Date field when Opportunity hits this stage</description>
        <formula>RecordTypeId=$Label.RecordTypeID_SMBSales_Opp &amp;&amp; ISPICKVAL( StageName , &quot;Stage Zero&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Stage is onboarding</fullName>
        <actions>
            <name>Stage_is_onboarding</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.X18_digit_ID__c</field>
            <operation>notEqual</operation>
            <value>0011a00000Hov1yAAB</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Account is Onboarding and Account is not the TEST ACCOUNT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Success Stage Date</fullName>
        <actions>
            <name>Success_Stage_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Success_Stage_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TestSZ</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Onboarding Disengaged,Closed Won,Closed Lost,Onboarding</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Bizible Opportunity Amount</fullName>
        <actions>
            <name>Bizible_Opp_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Total_Projected_MRR__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateOnboarder</fullName>
        <actions>
            <name>OnboarderUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Onboarder__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Won Stage Date</fullName>
        <actions>
            <name>Update_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Won_Stage_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Won_Stage_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Write Off Stage Date Update</fullName>
        <actions>
            <name>Write_Off_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Write Off</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales</value>
        </criteriaItems>
        <description>Update the Write Off Stage Date field when Opportunity hits this stage</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
