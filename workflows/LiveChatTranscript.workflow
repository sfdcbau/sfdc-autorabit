<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Chat_Transcript</fullName>
        <description>Send Chat Transcript</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>support@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Support/vChat_Transcript</template>
    </alerts>
    <rules>
        <fullName>Live Chat Transcript Email</fullName>
        <active>true</active>
        <criteriaItems>
            <field>LiveChatTranscript.Body</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_Chat_Transcript</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>LiveChatTranscript.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
