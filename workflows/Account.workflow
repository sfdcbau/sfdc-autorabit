<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Name_Changed</fullName>
        <description>Account Name Changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Account_Name_Change</template>
    </alerts>
    <alerts>
        <fullName>Account_Ownership_Change_Email_Alert</fullName>
        <ccEmails>ttehenry@gmail.com</ccEmails>
        <description>Account Ownership Change Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Record_Ownership_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Alert_Onboarder_has_been_assigned</fullName>
        <description>Alert: Onboarder has been assigned</description>
        <protected>false</protected>
        <recipients>
            <field>Onboarder__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/Onboarder_Assigned_Alert</template>
    </alerts>
    <alerts>
        <fullName>Change_to_Current_Projected_Live_Date</fullName>
        <description>Change to Current Projected Live Date</description>
        <protected>false</protected>
        <recipients>
            <recipient>gainsight@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Change_to_current_projected_live_date</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert</fullName>
        <ccEmails>csops@servicetitan.com</ccEmails>
        <description>Churn Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhabel@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>bnorton@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jculley@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oalomar@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Churn_Alert_CustPart</fullName>
        <description>Churn Alert for Accounts With Customer Partners</description>
        <protected>false</protected>
        <recipients>
            <recipient>lgildea@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Churn_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>CodeOrangeResolutionAlert</fullName>
        <description>CodeOrangeResolutionAlert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/CodeOrangeResolution</template>
    </alerts>
    <alerts>
        <fullName>Current_Health_Score_changes_to_1</fullName>
        <description>Current Health Score changes to 1</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Success/CurrentHealthScoreChangedEmail</template>
    </alerts>
    <alerts>
        <fullName>Current_Health_Score_changes_to_2</fullName>
        <description>Current Health Score changes to 2</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Success/CurrentHealthScoreChangedEmail</template>
    </alerts>
    <alerts>
        <fullName>Current_Score_Change_Onboarding</fullName>
        <description>Current Health Score Change_Onboarding</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mdegroot@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vach@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HealthScoreUpdated</template>
    </alerts>
    <alerts>
        <fullName>HealthScoreChangesTo1_Onboarding_PossibleRejection</fullName>
        <description>HealthScoreChangesTo1_Onboarding_PossibleRejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/HealthScore1OnboardingAlert_PossibleRejection</template>
    </alerts>
    <alerts>
        <fullName>HealthScoreChangesTo2_Onboarding_PossibleRejection</fullName>
        <description>HealthScoreChangesTo2_Onboarding_PossibleRejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/HealthScore2OnboardingAlert_PossibleRejection</template>
    </alerts>
    <alerts>
        <fullName>Health_Score_is_0</fullName>
        <description>Current Health Score changes to 0</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Success/CurrentHealthScoreChangedEmail</template>
    </alerts>
    <alerts>
        <fullName>Live_Success_Onboarding_CustStatus_changed_Incorectly</fullName>
        <description>Live/Success/Onboarding Status changed Incorrectly</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Changed_from_Live_Success_to_SDR_Accepted</template>
    </alerts>
    <alerts>
        <fullName>OB_Marketing_Email_Alert</fullName>
        <description>OB Marketing Email_Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/OB_Marketing_Email</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Financing_Hand_Off_Alert</fullName>
        <ccEmails>financing@servicetitan.com</ccEmails>
        <description>Onboarding-Financing Hand Off Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/IMP_Financing_Hand_Off_Email</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Health_Score_Change_2</fullName>
        <description>Onboarding Health Score Change To 2</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gainsight@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mdegroot@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vach@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HealthScoreUpdated</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Health_Score_Change_To_0</fullName>
        <description>Onboarding Health Score Change To 0</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gainsight@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jphillips@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vach@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HealthScoreUpdated</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Health_Score_Change_To_1</fullName>
        <description>Onboarding Health Score Change To 1</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gainsight@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jphillips@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mdegroot@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vach@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/HealthScoreUpdated</template>
    </alerts>
    <alerts>
        <fullName>Onboarding_Payments_Introduction_Alert</fullName>
        <ccEmails>paymentsetup@servicetitan.com</ccEmails>
        <description>Onboarding-Payments Introduction Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Onboarding/ST_Payments_Introduction</template>
    </alerts>
    <alerts>
        <fullName>ProjectedLiveDateChangeAlertForPerry</fullName>
        <ccEmails>Perry.Pokrandt@worldpay.us</ccEmails>
        <description>ProjectedLiveDateChangeAlertForPerry</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/CurrProjLiveDateAlert</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Onboarding_Account_Email_Alert</fullName>
        <description>Rejected Onboarding Account Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>implementation@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Automated_Reject_Notification</template>
    </alerts>
    <alerts>
        <fullName>RejectionEmailAlertProduct</fullName>
        <description>Rejection Email Alert - Product</description>
        <protected>false</protected>
        <recipients>
            <recipient>avartazarian@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hahluwalia@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/RejectionAlertProduct</template>
    </alerts>
    <alerts>
        <fullName>Rejection_Alert</fullName>
        <description>Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>chackett@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jgreen@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mdegroot@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejection_AlertC_CustPart</fullName>
        <description>Rejection Alert For Accounts with Customer Partners</description>
        <protected>false</protected>
        <recipients>
            <recipient>lgildea@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Rejection_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>ST_Payments_Unsuccessful_Contact_Alert</fullName>
        <ccEmails>payments@servicetitan.com, sysadmin@servicetitan.com</ccEmails>
        <description>ST Payments_Unsuccessful Contact Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Onboarding</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/ST_Payments_UnsuccessfulContact</template>
    </alerts>
    <alerts>
        <fullName>Tenant_Name_ID_Changed</fullName>
        <ccEmails>billing@servicetitan.com</ccEmails>
        <description>Tenant Name/ID Changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>ahuynh@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>apaz@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>clee@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gsong@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>hmantes@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mbale@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>oalomar@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Salesforce_Alerts/Tenant_Change</template>
    </alerts>
    <alerts>
        <fullName>WorldPay_Alert_for_Perry</fullName>
        <ccEmails>Perry.Pokrandt@worldpay.us</ccEmails>
        <description>WorldPay Alert for Perry</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/WorldPay_Alert</template>
    </alerts>
    <alerts>
        <fullName>WorldPay_Alert_for_Perry_wSuccessRep</fullName>
        <ccEmails>Perry.Pokrandt@worldpay.us</ccEmails>
        <description>WorldPay Alert for Perry w/Success Rep Name</description>
        <protected>false</protected>
        <recipients>
            <recipient>kchang@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thenry@servicetitan.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply_salesforce@servicetitan.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Salesforce_Alerts/WorldPay_Alert_W_SuccessRep</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Account_Owner_to_Sabrina</fullName>
        <description>Assign to Sabrina (or placeholder)</description>
        <field>OwnerId</field>
        <lookupValue>szaki@servicetitan.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign Account Owner to Sabrina</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Customer Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Customer_Health</fullName>
        <description>Update Customer Health to 4</description>
        <field>Health_Score__c</field>
        <literalValue>4</literalValue>
        <name>FU Customer Health</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_SQL_Date</fullName>
        <field>SQL_Date__c</field>
        <formula>TODAY()</formula>
        <name>FU SQL Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Partner_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Partner</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Partner Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SDR_Accepted_Stage_Date_Update</fullName>
        <field>SDR_Accepted_Date__c</field>
        <formula>Today()</formula>
        <name>SDR Accepted Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SDR_Disengaged_Stage_Date_Update</fullName>
        <field>SDR_Disengaged_Date__c</field>
        <formula>Today()</formula>
        <name>SDR Disengaged Stage Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Blue_Frog_Undiscounted_MRR</fullName>
        <description>Update Blue Frog Undiscounted MRR field with value from Blue Frog MRR field</description>
        <field>Current_MRR__c</field>
        <formula>BlueFrog_MRR__c</formula>
        <name>Update Blue Frog Undiscounted MRR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_Status_to_RFA</fullName>
        <description>Push back into RFA pool</description>
        <field>Customer_Status_picklist__c</field>
        <literalValue>Ready for Assignment</literalValue>
        <name>Update Customer Status to RFA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SDR_Owner_to_Blank</fullName>
        <description>Reset field to blank</description>
        <field>SDR_MDR_Owner__c</field>
        <name>Update SDR Owner to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Success_Call_Transition_Date</fullName>
        <field>First_Success_Review_Date__c</field>
        <formula>Today()</formula>
        <name>Update Success Call Transition Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Name Change</fullName>
        <actions>
            <name>Account_Name_Changed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify Admin when and Account Name is change via email alert</description>
        <formula>ISCHANGED( Name )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account SQL Date Stamp</fullName>
        <actions>
            <name>FU_SQL_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SQL</value>
        </criteriaItems>
        <description>Stamps SQL Date field when customer status changes to &quot;SQL&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Alert%3A Onboarder Assigned</fullName>
        <actions>
            <name>Alert_Onboarder_has_been_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert Onboarder when they are assigned to an Account</description>
        <formula>AND ( (NOT( ISBLANK( Onboarder__c ) )) 
,ISCHANGED( Onboarder__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Churn Alert</fullName>
        <actions>
            <name>Churn_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Churn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>TEST ACCOUNT - DON&apos;T DELETE!</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Churn Alert For Accounts w%2FCustomer-Partners</fullName>
        <actions>
            <name>Churn_Alert_CustPart</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Churn</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.of_Partners__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>TEST ACCOUNT - DON&apos;T DELETE!</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Current Health Score is 0</fullName>
        <actions>
            <name>Health_Score_is_0</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Health_Score__c</field>
            <operation>equals</operation>
            <value>0 - Wants to cancel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <description>When Current Health Score is 0 and Account is in Success - send email notification to CX PM &amp; Success Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Current Health Score is 1</fullName>
        <actions>
            <name>Current_Health_Score_changes_to_1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Health_Score__c</field>
            <operation>equals</operation>
            <value>1 - Code Red</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <description>When Current Health Score is 1 and Account is in Success - send email notification to CX PM &amp; Success Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Current Health Score is 2</fullName>
        <actions>
            <name>Current_Health_Score_changes_to_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Health_Score__c</field>
            <operation>equals</operation>
            <value>2 - Code Orange</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <description>When Current Health Score is 2 and Account is in Success - send email notification to CX PM &amp; Success Manager</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer Account</fullName>
        <actions>
            <name>Customer_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Lead_Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Customer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Live%2FSuccess to SDR Accepted</fullName>
        <actions>
            <name>Live_Success_Onboarding_CustStatus_changed_Incorectly</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>To prompt warning if Account Status changes from Live/Success/Onboarding to any pre-sale status</description>
        <formula>AND( OR(  TEXT(PRIORVALUE( Customer_Status_picklist__c )) = &quot;Live&quot;,  TEXT(PRIORVALUE( Customer_Status_picklist__c )) = &quot;Onboarding&quot;,  TEXT(PRIORVALUE( Customer_Status_picklist__c )) = &quot;Success&quot; ), OR(  ISPICKVAL(Customer_Status_picklist__c, &quot;Ready for Assignment&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Source and Enrich&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Marketing Disengaged&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Telephone Enrichment&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Sales Ops Vetting&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Account Preparation&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Sales Ops Disengaged&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;SDR Disengaged&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;SDR Assigned&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;SDR Accepted&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;SE Vetting&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Discovery Call&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Q&amp;A / DEMO&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Pricing / ROI Assessment&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Negotiation / Pending&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Closed Lost&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Dead - No Oppty&quot;), ISPICKVAL(Customer_Status_picklist__c, &quot;Stage Zero&quot;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Months to Reconnect%3A 2-6</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SDR Disengaged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.DispositionCode__c</field>
            <operation>equals</operation>
            <value>Perceived urgency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SubDisposition__c</field>
            <operation>equals</operation>
            <value>Months to reconnect: 2,Months to reconnect: 3,Months to reconnect: 4,Months to reconnect: 5,Months to reconnect: 6</value>
        </criteriaItems>
        <description>Connect to accounts after 3 months</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Assign_Account_Owner_to_Sabrina</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Customer_Status_to_RFA</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_SDR_Owner_to_Blank</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Months to Reconnect%3A 7-12</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SDR Disengaged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.DispositionCode__c</field>
            <operation>equals</operation>
            <value>Perceived urgency</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SubDisposition__c</field>
            <operation>equals</operation>
            <value>Months to reconnect: 7,Months to reconnect: 8,Months to reconnect: 9,Months to reconnect: 10,Months to reconnect: 11,Months to reconnect: 12</value>
        </criteriaItems>
        <description>Connect to accounts after 6 months</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Assign_Account_Owner_to_Sabrina</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Customer_Status_to_RFA</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_SDR_Owner_to_Blank</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Partner Account</fullName>
        <actions>
            <name>Partner_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Partner</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Lead_Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Perceived Need to RFA</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SDR Disengaged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.DispositionCode__c</field>
            <operation>equals</operation>
            <value>Perceived need</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SubDisposition__c</field>
            <operation>equals</operation>
            <value>&quot;Doesn&apos;t see the value, pre-pitch&quot;,&quot;Doesn&apos;t see the value, post-pitch&quot;,Happy with existing software,Hard pass</value>
        </criteriaItems>
        <description>Connect to accounts after 4 months</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Assign_Account_Owner_to_Sabrina</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Customer_Status_to_RFA</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_SDR_Owner_to_Blank</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Rejection Alert</fullName>
        <actions>
            <name>Rejection_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>TEST ACCOUNT - DON&apos;T DELETE!</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejection Alert For Accounts w%2FCustomer-Partners</fullName>
        <actions>
            <name>Rejection_AlertC_CustPart</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.of_Partners__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>TEST ACCOUNT - DON&apos;T DELETE!</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SDR Accepted Stage Date Update</fullName>
        <actions>
            <name>SDR_Accepted_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SDR Accepted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SDR Disengaged Stage Date Update</fullName>
        <actions>
            <name>SDR_Disengaged_Stage_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SDR Disengaged</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Actual Live Date</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Live</value>
        </criteriaItems>
        <description>When account customer status = live then set the actual live date to today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Current Health Score 4</fullName>
        <actions>
            <name>FU_Customer_Health</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When there is a new customer and account customer status is set to onboarding, the health score should be 4.</description>
        <formula>ISPICKVAL(Customer_Status_picklist__c , &apos;Onboarding&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tenant Name%2FID Change</fullName>
        <actions>
            <name>Tenant_Name_ID_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR(
AND(
 ISCHANGED(Tenant_Name__c ),
NOT(PRIORVALUE( Tenant_Name__c )=&quot;&quot;)
),
AND(
 ISCHANGED( Tenant_ID__c  ),
NOT(PRIORVALUE(  Tenant_ID__c  )=&quot;&quot;)
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Unable to Connect to RFA</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>SDR Disengaged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SubDisposition__c</field>
            <operation>equals</operation>
            <value>Unable to establish initial contact with the company,Unable to get past gatekeeper,Unable to reconnect with DM</value>
        </criteriaItems>
        <description>Automatically reinject back into RFA after 45 in the following sub-dispositions (Unable to establish initial contact, unable to reconnect, unable to get past gatekeeper)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Assign_Account_Owner_to_Sabrina</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Customer_Status_to_RFA</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_SDR_Owner_to_Blank</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Blue Frog Undiscounted MRR</fullName>
        <actions>
            <name>Update_Blue_Frog_Undiscounted_MRR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Blue Frog Undiscounted MRR field with value from Blue Frog MRR field</description>
        <formula>AND( 
(Name = &quot;bluefrog Plumbing + Drain&quot;), 
ISCHANGED( BlueFrog_MRR__c ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Success Call Transition Date</fullName>
        <actions>
            <name>Update_Success_Call_Transition_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Customer_Status_picklist__c</field>
            <operation>equals</operation>
            <value>Success</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.First_Success_Review_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update the Success Call Transition date when Customer Status is changed to &quot;Success&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
