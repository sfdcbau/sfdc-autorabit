trigger ST_AccountTrigger on Account ( before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    // Before Update Event
    if(Trigger.isBefore && Trigger.isUpdate){
        // SE-1892 --> Update Customer Account (field: Payments_Live_Date__c) first time when ST_Payments_Customer_Status__c is 'Live (Credit Cards)' OR 'Live (Credit Cards & Checks)' OR 'Live (Checks)'.
        AccUpdateOppContactTriggerHandler.updateAccPayLiveDate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }
    
    if(Trigger.isAfter){
        if(System.IsBatch() == false && System.isFuture() == false){ 
                AccUpdateOppContactTriggerHandler.handleOppContactUpdate(trigger.new,trigger.old );
        }
        
        if(Trigger.isUpdate){
            AccountHistoryTrackerHandler.handleCreateAccHistory(Trigger.old,Trigger.new);
        }
    }
    if(Trigger.isBefore){
        
        if(Trigger.isUpdate){
            if(ST_SendMailToUsers.isFirstRun()){
                list<Account> accList = new list<Account>();
                for(Account acc : trigger.new) {
                    if(acc.Health_Score__c != trigger.oldMap.get(acc.Id).Health_Score__c) {
                        accList.add(acc);
                    }
                }
                ST_SendMailToUsers.ST_SendMail(accList);
                
            }
        }
    }
}