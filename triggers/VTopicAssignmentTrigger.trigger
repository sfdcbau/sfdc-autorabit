trigger VTopicAssignmentTrigger on TopicAssignment (after insert, before delete) 
{
	new VTopicAssignmentTriggerDispatcher().dispatch();
}