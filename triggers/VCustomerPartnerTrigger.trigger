trigger VCustomerPartnerTrigger on CustomerPartner__c (after insert, after update, after undelete, before insert, before update) 
{
	new VCustomerPartnerTriggerDispatcher().dispatch();
}