trigger VCaseRatingTrigger on Case_Rating__c (after insert, after update) 
{
	new VCaseRatingTriggerDispatcher().dispatch();
}