/*
    Keep Customer Partner Association Weight in-sync with Partner Account
    
    Notes:
        - Assocation Weight in Account and CustomerPartner are different types.
            In Account, its a Picklist.
            In CustomerPartner, its a Decimal.
*/
trigger AccountUpdateCustomerPartnerAssociationWeight on Account (after insert, after update) 
{
    
   /* List<CustomerPartner__c> customerpartnerToUpdateList = new List<CustomerPartner__c>();
    Map<Id, Account> accountMap = new Map<Id, Account>();
    
    // Collect accounts with updated association weight
    for(Account acc : trigger.new)
    {
        Boolean hasAssociationWeightChange = false;
        
        if(trigger.isInsert)
        {
            hasAssociationWeightChange = true;
        }
        else{
            if(acc.Association_Weight__c != trigger.oldMap.get(acc.Id).Association_Weight__c)
            {
                hasAssociationWeightChange = true;
            }
        }
        
        if(hasAssociationWeightChange)
        {
            accountMap.put(acc.Id, acc);
        }
    }
    
    // Collect customer partners to update
   
    for(CustomerPartner__c cp : [Select Id, Partner__c, Association_Weight__c From CustomerPartner__c Where Partner__c In :accountMap.keySet()])
    {
        if(accountMap.containsKey(cp.Partner__c))
        {
            Account acc = accountMap.get(cp.Partner__c);
            
            cp.Association_Weight__c = Decimal.valueOf(acc.Association_Weight__c);
            customerpartnerToUpdateList.add(cp);
        }
    }
    
    // Update customer partners
    if(!customerpartnerToUpdateList.isEmpty())
    {
        update customerpartnerToUpdateList;
    }
    */
}