/*
    Trigger to add Account Contacts when Opportunity Primary Campaign is set.
    
    Notes:
        - Add ALL contacts in Opportunity Account to the campaign.
        - [20160316 SC]: Collect Opportunity Id which Primary Campaign has changed into static variable. This varialbe will be used to avoid Recursive update.
*/
trigger VOpportunityAddAccountContactToCompaign on Opportunity (after insert, after update) 
{
    
    List<CampaignMember> insertCampaignMemberList = new List<CampaignMember>();
    Map<Id, Set<Id>> campaignAccountSetMap = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> existingCampaignContactSetMap = new Map<Id, Set<Id>>();
    Map<Id, List<Contact>> accountContactListMap = new Map<Id, List<Contact>>();
    Set<Id> accountSet = new Set<Id>();
    
    // Collect campaigns and accounts
    for(Opportunity opp : Trigger.new)
    {
        if(!VCampaignUtil.VOpportunityAddAccountContactToCompaign_OpportunityIdSet.contains(opp.Id))
        {
            System.debug('######## VOpportunityAddAccountContactToCompaign. Add Opportunity Id: ' + opp.Id);
            VCampaignUtil.VOpportunityAddAccountContactToCompaign_OpportunityIdSet.add(opp.Id);
        }
                
        if(opp.CampaignId != null)
        {
            Boolean isPrimaryCampaignChanged = false;
        
            if(Trigger.isInsert)
            {
                isPrimaryCampaignChanged = true;
            }
            else
            {
                if(opp.CampaignId != Trigger.oldMap.get(opp.Id).CampaignId)
                {
                    isPrimaryCampaignChanged = true;
                }
            }
            
            if(isPrimaryCampaignChanged)
            {
                if(!campaignAccountSetMap.containsKey(opp.CampaignId))
                {
                    campaignAccountSetMap.put(opp.CampaignId, new Set<Id>());
                }
                
                if(!campaignAccountSetMap.get(opp.CampaignId).contains(opp.AccountId))
                {
                    campaignAccountSetMap.get(opp.CampaignId).add(opp.AccountId);
                }
                
                if(!accountSet.contains(opp.AccountId))
                {
                    accountSet.add(opp.AccountId);
                }
            }
        }
    }
    
    if(!accountSet.isEmpty())
    {
        // Collect contacts
        for(Contact con : [Select Id, AccountId From Contact Where AccountId In :accountSet])
        {
            if(!accountContactListMap.containsKey(con.AccountId))
            {
                accountContactListMap.put(con.AccountId, new List<Contact>());
            }
            
            accountContactListMap.get(con.AccountId).add(con);
        }
        
        // Collect existing campaign members
        for(CampaignMember cm : [Select CampaignId, ContactId From CampaignMember Where CampaignId In :campaignAccountSetMap.keySet()])
        {
            if(!existingCampaignContactSetMap.containsKey(cm.CampaignId))
            {
                existingCampaignContactSetMap.put(cm.CampaignId, new Set<Id>());
            }
            
            if(!existingCampaignContactSetMap.get(cm.CampaignId).contains(cm.ContactId))
            {
                existingCampaignContactSetMap.get(cm.CampaignId).add(cm.ContactId);
            }
        }
        
        // Collect campaign members to insert
        for(Id campaignId : campaignAccountSetMap.keySet())
        {
            for(Id accountId : campaignAccountSetMap.get(campaignId))
            {
                if(accountContactListMap.containsKey(accountId))
                {
                    for(Contact con : accountContactListMap.get(accountId))
                    {
                        Boolean existingmember = false;
                        
                        if(existingCampaignContactSetMap.containsKey(campaignId))
                        {
                            if(existingCampaignContactSetMap.get(campaignId).contains(con.Id))
                            {
                                existingmember = true;
                            }
                        }
                        
                        if(!existingmember)
                        {
                            CampaignMember newcm = new CampaignMember();
                            newcm.CampaignId = campaignId;
                            newcm.ContactId = con.Id;
                            insertCampaignMemberList.add(newcm);
                        }
                    }
                }
            }
        }
        
        // Insert campaign members
        if(!insertCampaignMemberList.isEmpty())
        {
            System.debug(insertCampaignMemberList);
            insert insertCampaignMemberList;
        }
    }
    
}