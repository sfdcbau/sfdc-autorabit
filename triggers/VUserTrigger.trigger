trigger VUserTrigger on User (after insert, after update) 
{
	new VUserTriggerDispatcher().dispatch();
}