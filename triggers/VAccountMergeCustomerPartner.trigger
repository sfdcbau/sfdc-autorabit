/*
	Trigger to evaluate customer-partner merge during Account merge.
	
	Notes:
		- Collect customer-partner records on deleted accounts into static variable. 
			On After event, use these records to compare and find duplicates to discard.
*/
trigger VAccountMergeCustomerPartner on Account (before delete, after delete) 
{
	
	if(Trigger.isBefore)
	{
		// Before the losing record customer-partners get deleted during the merge, save them into static variable.
		// We will use this static variable list to compare and find any duplicates to discard.
		for(CustomerPartner__c cp : [Select Id From CustomerPartner__c Where Customer__c In :Trigger.old])
		{
			VCustomerPartnerUtil.losingRecordCustomerPartnerSet.add(cp.Id);
		}
	}
	else
	{
		List<Id> deleteCustomerPartnerIDList = new List<Id>();
		Map<Id, Set<Id>> customerPartnerSetMap = new Map<Id, Set<Id>>(); // Map<CustomerId, Set<PartnerId>>
		Set<Id> masterRecordSet = new Set<Id>();
		
		// Collect losing records
		for(Account acc : Trigger.old)
		{
			if(acc.MasterRecordId != null)
			{
				masterRecordSet.add(acc.MasterRecordId);
			}
		}
		
		if(!masterRecordSet.isEmpty())
		{
			// Collect winnning record customer partners. EXCLUDE the losing record customer partners 
			for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c From CustomerPartner__c Where Customer__c In :masterRecordSet And Id Not In :VCustomerPartnerUtil.losingRecordCustomerPartnerSet])
			{
				if(cp.Partner__c != null)
				{
					if(!customerPartnerSetMap.containsKey(cp.Customer__c))
					{
						customerPartnerSetMap.put(cp.Customer__c, new Set<Id>());
					}
					
					if(!customerPartnerSetMap.get(cp.Customer__c).contains(cp.Partner__c))
					{
						customerPartnerSetMap.get(cp.Customer__c).add(cp.Partner__c);
					}
				}
			}
			
			// Collect losing customer partners to delete
			for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c From CustomerPartner__c Where Id In :VCustomerPartnerUtil.losingRecordCustomerPartnerSet])
			{
				if(customerPartnerSetMap.containsKey(cp.Customer__c))
				{
					if(customerPartnerSetMap.get(cp.Customer__c).contains(cp.Partner__c))
					{
						deleteCustomerPartnerIDList.add(cp.Id);
					}
				}
			}
		}
		
		System.debug('################ TEST: ' + masterRecordSet);
		System.debug(VCustomerPartnerUtil.losingRecordCustomerPartnerSet);
		System.debug(customerPartnerSetMap);
		System.debug(deleteCustomerPartnerIDList);
			
		// delete customer partners
		if(!deleteCustomerPartnerIDList.isEmpty())
		{
			VCustomerPartnerUtil.deleteCustomerPartners(deleteCustomerPartnerIDList);
		}
	}
	
}