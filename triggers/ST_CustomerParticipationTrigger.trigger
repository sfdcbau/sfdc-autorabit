trigger ST_CustomerParticipationTrigger on Customer_Participation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    //to check if CRP is already mapped to request....
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            CustomerParticipationTriggerhandler.handleInsertCustomerParticipation(Trigger.New);
        }
    }
    
    //for updating Counts on the CRP record based on Customer Participation Status......
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            CustomerParticipationTriggerhandler.handleUpdateCRP(Trigger.New);
        }
    }
}