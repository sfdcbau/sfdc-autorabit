/*
    Trigger to add Contact Account Opportunities to Campaign.
    
    Notes:
        - Add Open and Closed Lost opportunties.
        - [20160316 SC]: Fix Recursive SELF_REFERENCE_FROM_TRIGGER issue when adding new contacts from import file. 
*/
trigger VCampaignMemberAddAccountOpportunityToCampaign on CampaignMember (after insert) 
{    
       
    List<Opportunity> updateOpportunityList = new List<Opportunity>();
    Map<Id, Id> accountCampaignMap = new Map<Id, Id>();
    
    // Collect campaigns and contacts
    for(CampaignMember cm : [Select Id, CampaignId, ContactId, Contact.AccountId From CampaignMember Where Id In :Trigger.new])
    {
        if(cm.ContactId != null)
        {
            if(!accountCampaignMap.containsKey(cm.Contact.AccountId))
            {
                accountCampaignMap.put(cm.Contact.AccountId, cm.CampaignId);
            }
        }
    }
    
    if(!accountCampaignMap.isEmpty())
    {
        for(Id s : VCampaignUtil.VOpportunityAddAccountContactToCompaign_OpportunityIdSet)
        {
            System.debug('#### VOpportunityAddAccountContactToCompaign_OpportunityIdSet ID: ' + s);
        }
        
        // Collect opportunities to update
        for(Opportunity opp : [Select Id, AccountId, CampaignId From Opportunity 
            Where AccountId In :accountCampaignMap.keySet() And (IsClosed = false Or (IsClosed = true And IsWon = false))
                And Id Not In :VCampaignUtil.VOpportunityAddAccountContactToCompaign_OpportunityIdSet])
        {
            if(accountCampaignMap.containsKey(opp.AccountId))
            {
                Id campaignId = accountCampaignMap.get(opp.AccountId);
                
                if(campaignId != opp.CampaignId)
                {
                    if(!VCampaignUtil.VOpportunityAddAccountContactToCompaign_OpportunityIdSet.contains(opp.Id))
                    {
                        System.debug('######## VCampaignMemberAddAccountOpportunityToCampaign. Add Opportuntiy Id: ' + opp.Id);
                        VCampaignUtil.VOpportunityAddAccountContactToCompaign_OpportunityIdSet.add(opp.Id);
                    }
                    
                    System.debug('#### Opportunity to update: ' + opp.Id);
                    System.debug('#### Opportunity Old Campaign: ' + opp.CampaignId);
                    opp.CampaignId = accountCampaignMap.get(opp.AccountId);
                    System.debug('#### Opportunity New Campaign: ' + opp.CampaignId);
                    updateOpportunityList.add(opp);
                }
            }
        }
        
        // Update opportunities
        if(!updateOpportunityList.isEmpty())
        {
            System.debug(updateOpportunityList);
            update updateOpportunityList;
        }
    }
    
}