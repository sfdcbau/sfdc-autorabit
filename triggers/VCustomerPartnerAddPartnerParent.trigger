/*
	Trigger to add Partner Parent when Customer-Partner is added.
	
	Notes:
		- Add Partner account Parent. This will create another trigger to add the next parent in the hierarchy.
		- Avoid duplicate records.
	
	Updates:
		- [20161027 SC]: This trigger context has been moved to VCustomerPartnerTrigger
*/
trigger VCustomerPartnerAddPartnerParent on CustomerPartner__c (after insert) 
{
	/*
	List<CustomerPartner__c> insertCustomerPartnerList = new List<CustomerPartner__c>();
	Map<Id, Map<Id, Id>> customerPartnerSetMap = new Map<Id, Map<Id, Id>>(); // Map<CustomerId, Map<PartnerId, Current CustomerPartner__c>>
	Map<Id, Map<Id, Id>> leadPartnerSetMap = new Map<Id, Map<Id, Id>>(); // Map<LeadId, Map<PartnerId, Current CustomerPartner__c>>
	
	// Collect parent, parent.parent accounts
	for(CustomerPartner__c cp : [Select Id, Customer__c, Lead__c, Partner__c, Partner__r.ParentId, Partner__r.Parent.ParentId, Status__c From CustomerPartner__c Where Id In :Trigger.new])
	{
		// Collect lead partners
		if(cp.Lead__c != null) 
		{
			addLeadPartnerSetMap(cp.Lead__c, cp.Partner__r.ParentId, cp.Id);
		}
		
		// Collect customer partners
		if(cp.Customer__c != null) 
		{
			String leadcustomerAccountId = Static_ID__c.getInstance('Account_Lead_Customer').RecordID__c;
			if(cp.Customer__c != leadcustomerAccountId && String.valueOf(cp.Customer__c).substring(0,15) != leadcustomerAccountId)
			{
				addCustomerPartnerSetMap(cp.Customer__c, cp.Partner__r.ParentId, cp.Id);
			}
		}
	}
	
	// Validate customer partners
	if(!customerPartnerSetMap.isEmpty())
	{
		// Remove existing customer partners from the set
		for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c From CustomerPartner__c Where Customer__c In :customerPartnerSetMap.keySet()])
		{
			if(customerPartnerSetMap.get(cp.Customer__c).containsKey(cp.Partner__c))
			{
				customerPartnerSetMap.get(cp.Customer__c).remove(cp.Partner__c);
			}
		}
		
		// Collect customer partners to insert
		for(Id customerId : customerPartnerSetMap.keySet())
		{
			for(Id partnerId : customerPartnerSetMap.get(customerId).keySet())
			{
				CustomerPartner__c currentcp = Trigger.newMap.get(customerPartnerSetMap.get(customerId).get(partnerId));
				
				CustomerPartner__c newcp = new CustomerPartner__c();
				newcp.Customer__c = customerId;
				newcp.Partner__c = partnerId;
				newcp.Status__c = currentcp.Status__c;
				newcp.Inbound_Referral__c = 'No or Unknown';
				insertCustomerPartnerList.add(newcp);
			}
		}
	}
	
	// Validate lead partners
	if(!leadPartnerSetMap.isEmpty())
	{		
		// Remove existing lead partners from the set
		for(CustomerPartner__c cp : [Select Id, Lead__c, Partner__c From CustomerPartner__c Where Lead__c In :leadPartnerSetMap.keySet()])
		{
			if(leadPartnerSetMap.get(cp.Lead__c).containsKey(cp.Partner__c))
			{
				leadPartnerSetMap.get(cp.Lead__c).remove(cp.Partner__c);
			}
		}
		
		// Collect customer partners to insert
		for(Id leadId : leadPartnerSetMap.keySet())
		{
			for(Id partnerId : leadPartnerSetMap.get(leadId).keySet())
			{
				CustomerPartner__c currentcp = Trigger.newMap.get(leadPartnerSetMap.get(leadId).get(partnerId));
				
				CustomerPartner__c newcp = new CustomerPartner__c();
				newcp.Lead__c = leadId;
				newcp.Customer__c = currentcp.Customer__c;
				newcp.Partner__c = partnerId;
				newcp.Status__c = currentcp.Status__c;
				newcp.Inbound_Referral__c = 'No or Unknown';
				insertCustomerPartnerList.add(newcp);
			}
		}
	}
	
	// Insert customer partners
	if(!insertCustomerPartnerList.isEmpty())
	{
		insert insertCustomerPartnerList;
	}
	
	
	//
	//	Function to add partner Id to the customerPartnerSetMap
	//
	private void addCustomerPartnerSetMap(Id customerId, Id accountId, Id currentcpId)
	{
		if(accountId != null)
		{
			if(!customerPartnerSetMap.containsKey(customerId))
			{
				customerPartnerSetMap.put(customerId, new Map<Id, Id>());
			}
			
			if(!customerPartnerSetMap.get(customerId).containsKey(accountId))
			{
				customerPartnerSetMap.get(customerId).put(accountId, currentcpId);
			}
		}
	}
	
	//
	//	Function to add partner Id to the leadPartnerSetMap
	//
	private void addLeadPartnerSetMap(Id leadId, Id accountId, Id currentcpId)
	{
		if(accountId != null)
		{
			if(!leadPartnerSetMap.containsKey(leadId))
			{
				leadPartnerSetMap.put(leadId, new Map<Id, Id>());
			}
			
			if(!leadPartnerSetMap.get(leadId).containsKey(accountId))
			{
				leadPartnerSetMap.get(leadId).put(accountId, currentcpId);
			}
		}
	}
	*/
}