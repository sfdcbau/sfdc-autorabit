trigger ST_QueueManager_Trigger on QueueManager__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if( Trigger.isUpdate) {
        if (Trigger.isAfter){
            ST_QueueManagerTriggerHandler.handleUpdateOppsOwner( Trigger.new);
        }
    
    } 
    
}