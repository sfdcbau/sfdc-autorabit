Trigger ST_OpportunityTrigger on Opportunity ( before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    Trigger_Settings__mdt TriggerFlag = [SELECT 
                                         Label, isActive__c 
                                         FROM Trigger_Settings__mdt 
                                         where Label = 'ST_OpportunityTrigger_Trigger' limit 1];
    if(TriggerFlag.isActive__c) {
        
        // Before Insert Event
        if(Trigger.isBefore && Trigger.isInsert){
            ST_OpportunityTriggerEventHandler.beforeInsert(Trigger.new);
        }
        // After Insert Event
        if(Trigger.isAfter && Trigger.isInsert){
            ST_OpportunityTriggerEventHandler.afterInsert(Trigger.new, Trigger.oldMap);
        }
        // Before update Event
        if(Trigger.isBefore && Trigger.isUpdate){
            ST_OpportunityTriggerEventHandler.beforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        // After Update Event
        if(Trigger.isAfter && Trigger.isUpdate){
            ST_OpportunityTriggerEventHandler.afterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        // Before Delete Event
        if(Trigger.isBefore && Trigger.isDelete){
            ST_OpportunityTriggerEventHandler.beforeDelete(Trigger.old, Trigger.oldMap);
        }
        // After Delete Event
        if(Trigger.isAfter && Trigger.isDelete){
            ST_OpportunityTriggerEventHandler.afterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}