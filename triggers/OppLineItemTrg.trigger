Trigger OppLineItemTrg on OpportunityLineItem (After insert, After delete, before insert,After update ) {
    
    Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
    if(Trigger.isbefore){
        OppLineItemTrgHandler.errorHandlerMethodforDuplicateProducts(trigger.new);  
        OppLineItemTrgHandler.errorHandlerMethodforCoreProducts(trigger.new);
    }
    If(trigger.isAfter ) {
        if(trigger.isInsert) {
            OppLineItemTrgHandler.updateCoreProductonAccount(Trigger.new); 
            //ST_FinanceOppLineItemTrgHandler.oppClosedWonProductAdded(Trigger.new,Trigger.oldMap); 
        }
        if(trigger.isupdate) {
            OppLineItemTrgHandler.updateCoreProductonAccount(Trigger.new);      
        }
        
        if(Trigger.isDelete) {
            OppLineItemTrgHandler.updateCoreProductonAccount(Trigger.old);
        }
    }
}