/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 14 Feb, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 14 Feb, 2019
 # Description...................: This is trigger for MRR_Tracking__c object..
 # Test Class...................: ST_MRRTrackingTriggerTest
 #####################################################
*/
Trigger ST_MRRTrackingTrigger on MRR_Tracking__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    Trigger_Settings__mdt TriggerFlag = [SELECT Label, isActive__c FROM Trigger_Settings__mdt where Label = 'ST_MRRTrackingTrigger_Trigger' limit 1];
    
    if(TriggerFlag != null && TriggerFlag.isActive__c) {   // Trigger Switch orgwide
        
        ST_MRRTrackingEventHandler oppHandler = new ST_MRRTrackingEventHandler();
        
        // Before Insert
        if(Trigger.isBefore && Trigger.isInsert){
            oppHandler.beforeInsert(trigger.New);
        }
        
        // After Insert
        if(Trigger.isAfter && Trigger.isInsert){
            oppHandler.afterInsert(trigger.New,trigger.newMap,trigger.oldMap);
        }
        
        // Before Update
        if(Trigger.isBefore && Trigger.isUpdate){
            oppHandler.beforeUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
        }
        
        // After Update
        if(Trigger.isAfter && Trigger.isUpdate){
            oppHandler.afterUpdate(trigger.New,trigger.old,trigger.newMap,trigger.oldMap);
        }
        
        // Before Delete
        if(Trigger.isBefore && Trigger.isDelete){
            oppHandler.beforeDelete(trigger.old,trigger.oldMap);
        }
        
        // After Delete
        if(Trigger.isAfter && Trigger.isDelete){
            oppHandler.afterDelete(trigger.old,trigger.oldMap);
        }
    }
}