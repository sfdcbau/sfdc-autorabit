/*
	On Lead Conversion, convert Lead Customer Partner to be Account Customer Partner
	
	Notes:
		- [20160121 SC]: Remove duplicate customer-partner records.
		- [20160128 SC]: Keep the recent updated record when removing the duplicates.
		- [20160203 SC]: Assign converted account to customer-partner record if no duplicate or newer.
						 To avoid duplicate validation, we will need to delete the duplicates and then update the customer association.
		- [20161026 SC]: This trigger context has been moved to VLeadTrigger
*/
trigger LeadConvertCustomerPartner on Lead (after update) 
{
	/*
	// Make sure that this trigger will only be evaluated one time per execution
    if(!VCustomerPartnerUtil.canTriggerLeadConvertCustomerPartner)
    {
        return;
    }
    VCustomerPartnerUtil.canTriggerLeadConvertCustomerPartner = false;
    
    
	List<CustomerPartner__c> updateCustomerPartnerList = new List<CustomerPartner__c>();
	List<CustomerPartner__c> deleteCustomerPartnerList = new List<CustomerPartner__c>();
	List<RecordType> rtList = [Select Id, Name From RecordType Where SObjectType = 'CustomerPartner__c' And Name='Customer' Limit 1];
	Map<Id, Lead> convertedLeadMap = new Map<Id, Lead>();
	Map<Id, Map<Id, CustomerPartner__c>> customerPartnerSetMap = new Map<Id, Map<Id, CustomerPartner__c>>(); // Map<CustomerId, Map<PartnerId, CustomerPartner__c>>
	Set<Id> convertedAccountSet = new Set<Id>();
	
	// Converted leads
	for(Lead l : Trigger.New)
	{
		if(l.IsConverted && !Trigger.oldMap.get(l.Id).IsConverted)
		{	
			convertedLeadMap.put(l.Id, l);
			convertedAccountSet.add(l.ConvertedAccountId);
		}
	}
	
	// Evaluate converted leads
	if(!convertedLeadMap.isEmpty())
	{
		// Collect existing customer partners
		for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c, LastModifiedDate From CustomerPartner__c Where Customer__c In :convertedAccountSet])
		{
			if(!customerPartnerSetMap.containsKey(cp.Customer__c))
			{
				customerPartnerSetMap.put(cp.Customer__c, new Map<Id, CustomerPartner__c>());
			}
			
			if(!customerPartnerSetMap.get(cp.Customer__c).containsKey(cp.Partner__c))
			{
				customerPartnerSetMap.get(cp.Customer__c).put(cp.Partner__c, cp);
			}
		}
		
		// Collect customer-partners
		for(CustomerPartner__c cp : [Select Id, Lead__c, Customer__c, Partner__c, LastModifiedDate From CustomerPartner__c Where Lead__c In :convertedLeadMap.keySet()])
		{
			Lead l = convertedLeadMap.get(cp.Lead__c);
			if(l != null)
			{
				// [20160203 SC]: Assign converted account to customer-partner record if no duplicate or newer.
				Boolean canAssociateCustomer = true;
				
				if(customerPartnerSetMap.containsKey(l.ConvertedAccountId))
				{
					if(customerPartnerSetMap.get(l.ConvertedAccountId).containsKey(cp.Partner__c))
					{
						CustomerPartner__c cp_existing = customerPartnerSetMap.get(l.ConvertedAccountId).get(cp.Partner__c);
					
						// [20160128 SC]: Remove older records.
						if(cp_existing.LastModifiedDate < cp.LastModifiedDate)
						{
							deleteCustomerPartnerList.add(cp_existing);
						}
						else
						{
							deleteCustomerPartnerList.add(cp);
							canAssociateCustomer = false;
						}
					}
				}
				
				// Attach the customer-partners to the converted account
				if(canAssociateCustomer)
				{
					cp.Customer__c = l.ConvertedAccountId;
					if(!rtList.isEmpty())
					{
						cp.RecordTypeId = rtList[0].Id;
					}
					updateCustomerPartnerList.add(cp);
				}
			}
			
		}
	}
	
	// Delete customer-partners
	if(!deleteCustomerPartnerList.isEmpty())
	{
		delete deleteCustomerPartnerList;
	}
	
	// Update customer-partners
	if(!updateCustomerPartnerList.isEmpty())
	{
		update updateCustomerPartnerList;
	}
	*/
}