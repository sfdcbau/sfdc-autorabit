/*
	Trigger to check for existing customer-partner.
	
	Notes:
		- Need to check for combination of Lead-Customer-Partner values for Lead Customer-Partner. 
			KEY: LeadId_CustomerId_PartnerId
		- Need to check for combination of Customer-Partner values for Account Customer-Partner.
			KEY: CustomerId_PartnerId
			EXECPTION: Lead Customer record.
	
	Updates:
		- [20161027 SC]: This trigger context has been moved to VCustomerPartnerTrigger
*/
trigger VCustomerPartnerCheckDuplicate on CustomerPartner__c (before insert, before update) 
{
	/*
	String errormsg = '';
	Set<Id> leadSet = new Set<Id>();
	Set<Id> customerSet = new Set<Id>();
	Set<String> leadcustomerpartnerSet = new Set<String>();
	Set<String> customerpartnerSet = new Set<String>();
	
	
	System.debug(Trigger.new);
	
	for(CustomerPartner__c cp : Trigger.new)
	{
		Boolean check = false;
		
		if(Trigger.isInsert)
		{
			check = true;
		}
		else
		{
			if( (cp.Lead__c != Trigger.oldMap.get(cp.Id).Lead__c) || (cp.Customer__c != Trigger.oldMap.get(cp.Id).Customer__c) || (cp.Partner__c != Trigger.oldMap.get(cp.Id).Partner__c) )
			{
				check = true;
			}
		}
		
		if(check)
		{
			// Collect customer partners
			if(cp.Customer__c != null) 
			{
				if(!customerSet.contains(cp.Customer__c))
				{
					customerSet.add(cp.Customer__c);
				}
			}
			
			// Collect lead partners
			if(cp.Lead__c != null) 
			{
				if(!leadSet.contains(cp.Lead__c))
				{
					leadSet.add(cp.Lead__c);
				}
				
				// Collect key - Combination of LeadId, CustomerId, PartnerId
				String leadKey = String.valueOf(cp.Lead__c) + '_' + String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
				if(!leadcustomerpartnerSet.contains(leadKey))
				{
					leadcustomerpartnerSet.add(leadKey);
				}
			}
			
			// Collect key - Combination of CustomerId, PartnerId
			String accountKey = String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
			if(!customerpartnerSet.contains(accountKey))
			{
				customerpartnerSet.add(accountKey);
			}
		}
	}
	
	System.debug(leadcustomerpartnerSet);
	System.debug(customerpartnerSet);
	
	// Validate lead customer partners
	if(!leadcustomerpartnerSet.isEmpty())
	{
		for(CustomerPartner__c cp : [Select Id, Lead__c, Lead__r.Name, Customer__c, Customer__r.Name, Partner__c, Partner__r.Name From CustomerPartner__c Where Lead__c In :leadSet Or Customer__c In :customerSet])
		{
			String key = String.valueOf(cp.Lead__c) + '_' + String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
			
			System.debug('#################### LEAD KEY: '+key);
			
			if(leadcustomerpartnerSet.contains(key))
			{
				errormsg += 'Duplicate Customer-Partner: {Lead=' + cp.Lead__r.Name + ', Customer=' + cp.Customer__r.Name + ', Partner=' + cp.Partner__r.Name + '}\n';
			} 
		}
	}
	
	// Validate customer partners
	if(!customerpartnerSet.isEmpty())
	{
		for(CustomerPartner__c cp : [Select Id, Customer__c, Customer__r.Name, Partner__c, Partner__r.Name From CustomerPartner__c Where Customer__c In :customerSet And Customer__c <> :Static_ID__c.getInstance('Account_Lead_Customer').RecordID__c])
		{
			String key = String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
			
			System.debug('#################### ACCOUNT KEY: '+key);
			
			if(customerpartnerSet.contains(key))
			{
				errormsg += 'Duplicate Customer-Partner: {Customer=' + cp.Customer__r.Name + ', Partner=' + cp.Partner__r.Name + '}\n';
			} 
		}
	}
	
	// Attach error message to trigger record
	if(errormsg != '')
	{
		Trigger.new[0].addError(errormsg);
	}
	*/
}