/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 26 Dec, 2018
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 26 Dec, 2018
 # Description...................: This is trigger for CustomerPartner object.
 # Test Class...................: ST_CustomerPartnerTriggerTest
 #####################################################
*/
trigger ST_CustomerPartnerTrigger on CustomerPartner__c (after insert, after update, after delete) {
    ST_CustomerPartnerTriggerHandler handler = new ST_CustomerPartnerTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            handler.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        if(Trigger.isUpdate) {
            handler.onAfterUpdate(Trigger.old,Trigger.oldMap,Trigger.new, Trigger.newMap);
        }
        if(Trigger.isDelete) {
            handler.onAfterDelete(Trigger.old,Trigger.oldMap);
        }
    }
}