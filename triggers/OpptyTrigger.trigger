trigger OpptyTrigger on Opportunity (before Insert, After Update) {

if(trigger.isUpdate)
{
 if(trigger.isAfter){
  OpptyTriggerHandler.updateHandler(trigger.new);
  }
  }
  
  if(trigger.isInsert){
  if(trigger.isBefore){
if(trigger.new[0].recordtypeid == '0121a0000002PJrAAM')   {
  list<opportunity> opptyList = [SELECT Id, Managed_Tech_Quantity__c, Account.Type FROM Opportunity WHERE AccountId =: trigger.new[0].accountid AND StageName='Onboarding' AND recordtypeid='0121a000000ERIZ' order by CloseDate desc];
  
                  if(opptyList.size() >0){
  trigger.new[0].Of_Tech__c = opptyList[0].Managed_Tech_Quantity__c ;
  }
  }
  }
  }
}