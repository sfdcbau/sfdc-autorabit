/*
#####################################################################################
# Created By....................: Aman Gupta
# Created Date..................: 28 Dec, 2018
# Last Modified By..............: Kamal Singh
# Last Modified Date............: 12 APR, 2019
# Description...................: This is trigger for OpportunityTeamMember object.
# Test Class....................: ST_OpportunityTeamMemberTriggerTest
#####################################################################################    
*/   
trigger ST_OpportunityTeamMemberTrigger on OpportunityTeamMember (after insert, after update, after delete, before insert) {
    ST_OpportunityTeamMemberTriggerHandler handler = new ST_OpportunityTeamMemberTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    //Added by Kamal 
    //Validation on adding Sales Engineer opportunity team member to opportuinity 
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {    
            handler.onBeforeInsert(Trigger.new, Trigger.newMap); 
        }
    }
    
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            handler.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        if(Trigger.isUpdate) {
            handler.onAfterUpdate(Trigger.old,Trigger.oldMap,Trigger.new, Trigger.newMap);
        }  
        if(Trigger.isDelete) {
            handler.onAfterDelete(Trigger.old,Trigger.oldMap);
        }
    }     
}