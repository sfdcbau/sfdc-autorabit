trigger TalkdeskGainsightTrigger on talkdesk__Talkdesk_Activity__c (after insert) {
    for(talkdesk__Talkdesk_Activity__c a:trigger.new){
        TalkdeskGainsightLogActivity.gsCreateActivity(a.id);    
    }
}