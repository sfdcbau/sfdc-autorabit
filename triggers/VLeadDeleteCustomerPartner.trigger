/*
	Trigger to delete "Lead Customer" customer-partner record.
	
	Notes:
		- Do not delete customer-partners on leads that are being merged.
*/
trigger VLeadDeleteCustomerPartner on Lead (before delete, after delete) 
{
	
	if(Trigger.isBefore)
	{
		// Collect the customer-partner records before the Lead is deleted into static variable list.
		// We will use this static variable list to delete the records if the deletion is not resulted from merging.
		for(CustomerPartner__c cp : [Select Id From CustomerPartner__c Where Lead__c In :Trigger.old])
		{
			VCustomerPartnerUtil.losingRecordCustomerPartnerSet.add(cp.Id);
		}
	}
	else
	{
		// After delete, the Lead__c will either have the Master Record Id if merged. Otherwise, it will be NULL.
		List<CustomerPartner__c> deleteCustomerPartnerList = [Select Id From CustomerPartner__c Where Id In :VCustomerPartnerUtil.losingRecordCustomerPartnerSet And Lead__c = null];
		
		// delete customer partners
		if(!deleteCustomerPartnerList.isEmpty())
		{
			delete deleteCustomerPartnerList;
		}
	}
	
}