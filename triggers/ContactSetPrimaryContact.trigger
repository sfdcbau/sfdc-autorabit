/*
	Set Primary Contact on updating the Contact Primary flag
	
	Notes:
		- Make sure there are only one contact in an account with Primary Contact equals TRUE
*/
trigger ContactSetPrimaryContact on Contact (after insert, after update) 
{
	List<Account> accountToUpdateList = new List<Account>();
	List<Contact> contactToUpdateList = new List<Contact>();
	Map<Id, Id> accountPrimaryContactMap = new Map<Id, Id>(); // Map of Account and Contact
	
	// Collect Account Primary Contact
	for(Contact con : trigger.new)
	{
		Boolean isPrimaryChangedToTrue = false;
		
		if(trigger.isInsert)
		{
			if(con.Primary_Contact__c)
			{
				isPrimaryChangedToTrue = true;
			}
		}
		else if(trigger.IsUpdate)
		{
			if(con.Primary_Contact__c && !trigger.oldMap.get(con.Id).Primary_Contact__c)
			{
				isPrimaryChangedToTrue = true;
			}
		}
		
		
		if(isPrimaryChangedToTrue)
		{
			if(!accountPrimaryContactMap.containsKey(con.AccountId))
			{
				accountPrimaryContactMap.put(con.AccountId, con.Id);
			}
		}
	}
	
	// Collect accounts to update
	for(Account acc : [Select Id, Primary_Contact__c From Account Where Id In :accountPrimaryContactMap.keySet()])
	{
		Id contactId = accountPrimaryContactMap.get(acc.Id);
		if(contactId != null)
		{
			acc.Primary_Contact__c = contactId;
			accountToUpdateList.add(acc);
		}
	}
	
	if(!accountToUpdateList.isEmpty())
	{
		
		// Update accounts
		update accountToUpdateList;
		
		// Collect contacts to update
		for(Contact con : [Select Id, Primary_Contact__c From Contact Where AccountId In :accountToUpdateList And Id Not In :accountPrimaryContactMap.values()])
		{
			con.Primary_Contact__c = false;
			contactToUpdateList.add(con);
		}
		
		// Update contacts
		if(!contactToUpdateList.isEmpty())
		{
			update contactToUpdateList;
		}
	}
	
}