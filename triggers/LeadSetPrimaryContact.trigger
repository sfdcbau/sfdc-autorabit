/*
	Set Primary Contact after Lead Conversion
	
	Notes:
		- Set the Converted Contact the Account Primary Contact

	Updates:
		- [20161029 SC]: Trigger context has been moved to VLeadTrigger
*/
trigger LeadSetPrimaryContact on Lead (after update) 
{
	/*
	List<Lead> convertedLeadList = new List<Lead>();
	List<Account> accountToUpdateList = new List<Account>();
	Map<Id, Account> accountMap = new Map<Id, Account>();
	Set<Id> accountSet = new Set<Id>();
	
	// Converted leads
	for(Lead l : Trigger.New)
	{
		if(l.IsConverted && !Trigger.oldMap.get(l.Id).IsConverted)
		{	
			convertedLeadList.add(l);
			accountSet.add(l.ConvertedAccountId);
		}
	}
	
	// Collect accounts
	accountMap = new Map<Id, Account>([Select Id, Primary_Contact__c From Account Where Id In :accountSet]);
	
	// Set Primary Contact
	for(Lead l : convertedLeadList)
	{
		if(accountMap.containsKey(l.ConvertedAccountId))
		{
			Account acc = accountMap.get(l.ConvertedAccountId);
			acc.Primary_Contact__c = l.ConvertedContactId;
			accountToUpdateList.add(acc);
		}
	}
	
	// Update account
	if(!accountToUpdateList.isEmpty())
	{
		update accountToUpdateList;
	}
	*/
}