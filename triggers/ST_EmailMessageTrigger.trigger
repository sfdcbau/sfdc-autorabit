/*
 #####################################################
 # Created By...................: Aman Gupta
 # Created Date.................: 29 Mar, 2019
 # Last Modified By.............: Aman Gupta
 # Last Modified Date...........: 29 Mar, 2019
 # Description..................: This is trigger for EmailMessage object.
                                  All EmailMessage Trigger event methods called from here. 
                                  Code should be written in the respective handlers and call method from required event method.
 # Test Class...................: ST_EmailMessageTriggerTest
 # Note.........................: Please don't do any modification in Trigger
 #####################################################
*/
Trigger ST_EmailMessageTrigger on EmailMessage ( before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    List<Trigger_Settings__mdt> trigSetObjList = [SELECT 
                                                  Label, isActive__c 
                                                  FROM Trigger_Settings__mdt 
                                                  WHERE Label = 'ST_EmailMessageTrigger_Trigger' LIMIT 1];
    if(!trigSetObjList.isEmpty() && trigSetObjList[0].isActive__c) {
        // Before Insert Event
        if(Trigger.isBefore && Trigger.isInsert){
            ST_EmailMessageTriggerEventHandler.beforeInsert(Trigger.new);
        }
        // After Insert Event
        if(Trigger.isAfter && Trigger.isInsert){
            ST_EmailMessageTriggerEventHandler.afterInsert(Trigger.new, Trigger.oldMap);
        }
        // Before update Event
        if(Trigger.isBefore && Trigger.isUpdate){
            ST_EmailMessageTriggerEventHandler.beforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        // After Update Event
        if(Trigger.isAfter && Trigger.isUpdate){
            ST_EmailMessageTriggerEventHandler.afterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        // Before Delete Event
        if(Trigger.isBefore && Trigger.isDelete){
            ST_EmailMessageTriggerEventHandler.beforeDelete(Trigger.old, Trigger.oldMap);
        }
        // After Delete Event
        if(Trigger.isAfter && Trigger.isDelete){
            ST_EmailMessageTriggerEventHandler.afterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}