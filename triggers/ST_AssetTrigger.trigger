trigger ST_AssetTrigger on Asset (after delete, after insert, after undelete, after update,before update) {
    
    // @description : SE-1678 --> Update Account (field: ST_Payments_Customer_Status__c) for Payment Opportunities on the basis of Opportunity Stage whenever Opportunity record is created, updated.
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            //ST_UpdateCustomerStatusFromAssets.onAfterInsert(Trigger.new, Trigger.newMap);
        }
        if(Trigger.isUpdate) {
            ST_UpdateCustomerStatusFromAssets.onAfterUpdate(Trigger.old,Trigger.oldMap,Trigger.new, Trigger.newMap);
        }
    }
    
    
    // Added by Vishal
    if(trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        List<Asset> CMRRAssetList = new List<Asset>();
        If(Trigger.isInsert){
            Id assetSalesRecordTypeId = Schema.SObjectType.Asset.RecordTypeInfosByName.get('Sales').getRecordTypeId();
            Id UpsellAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
            for(Asset ast: trigger.new){ 
                if(ast.RecordTypeId == assetSalesRecordTypeId || ast.RecordTypeId == UpsellAssetRecordTypeId){
                    CMRRAssetList.add(ast);
                }  
            } 
        ST_UpdateCMRROnAccount.ST_UpdateCMRR(CMRRAssetList);  
        }
        if(Trigger.isUpdate){
            Id assetSalesRecordTypeId = Schema.SObjectType.Asset.RecordTypeInfosByName.get('Sales').getRecordTypeId();
            Id UpsellAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
            for(Asset ast: trigger.new){
                if((ast.RecordTypeId == assetSalesRecordTypeId || ast.RecordTypeId == UpsellAssetRecordTypeId)&& ( ast.Managed_Tech_Count__c != trigger.oldMap.get(ast.Id).Managed_Tech_Count__c || ast.Status != trigger.oldMap.get(ast.Id).Status)){
                    CMRRAssetList.add(ast);
                }  
            }
            ST_UpdateCMRROnAccount.ST_UpdateCMRR(CMRRAssetList);     
        }
    }   
    // Added by Mukul
    If(trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        List<Asset> assetList = new List<Asset>();
        If(Trigger.isInsert){
            Id assetSalesRecordTypeId = Schema.SObjectType.Asset.RecordTypeInfosByName.get('Sales').getRecordTypeId();
            for(Asset ast: trigger.new){ 
                if(ast.RecordTypeId == assetSalesRecordTypeId){
                    assetList.add(ast);
                }  
            } 
            ST_UpdateCustomerStatusFromAssets.Updatecustomerstatus(assetList); 
       }
        if(Trigger.isUpdate){
            Id assetSalesRecordTypeId = Schema.SObjectType.Asset.RecordTypeInfosByName.get('Sales').getRecordTypeId();
            for(Asset ast: trigger.new){
                if(ast.RecordTypeId == assetSalesRecordTypeId){ 
                    assetList.add(ast);
                }  
            }
            ST_UpdateCustomerStatusFromAssets.Updatecustomerstatus(assetList);    
        }
    }
    
}