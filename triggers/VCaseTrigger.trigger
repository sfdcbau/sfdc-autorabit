trigger VCaseTrigger on Case (before insert, after insert) 
{
    new VCaseTriggerDispatcher().dispatch();
}