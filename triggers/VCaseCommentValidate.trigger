/*
  Trigger to validate case comment.

  Notes:
    - Do not allow case comment insert on Case with status "Closed"
	
  Updates:
    - [20160516 SC]: Remove trigger context.
*/
trigger VCaseCommentValidate on CaseComment (after insert) 
{
  /*
  Map<Id, Case> commentCaseMap = new Map<Id, Case>();
    Set<Id> caseSet = new Set<Id>();
    
    // Collect case
    for(CaseComment cc : Trigger.new)
    {
        if(!caseSet.contains(cc.ParentId))
        {
            caseSet.add(cc.ParentId);
        }
    }
    
  commentCaseMap = new Map<Id, Case>([Select Id, Status From Case Where Id In :caseSet]);
    
    // Validate
    for(CaseComment cc : Trigger.new)
    {
        if(commentCaseMap.containsKey(cc.ParentId))
        {
            Case c = commentCaseMap.get(cc.ParentId);
            
            if(c.Status == 'Closed')
            {
                cc.addError('Cannot add comment to a closed case');
            }
        }
    }
	*/
}