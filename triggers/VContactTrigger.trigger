trigger VContactTrigger on Contact (after insert, after update) 
{
    new VContactTriggerDispatcher().dispatch();
}