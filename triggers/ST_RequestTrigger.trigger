trigger ST_RequestTrigger on Reference_Request__c (after update, before update) {
    
    //Checking the current user should by pass trigger or not
    if (!TriggerHelper.isBypassTrigger()) {
        system.debug('----------Trigger Executed-------');
         
        if(Trigger.isAfter){
            
            if(Trigger.isUpdate){
                RequestTriggerhandler.handleUpdateRequest(Trigger.New, trigger.oldmap);
            }
        }
        if(Trigger.isBefore){
        
          if(Trigger.isUpdate){
                RequestTriggerhandler.handleRequestStatusUpdate(Trigger.New, trigger.oldmap);
           }
        }
    }
}