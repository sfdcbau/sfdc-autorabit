/**********************************************************
*@author             : Mukul Kumar
*@Description        : ST_OnboardingTriggerEventHandler" calling from Onboarding trigger. Never change anything in Onboarding trigger. 
*                      Code should be written in the respective handlers and call method from required event method.
*					   Deactivate "ST_OnboardingTrigger_Trigger" custom metadata whenever need to deactivate onboarding trigger in production.
*@JIRA               : SDM-136
**********************************************************/
trigger ST_OnboardingTrigger on Onboarding__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    Trigger_Settings__mdt TriggerFlag = [SELECT 
                                         Label, isActive__c 
                                         FROM Trigger_Settings__mdt 
                                         where Label = 'ST_OnboardingTrigger_Trigger' limit 1];
    if(TriggerFlag.isActive__c) {
        
        // Before Insert Event
        if(Trigger.isBefore && Trigger.isInsert) {
            ST_OnboardingTriggerEventHandler.beforeInsert(Trigger.new);
        }
        // After Insert Event
        if(Trigger.isAfter && Trigger.isInsert) {
            ST_OnboardingTriggerEventHandler.afterInsert(Trigger.new, Trigger.oldMap);
        }
        // Before update Event
        if(Trigger.isBefore && Trigger.isUpdate) {
            ST_OnboardingTriggerEventHandler.beforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        // After Update Event
        if(Trigger.isAfter && Trigger.isUpdate) {
            ST_OnboardingTriggerEventHandler.afterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        // Before Delete Event
        if(Trigger.isBefore && Trigger.isDelete) {
            ST_OnboardingTriggerEventHandler.beforeDelete(Trigger.old, Trigger.oldMap);
        }
        // After Delete Event
        if(Trigger.isAfter && Trigger.isDelete) {
            ST_OnboardingTriggerEventHandler.afterDelete(Trigger.old, Trigger.oldMap);
        }
    }
}