public without sharing class VOpportunityTriggerDispatcher 
{
    public void dispatch() 
	{
		if(Trigger.isAfter && Trigger.isInsert)
        {
            setCustomerPartnerOpportunityOnNewOpportunity();
            setupCustomerPartnerOpportunities();
        }
		if(Trigger.isAfter && Trigger.isUpdate)
        {
            setCustomerPartnerOpportunityOnOpportunityClosedLost();
            setupCustomerPartnerOpportunities();
        }
    }
	
    private void setCustomerPartnerOpportunityOnNewOpportunity()
    {
        List<CustomerPartner__c> updateCustomerPartnerList = new List<CustomerPartner__c>();
        Map<Id, Opportunity> accountOpportunityMap = new Map<Id, Opportunity>();
        
        if(Trigger.isInsert)
        {
            for(Opportunity opp : (List<Opportunity>)Trigger.new)
            {
                if(opp.AccountId != null && !accountOpportunityMap.containsKey(opp.AccountId))
                    accountOpportunityMap.put(opp.AccountId, opp);
            }
        }
        
        if(!accountOpportunityMap.isEmpty())
        {
            for(CustomerPartner__c cp : [Select Id, Customer__c, Opportunity__c From CustomerPartner__c Where Customer__c In :accountOpportunityMap.keySet() And Opportunity__c = null])
            {
                Opportunity opp = accountOpportunityMap.get(cp.Customer__c);
                
                cp.Opportunity__c = opp.Id;
                updateCustomerPartnerList.add(cp);
            }
        }
        
        if(!updateCustomerPartnerList.isEmpty())
            update updateCustomerPartnerList;
    }
    
    private void setCustomerPartnerOpportunityOnOpportunityClosedLost()
    {
        List<CustomerPartner__c> updateCustomerPartnerList = new List<CustomerPartner__c>();
        List<Opportunity> opportunityList = new List<Opportunity>();
        Map<Id, Opportunity> accountOpenOpportunityMap = new Map<Id, Opportunity>(); // Map<AccountId, Opportunity>
        Set<Id> customerSet = new Set<Id>();
        
        if(Trigger.isUpdate)
        {
            for(Opportunity opp : (List<Opportunity>)Trigger.new)
            {
                Opportunity old = (Opportunity)Trigger.oldMap.get(opp.Id);
                
                if((opp.StageName.equalsIgnoreCase('Closed Lost') || old.StageName.equalsIgnoreCase('Closed Lost')) && opp.StageName != old.StageName)
                {
                    opportunityList.add(opp);
                    
                    if(opp.AccountId != null && !customerSet.contains(opp.AccountId))
                    	customerSet.add(opp.AccountId);
                }
            }
        }
        
        if(!customerSet.isEmpty())
        {
            // Get most recent open opportunity
            for(Opportunity opp : [Select Id, AccountId From Opportunity Where AccountId In :customerSet And IsClosed = false Order By CreatedDate Desc])
            {
                if(!accountOpenOpportunityMap.containsKey(opp.AccountId))
                    accountOpenOpportunityMap.put(opp.AccountId, opp);
            }
            
            for(CustomerPartner__c cp : [Select Id, Opportunity__c, Opportunity__r.StageName, Customer__c From CustomerPartner__c 
                                         Where Customer__c In :customerSet And (Opportunity__c = null Or Opportunity__r.StageName = 'Closed Lost')])
            {
                if(accountOpenOpportunityMap.containsKey(cp.Customer__c))
                {
                    Opportunity opp = accountOpenOpportunityMap.get(cp.Customer__c);
                    cp.Opportunity__c = opp.Id;
                }
                else
                {
                    cp.Opportunity__c = null;
                }
                
                updateCustomerPartnerList.add(cp);
            }
        }
        
        if(!updateCustomerPartnerList.isEmpty())
            update updateCustomerPartnerList;
    }
	
    private void setupCustomerPartnerOpportunities()
    {
        List<Customer_Partner_Opportunity__c> insertCustomerPartnerContactList = new List<Customer_Partner_Opportunity__c>();
        List<Customer_Partner_Opportunity__c> deleteCustomerPartnerContactList = new List<Customer_Partner_Opportunity__c>();
        Set<Id> accountSet = new Set<Id>();
        Map<Id, Set<Id>> insertAccountOpportunitySetMap = new Map<Id, Set<Id>>(); // Map<AccountId, Set<ContactId>>
        Map<Id, Set<Id>> deleteAccountOpportunitySetMap = new Map<Id, Set<Id>>(); // Map<AccountId, Set<ContactId>>
        
        if(Trigger.isInsert)
        {
            for(Opportunity opp : (List<Opportunity>)Trigger.new)
            {
                if(opp.AccountId != null)
                {
                    if(!accountSet.contains(opp.AccountId))
                        accountSet.add(opp.AccountId);
                    
                    if(!insertAccountOpportunitySetMap.containsKey(opp.AccountId))
                        insertAccountOpportunitySetMap.put(opp.AccountId, new Set<Id>());
                    
                    if(!insertAccountOpportunitySetMap.get(opp.AccountId).contains(opp.Id))
                        insertAccountOpportunitySetMap.get(opp.AccountId).add(opp.Id);
                }
            }
        }
        else if(Trigger.isUpdate)
        {
            for(Opportunity opp : (List<Opportunity>)Trigger.new)
            {
                Opportunity old = (Opportunity)Trigger.oldMap.get(opp.Id);
                
                if(opp.AccountId != old.AccountId)
                {
                    if(!accountSet.contains(opp.AccountId))
                        accountSet.add(opp.AccountId);
                    
                    if(!accountSet.contains(old.AccountId))
                        accountSet.add(old.AccountId);
                    
                    if(!insertAccountOpportunitySetMap.containsKey(opp.AccountId))
                        insertAccountOpportunitySetMap.put(opp.AccountId, new Set<Id>());
                    
                    if(!insertAccountOpportunitySetMap.get(opp.AccountId).contains(opp.Id))
                        insertAccountOpportunitySetMap.get(opp.AccountId).add(opp.Id);
                    
                    if(!deleteAccountOpportunitySetMap.containsKey(old.AccountId))
                        deleteAccountOpportunitySetMap.put(old.AccountId, new Set<Id>());
                    
                    if(!deleteAccountOpportunitySetMap.get(old.AccountId).contains(old.Id))
                        deleteAccountOpportunitySetMap.get(old.AccountId).add(old.Id);
                }
            }
        }
        
        if(!insertAccountOpportunitySetMap.isEmpty() || !deleteAccountOpportunitySetMap.isEmpty())
        {
            // Collect partner opportunities to delete
            for(Customer_Partner_Opportunity__c cpo : [Select Id, Customer_Partner__c, Customer_Partner__r.Customer__c, Customer_Partner__r.Partner__c, Opportunity__c 
                                                       From Customer_Partner_Opportunity__c Where (Customer_Partner__r.Customer__c In :accountSet Or Customer_Partner__r.Partner__c In :accountSet)])
            {
                if(deleteAccountOpportunitySetMap.containsKey(cpo.Customer_Partner__r.Customer__c))
                {
                    if(deleteAccountOpportunitySetMap.get(cpo.Customer_Partner__r.Customer__c).contains(cpo.Opportunity__c))
                        deleteCustomerPartnerContactList.add(cpo);
                }
                
                if(deleteAccountOpportunitySetMap.containsKey(cpo.Customer_Partner__r.Partner__c))
                {
                    if(deleteAccountOpportunitySetMap.get(cpo.Customer_Partner__r.Partner__c).contains(cpo.Opportunity__c))
                        deleteCustomerPartnerContactList.add(cpo);
                }
            }
            
            // Collect new partner contacts
            for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c From CustomerPartner__c Where (Customer__c In :accountSet Or Partner__c In :accountSet)])
            {
                if(insertAccountOpportunitySetMap.containsKey(cp.Customer__c))
                {
                    for(Id opportunityId : insertAccountOpportunitySetMap.get(cp.Customer__c))
                    {
                        Customer_Partner_Opportunity__c newcpo = new Customer_Partner_Opportunity__c();
                        newcpo.Customer_Partner__c = cp.Id;
                        newcpo.Opportunity__c = opportunityId;
                        newcpo.Is_Partner_Opportunity__c = false;
                        insertCustomerPartnerContactList.add(newcpo);
                    }
                }
                
                if(insertAccountOpportunitySetMap.containsKey(cp.Partner__c))
                {
                    for(Id opportunityId : insertAccountOpportunitySetMap.get(cp.Partner__c))
                    {
                        Customer_Partner_Opportunity__c newcpo = new Customer_Partner_Opportunity__c();
                        newcpo.Customer_Partner__c = cp.Id;
                        newcpo.Opportunity__c = opportunityId;
                        newcpo.Is_Partner_Opportunity__c = true;
                        insertCustomerPartnerContactList.add(newcpo);
                    }
                }
            }
        }
        
        if(!deleteCustomerPartnerContactList.isEmpty())
            delete deleteCustomerPartnerContactList;
        
        if(!insertCustomerPartnerContactList.isEmpty())
            insert insertCustomerPartnerContactList;
    }
}