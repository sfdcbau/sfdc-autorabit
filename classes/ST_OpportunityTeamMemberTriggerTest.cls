/*
#####################################################
# Created By....................: Aman Gupta
# Created Date................: 31 Dec, 2018
# Last Modified By..........: Aman Gupta
# Last Modified Date......: 31 Dec, 2018
# Description...................: This is test class for ST_OpportunityTeamMemberTrigger trigger and ST_OpportunityTeamMemberTriggerHandler class.
#####################################################
*/
@isTest
private class ST_OpportunityTeamMemberTriggerTest {
    /**
* @author : Aman Gupta
* @date : 31 Dec, 2018
* @description : This method create test data.
*/
    @testSetup static void setup() {
        Account cAccObj1 = new Account(Name = 'TestCAcc1');
        cAccObj1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account pAccObj1 = new Account(Name = 'TestPAcc1');
        pAccObj1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        
        insert new List<Account> {cAccObj1, pAccObj1};
            
            Opportunity oppObj1 = new Opportunity(Name = 'TestCAcc1Opp1', AccountId = cAccObj1.Id, StageName = 'Stage Zero', CloseDate = System.today().addDays(2));
        oppObj1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Opportunity oppObj2 = new Opportunity(Name = 'TestPAcc1Opp1', AccountId = pAccObj1.Id, StageName = 'Stage Zero', CloseDate = System.today().addDays(2));
        oppObj2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Opportunity oppObj3 = new Opportunity(Name = 'TestCAcc1Opp3', AccountId = cAccObj1.Id, StageName = 'Stage Zero', CloseDate = System.today().addDays(2));
        oppObj2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        insert new List<Opportunity> {oppObj1, oppObj2, oppObj3};
            }
    
    /**
* @author : Aman Gupta
* @date : 31 Dec, 2018
* @description : This method is to test ST_OpportunityTeamMemberTriggerHandler updateOpportunitySEImpact function.
*/
    @isTest static void testUpdateOpportunitySEImpact() {
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<Profile> pObj1= [SELECT Id FROM Profile WHERE Name = 'Standard Sales'];
        List<UserRole> roles = [select id from userrole where Name = 'Sales Manager'];
        
        User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        
        User userObj2 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        
        User userObj3 = new User(ProfileId = pObj1[0].Id,UserRoleId=roles[0].id, LastName = 'TestUser1113', Email = 'testuser1132@test.com', Username = 'Test.User1132@test.com', CompanyName = 'TestCom3', Title = 'TestTitle3', Alias = 'TU1312', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        
        insert new List<User> {userObj1, userObj2, userObj3};
            
            System.runAs (userObj1) {
                Account cAccObj1 = [Select Id, Name from Account where Name = 'TestCAcc1' LIMIT 1];
                Account pAccObj1 = [Select Id, Name from Account where Name = 'TestPAcc1' LIMIT 1];
                
                Opportunity oppObj1 = [Select Id, Name from Opportunity where Name = 'TestCAcc1Opp1' LIMIT 1];
                oppObj1.SEOwner__c = userobj3.id;
                update oppObj1;
                
                Opportunity oppObj2 = [Select Id, Name from Opportunity where Name = 'TestPAcc1Opp1' LIMIT 1];
                
                // insert OpportunityTeamMember
                OpportunityTeamMember oppTMObj1 = new OpportunityTeamMember(OpportunityId = oppObj1.Id, UserId = userObj1.Id,TeamMemberRole = 'Sales Engineer');
                OpportunityTeamMember oppTMObj2 = new OpportunityTeamMember(OpportunityId = oppObj1.Id, UserId = userObj2.Id,TeamMemberRole = 'Sales Rep');
                OpportunityTeamMember oppTMObj3 = new OpportunityTeamMember(OpportunityId = oppObj2.Id, UserId = userObj1.Id,TeamMemberRole = 'Sales Rep');
                insert new List<OpportunityTeamMember> {oppTMObj1, oppTMObj2, oppTMObj3};
                    
                    oppObj1 = [Select Id, Name, SE_Impact__c from Opportunity where Name = 'TestCAcc1Opp1' LIMIT 1];
                oppObj2 = [Select Id, Name, SE_Impact__c from Opportunity where Name = 'TestPAcc1Opp1' LIMIT 1];
                System.assertEquals(2, oppObj1.SE_Impact__c);
                System.assertEquals(null, oppObj2.SE_Impact__c);
                
                test.startTest();
                //update OpportunityTeamMember
                oppTMObj2.TeamMemberRole = 'Sales Engineer';
                oppTMObj3.TeamMemberRole = 'Sales Engineer';
                
                update new List<OpportunityTeamMember> {oppTMObj2, oppTMObj3};
                    
                    oppObj1 = [Select Id, Name, SE_Impact__c from Opportunity where Name = 'TestCAcc1Opp1' LIMIT 1];
                oppObj2 = [Select Id, Name, SE_Impact__c from Opportunity where Name = 'TestPAcc1Opp1' LIMIT 1];
                System.assertEquals(3, oppObj1.SE_Impact__c);
                System.assertEquals(1, oppObj2.SE_Impact__c);
                
                // delete OpportunityTeamMember
                delete new List<OpportunityTeamMember> {oppTMObj1, oppTMObj3};
                    
                    oppObj1 = [Select Id, Name, SE_Impact__c from Opportunity where Name = 'TestCAcc1Opp1' LIMIT 1];
                oppObj2 = [Select Id, Name, SE_Impact__c from Opportunity where Name = 'TestPAcc1Opp1' LIMIT 1];
                System.assertEquals(2, oppObj1.SE_Impact__c);
                System.assertEquals(null, oppObj2.SE_Impact__c);
                test.stopTest();
            }   
    }
    
    /**
* @author : Kamal Singh
* @date : 15 APR, 2019
* @description : This method is to test ST_OpportunityTeamMemberTriggerHandler addSalesEngineerValidation method.
*/
    @isTest static void testaddSalesEngineerValidation() {
        List<OpportunityTeamMember> oppTMList = new List<OpportunityTeamMember>();
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<UserRole> rObj= [SELECT Id FROM UserRole WHERE Name = 'Sales Manager'];
        User userObj1 = new User(ProfileId = pObj[0].Id, UserRoleId = rObj[0].Id, LastName = 'TestUser123', Email = 'testuser1111@test.com', Username = 'Test.User123@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU123', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj[0].Id, UserRoleId = rObj[0].Id, LastName = 'TestUser456', Email = 'testuser4444@test.com', Username = 'Test.User456@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU123', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        
        Opportunity oppObj1 = [Select Id, Name, SEOwner__c from Opportunity where Name = 'TestCAcc1Opp1' LIMIT 1];
        Opportunity oppObj2 = [Select Id, Name, SEOwner__c from Opportunity where Name = 'TestPAcc1Opp1' LIMIT 1];
        oppObj2.SEOwner__c = userObj1.Id;
        update oppObj2;
        Opportunity oppObj3 = [Select Id, Name, SEOwner__c from Opportunity where Name = 'TestCAcc1Opp3' LIMIT 1];
        oppObj3.SEOwner__c = userObj1.Id;
        update oppObj3;
        
        // create OpportunityTeamMember
        OpportunityTeamMember oppTMObj1 = new OpportunityTeamMember(OpportunityId = oppObj1.Id, UserId = userObj1.Id,TeamMemberRole = 'Sales Engineer');
        OpportunityTeamMember oppTMObj2 = new OpportunityTeamMember(OpportunityId = oppObj2.Id, UserId = userObj1.Id,TeamMemberRole = 'Sales Engineer');
        OpportunityTeamMember oppTMObj3 = new OpportunityTeamMember(OpportunityId = oppObj3.Id, UserId = userObj2.Id,TeamMemberRole = 'Sales Engineer');
        oppTMList.add(oppTMObj1);
        oppTMList.add(oppTMObj2);
        oppTMList.add(oppTMObj3);
        test.startTest();
        Boolean testExecution;
        Integer testSize =10;
        
        // insert OpportunityTeamMember
        ST_OpportunityTeamMemberTriggerHandler obj = new ST_OpportunityTeamMemberTriggerHandler(testExecution,testSize);
        obj.addSalesEngineerValidation(oppTMList);
        system.assertEquals(null, oppTMObj1.Id);
        system.assertEquals(null, oppTMObj2.Id); 
        test.stopTest();
    }
    
}