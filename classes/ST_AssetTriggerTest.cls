@isTest
private class ST_AssetTriggerTest{
    @TestSetUp
    public static void setup(){
        
        Product2 product = utilityHelperTest.createProduct();
        insert product;
        PricebookEntry standPB = utilityHelperTest.craeteStandardPBE2(product.id);
        insert standPB;
        Pricebook2 custPB = utilityHelperTest.craeteCustomPB2();
        insert custPB;        
        PricebookEntry pbe = utilityHelperTest.createPricebookEntry(custPB, product);
        insert pbe;
        Account acc = utilityHelperTest.createAccount();
        acc.Customer_Status_picklist__c = 'Stage Zero';
        insert acc;
        contact con = utilityHelperTest.createContact(acc.id);
        insert con;
        Opportunity opp = utilityHelperTest.createopportunity(acc.id, custPB.id,con.id);
        insert opp;
        Asset  ast = utilityHelperTest.createAsset(Opp.id, acc.id);
        insert ast;
        OpportunityLineItem oppLineItem = utilityHelperTest.createOppProduct(Opp.id, pbe.id, product.id);
        insert oppLineItem ;
    }
    
    @isTest static void testMethod1() {
        list<Asset> astlist = new list<Asset>();
        list<Asset> astnewlist = new list<Asset>();
        list<Onboarding__c> onbrdingList = new list<Onboarding__c>();
        Map<Id, Asset> oldmap = new map<Id, Asset>();
        Set<Id> accIds = new Set<Id>();
        
        Account acc = [select id, CMRR_Total__c, name, Industry__c, Software_newpicklist__c, No_Known_Active_Partners__c, Estimated_Total_Potential_Office_Users__c, 
                       Influencer__c, CountofOwnerPOCContacts__c,Customer_Status_picklist__c from account limit 1];
        Asset ast=    [select id, name, AccountId, Managed_Tech_Count__c, status,Opportunity__c,Opportunity__r.Name from asset Where RecordType.Name='Sales' limit 1];
        OpportunityLineItem  ol= [select id, name, Quantity, product2id,OpportunityID,TotalPrice,Recurring_Revenue__c,Projected_Quantity__c,PricebookEntryId,UnitPrice
                                  from OpportunityLineItem limit 1 ];
        Opportunity Opp  = [select id,name,Total_Projected_MRR__c,AccountId,StageName, DispositionCode__c, SubDisposition__c, Competing_Software__c 
                            From Opportunity where stageName = 'Stage Zero' limit 1];        
        
        astlist.add(ast);
        for(Asset asst : astlist) {
            asst.Managed_Tech_Count__c = 30;
            astnewlist.add(asst);
            oldmap.put(asst.Id , asst);
        }
        System.assertEquals(37, ol.Quantity);
        String AstId = ast.Id;    
        Test.starttest();
        ST_UpdateCustomerStatusFromAssets.Updatecustomerstatus(astlist);
        ST_UpdateCMRROnAccount.ST_UpdateCMRR(astlist);
        
        Test.stopTest();
    }
    @isTest static void testMethod2(){    
        list<Asset> astnewlist = new list<Asset>();
        list<Opportunity> optyList  = new list<Opportunity>();
        Account acc =[select id, CMRR_Total__c, name, Industry__c, Software_newpicklist__c, No_Known_Active_Partners__c, Estimated_Total_Potential_Office_Users__c,
                      Influencer__c,CountofOwnerPOCContacts__c,Customer_Status_picklist__c  from account limit 1];
        Asset ast =  [select id, name, AccountId, Managed_Tech_Count__c, status,Opportunity__c,Opportunity__r.Name from asset Where RecordType.Name='Sales' limit 1];
        OpportunityLineItem  ol=[select id, name, Quantity, product2id,OpportunityID,TotalPrice,Recurring_Revenue__c,Projected_Quantity__c,PricebookEntryId,UnitPrice
                                 from OpportunityLineItem limit 1 ];
        Opportunity Opp  = [select id,name,Total_Projected_MRR__c, StageName, DispositionCode__c, SubDisposition__c, Competing_Software__c, Address_Industry_Verified__c,
                            OBDiscoveryCallDate__c, AccountId,ImplementationDate__c, Onboarding_POC__c From  Opportunity Where StageName ='Stage Zero' limit 1];
        test.startTest();
        ast.status = 'Pending';
        update ast;
        astnewlist.add(ast);
        Opp.StageName = 'Closed Won';
        Opp.DispositionCode__c = 'Demo went well';
        update Opp;
        optyList.add(Opp);
        
        Map<Id, Asset> oldmap = new map<Id, Asset>([select id, name,Managed_Tech_Count__c, status,Opportunity__c,Opportunity__r.Name from asset Where RecordType.Name='Sales' limit 1]);
        ST_UpdateCMRROnAccount.ST_UpdateCMRR(astnewlist);
        test.stopTest();
        
    }
}