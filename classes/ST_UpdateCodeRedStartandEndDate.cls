/*
 #####################################################
 # Created By....................: Tharun G
 # Created Date................: 17 Jan, 2019
 # Last Modified By..........: Tharun G
 # Last Modified Date......: 17 Jan, 2019
 # Description...................: [SE-1153]: This is an invocable class calling from Process Builder "Databees_Updates" on Account
	Purpose of this class is when ever health score changes on Account, system will go to the related
	ST_onboarding records and update the Code Red Start and End Date accordingly.
 # Test Class...................: ST_OpportunityTriggerHandlerTest
 #####################################################
*/
public class ST_UpdateCodeRedStartandEndDate {
    
    @InvocableMethod
    public static void updateDateMethod(List<Account> newAccountList){
        
        Set<Id> accountIds = new Set<Id>();
        // Iterate through newAccounts and add it to a set.
        for(Account acc : newAccountList){
            accountIds.add(acc.id);
        }
        Id ST_OnboardingRecordTypeId = Schema.SObjectType.Onboarding__c.RecordTypeInfosByName.get('ST Onboarding').getRecordTypeId();
        List<Onboarding__c> listofOnboardingRecordsToUpdate = new List<Onboarding__c>();
        
        //Query ST Onboarding records for above set accounts.
       
        List<Onboarding__c>  obList = [Select id,  Account__c,
                                                   Code_Red_End_Date__c, 
                                                   Code_Red_Start_Date__c,
                                                   Account__r.Health_Score__c
                                                   from Onboarding__c where 
                                                   (Onboarding_Status__c = 'Pre-Implementation'or Onboarding_Status__c = 'Implementation'or Onboarding_Status__c ='Live')
                                                   AND Account__c in: accountIds
                                                   AND Onboarding__c.recordTypeId =:ST_OnboardingRecordTypeId];
                    
        if(!obList.isEmpty()){
            
            // Itterate the Oblist records and check the Account Health Score 
            for(Onboarding__c onb_Obj: obList){
                
                if(onb_Obj.Account__r.Health_Score__c == '1 - Code Red'){
                    
                    	 onb_Obj.Code_Red_Start_Date__c = System.today();                 
                }
                if(onb_Obj.Account__r.Health_Score__c != '1 - Code Red'){
                    
                    	onb_Obj.Code_Red_End_Date__c = System.today();    
                }
               listofOnboardingRecordsToUpdate.add(onb_Obj); 
            }
            
            // if list has records update onboardings
            if(!listofOnboardingRecordsToUpdate.isEmpty()){              
                update listofOnboardingRecordsToUpdate;    
            }     
        }
    }
    
}