/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐

*
* This class is updating the OB Discovery call Date after the Onboarding is created
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sanjeev Kaurav  <skaurav@servicetitan.com>
* @version        1.1
* @created        21-11-2018
* @Name           ST_UpdateObDiscoveryCall
* @Method         ST_UpdateDiscoCall()
* @TestClass      ST_UpdateObDiscoveryCallTest
* @Trigger        ST_OnboardingTrigger
@Jira UserStroy 

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ST_UpdateObDiscoveryCall{
    
    Public static void ST_UpdateDiscoCall(list<Opportunity> oppList, Map<Id, Opportunity> oldMap ){
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Map<Id,Opportunity> mapOfIdAndOpportunity = new Map<Id,Opportunity>();
        List<Onboarding__c> updateOnbList = new List<Onboarding__c>();
        
        for(Opportunity opp :oppList){
            
            if(opp.OBDiscoveryCallDate__c != oldMap.get(opp.Id).OBDiscoveryCallDate__c && opp.RecordTypeId == OppRecordTypeId ) {
            
                if(opp.stagename == 'Closed Won' && opp.Delayed_Implementation__c == true && opp.OBDiscoveryCallDate__c!=null){                
                    mapOfIdAndOpportunity.put(opp.id,opp);
                }
            }
        }
        //Updating Onboarding OB_Discovery_Call_Date__c with Opportunity OBDiscoveryCallDate__c
        If(mapOfIdAndOpportunity.keyset().size()>0){
            
            for(Onboarding__c onb : [Select id,Opportunity__c,OB_Discovery_Call_Date__c from Onboarding__c where Opportunity__c IN :mapOfIdAndOpportunity.keyset() AND RecordType.Name='ST Onboarding']){
            
                if(mapOfIdAndOpportunity.containsKey(onb.Opportunity__c)){
                    onb.OB_Discovery_Call_Date__c = mapOfIdAndOpportunity.get(onb.Opportunity__c).OBDiscoveryCallDate__c;
                    updateOnbList.add(onb);
                }
            }
        }
        if(updateOnbList.size()>0){
            update updateOnbList;
        }
        
        
    }
}