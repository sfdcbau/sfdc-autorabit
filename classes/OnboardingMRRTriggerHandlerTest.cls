@isTest
public class OnboardingMRRTriggerHandlerTest{
 
    public static testMethod void testAssetCreation(){
        Id AccRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Strategic').getRecordTypeId();
        system.debug('===>AccRT' +AccRT );
        
        Account parentAcc = new Account(Name = 'Parent Test Account', 
                                RecordTypeId= AccRT,
                                BillingCity = 'Frisco',
                                BillingCountry = 'United States',
                                BillingPostalCode = '73035',
                                BillingState = 'California',
                                BillingStreet = 'test',
                                Pricing_Model__c = 'New Pricing (Managed Techs)',
                                Software_newpicklist__c = 'Other',
                                
                               
                                No_Known_Active_Partners__c = true,
                                Industry__c = 'HVAC',
                                Influencer__c = 'Unknown',
                                Influencer_Detail__c = 'Test',
                                PlusOrMinus5EstTechs__c = '5+',
                                Estimated_Total_Potential_Office_Users__c = 3,
                                Residential__c = 70,
                                Tenant_ID__c = '12341234234234242342342',
                                Tenant_Name__c = 'testyMcTestersonsTest1', 
                                Initial_Username__c = 'ThisIsAMotherFlippinTest',
                                Time_Zone__c = 'Pacific (UTC-8:00)'
                                );
        insert parentAcc;
        
        //Create contact record
        Contact con1=new Contact(FirstName='test',LastName='test1',Email='test@gmail.com',AccountId= parentAcc.Id,Role__c='POC');
        insert con1;
        
        Account childAcc = new Account (Name = 'Child Test Acc', 
                                       RecordTypeId= AccRT,BillingCity = 'Frisco',
                                BillingCountry = 'United States',
                                BillingPostalCode = '75630',
                                BillingState = 'Texas',
                                BillingStreet = 'test',
                                Pricing_Model__c = 'New Pricing (Managed Techs)',
                                Software_newpicklist__c = 'Other',
                                
                               
                                No_Known_Active_Partners__c = true,
                                Industry__c = 'HVAC',
                                Influencer__c = 'Unknown',
                                Influencer_Detail__c = 'Test',
                                PlusOrMinus5EstTechs__c = '5+',
                                Estimated_Total_Potential_Office_Users__c = 3,
                                Residential__c = 70,
                                Tenant_ID__c = '123412342342342423423421',
                                Tenant_Name__c = 'testyMcTestersonsTest11', 
                                Initial_Username__c = 'ThisIsAMotherFlippinTest1',
                                Time_Zone__c = 'Pacific (UTC-8:00)');
        Test.startTest();   
        insert childAcc;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prd1 = new Product2 (Name='Managed Tech',Description='Test Product Entry 1',productCode = 'ABC', isActive = true);
        insert prd1;
    
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
        insert pbe1;
    
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        system.debug('===>OPPRT' +OppRT );

        Opportunity o = new Opportunity (Name='Opp1',
                                         StageName='Stage Zero', 
                                         RecordTypeId= OppRT, 
                                         CloseDate=Date.today(),
                                         Pricebook2Id = pbe1.Pricebook2Id, 
                                         AccountId = parentAcc.id,
                                         Address_Industry_Verified__c = true,
                                         Software_new__c = 'test',
                                         Competing_Software__c = 'Other',
                                         Onboarding_POC__c=con1.Id,
                                         AE_Discovery_POC__c = con1.Id,
                                         OBDiscoveryCallDate__c = system.today(),
                                         ImplementationDate__c = system.today()
                                         );
        insert o;
        
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = o.id;
        oli.Product2Id = prd1.id;
        oli.unitPrice = 100;
        oli.Quantity = 38;
        
        insert oli;
        
        o.StageName = 'Closed Won';
        o.DispositionCode__c = 'Better functionality';
        update o;
        
        Onboarding__c ob = new Onboarding__c (Name = 'Onboarding Test',
                                              Account__c = childAcc.id,
                                              Parent_Account__c = parentAcc.id,
                                              Opportunity__c = o.id,
                                              Manual_Check_Add_On__c = true,
                                              Number_of_Techs__c = 1,
                                              
                                              RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId() 
                                             );
        insert ob; 
        
        
        
        ob.Number_of_Techs__c = 2;
        update ob; 
        
        Test.stopTest();

                                          
        
    }
}