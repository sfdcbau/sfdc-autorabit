@isTest
global class TalkdeskGainsightLogActivityTest implements HttpCalloutMock {
    
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {      
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"animals": ["majestic badger", "fluffy bunny", "scary bear", "chicken", "mighty moose"]}');
        response.setStatusCode(200);
        return response; 
    }
    
    static testMethod void testDoPost(){
        // fetching talkdesk__Talkdesk_Activity__c record

        //Data
        List<Contact> cList = New List<Contact>();
        
            
        Talkdesk_GS_Call_Types__c  tct = new Talkdesk_GS_Call_Types__c ();
        tct.Name = 'Success Call';
        tct.Gainsight_ID__c  = '00e1a000000tbPnAAI';
        insert(tct);
        
        User u = new user();
        u.FirstName = 'Talkdesk';
        u.LastName = 'Test';
        u.email = 'talkdesktest@talkdesk.com';
        u.username = 'talkdesk.test@talkdesk.com';
        u.Alias = 'pzeel'; 
        u.TimeZoneSidKey = 'America/Los_Angeles'; 
        u.LocaleSidKey = 'en_US'; 
        u.EmailEncodingKey = 'ISO-8859-1'; 
        u.ProfileId = '00e1a000000tbPnAAI'; 
        u.LanguageLocaleKey = 'en_US';
        insert(u);
        
        //Account
        Account a = new Account();
        a.name = 'Test Account';
        insert(a);
          
        //Contacts
        Contact c = new Contact();
        c.FirstName = 'Contact';
        c.LastName = 'One';
        c.accountID = a.Id;
        c.Phone = '+14155555555';
        c.MobilePhone = '+14155555555';
        cList.add(c);
              
        Contact c2 = new Contact();
        c2.FirstName = 'Contact';
        c2.LastName = 'Two';
        c2.Phone = '+14155555554';
        c2.MobilePhone = '+14155555554';
        cList.add(c2);
          
        insert(cList);
        
        //Task
        Talkdesk__Talkdesk_Activity__c t = new Talkdesk__Talkdesk_Activity__c();
        t.Name = 'Inbound Call';
        t.talkdesk__User__c = u.id; 
        t.Talkdesk__Start_Time__c = system.datetime.now();
        t.Talkdesk__Notes__c='These are test notes'; 
        t.Talkdesk__Disposition_Code__c='Successful Call'; 
        t.Talkdesk__Talkdesk_id__c='CAdefa1241rasfa153';
        t.Talkdesk__Account__c=a.id;
        insert(t);

        TalkdeskGainsightLogActivity.gsCreateActivity(t.id);  
       
        List<talkdesk__Talkdesk_Activity__c> activityObjList = [Select Id, Name from talkdesk__Talkdesk_Activity__c];
        System.assertEquals(1, activityObjList.size());
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TalkdeskGainsightLogActivityTest()); 
        
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        //TalkdeskGainsightLogActivity.gsCreateActivity(activityObjList[0].Id);
        
    }     
}