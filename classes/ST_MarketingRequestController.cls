/**
* ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is an controller extension class used for Visualforce page 'ST_MarketingRequestController' with Reference_Request__c standard controller
* This is called from Marketing Request tab
* This class creates a Reference Request record when user enter all required details
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          01-16-2019
* @Jira User Story  SC-09
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘\
* Test Class: ST_MarketingRequestController_Test
*/

public class ST_MarketingRequestController {
    public Reference_Request__c recRefRequest{get;set;}
    public Boolean IsSaveSuccessfull {get;set;}
    public Boolean IsDuplicateAccount {get;set;}
    public String successMessage{get;set;}
    public Boolean IsProAccSaveSuccessfull {get;set;}
    public List<Reference_Request__c> lstRequest = new List<Reference_Request__c>();
    
    //newchange starts...
    public Proposed_Account__c acc {get;set;}
    public Set<Id> setAccId = new Set<Id>();
    public List<Proposed_Account__c > lstAcctoShow {get;set;}
    public Boolean display{get;set;}
    public Boolean display2{get;set;}
    public Integer rowToRemove {get;set;}
    public Set<Id> setAccProId = new Set<Id>();
    //newchange end.....
    
    // Constructer declaration....
    public ST_MarketingRequestController(){   
        successMessage = '';
        recRefRequest= new Reference_Request__c();
        recRefRequest.RecordTypeID = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.MarketingRequestRecordType).getRecordTypeId(); 
        acc = new Proposed_Account__c();
    }
    
    //Standard Submit funcion....
    public PageReference Submit() {
        Id requestRecordTypeId; 
        if(Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.MarketingRequestRecordType) != null){
            
            //getting recordtype for Reference Request custom object
            requestRecordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.MarketingRequestRecordType).getRecordTypeId();
            IsSaveSuccessfull = false;
        }
        
        // inserting Reference Request record......
        recRefRequest.RecordTypeId = requestRecordTypeId;
        recRefRequest.Reference_Status__c = 'New';
        
        // inserting Request record......
        try{
            lstRequest.add(recRefRequest);
            if(lstRequest != null && lstRequest.size() > 0 ){
                insert lstRequest;
                IsSaveSuccessfull = true;
                successMessage = label.Request_Submit_Message;
            }
            if(lstAcctoShow != null && lstAcctoShow.size() > 0){
                for(Proposed_Account__c eachProposedAcc : lstAcctoShow){
                    eachProposedAcc.Request__c =  lstRequest[0].Id; 
                }
                update lstAcctoShow;
                
            }
        }     
        catch(DMLException ex){
            IsSaveSuccessfull = false;      
            lstRequest.clear();
        } 
        return null; 
    }
    public pagereference cancel() { 
        pagereference pgRef = new pagereference('/'); 
        pgRef.setRedirect(true); 
        return pgRef;
    }
    
    public PageReference SaveMultipleAccounts(){
        try{ 
            if(acc != null){
                system.debug('acc:::' + acc);
                if(acc.Id == null && acc.Account__c != null){
                    system.debug('setAccProId::::' + setAccProId);
                    if(setAccProId.size() > 0 && setAccProId.contains(acc.Account__c)){
                        IsDuplicateAccount = true;
                        IsProAccSaveSuccessfull = false;
                    }
                    else{
                        IsProAccSaveSuccessfull = false; 
                        insert acc;    
                    }
                    
                }
                else{
                    IsProAccSaveSuccessfull = true;
                    IsDuplicateAccount = false;
                }
            }
            if(acc.Id != null){
                setAccId.add(acc.Id);
                lstAcctoShow = savedRecord(setAccId);
                acc = new Proposed_Account__c();
            }
            
        }
        catch(Exception ex){    
        }
        return null;
    }
    
    //new change starts....
    public List<Proposed_Account__c> savedRecord(set<Id> setAccId){
        
        List<Proposed_Account__c> lstAccToShow = new List<Proposed_Account__c>();
        for(Proposed_Account__c eachAcc : [Select Id,Account__c,Selected_Account_Name__c from Proposed_Account__c where Id IN :setAccId]){
            lstAccToShow.add(eachAcc);
            setAccProId.add(eachAcc.Account__c);
        }
        
        display = true; 
        display2 = true;
        return lstAccToShow;
        
    }
    
    public void removeRowFromAccList(){
        if(lstAcctoShow != null && lstAcctoShow.size()> 0){
            system.debug('lstAcctoShow::' + lstAcctoShow);
            Proposed_Account__c eachAcc = lstAcctoShow[rowToRemove -1];
            lstAcctoShow.remove(rowToRemove -1);
            setAccProId.remove(eachAcc.Account__c);
            try{
                delete eachAcc;
                IsProAccSaveSuccessfull = false;
                IsDuplicateAccount = false;
                acc = new Proposed_Account__c();
            }
            catch(Exception ex){ 
            }
        }
        if(lstAcctoShow.size() == 0 ){ 
            display = false; 
            
        }
        
    }
}