/****************************************
* @auther : Himanshu Sharma
* @Used   : Trigger Name : ST_AssetTrigger
* @Functionality : Update Upsell Asset Status when Sales Asset status becomes Inactive
* 
* ***************************************/
public class ST_UpsellAssetTriggerUtilityHandler {
/**
    * @author : Himanshu Sharma
    * @date : 18 Mar, 2019
    * @description : SE-2455 --> Update Upsell Assets as Inactive if corresponding Sales asset becomes Inactive
*/
    public static void updateUpsellAssettoInactive(List<Asset> triggerNew, Map<Id,Asset> triggeroldMap) {
        Id salesRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Map<Id,List<Asset>> mapOppIdtoAsset = new Map<Id,List<Asset>>();
        Map<Id,List<Asset>> mapUpsellOppIdtoAsset = new Map<Id,List<Asset>>();
        List<Asset> lstAssettoUpdate = new List<Asset>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        
        for(Asset eachAsset : triggerNew){
            if(eachAsset.RecordTypeId == salesRecTypeId && eachAsset.Status == 'Inactive' && eachAsset.Opportunity__c != null && 
               (triggeroldMap == null || triggeroldMap.get(eachAsset.Id).Status != eachAsset.Status)){
                   if(!mapOppIdtoAsset.containsKey(eachAsset.Opportunity__c))
                       mapOppIdtoAsset.put(eachAsset.Opportunity__c,new List<Asset>());
                   mapOppIdtoAsset.get(eachAsset.Opportunity__c).add(eachAsset);
               }
        }
        
        if(mapOppIdtoAsset != null && mapOppIdtoAsset.size() > 0){
            for(Opportunity eachOpp : [Select Id,Parent_Opportunity__c,(Select Id,Status,Opportunity__r.Parent_Opportunity__c from Assets__r) from Opportunity where Parent_Opportunity__c IN :mapOppIdtoAsset.keySet() AND RecordType.DeveloperName = 'Upsell']){
                for(Asset eachAsset : eachOpp.Assets__r){
                    if(!mapUpsellOppIdtoAsset.containsKey(eachOpp.Parent_Opportunity__c))
                        mapUpsellOppIdtoAsset.put(eachOpp.Parent_Opportunity__c,new List<Asset>());
                    mapUpsellOppIdtoAsset.get(eachOpp.Parent_Opportunity__c).add(eachAsset);
                }
                lstOpp.add(eachOpp);
            }
        }
        
        if(lstOpp != null && lstOpp.size() > 0){
            for(Opportunity eachOpp : lstOpp){
                if(mapUpsellOppIdtoAsset.containsKey(eachOpp.Parent_Opportunity__c) && mapUpsellOppIdtoAsset.get(eachOpp.Parent_Opportunity__c) != null){
                    lstAssettoUpdate = mapUpsellOppIdtoAsset.get(eachOpp.Parent_Opportunity__c);
                }
            }
        }
        
        if(lstAssettoUpdate != null && lstAssettoUpdate.size() > 0){
            for(Asset eachAsset : lstAssettoUpdate){
                eachAsset.Status = 'Inactive';
            }
        }
        if(lstAssettoUpdate != null && lstAssettoUpdate.size() > 0){
            update lstAssettoUpdate;
        }
    }
}