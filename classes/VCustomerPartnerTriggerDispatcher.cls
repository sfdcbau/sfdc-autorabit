public without sharing class VCustomerPartnerTriggerDispatcher 
{
    Map<Id, Map<Id, Id>> customerPartnerSetMap = new Map<Id, Map<Id, Id>>(); // Map<CustomerId, Map<PartnerId, Current CustomerPartner__c>>
    Map<Id, Map<Id, Id>> leadPartnerSetMap = new Map<Id, Map<Id, Id>>(); // Map<LeadId, Map<PartnerId, Current CustomerPartner__c>>
    
    public void dispatch() 
	{
        if(Trigger.isBefore && Trigger.isInsert)
        {
            checkDuplicate();
            setOnlyOneOpenOpportunity();
        }
        if(Trigger.isBefore && Trigger.isUpdate)
        {
            checkDuplicate();
            setMostRecentOpenOpportunity();
            
        }
        if(Trigger.isAfter && Trigger.isInsert)
        {
            addPartnerParent();
            setupPartnerContacts();
            setupPartnerOpportunities();
        }
        
        if(Trigger.isAfter && Trigger.isUpdate)
        {
            setupPartnerContacts();
            setupPartnerOpportunities();
        }
        if(Trigger.isAfter && Trigger.isUndelete)
        {
            setupPartnerContacts();
            setupPartnerOpportunities();
        }
    }
    
    /*
        Check for existing customer-partner.
        
        Notes:
            - Need to check for combination of Lead-Customer-Partner values for Lead Customer-Partner. 
                KEY: LeadId_CustomerId_PartnerId
            - Need to check for combination of Customer-Partner values for Account Customer-Partner.
                KEY: CustomerId_PartnerId
                EXECPTION: Lead Customer record.
    */
    private void checkDuplicate()
    {
        
        String errormsg = '';
        Set<Id> leadSet = new Set<Id>();
        Set<Id> customerSet = new Set<Id>();
        Set<String> leadcustomerpartnerSet = new Set<String>();
        Set<String> customerpartnerSet = new Set<String>();
        
        for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
        {
            Boolean check = false;
            
            if(Trigger.isInsert)
            {
                check = true;
            }
            else
            {
                CustomerPartner__c old = (CustomerPartner__c)Trigger.oldMap.get(cp.Id);
                
                if( (cp.Lead__c != old.Lead__c) || (cp.Customer__c != old.Customer__c) || (cp.Partner__c != old.Partner__c) )
                {
                    check = true;
                }
            }
            
            if(check)
            {
                // Collect customer partners
                if(cp.Customer__c != null) 
                {
                    if(!customerSet.contains(cp.Customer__c))
                    {
                        customerSet.add(cp.Customer__c);
                    }
                }
                
                // Collect lead partners
                if(cp.Lead__c != null) 
                {
                    if(!leadSet.contains(cp.Lead__c))
                    {
                        leadSet.add(cp.Lead__c);
                    }
                    
                    // Collect key - Combination of LeadId, CustomerId, PartnerId
                    String leadKey = String.valueOf(cp.Lead__c) + '_' + String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
                    if(!leadcustomerpartnerSet.contains(leadKey))
                    {
                        leadcustomerpartnerSet.add(leadKey);
                    }
                }
                
                // Collect key - Combination of CustomerId, PartnerId
                String accountKey = String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
                if(!customerpartnerSet.contains(accountKey))
                {
                    customerpartnerSet.add(accountKey);
                }
            }
        }
        
        System.debug(leadcustomerpartnerSet);
        System.debug(customerpartnerSet);
        
        // Validate lead customer partners
        if(!leadcustomerpartnerSet.isEmpty())
        {
            for(CustomerPartner__c cp : [Select Id, Lead__c, Lead__r.Name, Customer__c, Customer__r.Name, Partner__c, Partner__r.Name From CustomerPartner__c Where Lead__c In :leadSet Or Customer__c In :customerSet])
            {
                String key = String.valueOf(cp.Lead__c) + '_' + String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
                
                System.debug('#################### LEAD KEY: '+key);
                
                if(leadcustomerpartnerSet.contains(key))
                {
                    errormsg += 'Duplicate Customer-Partner: {Lead=' + cp.Lead__r.Name + ', Customer=' + cp.Customer__r.Name + ', Partner=' + cp.Partner__r.Name + '}\n';
                } 
            }
        }
        
        // Validate customer partners
        if(!customerpartnerSet.isEmpty())
        {
            for(CustomerPartner__c cp : [Select Id, Customer__c, Customer__r.Name, Partner__c, Partner__r.Name From CustomerPartner__c Where Customer__c In :customerSet And Customer__c <> :Static_ID__c.getInstance('Account_Lead_Customer').RecordID__c])
            {
                String key = String.valueOf(cp.Customer__c) + '_' + String.valueOf(cp.Partner__c);
                
                System.debug('#################### ACCOUNT KEY: '+key);
                
                if(customerpartnerSet.contains(key))
                {
                    errormsg += 'Duplicate Customer-Partner: {Customer=' + cp.Customer__r.Name + ', Partner=' + cp.Partner__r.Name + '}\n';
                } 
            }
        }
        
        // Attach error message to trigger record
        if(errormsg != '')
        {
            Trigger.new[0].addError(errormsg);
        }
        
    }
    
    /*
        Add Partner Parent when Customer-Partner is added.
        
        Notes:
            - Add Partner account Parent. This will create another trigger to add the next parent in the hierarchy.
            - Avoid duplicate records.
    */
    private void addPartnerParent()
    {
        List<CustomerPartner__c> insertCustomerPartnerList = new List<CustomerPartner__c>();
        
        // Collect parent, parent.parent accounts
        for(CustomerPartner__c cp : [Select Id, Customer__c, Lead__c, Partner__c, Partner__r.ParentId, Partner__r.Parent.ParentId, Status__c From CustomerPartner__c 
                                     Where Id In :(List<CustomerPartner__c>)Trigger.new])
        {
            // Collect lead partners
            if(cp.Lead__c != null) 
            {
                addLeadPartnerSetMap(cp.Lead__c, cp.Partner__r.ParentId, cp.Id);
            }
            
            // Collect customer partners
            if(cp.Customer__c != null) 
            {
                String leadcustomerAccountId = Static_ID__c.getInstance('Account_Lead_Customer').RecordID__c;
                if(cp.Customer__c != leadcustomerAccountId && String.valueOf(cp.Customer__c).substring(0,15) != leadcustomerAccountId)
                {
                    addCustomerPartnerSetMap(cp.Customer__c, cp.Partner__r.ParentId, cp.Id);
                }
            }
        }
        
        // Validate customer partners
        if(!customerPartnerSetMap.isEmpty())
        {
            // Remove existing customer partners from the set
            for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c From CustomerPartner__c Where Customer__c In :customerPartnerSetMap.keySet()])
            {
                if(customerPartnerSetMap.get(cp.Customer__c).containsKey(cp.Partner__c))
                {
                    customerPartnerSetMap.get(cp.Customer__c).remove(cp.Partner__c);
                }
            }
            
            // Collect customer partners to insert
            for(Id customerId : customerPartnerSetMap.keySet())
            {
                for(Id partnerId : customerPartnerSetMap.get(customerId).keySet())
                {
                    CustomerPartner__c currentcp = (CustomerPartner__c)Trigger.newMap.get(customerPartnerSetMap.get(customerId).get(partnerId));
                    
                    CustomerPartner__c newcp = new CustomerPartner__c();
                    newcp.Customer__c = customerId;
                    newcp.Partner__c = partnerId;
                    newcp.Status__c = currentcp.Status__c;
                    newcp.Inbound_Referral__c = 'No or Unknown';
                    insertCustomerPartnerList.add(newcp);
                }
            }
        }
        
        // Validate lead partners
        if(!leadPartnerSetMap.isEmpty())
        {		
            // Remove existing lead partners from the set
            for(CustomerPartner__c cp : [Select Id, Lead__c, Partner__c From CustomerPartner__c Where Lead__c In :leadPartnerSetMap.keySet()])
            {
                if(leadPartnerSetMap.get(cp.Lead__c).containsKey(cp.Partner__c))
                {
                    leadPartnerSetMap.get(cp.Lead__c).remove(cp.Partner__c);
                }
            }
            
            // Collect customer partners to insert
            for(Id leadId : leadPartnerSetMap.keySet())
            {
                for(Id partnerId : leadPartnerSetMap.get(leadId).keySet())
                {
                    CustomerPartner__c currentcp = (CustomerPartner__c)Trigger.newMap.get(leadPartnerSetMap.get(leadId).get(partnerId));
                    
                    CustomerPartner__c newcp = new CustomerPartner__c();
                    newcp.Lead__c = leadId;
                    newcp.Customer__c = currentcp.Customer__c;
                    newcp.Partner__c = partnerId;
                    newcp.Status__c = currentcp.Status__c;
                    newcp.Inbound_Referral__c = 'No or Unknown';
                    insertCustomerPartnerList.add(newcp);
                }
            }
        }
        
        // Insert customer partners
        if(!insertCustomerPartnerList.isEmpty())
        {
            insert insertCustomerPartnerList;
        }
    }
    
    /*
    	Function to add partner Id to the customerPartnerSetMap
    */
    private void addCustomerPartnerSetMap(Id customerId, Id accountId, Id currentcpId)
    {
        if(accountId != null)
        {
            if(!customerPartnerSetMap.containsKey(customerId))
            {
                customerPartnerSetMap.put(customerId, new Map<Id, Id>());
            }
            
            if(!customerPartnerSetMap.get(customerId).containsKey(accountId))
            {
                customerPartnerSetMap.get(customerId).put(accountId, currentcpId);
            }
        }
    }
    
    /*
		Function to add partner Id to the leadPartnerSetMap
	*/
    private void addLeadPartnerSetMap(Id leadId, Id accountId, Id currentcpId)
    {
        if(accountId != null)
        {
            if(!leadPartnerSetMap.containsKey(leadId))
            {
                leadPartnerSetMap.put(leadId, new Map<Id, Id>());
            }
            
            if(!leadPartnerSetMap.get(leadId).containsKey(accountId))
            {
                leadPartnerSetMap.get(leadId).put(accountId, currentcpId);
            }
        }
    }
    
    /*
		Notes:
			- If opportunity is blank and there only 1 open opportunity associated to the Customer
	*/
    private void setOnlyOneOpenOpportunity()
    {
        List<CustomerPartner__c> updateCustomerPartnerList = new List<CustomerPartner__c>();
        Map<Id, List<Opportunity>> accountOpenOpportunityListMap = new Map<Id, List<Opportunity>>(); // Map<AccountId, List<Opportunity>>
        Set<Id> customerSet = new Set<Id>();
        
        if(Trigger.isInsert)
        {
            for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
            {
                if(cp.Customer__c != null)
                {
                    updateCustomerPartnerList.add(cp);
                    
                    if(!customerSet.contains(cp.Customer__c))
                        customerSet.add(cp.Customer__c);
                }
            }
        }
        
        if(!updateCustomerPartnerList.isEmpty())
        {
            for(Opportunity opp : [Select Id, AccountId From Opportunity Where AccountId In :customerSet And IsClosed = false Order By CreatedDate Desc])
            {
                if(!accountOpenOpportunityListMap.containsKey(opp.AccountId))
                    accountOpenOpportunityListMap.put(opp.AccountId, new List<Opportunity>());
                
                accountOpenOpportunityListMap.get(opp.AccountId).add(opp);
            }
            
            for(CustomerPartner__c cp : updateCustomerPartnerList)
            {
                if(accountOpenOpportunityListMap.containsKey(cp.Customer__c))
                {
                    List<Opportunity> oppList = accountOpenOpportunityListMap.get(cp.Customer__c);
                    if(oppList.size() == 1)
                    	cp.Opportunity__c = oppList[0].Id;
                }
            }
        }
    }
    
    /*
		Notes:
			- If a Customer (Account) is added to a Customer-Partner Record, add the Most Recent Open Opportunity to the Opportunity field on the Customer-Partner Record
	*/
    private void setMostRecentOpenOpportunity()
    {
        List<CustomerPartner__c> updateCustomerPartnerList = new List<CustomerPartner__c>();
        Map<Id, Opportunity> accountOpenOpportunityMap = new Map<Id, Opportunity>(); // Map<AccountId, Opportunity>
        Set<Id> customerSet = new Set<Id>();
        
        if(Trigger.isUpdate)
        {
            for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
            {
                CustomerPartner__c old = (CustomerPartner__c)Trigger.oldMap.get(cp.Id);
                
                if(cp.Customer__c != null && cp.Customer__c != old.Customer__c)
                {
                    updateCustomerPartnerList.add(cp);
                    
                    if(!customerSet.contains(cp.Customer__c))
                        customerSet.add(cp.Customer__c);
                }
            }
        }
        
        if(!updateCustomerPartnerList.isEmpty())
        {
            for(Opportunity opp : [Select Id, AccountId From Opportunity Where AccountId In :customerSet And IsClosed = false Order By CreatedDate Desc])
            {
                if(!accountOpenOpportunityMap.containsKey(opp.AccountId))
                    accountOpenOpportunityMap.put(opp.AccountId, opp);
            }
            
            for(CustomerPartner__c cp : updateCustomerPartnerList)
            {
                if(accountOpenOpportunityMap.containsKey(cp.Customer__c))
                {
                    Opportunity opp = accountOpenOpportunityMap.get(cp.Customer__c);
                    cp.Opportunity__c = opp.Id;
                }
                else
                {
                    cp.Opportunity__c = null;
                }
            }
        }
    }
	
    /*
		Create partner contacts for each partner account of the Customer-Partner record.
		Make sure the contact records are updated for these Insert, Update, Delete, and Undelete events.
	*/
    private void setupPartnerContacts()
    {
        Set<Id> customerPartnerSet = new Set<Id>();
        
        if(Trigger.isInsert || Trigger.isUndelete)
        {
            for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
            {
                if(cp.Customer__c != null || cp.Partner__c != null)
                {
                    customerPartnerSet.add(cp.Id);
                }
            }
        }
        else if(Trigger.isUpdate)
        {
            for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
            {
                CustomerPartner__c old = (CustomerPartner__c)Trigger.oldMap.get(cp.Id);
                
                if(cp.Customer__c != old.Customer__c || cp.Partner__c != old.Partner__c)
                {
                    customerPartnerSet.add(cp.Id);
                }
            }
        }
        
        if(!customerPartnerSet.isEmpty())
        	VCustomerPartnerUtil.addCustomerPartnerContacts(customerPartnerSet);
    }
	
    /*
		Create partner contacts for each partner account of the Customer-Partner record.
		Make sure the contact records are updated for these Insert, Update, Delete, and Undelete events.
	*/
    private void setupPartnerOpportunities()
    {
        Set<Id> customerPartnerSet = new Set<Id>();
        
        if(Trigger.isInsert || Trigger.isUndelete)
        {
            for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
            {
                if(cp.Customer__c != null || cp.Partner__c != null)
                {
                    customerPartnerSet.add(cp.Id);
                }
            }
        }
        else if(Trigger.isUpdate)
        {
            for(CustomerPartner__c cp : (List<CustomerPartner__c>)Trigger.new)
            {
                CustomerPartner__c old = (CustomerPartner__c)Trigger.oldMap.get(cp.Id);
                
                if(cp.Customer__c != old.Customer__c || cp.Partner__c != old.Partner__c)
                {
                    customerPartnerSet.add(cp.Id);
                }
            }
        }
        
        if(!customerPartnerSet.isEmpty())
        	VCustomerPartnerUtil.addCustomerPartnerOpportunities(customerPartnerSet);
    }

}