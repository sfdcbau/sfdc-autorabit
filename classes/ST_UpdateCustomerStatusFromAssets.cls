/****************************************
* @auther : Mukul kumar
* @Used   : Trigger Name : ST_AssetTrigger
* @Functionality : Update Customer Status on account when Asset status changed.
* 
* ***************************************/
public class ST_UpdateCustomerStatusFromAssets {
    
    private static Boolean isAccSTPayCusStatusUpdated = false;
    
    /**
    * @author : Aman Gupta
    * @date : 13 Feb, 2019
    * @description : This method is to handle onAfterInsert event of Opportunity trigger.
    */
    public static void onAfterInsert(List<Asset> triggernew, Map<Id,Asset> triggernewMap) {
        //Update Account (field: ST_Payments_Customer_Status__c) for Payment Opportunities on the basis of Opportunity Stage whenever Opportunity record is created, updated.
        //if(!isAccSTPayCusStatusUpdated) {
        //ST_UpdateCustomerStatusFromAssets.updateAccSTPayCusStatus(triggernew);
        //isAccSTPayCusStatusUpdated = true;
        //}
    }
    
    /**
    * @author : Aman Gupta
    * @date : 13 Feb, 2019
    * @description : This method is to handle onAfterUpdate event of Opportunity trigger.
    */
    public static void onAfterUpdate(List<Asset> triggerold, Map<Id,Asset> triggeroldMap, List<Asset> triggernew, Map<Id,Asset> triggernewMap) {
        //Update Account (field: ST_Payments_Customer_Status__c) for Payment Opportunities on the basis of Opportunity Stage whenever Opportunity record is created, updated.
        ST_UpsellAssetTriggerUtilityHandler.updateUpsellAssettoInactive(triggernew,triggeroldMap);
        if(!isAccSTPayCusStatusUpdated) {
            // update record if old value != new value
            List<Asset> assetObjList = new List<Asset> ();
            for(Id assetId : triggernewMap.keySet()) {
                if(triggeroldMap.get(assetId).Status != triggernewMap.get(assetId).Status)
                    assetObjList.add(triggernewMap.get(assetId));
            }
            if(!assetObjList.isEmpty()) {
                ST_UpdateCustomerStatusFromAssets.updateAccSTPayCusStatus(assetObjList);
                //isAccSTPayCusStatusUpdated = true;
            }
        }
    }
    
    public static void Updatecustomerstatus(list<Asset> assetslist){
        
        Trigger_Settings__mdt TriggerFlag = [SELECT 
                                             Label, isActive__c 
                                             FROM Trigger_Settings__mdt 
                                             where Label = 'ST_UpdateCustomerStatusFromAssets' limit 1];
        //Profile SysAdminProfile = [select id from Profile where Name='System Administrator'];
        
        if(TriggerFlag.isActive__c) {
            Id AsstRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
            Set<Id> accountIds = new Set<Id>();
            list<Asset> astlist = new list<Asset>();
            Map<Id, list<Asset>> assetMap = new Map<Id, list<Asset>>();
            list<Account> accountlist = new list<Account>();
            list<Account> updateLiveAcclist = new list<Account>();
            
            for(Asset ast : assetslist) {
                accountIds.add(ast.AccountId);
            }
            if(accountIds.size() > 0){
                astlist     = [SELECT 
                               Id, name, AccountId, Status, RecordTypeId, Account.Customer_Status_picklist__c, Opted_Out_Reason__c 
                               From Asset 
                               Where AccountId IN : accountIds AND RecordTypeId = :AsstRecordTypeId order by lastmodifieddate desc];
                accountlist = [Select 
                               Id, name, Customer_Status_picklist__c, DispositionCode__c 
                               From Account 
                               Where Id IN : accountIds];
            }
            for(Asset ast : astlist) {
                if(assetMap.containsKey(ast.AccountId) && assetMap.get(ast.AccountId) != null) {
                    list<Asset> assetlist = assetMap.get(ast.AccountId);
                    assetlist.add(ast);
                    assetMap.put(ast.AccountId, assetlist);
                }else if(!assetMap.containsKey(ast.AccountId)){
                    assetMap.put(ast.AccountId, new list<asset>{ast});
                }
            }
            for(Account acc : accountlist) {
                integer i = 0, j = 0, k = 0;
                for(Asset ast : assetMap.get(acc.Id)) {
                    
                    if(ast.Status == 'Live & Using') {
                        i = i+1;
                    } else if(ast.Status == 'Live') {
                        j = j+1;
                    } else if(ast.Status == 'Inactive') {
                        k = k+1;
                    }
                    if( i >= 1) {
                        acc.Customer_Status_picklist__c = 'Success';
                    } else if (j >= 1) {
                        acc.Customer_Status_picklist__c = 'Live';
                    } else if(k == assetMap.get(acc.Id).size()) {
                        acc.Customer_Status_picklist__c = 'Customer Lost'; 
                        if(ast.Opted_Out_Reason__c == 'Merger/Acquisition') {
                            acc.DispositionCode__c = ast.Opted_Out_Reason__c;
                            acc.SubDisposition__c = '';
                        }
                    } else {
                        acc.Customer_Status_picklist__c = 'Onboarding';  
                    }
                }
                updateLiveAcclist.add(acc);
            }
            if(updateLiveAcclist.size() > 0) {
                
                update updateLiveAcclist;
                
                
            }
        }
    } 
    
    /**
    * @author : Aman Gupta
    * @date : 13 Feb, 2019
    * @description : SE-1678 --> Update Account (field: ST_Payments_Customer_Status__c) for Payment Assets on the basis of Asset Status whenever Asset record is updated.
    */
    public static void updateAccSTPayCusStatus(List<Asset> assetObjList) {
        Set<Id> accIdSet = new Set<Id> ();
        List<Account> accObjList = new List<Account> ();
        
        // filter Payment Onboardings
        for(Asset assetObj : assetObjList) {
            if(assetObj.RecordTypeId == Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId()) {
                accIdSet.add(assetObj.AccountId);
            }
        }
        
        // fetch Accounts if accIdSet not empty
        if(!accIdSet.isEmpty()) {
            for(Account accObj : [SELECT Id, Name, (SELECT Id, Name, Status FROM Assets WHERE RecordType.DeveloperName = 'Payments' AND Opportunity__r.StageName != 'Closed Lost') FROM Account WHERE Id IN :accIdSet]) {
                if(!accObj.Assets.isEmpty()) {
                    Boolean isCreditCardAssetLive = false;
                    Boolean isCheckAssetLive = false;
                    Integer noOfInActiveAsset = 0;
                    
                    for(Asset assetObj : accObj.Assets) {
                        if(assetObj.Status.equalsIgnoreCase('Live') && assetObj.Name.containsIgnoreCase(Label.ST_Payment_Credit_Card))
                            isCreditCardAssetLive = true;
                        else if(assetObj.Status.equalsIgnoreCase('Live') && assetObj.Name.containsIgnoreCase(Label.ST_Payment_Check))
                            isCheckAssetLive = true;
                        else if(assetObj.Status.equalsIgnoreCase('Inactive'))
                            noOfInActiveAsset = noOfInActiveAsset + 1;
                    }
                    
                    // update Account if isCreditCardAssetLive = true or isCheckAssetLive
                    if(isCreditCardAssetLive || isCheckAssetLive) {
                        if(isCreditCardAssetLive && isCheckAssetLive)
                            accObj.ST_Payments_Customer_Status__c = 'Live (Credit Cards & Checks)';
                        else if(isCreditCardAssetLive)
                            accObj.ST_Payments_Customer_Status__c = 'Live (Credit Cards)';
                        else if(isCheckAssetLive)
                            accObj.ST_Payments_Customer_Status__c = 'Live (Checks)';
                        
                        accObjList.add(accObj);
                    }
                    else if(accObj.Assets.size() == noOfInActiveAsset) {
                        // all Assets are Inactive
                        accObj.ST_Payments_Customer_Status__c = 'Churned';
                        accObjList.add(accObj);
                    }
                }
            }
        }
        
        // update accObjList if not empty
        if(!accObjList.isEmpty()) {
            update accObjList;
            isAccSTPayCusStatusUpdated = true;
        }
    }
    
}