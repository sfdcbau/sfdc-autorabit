/**************************************************************************************************************
    * @author      : Ravi Pureti
    * @date        : 27 Mrach, 2019
    * @description : This method is to handle the OB status between Pre-implementation and Implementation.
    * @test class  : ST_SalesOnboardingHandler
    * @Parent Class: ST_OnboardingTriggerEventHandler
    * @Version     : V 1.0
    * @JIRA Number : SE-2509
   ************************************************************************************************************ */
public class ST_SalesOnboardingHandler {
    
     Public static void updateOnboardingStatus(list<Onboarding__c> triggerNew, Map<Id, Onboarding__c> triggerOldMap){
         List<String> lstOBAccLevel = Label.OBAccLevel.split(',');
         Id obRecordTypeId = SObjectType.Onboarding__c.RecordTypeInfosByName.get('ST Onboarding').getRecordTypeId();
         for(Onboarding__c eachOB : triggerNew){
             //system.debug('eachOB.OB_Configuration_Call_Date__c == null:::' + eachOB.OB_Configuration_Call_Date__c );
             //system.debug('triggerOldMap.get(eachOB.Id).OB_Configuration_Call_Date__c' + triggerOldMap.get(eachOB.Id).OB_Configuration_Call_Date__c);
             if(eachOB.RecordTypeId == obRecordTypeId && String.valueOf(eachOB.Onboarding_Status__c) == 'Pre-Implementation' && lstOBAccLevel.contains(String.valueOf(eachOB.OB_Account_Level__c)) &&
               ((eachOB.OB_Configuration_Call_Date__c == null && triggerOldMap.get(eachOB.Id).OB_Configuration_Call_Date__c != null) || 
                (eachOB.OB_Configuration_Call_Date__c != null && triggerOldMap.get(eachOB.Id).OB_Configuration_Call_Date__c == null))){ 
                    eachOB.Onboarding_Status__c = 'Implementation';
             }
         }
     }
}