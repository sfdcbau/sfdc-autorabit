global class SalesWeeklyAEForecast implements Schedulable {
   global void execute(SchedulableContext sc) {
       sendReport();
   }
    global Static void sendReport(){
        Map<String,List<Opportunity>> maplastWeekOpp = new Map<String,List<Opportunity>>();
       Map<String,List<Opportunity>> mapnextWeekOpp = new Map<String,List<Opportunity>>();
        Set<String> setEmail = new Set<String>();
   	   Date currentDate = system.today();
	   List<Opportunity> lastWeekOpportunityList = [SELECT Id,Name,Owner.Name,Owner.email,CloseDate,AE_Confidence__c FROM Opportunity WHERE (CloseDate >=:currentDate.addDays(-7) AND CloseDate <:currentDate) /*AND AE_Confidence__c = 'High Probability'*/ AND RecordType.Name = 'Sales' AND StageName !='Closed Won'];
       List<Opportunity> nextWeekOpportunityList = [SELECT Id,Name,Owner.Name,Owner.email,CloseDate,AE_Confidence__c FROM Opportunity WHERE (CloseDate <=:currentDate.addDays(6) AND CloseDate >=:currentDate) AND AE_Confidence__c = 'High Probability' AND RecordType.Name = 'Sales' AND StageName !='Closed Won'];
       if(lastWeekOpportunityList != null && !lastWeekOpportunityList.isEmpty()){
           for(Opportunity objOpportunity : lastWeekOpportunityList){
               setEmail.add(objOpportunity.owner.email);
               List<Opportunity> listOpportunity = new List<Opportunity>();
               if(maplastWeekOpp != null && maplastWeekOpp.get(objOpportunity.owner.email) != null){
                   listOpportunity = maplastWeekOpp.get(objOpportunity.owner.email);
               }
               listOpportunity.add(objOpportunity);
               maplastWeekOpp.put(objOpportunity.owner.email,listOpportunity);
           }
       } 
       
       if(nextWeekOpportunityList != null && !nextWeekOpportunityList.isEmpty()){
           for(Opportunity objOpportunity : nextWeekOpportunityList){
               setEmail.add(objOpportunity.owner.email);
               List<Opportunity> listOpportunity = new List<Opportunity>();
               if(mapnextWeekOpp != null && mapnextWeekOpp.get(objOpportunity.owner.email) != null){
                   listOpportunity = mapnextWeekOpp.get(objOpportunity.owner.email);
               }
               listOpportunity.add(objOpportunity);
               mapnextWeekOpp.put(objOpportunity.owner.email,listOpportunity);
           }
       }
       for(String ownerEmail : setEmail){
           sendMail(ownerEmail,JSON.serialize(maplastWeekOpp.get(ownerEmail)),JSON.serialize(mapnextWeekOpp.get(ownerEmail)));
       }
    }
    
    @future(callout=true)
    global static void sendMail(String toMail,String strListOldOpportunity,String strListNewOpportunity){
        List<Opportunity> listOldOpportunity = (List<Opportunity>)JSON.deSerialize(strListOldOpportunity,List<Opportunity>.class);
        List<Opportunity> listNewOpportunity = (List<Opportunity>)JSON.deSerialize(strListNewOpportunity,List<Opportunity>.class);
        OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress where DisplayName = 'noreply_salesforce@servicetitan.com' limit 1];
        //String fromEmail = Label.SalesForcastReport;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {toMail};
        String strBody = '<html><body><p>You have forecasted to close [Total CMRR of deals in High Confidence with close dates this week] by this Friday via the following deals:</p>';
        
        String strNewOpp = '';
        if(listNewOpportunity != null && !listNewOpportunity.isEmpty()){
            strNewOpp += '<br></br>List of Opps with Salesforce links that are in High Confidence with close dates of this week<br></br><table style="border: 1px solid black"> <tr style="border: 1px solid black"> <th style="border: 1px solid black">Opportunity Name</th> <th style="border: 1px solid black">Close Date</th> <th style="border: 1px solid black">AE Confidence</th> </tr>';
            for(Opportunity objOpportunity : listNewOpportunity){ 
                strNewOpp += '<tr style="border: 1px solid black">';
               
                strNewOpp += '<td style="border: 1px solid black"><a href= '+URL.getSalesforceBaseUrl().toExternalForm() + '/' + objOpportunity.id + '> '+objOpportunity.Name+'</a> </td>';
                //strNewOpp += ' <td style="border: 1px solid black">'+objOpportunity.Id+'</td>';
                strNewOpp += '<td style="border: 1px solid black">'+objOpportunity.CloseDate+'</td>';
                strNewOpp += ' <td style="border: 1px solid black">'+objOpportunity.AE_Confidence__c+'</td>';
                strNewOpp += ' </tr>';
            }
            strNewOpp += '</table><br></br>';
        }
        
        strBody += strNewOpp;
        
        String strOldOpp = '';
        if(listOldOpportunity != null && !listOldOpportunity.isEmpty()){
            strOldOpp += '<br></br>Also, below please find a list of any outstanding deals from last week:';
            strOldOpp += '<br></br>List of open Opps with Salesforce links with past close dates<br></br><table style="border: 1px solid black"> <tr style="border: 1px solid black"> <th style="border: 1px solid black">Opportunity Name</th> <th style="border: 1px solid black">Close Date</th> <th style="border: 1px solid black">AE Confidence</th> </tr>';
            for(Opportunity objOpportunity : listOldOpportunity){ 
                strOldOpp += '<tr style="border: 1px solid black">';
                strOldOpp += '<td style="border: 1px solid black"><a href= '+URL.getSalesforceBaseUrl().toExternalForm() + '/' + objOpportunity.id + '> '+objOpportunity.Name+'</a> </td>';
                //strOldOpp += ' <td style="border: 1px solid black">'+objOpportunity.Id+'</td>';
                strOldOpp += '<td style="border: 1px solid black">'+objOpportunity.CloseDate+'</td>';
                strOldOpp += ' <td style="border: 1px solid black">'+objOpportunity.AE_Confidence__c+'</td>';
                strOldOpp += ' </tr>';
            }
            strOldOpp += '</table><br></br>';
        }
        strBody += strOldOpp;
        
        strBody += 'Please update any Opportunities or outstanding close dates by 9:00am today.</body></html>';
        email.setOrgWideEmailAddressId(owa.Id);
        //Contact objConatct = new Contact(LastName='Test',Role__c = 'Owner',email=fromEmail);
        //insert objConatct;
        //email.setWhatId(objConatct.Id);
        email.setToAddresses(to);
        email.setSubject('AE Weekly Forecast Report');
         
        email.setHtmlBody(strBody);
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            //apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
        }
        //delete objConatct;
    }
}