/*
#####################################################
# Created By....................: Aman Gupta
# Created Date................: 14 Feb, 2019
# Last Modified By..........: Vishal Labh
# Last Modified Date......: 16 April, 2019
# Description...................: All MRR_Tracking__c trigger functionality will be written in "ST_MRRTrackingUtilityHandler" class method and Need to call it from "ST_MRRTrackingEventHandler".
# Test Class...................: ST_MRRTrackingTriggerTest
#####################################################
*/
public class ST_MRRTrackingUtilityHandler {
    /**
* @author : Aman Gupta
* @date : 18 Feb, 2019
* @description : SE-1560 --> Update Account (field: CC_Payment_MRR__c, Check_Payment_MRR__c) with total Check and Credit Card Payment MMR whenever MRR_Tracking__c record is created, updated, deleted.
                 SE-2623 --> Update Account (field: CC_Payment_MRR__c, Check_Payment_MRR__c) with Most current month MRR whenever MRR_Tracking__c record is created, updated, deleted.
*/
    public static void countSumofMRR(List<MRR_Tracking__c> mrrTrackObjList){
        System.debug('mrrTrackObjList = ' + mrrTrackObjList);
        
        Set<Id> assIdSet = new Set<Id>();
        if(!mrrTrackObjList.isEmpty()){
            for(MRR_Tracking__c mrrTrackObj : mrrTrackObjList){
                if(mrrTrackObj.RecordTypeId == Schema.SObjectType.MRR_Tracking__c.getRecordTypeInfosByName().get('Payments').getRecordTypeId())
                    assIdSet.add(mrrTrackObj.Asset__c); 
            }
        }
        
        Set<Id> accIdSet = new Set<Id>();
        // fetch AccountId for Assets
        for(Asset assObj : [SELECT Id, Name, AccountId FROM Asset WHERE Id IN :assIdSet]) {
            accIdSet.add(assObj.AccountId);
        }
        
        List<Account> accObjList = new List<Account> ();
        Date todayDate = system.today();
        Map<String, Integer> monthMap = new Map<String, Integer> {'JAN' => 1, 'FEB' => 2, 'MAR' => 3, 'APR' => 4, 'MAY' => 5, 'JUN' => 6, 'JUL' => 7, 'AUG' => 8, 'SEP' => 9, 'OCT' => 10, 'NOV' => 11, 'DEC' => 12};
            // Account Id and related Asset List
            Map<Id, List<Asset>> accIdAssetListMap = new Map<Id, List<Asset>> ();
        
        // fetch and process all Asset records associated with accIdSet
        for(Asset assObj : [SELECT Id, Product2.Name, AccountId, (Select Id, MRR__c, Year__c, Month__c, Asset__c, Asset__r.AccountId from Asset_MRR_Tracking__r) FROM Asset WHERE AccountId IN : accIdSet AND RecordType.DeveloperName = 'Payments']) {
            if(!accIdAssetListMap.containsKey(assObj.AccountId))
                accIdAssetListMap.put(assObj.AccountId, new List<Asset> {assObj});
            else
                accIdAssetListMap.get(assObj.AccountId).add(assObj);
        }
        
        //Updated by vishal for SE-2623
        // iterate accIdAssetListMap
        for(Id accId : accIdAssetListMap.keySet()) {
            List<MRR_Tracking__c> ccMRRObjList = new List<MRR_Tracking__c>();
            List<MRR_Tracking__c> chMRRObjList = new List<MRR_Tracking__c>();
            
            for(Asset assetObj : accIdAssetListMap.get(accId)) {
                for(MRR_Tracking__c mrrTrackObj : assetObj.Asset_MRR_Tracking__r) {
                    // checking Product if Credit Card
                    if(String.isNotBlank(assetObj.Product2.Name) && assetObj.Product2.Name.containsIgnoreCase(Label.ST_Payment_Credit_Card)) {
                        // not process MRR_Tracking__c if Year__c or Month__c is null
                        if(mrrTrackObj.Year__c != null && mrrTrackObj.Month__c != null) {
                            // compare current year
                            if(Integer.valueOf(mrrTrackObj.Year__c) == todayDate.year()) {
                                if(ccMRRObjList.isEmpty())
                                    ccMRRObjList.add(mrrTrackObj);
                                else {
                                    if(Integer.valueOf(ccMRRObjList[0].Year__c) == System.today().year()-1){
                                        ccMRRObjList = new List<MRR_Tracking__c> {mrrTrackObj};
                                            }
                                    else {
                                        if(monthMap.get(mrrTrackObj.Month__c) == monthMap.get(ccMRRObjList[0].Month__c))
                                            ccMRRObjList.add(mrrTrackObj);
                                        else if(monthMap.get(mrrTrackObj.Month__c) > monthMap.get(ccMRRObjList[0].Month__c))
                                            ccMRRObjList = new List<MRR_Tracking__c> {mrrTrackObj};
                                                }
                                } 
                            } 
                            // compare previous year
                            if(ccMRRObjList.isEmpty()) {
                                if(Integer.valueOf(mrrTrackObj.Year__c) == System.today().year()-1){
                                    if(ccMRRObjList.isEmpty())
                                        ccMRRObjList.add(mrrTrackObj);
                                    else {
                                        if(monthMap.get(mrrTrackObj.Month__c) == monthMap.get(ccMRRObjList[0].Month__c))
                                            ccMRRObjList.add(mrrTrackObj);
                                        else if(monthMap.get(mrrTrackObj.Month__c) > monthMap.get(ccMRRObjList[0].Month__c))
                                            ccMRRObjList = new List<MRR_Tracking__c> {mrrTrackObj};
                                                }
                                }
                            }
                        }
                    }
                    
                    // checking Product if Check
                    if(String.isNotBlank(assetObj.Product2.Name) && assetObj.Product2.Name.containsIgnoreCase(Label.ST_Payment_Check)) {
                        // not process MRR_Tracking__c if Year__c or Month__c is null
                        if(mrrTrackObj.Year__c != null && mrrTrackObj.Month__c != null) {
                            // compare current year
                            if(Integer.valueOf(mrrTrackObj.Year__c) == todayDate.year()) {
                                if(chMRRObjList.isEmpty())
                                    chMRRObjList.add(mrrTrackObj);
                                else {
                                    if(Integer.valueOf(chMRRObjList[0].Year__c) == System.today().year()-1){
                                        chMRRObjList = new List<MRR_Tracking__c> {mrrTrackObj};
                                            }
                                    else {
                                        if(monthMap.get(mrrTrackObj.Month__c) == monthMap.get(chMRRObjList[0].Month__c))
                                            chMRRObjList.add(mrrTrackObj);
                                        else if(monthMap.get(mrrTrackObj.Month__c) > monthMap.get(chMRRObjList[0].Month__c))
                                            chMRRObjList = new List<MRR_Tracking__c> {mrrTrackObj};
                                                }
                                } 
                            } 
                            // compare previous year
                            if(chMRRObjList.isEmpty()) {
                                if(Integer.valueOf(mrrTrackObj.Year__c) == System.today().year()-1){
                                    if(chMRRObjList.isEmpty())
                                        chMRRObjList.add(mrrTrackObj);
                                    else {
                                        if(monthMap.get(mrrTrackObj.Month__c) == monthMap.get(chMRRObjList[0].Month__c))
                                            chMRRObjList.add(mrrTrackObj);
                                        else if(monthMap.get(mrrTrackObj.Month__c) > monthMap.get(chMRRObjList[0].Month__c))
                                            chMRRObjList = new List<MRR_Tracking__c> {mrrTrackObj};
                                                }
                                }
                            }
                        }
                    }
                }
            }
            
            Account accObj = new Account(Id = accId);
            Decimal totalCreditCardMRR = 0;
            Decimal totalCheckMRR = 0;
            
            // iterate all Credit card MRR_Tracking__c
            for(MRR_Tracking__c mrrTrackObj : ccMRRObjList) {
                totalCreditCardMRR = totalCreditCardMRR + mrrTrackObj.MRR__c;
            }
            
            // iterate all Check MRR_Tracking__c
            for(MRR_Tracking__c mrrTrackObj : chMRRObjList) {
                totalCheckMRR = totalCheckMRR + mrrTrackObj.MRR__c;
            }
            
            accObj.CC_Payment_MRR__c = totalCreditCardMRR;
            accObj.Check_Payment_MRR__c = totalCheckMRR;
            accObjList.add(accObj);
        } 
        
        // updating accList
        if(!accObjList.isEmpty())
            update accObjList;
    }
}