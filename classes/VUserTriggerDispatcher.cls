public without sharing class VUserTriggerDispatcher 
{
	
	public void dispatch() 
	{
		if(Trigger.isAfter && Trigger.isInsert)
			executeAfterInsert();
			
		if(Trigger.isAfter && Trigger.isUpdate)
			executeAfterUpdate();
    }
    
    private void executeAfterInsert()
    {
		associateContactToUser();
    }
    
    private void executeAfterUpdate()
    {
		associateContactToUser();
    }
    
    private void associateContactToUser()
    {
    	List<Id> userIdList = new List<Id>();
    	
    	// Collect user-contact map
    	for(User u : (List<User>)Trigger.new)
    	{
    		if(u.ContactId != null)
    		{
    			userIdList.add(u.Id);
    		}
    	}
    	
    	if(!userIdList.isEmpty())
    	{
    		VUserTriggerDispatcher.associateContactToUserOnFuture(userIdList);
    	}
    }
    
    @future
    private static void associateContactToUserOnFuture(List<Id> userIdList)
    {
    	List<Contact> updateContactList = new List<Contact>();
    	Map<Id, Id> contactUserMap = new Map<Id, Id>();
    	
    	// Collect user-contact map
    	for(User u : [Select Id, ContactId From User Where Id In :userIdList])
    	{
    		if(u.ContactId != null)
    		{
    			contactUserMap.put(u.ContactId, u.Id);
    		}
    	}
    	
    	// Collect contact to be updated
    	for(Contact con : [Select Id, User__c From Contact Where Id In :contactUserMap.keySet()])
    	{
    		if(con.User__c == null)
    		{
    			con.User__c = contactUserMap.get(con.Id);
    			updateContactList.add(con);
    		}
    	}
    	
    	// Update contacts
    	if(!updateContactList.isEmpty())
    	{
    		update updateContactList;
    	}
    }
	
}