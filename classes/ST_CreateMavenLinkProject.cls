/****************************************
* @auther : Mukul kumar
* @Used   : Trigger Name : ST_OnboardingTrigger
* @Functionality : Create MavenLink Project for core product whenever new onboarding record will get created.
* @TesCls        : ST_SendMailToUsersTest

* ***************************************/
public class ST_CreateMavenLinkProject {
    Public static void CreateMavenLink(List<Onboarding__c> onboardinglist) {
        
        Trigger_Settings__mdt TriggerFlag = [SELECT 
                                             Label, isActive__c 
                                             FROM Trigger_Settings__mdt 
                                             where Label = 'ST_CreateMavenLinkProject' limit 1];
        if(TriggerFlag.isActive__c) {
            Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
            Id OnbordingRecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
            list<mavenlink__Mavenlink_Project__c> insertMavenlink = new list<mavenlink__Mavenlink_Project__c>();
            Set<Id> OnbrdId = new Set<Id>();
            Set<Id> OpptyId = new Set<Id>();
            Map<Id, list<String>> coreProductMap = new Map<Id, list<String>>();
            List<Onboarding__c> Onbrdlist = new list<Onboarding__c>();
            List<OpportunityLineItem> oppLineList = new list<OpportunityLineItem>();
            
            for(Onboarding__c onbrding : onboardinglist) { 
                if(onbrding.RecordTypeId == OnbordingRecordTypeId) {
                    OnbrdId.add(onbrding.Id);
                    OpptyId.add(onbrding.Opportunity__c);
                }
            }
            if(OpptyId.size() > 0) {
                oppLineList = [Select 
                               Id, name, OpportunityId, Opportunity.Name, Product2.Name, Opportunity.RecordTypeId 
                               from OpportunityLineItem 
                               where OpportunityId IN : OpptyId AND Opportunity.RecordTypeId = :OppRecordTypeId];
            }
            if(oppLineList.size() > 0) {
                for(OpportunityLineItem oppLine : oppLineList) {
                    if(coreProductMap.containsKey(oppLine.OpportunityId)) {
                        coreProductMap.get(oppLine.OpportunityId).add(oppLine.Product2.Name);
                    } else if(!coreProductMap.containsKey(oppLine.OpportunityId)) {
                        coreProductMap.put(oppLine.OpportunityId, new list<String>{oppLine.Product2.Name});
                    }
                }
            }
            String[] coreProd = Label.CoreProducts.split(',');
            if(OnbrdId.size() > 0) {
                Onbrdlist = [Select 
                             Id, name, Account__c, Account__r.name, Opportunity__c, Onboarder__c, OB_Discovery_Call_Date__c
                             From Onboarding__c
                             Where Id IN : OnbrdId AND name like : coreProd];
            }            
            if(Onbrdlist.size() > 0) {  // added more custom lables SE-3325 
                for(Onboarding__c onbrding : Onbrdlist) {
                    for(String coreProduct : coreProductMap.get(onbrding.Opportunity__c)) {
                        if(coreProduct == label.Essentials_Package || coreProduct == label.Premier_Package
                           || coreProduct == label.Starter_Package || coreProduct == label.Managed_Tech || coreProduct == label.Starter_User
                           || coreProduct == label.Essentials_User || coreProduct == label.Premier_User || coreProduct == label.Cleaning_Package 
                           || coreProduct == label.Growth_Product|| coreProduct == label.Pro_Product|| coreProduct == label.Starter_Product) {
                               mavenlink__Mavenlink_Project__c mavenlink = new mavenlink__Mavenlink_Project__c();
                               mavenlink.Name = onbrding.Account__r.name;
                               mavenlink.mavenlink__Account__c = onbrding.Account__c;
                               mavenlink.mavenlink__Opportunity__c = onbrding.Opportunity__c;
                               mavenlink.Onboarding__c = onbrding.Id;
                               mavenlink.mavenlink__Project_Title__c = onbrding.Account__r.name;
                               mavenlink.mavenlink__Project_Manager__c = onbrding.Onboarder__c;
                               mavenlink.mavenlink__Start_Date__c = onbrding.OB_Discovery_Call_Date__c;
                               mavenlink.mavenlink__Object_Type__c = 'Opportunity';
                               mavenlink.mavenlink__Project_Type__c ='Execution';
                               insertMavenlink.add(mavenlink);
                           }
                    }
                    Break;
                }
            } else {  // Added by Vishal Labh for SE-2856
                for(Onboarding__c onbrding : [Select Id, name, RecordTypeId, Account__c, Account__r.name, Opportunity__c, Onboarder__c, OB_Discovery_Call_Date__c From Onboarding__c Where Id IN : OnbrdId]) { 
                    if(onbrding.RecordTypeId == OnbordingRecordTypeId) {
                        mavenlink__Mavenlink_Project__c mavenlink = new mavenlink__Mavenlink_Project__c();
                        mavenlink.Name = onbrding.Account__r.name;
                        mavenlink.mavenlink__Account__c = onbrding.Account__c;
                        mavenlink.mavenlink__Opportunity__c = onbrding.Opportunity__c;
                        mavenlink.Onboarding__c = onbrding.Id;
                        mavenlink.mavenlink__Project_Title__c = onbrding.Account__r.name;
                        mavenlink.mavenlink__Project_Manager__c = onbrding.Onboarder__c;
                        mavenlink.mavenlink__Start_Date__c = onbrding.OB_Discovery_Call_Date__c;
                        mavenlink.mavenlink__Object_Type__c = 'Opportunity';
                        mavenlink.mavenlink__Project_Type__c ='Execution';
                        insertMavenlink.add(mavenlink);
                    }
                }
            }
            if(insertMavenlink.size() > 0) {
                insert insertMavenlink;
            }
        }
    }
}