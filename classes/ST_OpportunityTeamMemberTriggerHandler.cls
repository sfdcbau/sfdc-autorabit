/*
#####################################################
# Created By....................: Aman Gupta
# Created Date..................: 28 Dec, 2018
# Last Modified By..............: Kamal Singh
# Last Modified Date............: 12 APR, 2019
# Description...................: This is trigger handler class for ST_OpportunityTeamMemberTrigger trigger.
# Test Class....................: ST_OpportunityTeamMemberTriggerTest
# Modified Date ................: 04/03/2019
# Version ......................: V 1.2
# Jira User Story ..............: SE-2911
#####################################################
*/
public class ST_OpportunityTeamMemberTriggerHandler {
    
    private Boolean isTriggerContext = false;
    private Integer batchSize = 0;
    private static Boolean isSEImpactUpdated = false;
    
    /**
* @author : Aman Gupta
* @date : 28 Dec, 2018
* @description : Parameterized Constructor
*/
    public ST_OpportunityTeamMemberTriggerHandler(Boolean isExecuting, Integer size) {
        isTriggerContext = isExecuting;
        batchSize = size;
    }
    
    //*************************** Trigger Event Handler Start ********************************//
    /**
* @author : Aman Gupta
* @date : 28 Dec, 2018
* @description : This method is to handle onAfterInsert event of OpportunityTeamMember trigger.
*/
    public void onAfterInsert(List<OpportunityTeamMember> triggernew, Map<Id,OpportunityTeamMember> triggernewMap) {
        //update SE_Impact__c on Opportunity
        updateOpportunitySEImpact(triggernew);
        isSEImpactUpdated = true;
    }
    
    /**
* @author : Aman Gupta
* @date : 28 Dec, 2018
* @description : This method is to handle onAfterUpdate event of OpportunityTeamMember trigger.
*/
    public void onAfterUpdate(List<OpportunityTeamMember> triggerold, Map<Id,OpportunityTeamMember> triggeroldMap, List<OpportunityTeamMember> triggernew, Map<Id,OpportunityTeamMember> triggernewMap) {
        //update SE_Impact__c on Opportunity
        if(Test.isRunningTest() || !isSEImpactUpdated) {
            updateOpportunitySEImpact(triggernew);
            isSEImpactUpdated = true;
        }
    }
    
    /**
* @author : Aman Gupta
* @date : 28 Dec, 2018
* @description : This method is to handle onAfterDelete event of OpportunityTeamMember trigger.
*/
    public void onAfterDelete(List<OpportunityTeamMember> triggerold, Map<Id,OpportunityTeamMember> triggeroldMap) {
        //update SE_Impact__c on Opportunity
        updateOpportunitySEImpact(triggerold);
        isSEImpactUpdated = true;
    }
    
    /**
* @author : Kamal Singh
* @date : 12 Apr, 2019
* @description : This method is to handle onBeforeInsert event of OpportunityTeamMember trigger.
*/
    
    public void onBeforeInsert(List<OpportunityTeamMember> triggernew, Map<Id,OpportunityTeamMember> triggernewMap) {
        //validation on Opportunity
        addSalesEngineerValidation(triggernew); 
    }
    //*************************** Trigger Event Handler End ********************************//
    
    //*************************** Methods invoked by Event Handler methods Starts ********************************//
    /**
* @author : Aman Gupta
* @date : 31 Dec, 2018
* @description : SE1120 --> Update Opportunity (field: SE_Impact__c) with total no. of Sales Engineers OpportunityTeamMember records whenever OpportunityTeamMember record is created, updated, deleted.
*/
    public void updateOpportunitySEImpact(List<OpportunityTeamMember> oppTMObjList) {
        // check whether to run updateOpportunitySEImpact or not
        List<Trigger_Settings__mdt> trgSetObjList = [SELECT 
                                                     Label, isActive__c 
                                                     FROM Trigger_Settings__mdt 
                                                     WHERE Label = 'ST_UpdateOpportunitySEImpact' LIMIT 1];
        if(!trgSetObjList.isEmpty() && trgSetObjList[0].isActive__c) {
            Set<Id> oppIdSet = new Set<Id>();
            // populating oppIdSet set with Opportunity Id's.
            for(OpportunityTeamMember oppTMObj : oppTMObjList) {
                oppIdSet.add(oppTMObj.OpportunityId);
            }
            
            List<Opportunity> oppObjList = new List<Opportunity>();
            // populating oppObjList list with Opportunity records needs to be updated
            for(Opportunity oppObj : [SELECT Id, SEOwner__c, (SELECT Id from OpportunityTeamMembers WHERE TeamMemberRole = 'Sales Engineer') from Opportunity WHERE Id IN : oppIdSet]) {
                Integer noOfSE = 0;
                if(!oppObj.OpportunityTeamMembers.isEmpty())
                    noOfSE = oppObj.OpportunityTeamMembers.size();
                if(oppObj.SEOwner__c != null)
                    noOfSE = noOfSE + 1;
                oppObj.SE_Impact__c = (noOfSE != 0) ? noOfSE : null;
                oppObjList.add(oppObj);
            }
            // update oppObjList if not null
            if(!oppObjList.isEmpty()) {
                update oppObjList;    
            }
        }
    }
    //*************************** Methods invoked by Event Handler methods Ends ********************************//
    
    /**
* @author : Kamal Singh
* @date : 12 Apr, 2019
* @description : SE-3185 --> Validation on Adding Sales Engineer as Opportunity Team Member.
*/
    public void addSalesEngineerValidation(List<OpportunityTeamMember> triggerNew){ 
        Set<Id> setOppId;
        Map<Id, List<OpportunityTeamMember>> mapIdtoObjTM = new Map<Id, List<OpportunityTeamMember>>();
        Map<Id,Id> mapOppIdtoUserId = new Map<Id,Id>();
        Id salesRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.OppRecordTypeName).getRecordTypeId();
        
        // populating oppIdSet set with Opportunity Id's.
        for(OpportunityTeamMember oppTMObj : triggerNew) {
            setOppId = new Set<Id>();
            setOppId.add(oppTMObj.OpportunityId);
        } 
        if(setOppId != null && setOppId.size() > 0){
            for(Opportunity eachOpp : [SELECT Id, StageName, RecordTypeId, SEOwner__c 
                                       FROM Opportunity 
                                       WHERE Id IN :setOppId AND RecordTypeId = :salesRecordTypeId AND StageName != :label.OppStageClosedWon AND StageName != :label.OppStageClosedLost]){
                                           mapOppIdtoUserId.put(eachOpp.Id, eachOpp.SEOwner__c);
                                       }
        } 
        if(mapOppIdtoUserId != null && mapOppIdtoUserId.size() > 0){
            for(OpportunityTeamMember oppTMObj : triggerNew) {
                if(mapOppIdtoUserId.containsKey(oppTMObj.OpportunityId) && mapOppIdtoUserId.get(oppTMObj.OpportunityId) == null && oppTMObj.TeamMemberRole == label.ST_Team_Role){
                    oppTMObj.addError(label.SalesEngineerValidationError);
                    
                }
            }
        }
        
        if(mapOppIdtoUserId != null && mapOppIdtoUserId.size() > 0){
            for(OpportunityTeamMember oppTMObj : triggerNew) {
                if(mapOppIdtoUserId.containsKey(oppTMObj.OpportunityId) && mapOppIdtoUserId.get(oppTMObj.OpportunityId) != null && oppTMObj.TeamMemberRole == label.ST_Team_Role){
                    String userId = mapOppIdtoUserId.get(oppTMObj.OpportunityId);
                    if(oppTMObj.UserId == userId){
                        oppTMObj.addError(label.SEOwnerValidationError);
                    }
                }
            }
        }
        
    }   
}