@IsTest 
private class VCaseEmailFeedbackCommentTest 
{
	
    @IsTest(SeeAllData=false) 
    public static void testSubmitComment() 
    {
        
        // Insert case
        Case c1 = new Case();
        c1.Status = 'New';
        c1.Origin = 'Chat';
        c1.Description = 'Test';
        c1.Subject = 'Test';
        insert c1;
        
        test.startTest();
        
        PageReference pageref = Page.VCaseEmailFeedbackComment;
        pageref.getParameters().put('id', c1.Id);
        test.setCurrentPage(pageref);
        
        VCaseEmailFeedbackCommentController ctrl = new VCaseEmailFeedbackCommentController();
        ctrl.mycasecomment.CommentBody = 'Hello';
        ctrl.submit();
        
        System.assertEquals(ctrl.isCompleted, true);
        
        test.stopTest();
    }
	
    @IsTest(SeeAllData=false) 
    public static void testSubmitCommentOnError() 
    {
        
        // Insert case
        Case c1 = new Case();
        c1.Status = 'New';
        c1.Origin = 'Chat';
        c1.Description = 'Test';
        c1.Subject = 'Test';
        insert c1;
        
        test.startTest();
        
        PageReference pageref = Page.VCaseEmailFeedbackComment;
        pageref.getParameters().put('id', c1.Id);
        test.setCurrentPage(pageref);
        
        VCaseEmailFeedbackCommentController ctrl = new VCaseEmailFeedbackCommentController();
        
        try
        {
            ctrl.mycasecomment.ParentId = null;
            ctrl.mycasecomment.CommentBody = 'Hello';
        	ctrl.submit();
        }
        catch(Exception ex)
        {}
        
        test.stopTest();
    }
    
}