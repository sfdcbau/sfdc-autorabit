/**
	Test Customer-Partner Clean Up

	Updates:
		- [20161029 SC]: Test Methods have been moved to VCustomerPartnerTriggerTest
 */
@isTest
private class CustomerPartnerCleanUpTest 
{
	/*
    @IsTest(SeeAllData=false) 
    public static void testLeadDeleteCustomerPartner() 
    {
        // Add leads
        List<Lead> leadList = new List<Lead>();
        
		Lead l1 = new Lead();
		l1.Company = 'Test Company 1';
		l1.Firstname = 'User';
		l1.Lastname = 'Test1';
		l1.Status = 'Open';
		l1.Estimated_Total_Potential_Office_Users__c = 1;
		l1.No_of_Technicians__c = 1;
		l1.Disposition_Code__c = 'Unreachable';
		l1.Referred_By__c = 'Test';
		leadList.add(l1);
		
		insert leadList;
		
		// Add accounts
		List<Account> accountList = new List<Account>();
		
		Account acc0 = new Account();
		acc0.Name = 'Lead Customer 28731983';
		acc0.BillingCountry = 'United States';
		accountList.add(acc0);
		
		Account acc1 = new Account();
		acc1.Name = 'Test Customer 827312313';
		acc1.BillingCountry = 'United States';
		accountList.add(acc1);
		
		Account acc2 = new Account();
		acc2.Name = 'Test Partner 28731983';
		acc2.BillingCountry = 'United States';
		accountList.add(acc2);
		
		Account acc3 = new Account();
		acc3.Name = 'Test Partner 98569034';
		acc3.BillingCountry = 'United States';
		accountList.add(acc3);
		
		insert accountList;
		
		Static_ID__c csStatic = new Static_ID__c();
		csStatic.Name = 'Account_Lead_Customer';
		csStatic.RecordName__c = acc0.Name;
		csStatic.RecordID__c = acc0.Id;
		insert csStatic;
		
		// Add customer partners
		List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
		
		CustomerPartner__c cp1 = new CustomerPartner__c();
		cp1.Lead__c = l1.Id;
		cp1.Customer__c = accountList[1].Id;
		cp1.Partner__c = accountList[2].Id;
		customerpartnerList.add(cp1);
		
		CustomerPartner__c cp2 = new CustomerPartner__c();
		cp2.Lead__c = l1.Id;
		cp2.Customer__c = accountList[1].Id;
		cp2.Partner__c = accountList[3].Id;
		customerpartnerList.add(cp2);
		
		insert customerpartnerList;
		
		Test.startTest();
		
		delete leadList;
		
		System.assertEquals(([Select Id From CustomerPartner__c Where Id = :customerpartnerList]).size(), 0);
		
		Test.stopTest();
    }
	
    @IsTest(SeeAllData=false) 
    public static void testCustomerPartnerAddPartnerParent() 
    {	
        // Add leads
        List<Lead> leadList = new List<Lead>();
        
		Lead l1 = new Lead();
		l1.Company = 'Test Company 1';
		l1.Firstname = 'User';
		l1.Lastname = 'Test1';
		l1.Status = 'Open';
		l1.Estimated_Total_Potential_Office_Users__c = 1;
		l1.No_of_Technicians__c = 1;
		l1.Disposition_Code__c = 'Unreachable';
		l1.Referred_By__c = 'Test';
		leadList.add(l1);
		
		insert leadList;
		
		// Add accounts
		List<Account> accountList = new List<Account>();
		
		Account acc0 = new Account();
		acc0.Name = 'Lead Customer 28731983';
		acc0.BillingCountry = 'United States';
		accountList.add(acc0);
		
		Account acc1 = new Account();
		acc1.Name = 'Test Customer 827312313';
		acc1.BillingCountry = 'United States';
		accountList.add(acc1);
		
		Account acc2 = new Account();
		acc2.Name = 'Test Partner 28731983';
		acc2.BillingCountry = 'United States';
		accountList.add(acc2);
		
		Account acc3 = new Account();
		acc3.Name = 'Test Partner 98569034';
		acc3.BillingCountry = 'United States';
		accountList.add(acc3);
		
		insert accountList;
		
		Static_ID__c csStatic = new Static_ID__c();
		csStatic.Name = 'Account_Lead_Customer';
		csStatic.RecordName__c = acc0.Name;
		csStatic.RecordID__c = acc0.Id;
		insert csStatic;
		
		// Insert partner parent
		Account acc3_child = new Account();
		acc3_child.ParentId = accountList[2].Id;
		acc3_child.Name = 'Test Partner 98569034';
		acc3_child.BillingCountry = 'United States';
		insert acc3_child;
		
		// Add customer partners
		List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
		
		CustomerPartner__c cp1 = new CustomerPartner__c();
		cp1.Customer__c = accountList[1].Id;
		cp1.Partner__c = acc3_child.Id;
		customerpartnerList.add(cp1);
		
		CustomerPartner__c cp2 = new CustomerPartner__c();
		cp2.Lead__c = leadList[0].Id;
		cp2.Customer__c = accountList[1].Id;
		cp2.Partner__c = acc3_child.Id;
		customerpartnerList.add(cp2);
		
		insert customerpartnerList;
		
		Test.startTest();
		
		// Check if the customer-partner record is added to the partner parent.
		System.assertNotEquals(([Select Id From CustomerPartner__c Where Id = :accountList[3].Id]).Size(), 1);
		
		// Test duplicate
		try
		{
			CustomerPartner__c cp_dup = new CustomerPartner__c();
			cp_dup.Customer__c = accountList[1].Id;
			cp_dup.Partner__c = acc3_child.Id;
			customerpartnerList.add(cp_dup);
		}
		catch(Exception ex)
		{
			
		}
		
		Test.stopTest();
    }
	
    @IsTest(SeeAllData=false) 
    public static void testAccountMergeCustomerPartner() 
    {	
		// Add accounts
		List<Account> accountList = new List<Account>();
		
		Account acc0 = new Account();
		acc0.Name = 'Lead Customer 28731983';
		acc0.BillingCountry = 'United States';
		accountList.add(acc0);
		
		Account acc1 = new Account();
		acc1.Name = 'Test Customer 827312313';
		acc1.BillingCountry = 'United States';
		accountList.add(acc1);
		
		Account acc2 = new Account();
		acc2.Name = 'Test Customer 92834023';
		acc2.BillingCountry = 'United States';
		accountList.add(acc2);
		
		Account acc3 = new Account();
		acc3.Name = 'Test Partner 28731983';
		acc3.BillingCountry = 'United States';
		accountList.add(acc3);
		
		Account acc4 = new Account();
		acc4.Name = 'Test Partner 98569034';
		acc4.BillingCountry = 'United States';
		accountList.add(acc4);
		
		insert accountList;
		
		Static_ID__c csStatic = new Static_ID__c();
		csStatic.Name = 'Account_Lead_Customer';
		csStatic.RecordName__c = acc0.Name;
		csStatic.RecordID__c = acc0.Id;
		insert csStatic;
		
		// Add customer partners
		List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
		
		CustomerPartner__c cp1 = new CustomerPartner__c();
		cp1.Customer__c = accountList[1].Id;
		cp1.Partner__c = accountList[3].Id;
		customerpartnerList.add(cp1);
		
		CustomerPartner__c cp2 = new CustomerPartner__c();
		cp2.Customer__c = accountList[2].Id;
		cp2.Partner__c = accountList[3].Id;
		customerpartnerList.add(cp2);
		
		CustomerPartner__c cp3 = new CustomerPartner__c();
		cp3.Customer__c = accountList[2].Id;
		cp3.Partner__c = accountList[4].Id;
		customerpartnerList.add(cp3);
		
		insert customerpartnerList;
		
		Test.startTest();
		
		merge accountList[1] accountList[2];
		
		// Check if the customer-partner record is moved to the master customer.
		System.assertEquals(([Select Customer__c From CustomerPartner__c Where Id = :customerpartnerList[1].Id])[0].Customer__c, accountList[1].Id);
		
		Test.stopTest();
    }
	
    @IsTest(SeeAllData=false) 
    public static void testLeadMergeCustomerPartner() 
    {
        // Add leads
        List<Lead> leadList = new List<Lead>();
        
		Lead l1 = new Lead();
		l1.Company = 'Test Company 1';
		l1.Firstname = 'User';
		l1.Lastname = 'Test1';
		l1.Status = 'Open';
		l1.Estimated_Total_Potential_Office_Users__c = 1;
		l1.No_of_Technicians__c = 1;
		l1.Disposition_Code__c = 'Unreachable';
		l1.Referred_By__c = 'Test';
		leadList.add(l1);
		
		Lead l2 = new Lead();
		l2.Company = 'Test Company 1';
		l2.Firstname = 'User';
		l2.Lastname = 'Test2';
		l2.Status = 'Open';
		l2.Estimated_Total_Potential_Office_Users__c = 1;
		l2.No_of_Technicians__c = 1;
		l2.Disposition_Code__c = 'Unreachable';
		l2.Referred_By__c = 'Test';
		leadList.add(l2);
		
		insert leadList;
		
		// Add accounts
		List<Account> accountList = new List<Account>();
		
		Account acc0 = new Account();
		acc0.Name = 'Lead Customer 28731983';
		acc0.BillingCountry = 'United States';
		accountList.add(acc0);
		
		Account acc1 = new Account();
		acc1.Name = 'Test Customer 827312313';
		acc1.BillingCountry = 'United States';
		accountList.add(acc1);
		
		Account acc2 = new Account();
		acc2.Name = 'Test Customer 92834023';
		acc2.BillingCountry = 'United States';
		accountList.add(acc2);
		
		Account acc3 = new Account();
		acc3.Name = 'Test Partner 28731983';
		acc3.BillingCountry = 'United States';
		accountList.add(acc3);
		
		Account acc4 = new Account();
		acc4.Name = 'Test Partner 98569034';
		acc4.BillingCountry = 'United States';
		accountList.add(acc4);
		
		insert accountList;
		
		Static_ID__c csStatic = new Static_ID__c();
		csStatic.Name = 'Account_Lead_Customer';
		csStatic.RecordName__c = acc0.Name;
		csStatic.RecordID__c = acc0.Id;
		insert csStatic;
		
		// Add customer partners
		List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
		
		CustomerPartner__c cp1 = new CustomerPartner__c();
		cp1.Lead__c = leadList[0].Id;
		cp1.Customer__c = accountList[1].Id;
		cp1.Partner__c = accountList[3].Id;
		customerpartnerList.add(cp1);
		
		CustomerPartner__c cp2 = new CustomerPartner__c();
		cp2.Lead__c = leadList[1].Id;
		cp2.Customer__c = accountList[2].Id;
		cp2.Partner__c = accountList[3].Id;
		customerpartnerList.add(cp2);
		
		CustomerPartner__c cp3 = new CustomerPartner__c();
		cp3.Lead__c = leadList[1].Id;
		cp3.Customer__c = accountList[2].Id;
		cp3.Partner__c = accountList[4].Id;
		customerpartnerList.add(cp3);
		
		insert customerpartnerList;
		
		Test.startTest();
		
		merge leadList[0] leadList[1];
		
		// Check if the customer-partner record is moved to the master customer.
		System.assertEquals(([Select Lead__c From CustomerPartner__c Where Id = :customerpartnerList[1].Id])[0].Lead__c, leadList[0].Id);
		
		Test.stopTest();
    }
    */
}