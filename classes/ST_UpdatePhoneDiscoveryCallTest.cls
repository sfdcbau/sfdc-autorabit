/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Sanjeev Kaurav  <skaurav@servicetitan.com>
* @version        1.1
* @created        07-01-2018
* @ClassName      ST_UpdatePhoneDiscoveryCall
* @TestClass      ST_UpdatePhoneDiscoveryCallTest
@Jira UserStroy   SE-1053
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public class ST_UpdatePhoneDiscoveryCallTest{ 
    
     static testmethod void  TestObDiscoveryCalldate(){
        
        
        Account acc = utilityHelperTest.createAccount();
        acc.Phone_Discovery_Call__c =  date.valueOf('2017-08-08');
        insert acc;
        contact con = utilityHelperTest.createContact(acc.id);
        insert con;
        
        Event eve = new Event(ActivityDateTime = date.valueOf('2017-08-08'),subject = 'Phone Discovery Call', whoid = con.id,StartDateTime = date.valueOf('2017-08-08'),EndDateTime = date.valueOf('2017-08-08'));
        insert eve;
        
        list<Event> eveList = new List<Event>();
        eveList.add(eve);
        System.assertEquals( acc.Phone_Discovery_Call__c, eve.ActivityDateTime);
        ST_UpdatePhoneDiscoveryCall.PopulatePhoneDiscoveryCall(eveList);
        
    } 

}