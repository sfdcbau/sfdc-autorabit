/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a handler class for OpportunityTrigger.
*
* This class automatically Update the value for the Account field MostRecentClosedDate which has Opportunity RecordType - Sales and Opp Stage - Onboarding.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Ravindra Savaliya   <rsavaliya@servicetitan.com>
* @version        1.0
* @created        08-23-2018
* @Name           OpptyTriggerHandler 
* @TesClass       EditOpptyFromAccountTest
* @Method         insertHandler(), updateHandler1()
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/public class OpptyTriggerHandler{
    
    public static void updateHandler1(List<Opportunity> oppList){
         List<Opportunity> optList = new List<Opportunity>();
         for(Opportunity optt :oppList)
            {
                if(optt.id != null){            
                     optList.add(optt);           
                }
         }
        
        List <opportunity> opptyupdatelist = OpptyTriggerHandler.updateHandler(optList);
        for(Opportunity optt :oppList){
            if(!opptyupdatelist.isEmpty())
            {
              optt.of_tech__c = opptyupdatelist[0].of_tech__c;
            }
        }
    }

public static void insertHandler(List<Opportunity> oppList)
{ 
    Set<Id> accountIds = new Set<Id>();
    Map<Id,Opportunity> mapOfIdAndOpportunity = new Map<Id,Opportunity>();
    for(Opportunity opt : oppList){
        if(opt.recordtypeid == '0121a0000002PJrAAM')   {
            accountIds.add(opt.accountid);
            /*
            list<opportunity> opptyList = [SELECT Id, Managed_Tech_Quantity__c, Account.Type FROM Opportunity WHERE AccountId =: opt.accountid AND StageName='Closed Won' AND recordtypeid='0121a000000ERIZ' order by CloseDate desc];
             if(opptyList.size() >0){
                opt.Of_Tech__c = opptyList[0].Managed_Tech_Quantity__c ;
            }  
            */
        }
    }       
    for(Opportunity opp : [Select id, accountId, Managed_Tech_Quantity__c from Opportunity where accountId IN :accountIds AND StageName='Closed Won' AND recordtypeid='0121a000000ERIZ' order by CloseDate desc]){
        
        mapOfIdAndOpportunity.put(opp.accountId,opp);
    }
    for(Opportunity opt : oppList){
        if(mapOfIdAndOpportunity.containskey(opt.accountid)){
        opt.Of_Tech__c = mapOfIdAndOpportunity.get(opt.accountId).Managed_Tech_Quantity__c;
        }
    }
    
}
public static list<opportunity> updateHandler(List<Opportunity> oppList)
{
  List<Opportunity> oppstotech = [SELECT Id, Managed_Tech_Quantity__c, Account.Type FROM Opportunity WHERE AccountId =: oppList[0].accountid and RecordType.Name ='Sales' and StageName = 'Closed Won' order by CloseDate desc];
           List<Opportunity> oppstoBeUpdated=[Select Id,Of_Tech__c FROM Opportunity WHERE RecordType.Name ='ServiceTitan Payments' and AccountId = :oppList[0].accountid];
            //List<Opportunity> oppstoBeUpdated
            System.debug('oppstotech before : '+oppstotech);
            List<Opportunity> oppupdatingList = new List<Opportunity>();
            System.debug('oppstoBeUpdated before : '+oppstoBeUpdated);
            if(oppstotech != null && !oppstotech.isEmpty()){
                for(Opportunity op : oppstoBeUpdated){
                    
                 System.debug('op.of_tech__c : '+op.of_tech__c);
                 System.debug('oppstotech[0].Managed_Tech_Quantity__c: '+ oppstotech[0].Managed_Tech_Quantity__c);
                if(op.of_tech__c != oppstotech[0].Managed_Tech_Quantity__c){
                    op.Of_Tech__c=oppstotech[0].Managed_Tech_Quantity__c;
                    oppupdatinglist.add(op);
                    }
                }
            }
            
            System.debug('oppstoBeUpdated after : '+oppstoBeUpdated);
            return oppupdatinglist;
}
}