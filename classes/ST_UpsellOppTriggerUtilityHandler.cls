/**********************************************************
*@author             : Himanshu Sharma
*@Description        : 
*@NewUtility handler : 
						ST_UpsellOppTriggerUtilityHelperTest
*@JIRA               : SE-2424
**********************************************************/

public class ST_UpsellOppTriggerUtilityHandler {
    
    public static Id upsellRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
    
    /**
    * @author : Himanshu Sharma
    * @date : 19 Mar, 2019
    * @description : [SE-2424] --> Update Discovery Call, Proposal Sent and Pricing Negotiation Stage at there respective stages
    */  
    public static void updateCallDatesonRespStages(List<Opportunity> triggerNew, Map<Id,Opportunity> triggerOldMap) {
        List<String> lstOppStages = Label.UpsellOppStageNames.split(',');
        for(Opportunity eachOpp : triggerNew){
            if(eachOpp.RecordTypeId == upsellRTId && lstOppStages.contains(String.valueOf(eachOpp.StageName)) &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Stagename != eachOpp.StageName)){
                   
                   if(eachOpp.StageName == 'Discovery Call'){
                       eachOpp.Discovery_Call_Stage_Date__c= System.today();
                   }
                   else if(eachOpp.StageName == 'Pricing / Negotiation'){
                       eachOpp.Stage_Date_for_Pricing_Negotiation__c = System.today();
                   }
                   else if(eachOpp.StageName == 'Proposal Sent'){
                       eachOpp.Stage_Date_for_Proposal_Sent__c = System.today();
                   }
               }
        }
    }
    
    /**
    * @author : Himanshu Sharma
    * @date : 19 Mar, 2019
    * @description : [SE-2424] --> Change Opp Owner to Sales Opp Owner
    */  
    public static void updateOppOwnerasSalesOppOwner(List<Opportunity> triggerNew, Map<Id,Opportunity> triggerOldMap) {
        Set<Id> setParentOppId = new Set<Id>();
        Map<Id,Id> mapOppidtoOwnerId = new Map<Id,Id>();
        
        for(Opportunity eachOpp : triggerNew){
            system.debug('eachOpp:::' + eachOpp);
            if(eachOpp.Parent_Opportunity__c != null && triggerOldMap == null && eachOpp.RecordTypeId == upsellRTId){
                setParentOppId.add(eachOpp.Parent_Opportunity__c);
            }
        }
        
        if(setParentOppId != null && setParentOppId.size() > 0){
            for(Opportunity eachOpp : [Select Id,OwnerId from Opportunity where Id IN :setParentOppId]){
                mapOppidtoOwnerId.put(eachOpp.Id, eachOpp.OwnerId);
            }
        }
        if(mapOppidtoOwnerId != null && mapOppidtoOwnerId.size() > 0){
            for(Opportunity eachOpp : triggerNew){
                if(mapOppidtoOwnerId.containsKey(eachOpp.Parent_Opportunity__c) && mapOppidtoOwnerId.get(eachOpp.Parent_Opportunity__c) != null){
                    eachOpp.OwnerId = mapOppidtoOwnerId.get(eachOpp.Parent_Opportunity__c);
                }
            }
        }
    }
}