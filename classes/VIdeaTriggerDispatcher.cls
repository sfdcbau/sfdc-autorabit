public without sharing class VIdeaTriggerDispatcher 
{
	
	public void dispatch() 
	{
		if(Trigger.isAfter && Trigger.isUpdate)
			executeAfterUpdate();
    }
    
    private void executeAfterUpdate()
    {
		sendEmailNotifications();
        sendEmailNotificationsOnNewResponse();
    }
	
    /*
		Notify Idea Subscribers on new Idea updates

		Conditions: 
			- [20160714 SC]: If Idea Response_Comment_ID__c is NOT changed, notify only subsribers with 'AllUpdates'.
	*/
	private void sendEmailNotifications()
    {
        if(Email_Template_Setting__c.getInstance('NewIdeaUpdate') != null)
        {
            List<EmailTemplate> emailtemplateList = [Select Id, Subject, HtmlValue, Body From EmailTemplate Where Id = :Email_Template_Setting__c.getInstance('NewIdeaUpdate').EmailTemplateID__c Limit 1];
            if(!emailtemplateList.isEmpty())
            {
                List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
                Map<Id, Set<Id>> ideaRecipientSetMap = new Map<Id, Set<Id>>(); // Map<Idea Id, Set<User Id>>
                Map<Id, Set<Id>> ideaSubscriberSetMap = new Map<Id, Set<Id>>(); // Map<Idea Id, Set<User Id>>
                Map<String, String> stringReplaceMap = new Map<String, String>();
                
                // Collect subscribers
                for(Idea_Subscription__c es : [Select Idea__c, Subscriber__c  From Idea_Subscription__c 
                                               	Where Idea__c In :(List<Idea>)Trigger.new And (Notify_of_Update_Type__c = 'AllUpdates' Or Notify_of_Update_Type__c = null)])
                {
                    if(!ideaSubscriberSetMap.containsKey(es.Idea__c))
                    {
                        ideaSubscriberSetMap.put(es.Idea__c, new Set<Id>());
                    }
                    
                    if(!ideaSubscriberSetMap.get(es.Idea__c).contains(es.Subscriber__c))
                    {
                        ideaSubscriberSetMap.get(es.Idea__c).add(es.Subscriber__c);
                    }
                }
                
                // Collect recipients
                for(Idea i : (List<Idea>)Trigger.new)
                {
                    Idea old = (Idea)Trigger.oldMap.get(i.Id);
                    
                    if(i.Response_Comment_ID__c == old.Response_Comment_ID__c) // Idea Update
                    {
                        if(!ideaRecipientSetMap.containsKey(i.Id))
                        {
                            ideaRecipientSetMap.put(i.Id, new Set<Id>());
                        }
                        
                        // Add subscribers
                        if(ideaSubscriberSetMap.containsKey(i.Id))
                        {
                            for(Id subscriberId : ideaSubscriberSetMap.get(i.Id))
                            {
                                if(!ideaRecipientSetMap.get(i.Id).contains(subscriberId))
                                {
                                    ideaRecipientSetMap.get(i.Id).add(subscriberId);
                                }
                            }           
                        }
                    }
                }
                
                // Collect emails
                if(!ideaRecipientSetMap.isEmpty())
                {
                    stringReplaceMap = new Map<String, String>();
                    
                    for(Id ideaId : ideaRecipientSetMap.keySet())
                    {   
                        Idea myidea = ((Map<Id,Idea>)Trigger.newMap).get(ideaId);
                        
                        stringReplaceMap.put('{!Idea.Id}', myidea.Id);
                        stringReplaceMap.put('{!Idea.Title}', myidea.Title);
                        
                        for(Id userId : ideaRecipientSetMap.get(ideaId))
                        {	
                            String subject = emailtemplateList[0].Subject;
                            String textbody = emailtemplateList[0].Body;
                            String htmlbody = emailtemplateList[0].HtmlValue;
                            
                            for(String k : stringReplaceMap.keySet())
                            {
                                if(stringReplaceMap.get(k) != null)
                                {
                                    subject = subject.replace(k, stringReplaceMap.get(k));
                                    textbody = textbody.replace(k, stringReplaceMap.get(k));
                                    htmlbody = htmlbody.replace(k, stringReplaceMap.get(k));
                                }
                            }
                            
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            if(Email_Template_Setting__c.getInstance('NewIdeaUpdate').Organization_Wide_Email_Address_Id__c != null)
                            {
                                email.setOrgWideEmailAddressId(Email_Template_Setting__c.getInstance('NewIdeaUpdate').Organization_Wide_Email_Address_Id__c);
                            }
                            email.setTargetObjectId(userId);
                            email.setSaveAsActivity(false);
                            email.setTemplateId(emailtemplateList[0].Id);
                            email.setSubject(subject);
                            email.setPlainTextBody(textbody);
                            email.setHtmlBody(htmlbody);
                            emailList.add(email);
                        }
                    }
                }
                
                // Send emails
                if(!emailList.isEmpty())
                {
                    try
                    {
                        Messaging.sendEmail(emailList);
                    }
                    catch(Exception ex)
                    {
                        System.debug('#### Email Error: ' + ex);
                    }
                }  
            }
        }          
    }
    
	
    /*
		Notify Idea Subscribers on new Idea updates

		Conditions: 
			- [20160714 SC]: If Idea Response_Comment_ID__c is changed and not equals to NULL, notify all subscribers.
	*/
	private void sendEmailNotificationsOnNewResponse()
    {
        if(Email_Template_Setting__c.getInstance('NewIdeaResponseUpdate') != null)
        {
            List<EmailTemplate> emailtemplateList = [Select Id, Subject, HtmlValue, Body From EmailTemplate Where Id = :Email_Template_Setting__c.getInstance('NewIdeaResponseUpdate').EmailTemplateID__c Limit 1];
            if(!emailtemplateList.isEmpty())
            {
                List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
                Map<Id, Set<Id>> ideaRecipientSetMap = new Map<Id, Set<Id>>(); // Map<Idea Id, Set<User Id>>
                Map<Id, Set<Id>> ideaSubscriberSetMap = new Map<Id, Set<Id>>(); // Map<Idea Id, Set<User Id>>
                Map<String, String> stringReplaceMap = new Map<String, String>();
                
                // Collect subscribers
                for(Idea_Subscription__c es : [Select Idea__c, Subscriber__c  From Idea_Subscription__c Where Idea__c In :(List<Idea>)Trigger.new])
                {
                    if(!ideaSubscriberSetMap.containsKey(es.Idea__c))
                    {
                        ideaSubscriberSetMap.put(es.Idea__c, new Set<Id>());
                    }
                    
                    if(!ideaSubscriberSetMap.get(es.Idea__c).contains(es.Subscriber__c))
                    {
                        ideaSubscriberSetMap.get(es.Idea__c).add(es.Subscriber__c);
                    }
                }
                
                // Collect recipients
                for(Idea i : (List<Idea>)Trigger.new)
                {
                    Idea old = (Idea)Trigger.oldMap.get(i.Id);
                    
                    if(i.Response_Comment_ID__c != null && i.Response_Comment_ID__c != old.Response_Comment_ID__c) // Product Team Update
                    {
                        if(!ideaRecipientSetMap.containsKey(i.Id))
                        {
                            ideaRecipientSetMap.put(i.Id, new Set<Id>());
                        }
                        
                        // Add subscribers
                        if(ideaSubscriberSetMap.containsKey(i.Id))
                        {
                            for(Id subscriberId : ideaSubscriberSetMap.get(i.Id))
                            {
                                if(!ideaRecipientSetMap.get(i.Id).contains(subscriberId))
                                {
                                    ideaRecipientSetMap.get(i.Id).add(subscriberId);
                                }
                            }           
                        }
                    }
                }
                
                // Collect emails
                if(!ideaRecipientSetMap.isEmpty())
                {
                    stringReplaceMap = new Map<String, String>();
                    
                    for(Id ideaId : ideaRecipientSetMap.keySet())
                    {   
                        Idea myidea = ((Map<Id,Idea>)Trigger.newMap).get(ideaId);
                        
                        stringReplaceMap.put('{!Idea.Id}', myidea.Id);
                        stringReplaceMap.put('{!Idea.Title}', myidea.Title);
                        
                        for(Id userId : ideaRecipientSetMap.get(ideaId))
                        {	
                            String subject = emailtemplateList[0].Subject;
                            String textbody = emailtemplateList[0].Body;
                            String htmlbody = emailtemplateList[0].HtmlValue;
                            
                            for(String k : stringReplaceMap.keySet())
                            {
                                if(stringReplaceMap.get(k) != null)
                                {
                                    subject = subject.replace(k, stringReplaceMap.get(k));
                                    textbody = textbody.replace(k, stringReplaceMap.get(k));
                                    htmlbody = htmlbody.replace(k, stringReplaceMap.get(k));
                                }
                            }
                            
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            if(Email_Template_Setting__c.getInstance('NewIdeaResponseUpdate').Organization_Wide_Email_Address_Id__c != null)
                            {
                                email.setOrgWideEmailAddressId(Email_Template_Setting__c.getInstance('NewIdeaResponseUpdate').Organization_Wide_Email_Address_Id__c);
                            }
                            email.setTargetObjectId(userId);
                            email.setSaveAsActivity(false);
                            email.setTemplateId(emailtemplateList[0].Id);
                            email.setSubject(subject);
                            email.setPlainTextBody(textbody);
                            email.setHtmlBody(htmlbody);
                            emailList.add(email);
                        }
                    }
                }
                
                // Send emails
                if(!emailList.isEmpty())
                {
                    try
                    {
                        Messaging.sendEmail(emailList);
                    }
                    catch(Exception ex)
                    {
                        System.debug('#### Email Error: ' + ex);
                    }
                }  
            }
        }          
    }
    
}