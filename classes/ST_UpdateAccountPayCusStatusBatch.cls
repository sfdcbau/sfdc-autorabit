/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 01 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 01 Mar, 2019
 # Description...................: SE-2198 --> Update Account Payments Customer Status (field: ST_Payments_Customer_Status__c) for for Historical Data.
 # Test Class....................: ST_UpdateAccountPayCusStatusBatchTest
 #####################################################
*/
global class ST_UpdateAccountPayCusStatusBatch implements Database.Batchable<sObject>, Database.Stateful{
    global String query;
	global Set<Id> existAccIdSet;
    
    /**
     * @author : Aman Gupta
     * @date : 01 Mar, 2019
     * @description : Default Constructor.
    */
    global ST_UpdateAccountPayCusStatusBatch() {
        String query = 'SELECT Id, StageName, AccountId FROM Opportunity WHERE RecordType.DeveloperName = \'ServiceTitan_Payments\' ORDER BY LastModifiedDate DESC';
        this.query = query;
		existAccIdSet = new Set<Id> ();
    }
    
    /**
     * @author : Aman Gupta
     * @date : 01 Mar, 2019
     * @description : Parameterized Constructor.
    */
    global ST_UpdateAccountPayCusStatusBatch(String query) {
        this.query = query;
		existAccIdSet = new Set<Id> ();
    }
    
    /**
     * @author : Aman Gupta
     * @date : 01 Mar, 2019
     * @description : Database.Batchable interface start method.
    */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    /**
     * @author : Aman Gupta
     * @date : 01 Mar, 2019
     * @description : Database.Batchable interface execute method.
    */
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        
        Set<Id> accIdSet = new Set<Id> ();
		// iteate all Opportunities
		for(Opportunity oppObj : (List<Opportunity>)scope) {
			// filter non processed Accounts
			if(!existAccIdSet.contains(oppObj.AccountId)) {
				accIdSet.add(oppObj.AccountId);
			}
		}
		
		// populate existAccIdSet
		existAccIdSet.addAll(accIdSet);
		
		// query Accounts
		List<Account> accObjList = [SELECT Id, Name
									,(SELECT Id, StageName FROM Opportunities WHERE RecordType.DeveloperName = 'ServiceTitan_Payments' ORDER BY LastModifiedDate DESC)
									,(SELECT Id, Name, Onboarding_Status__c, Date_of_First_Transaction__c FROM Onboarding__r WHERE RecordType.DeveloperName = 'Payments_Onboarding' AND Opportunity__r.StageName != 'Closed Lost' ORDER BY Date_of_First_Transaction__c)
									,(SELECT Id, Name, Status FROM Assets WHERE RecordType.DeveloperName = 'Payments' AND Opportunity__r.StageName != 'Closed Lost')
									FROM Account
									WHERE (ST_Payments_Customer_Status__c = null OR ST_Payments_Customer_Status__c = '') AND Id IN :accIdSet];
		
		List<Account> updateAccObjList = new List<Account> ();
        
		for(Account accObj : accObjList) {
			String payCusStatus = '';
			Date payCusCreditCardDateOfFirstTrans = null;
			Date payCusCheckDateOfFirstTrans = null;
			
            // checking all Opportunities and update Account ST_Payments_Customer_Status__c accordingly.
            if(!accObj.Opportunities.isEmpty()) {
                Boolean isAllOppClosedLost = true;
				Set<String> oppStageSet = new Set<String> {'Prospect', 'Discovery Call', 'Negotiation / Pending', 'Pending Boarding'};
                for(Opportunity oppObj : accObj.Opportunities) {
                    if(!oppObj.StageName.equalsIgnoreCase('Closed Lost')) {
						if(oppStageSet.contains(oppObj.StageName)) {
							payCusStatus = (oppObj.StageName.equalsIgnoreCase('Negotiation / Pending')) ? 'Negotiation Pending' : oppObj.StageName;
						}
                        isAllOppClosedLost = false;
                        break;
                    }
                }
                // update Account if all Opp's are closed lost
                if(isAllOppClosedLost) {
                    payCusStatus = 'Opted Out';
                }
            }
            
			// checking all Onboardings and update Account ST_Payments_Customer_Status__c accordingly.
            if(!accObj.Onboarding__r.isEmpty()) {
                Boolean isAllOnBDPendSTLive = true;
                Date ccDateOfFirstTrans = null;
                Date cDateOfFirstTrans = null;
                for(Onboarding__c onBDObj : accObj.Onboarding__r) {
                    if(onBDObj.Onboarding_Status__c.equalsIgnoreCase('Pending ST Live')) {
                        if(ccDateOfFirstTrans == null && onBDObj.Name.containsIgnoreCase(Label.ST_Payment_Credit_Card))
                            ccDateOfFirstTrans = onBDObj.Date_of_First_Transaction__c;
                        
                        if(cDateOfFirstTrans == null && onBDObj.Name.containsIgnoreCase(Label.ST_Payment_Check))
                            cDateOfFirstTrans = onBDObj.Date_of_First_Transaction__c;    
                    }
                    else {
                        isAllOnBDPendSTLive = false;
                        break;
                    }
                }
                
                // update Account if all Onboarding's are 'Pending ST Live'
                if(isAllOnBDPendSTLive) {
                    payCusCreditCardDateOfFirstTrans = ccDateOfFirstTrans;
					payCusCheckDateOfFirstTrans = cDateOfFirstTrans;
					payCusStatus = 'Pending ST Live';
                }
            }
            
            // checking all Assets and update Account ST_Payments_Customer_Status__c accordingly.
            if(!accObj.Assets.isEmpty()) {
                Boolean isCreditCardAssetLive = false;
                Boolean isCheckAssetLive = false;
                Integer noOfInActiveAsset = 0;
                
                for(Asset assetObj : accObj.Assets) {
                    if(assetObj.Status.equalsIgnoreCase('Live') && assetObj.Name.containsIgnoreCase(Label.ST_Payment_Credit_Card))
                        isCreditCardAssetLive = true;
                    else if(assetObj.Status.equalsIgnoreCase('Live') && assetObj.Name.containsIgnoreCase(Label.ST_Payment_Check))
                        isCheckAssetLive = true;
                    else if(assetObj.Status.equalsIgnoreCase('Inactive'))
                        noOfInActiveAsset = noOfInActiveAsset + 1;
                }
                
                // update Account if isCreditCardAssetLive = true or isCheckAssetLive
                if(isCreditCardAssetLive || isCheckAssetLive) {
                    if(isCreditCardAssetLive && isCheckAssetLive)
                        payCusStatus = 'Live (Credit Cards & Checks)';
                    else if(isCreditCardAssetLive)
                        payCusStatus = 'Live (Credit Cards)';
                    else if(isCheckAssetLive)
                        payCusStatus = 'Live (Checks)';
                }
                else if(accObj.Assets.size() == noOfInActiveAsset) {
                    // all Assets are Inactive
                    payCusStatus = 'Churned';
                }
            }
			
			if(payCusCreditCardDateOfFirstTrans != null)
				accObj.Credit_Card_Date_of_First_Transaction__c = payCusCreditCardDateOfFirstTrans;
			if(payCusCheckDateOfFirstTrans != null)
				accObj.Check_Date_of_First_Transaction__c = payCusCheckDateOfFirstTrans;
            if(String.isNotBlank(payCusStatus)) {
				accObj.ST_Payments_Customer_Status__c = payCusStatus;
				updateAccObjList.add(accObj);
			}
        }
        
        // update updateAccObjList if not empty
        if(!updateAccObjList.isEmpty())
            update updateAccObjList;
		
    }
    
    /**
     * @author : Aman Gupta
     * @date : 01 Mar, 2019
     * @description : Database.Batchable interface finish method.
    */
    global void finish(Database.BatchableContext bc) {
        // execute any post-processing operations
    }
}