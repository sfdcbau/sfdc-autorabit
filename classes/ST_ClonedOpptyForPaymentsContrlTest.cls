@isTest
Public class ST_ClonedOpptyForPaymentsContrlTest{
    @isTest
    static void Testmeth1(){
        Account A = new Account();
        A.name='test';
        insert A;
        
        Product2 prod = new Product2(Name = 'Platform Payments-Credit Card');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 0, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Platform Payments-Credit Card', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.AccountId = A.id;
        op.name='test';
        op.StageName = 'Prospect';
        op.CloseDate=system.today();
        op.recordTypeId = recTypeId;
        
        insert op;
        
        List<String> lstProdIds = new List<String>();
        lstProdIds.add(prod.Id);
        ST_ClonedOpportunityForPaymentsContrl.cloneOpportunity(op.id,'clone',JSON.serialize(lstProdIds));
        ST_ClonedOpportunityForPaymentsContrl.getProducts();
        ST_ClonedOpportunityForPaymentsContrl.getOppStageName(op.Id);
    }       
}