global without sharing class VEmailMessageClosedCaseAlert 
{
	
	global class Request
	{
		@InvocableVariable(label='Email Message ID' required=true)
		public Id emailMessageId;
	}
	
    @InvocableMethod(label='Send Closed Case Email Alert')
	global static void sendClosedCaseEmailAlert(List<Request> requests)
    {
        List<EmailTemplate> emailtemplateList = new List<EmailTemplate>();
        String customSettingName = 'NewEmailMessageOnClosedCase';
        String orgWideEmailAddressId = null;
        
        // Get email template setting
        if(Email_Template_Setting__c.getInstance(customSettingName) != null)
        {
            emailtemplateList = [Select Id, Subject, HtmlValue, Body From EmailTemplate Where Id = :Email_Template_Setting__c.getInstance(customSettingName).EmailTemplateID__c Limit 1];
        	
            if(Email_Template_Setting__c.getInstance(customSettingName).Organization_Wide_Email_Address_Id__c != null)
            {
                orgWideEmailAddressId = Email_Template_Setting__c.getInstance(customSettingName).Organization_Wide_Email_Address_Id__c;
            }
        }
        
        if(!emailtemplateList.isEmpty() && !requests.isEmpty())
        {
            try
            {
                List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            
                // For this purpose, we just need to use the first request in the requests list
                List<EmailMessage> emailmessageList = [Select Id, ParentId, Parent.ContactId, FromAddress From EmailMessage Where Id = :requests[0].emailMessageId And Parent.IsClosed = true Limit 1];
                if(!emailmessageList.isEmpty())
                {
                    List<String> emailaddressList = new List<String>();
                	emailaddressList.add(emailmessageList[0].FromAddress);
                	//emailaddressList.add('sulung.chandra@veltig.com');
                    
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    if(orgWideEmailAddressId != null){email.setOrgWideEmailAddressId(orgWideEmailAddressId);}
                    email.setTemplateId(emailtemplateList[0].Id);
                    email.setTreatTargetObjectAsRecipient(false);
                	email.setToAddresses(emailaddressList);
                    
                    if(emailmessageList[0].Parent.ContactId != null)
                    {
                        email.setTargetObjectId(emailmessageList[0].Parent.ContactId);
                        email.setWhatId(emailmessageList[0].ParentId);
                    }
                    else
                    {
                        email.setTargetObjectId(UserInfo.getUserId());
                    }
                    
                    email.setSaveAsActivity(false);
                    emailList.add(email);
                }
                
                if(!emailList.isEmpty())
                {
                    Messaging.sendEmail(emailList);
                }
            }
            catch(Exception ex)
            {
                System.debug('#### Email Error: ' + ex);
                    
                List<String> adminemailaddressList = new List<String>();
                adminemailaddressList.add('sulung.chandra@veltig.com');
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                if(orgWideEmailAddressId != null){email.setOrgWideEmailAddressId(orgWideEmailAddressId);}
                email.setToAddresses(adminemailaddressList);
                email.setSubject('Unable to send New Alert on Closed Case');
                email.setPlainTextBody(ex.getMessage());
                email.setSaveAsActivity(false);
                
                List<Messaging.SingleEmailMessage> erroremailList = new List<Messaging.SingleEmailMessage>();
                erroremailList.add(email);
                
                try
                {
                    Messaging.sendEmail(erroremailList);
                }
                catch(Exception ex2){}  
            }
            
        }
        
    }
    
}