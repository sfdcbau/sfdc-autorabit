public class SvcTitanTestUtils {
    //Test userJSON
    public  String userJSON='{"count": 4, '+
                        '"users": {'+
                        '"8007375": {"email_address": "SVernekar+con1@mavenlink.com","id": "8007375"},'+
                        '"8053275": {"email_address": "svernekar+sso@mavenlink.com","id": "8053275"},'+
                        '"9257175": {"email_address": "svernekar+onb@mavenlink.com","id": "9257175"},'+
                        '"9257165": {"email_address": "svernekar+obMgr@mavenlink.com","id": "9257165"}'+
                        '}}';
                        
        //Test JSON for project tempalte with resource placeholders
        public  String templateJSON='{"count": 1, '+
                                        '"project_templates": {'+
                                           '"1476565": {"title": "My New Template with Resource only","id": "1476565"}'+
                                        '},'+
                                        '"project_template_assignments": {'+
                                          '"1261075": {"name": "Onboarder","id": "1261075"}'+
                                        '}'+
                                    '}';
}