@isTest
private class BatchQualifiedDateUpdateTest {
	static testmethod void  testQualifiedDateUpdate(){
    	Account objAccount = new Account(Name = 'Test Account',Qualified_Date__c = null);
		insert objAccount;
        
        //get standard pricebook
		Id pricebookId = Test.getStandardPricebookId();

		Product2 objProduct = new Product2 (Name='Test Card',Description='Test Product Entry 1',productCode = 'ABC', isActive = true);
		insert objProduct;
		
		PricebookEntry objPricebookEntry = new PricebookEntry (Product2ID=objProduct.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
		insert objPricebookEntry;
		
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();

		Opportunity objOpportunity = new Opportunity (Name='Opp1',StageName='Stage Zero', RecordTypeId= opportunityRecordTypeId, CloseDate=Date.today(),
                                            Pricebook2Id = objPricebookEntry.Pricebook2Id, AccountId = objAccount.id,Discovery_Call_Stage_Date__c = system.today());
		insert objOpportunity;
        
        Test.StartTest();
        Database.executeBatch(new BatchQualifiedDateUpdate());
		Test.StopTest();
    }
}