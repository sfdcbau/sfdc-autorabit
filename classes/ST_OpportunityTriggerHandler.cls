public class ST_OpportunityTriggerHandler{

/** ------------------------------------------------------------------------------------------------
* @author         Himanshu Sharma  
* @version        1.0
* @created        12/28/2018
* @Name           checkContactRoleonOpp 
* @Purpose        method to check if there is no ContactRole on Opportunity with Role Owner/POC and Opportunity is getting closed
* @test class     Test_ST_OpportunityTriggerHandler
* -------------------------------------------------------------------------------------------------+
*/
    public static void checkContactRoleonOpp(List<Opportunity> triggerNew, Map<Id,Opportunity> triggerOldMap){
        Set<Id> setOppId = new Set<Id>();
        Map<Id,List<OpportunityContactRole>> mapOppidtoOppRole = new Map<Id,List<OpportunityContactRole>>(); 
        String allRoles = Label.OppContactRoleforClosedWon;
        List<String> lstContactRoles = new List<String>();
            lstContactRoles = (allRoles.split(','));
        Id SalesRecTypeId;
        
            if(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RecordTypeForOppStage) != null){
                SalesRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RecordTypeForOppStage).getRecordTypeId();
            }
        
        for(Opportunity eachOpp : triggerNew){
            if(SalesRecTypeId != null && eachOpp.RecordTypeId == SalesRecTypeId && string.valueOf(eachOpp.StageName)  == Label.OppStageName && (triggerOldMap == null ||
               triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                setOppId.add(eachOpp.id);
            }
        }
        
        if(setOppId != null && setOppId.size() > 0){
            for(OpportunityContactRole  eachOppRole : [Select Id,Role,OpportunityId from OpportunityContactRole where OpportunityId in : setOppId and Role IN :lstContactRoles]){
                    if(!mapOppidtoOppRole.containsKey(eachOppRole.OpportunityId))
                        mapOppidtoOppRole.put(eachOppRole.OpportunityId,new List<OpportunityContactRole>());
                        mapOppidtoOppRole.get(eachOppRole.OpportunityId).add(eachOppRole);
            }
        }
        
        if(mapOppidtoOppRole != null && mapOppidtoOppRole.size () > 0){
            for(Opportunity eachOpp : triggerNew){
                if(mapOppidtoOppRole.containsKey(eachOpp.Id) && mapOppidtoOppRole.get(eachOpp.Id) != null){
                 // Do Nothing
                }
                else{
                    if(SalesRecTypeId != null && eachOpp.RecordTypeId == SalesRecTypeId && String.valueOf(eachOpp.StageName) == Label.OppStageName){
                        eachOpp.addError(Label.OppClosedWonError);
                    }
                }
            }
        }
        else{
            for(Opportunity eachOpp : triggerNew){
                if(SalesRecTypeId != null && eachOpp.RecordTypeId == SalesRecTypeId && String.valueOf(eachOpp.StageName) == Label.OppStageName){
                    eachOpp.addError(Label.OppClosedWonError);
                }
                
            }
        }
            
    }
}