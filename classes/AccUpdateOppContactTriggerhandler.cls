/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is the handler class for populating all child opportunities of accounts with the primary contact
*
* Every time the primary contact on the account is inserted or updated, this will
* It also assigns backs the created Onboarding asset to the OLI.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Andrew Huynh   <ahuynh@servicetitan.com>
* @version        2.0
* @created        05/10/2018
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘\
* Test Class: AccUpdateOppContactTriggerhandlerTest
*/


public with sharing class AccUpdateOppContactTriggerhandler {
    
    //Getting the new and old values of primary contact on the account through parameters. 
    public static void handleOppContactUpdate(list<account> newAccValues, list<account> oldAccValues){
        boolean handlerSwitch = false;
        List<Trigger_Settings__mdt> TS =  new List<Trigger_Settings__mdt>();
    
        TS = [SELECT Label, isActive__c FROM Trigger_Settings__mdt where Label='ST_AccUpdateOppContact'];
        
        system.debug('TS is ====> ' + TS);
        
        for(Trigger_Settings__mdt triggerSettings: TS){
            if(triggerSettings.isActive__c == true){
                handlerSwitch = true;
            }
        }
        
        system.debug('handlerSwitch ====> ' + handlerSwitch);
        
        if(handlerSwitch == true){
            if(newAccValues != null){
                system.debug('NewList list is ====>' + newAccValues);
                system.debug('OldList list is ====>' + oldAccValues);
                 
                //Establishing lists and to use later in this handler
                map<id,id> OldPrimaryContact = new map<id,id>();
                map<id,id> AccountContactIdMap = new map<id,id>();
                list<opportunity> oppsToUpdate = new list<opportunity>();
                
                if(oldAccValues != null){
                    for(Account oldValues: oldAccValues){
                        OldPrimaryContact.put(oldValues.id,oldValues.Primary_Contact__c);
                    }
                }
                
                //Loop trough new values 
                for(Account newValues: newAccValues){
                    //If Primary contact has changed
                    if(OldPrimaryContact.keyset().size()>0){
                        system.debug('OldPrimaryContact ====> ' + OldPrimaryContact);
                        if(newValues.Primary_Contact__c != OldPrimaryContact.get(newValues.id)){
                                AccountContactIdMap.put(newValues.id, newValues.Primary_Contact__c);
                        }
                    }
                    
                    //If Primary Contact has not changed or is insert
                    If(OldPrimaryContact.keyset() == null){
                        AccountContactIdMap.put(newValues.id, newValues.Primary_Contact__c);
                    }
                }
                    
               
                system.debug('AccountContactIdMap is ====> ' + AccountContactIdMap);
                
                if(AccountContactIdMap.keyset().Size()>0){
                  
                    //Get all child opportunities of account.
                    list<opportunity> relatedOpps = [SELECT ID, AccountId, Primary_Contact__c FROM Opportunity WHERE AccountId IN :AccountContactIdMap.keyset()];
                    
                    //loop through all child opportunities of account
                    if(relatedOpps.size()>0){
                        system.debug('relatedOpps list is ====>' + relatedOpps);
                        
                        for(Opportunity o: relatedOpps){
                             
                            //Setting up new opportunity
                            opportunity updateOpp = new opportunity();
                             
                            updateOpp.id = o.id;
                            updateOpp.Primary_Contact__c = AccountContactIdMap.get(o.AccountId);
                             
                            oppsToUpdate.add(updateOpp);
                             
                         }
                         
                         system.debug('oppsToUpdate list is ====>' + oppsToUpdate);
                         
                         //update related opportunities with new contact
                         try{
                             if(oppsToUpdate.size()>0){
                                 update oppsToUpdate;
                             }
                         } catch(DMLException e){
                             System.debug('An Exception has occured:' + e.getMessage() + '. Please contact your administrator' );
                             //return null;
                         }
                   
                    }//relatedOpps.size()
                
                }//AccountContactIdMap.keyset().size()
               
            }//Accnewmap keyset() 
           
        }//handlerswitch 
        
    }
    
    /**
     * @author : Aman Gupta
     * @date : 27 Dec, 2018
     * @description : SE-1892 --> Update Customer Account (field: Payments_Live_Date__c) first time when ST_Payments_Customer_Status__c is 'Live (Credit Cards)' OR 'Live (Credit Cards & Checks)' OR 'Live (Checks)'.
    */
    public static void updateAccPayLiveDate(List<Account> triggerold, Map<Id, Account> triggeroldMap, List<Account> triggernew, Map<Id, Account> triggernewMap) {
        // update record if old value != new value
        List<String> payCustStatusList = new List<String> {'Live (Credit Cards)', 'Live (Credit Cards & Checks)', 'Live (Checks)'};
            List<Account> accObjList = new List<Account> ();
        for(Id accId : triggernewMap.keySet()) {
            if(triggeroldMap.get(accId).RecordTypeId == SObjectType.Account.RecordTypeInfosByName.get('Customer').getRecordTypeId()
               && triggernewMap.get(accId).RecordTypeId == SObjectType.Account.RecordTypeInfosByName.get('Customer').getRecordTypeId()
               && triggeroldMap.get(accId).ST_Payments_Customer_Status__c != triggernewMap.get(accId).ST_Payments_Customer_Status__c
               && !payCustStatusList.contains(triggeroldMap.get(accId).ST_Payments_Customer_Status__c) 
               && payCustStatusList.contains(triggernewMap.get(accId).ST_Payments_Customer_Status__c)
               && triggeroldMap.get(accId).Payments_Live_Date__c == null)
                triggernewMap.get(accId).Payments_Live_Date__c = System.today();
        }
    }
}