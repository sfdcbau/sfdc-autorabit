/** ------------------------------------------------------------------------------------------------
* @author         Himanshu Sharma  
* @version        1.0
* @created        12/28/2018
* @Name           ST_OpportunityContactRoleValidatorTest
* @Purpose        test class for ST_OpportunityContactRoleValidator
* @Version        1.1
* @Modified Date  04/14/2019
* Jira User Story SE-2960
* -------------------------------------------------------------------------------------------------+
*/

@istest
private class ST_OpportunityContactRoleValidatorTest {

   @testsetup
    static void createData(){
        Account acc=new Account(Name='testAcc');
        acc.billingcity = 'Ahmedabad';
        acc.billingcountry = 'United States';
        acc.billingstate = 'California';
        acc.Industry__c = 'asd';
        acc.No_Known_Active_Partners__c = true;
        acc.Influencer__c ='Yes';
        acc.Residential__c =13.00;
        acc.Time_Zone__c ='Pacific (UTC-8:00)';
        acc.Tenant_ID__c ='11223';
        acc.Tenant_Name__c ='dicnd';
        acc.Tenant_Tags__c ='bluefrog';
        acc.Initial_Username__c ='kjbdcc';
        acc.Influencer_Detail__c ='njjid';
        acc.PlusOrMinus5EstTechs__c = '0-2';
        acc.Estimated_Total_Potential_Office_Users__c = 12;
        acc.Estimated_Total_Potential_Technicians__c = 11;
        acc.CountofOwnerPOCContacts__c =2;
        acc.Pricing_Model__c = '2018';
        acc.Software_newpicklist__c = 'Acowin';
        Insert acc;
        
        Contact con1 = new Contact(FirstName='test', LastName='test1', Email='test@gmail.com', AccountId=acc.Id, Role__c='Accountant');
        insert con1;
        
        RecordType srt = [Select Id, Name from RecordType where Name='Sales' Limit 1];
        Product2 p2 = new product2();
        p2.name = 'prod';
        insert p2;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=p2.id, Pricebook2ID=pricebookId, UnitPrice=50, isActive=true);
        insert pbe1;
        
        Opportunity op1 = new Opportunity (Name='testOpportunity1', of_tech__c=100, AccountId=acc.Id, Onboarding_POC__c=con1.Id, StageName='Stage Zero', Pricebook2Id = pbe1.Pricebook2Id, RecordType=srt, CloseDate=System.today());
        insert op1;
        
    }
    
    
    @istest
    static void testcheckContactRoleonOpp(){
        Account eachAcc = [Select Id from Account LIMIT 1];
        Contact con1 = new Contact(FirstName='test1', LastName='test11', Email='test1@gmail.com', AccountId=eachAcc.Id, Role__c='Accountant');
        insert con1;
        
        Opportunity eachOpp = [Select id,stageName from Opportunity LIMIT 1];
        test.startTest();
        eachOpp.StageName ='SE Vetting';
        update eachOpp; 
        
        try{
            eachOpp.DQ_Rationale__c = 'test';
            eachOpp.StageName ='Closed Won';
            update eachOpp;    
        }
        catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('At least one Owner or POC contact must exist on this Account for the Opportunity to be set to Closed Won. Please go to the appropriate Contact record on this account and update the "Role" field to identify either the Owner or POC.') ? true : false;
        }
        
        
        test.stopTest();
        
        
    }
    
    @istest
    static void checkContactRoleonOppPositiveTest(){
        
        Opportunity eachOpp = [Select id,stageName,DQ_Rationale__c from Opportunity LIMIT 1];
        
        Account eachAcc = [Select Id from Account LIMIT 1];
        
        Contact eachCon = new Contact(FirstName='test2', LastName='test12', Email='test2@gmail.com', AccountId=eachAcc.Id, Role__c='POC');
        insert eachCon;
        
        Product2 p2 = [Select id from Product2 LIMIT 1];
        
        
        test.startTest();
        eachOpp.StageName ='SE Vetting';
        update eachOpp; 
        
        eachOpp.DQ_Rationale__c = 'test';
        eachOpp.StageName ='Closed Won';
        eachOpp.Address_Industry_Verified__c = true;
        eachOpp.OBDiscoveryCallDate__c = system.today();
        eachOpp.ImplementationDate__c = system.today();
        eachOpp.Onboarding_POC__c = eachCon.Id;
        eachOpp.AE_Discovery_POC__c  = eachCon.Id;
        eachOpp.Competing_Software__c = 'Acowin';
        try{
            update eachOpp;    
        }
        catch(Exception ex){
            system.debug('Exception is ' + ex.getMessage());
        }
        test.stopTest();
        
        
    }
}