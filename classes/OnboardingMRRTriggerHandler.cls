/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is the handler class for automatically calculating CMRR on the Onboarding object 
*
* This class updates the CMRR field on the onboarding object when it is inserted or updated 
* For strategic accounts, the class goes from the onboarding object --> account --> parent account -->
opportunity --> opportunity product --> field for price per tech. 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Andrew Huynh   <ahuynh@servicetitan.com>
* @version        1.0
* @created        08/24/2018
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘\
* Test Class: 
*/

public with sharing class OnboardingMRRTriggerHandler{
    
    private static Boolean bool = false;
    
    public static void handleInsertUpdateMRR(list<Onboarding__c> onboardingList){
        List<Trigger_Settings__mdt> TS =  new List<Trigger_Settings__mdt>();
        
        TS = [SELECT Label,isActive__c FROM Trigger_Settings__mdt where Label='ST_OnboardingMRRTriggerHandler' ];
        
        for (Trigger_Settings__mdt sett : TS){
            if(sett.isActive__c == TRUE){
                bool = true;
            }
        }
        
        system.debug('Debug statment Bool ====> ' + bool);
        
        if(bool == true){   
            if(onboardingList.size()>0 && OnboardingMRRTriggerHandlerFirstRun.FirstRun){
                OnboardingMRRTriggerHandlerFirstRun.FirstRun = false;
                system.debug('debug statement, onboarding list is -->' + onboardingList);
                
                list<onboarding__c> strategicOnboardingList= new list<onboarding__c>();
                list<id> accIds = new list<id>();
                list<id> strategicAccIds = new list<id>();
                map<id,id> ObAccMap = new map<id,id>();
                
                for(onboarding__c o: onboardingList){
                    //Onboarding MRR for Strategic account
                    if(o.Parent_Account__c != null){
                        if(o.RecordTypeId == Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId()){
                            strategicOnboardingList.add(o);
                            accIds.add(o.Parent_Account__c);
                            ObAccMap.put(o.id,o.Parent_Account__c);
                        }
                    }
                }
                
                if(accIds.size()>0){
                    for(account a: [SELECT id, recordTypeId FROM Account WHERE Id IN: accIds]){
                        if(a.recordTypeId == Schema.SObjectType.Account.getRecordTypeInfosByName().get('Strategic').getRecordTypeId()){
                            strategicAccIds.add(a.id);
                        }
                    }
                }
                
                //Onboarding MRR for Strategic account
                if(strategicAccIds.size()>0){
                    list<id> stratOppIds = new list<id>();
                    map<id,id> accOppIdMap = new map<id,id>();
                    map<id,decimal> oppPricePerTechMap = new map<id,decimal>();
                    list<onboarding__c> obToUpdate = new list<onboarding__c>();
                    
                    system.debug('DEBUG ====> strategicAccIds are ' + strategicAccIds);
                    
                    for(opportunity o: [SELECT Id, Name, stagename, closeDate, AccountId, RecordTypeId FROM Opportunity WHERE StageName = 'Closed Won' AND AccountId IN: strategicAccIds ORDER BY closeDate DESC LIMIT 1]){
                        system.debug('DEBUG opportunities are ====> ' + o);
                        if(o.RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId()){
                            stratOppIds.add(o.id);
                            accOppIdMap.put(o.accountId,o.id);
                        }
                    }
                    
                    if(stratOppIds.size()>0){
                        for(opportunityLineItem oli: [SELECT Id, unitPrice, quantity, Projected_Quantity__c, product2.name, Opportunity.id, LastModifiedDate FROM OpportunityLineItem WHERE Opportunity.id IN: stratOppIds AND Product2.name = 'Managed Tech' ORDER BY LastModifiedDate LIMIT 1]){
                            system.debug('DEBUG opportunity line items are ====> ' + oli);
                            oppPricePerTechMap.put(oli.Opportunity.id,oli.unitPrice);
                        }
                    }
                    
                    if(oppPricePerTechMap.keyset().size()>0){
                        for(onboarding__c o: strategicOnboardingList){
                            if(o.Parent_Account__c != null){
                                system.debug('DEBUG STATEMENT Parent Account is ====> ' + o.Parent_Account__c);
                                if(o.Number_of_Techs__c != null){
                                    system.debug('DEBUG ====>, the projected MRR is ' + o.Number_of_Techs__c * oppPricePerTechMap.get(accOppIdMap.get(o.Parent_Account__c)));
                                    onboarding__c oUpdate = new onboarding__c();
                                    
                                    oUpdate.id = o.id;
                                    oUpdate.Projected_MRR__c = o.Number_of_Techs__c * oppPricePerTechMap.get(accOppIdMap.get(o.Parent_Account__c));
                                    
                                    obToUpdate.add(oUpdate);
                                }
                                
                                if(o.Number_of_Techs__c == null){
                                    onboarding__c oUpdate = new onboarding__c();
                                    
                                    oUpdate.id = o.id;
                                    oUpdate.Projected_MRR__c = null;
                                    
                                    obToUpdate.add(oUpdate);
                                }
                                
                            }
                        }
                        
                        if(obToUpdate != null){
                            update obToUpdate;
                        }
                    
                    }
                   
                }//Bracket for onboarding MRR strategic 
                
            }
        }
    }

}