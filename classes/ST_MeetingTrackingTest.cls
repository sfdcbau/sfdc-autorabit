@isTest
private class ST_MeetingTrackingTest{

    static testmethod void  TestMeetingPositive(){
    
        //Create Account Record
        Account acc = new Account();
        acc.Name='Test Account' ;
        insert acc;
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
         //Create Opportunity Record
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', Discovery_Call_Date_Time__c = null,Initial_Discovery_Call_Date_Time__c = system.today(),Count_of_Discovery_Call_Reschedules__c  = null,OBDiscoveryCallDate__c = system.today(),RecordTypeId= OppRT, CloseDate=Date.today() ,AccountId = acc.id, ImplementationDate__c= System.today()+2);
        insert opp1;
        
        //Create Event Data
        Event eve = new Event();
        eve.Subject = 'Scheduled Discovery Call';
        eve.ActivityDateTime = System.today() ; 
        eve.ActivityDate = System.today() ; 
        eve.DurationInMinutes = 30;
        eve.whatid= opp1.id;
        eve.type = 'Scheduled Discovery Call';
        insert eve;
        
        System.assertEquals(eve.ActivityDateTime, opp1.Initial_Discovery_Call_Date_Time__c);  //Checking Event's ActivityDateTime and Initital Discovery Call date Time
        System.assertEquals(eve.type, 'Scheduled Discovery Call');
    }
    
    static testmethod void  TestMeetingNegative(){
    
        //Create Account Record
        Account acc = new Account();
        acc.Name='Test Account' ;
        insert acc;
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
         //Create Opportunity Record  Initial_Discovery_Call_Date_Time__c = system.today()
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', Discovery_Call_Date_Time__c = null,Count_of_Discovery_Call_Reschedules__c  = null,OBDiscoveryCallDate__c = system.today(),RecordTypeId= OppRT, CloseDate=Date.today() ,AccountId = acc.id, ImplementationDate__c= System.today()+2);
        insert opp1;
        
        //Create Event Data
        Event eve = new Event();
        eve.Subject = 'Scheduled Discovery Call';
        eve.ActivityDateTime = System.today() ; 
        eve.ActivityDate = System.today() ; 
        eve.DurationInMinutes = 30;
        eve.whatid= opp1.id;
        eve.type = 'Meeting';
        insert eve;
        
        Opportunity opp = [Select id, Initial_Discovery_Call_Date_Time__c, Discovery_Call_Date_Time__c from Opportunity where id = :opp1.id];
        
        System.assertNotEquals(eve.ActivityDateTime, opp.Initial_Discovery_Call_Date_Time__c);  //Checking Event's ActivityDateTime and Initital Discovery Call date Time
        
    }
}