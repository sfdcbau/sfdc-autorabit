/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SvcTitanMavenlinkExtensionTest {

    
        
        
        @TestSetup
        static void setup()
        {
            Profile pf= [Select Id from profile where Name='System Administrator'];
        
            User onboarder = new User(
                    LastName = 'SubbuOBN',
                    UserName = 'svernekar+obnX@mavenlink.com',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    LocaleSidKey = 'en_US',
                    LanguageLocaleKey = 'en_US',
                    EmailEncodingKey = 'ISO-8859-1',
                    ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
                    Alias = 'SubbuOBN',
                    Email = 'svernekar+obn@mavenlink.com',
                    CommunityNickname = 'SubbuOBN',
                    IsActive = false
                );
               
                insert onboarder;
        }
                
      static testMethod void testExecute() {  
            
            user onboarder = [Select id,lastname,username,profileid from user limit 1];
            /*
            User onboardMgr = new User(
                LastName = 'SubbuOBM',
                UserName = 'svernekar+obMgr@mavenlink.com',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'ISO-8859-1',
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
                Alias = 'SubbuOBM',
                Email = 'svernekar+obMgr@mavenlink.com',
                CommunityNickname = 'SubbuOBMgr'
            );
            
            insert onboardMgr;*/
            
        Account seedAcc = new Account(
                name='Test Account',
                BillingCity='Boston',
                Onboarder__c = onboarder.Id,
                //Onboarding_Mgr__c = onboardMgr.Id,
                Account_Level__c = 'Emerging'
                );
        insert seedAcc;
        
        Opportunity seedOpp = new Opportunity(
            name = 'opp',
            AccountId = seedAcc.id,
            stageName = 'Stage Zero', //Internal Testing
//          stageName = '4 - Scope Commitment', //Need to update this with stage from Vizient environment
            Amount = 100000,
            closeDate = Date.today()
        );
        insert seedOpp;
        
              
        
        Product2 seedProduct = new Product2(name='Product',productCode='XX1234');
        insert seedProduct;
        
    id standardPBID = Test.getStandardPricebookId(); 
    
    PricebookEntry seedPBE = new PriceBookEntry(
            Product2Id=seedProduct.Id,
            Pricebook2Id=standardPBID,
          UnitPrice = 3000000.00,
          UseStandardPrice = false,
          IsActive=true);
          
        insert seedPBE;
    
    /*OpportunityLineItem seedOLI = new OpportunityLineItem(
            Description = 'Test OLI',
            OpportunityId = seedOpp.Id,
            Quantity = 1.0,
            UnitPrice = 3000000.00,
            PriceBookEntryId = seedPBE.id,
            );
        
        insert seedOLI;*/
        
       
        
        mavenlink__Mavenlink_Project__c seedProject = new mavenlink__Mavenlink_Project__c(
            Name = 'Test Project Titan OLI',
            mavenlink__Project_Title__c = 'Test Project Titan OLI',
            mavenlink__Account__c = seedAcc.Id,
            mavenlink__Opportunity__c = seedOpp.Id,
            //mavenlink__Related_OLI__c = seedOLI.Id,
            mavenlink__Automatically_Created__c = true,
            mavenlink__Project_Type__c = 'Execution',
            mavenlink__Object_Type__c = 'OLI');
        insert seedProject;
 
         mavenlink__PackageConfiguration__c config = new mavenlink__PackageConfiguration__c(
            name = 'default_config',
            mavenlink__OLI_Opportunity_Stage_Values__c = 'Stage Zero' //Internal Testing
//          mavenlink__OLI_Opportunity_Stage_Values__c = '4 - Scope Commitment'
        );
        insert config;       
        //Test.startTest();
        
        mavenlink.MavenlinkExtensionPointData inputData = new mavenlink.MavenlinkExtensionPointData();
        inputData.putData('Project Type','Execution');
        inputData.putData('Object Type','Opportunity');
        
        List<Id> objectIds = new List<Id>();
        objectIds.add(seedAcc.Id);
        objectIds.add(seedOpp.Id);
        objectIds.add(seedProject.Id);
        
        mavenlink.MavenlinkExtensionPointState inputState = new mavenlink.MavenlinkExtensionPointState('ProjectStartDate','TestClass','TestMethod');
        Object obj = SvcTitanMavenlinkExtension.execute(inputState,inputData,objectIds);
        
        inputState = new mavenlink.MavenlinkExtensionPointState('AfterProjectCreate','TestClass','TestMethod');
        //update seedOpp;
        obj = SvcTitanMavenlinkExtension.execute(inputState,inputData,objectIds);
        
        Opportunity oppTemp=[select AccountId from Opportunity where Id=:seedOpp.id];
        System.debug(oppTemp.AccountId);
        // Test.stopTest();
    }
    
    /*static testMethod void UnitTest(){
            List<String> emails=new List<String>();
            emails.add('abcd@mavenlink.com');
            Test.startTest();
            Map<String, String> tst = SvcTitanMavenlinkExtension.getUserIds(emails);
            Map<String,String> tst2 = SvcTitanMavenlinkExtension.validateTemplateFetchResourceHolders('123');
            Test.stopTest();
    }*/
    
    
    
   
}