global class SvcTitanMavenlinkExtension implements mavenlink.MavenlinkExtensionPoint {

private static string c = 'SvcTitanMavenlinkExtension';

//Custom Exception class
class SvcTitanException extends Exception {
    
}
    //Method executed from within Mavenlink for SalesForce
    global static Object execute(
                mavenlink.MavenlinkExtensionPointState inputState,
                mavenlink.MavenlinkExtensionPointData inputData,
                List<Id> objectIds)
    {
        String m='execute';
        String extensionPointType = inputState.getExtensionPointType();
        String projectType = (String)inputData.getData('Project Type');
        String objectType = (String)inputData.getData('Object Type');
        
        mavenlink.MavenlinkLogExternal.Write(c,m,'Reached execute for objectId size='+objectIds.size());
        
            if (extensionPointType=='AfterProjectCreate' && objectType == 'Opportunity'){
                Boolean afterProjectCreate = AfterProjectCreate(objectType,projectType,objectIds);
            return afterProjectCreate;
            }
            /*
            else if (extensionPointType=='ProjectStartDate' && objectType == 'Opportunity'){
            Date projectStartDate = ProjectStartDate(objectType,projectType,objectIds);
            return projectStartDate;
            }*/else return null;
    }
    

    /*
        function: ProjectStartDate
                Implements custom logic to determine the Project Start Date
    */ 
    /*   
    private static Date ProjectStartDate(String objectType,String projectType,List<Id> objectIds)
    {
            String m = 'ProjectStartDate';
            Date projectStartDate = null;
            try{
                    if(objectType.equals('Opportunity')){
                    mavenlink.MavenlinkLogExternal.Write(c,m,'Reached ProjectStartDate for objectType='+objectType);
                    Id oppId = mavenlink.MavenlinkExternalExtensionPointHelper.getIdofType(objectIds,'Opportunity');
                    
                    Opportunity oppTemp = [select accountid from Opportunity where Id=:oppId];
                    
                    Account accTemp=null;
                    
                    if(oppTemp.accountid!=null){
                            accTemp=[select id, ImplementationDate__c from Account where Id=:oppTemp.Accountid];
                            projectStartDate = accTemp.ImplementationDate__c;
                    }
                    }
            }catch(Exception e){
                mavenlink.MavenlinkLogExternal.Write(c,m,'Error: '+e.getMessage());
            }
        return projectStartDate;
    }
    */
    /*
        Function: AfterProjectCreate
                Implements custom logic to 
                1. Invite additional users to the newly created project
                2. Apply a template and use resource placeholder in Mavenlink to assign Onboarder
                    to the tasks
    */ 
    private static Boolean AfterProjectCreate(String objectType,String projectType,List<Id> objectIds)
    {
            String m='AfterProjectCreate';
            
            try{
                    if(objectType.equals('Opportunity')){
                    //Retrieve emailIds for specific users
                    //Opportunity.Data Manager
                    //Opportunity.Data Engineer
                    //Opportunity.Onboarding Manager
                    //Opportunity.Training Manager
                    //Opportunity.Accounting Manager
                    Id oppId = mavenlink.MavenlinkExternalExtensionPointHelper.getIdofType(objectIds,'Opportunity');
                    
                    mavenlink__Mavenlink_Project__c project = mavenlink.MavenlinkExternalExtensionPointHelper.getProject(objectIds);
                    
                    Opportunity oppTemp = [select AccountId, AccountingManager__c, DataManager__c, DataEngineer__c, DataScheduler__c, TrainingManager__c, OnboardingManager__c, MavenlinkTemplateID__c from Opportunity where Id=:oppId];
                    
                    Account accTemp=null;
                    
                    List<String> userIdEmails = new List<String>();
                    
                    userIdEmails.add(oppTemp.AccountingManager__c);
                    userIdEmails.add(oppTemp.DataManager__c);
                    userIdEmails.add(oppTemp.DataEngineer__c);
                    userIdEmails.add(oppTemp.DataScheduler__c);
                    userIdEmails.add(oppTemp.TrainingManager__c);
                    userIdEmails.add(oppTemp.OnboardingManager__c);
                    
                    if(oppTemp.AccountId!=null){
                            accTemp=[select id, Onboarder__r.email from Account where Id=:oppTemp.Accountid];
                            userIdEmails.add(accTemp.Onboarder__r.email);
                    }
                    
                    //Retrieving Internal Mavenlink Ids for the user Email as a Map<userEmail,MavenlinkInternalUserId>
                    Map<String, String> userIdMap=getUserIds(userIdEmails);
                    
                    //Function call to send add the managers to the Mavenlink Project
                    inviteManagers(oppTemp,userIdMap,project.mavenlink__Mavenlink_Id__c);
                    //inviteManagers(oppTemp,accTemp,project.mavenlink__Mavenlink_Id__c);
                    
                    //Contrusting a map of Resource Place Holder Names in Mavenlink to the users provided in Salesforce
                    // as a Map<'MavenlinkPlaceHolderName',MavenlinkInternalUserId>
                    // The 'MavenlinkPlaceHolderName' should exactly match the Resource PlaceHolders in the Mavenlink Template
                    Map<String,String> resourceHolderUserMap=new Map<String,String>();
                    
                    if(oppTemp.AccountId!=null){
                            resourceHolderUserMap.put('Implementation Specialist',userIdMap.get(accTemp.Onboarder__r.email));
                            resourceHolderUserMap.put('Data Engineer',userIdMap.get(oppTemp.DataEngineer__c));
                            resourceHolderUserMap.put('To Be Assigned (Data)',userIdMap.get(oppTemp.DataScheduler__c));
                    }
                    //Function call to construct the JSON and apply template(project.mavenlink__Mavenlink_Id__c) to Mavenlink
                    //assigning tasks based on mapping in resourceHolderUserMap
                    applyTemplate(oppTemp.MavenlinkTemplateID__c, resourceHolderUserMap,project.mavenlink__Mavenlink_Id__c);
                    
                    }    
            }catch(Exception e){
                mavenlink.MavenlinkLogExternal.Write(c,m,'Errored: '+e.getMessage()); return false;
            }
            
            return true;
    }
    
    /*
            Function: inviteManagers
                    Invoked by AfterProjectCreate to invite additional users to the Mavenlink Project
    */
    public static void inviteManagers(Opportunity oppTemp, Map<String, String> userIdMap, String wsId){
            String m='inviteManagers';
            try{
            //Fetch Mavenlink UserIDs and invite users
                String accMgrInvited=mavenlink.MavenlinkExternalAPI.inviteUser(userIdMap.get(oppTemp.AccountingManager__c),wsId);
                String dataMgrInvited=mavenlink.MavenlinkExternalAPI.inviteUser(userIdMap.get(oppTemp.DataManager__c),wsId);
                String dataEngineerInvited=mavenlink.MavenlinkExternalAPI.inviteUser(userIdMap.get(oppTemp.DataEngineer__c),wsId);
                String dataSchedulerInvited=mavenlink.MavenlinkExternalAPI.inviteUser(userIdMap.get(oppTemp.DataScheduler__c),wsId);
                String trngMgrInvited=mavenlink.MavenlinkExternalAPI.inviteUser(userIdMap.get(oppTemp.TrainingManager__c),wsId);
                String onboardingMgrInvited=mavenlink.MavenlinkExternalAPI.inviteUser(userIdMap.get(oppTemp.OnboardingManager__c),wsId);
            }catch(exception e){
                mavenlink.MavenlinkLogExternal.Write(c,m,'Error: '+e.getMessage());
            }
    }
    
    /*
            Function: getUserIds
                    Invoked by AfterProjectCreate to invite fetch Mavenlink internal userIds using
                    their email addresses
    */
    public static Map<String, String> getUserIds(List<String> userEmails){
        String m = 'getUserIds';
        String userId=null;
        Integer totalUserCount=0;
        Integer totalPages=0;
        Map<String,Object> apiJSON = new Map<String,Object>();
        Map<String, String> userIdMap=new Map<String, String>();
        Set<String> userEmailsSet=new Set<String>(userEmails);
        
        mavenlink.MavenlinkLogExternal.Write(c,m,'Total users to find = '+userEmails.size());
        if (!Test.isRunningTest()){
                apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/users.json?on_my_account=true&per_page=1','GET',null);
        }else{
                apiJSON = (Map<String,Object>) JSON.deserializeUntyped(new SvcTitanTestUtils().userJSON);
                
        }

            totalUserCount=Integer.valueOf(apiJSON.get('count'));
            mavenlink.MavenlinkLogExternal.Write(c,m,'Total User count='+totalUserCount);
            totalPages=(totalUserCount/200)+1;
            mavenlink.MavenlinkLogExternal.Write(c,m,'Total Pages='+totalPages);

        
        for(Integer j=1;j<=totalPages;j++){ //1
            
            if(userEmailsSet.isEmpty()) break; //Do not continue if no users to look for
            
             if (!Test.isRunningTest()){
                    apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/users.json?on_my_account=true&per_page=200&page='+j,'GET',null);
             }else{
                apiJSON = (Map<String,Object>) JSON.deserializeUntyped(new SvcTitanTestUtils().userJSON);
                }
            mavenlink.MavenlinkLogExternal.Write(c,m,'Users API call retrieved='+apiJSON);
                Map<String,Object> usersJSON = (Map<String,Object>) apiJSON.get('users');
                mavenlink.MavenlinkLogExternal.Write(c,m,'Users object retrieved='+usersJSON);
        
            Integer i=0;
            //if (!Test.isRunningTest()){ //2
                for(Object key:usersJSON.keySet()){ //3
                            if(userEmailsSet.isEmpty()) break; //Do not continue if no users to look for
                            
                        Map<String,Object> idJSON = new Map<String,Object>();
                          idJSON =  (Map<String,Object>) usersJSON.get((new List<String>(usersJSON.keySet()))[i]);
                        //mavenlink.MavenlinkLogExternal.Write(c,m,'UserId object retrieved='+idJSON);
                    
                        //mavenlink.MavenlinkLogExternal.Write(c,m,'userids['+i+']='+(String)idJSON.get('id'));
                        for(String userEmail:userEmailsSet){ //4
                                //mavenlink.MavenlinkLogExternal.Write(c,m,'Looking for user '+userEmail);
                            if(idJSON.get('email_address') == userEmail) { //5
                                   mavenlink.MavenlinkLogExternal.Write(c,m,'User '+userEmail+ ' found='+userId);
                                userId = (String)idJSON.get('id');
                                userIdMap.put(userEmail,userId);
                                userEmailsSet.remove(userEmail);
                            } //5
                        } //4
                        i++;
                 } //3
            //} //2
        } //1
       
            return userIdMap;
    }
    

    /*
            Function: applyTemplate
                    Invoked by AfterProjectCreate to 
                    1. verify the Mavenlink template
                    2. Fetch resource placeholders from Mavenlink
                    3. Apply template with Onboarder as the resource
    */
    public static void applyTemplate(String templateID, Map<String,String> resourceHolderUserMap,String wsId){
            String m='applyTemplate';
            try{
                        Map<String,String> resourceHolderIDMap=validateTemplateFetchResourceHolders(templateID);
 
                    if(resourceHolderIDMap.get('count')!=null){//When template is valid Count!=null
                           Map<String,Object> apiJSON = new Map<String,Object>();
    
                        List<String> resourceHolderNames=new List<String>(resourceHolderUserMap.keySet());
                        
                        String resourceJSON=null;
                        String project_template_assignment_mappingsJSON='"project_template_assignment_mappings":[';
                        if (resourceHolderNames!=null && resourceHolderNames.size()>0){//proceed only when there are resources for placeholder
                            for(String resHolderName:resourceHolderNames){
                                    if(resourceHolderIDMap.get(resHolderName)!=null){//proceed when Required placeholder  in template
                                    if(resourceHolderUserMap.get(resHolderName)!=null){//proceed when Required placeholder userId found
                                        resourceJSON='{"resource_id":"'+resourceHolderIDMap.get(resHolderName)+'","user_id":"'+resourceHolderUserMap.get(resHolderName)+'"}';
                                        project_template_assignment_mappingsJSON=project_template_assignment_mappingsJSON+resourceJSON+',';
                                        }else{
                                            throw new SvcTitanException('Insuffient User Data for '+resHolderName+' to apply template');
                                    }
                                }else{
                                        throw new SvcTitanException('Insuffient ResourceHolder Data for  '+resHolderName+' to apply template');
                                    }
                            }
                            project_template_assignment_mappingsJSON=project_template_assignment_mappingsJSON.removeEnd(',')+']';
                            String templateJSON='{"project_tracker_template_id":"'+templateID+'",'+project_template_assignment_mappingsJSON+'}';
                            String wsJSON='{"workspace":'+templateJSON+'}';
                            
                            mavenlink.MavenlinkLogExternal.Write(c,m,'Worskpace JSON='+wsJSON);
                            
                            if (!Test.isRunningTest()){
                                apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/workspaces/'+wsId+'.json','PUT',wsJSON);
                            }
                        }else{
                                throw new SvcTitanException('No User Data to apply template');
                        }
                    }
            }catch(Exception e){
                mavenlink.MavenlinkLogExternal.Write(c,m,'Error on line '+e.getLineNumber()+':'+e.getMessage()); throw e;
            }
        }
    
    
    /*
        Function:validateTemplateFetchResourceHolders(string templateID)
                Invoked by applyTemplate to validate the Mavenlink TemplateID and
                fetch resource placeholer Ids from Mavenlink
                Returns 
                1. Empty Map(size=0) when when invalid templateID
    */
    public static Map<String,String> validateTemplateFetchResourceHolders(string templateID){
            String m='fetchTemplateResourceHolders';
            String templateCount=null;
            Map<String,Object> apiJSON = new Map<String,Object>();
            Map<String,String> resourceHolders=new Map<String, String>();
            
            If (templateID!=null){
                
                mavenlink.MavenlinkLogExternal.Write(c,m,'templateID = '+templateID);
                
                if (!Test.isRunningTest()){
                apiJSON = mavenlink.MavenlinkExternalAPI.callAPI('/project_templates/'+templateID+'.json?per_page=200&include=project_template_assignments','GET',null);
                }else{
                        apiJSON = (Map<String,Object>) JSON.deserializeUntyped(new SvcTitanTestUtils().templateJSON);
                }
                mavenlink.MavenlinkLogExternal.Write(c,m,'Result json'+apiJSON); templateCount=String.valueOf((Integer)apiJSON.get('count'));
            
            If (templateCount!=null && templateCount.equals('1')) {
                    resourceHolders.put('count',templateCount);
                    Map<String,Object> mProjectTemplateAssignments=(Map<String,Object>)apiJSON.get('project_template_assignments');
                    List<String> resourceHolderIDs=new List<String>(mProjectTemplateAssignments.keySet());
                    
                    
                    Integer i=0;
                    
                    for(String key:resourceHolderIDs){
                        Map<String,Object> idJSON = new Map<String,Object>();
                    idJSON =  (Map<String,Object>) mProjectTemplateAssignments.get(resourceHolderIDs[i]);
                    resourceHolders.put((String)idJSON.get('name'),(String)idJSON.get('id'));
                    mavenlink.MavenlinkLogExternal.Write(c,m,'Fetched Resource placeholder '+idJSON.get('name'));
                    i++;
                }
                }
             return resourceHolders;
            }
            
            return null;
    }
    
    
    
}