/**********************************************************
*@author             : Mukul Kumar
*@Description        : All Opportunity Trigger event will be managed here. 
*                      Code should be written in the respective handlers and call method from required event method.
*@NewUtility handler : Create new method for sales functionality in "ST_SalesOpportunityTriggerUtilityHandler" class  and call it from this class..
*                      Create new method for Payment functionalities in "ST_PaymentOppTriggerUtilityHandler" class and call it from this class.
*@JIRA               : SDM-135
**********************************************************/

public class ST_OpportunityTriggerEventHandler {
    
    // All before insert event logic should be called from beforeInsert method
    public static void beforeInsert(List<Opportunity> triggerNew) {
        OpptyTriggerHandler.insertHandler(triggerNew);
        ST_SalesOpportunityTriggerUtilityHandler.updatePrimaryContact(triggerNew);  //Added by Sanjeev
        ST_UpsellOppTriggerUtilityHandler.updateOppOwnerasSalesOppOwner(triggerNew,null);
        //ST_FinanceOppTriggerUtilityHandler.updateOppOwnerasQueueManager(triggerNew,null);
    }
    
    // All after insert Event logic should be called from afterInsert method
    public static void afterInsert(List<Opportunity> triggerNew, Map<Id, Opportunity> newMap) {
        CreateQMonOppTriggerHandler.handleCreateQM(triggerNew);
        ST_SalesAddOnProductHandler.onAfterInsert(triggerNew, newMap);
        new VOpportunityTriggerDispatcher().dispatch();
        //ST_FinanceOppTriggerUtilityHandler.addProductstoNewlyFinOpp(triggerNew,null);
        //ST_FinanceOppTriggerUtilityHandler.createFinanceAging(triggerNew,null);
    }
    
    // All after insert Event logic should be called from beforeUpdate method
    public static void beforeUpdate(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        OpptyTriggerHandler.updateHandler1(triggerNew);
        //ST_OpportunityContactRoleValidator.checkContactRoleonOpp(triggerNew, oldMap);
        ST_SalesAddOnProductHandler.onBeforeUpdate(triggerOld, oldMap, triggerNew, newMap);
        ST_SalesOpportunityTriggerUtilityHandler.UpdateOpportunityRecords(triggerNew, oldMap); //Added by Sanjeev
        //added by Himanshu
      
        ST_SalesOpportunityTriggerUtilityHandler.updateAEConfidence(triggerNew,oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.updateDiscoveryCallStageDate(triggerNew,oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.updateMeetingYettoRun(triggerNew,oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.updateManageTechQuantityCheck(triggerNew,oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.CountReschedulesforNewRecords(triggerNew,oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.UpdateDiscoveryMeetingRan(triggerNew,oldMap);
        ST_UpsellOppTriggerUtilityHandler.updateCallDatesonRespStages(triggerNew,oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.AutoPopulateStageDatesforCCProcessed(triggerNew, oldMap);
        ST_SalesOpportunityTriggerUtilityHandler.updateSEImpactField(triggerNew,oldMap);
        
    }
    
    // All after insert Event logic should be called from afterUpdate method
    public static void afterUpdate(List<Opportunity> triggerNew, List<Opportunity> triggerOld, Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        if(ST_Recursionhandler.runOnce()) {
            OpportunityTriggerHandler.handleInsertAsset(triggerNew, oldMap);
            ST_Recursionhandler.run = false;
        }
        ST_PaymentOpportunityHandler.updateOnboardingAndOpportunity(triggerNew, oldMap);
        ST_UpdateObDiscoveryCall.ST_UpdateDiscoCall(triggerNew, oldMap);
        ST_SalesAddOnProductHandler.onAfterUpdate(triggerOld, oldMap, triggerNew, newMap);
        // Create Finace Opportunity : Added by Mukul
        //ST_CreateFinanceAssets.createFinanceAssets(triggerNew,oldMap); 
        new VOpportunityTriggerDispatcher().dispatch();
        //ST_FinanceOppTriggerUtilityHandler.updateAccountStatusfromOpportunity(newMap, oldMap);
    }
    
    // All after insert Event logic should be called from beforeDelete method
    public static void beforeDelete(List<Opportunity> triggerOld, Map<Id, Opportunity> oldMap) {
        
    }
    
    // All after insert Event logic should be called from afterDelete method
    public static void afterDelete(List<Opportunity> triggerOld, Map<Id, Opportunity> oldMap) {
        
    }
}