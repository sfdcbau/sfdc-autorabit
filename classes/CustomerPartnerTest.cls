/**
	Test Customer-Partner creation
 */
@isTest
private class CustomerPartnerTest 
{
	
    @IsTest(SeeAllData=false) 
    public static void testCustomerPartnerAddPartnerParent() 
    {
        
    }

	/*
		Test set Primary Contact on lead conversion
		
		Notes:
			- Need to see all data because we need access to the custom setting. 
			- Commented because zendesk always gives an issue with seeAllData=true
	*/
	/*
    @IsTest(SeeAllData=true) 
    public static void testConvertCustomerPartner() 
    {
        List<Lead> leadList = new List<Lead>();
		List<Database.LeadConvert> leadconvertList = new List<Database.LeadConvert>();
		List<LeadStatus> leadconvertedstatusList = [Select MasterLabel From LeadStatus Where IsConverted = true Limit 1];
		
		// Add leads
		Lead l1 = new Lead();
		l1.Company = 'Test Company 1';
		l1.Firstname = 'User';
		l1.Lastname = 'Test1';
		l1.Status = 'Open';
		l1.Estimated_Total_Potential_Office_Users__c = 1;
		l1.No_of_Technicians__c = 1;
		l1.Disposition_Code__c = 'Unreachable';
		l1.Referred_By__c = 'Test';
		leadList.add(l1);
		
		insert leadList;
		
		Test.startTest();
		
		// Create new lead customer-partner
        String cpId1 = CustomerPartnerServiceController.createLeadCustomerPartner(leadList[0].id);
        //System.assertNotEquals(cpId1,'');
		
		// Convert leads
		Database.LeadConvert lc1 = new Database.LeadConvert();
		lc1.setLeadId(l1.Id);
		lc1.setConvertedStatus(leadconvertedstatusList[0].MasterLabel);
		leadconvertList.add(lc1);
		
		List<Database.LeadConvertResult>lcrList = Database.convertLead(leadconvertList);
		
		// Create new account customer-partner
        String cpId2 = CustomerPartnerServiceController.createAccountCustomerPartner(lcrList[0].getAccountId());
        //System.assertNotEquals(cpId2,'');
		
		Test.stopTest();
    }
    */
}