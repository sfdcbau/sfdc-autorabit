@isTest
private class SalesWeeklyAEForecastTest {
    static testmethod void testWeeklyReportTest(){
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        
        list <Account> accList = new list<Account>();
        accList.add(acc);
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        system.debug('===>OPPRT' +OppRT );
        
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', RecordTypeId= OppRT, CloseDate=Date.today(), AccountId = acc.id,AE_Confidence__c = 'High Probability');
        insert opp1;
        Opportunity opp2 = new Opportunity (Name='Opp2',StageName='Stage Zero', RecordTypeId= OppRT, CloseDate=system.today().addDays(-6), AccountId = acc.id,AE_Confidence__c = 'High Probability');
        insert opp2;
        Test.startTest();
        System.schedule('Scheduled Job 2', '0 44 * * * ?', new SalesWeeklyAEForecast());
        Test.stopTest(); 
    }
}