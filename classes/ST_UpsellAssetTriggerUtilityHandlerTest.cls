@istest
public class ST_UpsellAssetTriggerUtilityHandlerTest {
	
    @testsetup
    static void createData(){
        Contact con = New Contact(lastname = 'Test');
        Insert con;
        
        Account acc = utilityHelperTest.createAccount();
        acc.Primary_Contact__c = con.id; 
        Insert acc;
        
         Contact con2 = New Contact(lastname = 'Test',AccountId = acc.Id, Role__c = 'Accountant');
        Insert con2;
        
        // inserting Product
        Product2 addOnProdObj1 = new Product2(Name = 'AddOnProd', Product_Type__c = 'Add-on', Core_Product__c = false);
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert new List<Product2> {coreProdObj1, addOnProdObj1};
        
        // inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        PricebookEntry pbOj2 = new PricebookEntry (Product2ID = addOnProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=100, isActive=true);
        insert new List<PricebookEntry> {pbOj1, pbOj2};
        
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity opp = new Opportunity (Name='Opp1',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId, CloseDate=System.today() -2 ,AccountId = acc.id, 
        ImplementationDate__c= System.today()+2, Primary_Contact__c  = acc.Primary_Contact__c, Software_new__c  = acc.Software_newpicklist__c, Initial_Tech_Count__c = 'No Tech Count at Opp Creation');
        insert opp;
        
        Id opportunityRecordTypeIdUpsell = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        Opportunity oppUpsell = new Opportunity (Name='Opp1',StageName='Prospect', RecordTypeId= opportunityRecordTypeIdUpsell, CloseDate=System.today() -2 ,AccountId = acc.id,Parent_Opportunity__c = opp.Id);
        insert oppUpsell;
        
        Id assetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Asset eachAsset = new Asset(Name = 'test',Status='Pending',AccountId = acc.Id,Opportunity__c = opp.Id,RecordTypeId=assetRecordTypeId);
        insert eachAsset;
        
        Id assetRecordTypeIdUpsell = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        Asset eachAssetpsell = new Asset(Name = 'test',Status='Pending',AccountId = acc.Id,Opportunity__c = oppUpsell.Id,RecordTypeId=assetRecordTypeIdUpsell);
        insert eachAssetpsell;
        
    }
     @isTest
    static void updateAEConfidenceTest(){
        Opportunity eachOpp = [Select id,stageName,DQ_Rationale__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        system.assertEquals(eachOpp.RecordType.DeveloperName , 'Sales');
        
        Opportunity eachOppUpsell = [Select id,stageName,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Upsell' LIMIT 1];
        system.assertEquals(eachOppUpsell.RecordType.DeveloperName , 'Upsell');
        
        Contact eachCon = [Select id,AccountId from Contact where AccountId != null];
        system.assertNotEquals(eachCon.AccountId , null);
        
        Asset eachasset = [Select id,Status from asset where RecordType.DeveloperName = 'Sales'];
        system.assertNotEquals(eachasset.Status , null);
        
        
        Product2 p2 = [Select id,Core_Product__c from Product2 WHERE Core_Product__c = false LIMIT 1];
        system.assertEquals(p2.Core_Product__c , false);
        
        OpportunityContactRole objRole = new OpportunityContactRole();
        objRole.OpportunityId = eachOpp.Id;
        objRole.Role = 'Owner';
        objRole.ContactId = eachCon.Id;
        insert objRole;
        
        system.assertEquals(objRole.Role , 'Owner');
         
        test.startTest();
            eachOpp.DQ_Rationale__c = 'test';
            eachOpp.StageName ='Closed Won';
            eachOpp.Address_Industry_Verified__c = true;
            eachOpp.OBDiscoveryCallDate__c = system.today();
            eachOpp.ImplementationDate__c = system.today() + 22;
            eachOpp.Onboarding_POC__c = eachCon.Id;
            eachOpp.AE_Discovery_POC__c  = eachCon.Id;
            eachOpp.Competing_Software__c = 'Acowin';
            eachOpp.OB_Discovery_Call_Date_at_Close__c = false;
            eachOpp.AE_Confidence__c = 'Low Probability' ;
            eachOpp.Delayed_Implementation__c = false;
            try{
                update eachOpp;   
                system.assertEquals(eachOpp.StageName, 'Closed Won');
            }
            catch(Exception ex){
                system.debug('Exception is ' + ex.getMessage());
            }
        eachasset.status ='Inactive';
        eachasset.Inactive_Reason__c = 'Denied by partner';
        update eachasset;
        test.stopTest();
    }
}