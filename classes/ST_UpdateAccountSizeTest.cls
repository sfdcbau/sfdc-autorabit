@isTest
public class ST_UpdateAccountSizeTest {
    @testsetup
    static void createTestData() { 
        Product2 product = utilityHelperTest.createProduct();
        product.name = 'Platform Payments-Check';
        product.Core_Product__c = TRUE;
        insert product;
        
        PricebookEntry standPB = utilityHelperTest.craeteStandardPBE2(product.id);
        insert standPB;
        
        Pricebook2 custPB = utilityHelperTest.craeteCustomPB2();
        insert custPB;   
        
        PricebookEntry pbe = utilityHelperTest.createPricebookEntry(custPB, product);
        insert pbe;
        
        Account acc = utilityHelperTest.createAccount();
        insert acc;
        
        contact con = utilityHelperTest.createContact(acc.id);
        insert con;
        
        Id oppSalesRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity opp = utilityHelperTest.createopportunity(acc.id,custPB.id,con.Id);
        insert opp;
        
        OpportunityLineItem ol = utilityHelperTest.createOppProduct(opp.Id, pbe.Id, product.id);
        insert ol;
        ol.unitprice = 0;
        ol.Projected_Quantity__c = 0;
        update ol;
    }
    
    @isTest static void testMethod1(){ 
        List<Account> accObjList = [Select Id, name, Account_Size__c from Account];
        
        // update Estimated_Total_Potential_Technicians__c = null
        accObjList[0].Estimated_Total_Potential_Technicians__c = null;
        update accObjList[0];
        
        ST_UpdateAccountSize.updateSizeOfAcc(accObjList);
        System.assertNotEquals(null, accObjList.size());
    }
    
    @isTest static void testMethod2(){ 
        List<Account> accObjList = [Select Id, name, Account_Size__c from Account];
        
        // update Estimated_Total_Potential_Technicians__c = 2
        accObjList[0].Estimated_Total_Potential_Technicians__c = 2;
        update accObjList[0];
        
        ST_UpdateAccountSize.updateSizeOfAcc(accObjList);
        System.assertNotEquals(null, accObjList.size());
    }
    
    @isTest static void testMethod3(){ 
        List<Account> accObjList = [Select Id, name, Account_Size__c from Account];
        
        // update Estimated_Total_Potential_Technicians__c = 8
        accObjList[0].Estimated_Total_Potential_Technicians__c = 8;
        update accObjList[0];
        
        ST_UpdateAccountSize.updateSizeOfAcc(accObjList);
        System.assertNotEquals(null, accObjList.size());
    }
    
    @isTest static void testMethod4(){ 
        List<Account> accObjList = [Select Id, name, Account_Size__c from Account];
        
        // update Estimated_Total_Potential_Technicians__c = 20
        accObjList[0].Estimated_Total_Potential_Technicians__c = 20;
        update accObjList[0];
        
        ST_UpdateAccountSize.updateSizeOfAcc(accObjList);
        System.assertNotEquals(null, accObjList.size());
    }
    
    @isTest static void testMethod5(){ 
        List<Account> accObjList = [Select Id, name, Account_Size__c from Account];
        
        // update Estimated_Total_Potential_Technicians__c = 30
        accObjList[0].Estimated_Total_Potential_Technicians__c = 30;
        update accObjList[0];
        
        ST_UpdateAccountSize.updateSizeOfAcc(accObjList);
        System.assertNotEquals(null, accObjList.size());
    }
}