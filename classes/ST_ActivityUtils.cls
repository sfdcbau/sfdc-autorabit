/* ──────────────────────────────────────────────────────────────────────────────────────────────────
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a handler class for ST_TaskTrigger.
*
* This class automatically counts the number of call made to the Account to give an insight as to how many time an account was touched(Calls/ email).
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jay Prasad   <jprasd@servicetitan.com>
* @version        1.0
* @created        06-31-2018
* @Name           ST_ActivityUtils
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class  ST_ActivityUtils {
    
    //configs
    set<Id> accountIds;

    //Constructor
    public ST_ActivityUtils(List<sObject> records){
       
        accountIds = new set<id>();
        captureWhatAndWhoIds(records);
    }

    public void updateAccountActivityCount() {
        if(accountIds.size() == 0) return;
        updateActivityHistory(accountIds);
    }

    private void updateActivityHistory(Set<Id> updateIds){
        
        List<ActivityHistory> CallMades = new List<ActivityHistory>();
        List<ActivityHistory> DMConnects = new List<ActivityHistory>();
        List<Account> updateAccList = new List<Account>();

        List<Account> accList = new List<Account>();
        accList = [SELECT Id, (SELECT Id, Activity_Type_NEW__c FROM ActivityHistories WHERE Activity_Type_NEW__c!=null) FROM Account WHERE Id IN :updateIds];
        //for(Account ac : accountIds ){
            system.debug('====>' +accList);
    
        //}
        for(Account ac : acclist){
            CallMades = new List<ActivityHistory>();
            DMConnects = new List<ActivityHistory>();
            for(ActivityHistory  ah : ac.activityhistories){
                CallMades.add(ah);
                if((ah.Activity_Type_NEW__c=='DM Connect')||(ah.Activity_Type_NEW__c=='GK-to-DM Connect')) {
                DMConnects.add(ah); 
                }
            }
            system.debug('===> #Calls Made' + CallMades.size());
            system.debug('===> #DM Connects' + DMConnects.size());

            ac.Calls__c = string.valueOf(CallMades.size());
            ac.DM_Connects__c =string.valueOf(DMConnects.size());
            updateAccList.add(ac);
        }
        if(!updateAccList.isEmpty()){
            update updateAccList;
            
        }
        
    }


    private void captureWhatAndWhoIds(List<sObject> objects) {
        for(sObject o : objects) {
            Id whatId = (Id)o.get('WhatId');
            Id whoId = (Id)o.get('WhoId');
            if(whatId != null) {
              String objectName = getObjectNameFromId(whatId);
               if(objectName == 'account') accountIds.add(whatId);
                //if(objectName == 'opportunity') opportunityIds.add(whatId);
                accountIds.add(whatId);
            }
            if(whoId != null) {
                String objectName = getObjectNameFromId(whoId);
                //if(objectName == 'contact') contactIds.add(whoId);
                //if(objectName == 'lead') leadIds.add(whoId);
                //String objectName ='account';
            } 
            //Id ID = .get('What');
            //accountIds.add((ID)o.get('Id'));
        }

        system.debug('=====>AccountIDs' +accountIds);
    }

    private String getObjectNameFromId(Id objId) {
        String preFix = String.valueOf(objId).left(3).toLowercase();
        if(prefix == '001') return 'account';
        if(prefix == '003') return 'contact';
        if(prefix == '006') return 'opportunity';
        if(prefix == '00q') return 'lead';
        //if(prefix == '500') return 'case';
        return '';
    } 


    private String getStringFromIdSet(set<id> idSet) {
        string idString = '';
        for(Id i : idSet) idString+= '\'' + i + '\',';
        return idString == '' ? idString : idString.left(idString.length()-1); //If idString contains some ids we want to ensure we strip out the last comma
    } 


    //this to accept an object id
    private sObject createObject(String typeName, Id objId) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            // throw an exception
        }
        
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(objId);
    } 

}