@isTest
public class Test_OppTriggerHandler {
    @isTest
    public static void testOppTriggerHandler(){
        //Create Account record
        Account acc=new Account(Name='testAcc');
        acc.billingcity = 'Ahmedabad';
        acc.billingcountry = 'United States';
        acc.billingstate = 'California';
        acc.Industry__c = 'asd';
        acc.No_Known_Active_Partners__c = true;
        acc.Influencer__c ='Yes';
        acc.Residential__c =13.00;
        acc.Time_Zone__c ='Pacific (UTC-8:00)';
        acc.Tenant_ID__c ='11223';
        acc.Tenant_Name__c ='dicnd';
        acc.Tenant_Tags__c ='bluefrog';
        acc.Initial_Username__c ='kjbdcc';
        acc.Influencer_Detail__c ='njjid';
        //acc.Overall_Strategic_Importance_Score__c =6.00;
        //acc.ImplementationDate__c = System.today();
        acc.PlusOrMinus5EstTechs__c = '0-2';
        acc.Estimated_Total_Potential_Office_Users__c = 12;
        acc.Estimated_Total_Potential_Technicians__c = 11;
        acc.CountofOwnerPOCContacts__c =2;
        Insert acc;
        
        //Create User for  role
        User u1=new User(firstName='test',LastName='test1',Email='test@gmail.com');
        //insert u1;
        
        //Create contact record
        Contact con1=new Contact(FirstName='test',LastName='test1',Email='test@gmail.com',AccountId=acc.Id,Role__c='POC');
        insert con1;
        
        RecordType srt=[Select Id,Name from RecordType where Name='Sales' Limit 1];
         RecordType srtpy=[Select Id,Name from RecordType where Name='ServiceTitan Payments' Limit 1];
        System.debug('srtpy : '+srtpy);
          //Insert opportunity related to that account
        Product2 p2 = new product2();
        p2.name = 'prod';
        insert p2;
        
          Id pricebookId = Test.getStandardPricebookId();
        
         PricebookEntry pbe1 = new PricebookEntry (Product2ID=p2.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
    insert pbe1;
        Opportunity op1=new Opportunity (Name='testOpportunity1',of_tech__c=100,AccountId=acc.Id,Onboarding_POC__c=con1.Id,StageName='Stage Zero',Pricebook2Id = pbe1.Pricebook2Id,RecordType=srt,CloseDate=System.today());
        insert op1;
       
        
        opportunitylineitem oli = new opportunitylineitem();
        oli.OpportunityId = op1.id;
        oli.product2Id = p2.id;
        oli.opportunity = op1;
        oli.ServiceDate = system.today();
        oli.Quantity = 10;
        oli.unitprice = 10;
        insert oli;
        op1.Address_Industry_Verified__c = true;
        op1.Negotiation_Stage_Date__c = System.today();
        op1.Pricing_Stage_Date__c = System.today();
        op1.Q_A_Stage_Date__c = System.today();
        op1.Discovery_Call_Stage_Date__c = System.today();
        op1.Onboarding_POC__c=con1.Id;
        update op1;
        op1.stagename = 'Onboarding';
        update op1;
        Opportunity op2=new Opportunity(Name='testOpportunity2',Of_Tech__c=200,AccountId=acc.Id,StageName='Stage Zero',RecordTypeID=srtpy.Id,CloseDate=System.today());
        System.debug('op2 : '+ op2);
        insert op2;
        op2.StageName='Prospect';
        update op2;
         
         List<Opportunity> oppList=new List<Opportunity>{op1};
        OpptyTriggerHandler.updateHandler(oppList);
      
      //   OpptyTriggerHandler.insertHandler(oppList,acc.Id);
    } 
}