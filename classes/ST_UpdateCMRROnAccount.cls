/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
*Basically Trigger is doing when Opportunity is 'Closed won' & Record Type= 'Sales' & Assets Status=Active(Pending,Live,Live & Using) 
*& Record Type sales then it will display the CMRR_Num_c = SUM of CMRR Value of All Closed won Opportunities with Active Assets. 
*The sum of CMRR Values calculated using Total_Projected_MRR_c field on opportunity object which is Roll-up to the OLI.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vishal Labh  <vlabh@servicetitan.com>
* @version        1.2
* @created        04/10/2018*
@Updated        17/10/2018
* @Name           ST_UpdateCMRROnAccount
* @Method         ST_UpdateCMRR()
* @Trigger        ST_AssetTrigger
* @TestClass      ST_AssetTriggerTest
* @TestClassHelper utilityHelperTest  
* @Version         1.3
* @Modified Date   03/19/2018
* @Modified By     Himanshu 
* @User Story      SE- 2424 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/public class ST_UpdateCMRROnAccount {
    Public static void ST_UpdateCMRR(list<Asset> assetlist ){
        
        // Trigger functionality On/Off through custom metadata
        Trigger_Settings__mdt TriggerFlag = [SELECT 
                                             Label, isActive__c 
                                             FROM Trigger_Settings__mdt 
                                             Where Label='ST_AssetTrigger' limit 1];
        if(TriggerFlag.isActive__c) {
            Id AsstRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
            Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
            Id UpsellAssetRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
            Id UpsellOppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
            
            Map<Id, Double> oppMap = new Map<Id, Double>();
            Set<Id> oppIds = new Set<Id>();  
            Set<Id> accountIds = new Set<Id>();  
            Set<Id> acIdforInactiveAst = new Set<Id>();  
            list<Asset> astlist = new list<Asset>();
            for(Asset ast : assetlist) {
                if(ast.AccountId!=null){
                    accountIds.add(ast.AccountId);
                }
                
            }
            
            if(accountIds.size() > 0){
                astlist = [SELECT 
                           Id, name, AccountId, Status, RecordTypeId, Opportunity__c 
                           From Asset 
                           Where AccountId IN : accountIds AND (RecordTypeId = : AsstRecordTypeId OR RecordTypeId = :UpsellAssetRecordTypeId) ];
                           
                           system.debug('@@astlist'+astlist );
            }  
            // Checking status of Active assets With recordtype=sales
            Integer astCount = 0;
            for(Asset ast : astlist) {
                if(ast.Status != 'Inactive') {
                    oppIds.add(ast.Opportunity__c);
                } else if(ast.Status == 'Inactive'){
                    astCount = astCount + 1;
                    if(astlist.size() == astCount) {
                        acIdforInactiveAst.add(ast.AccountId);
                    }
                } 
            }
             system.debug('@@oppIds'+oppIds);
            if(oppIds.size() > 0) {
                // Taking opportunity Total_Projected_MRR__c (Rollup-Summary) for recordtype sales & stagename 'closed won' for updating CMRR_Total__c field on account
                List<opportunity> opportunitylist = [SELECT 
                                                     Id, name, AccountId, Total_Projected_MRR__c 
                                                     From opportunity 
                                                     Where Id IN : oppIds AND (RecordTypeId = :OppRecordTypeId OR RecordTypeId =: UpsellOppRecordTypeId ) AND Stagename = 'Closed Won'];
                
                if(opportunitylist.size() > 0) {
                    for(opportunity  opp : opportunitylist) {
                        if(oppMap.containskey(opp.AccountId) && oppMap.get(opp.AccountId) != null){
                            Double ProjectedMMR = oppMap.get(opp.AccountId);
                            ProjectedMMR = ProjectedMMR + opp.Total_Projected_MRR__c;
                            oppMap.put(opp.AccountId, ProjectedMMR);
                        }else {
                            oppMap.put(opp.AccountId, opp.Total_Projected_MRR__c);
                        }
                    }
                    System.debug('@@OppMap'+oppMap);
                    system.debug('@@opportunitylist '+opportunitylist );
                }
                // updating account CMRR_Total__c field with above conditions 
                List<Account> accountlist = [SELECT 
                                             Id, name, CMRR_Total__c 
                                             From Account 
                                             Where Id IN : accountIds];
                if(accountlist.size() > 0) {
                    List<Account> updateAccount = new List<Account>();
                    for(Account acc : accountlist) {
                        acc.CMRR_Total__c = oppMap.get(acc.Id); 
                        updateAccount.add(acc);
                    }
                    if(updateAccount.size() > 0) {
                        update updateAccount;
                    }
                }
            }
            if(acIdforInactiveAst.size() > 0) {
                list<Account> acclist = [SELECT 
                                         Id, name, CMRR_Total__c 
                                         From Account 
                                         Where Id IN : acIdforInactiveAst];
                if(acclist.size() > 0) {
                    List<Account> updateAccount = new List<Account>();
                    for(Account ac : acclist) {
                        ac.CMRR_Total__c = 0;
                        updateAccount.add(ac);
                    }
                    if(updateAccount.size() > 0) {
                        update updateAccount;
                    }
                }
            } 
        }   
    }
}