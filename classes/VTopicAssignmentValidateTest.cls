@IsTest 
public with sharing class VTopicAssignmentValidateTest 
{
    
    @IsTest(SeeAllData=false) 
    public static void testFeedItemTopicAssignmentValidate() 
    {
        List<Topic> newtopicList = new List<Topic>();
        
        Topic t1 = new Topic();
        t1.Name = 'Dispatching';
        t1.NetworkId = Network.getNetworkId();
        newtopicList.add(t1);
        
        Topic t2 = new Topic();
        t2.Name = 'Marketing';
        t2.NetworkId = Network.getNetworkId();
        newtopicList.add(t2);
		        
        insert newtopicList;
        
        Test.startTest();
        
        String feedItemId;
        
        List<User> userList = [Select Id From User Where IsActive = true And Profile.Name = 'Standard Success' Limit 1];
        if(!userList.isEmpty())
        {
            System.runAs(userList[0])
            {
                Account acc = new Account();
                acc.Name = 'Test 018210821';
                insert acc;
                
                FeedItem fi = new FeedItem();
                fi.ParentId = acc.Id;
                fi.Body = 'Test';
                insert fi;
                
                feedItemId = fi.Id;
                
                List<Topic> topicList = [Select Id From Topic Where Name = 'Dispatching' And NetworkId = :Network.getNetworkId() Limit 1];
                if(!topicList.isEmpty())
                {
                    TopicAssignment ta = new TopicAssignment();
                    ta.EntityId = fi.Id;
                    ta.TopicId = topicList[0].Id;
                    insert ta;
                }
            }
        }
        
        List<FeedItem> feedItemList = [Select Id From FeedItem Where Id = :feedItemId Limit 1];
        
        // Insert new topic assignment
        try
        {
            List<Topic> topicList = [Select Id From Topic Where Name = 'Marketing' And NetworkId = :Network.getNetworkId() Limit 1];
            if(!topicList.isEmpty())
            {
                TopicAssignment ta = new TopicAssignment();
                ta.EntityId = feedItemList[0].Id;
                ta.TopicId = topicList[0].Id;
                insert ta;
            }
        }
        catch(Exception ex)
        {
            System.assert(ex.getMessage().contains('You do not have permission to assign topic to this post'));
        }
        
        
        // Delete topic assignment
        try
        {
            List<TopicAssignment> topicAssignmentList = [Select Id From TopicAssignment Where Topic.Name = 'Dispatching' And EntityId = :feedItemList[0].Id Limit 1];
            if(!topicAssignmentList.isEmpty())
            {
                delete topicAssignmentList;
            }
        }
        catch(Exception ex)
        {
            System.assert(ex.getMessage().contains('You do not have permission to assign topic to this post'));
        }
        
        test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testFeedItemTopicAssignmentValidateBypass() 
    {
        List<User> currentUserList = [Select Id, ProfileId From User Where Id = :UserInfo.getUserId() Limit 1];
        
        Edit_All_Topic_Assignment_Profile__c cs = new Edit_All_Topic_Assignment_Profile__c();
        cs.Name = 'System Administrator';
        cs.Profile_ID__c = currentUserList[0].ProfileId;
        insert cs;
        
        List<Topic> newtopicList = new List<Topic>();
        
        Topic t1 = new Topic();
        t1.Name = 'Dispatching';
        t1.NetworkId = Network.getNetworkId();
        newtopicList.add(t1);
        
        Topic t2 = new Topic();
        t2.Name = 'Marketing';
        t2.NetworkId = Network.getNetworkId();
        newtopicList.add(t2);
		        
        insert newtopicList;
        
        Test.startTest();
        
        String feedItemId;
        
        List<User> userList = [Select Id From User Where IsActive = true And Profile.Name = 'Standard Success' Limit 1];
        if(!userList.isEmpty())
        {
            System.runAs(userList[0])
            {
                Account acc = new Account();
                acc.Name = 'Test 018210821';
                insert acc;
                
                FeedItem fi = new FeedItem();
                fi.ParentId = acc.Id;
                fi.Body = 'Test';
                insert fi;
                
                feedItemId = fi.Id;
                
                List<Topic> topicList = [Select Id From Topic Where Name = 'Dispatching' And NetworkId = :Network.getNetworkId() Limit 1];
                if(!topicList.isEmpty())
                {
                    TopicAssignment ta = new TopicAssignment();
                    ta.EntityId = fi.Id;
                    ta.TopicId = topicList[0].Id;
                    insert ta;
                }
            }
        }
        
        List<FeedItem> feedItemList = [Select Id From FeedItem Where Id = :feedItemId Limit 1];
        
        // Insert new topic assignment
        List<Topic> topicList = [Select Id From Topic Where Name = 'Marketing' And NetworkId = :Network.getNetworkId() Limit 1];
        if(!topicList.isEmpty())
        {
            TopicAssignment ta = new TopicAssignment();
            ta.EntityId = feedItemList[0].Id;
            ta.TopicId = topicList[0].Id;
            insert ta;
        }
        
        System.assertEquals(2, ([Select Id From TopicAssignment Where EntityId = :feedItemList[0].Id]).size());
        
        test.stopTest();
    }
    
}