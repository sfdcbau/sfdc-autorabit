/****************************************
* @author   : Ravi Pureti
* @JIRA     : SE-2509
* @testcls  : ST_BatchOnboardingStatusChangedTest
* *****************************************/

global class ST_BatchOnboardingStatusChanged implements Database.Batchable<sObject>,Schedulable{
    
    String query;
    // Start method : Doing query on onboarding 
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        List<String> lstOBAccLevel = Label.OBAccountLevel.split(',');
        query = 'SELECT '+
            'ID, Onboarding_Status__c, OB_Account_Level__c, RecordtypeId from Onboarding__c' + ' ' +
            ' WHERE Onboarding_Status__c = \'Pre-Implementation\'' + ' ' + 'and OB_Account_Level__c IN :lstOBAccLevel and OB_Discovery_Call_Date__c = TODAY';
        return Database.getQueryLocator(query);
    }
    // Execute method : getting onboarding record from start method
    public void execute(Database.BatchableContext bc, List<Onboarding__c> lstUpdateOBStatus){
        
        Id devRecordTypeId;
        if(Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding') != null)    
            devRecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
        list<Onboarding__c> updateOnbordingList = new list<Onboarding__c>();
        // Itterating list of onboarding and updating onboarding status.
        if(lstUpdateOBStatus != null && lstUpdateOBStatus.size() > 0){
            for(Onboarding__c eachOb : lstUpdateOBStatus){
                if(eachOb.RecordtypeId == devRecordTypeId) {
                    eachOb.Onboarding_Status__c = 'Implementation';
                    updateOnbordingList.add(eachOb);
                }
            }
        }
        if(updateOnbordingList != null && updateOnbordingList.size() > 0){
            Database.SaveResult[] srList = Database.update(updateOnbordingList, false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted Opportunity. Opportunity ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
    // Finish method
    public void finish(Database.BatchableContext bc){
        
    }
    // Scheduling batch class
    public void execute(SchedulableContext sc){
        //Calling Batch class
        ST_BatchOnboardingStatusChanged batch = new ST_BatchOnboardingStatusChanged();
        Database.executeBatch(batch, 100);
    }
}