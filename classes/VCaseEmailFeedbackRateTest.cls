/**
    Test methods for Case Email Feedback page controllers
 */
@IsTest 
private class VCaseEmailFeedbackRateTest 
{

    /*
        CaseEmailFeedback - Good
    */
    @IsTest(SeeAllData=false) 
    public static void testCaseRating() 
    {
        
        // Insert case
        Case c1 = new Case();
        c1.Status = 'New';
        c1.Origin = 'Web';
        c1.Description = 'Test';
        c1.Subject = 'Test';
        insert c1;
        
        test.startTest();
        
        PageReference pageref = Page.VCaseEmailFeedbackRate;
        pageref.getParameters().put('id', c1.Id);
        pageref.getParameters().put('i', '3');
        test.setCurrentPage(pageref);
        
        VCaseEmailFeedbackRateController ctrl = new VCaseEmailFeedbackRateController();
        ctrl.loadQueryRequest();
        
        // Check case
        List<Case> caseList = [Select Id, Rate__c From Case Where Id = :c1.Id Limit 1];
        //System.assertEquals(caseList[0].Rate__c, '3');
        
        // Rate it again
        ctrl.loadQueryRequest();
        
        test.stopTest();
    }
    
}