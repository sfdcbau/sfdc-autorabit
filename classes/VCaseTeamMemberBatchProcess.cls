/*
    Create Case Team Member
*/
global class VCaseTeamMemberBatchProcess implements Database.Batchable<sObject>
{
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
        Date mydate = Date.today();
        String query = 'Select Id, Case__c, Completed__c, Errored__c, Error_Message__c From Case_Team_Member_Batch__c Where Completed__c = false And Errored__c = false';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Case_Team_Member_Batch__c> scope) 
    {
        
        Set<Id> caseSet = new Set<Id>();
        
        // Collect batch to update
        for(Case_Team_Member_Batch__c ctmb : scope)
        {
            if(!caseSet.contains(ctmb.Case__c))
                caseSet.add(ctmb.Case__c);
        }
        
        if(!caseSet.isEmpty())
        {
            List<Case_Team_Member_Batch__c> updateBatchList = new List<Case_Team_Member_Batch__c>();
            
            try
            {
                VCaseUtil.addCaseTeamMember(caseSet);
            }
            catch(DmlException de)
            {
                for(Case_Team_Member_Batch__c ctmb : scope)
                {
                    ctmb.Errored__c = true;
                    ctmb.Error_Message__c = de.getMessage();
                    updateBatchList.add(ctmb);
                }
            }
            
            // if the list is empty, then there is no error 
            if(updateBatchList.isEmpty())
            {
                for(Case_Team_Member_Batch__c ctmb : scope)
                {
                    ctmb.Completed__c = true;
                    updateBatchList.add(ctmb);
                }
            }
            
            update updateBatchList;
        }
    }
    
    global void finish(Database.BatchableContext BC) 
    {
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems,CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()]; 
		System.debug('##### AsyncApexJob ID: ' + a.Id); 
    }
}