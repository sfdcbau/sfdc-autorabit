/**
    Test account update to keep related Customer Partner in-sync 
 */
@isTest
private class AccountUpdateCustomerPartnerTest {

    /*
        Test Update Customer Partner Assocation Weight
    */
    /*
    @IsTest(SeeAllData=false) 
    public static void testUpdateCustomerPartnerAssociationWeight() 
    {
        // Customer account
        Account customeracc = new Account();
        customeracc.Name = 'Customer Account';
        customeracc.BillingCountry = 'United States';
        insert customeracc;
        
        // Partner account
        Account partneracc = new Account();
        partneracc.Name = 'Partner Account';
        partneracc.BillingCountry = 'United States';
        insert partneracc;
        
        Test.startTest();
        
        // Create customer partner under customer account
        CustomerPartnerServiceController.createAccountCustomerPartner(customeracc.Id);
        
        // Get the created customer-partner
        List<CustomerPartner__c> cpList = [Select Id From CustomerPartner__c Where Customer__c = :customeracc.Id];
        
        System.assertEquals(cpList.size(), 1);
        
        // Assign partner
        cpList[0].Partner__c = partneracc.Id;
        update cpList;
        
        // Association weight is empty
        System.assertEquals(([Select Id, Association_Weight__c From CustomerPartner__c Where Customer__c = :customeracc.Id])[0].Association_Weight__c, null);
        
        // Update partner association weight
        partneracc.Association_Weight__c = '10';
        update partneracc;
        
        // Association weight is 10
        System.assertEquals(([Select Id, Association_Weight__c From CustomerPartner__c Where Customer__c = :customeracc.Id])[0].Association_Weight__c, 10);
        
        Test.stopTest();
        
    }*/
}