/***********************************************
 * @testclass : TalkdeskAPIControllerTest

 * *********************************************/

/*/services/apexrest/talkdeskAPI*/
@RestResource(urlMapping='/talkdeskAPI/*')
global class TalkdeskAPIController {
    @HttpPost
    global static void gsCreateActivity(string source_address, string accountID){
            
            JSONGenerator gen = JSON.createGenerator(true);
            RestResponse res = RestContext.response;
            res.addHeader('Content-Type','application/json');
            
            String gsURL = 'Error creating URL';
            String status = 'failure';
            List<Talkdesk_Gainsight_Settings__mdt> tcs = new List<Talkdesk_Gainsight_Settings__mdt>([SELECT Gainsight_URL_Base__c 
                                                                                      FROM Talkdesk_Gainsight_Settings__mdt
                                                                                      WHERE Gainsight_URL_Base__c!=NULL]);
            if(tcs.size()>0){
                status = 'success';
                gsURL = tcs[0].Gainsight_URL_Base__c + accountID;
            }
            // Write JSON Body
            gen.writeStartObject();
            gen.writeStringField('status', source_address);
            gen.writeStringField('accountURL', gsURL);
            gen.writeEndObject();
              
            String pretty = gen.getAsString();
            system.debug('HTTP Response---'+ pretty);
              
            // Send Body
            res.responseBody = Blob.valueOf(pretty);
         }
}