/*
Test class for field update in Opportunity based on Opportunitylineitem. 
@Method :- allTheDataForThisTestClass
        Creating all the required data for the test class   
@Method :- Test_Method
*/
@isTest
public class TestOppLineItemtrg {

    @testSetup
        Static void allTheDataForThisTestClass(){
        List<Product2> prd_List = new List<Product2>();
        list<pricebookentry> pbr_list = new list<pricebookentry>();    
   Id pricebookId = Test.getStandardPricebookId();
        Product2 prd = new Product2();
        prd.Name = 'TestParent';
        prd.ProductCode = 'TestParent';
        prd.IsActive = true;
        prd_List.add(prd);
        
            for(integer i = 0; i<3;i++){
            Product2 pd = new Product2();
            pd.Name = 'TestChild'+i;
            pd.ProductCode = 'TestChild'+i;
            pd.IsActive = true;
           prd_list.add(pd);  
        }
        insert prd_List;
        
            list<product2> products = [select id,name from product2 where name 
                                       like 'TestChild%'];
             product2 pro = [select id,name from product2 where ProductCode = 'TestParent']; 
                                       
       list<ST_ProductOption__c> prdOptions  = new list<ST_ProductOption__c>();
        for(Product2 pd1 : products){
        ST_ProductOption__c Po = new ST_ProductOption__c();
        po.Configured_SKU__c = pro.id;
        po.Optional_SKU__c = pd1.id;
        po.Type__c = 'Component';
        po.Selected__c = true;
        po.Bundled__c = true;
            po.Quantity__c = 1;
            prdOptions.add(po);
            pricebookentry bookentry = new pricebookentry();
            bookentry.Pricebook2Id = pricebookId;
            bookentry.Product2Id = pd1.id;
            bookentry.UnitPrice = 0;
            bookentry.IsActive = true;
            pbr_list.add(bookentry);
        }
        insert pbr_list;
        insert prdoptions;
        list<ST_ProductOption__c> stpo = [select id,configured_sku__c,Optional_SKU__c from ST_ProductOption__c
                                        where configured_sku__c =: prd.Id  ] ;
        system.debug('productoptions---->'+ stpo.size());
        system.debug('productoptions---->'+stpo);
        
        account acc = new account();
        acc.RecordTypeId = '0121a000000ERIK';
        acc.Name = 'Test Customer';
        insert acc;    
            
        opportunity opp = new opportunity();
        opp.RecordTypeId = '0121a0000002PJr';
        opp.Name = 'Test Opportunity';
        opp.AccountId = acc.id;
        opp.CloseDate = system.today()+5;
        opp.StageName = 'Prospect';
        insert opp;
            
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = 
                       pricebookId, Product2Id = prd.Id, UnitPrice = 0, IsActive = true);
        insert standardPBE;

      
           list<Product2> prod2 = [select id,(select optional_sku__c from options__r)
                           from product2 where id IN (select configured_sku__C from ST_ProductOption__c)];
                system.debug('ProductRelatedChild------>'+prod2); 
        list<ST_ProductOption__c> Prodop = [select optional_sku__c,configured_sku__c 
                           from ST_ProductOption__c where configured_sku__c =: pro.id ];
        system.debug('ProductRelatedChild------>'+prodop);
        }
    
    static TestMethod void Test_Method(){
        
        
        product2 pp = [select id,name from product2 where name = 'TestParent'];
        pricebookentry pbe = [select id from pricebookentry where product2id =: pp.id ];
        opportunity opp1 = [select id from opportunity where name = 'Test Opportunity' limit 1];
      
        OpportunityLineItem oli = new OpportunityLineItem();
   
        oli.OpportunityId = opp1.id;
        oli.PricebookEntryId = pbe.id;
        oli.Quantity = 1;
        oli.TotalPrice = 0;
        insert oli;
   list<OpportunityLineItem> oppli = [select id,name, product2id, pricebookentryid
                                      from OpportunityLineItem ];
        system.debug('opplineitem---->'+ oppli );
        system.debug('opplineitem size---->'+ oppli.size());
       // system.assertEquals(4,oppli.size());
        
        list<OpportunityLineItem> listoli= new list<OpportunityLineItem>();
        for(OpportunityLineItem ol : oppli){
            if(ol.product2id == pp.id){
                listoli.add(ol);
            }
        }
        
        system.debug('list to delete------>'+ listoli);
        delete listoli; 
        list<OpportunityLineItem> output = [select id,name,Product2id,opportunityId 
                                          from OpportunityLineItem ];
        system.debug('delete output----->After Deletion---->'+output);
    
    }
    

}