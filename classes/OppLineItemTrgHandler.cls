/* This Method is used to show the Product package on Account level 
* Object: Opportunity, OpportunityProduct, Account
* Test Class: OppLineItemtrgTest
* Trigger Name : OpportunityLineItem
*/
public class OppLineItemTrgHandler {
    
    public static void updateCoreProductonAccount(list<OpportunityLineItem> triggerNew){
        Set<Id> oppId = new Set<Id>();
        
        list<Account> UpdateAccList = new list<Account>();
         Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        // Storing OpportunityId and ProductId from OLI
        for(OpportunityLineItem oli : triggerNew) {
            oppId.add(oli.OpportunityId);
        }
        system.debug('in Update');
        system.debug('@@OppId'+OppId);
        for(Opportunity opp : [Select 
                               Id, AccountId,Account.Custom_Package__c, Account.Products_IN_OpportunityLineItem__c, 
                               (Select Id,Custom_Package__c, Product2.Name, Product2.Core_Product__c, Opportunity.AccountId from OpportunityLineItems)
                               From Opportunity where Id IN : oppId AND RecordTypeID =: OppRecordTypeId]){ 
                                   String productName = '';
                                   String CustomPackage = '';
                                   for(OpportunityLineItem oli : opp.OpportunityLineItems) {
                                       if(oli.Product2.Core_Product__c == true && oli.Product2.Name != label.Managed_Tech) 
                                           productName = oli.Product2.name;
                                           CustomPackage = oli.Custom_Package__c;
                                   }
                                   UpdateAccList.add(new account(Id = opp.AccountId, Products_IN_OpportunityLineItem__c = productName, Custom_Package__c = CustomPackage ));
                               }
        if(UpdateAccList.size() > 0) {
            update UpdateAccList;
            system.debug('acclits'+UpdateAccList);
        }
    }
    Public static void errorHandlerMethodforDuplicateProducts(List<OpportunityLineItem> newOppLineItms ){
        
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        String errorLabel = System.label.DuplicateProductsError;
        
        //Store Product id's with Opportunity id's 
        Map<Id,List<Id>> mapProduct = new Map<Id,List<Id>>();
        Set<Id> setOppIds = new Set<Id>();
        //get Opp id for new opps and store in set
        for(OpportunityLineItem oli: newOppLineItms){
                setOppIds.add(oli.OpportunityId);              
        }
        // get productid for each opp and put it in Map
        for(OpportunityLineItem oli: [Select Id, Product2Id, OpportunityId, Opportunity.RecordTypeId from OpportunityLineItem where OpportunityId IN : setOppIds and Opportunity.RecordTypeId = :OppRecordTypeId]){
            if(mapProduct.containsKey(oli.OpportunityId))
                mapProduct.get(oli.OpportunityId).add(oli.Product2Id);
            else
                mapProduct.put(oli.OpportunityId,new List<Id>{oli.Product2Id});
        }
        // get the Productid for new OLI and check for duplicates
        for(OpportunityLineItem newOli: newOppLineItms){
            if(mapProduct.containsKey(newOli.OpportunityId)){
                for(Id prodId: mapProduct.get(newOli.OpportunityId)){
                    if(newOli.Product2Id.equals(prodId))
                        newOli.addError(errorLabel);
                }
            }
        }
        
    } 
    Public static void errorHandlerMethodforCoreProducts(List<OpportunityLineItem> newOppLineItms ){
        
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        String errorLabel = System.label.MultipleCoreProductError;
        Set<Id> OppId = new Set<Id>();
        Set<Id> prodId = new Set<Id>();
        
        for(OpportunityLineItem oppline : newOppLineItms) {
                OppId.add(oppline.OpportunityId);
                prodId.add(oppline.Product2Id);
        }
        List<Opportunity> listOpportunity = [Select
                                             Id, Name, (Select Id, name, Product2Id, Product2.Core_Product__c from OpportunityLineItems where Product2.Core_Product__c = True)
                                             From Opportunity
                                             Where Id IN : OppId and RecordTypeId = :OppRecordTypeId]; 
        for(Opportunity opp : listOpportunity) {
            for(OpportunityLineItem opLine : opp.OpportunityLineItems) {
                prodId.add(opLine.Product2Id);
            }
        }
        list<Product2> productlist = [Select 
                                      Id, name, Core_Product__c 
                                      From Product2 
                                      Where Id IN : prodId AND Core_Product__c = True];
        
        for(OpportunityLineItem oppline : newOppLineItms) { 
            if(productlist.size() >= 2) {
                oppline.addError(errorLabel);
            }
        }
    }   
}