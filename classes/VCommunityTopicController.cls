public class VCommunityTopicController 
{
    
    /*Get total topic Ideas*/
    @AuraEnabled
    public static Integer getTotalTopicIdeas(String topicId) {
        Integer TotalIdeas = 0;
        try {
            
            Id currentUserId = userinfo.getuserid();
            if(topicId != ''){
                List<Community> zoneList = [SELECT CreatedById, CreatedDate, Description, Id, IsActive, LastModifiedById,
                     LastModifiedDate, Name, NetworkId, SystemModstamp 
                     FROM Community WHERE NetworkId = :Network.getNetworkId() AND IsActive = true
                    ];
                
                List<Topic> topicList = [Select Id, Name From Topic Where Id = :topicId And NetworkId = :Network.getNetworkId() Limit 1];
                
               	if(!zoneList.isEmpty() && !topicList.isEmpty()){
                    Id selectedZone = zoneList[0].Id;
                    
                    String selectedCategory = topicList[0].Name;
                    TotalIdeas = (Integer)Database.Query('SELECT count(Id) TotalCount FROM Idea WHERE IsDeleted = false AND CommunityId = :selectedZone And Categories includes (:selectedCategory)')[0].get('TotalCount');
                }
            }
        } catch (Exception e) {
            System.debug(e);
        }
        return TotalIdeas;
    }
    
    /*Get total topic discussions*/
    @AuraEnabled
    public static Integer getTotalTopicDiscussions(String topicId) {
        Integer totalDiscussions = 0;
        try {
            if(topicId != ''){
                Id selectedZone = Network.getNetworkId();
                String entityType = 'FeedItem';
                
                totalDiscussions = (Integer)Database.Query('SELECT count(Id) TotalCount FROM TopicAssignment WHERE NetworkId = :selectedZone And TopicId = :topicId And EntityType = :entityType')[0].get('TotalCount');
            }
        } catch (Exception e) {
            System.debug(e);
        }
        return totalDiscussions;
    }

}