/*
	Test
*/
@IsTest 
public class VCaseCommentNewExtensionTest 
{
	@IsTest(SeeAllData=false)
    public static void testNewComment() 
    {
        Account account = new Account();
    	account.name = 'Test Account 120934701234';
    	insert account;
    	
    	Contact contact = new Contact();
    	contact.accountId = account.id;
    	contact.firstname = 'Contact First 132412434';
    	contact.lastname = 'Contact Last 1234142314';
    	contact.email = 'test@coupa1238103.com';
    	insert contact;
    	
    	Case newcase = new Case();
    	newcase.accountId = account.id;
    	newcase.contactId = contact.id;
    	newcase.subject = 'Test Case';
    	newcase.description = 'Test';
    	newcase.status = 'New';
    	insert newcase;

        VCaseCommentNewExtension ctrl = new VCaseCommentNewExtension(new ApexPages.StandardController(newcase));
    	
        test.startTest();
        
        // Private comment
        ctrl.comment.CommentBody = 'Test Private';
        ctrl.savePrivateComment();
        
        // Public comment
        ctrl.comment.CommentBody = 'Test Public';
        ctrl.savePublicComment();
    	
    	test.stopTest();
    }
}