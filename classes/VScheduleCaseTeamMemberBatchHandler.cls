/*
	Notes:
		- Schedule Jobs: 
			System.Schedule('Add Case Team Member - Minute 00', '0 0 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 05', '0 5 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 10', '0 10 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 15', '0 15 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 20', '0 20 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 25', '0 25 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 30', '0 30 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 35', '0 35 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 40', '0 40 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 45', '0 45 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 50', '0 50 * * * ?', new VScheduleCaseTeamMemberBatch());
            System.Schedule('Add Case Team Member - Minute 55', '0 55 * * * ?', new VScheduleCaseTeamMemberBatch());
*/
public class VScheduleCaseTeamMemberBatchHandler implements VScheduleCaseTeamMemberBatch.IScheduleDispatched
{
    public void execute(SchedulableContext sc)
    {
		try
		{
			VCaseTeamMemberBatchProcess bc = new VCaseTeamMemberBatchProcess();
			Database.executeBatch(bc, 200);
		}
		catch(Exception ex)
		{
            System.debug('##### Exception: ' + ex.getMessage());
            
			List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setUseSignature(false);
			mail.setToAddresses(new String[] {'sulung.chandra@veltig.com'});
			mail.setSubject('Schedule Add Team Member had encountered exception');
			mail.setHtmlBody(ex.getMessage());
			mailList.add(mail);
			
			Messaging.sendEmail(mailList);
		}
    } 
}