/*
	Controller to collect user feedback from the email
*/
public without sharing class VCaseEmailFeedbackRateController 
{
	private static final String QUERYNAME_CASEID = 'id';
	private static final String QUERYNAME_INTENTION = 'i';
	
	public  String caseId {get;set;}
	public  String intention {get;set;}
	public String message {get;set;}
	
	public VCaseEmailFeedbackRateController()
	{
		this.caseId = '';
		this.intention = '';
		this.message = '';
	}
    
    /*
    	Load the request when the page first loaded
    */
    public PageReference loadQueryRequest()
    {
        this.caseId = this.getQueryRequest(QUERYNAME_CASEID);
        this.intention = this.getQueryRequest(QUERYNAME_INTENTION);
        
        this.saveFeedback();
        
        return null;
    }
	
    /*
    	Get the requested query
    */
    private String getQueryRequest(String queryname)
    {
    	if(ApexPages.currentPage().getParameters().get(queryname) != null)
    	{
    		return ApexPages.currentPage().getParameters().get(queryname).trim();
    	}
    	return '';
    }
    
    /*
    	Save feedback
    */
    private void saveFeedback()
    {
		this.message = '';
		
		Map<String, String> intentionMap = new Map<String, String>{
			'-3'=>'Very Dissatisfied', 
			'-2'=>'Mostly Dissatisfied', 
			'-1'=>'Somewhat Dissatisfied', 
			'0'=>'Neutral', 
			'1'=>'Somewhat Satisfied', 
			'2'=>'Mostly Satisfied', 
			'3'=>'Very Satisfied'
		};
		
		if((this.caseId != '') && (this.intention != ''))
		{
			List<Case> caseList = [Select Id, Rate__c, IsClosed From Case Where Id = :this.caseId Limit 1];
			if(!caseList.isEmpty())
			{
				if(caseList[0].Rate__c != null && caseList[0].Rate__c != '')
				{
					if(intentionMap.containsKey(caseList[0].Rate__c))
                        this.message = 'This case has been rated ' + intentionMap.get(caseList[0].Rate__c);
                    else
                        this.message = 'This case has been rated ' + caseList[0].Rate__c;
				}
				else
				{	
					List<Case_Rating__c> crList = new List<Case_Rating__c>();
					Case_Rating__c cr = new Case_Rating__c();
					cr.Case__c = caseList[0].Id;
                    cr.Case_ID__c = caseList[0].Id;
					
					if(intentionMap.containsKey(this.intention))
					{
						cr.Rate__c = this.intention;
						crList.add(cr);
					}
					
					if(!crList.isEmpty())
					{
						Savepoint sp = Database.setSavepoint();
		
						try
						{
							upsert crList Case_ID__c;
							this.message = 'Your rating has been saved as ' + intentionMap.get(this.intention);
						}
						catch(Exception ex)
						{
							Database.rollback(sp);
							
							this.message = ex.getMessage();
						}
					}
					else
					{
						this.message = 'Incorrect rating.';
					}
				}
			}
			else
			{
				this.message = 'Case not found.';
			}
		}
		else
		{
			this.message = 'Invalid request.';
		}
    }
    
}