/*
 #####################################################
 # Created By....................: Vishal Labh
 # Created Date................: 18 Jan, 2019
 # Description...................: This is Invocable class for PB "Update_Account_Size_Bucket_from_Est_Tech_Field"
 # Test Class...................: ST_UpdateAccountSizeTest
 # Jira Story....................: SDM-104
 #####################################################
*/
public class ST_UpdateAccountSize {
    @InvocableMethod
    public static void updateSizeOfAcc(List<Account> accList) {
        set<id> accountids = new set<id>();
        List<Account> accountUpdateList = new List<Account>();
        for(Account acc : accList){
            accountids.add(acc.id);
        }
        // Fethcing accounts with opportuinity having record type =sales
        for(Account accObj : [Select Id, Estimated_Total_Potential_Technicians__c, (Select Total_Projected_MRR__c from Opportunities where RecordType.Name = 'Sales') from Account Where ID IN : accountids]) {  
            Boolean isTotalProjMMRNull = false;  
            for(Opportunity oppObj :  accObj.Opportunities) {
                if(oppObj.Total_Projected_MRR__c == null || oppObj.Total_Projected_MRR__c == 0) {
                    isTotalProjMMRNull = true;
                }
            }
            if(isTotalProjMMRNull == True) {  
           //validating  Estimated_Total_Potential_Technicians__c 
                if(accObj.Estimated_Total_Potential_Technicians__c == null){
                    accObj.Account_Size__c = 'Unknown';
                }
                else if(accObj.Estimated_Total_Potential_Technicians__c >=1 && accObj.Estimated_Total_Potential_Technicians__c <=4 ){
                    accObj.Account_Size__c = '1-4'; 
                }
                else if(accObj.Estimated_Total_Potential_Technicians__c >=5 && accObj.Estimated_Total_Potential_Technicians__c <=10){
                    accObj.Account_Size__c = '5-10';
                }
                else if(accObj.Estimated_Total_Potential_Technicians__c >=11 && accObj.Estimated_Total_Potential_Technicians__c <=25){
                    accObj.Account_Size__c = '11-25';
                }
                else if(accObj.Estimated_Total_Potential_Technicians__c > 25 ){
                    accObj.Account_Size__c = '>25';
                }
                accountUpdateList.add(accObj);
            }
        }  
        // Updating Accounts
        if(!accountUpdateList.isEmpty()) {
            Update accountUpdateList;
        }       
    }
    
}