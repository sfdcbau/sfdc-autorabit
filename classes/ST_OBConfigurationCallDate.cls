/***********************************************
* @author    : Kamal Singh
* @testclass : ST_OBConfigurationCallDateTest
* @PB Name   : Calendly_Event_Updates_Onboarder_Field
* @Created   : 03/11/2019
* @version   : V.1.0
* @Modified Date: 03/14/2019
* @Version   : V.1.1 
* @User Story: SE-2471
* @Version   : V.1.2
* @UserStory : SE-2960
* @Modified Date : 04/12/2019
* *********************************************/
public class ST_OBConfigurationCallDate {
    
    @InvocableMethod
    
    //method to update most recent onboarding OB Configuration Call Date
    public static void populateOBConfigurationCall(List<Event> listOfEvents){
        Set<Id> whoIdlist = new Set<Id>();
        Set<Id> accId = new Set<Id>();
        Id OnboardingRecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
        Date updateEventDate ;
        for(Event eachEvent : listOfEvents) {
            whoIdlist.add(eachEvent.WhoId);
            updateEventDate = eachEvent.ActivityDate;
        }
        //retreiving contacts list based on event whoId.....
        List<Contact> listOfContacts = [SELECT 
                                        Id, AccountId, Account.customer_status_picklist__c 
                                        From Contact 
                                        Where Id IN : whoIdlist];
        //iterating the contacts list to check Account customer status.....
        for(Contact con : listOfContacts) {
            if( con.Account.customer_status_picklist__c == 'Stage Zero' || con.Account.customer_status_picklist__c == 'CC Processed' || con.Account.customer_status_picklist__c == 'SE Vetting' || con.Account.customer_status_picklist__c == 'Discovery Call' || con.Account.customer_status_picklist__c == 'Q&A / DEMO'
               || con.Account.customer_status_picklist__c == 'Pricing / ROI Assessment' || con.Account.customer_status_picklist__c == 'Negotiation / Pending' || con.Account.customer_status_picklist__c == 'Onboarding'){
                   AccId.add(con.Accountid);
               }
        }
        if(AccId != null && AccId.size() > 0) {
            
            //query for most recent onboarding.....
            Onboarding__c[] obToUpdate = [SELECT 
                                          Id, Name, Account__c, 
                                          OB_Configuration_Call_Date__c,
                                          Onboarding_Status__c,
                                          Initial_OB_Configuration_Call_Date__c
                                          From Onboarding__c 
                                          Where Account__c IN :AccId AND RecordTypeId = : OnboardingRecordTypeId 
                                          AND (Onboarding_Status__c = 'Pre-Implementation' OR Onboarding_Status__c = 'Implementation')
                                          Order by CreatedDate DESC limit 1]; 
            
            
            
            // Check if Inital OB Configuartion Call Date field is null, if it is null populate with Activity Date
            
            if(obToUpdate!=null && obToUpdate.size()>0){
                
                if( obToUpdate[0].Initial_OB_Configuration_Call_Date__c== null){
                    obToUpdate[0].Initial_OB_Configuration_Call_Date__c = updateEventDate; 
                }
                
                obToUpdate[0].OB_Configuration_Call_Date__c = updateEventDate;
                update obToUpdate;
            }
        }
    }
}