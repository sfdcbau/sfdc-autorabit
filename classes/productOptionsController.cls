/*
* Custom Controller class for ProductOptions VF page.
* This controller class fetches all the related product options
  of Products 
* Test Class: ProductOptionsCtrTest
*/
public Without sharing class productOptionsController {
public    Opportunity  Opp ;
    Public pagereference Back_To_Opportunity {get;set;}
  public  List<OpportunityLineItem> OppLineItemList = new List<OpportunityLineItem>();
  public Set<ID> Oli_Ids = new Set<ID>();
  public List<ID> ProductIds = new list<Id>();
  public  Map<ID,ID> ChildProductToParentIds = new Map<id,id>(); 
  public  list<Product2> ProductList ;
  public list<Product2> products ;
  public  Map<Product2,List<Product2>> ProductToChildProductsName {get; set;} 
  public Map<Product2,OpportunityLineItem> ProductToOpportunityLineItemMap {get;set;} 
 public productOptionsController(){
    ProductToChildProductsName = new Map<Product2,List<Product2>>();
     ProductToOpportunityLineItemMap = new Map<Product2,OpportunityLineItem>();
     Opp = [select id from Opportunity 
                where Id =: ApexPages.currentPage().getParameters().get('ID')];
     system.debug('oppid----->'+opp.id);
 OppLineItemList = [select id,Product2ID,OpportunityId,Quantity,unitprice,MRR__c from OpportunityLineItem
                   where OpportunityId =: Opp.Id];
     system.debug('oppLineitemList----->'+oppLineitemList);
     
     for(OpportunityLineItem oli : OppLineItemList){
         Oli_Ids.add(oli.product2id);
     }
   ProductList = [select id,name,(select Optional_SKU__c from Options__r) 
                 from Product2 where ID IN: Oli_Ids ];
     system.debug('ProductList----->'+ProductList);
     for(Product2 prd : ProductList){
         if(!Prd.Options__r.iSEmpty()){
             for(ST_ProductOption__c po : Prd.Options__r ){
                 ProductIds.add(po.Optional_SKU__c);
                 ChildProductToParentIds.put(po.Optional_SKU__c, prd.id);
             } 
         }
       
     }
     system.debug('productids----->'+productids);
      products = [select Id,Name from product2 
                                where Id IN: ProductIds ];
     system.debug('products------>'+products);
     for(Product2 parent : ProductList){
     list<Product2> prodName  = new list<Product2>();    
         if(!Parent.Options__r.iSEmpty()) { 
             //prodname.clear();
     for(product2 child : products){
          
         system.debug('parentid------>'+parent.id);
         system.debug('ChildProductToParentIds.get(child.id)----->'+
                      ChildProductToParentIds.get(child.id));
         if(parent.id == ChildProductToParentIds.get(child.id)){
             system.debug('in loop--->'+child);
             
                 prodName.add(child);
         }  
        }
              ProductToChildProductsname.put(parent,prodName);
             system.debug('end result------>'+ProductToChildProductsname);
             
     } 
         
   }
     for(OpportunityLineItem oli : OppLineItemList){
     for(product2 prd : ProductToChildProductsName.keySet()){
         if(prd.ID == oli.Product2Id ){
             ProductToOpportunityLineItemMap.put(prd, oli);
            
         }
             }
     }
     }
    //Command Button Logic
     Public pagereference Back_To_Opportunity(){
         
         return(new pagereference(system.label.Domain_URL+
                                 opp.ID).setRedirect(true));
        
     }

}