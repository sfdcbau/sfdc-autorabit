@isTest
Private class CreateOrEditContactControllerTest {
    static testmethod void CreateOrEditContact(){
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        
        list <Account> accList = new list<Account>();
        accList.add(acc);
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        system.debug('===>OPPRT' +OppRT );
        
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', RecordTypeId= OppRT, CloseDate=Date.today(), AccountId = acc.id);
        insert opp1;
        Contact Con = new Contact (LastName='Con1',Role__c='Owner', AccountId = acc.id);
        insert Con;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.CreateOrEditContact')); 
        System.currentPageReference().getParameters().put('oppId', opp1.Id);
        System.currentPageReference().getParameters().put('accid', acc.Id);
        CreateOrEditContactController obj = new CreateOrEditContactController();
        //obj.addContact();
        obj.getRoles();
        obj.objContactWrapper.lastName = 'Test';
        obj.objContactWrapper.Role = 'Owner';
        obj.addContact();
        obj.listWrapper[0].checked = true;
        obj.updateOpp();
        obj.updateContact();
        Test.stopTest(); 
    }   
}