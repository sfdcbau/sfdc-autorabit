/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a handler class for Account History Tracking.
*
* This class helps create a new AccountHisotryTracking Record when a specific field is changed that is designated for tracking.
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Andrew Huynh<ahuynh@servicetitan.com>
* @version        1.0
* @created        06-19-2018
* @Name           AccountHistoryTrackerHandler
* @Method         handleCreateAccHistory()
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class AccountHistoryTrackerHandler {
    private static Boolean bool = false; 
    
    public static void handleCreateAccHistory(list<account> oldValues, list<account> newValues){
        
        system.debug('old values ====>' + oldValues);
        system.debug('new values ====>' + newValues);
        
        //Check if Trigger is On
        List<Trigger_Settings__mdt> TS = new list<Trigger_Settings__mdt>();
        
        TS = [SELECT Label, isActive__c FROM Trigger_Settings__mdt WHERE Label = 'ST_AccountHistoryTracking'];
        
        for(Trigger_Settings__mdt sett : TS){
            if(sett.isActive__c == true){
                bool = true;
            }
        }
        
        System.debug('bool is ====> ' + bool);
        
        if(bool == true){
            
            final List<Schema.FieldSetMember> trackedFields = SObjectType.Account.FieldSets.History_Tracking.getFields();
            final map<id,Account> oldIdAccMap = new map<id,Account>();
            Account oldAccount = new account();
        
            if(!trackedFields.isEmpty()){ 
                
                for(Account oa : oldValues){
                    oldIdAccMap.put(oa.id, oa);
                }
                
                final List<AccountHistoryTracking__c> fieldChanges = new List<AccountHistoryTracking__c>();
        
                for (Account newAccount : newValues) {
                    
                    if(oldIdAccMap.keyset().size() > 0){
                        oldAccount = oldIdAccMap.get(newAccount.id);
                    }                    
                    system.debug('oldAccount is ====> ' + oldAccount);
            
                    for (Schema.FieldSetMember fsm : trackedFields) {
            
                        String fieldName  = fsm.getFieldPath();
                        String fieldLabel = fsm.getLabel();
                        
                        if(fieldName != null){
                            system.debug('Field name of newAccount ====> ' + newAccount.get(fieldName));
                            system.debug('Field name of oldAccount ====> ' + oldAccount.get(fieldName));
                
                            if (newAccount.get(fieldName) != oldAccount.get(fieldName)){
                
                                String oldValue = String.valueOf(oldAccount.get(fieldName));
                                String newValue = String.valueOf(newAccount.get(fieldName));
                                
                                if(oldValue != null){
                                    system.debug('oldValue is ====> ' + oldValue);
                                }
                                
                                if(newValue != null){
                                    system.debug('newValue is ====> ' + newValue);
                                }
                    
                                if (oldValue != null && oldValue.length()>255){ 
                                    oldValue = oldValue.substring(0,255);
                                }
                                
                                if (newValue != null && newValue.length()>255){ 
                                    newValue = newValue.substring(0,255); 
                                }
                                
                                final AccountHistoryTracking__c accountHistory = new AccountHistoryTracking__c();
                    
                                accountHistory.name                         = fieldLabel;
                                accountHistory.Related_Account__c           = newAccount.Id;
                                accountHistory.Changed_By__c                = UserInfo.getUserId();
                                accountHistory.Old_Value__c                 = oldValue;
                                accountHistory.New_Value__c                 = newValue;
                                accountHistory.Date_Time_of_Field_Change__c = datetime.now();
                    
                                fieldChanges.add(accountHistory);
                            }
                            
                        }
                        
                    }
                }
               
                if (!fieldChanges.isEmpty()) {
                    insert fieldChanges;
                }
                
            }
            
        }
    }
}