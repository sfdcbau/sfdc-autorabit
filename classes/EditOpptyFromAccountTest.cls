@isTest
public class EditOpptyFromAccountTest {
    
  @isTest
    public static void testupdateOppty(){
        //create test data
        //Insert account
        Account act1=new Account(Name='test');
        Insert act1;
         Id oppSTPayements = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        
        //Insert oppertunity related to that account
        Opportunity op1=new Opportunity (Name='testOpportunity',AccountId=act1.Id,StageName='Stage Zero',CloseDate=System.today(),Of_Tech__c=1, recordtypeid=oppSTPayements);
        insert op1;
        
        
        List<ID> ids=new list<ID>{act1.Id};
        //Test the method
        Test.startTest();
        EditOpptyFromAccount.updateOppty(ids);
        Test.stopTest();
    }
    static testmethod void  TestInsertopp(){
    
     
     Account acNew = new Account(Time_Zone__c = 'Pacific (UTC-8:00)',Customer_Status_picklist__c ='Stage Zero', Software_newpicklist__c = 'Acowin',Pricing_Model__c = 'New Pricing (Managed Techs)',Residential__c = 10,Initial_Username__c = 'ska',Tenant_ID__c  = '23456789',Tenant_Name__c = 'Tenant',Estimated_Total_Potential_Technicians__c = 2,Estimated_Total_Potential_Office_Users__c = 1,Name = 'Test Account', BillingCity='Gwl', BillingState = 'Uttar pradesh', BillingCountry='india', Industry__c = 'Plumbing', No_Known_Active_Partners__c = true, Influencer__c = 'Unknown');
        insert acNew;
        
        Contact con =new Contact(Lastname ='Tets',accountid=acNew.id,Role__c = 'Owner', email='test@gmail.com');
        insert con;
       
        //get standard pricebook
        Id pricebookId = Test.getStandardPricebookId();

        Product2 prd1 = new Product2 (Name='Managed Tech',Description='Test Product Entry 1',productCode = 'ABC', isActive = true,core_product__c = true);
        insert prd1;
        
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
        insert pbe1;
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        //ServiceTitan Payments
        Id oppSalesRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Opportunity opp1 = new Opportunity (Competing_Software__c = 'AppointmentPlus',Delayed_Implementation__c = true,recordtypeid = OppRT,ImplementationDate__c = system.today(),Name='Opp1',StageName='Stage Zero',Onboarding_POC__c=con.id, CloseDate=Date.today(),Pricebook2Id = pbe1.Pricebook2Id, AccountId = acNew.id);
        insert opp1;
         
        list<Opportunity> oppNewList = new list<Opportunity>();
        oppNewList.add(opp1);
         
        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=pbe1.id , quantity=35, totalprice=200 ,Product2id = prd1.id);
        insert lineItem1;   
             
        list<opportunity> opptest = [SELECT Id, StageName FROM opportunity WHERE Id =: opp1.Id limit 1 ];
        
         opptest[0].StageName='Closed Won';
         opptest[0].recordtypeid = oppSalesRT;
         opptest[0].Address_Industry_Verified__c = true;
         opptest[0].OBDiscoveryCallDate__c = System.today() + 30;
         opptest[0].AE_Discovery_POC__c = con.id;
        
         //opptest[0].Legacy_Opportunity_Id__c = '1245678';
         
         update opptest;
         
         Onboarding__c obSales = new Onboarding__c();
         obSales.Account__c = acNew.id;
         obsales.OB_Discovery_Call_Date__c = system.today()+7;
         obsales.Primary_Contact__c = con.id;
         obsales.name= 'Managed';
         obSales.Opportunity__c = opp1.id ;
         obSales.Live_Date__c = system.today() - 90;
         obSales.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId(); 
         insert obSales; 
        
    }
}