Public Class VCommunityAnnouncementController
{

    /*Get total Announcements*/
    @AuraEnabled
    public static Integer getTotalAnnouncements() {
        Integer totalAnnouncements = 0;
        try {
            String lang = 'en_US';
            String publishStatus = 'Online';
            totalAnnouncements = (Integer)Database.Query('Select Count(Id) TotalCount From Announcement__kav Where Language = :lang And PublishStatus = :publishStatus')[0].get('TotalCount');
        } catch (Exception e) {
            System.debug(e);
        }
        return totalAnnouncements;
    }

    /*Get Announcements*/
    @AuraEnabled
    public static List<Announcement__kav> getAnnouncements(Integer announcementsPerPage, Integer selPageNo) {
        Integer recordsPerPage = integer.valueof(announcementsPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recordsPerPage;
        List<Announcement__kav> AnnouncementList = new List<Announcement__kav>();
        try {
            String lang = 'en_US';
            String publishStatus = 'Online';
            AnnouncementList = Database.Query('Select Id, ArticleType, KnowledgeArticleId, Summary, Title, UrlName, LastPublishedDate From Announcement__kav Where Language = :lang And PublishStatus = :publishStatus Order By LastPublishedDate Desc NULLS LAST LIMIT :recordsPerPage offset :pageoffset');
        } catch (Exception e) {
            System.debug(e);
        }
        return AnnouncementList;
    }

}