public with sharing class VCommunityIdeaController {
    /*Get the URL to force login*/
    @AuraEnabled
    public static String getLoginURL(String returnURL) {
        PageReference pr = Network.forwardToAuthPage(returnURL);
        return pr.getUrl();
    }
    
    /*Current Community Zone*/
    @AuraEnabled
    public static List<Community> getCurrentCommunityZones() {
        List<Community> zones = new List<Community>();
        try {
            zones = [SELECT CreatedById, CreatedDate, Description, Id, IsActive, LastModifiedById,
                     LastModifiedDate, Name, NetworkId, SystemModstamp 
                     FROM Community WHERE NetworkId = :Network.getNetworkId() AND IsActive = true
                    ];
        } catch (Exception e) {
            System.debug(e);
        }
        
        return zones;
    }
    
    /*Get categories picklist options*/
    @AuraEnabled
    public static List<SelectOptionDetail> getIdeaCategorySelectOptions(){
        List<SelectOptionDetail> mylist = new List<SelectOptionDetail>();
        
        /*
        Schema.DescribeFieldResult fr = Idea.Categories.getDescribe();
        List<Schema.PicklistEntry> pleList = fr.getPicklistValues();
			
        for(Schema.PicklistEntry ple : pleList)
        {
            mylist.add(new SelectOptionDetail(ple.getLabel(), ple.getValue()));
        }
        */
        
        List<String> names = new List<String>(Customer_Community_Idea_Category__c.getAll().keySet());
        names.sort();
        
        for(String name : names)
        {
            Customer_Community_Idea_Category__c cs = Customer_Community_Idea_Category__c.getAll().get(name);
            mylist.add(new SelectOptionDetail(cs.Name, cs.Name));
        }
            
        return mylist;
    }
    
    /*Get status picklist options*/
    @AuraEnabled
    public static List<SelectOptionDetail> getIdeaStatusSelectOptions(){
        List<SelectOptionDetail> mylist = new List<SelectOptionDetail>();
        
        List<String> names = new List<String>(Customer_Community_Idea_Status__c.getAll().keySet());
        names.sort();
        
        for(String name : names)
        {
            Customer_Community_Idea_Status__c cs = Customer_Community_Idea_Status__c.getAll().get(name);
            mylist.add(new SelectOptionDetail(cs.Name, cs.Name));
        }
            
        return mylist;
    }
    
    /*Current community Zone Id's*/
    @AuraEnabled
    public static List<Id> getCurrentCommunityZoneIDs() {
        List<Id> zones = new List<Id>();
        try {
            List<Community> zoneRecords = getCurrentCommunityZones();
            zones = new List<Id>();
            for (Community cmt : zoneRecords) {
                zones.add(cmt.Id);
            }
        } catch (Exception e) {
            System.debug(e);
        }
        
        return zones;
    }
    
    /*Get My Ideas*/
    @AuraEnabled
    public static List<Idea> getMyIdeas() {
        List<Idea> myIdeas = new List<Idea>();
        try {
            myIdeas = [SELECT Id, Title, Status, RecordTypeId, CreatedById, CreatedDate, CreatorName,
                    CreatorSmallPhotoUrl, Body, Categories, NumComments, VoteScore, VoteTotal 
                    FROM Idea WHERE CreatedById = :UserInfo.getUserId() AND CommunityId IN :getCurrentCommunityZoneIDs() And IsMerged = false
                    ORDER BY VoteTotal DESC NULLS LAST LIMIT 10
            ];
        } catch (Exception e) {
            System.debug(e);
        }
        return myIdeas;
    }
    
    /*Get total Ideas*/
    @AuraEnabled
    public static Integer getTotalIdeas(Integer ideasPerPage, String selectedFilter, Integer selPageNo, String selectedZone, String selectedCategory, String selectedStatus, String keyword) {
        Integer recordsPerPage = integer.valueof(ideasPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recordsPerPage;
        
        Integer TotalIdeas = 0;
        try {
            
            Id currentUserId = userinfo.getuserid();
            String additionalFilter = '';
            
            if(selectedCategory != null && selectedCategory != ''){
                additionalFilter = additionalFilter + ' And Categories includes (:selectedCategory) ';
            }
            
            if(selectedStatus != null && selectedStatus != ''){
                additionalFilter = additionalFilter + ' And Status = :selectedStatus ';
            }
            
            if(keyword != null && keyword != ''){
                String keywordString = '%' + String.escapeSingleQuotes(keyword) + '%'; 
                additionalFilter = additionalFilter + ' And Title Like :keywordString ';
            }
            
            if(selectedFilter == 'Recent' || selectedFilter == 'Popular'){
                TotalIdeas = (Integer)Database.Query('SELECT count(Id) TotalCount FROM Idea WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone' + additionalFilter)[0].get('TotalCount');
            }else if(selectedFilter == 'my'){
                TotalIdeas = (Integer)Database.Query('SELECT count(Id) TotalCount FROM Idea WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone and CreatedById =: currentUserId' + additionalFilter)[0].get('TotalCount');
            }
        } catch (Exception e) {
            System.debug(e);
        }
        return TotalIdeas;
    }
    
    /*Get Ideas*/
    @AuraEnabled
    public static List<Idea> getIdeasList(Integer ideasPerPage, String selectedFilter, Integer selPageNo, String selectedZone, String selectedCategory, String selectedStatus, String keyword) {
        system.debug('----Venky: '+selectedZone);
        Integer recordsPerPage = integer.valueof(ideasPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recordsPerPage;
        List<Idea> IdeasList = new List<Idea>();
        try {
            
            Id currentUserId = userinfo.getuserid();
            String additionalFilter = '';
            
            if(selectedCategory != null && selectedCategory != ''){
                additionalFilter = additionalFilter + ' And Categories includes (:selectedCategory) ';
            }
            
            if(selectedStatus != null && selectedStatus != ''){
                additionalFilter = additionalFilter + ' And Status = :selectedStatus ';
            }
            
            if(keyword != null && keyword != ''){
                String keywordString = '%' + String.escapeSingleQuotes(keyword) + '%'; 
                additionalFilter = additionalFilter + ' And Title Like :keywordString ';
            }
            
            if(selectedFilter == null)
                selectedFilter = '';
            
            if(selectedFilter.toLowerCase() == 'recent'){
                IdeasList = Database.Query('SELECT Id, Title, Status, RecordTypeId, CreatedById, CreatedDate, CreatorName, ' +
                        'CreatorSmallPhotoUrl, CreatorFullPhotoUrl, Body, Categories, NumComments, VoteScore, VoteTotal, ' +
                        '(Select Id, ParentId, Type, CreatedDate, CreatedById From Votes where CreatedById =: currentUserId) ' +
                        'FROM Idea ' +
                        'WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone ' + additionalFilter +
                        'ORDER BY CreatedDate DESC NULLS LAST LIMIT :recordsPerPage offset :pageoffset');
            }else if(selectedFilter.toLowerCase() == 'popular'){
                 IdeasList = Database.Query('SELECT Id, Title, Status, RecordTypeId, CreatedById, CreatedDate, CreatorName, ' +
                        'CreatorSmallPhotoUrl, CreatorFullPhotoUrl, Body, Categories, NumComments, VoteScore, VoteTotal, ' +
                        '(Select Id, ParentId, Type, CreatedDate, CreatedById From Votes where CreatedById =: currentUserId) ' +
                        'FROM Idea ' +
                        'WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone ' + additionalFilter +
                        'ORDER BY VoteTotal DESC NULLS LAST LIMIT :recordsPerPage offset :pageoffset ');
            }else if(selectedFilter.toLowerCase() == 'my'){
                 IdeasList = Database.Query('SELECT Id, Title, Status, RecordTypeId, CreatedById, CreatedDate, CreatorName, ' +
                        'CreatorSmallPhotoUrl, CreatorFullPhotoUrl, Body, Categories, NumComments, VoteScore, VoteTotal, ' +
                        '(Select Id, ParentId, Type, CreatedDate, CreatedById From Votes where CreatedById =: currentUserId) ' +
                        'FROM Idea ' +
                        'WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone and CreatedById =: currentUserId ' + additionalFilter +
                        'ORDER BY CreatedDate DESC NULLS LAST LIMIT :recordsPerPage offset :pageoffset ');
            }
            
            System.debug('##### selectedFilter: '+selectedFilter);
            
        } catch (Exception e) {
            System.debug(e);
        }
        return IdeasList;
    }
    
    /*Get one user's Ideas*/
    @AuraEnabled
    public static List<Idea> getUserIdeas(String userId) {
        system.debug('----VUserId: '+ userId);
        List<Idea> IdeasList = new List<Idea>();
        try {
            IdeasList = [
                SELECT 
                    Id,
                    Title,
                    Status,
                    CreatedDate,
                    CreatorName,
                    RecordTypeId
                FROM Idea 
                WHERE CreatedById = :userId And IsMerged = false
                ORDER BY VoteTotal DESC NULLS LAST LIMIT 10
                         ];
        } catch (Exception e) {
            System.debug(e);
        }
        
        return IdeasList;
    }
    
    /*Get total topic Ideas*/
    @AuraEnabled
    public static Integer getTotalTopicIdeas(Integer ideasPerPage, Integer selPageNo, String topicId) {
        Integer recordsPerPage = integer.valueof(ideasPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recordsPerPage;
        
        Integer TotalIdeas = 0;
        try {
            
            Id currentUserId = userinfo.getuserid();
            if(topicId != ''){
                List<Community> zoneList = [SELECT CreatedById, CreatedDate, Description, Id, IsActive, LastModifiedById,
                     LastModifiedDate, Name, NetworkId, SystemModstamp 
                     FROM Community WHERE NetworkId = :Network.getNetworkId() AND IsActive = true
                    ];
                
                List<Topic> topicList = [Select Id, Name From Topic Where Id = :topicId And NetworkId = :Network.getNetworkId() Limit 1];
                
               	if(!zoneList.isEmpty() && !topicList.isEmpty()){
                    Id selectedZone = zoneList[0].Id;
                    
                    String selectedCategory = topicList[0].Name;
                    TotalIdeas = (Integer)Database.Query('SELECT count(Id) TotalCount FROM Idea WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone And Categories includes (:selectedCategory)')[0].get('TotalCount');
                }
            }
        } catch (Exception e) {
            System.debug(e);
        }
        return TotalIdeas;
    }
    
    /*Get topic Ideas*/
    @AuraEnabled
    public static List<Idea> getTopicIdeasList(Integer ideasPerPage, Integer selPageNo, String topicId) {
        Integer recordsPerPage = integer.valueof(ideasPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recordsPerPage;
        List<Idea> IdeasList = new List<Idea>();
        try {
            
            Id currentUserId = userinfo.getuserid();
            if(topicId != ''){
                List<Community> zoneList = [SELECT CreatedById, CreatedDate, Description, Id, IsActive, LastModifiedById,
                     LastModifiedDate, Name, NetworkId, SystemModstamp 
                     FROM Community WHERE NetworkId = :Network.getNetworkId() AND IsActive = true
                    ];
                
                List<Topic> topicList = [Select Id, Name From Topic Where Id = :topicId And NetworkId = :Network.getNetworkId() Limit 1];
                
               	if(!zoneList.isEmpty() && !topicList.isEmpty()){
                    Id selectedZone = zoneList[0].Id;
                    
                    if(!topicList.isEmpty()){
                        String selectedCategory = topicList[0].Name;
                        IdeasList = Database.Query('SELECT Id, Title, Status, RecordTypeId, CreatedById, CreatedDate, CreatorName, ' +
                            'CreatorSmallPhotoUrl, CreatorFullPhotoUrl, Body, Categories, NumComments, VoteScore, VoteTotal, ' +
                            '(Select Id, ParentId, Type, CreatedDate, CreatedById From Votes where CreatedById =: currentUserId) ' +
                            'FROM Idea ' +
                            'WHERE IsDeleted = false And IsMerged = false AND CommunityId = :selectedZone And Categories includes (:selectedCategory)' +
                            'ORDER BY CreatedDate DESC NULLS LAST LIMIT :recordsPerPage offset :pageoffset');
                    }
                }
            }
            
        } catch (Exception e) {
            System.debug(e);
        }
        return IdeasList;
    }
    
    /*Promote an Idea*/
    @AuraEnabled
    public static String promoteIdea(String IdeaId) {
        List<Idea> recentIdeas = new List<Idea>();
        try {
            Vote newVote = new Vote();
            newVote.Type = 'Up';
            newVote.ParentId = IdeaId;
            insert newVote;
            
        } catch (Exception e) {
            System.debug(e);
        }
        return 'Idea Promoted';
    }
    
    /*Promote an Idea from IdeaDetails page*/
    @AuraEnabled
    public static Idea promoteFromIdeaDetails(String IdeaId) {
        try {
            Vote newVote = new Vote();
            newVote.Type = 'Up';
            newVote.ParentId = IdeaId;
            insert newVote;
            
        } catch (Exception e) {
            System.debug(e);
        }
        return getIdea(IdeaId);
    }
        
    /*Demote an Idea*/
    @AuraEnabled
    public static String demoteIdea(String IdeaId) {
        List<Idea> recentIdeas = new List<Idea>();
        try {
            Vote newVote = new Vote();
            newVote.Type = 'Down';
            newVote.ParentId = IdeaId;
            insert newVote;
            
        } catch (Exception e) {
            System.debug(e);
        }
        return 'Idea Demoted';
    }
    
    /*Demote an Idea from IdeaDetails page*/
    @AuraEnabled
    public static Idea demoteFromIdeaDetails(String IdeaId) {
        try {
            Vote newVote = new Vote();
            newVote.Type = 'Down';
            newVote.ParentId = IdeaId;
            insert newVote;
            
        } catch (Exception e) {
            System.debug(e);
        }
        return getIdea(IdeaId);
    }
    
    /*Get an Idea*/
    @AuraEnabled
    public static Idea getIdea(String IdeaId) {
        Idea thisidea = null;
        try {
            thisIdea = [
                SELECT 
                    Id,
                    Title,
                    Status,
                    RecordTypeId,
                    CreatedById,
                    CreatedDate,
                    CreatorName,
                    CreatorSmallPhotoUrl,
                	CreatorFullPhotoUrl,
                    Body,
                    Categories,
                    NumComments,
                    VoteScore,
                    VoteTotal,
                	Response_Comment_ID__c,
                    (Select Id, ParentId, Type, CreatedDate, CreatedById From Votes where CreatedById =: userinfo.getuserid()),
                    (Select Id, CommentBody, CreatedById, CreatedBy.Name, CreatedDate, UpVotes From Comments where IsDeleted = false order by CreatedDate)
                FROM Idea 
                WHERE Id = :IdeaId And IsMerged = false
                LIMIT 1
            ];
        } catch (Exception e) {
            System.debug(e);
        }
        system.debug('****** thisIdea: '+thisIdea);
        return thisIdea;
    }
    
    /*get Idea Comments*/
    @AuraEnabled
    public static List<IdeaComment> getComment(String IdeaId){
        system.debug('ideaId: '+IdeaId);
        List<IdeaComment> IdeaCommentList = [Select Id, CommentBody, CreatedById, CreatedBy.Name, CreatedDate, UpVotes, CreatorSmallPhotoUrl, CreatorFullPhotoUrl,
                                             (Select Id, ParentId, Type, CreatedDate, CreatedById From Votes where CreatedById =: userinfo.getuserid())
                                             From IdeaComment where IdeaId =: IdeaId and IsDeleted = false order by CreatedDate];
        
        return IdeaCommentList;
    }
    
    /*get Idea Team Comment*/
    @AuraEnabled
    public static IdeaComment getResponseComment(String IdeaId){
        Idea i = getIdea(IdeaId);
        if(i != null){
            List<IdeaComment> IdeaCommentList = [Select Id, CommentBody, CreatedById, CreatedBy.Name, CreatedDate, UpVotes, CreatorSmallPhotoUrl, CreatorFullPhotoUrl 
                                             From IdeaComment where Id =: i.Response_Comment_ID__c and IsDeleted = false Limit 1];
            if(!IdeaCommentList.isEmpty())
            {
                return IdeaCommentList[0];
            }
        }
        return null;
    }
    
    /* Set Response Comment*/
    @AuraEnabled
    public static IdeaComment setResponseComment(String IdeaId, String commentId){
        Idea i = getIdea(IdeaId);
        if(i != null){
            try {
            	i.Response_Comment_ID__c = commentId;
                update i;
                
            } catch (Exception e) {
                System.debug('#### error: '+e);
            }
        }
        return getResponseComment(ideaId);
    }
    
    /* Remove Response Comment*/
    @AuraEnabled
    public static IdeaComment removeResponseComment(String IdeaId){
        Idea i = getIdea(IdeaId);
        if(i != null){
            try {
            	i.Response_Comment_ID__c = null;
                update i;
                
            } catch (Exception e) {
                System.debug(e);
            }
        }
        return getResponseComment(ideaId);
    }
    
    /*unlike Idea Comment*/
    @AuraEnabled
    public static List<IdeaComment> unlikeThisComment(String IdeaId, String VoteId){
        delete [select id from Vote where Id =: VoteId]; 
        
        return getComment(ideaId);
    }
    
    /* like Idea Comment*/
    @AuraEnabled
    public static List<IdeaComment> likeThisComment(String IdeaId, String commentId){
        try {
            Vote newVote = new Vote();
            newVote.Type = 'Up';
            newVote.ParentId = commentId;
            insert newVote;
            
        } catch (Exception e) {
            System.debug(e);
        }
        
        return getComment(ideaId);
    }
    
    /*Post Idea Comment*/
    @AuraEnabled
    public static List<IdeaComment> postComment(String IdeaId, String CommentBody){
        system.debug('ideaId: '+IdeaId);
        system.debug('newComment: '+CommentBody);
        IdeaComment newComment = new IdeaComment();
        newComment.IdeaId = IdeaId;
        newComment.CommentBody = CommentBody;
        insert newComment;
        
        return getComment(IdeaId);
    }
    
    /*Delete Idea Comment*/
    @AuraEnabled
    public static List<IdeaComment> deleteThisComment(String IdeaId, String commentId){
        system.debug('commentId: '+commentId);
        delete [select id from IdeaComment where Id =: commentId]; 
        
        return getComment(IdeaId);
    }
    
    /*Post Idea*/
    @AuraEnabled
    public static String postIdea(String title, String description, String zone, String categories, String status) {
        Idea newIdea = new Idea();
        system.debug('zone: '+zone);
        try {
            Community Settings = [SELECT Id, Name FROM Community where Id =: zone LIMIT 1];
            newIdea.Title = title;
            newIdea.Body = description;
            newIdea.CommunityId = Settings.Id;
            newIdea.Categories = categories;
            newIdea.Status = status;
            system.debug('newIdea: '+newIdea);
            insert newIdea;
        } catch (Exception e) {
            System.debug(e);
        }
        return newIdea.Id;
    }
    
    /*Idea Subscription*/
    @AuraEnabled
    public static Idea_Subscription__c getIdeaSubscription(String ideaId) 
    {
        List<Idea_Subscription__c> esList = [Select Id, Notify_of_Update_Type__c From Idea_Subscription__c Where Idea__c = :ideaId And Subscriber__c = :UserInfo.getUserId() Limit 1];
        if(!esList.isEmpty())
        {
            return esList[0];
        }
        return null;
    }
    
    /*Is Subscribed*/
    @AuraEnabled
    public static Boolean isIdeaSubscribed(String ideaId) 
    {
        return (VCommunityIdeaController.getIdeaSubscription(ideaId) != null) ? true : false;
    }
    
    /*Subscribe Idea*/
    @AuraEnabled
    public static void subscribeIdea(String ideaId) 
    {
        if(VCommunityIdeaController.getIdeaSubscription(ideaId) == null)
        {
            try
            {
                Idea_Subscription__c es = new Idea_Subscription__c();
                es.Idea__c = ideaId;
                es.Subscriber__c = UserInfo.getUserId();
                es.Notify_of_Update_Type__c  = 'AllUpdates ';
                insert es;
                
                System.debug('#### es: ' +es);
            }
            catch(Exception ex)
            {
                System.debug('#### Error: '+ex);
            }
        }
    }
    
    /*Subscribe Idea*/
    @AuraEnabled
    public static void subscribeIdeaOnProductTeamUpdates(String ideaId) 
    {
        if(VCommunityIdeaController.getIdeaSubscription(ideaId) == null)
        {
            try
            {
                Idea_Subscription__c es = new Idea_Subscription__c();
                es.Idea__c = ideaId;
                es.Subscriber__c = UserInfo.getUserId();
                es.Notify_of_Update_Type__c  = 'ProductTeamUpdates ';
                insert es;
                
                System.debug('#### es: ' +es);
            }
            catch(Exception ex)
            {
                System.debug('#### Error: '+ex);
            }
        }
    }
    
    /*Subscribe Idea*/
    @AuraEnabled
    public static void unsubscribeIdea(String ideaId) 
    {
        Idea_Subscription__c es = VCommunityIdeaController.getIdeaSubscription(ideaId);
        if(es != null)
        {
            try
            {
                delete es;
            }
            catch(Exception ex)
            {
                System.debug('#### Error: '+ex);
            }
        }
    }
    
    /*Find similar ideas*/
    @AuraEnabled
    public static List<Idea> findSimilarIdeas(String title) {
        List<Idea> similarIdeas;
        try {
            String ideaTitle = '%'+title+'%';
            similarIdeas = [select Id, Title, VoteTotal from Idea where Title LIKE : ideaTitle And IsMerged = false];
            // There is an API call available for this. See https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_classes_ideas.htm
            // You call it like this:
            // ID[] results = Ideas.findSimilar(Idea idea);
        } catch (Exception e) {
            System.debug(e);
        }
        return similarIdeas;
    }
    
    /* Determines whether the surrent user is a guest or not */
    @AuraEnabled
    public static boolean isGuestUser() {
        String userType = getUserType();
        System.debug('Switching off of user type: ' + userType);
        return (userType == 'Guest' ? true : false);
    }
    
    /* Determines whether the surrent user is a admin or not */
    @AuraEnabled
    public static boolean isAdminUser() {
        List<User> userList = [Select Id From User Where Id = :UserInfo.getUserId() And Profile.Name = 'System Administrator' Limit 1]; 
        if(!userList.isEmpty()){
            return true;
        }
        return false;
    }
    
    /* Determines the current user type */
    @AuraEnabled
    public static String getUserId() {
        return UserInfo.getUserId();
    }
    
    /* Determines the current user type */
    @AuraEnabled
    public static String getUserType() {
        Id userId = UserInfo.getUserId();
        System.debug('User type for user: ' + UserInfo.getUserType());
        return UserInfo.getUserType();
    }
    
    /*Select Option class with Aura Enabled properties*/
    public class SelectOptionDetail{
        @AuraEnabled
        public String name {get;set;}
		
        @AuraEnabled
        public String value {get;set;}
		
		public selectOptionDetail(String name, String value){
			this.name = name;
			this.value = value;
		}
    }
}