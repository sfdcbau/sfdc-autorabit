/*
	Util class for user methods
*/
public with sharing class VUserUtil 
{
	
	@AuraEnabled
	public static User getCurrentUser()
	{
		return [Select Id, Name, UserType, ProfileId, Profile.Name From User Where Id = :UserInfo.getUserId() Limit 1];
	}
	
}