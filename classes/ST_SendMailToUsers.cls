/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a handler class for ST_SendMailToUsers.
*
* This class sending mail to the opportunity owner and some users when Health score is 0-3 and live date is null or less than 90 days.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Sanjeev Kaurav  <skaurav@servicetitan.com>
* @version        1.1
* @created        04-30-2018
  @Updated        16/10/2018
* @Name           ST_SendMailToUsers
* @Method         ST_SendMail()
* @TestClass      ST_SendMailToUsersTest
* @Trigger        ST_AccountTrigger

* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class ST_SendMailToUsers {
    
    
    public static boolean firstRun = true;
    public static boolean isFirstRun(){
        if(firstRun){
            firstRun = false;
            return true;
        }else{
            return firstRun;
        }
    }
    Public static void ST_SendMail(list<account> newAccountValues){
        
        Map<Id,Account> mapofIdAndAccount= new Map<Id,Account>();
        Map<String, Account__c> csAccount = Account__c.getAll();
        
        Boolean isActive = false;
        
        Set<Id> onboardingId = new Set<Id>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress[] owea = [select Id,Address from OrgWideEmailAddress where Address = 'noreply_salesforce@servicetitan.com' limit 1];
        
        for(Account acc:newAccountValues){
            if(csAccount.containskey(acc.Health_Score__c) == true && csAccount.get(acc.Health_Score__c) != null){
                isActive = true;
                mapofIdAndAccount.put(acc.id,acc);
            }      
        }
        
        if(isActive == true){
            
            List<Onboarding__c> onboardingList = [Select id,name,Live_Date__c,Account__c,Opportunity__c from Onboarding__c
                                                  WHERE  RecordType.Name='ST Onboarding' AND  Account__c in : mapofIdAndAccount.keyset() AND (Live_Date__c = null OR Live_Date__c = LAST_90_DAYS)];
        
            set<Id> actId = new set<Id>();
            if(onboardingList.size()>0 ){
                for(Onboarding__c onboard : onboardingList){
                    if(onboard.Account__c!=null){
                        actId.add(onboard.Account__c);
                    }   
                }  
            }
            list<Opportunity> opportunityList = [Select id,ownerid,stagename,RecordType.Name,CloseDate,isclosed,name, Onboarder_Name__c, Account_Current_Health_Score__c,owner.name,account.name,accountid from Opportunity
                                                 where accountid in :actId AND RecordType.Name='Sales' AND isclosed  = true AND StageName='Closed Won' 
                                                 ORDER BY CloseDate DESC];
            
            list<string> emailList = new list<string>();
            list<ST_LowHealthScoreEmail__c> cs = ST_LowHealthScoreEmail__c.getall().values();
            for(ST_LowHealthScoreEmail__c email : cs){
                emailList.add(email.Email__c);
            }
        
            String emailbody = system.label.Low_Health_Score_Email_Body; 
            
             //Id emailTemplateId = [select id from EmailTemplate where DeveloperName= :system.label.Low_Health_Score_Email_Template].id; 
            if(opportunityList.size()>0 ){
                Opportunity opptt = opportunityList[0]; 
                String subject = system.label.Current_Health_Score_Subject +' '+opptt.Name+' '+system.label.Current_Health_Score_Subject1;
        
                String oppURL = system.label.Low_Health_Score_URL+opptt.id;
                String emailbody2 = 'Hi'+' '+opptt.owner.name+','+'\n'+'\n'+opptt.account.name+' '+system.label.Low_Health_Score_Email_Body+' '+mapofIdAndAccount.get(opptt.accountid).Health_Score__c
                +'. '+system.label.Low_Health_Score_Email_Body1+' '+opptt.Onboarder_Name__c+' '+system.label.Low_Health_Score_Email_Body2
                +'\n '+'\n'+system.label.Low_Health_Score_Email_Body3+' '+opptt.Onboarder_Name__c +' '+system.label.Low_Health_Score_Email_Body4+'\n'+'\n'+'Opportunity Link'+': '+oppURL+'\n'+'Account Link'+': '+system.label.Low_Health_Score_URL+opptt.accountid;
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSubject(subject);
                mail.setTargetObjectId(opptt.ownerid); 
                mail.setSaveAsActivity(false);
                mail.setPlainTextBody(emailbody2);
                //mail.setTemplateId(emailTemplateId); 
                if ( owea.size() > 0 ) {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                mail.setToAddresses(emailList); 
                mails.add(mail);     
            }
            
            Messaging.sendEmail(mails);
        }
        
    }
}