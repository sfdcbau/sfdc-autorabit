/***********************************************
* @author    : Kamal Singh
* @class	 : ST_OBConfigurationCallDate
* @method	 : populateOBConfigurationCall
* @Created   : 03/12/2019
* *********************************************/

@isTest
private class ST_OBConfigurationCallDateTest {
    
    //test method for new event creation....
    static testmethod void TestOBConfigurationCallDateInsert(){
        
    //creating test data....
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.customer_status_picklist__c = 'Onboarding';
        insert acc;
        
        Contact con = new contact();
        con.accountid = acc.id;
        con.lastname ='Test Contact';
        insert con;
        
        Id OBrecordType = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
        Onboarding__c ob = new Onboarding__c();
        ob.RecordTypeId = OBrecordType;
        ob.Name = 'Test Onboarding';
        ob.Onboarding_Status__c = 'Pre-Implementation';
        ob.Account__c = acc.id;
        insert ob;
        
        Event event = new Event();
        event.Subject = 'Calendly SMB OB Configuration Call Calendar';
        event.ActivityDateTime = System.today() ; 
        event.ActivityDate = System.today() ; 
        event.DurationInMinutes = 30;
        event.whoId = con.id;
        insert event;
        List<Event> lstevent =new List<Event>();
        lstevent.add(event);
        
        //Calling method from class....
        Test.startTest();
        ST_OBConfigurationCallDate.populateOBConfigurationCall(lstevent);
        Test.stopTest(); 
        
        //checking onboarding's OB Configuration Call Date....
        Onboarding__c obAfterUpdate = [Select Id, Name, Account__c, OB_Configuration_Call_Date__c FROM Onboarding__c  limit 1];
        System.assertEquals(System.today(), obAfterUpdate.OB_Configuration_Call_Date__c);
    }
    
    //test method for event update....
    static testmethod void TestOBConfigurationCallDateUpdate(){
        
    //creating test data....    
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.customer_status_picklist__c = 'Onboarding';
        insert acc;
        
        Contact con = new contact();
        con.accountid = acc.id;
        con.lastname ='Test Contact';
        insert con;
        
        Id OBrecordType = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
        Onboarding__c ob = new Onboarding__c();
        ob.RecordTypeId = OBrecordType;
        ob.Name = 'Test Onboarding';
        ob.Onboarding_Status__c = 'Pre-Implementation';
        ob.Account__c = acc.id;
        insert ob;
        
        Event event = new Event();
        event.Subject = 'Calendly SMB OB Configuration Call Calendar';
        event.ActivityDateTime = System.today() ; 
        event.ActivityDate = System.today() ; 
        event.DurationInMinutes = 30;
        event.whoId = con.id;
        insert event;
        
        event.ActivityDate = System.today() + 10 ;
        update event;
        
        List<Event> lstevent =new List<Event>();
        lstevent.add(event);
        
        //Calling method from class....
        Test.startTest();
        ST_OBConfigurationCallDate.populateOBConfigurationCall(lstevent);
        Test.stopTest();
        
        //checking onboarding's OB Configuration Call Date....
        Onboarding__c obAfterUpdate = [Select Id, Name, Account__c, OB_Configuration_Call_Date__c FROM Onboarding__c  limit 1];
        System.assertEquals(System.today() +10, obAfterUpdate.OB_Configuration_Call_Date__c);
    }
}