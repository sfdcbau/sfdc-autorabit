/**
* ────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is the handler class for updating CRP count activities if request status is changed to complete 
* CRP staus is to be Active
* Triger name:ST_RequestTrigger
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @version        1.0
* @created        12-13-2018
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────┘\
* Test Class: RequestTriggerhandler_Test
*/
public class RequestTriggerhandler {
    
    //to update crp activities once request completed....
    public static void handleUpdateRequest(List<Reference_Request__c> requestList, Map<Id, Reference_Request__c> oldMap){
        Set<Reference_Request__c> setRequestId = new Set<Reference_Request__c>();
        Set<Id> setCrpId = new Set<Id>();
        list<Customer_Participation__c> customerParticapationList = new list<Customer_Participation__c>();
        list<Customer_Reference_Program__c> lstCRPtoCheck = new list<Customer_Reference_Program__c>();
        list<Customer_Reference_Program__c> updateCustmrRefProg = new list<Customer_Reference_Program__c>(); 
        
        if(requestList.size()>0){ 
            for(Reference_Request__c req : requestList) {
                if(req.reference_status__c != oldMap.get(req.Id).reference_status__c && req.reference_status__c == 'Completed' ) {
                    setRequestId.add(req);
                } 
            }
            if(setRequestId.size() > 0) {
                customerParticapationList = [SELECT
                                             Id, Customer_Reference_Id__c, Request_Id__c, Status__c
                                             FROM Customer_Participation__c
                                             WHERE Request_Id__c IN : setRequestId AND Status__c = 'Accepted' ];
            }
            if(customerParticapationList.size() > 0) {
                for(Customer_Participation__c custmrPartn : customerParticapationList) {
                    setCrpId.add(custmrPartn.Customer_Reference_Id__c); }
            }
            if(setCrpId.size() > 0) {
                lstCRPtoCheck = [SELECT
                                 Id, CRP_Status__c, Activities_Completed_This_Year__c, Activities_Completed_Total__c,
                                 Activities_Available__c, Max_Activities_Per_Year__c, Most_Recent_Activity_Completed_Date__c
                                 FROM Customer_Reference_Program__c
                                 WHERE Id IN : setCrpId];
            }
            if(lstCRPtoCheck.size() > 0) {
                for(Customer_Reference_Program__c custmrRefProg : lstCRPtoCheck) {
                    if(custmrRefProg.CRP_Status__c == 'Active') {
                        custmrRefProg.Activities_Completed_This_Year__c = custmrRefProg.Activities_Completed_This_Year__c + 1;
                        custmrRefProg.Activities_Completed_Total__c = custmrRefProg.Activities_Completed_Total__c + 1;
                        custmrRefProg.Activities_Available__c = custmrRefProg.Max_Activities_Per_Year__c - custmrRefProg.Activities_Completed_This_Year__c;
                        custmrRefProg.Most_Recent_Activity_Completed_Date__c = system.Today();
                    }   
                    updateCustmrRefProg.add(custmrRefProg);
                }
                if(updateCustmrRefProg.size() > 0) {
                    update updateCustmrRefProg;
                }
            }
        }
    }
    
    //to check if crp linked to request before completion.....
    public static void handleRequestStatusUpdate(List<Reference_Request__c> triggerNew, Map<Id, Reference_Request__c> triggerOldMap){
        Set<Id> setReqId = new Set<Id>();
        Map<Id,List<Customer_Participation__c>> mapReqIdtoParticipation = new Map<Id,List<Customer_Participation__c>>();
        
        for(Reference_Request__c eachReq : triggerNew){
            if(eachReq.reference_status__c == 'Completed' && (triggerOldMap == null || triggerOldMap.get(eachReq.Id).reference_status__c != eachReq.reference_status__c)){
                setReqId.add(eachReq.Id);
            }
            
        }
        if(setReqId != null && setReqId.size() > 0){
            for(Customer_Participation__c eachPart : [SELECT Id, Customer_Reference_Id__c, Request_Id__c, Status__c FROM Customer_Participation__c WHERE Request_Id__c IN : setReqId]){
                if(!mapReqIdtoParticipation.containsKey(eachPart.Request_Id__c))
                    mapReqIdtoParticipation.put(eachPart.Request_Id__c, new List<Customer_Participation__c>());
                mapReqIdtoParticipation.get(eachPart.Request_Id__c).add(eachPart);   
            }
        }
        if(mapReqIdtoParticipation != null && mapReqIdtoParticipation.size() > 0){
            for(Reference_Request__c eachReq : triggerNew){
                if(eachReq.reference_status__c == 'Completed' && (triggerOldMap == null || triggerOldMap.get(eachReq.Id).reference_status__c != eachReq.reference_status__c)){
                    if(mapReqIdtoParticipation.containsKey(eachReq.Id) && mapReqIdtoParticipation.get(eachReq.Id) != null){
                        // do nothing
                    }
                    else{
                        eachReq.addError(Label.RequestCompletedStatusErrorMessage);
                    }
                } 
            }
        }
        else{
            for(Reference_Request__c eachReq : triggerNew){
                if(eachReq.reference_status__c == 'Completed' && (triggerOldMap == null || triggerOldMap.get(eachReq.Id).reference_status__c != eachReq.reference_status__c)){
                    eachReq.addError(Label.RequestCompletedStatusErrorMessage);
                } 
            }
        } 
    }
}