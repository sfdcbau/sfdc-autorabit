/*
	Util class for Campaign process
*/
public with sharing class VCampaignUtil 
{
	public static boolean canTriggerVCampaignMemberAddAccountOpportunityToCampaign = true;
	public static boolean canTriggerVOpportunityAddAccountContactToCompaign = true;
	public static Set<Id> VOpportunityAddAccountContactToCompaign_OpportunityIdSet = new Set<Id>();
}