@isTest
public class AccountUpdateOnClosedLostTest {
  /*  @isTest
    public static void updateAccountRecordsForSDR(){
        //Create test records 
        Account acc=new Account(Name='Test');
        insert acc;
        Account_Setting__c accountset= new Account_Setting__c(Name='Index',BDRs_Index__c=0,SDRs_Index__c=0);
        insert accountset;
        
        //Call the Method
        AccountUpdateOnClosedLost.updateAccountRecords(new List<Id>{acc.Id});
        
        Account updatedacc=[select Id,Customer_Status_picklist__c from Account where Id=:acc.Id];
        System.assertEquals('SDR Assigned', updatedacc.Customer_Status_picklist__c);
    }
    @isTest
    public static void updateAccountRecordstoFirstUserSDR(){
        //Create test records 
        Account acc=new Account(Name='Test');
        insert acc;
        //Query public group
        List<GroupMember> BDRUSERLIST=[select Id,UserOrGroupId from GroupMember where Group.type='Regular' AND Group.DeveloperName='BDRs'];
        List<GroupMember> SDRUSERLIST=[select Id,UserOrGroupId from GroupMember where Group.type='Regular' AND Group.DeveloperName='SDRs'];
        
        Account_Setting__c accountset= new Account_Setting__c(Name='Index',BDRs_Index__c=BDRUSERLIST.size(),SDRs_Index__c=SDRUSERLIST.size());
        insert accountset;
        
        //Call the Method
        AccountUpdateOnClosedLost.updateAccountRecords(new List<Id>{acc.Id});
        
        Account updatedacc=[select Id,Customer_Status_picklist__c from Account where Id=:acc.Id];
        System.assertEquals('SDR Assigned', updatedacc.Customer_Status_picklist__c);
    }
    @isTest
    public static void updateAccountRecordstoFirstUserBDR(){
        //Create test records 
        Account acc=new Account(Name='Test',Assoc_Segment_Ranking__c=2);
        insert acc;
        //Query public group
        List<GroupMember> BDRUSERLIST=[select Id,UserOrGroupId from GroupMember where Group.type='Regular' AND Group.DeveloperName='BDRs'];
        List<GroupMember> SDRUSERLIST=[select Id,UserOrGroupId from GroupMember where Group.type='Regular' AND Group.DeveloperName='SDRs'];
        
        Account_Setting__c accountset= new Account_Setting__c(Name='Index',BDRs_Index__c=BDRUSERLIST.size(),SDRs_Index__c=SDRUSERLIST.size());
        insert accountset;
        
        //Call the Method
        AccountUpdateOnClosedLost.updateAccountRecords(new List<Id>{acc.Id});
        
        Account updatedacc=[select Id,Customer_Status_picklist__c from Account where Id=:acc.Id];
        System.assertEquals('SDR Assigned', updatedacc.Customer_Status_picklist__c);
    }
    @isTest
    public static void updateAccountRecordsForBDR(){
        //Create test records 
        Account acc=new Account(Name='Test',Assoc_Segment_Ranking__c=2);
        insert acc;
        Account_Setting__c accountset= new Account_Setting__c(Name='Index',BDRs_Index__c=0,SDRs_Index__c=0);
        insert accountset;
        
        //Call the Method
        AccountUpdateOnClosedLost.updateAccountRecords(new List<Id>{acc.Id});
        
        Account updatedacc=[select Id,Customer_Status_picklist__c from Account where Id=:acc.Id];
        System.assertEquals('SDR Assigned', updatedacc.Customer_Status_picklist__c);
    }*/
}