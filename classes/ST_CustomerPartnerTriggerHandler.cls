/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 26 Dec, 2018
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 27 Dec, 2018
 # Version .................: V 1.0
 # Description...................: This is trigger handler class for ST_CustomerPartnerTrigger trigger.
 # Test Class...................: ST_CustomerPartnerTriggerTest
 @ Updated By.....................: Sanjeev
 @ Version .......................: V 1.1
 @ Modified Date ..................: 03/30/20109
 #####################################################
*/
public class ST_CustomerPartnerTriggerHandler {
    
    private Boolean isTriggerContext = false;
    private Integer batchSize = 0;
    private static Boolean isAccRankUpdated = false;
    
    /**
     * @author : Aman Gupta
     * @date : 26 Dec, 2018
     * @description : Parameterized Constructor
    */
    public ST_CustomerPartnerTriggerHandler(Boolean isExecuting, Integer size) {
        isTriggerContext = isExecuting;
        batchSize = size;
    }
    
    //*************************** Trigger Event Handler Start ********************************//
    /**
     * @author : Aman Gupta
     * @date : 26 Dec, 2018
     * @description : This method is to handle onAfterInsert event of CustomerPartner__c trigger.
    */
    public void onAfterInsert(List<CustomerPartner__c> triggernew, Map<Id,CustomerPartner__c> triggernewMap) {
        //update Assoc_Segment_Ranking__c on Account
        updateAccountRanking(null, triggernew);
        isAccRankUpdated = true;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 26 Dec, 2018
     * @description : This method is to handle onAfterUpdate event of CustomerPartner__c trigger.
    */
    public void onAfterUpdate(List<CustomerPartner__c> triggerold, Map<Id,CustomerPartner__c> triggeroldMap, List<CustomerPartner__c> triggernew, Map<Id,CustomerPartner__c> triggernewMap) {
        //update Assoc_Segment_Ranking__c on Account
        if(Test.isRunningTest() || !isAccRankUpdated) {
            updateAccountRanking(triggerold, triggernew);
            isAccRankUpdated = true;
        }
    }
    
    /**
     * @author : Aman Gupta
     * @date : 26 Dec, 2018
     * @description : This method is to handle onAfterDelete event of CustomerPartner__c trigger.
    */
    public void onAfterDelete(List<CustomerPartner__c> triggerold, Map<Id,CustomerPartner__c> triggeroldMap) {
        //update Assoc_Segment_Ranking__c on Account
        updateAccountRanking(triggerold, null);
        isAccRankUpdated = true;
    }
    //*************************** Trigger Event Handler End ********************************//
    
    //*************************** Methods invoked by Event Handler methods Starts ********************************//
    /**
     * @author : Aman Gupta
     * @date : 27 Dec, 2018
     * @description : SE1105 --> Update Customer Account (field: Assoc_Segment_Ranking__c) with minimum value of its CustomerPartner__c records (field: Assoc_Segment_Ranking__c) whenever CustomerPartner__c record is created, updated, deleted.
    */
    public void updateAccountRanking(List<CustomerPartner__c> oldCustPartObjList, List<CustomerPartner__c> newCustPartObjList) {
        // check whether to run updateAccountRanking or not
        List<Trigger_Settings__mdt> trgSetObjList = [SELECT 
                                                     Label, isActive__c 
                                                     FROM Trigger_Settings__mdt 
                                                     WHERE Label = 'ST_UpdateAccountRanking' LIMIT 1];
        if(!trgSetObjList.isEmpty() && trgSetObjList[0].isActive__c) {
            Set<Id> cAccIdSet = new Set<Id>();
            // populating pAccIdSet set with Old Partner Account Id's.
            if(oldCustPartObjList != null && !oldCustPartObjList.isEmpty()) {
                for(CustomerPartner__c cpObj : oldCustPartObjList)
                    cAccIdSet.add(cpObj.Customer__c);
            }
            // populating pAccIdSet set with New Partner Account Id's.
            if(newCustPartObjList != null && !newCustPartObjList.isEmpty()) {
                for(CustomerPartner__c cpObj : newCustPartObjList)
                    cAccIdSet.add(cpObj.Customer__c);
            }
            
            List<Account> accObjList = new List<Account>();
            // populating accObjList list with Account records needs to be updated
            for(Account accObj : [SELECT Id, (SELECT Id, Partner__r.Assoc_Segment_Partner_Rank__c from Partners__r WHERE Partner__r.Assoc_Segment_Partner_Rank__c != null) from Account WHERE Id IN : cAccIdSet]) {
                Decimal minRank = null;
                for(CustomerPartner__c cpObj : accObj.Partners__r) {
                    if(minRank != null) {
                        if(minRank > cpObj.Partner__r.Assoc_Segment_Partner_Rank__c)
                            minRank = cpObj.Partner__r.Assoc_Segment_Partner_Rank__c;
                    }
                    else
                        minRank = cpObj.Partner__r.Assoc_Segment_Partner_Rank__c;
                }
                accObj.Assoc_Segment_Ranking__c = Integer.valueOf(minRank);
                accObjList.add(accObj);
            }
            /*for(Account accObj : [SELECT Id, (SELECT Id, Assoc_Segment_Ranking__c from Partners__r WHERE Assoc_Segment_Ranking__c != null) from Account WHERE Id IN : cAccIdSet]) {
                Integer minRank = null;
                for(CustomerPartner__c cpObj : accObj.Partners__r) {
                    if(minRank != null)
                        minRank = (minRank > (Integer)cpObj.Assoc_Segment_Ranking__c) ? (Integer)cpObj.Assoc_Segment_Ranking__c : minRank;
                    else
                        minRank = (Integer)cpObj.Assoc_Segment_Ranking__c;
                }
                accObj.Assoc_Segment_Ranking__c = minRank;
                accObjList.add(accObj);
            }
            */
            // update accObjList if not null
            if(!accObjList.isEmpty()) {
                update accObjList;    
            }
        }
    }
    //*************************** Methods invoked by Event Handler methods Ends ********************************//
}