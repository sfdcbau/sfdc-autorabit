/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This Class is used to create Payment Opportunities when all the payment opportunity in closed lost stage 
*
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Manoj Kumar
* @version        1.1
* @created        12-19-2018
  @Updated        01/14/2019
* @Name           ST_AddPaymentOpportunityController
* @TestClass      ST_AddPaymentOpportunityControllerTest
*         
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class ST_AddPaymentOpportunityController{

    //Method to check whether Payment Opportunity(Open Opportunity - When all the payment opportunity is closed lost) exist or not
    @AuraEnabled
    public static boolean isPaymentOpportunityExist(Id accountId){        
        boolean isPaymentOpportunityExist = false;
        
        try{
            if(String.isNotBlank(accountId)){
            
                Id paymentsOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();   
                                                                           
                Opportunity[] opportunity = [SELECT Id FROM Opportunity WHERE AccountId = :accountId and 
                                                    RecordTypeId =: paymentsOpportunityRecordTypeId and StageName != 'Closed Lost'];
               
                if (opportunity.size() > 0)
                   isPaymentOpportunityExist = true;
                else 
                   isPaymentOpportunityExist = false;
                   
            }
        }catch(Exception ex){
            system.debug('Error in AddPaymentOpportunityController---isPaymentOpportunityExist--' + ex.getMessage() );
        }
        
        return isPaymentOpportunityExist;
    }
}