/**
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐

* This is called from 'Request: Notification Alerts to CRM and Requester when Request status changes' process on request object
* This class creates a Customer Reference program record for request or link request to CRP when internal request is approved by CRM
* ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          1-11-2019
* @Jira User Story  SC-26
* ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘*/

public class ST_RequestCRPRecordCreation {
    
    @InvocableMethod
    // method is called from process 'Request: Notification Alerts to CRM and Requester when Request status changes'
    public static void insertNewCRP(List<Id> request){
        Customer_Reference_Program__c newCRP = new  Customer_Reference_Program__c();
        
        List<Customer_Reference_Program__c> listCRPtoInsert = new List<Customer_Reference_Program__c>(); 
        Map<Id,List<Reference_Request__c>> mapIdtoRequest = new Map<Id,List<Reference_Request__c>>();
        List<Reference_Request__c> lstRequesttoUpdate = new List<Reference_Request__c>();
        List<Reference_Request__c> lstnewrequesttoUpdate = new List<Reference_Request__c>(); 
        Set<Id> setContactId = new Set<Id>();
        List<Customer_Reference_Program__c> lstCRPtoUpdateCRDId = new List<Customer_Reference_Program__c>();
        Map<Id,Customer_Reference_Program__c> mapIdtoCRPtoUpdate = new Map<Id,Customer_Reference_Program__c>();
        
        //query the request and creating map of contacts to request....
        for(Reference_Request__c eachRequest : [SELECT Id, Contact__c, ownerId, Contact__r.AccountId,  Reference_Status__c, Customer_Reference_Detail_Id__c
                                                FROM Reference_Request__c WHERE Id = :request]){
                                                    if(!mapIdtoRequest.containsKey(eachRequest.Contact__c))
                                                        mapIdtoRequest.put(eachRequest.Contact__c, new List<Reference_Request__c>()); 
                                                    mapIdtoRequest.get(eachRequest.Contact__c).add(eachRequest);
                                                    lstnewrequesttoUpdate.add(eachRequest); 
                                                    setContactId.add(eachRequest.Contact__c);
                                                } 
        
        //query and updating CRP if a CRP exists for the contact......
        for(Customer_Reference_Program__c eachCRP : [SELECT Id, Contact__c, Request_Id__c
                                                     FROM Customer_Reference_Program__c WHERE Contact__c in :setContactId]){ 
                                                         if(mapIdtoRequest.containsKey(eachCRP.Contact__c) && mapIdtoRequest.get(eachCRP.Contact__c) != null){ 
                                                             for(Reference_Request__c eachRequest : mapIdtoRequest.get(eachCRP.Contact__c)){
                                                                 if(request.contains(eachRequest.Id)){
                                                                     eachCRP.Request_Id__c = eachRequest.Id; 
                                                                     mapIdtoCRPtoUpdate.put(eachCRP.Id,eachCRP);
                                                                 }
                                                             } 
                                                         }
                                                     }
        
        //creating new CRP if does not exist for the contact.........
        if(lstnewrequesttoUpdate != null && lstnewrequesttoUpdate.size() > 0 && mapIdtoCRPtoUpdate.size() == 0){
            for(Reference_Request__c eachNewRequest : lstnewrequesttoUpdate){ 
                newCRP.Request_Id__c = eachNewRequest.Id;
                newCRP.Account__c = eachNewRequest.Contact__r.AccountId;
                newCRP.Contact__c = eachNewRequest.Contact__c;
                newCRP.CRP_Status__c = 'Approved';
                newCRP.Approved_Date__c = System.today();
                newCRP.Customer_Reference_Detail_Id__c = eachNewRequest.Customer_Reference_Detail_Id__c;
                newCRP.Max_Activities_Per_Year__c = 12;
                newCRP.ownerId  = eachNewRequest.ownerId;
                listCRPtoInsert.add(newCRP); 
            }
        } 
        
        try{
            if(listCRPtoInsert != null && listCRPtoInsert.size() > 0){
                insert listCRPtoInsert;
            }
            if(mapIdtoCRPtoUpdate != null && mapIdtoCRPtoUpdate.size() > 0){
                update mapIdtoCRPtoUpdate.values();
            }   
            
        }
        catch(DmlException ex) {
            System.debug('An unexpected error has occurred: ' + ex.getMessage());
        }         
    }
}