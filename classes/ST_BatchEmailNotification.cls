/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class Will send Email Notifications to certain users when stick rate is below as expected for Cold Outbound,Marketing,Events & Warm
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          01-04-2019
* @Updated          03-04-2019
* @Jira User Story  SE-2869 
* @Author           Vishal Labh
* @TestClass		ST_BatchEmailNotificationTest	
* ──────────────────────────────────────────────────────────────────────	───────────────────────────┘\
* 
*/
global class ST_BatchEmailNotification implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    
    global Double countOfMeetingRanMarketing = 0;
    global Double countOfPowerofOneMarketing = 0;
    global Double countOfMeetingRanCold = 0;
    global Double countOfPowerofOneCold = 0;
    global Double countOfMeetingRanWarm = 0;
    global Double countOfPowerofOneWarm = 0;
    global Double countOfMeetingRanEvents = 0;
    global Double countOfPowerofOneEvents = 0;
    
    global Integer oppCount = 0;
    
    global Database.querylocator start(Database.BatchableContext BC){
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        String query = 'SELECT '+
            +' Id, Account.name, Account.owner.name, name, Initial_Discovery_Call_Date_Time__c, SDR_MDR_Owner__r.Name,' 
            +' Size_Segment__c, Owner.UserRole.Name, Discovery_Call_Meeting_Result_New__c, Account.New_Markets__c, Meeting_Ran__c,'
            +' Opportunity_Power_of_One__c, RecordtypeId, SumChnnl__c FROM Opportunity '+
            +' WHERE RecordTypeId = : recTypeId';
        return Database.getQueryLocator(query);
    }
    //Executing the Summerised channel of Opportunity 
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){ 
        System.debug('scope.size() = ' + scope.size());
        
        List<String> accOwnerList = Label.AccountOwner_SDRMDROwner.split(',');
        String accNameStr = Label.TestaccountName;
        List<String> roleNameList = Label.RoleNameList.split(',');
        
        Date startDate = System.today().toStartOfMonth();
        Datetime startDateTime = Datetime.newInstance(startDate.year(), startDate.month(), startDate.day(), 0, 0, 0);
        
        Date endDate = startDate.addDays(Date.daysInMonth(startDate.year(), startDate.month()) - 1);
        Datetime endDateTime = Datetime.newInstance(endDate.year(), endDate.month(), endDate.day(), 0, 0, 0);
        
        for(Opportunity oppObj : scope) { 
            if(oppObj.Initial_Discovery_Call_Date_Time__c < System.today() 
               && oppObj.Initial_Discovery_Call_Date_Time__c >= startDateTime
               && oppObj.Initial_Discovery_Call_Date_Time__c <= endDateTime
               && !(oppObj.Account.name != null && oppObj.Account.name.contains(accNameStr))
               && !accOwnerList.contains(oppObj.Account.Owner.Name) 
               && !accOwnerList.contains(oppObj.SDR_MDR_Owner__r.Name) 
               && oppObj.Size_Segment__c == 'Corporate'
               && oppObj.Discovery_Call_Meeting_Result_New__c != 'Non-Compliant' 
               && oppObj.Account.New_Markets__c == false) {
                   Boolean containRole = true;
                   if(oppObj.Owner.UserRole.Name != null) {
                       for(String roleName : roleNameList) {
                           if(!oppObj.Owner.UserRole.Name.contains(roleName)) {
                               containRole = false;
                               break;
                           }
                       }
                   }
                   
                   if(!containRole) {
                       if(oppObj.SumChnnl__c == Label.SumChannelMarketing) {
                           countOfMeetingRanMarketing = countOfMeetingRanMarketing + oppObj.Meeting_Ran__c;
                           countOfPowerofOneMarketing = countOfPowerofOneMarketing + oppObj.Opportunity_Power_of_One__c;
                       } else if(oppObj.SumChnnl__c == Label.SumChannelWarmOutbound) {
                           countOfMeetingRanWarm = countOfMeetingRanWarm + oppObj.Meeting_Ran__c;
                           countOfPowerofOneWarm = countOfPowerofOneWarm + oppObj.Opportunity_Power_of_One__c;                       
                       } else if(oppObj.SumChnnl__c == Label.SumChannelColdOutbound) {
                           countOfMeetingRanCold = countOfMeetingRanCold + oppObj.Meeting_Ran__c;
                           countOfPowerofOneCold = countOfPowerofOneCold + oppObj.Opportunity_Power_of_One__c;
                       } else if(oppObj.SumChnnl__c == Label.SumChannelEvents) {
                           countOfMeetingRanEvents = countOfMeetingRanEvents + oppObj.Meeting_Ran__c;
                           countOfPowerofOneEvents = countOfPowerofOneEvents + oppObj.Opportunity_Power_of_One__c;
                       }
                       oppCount = oppCount + 1;    
                       
                   }
               }
        }       
    }
    global void finish(Database.BatchableContext BC){
        Double stickRateMarketing = 0;
        Double stickRateWarm = 0;
        Double stickRateCold = 0;
        Double stickRateEvents = 0;
        
        //Calculating the % for cold outbound, Marketing, Events & Warm  
        if(countOfPowerofOneMarketing != 0)
            stickRateMarketing = ( countOfMeetingRanMarketing / countOfPowerofOneMarketing)* 100 ;
        if(countOfPowerofOneWarm != 0)
            stickRateWarm = ( countOfMeetingRanWarm / countOfPowerofOneWarm)* 100 ;
        if(countOfPowerofOneCold != 0)
            stickRateCold = ( countOfMeetingRanCold / countOfPowerofOneCold)* 100 ;
        if(countOfPowerofOneEvents != 0)
            stickRateEvents = ( countOfMeetingRanEvents / countOfPowerofOneEvents)* 100 ;
        
        List<Messaging.SingleEmailMessage> emailMsgList = new List <Messaging.SingleEmailMessage>();
        
        if(stickRateMarketing < Double.valueOf(Label.StickRateMKTGChange)){
            // Sending Email Notifications to Marketing Recipients
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            List<String> sendingTo = Label.StickRateMarketingEmailList.split(',');
            semail.setToAddresses(sendingTo);
            semail.setSubject('Stick Rate Marketing is Low');
            semail.setPlainTextBody('Stick rate of Marketing has dropped. Please look into the no shows and address with your team :' + Label.CA_Dash_Marketing);
            emailMsgList.add(semail);
            
        }
        
        if (stickRateWarm < Double.valueOf(Label.StickWarmRateChange)) {
            // Sending Email Notifications to Warm Outbound on Recipients
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            List<String> sendingTo = Label.StickRateWarmEmailList.split(',');
            semail.setToAddresses(sendingTo);
            semail.setSubject('Stick Rate of Warm Outbound is Low');
            semail.setPlainTextBody('Stick rate of Warm Outbound has dropped. Please look into the no shows and address with your team :' + Label.CA_Dash_Warm_Outbound);
            emailMsgList.add(semail);
            
        }
        
        if (stickRateCold < Double.valueOf(Label.StickRateColdChange)) {
            // Sending Email Notifications to Cold Outbound Recipients
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            List<String> sendingTo = Label.StickRateColdOutboundEmailList.split(',');
            semail.setToAddresses(sendingTo);
            semail.setSubject('Stick Rate of Cold Outbound is Low');
            semail.setPlainTextBody('Stick rate of Cold Outbound has dropped. Please look into the no shows and address with your team :' + Label.CA_Dash_Cold_Outbound);
            emailMsgList.add(semail);
            
        }
        
        if (stickRateEvents < Double.valueOf(Label.StickRateEventsChange)) {
            // Sending Email Notifications to Events Channels Recipients 
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            List<String> sendingTo = Label.StickRateEventsEmailList.split(',');
            semail.setToAddresses(sendingTo);
            semail.setSubject('Stick Rate of Events is Low');
            semail.setPlainTextBody('Stick rate of Events has dropped. Please look into the no shows and address with your team :' + Label.CA_Dash_Events);
            emailMsgList.add(semail);
            
        }
        if(!Test.isRunningTest() && !emailMsgList.isEmpty())
            Messaging.sendEmail(emailMsgList);
    }
    
    global void execute(SchedulableContext SC) {
        ST_BatchEmailNotification batch = new ST_BatchEmailNotification();
        Database.executeBatch(batch);
    }  
}