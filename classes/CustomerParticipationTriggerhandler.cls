/**
* ----------------------------------------------------------------------------------------------------+
* This is the handler class for checking if an CRP is already mapped to the particular Request 
*
* Triger name:ST_CustomerParticipationTrigger
* -----------------------------------------------------------------------------------------------------
* @version        1.0
* @created        12-12-2018
* -----------------------------------------------------------------------------------------------------+\
* Test Class: CustomerParticipationTriggerhandler_Test
*/
public class CustomerParticipationTriggerhandler {
    
    //method to check if CRP is already mapped to request....
    public static void handleInsertCustomerParticipation(List<Customer_Participation__c> lstCustomerParticipation){
        
        Map<Id,List<Customer_Participation__c>> mapRefIdtoParticipation = new Map<Id,List<Customer_Participation__c>>();
        Map<Id,Decimal> mapIdtoDesiredRef = new Map<Id,Decimal>();
        List<Customer_Participation__c> lstrefRequest = new List<Customer_Participation__c>();
        Map<Id,Id> mapCRPIdtoReq = new Map<Id,Id>();
        Set<Id> setReqId = new Set<Id>();
        
        if(lstCustomerParticipation != null && lstCustomerParticipation.size() > 0){
            for(Customer_Participation__c eachPart : lstCustomerParticipation){
                setReqId.add(eachPart.Request_Id__c);
            }
        }
        
        if(setReqId != null && setReqId.size() > 0){
            for(Customer_Participation__c eachReq : [Select Id,Request_Id__c,Customer_Reference_Id__c,Request_Id__r.References_Desired__c,Status__c from Customer_Participation__c where Request_Id__c IN :setReqId] ){
                if(!mapRefIdtoParticipation.containsKey(eachReq.Request_Id__c))
                    mapRefIdtoParticipation.put(eachReq.Request_Id__c, new List<Customer_Participation__c>());
                mapRefIdtoParticipation.get(eachReq.Request_Id__c).add(eachReq);
                
                mapIdtoDesiredRef.put(eachReq.Request_Id__c, eachReq.Request_Id__r.References_Desired__c);
                mapCRPIdtoReq.put(eachReq.Customer_Reference_Id__c,eachReq.Id); 
            } 
        }
        
        if(mapRefIdtoParticipation != null && mapRefIdtoParticipation.size() > 0){
            for(Customer_Participation__c eachPart : lstCustomerParticipation){
                if(mapRefIdtoParticipation.containsKey(eachPart.Request_Id__c) && mapRefIdtoParticipation.get(eachPart.Request_Id__c) != null){ 
                    for(Customer_Participation__c eachreq : mapRefIdtoParticipation.get(eachPart.Request_Id__c)){
                        if(String.valueOf(eachreq.Status__c) == 'Accepted' ){
                            lstrefRequest.add(eachreq);
                        }
                    }
                }
                
                if(lstrefRequest != null && lstrefRequest.size() > 0 && lstrefRequest.size() >= mapIdtoDesiredRef.get(eachPart.Request_Id__c) ){
                    eachPart.addError(Label.CustmrParticipationTriggerError);
                }
                else if(mapCRPIdtoReq.containsKey(eachPart.Customer_Reference_Id__c) && mapCRPIdtoReq.get(eachPart.Customer_Reference_Id__c) != null ){
                    eachPart.addError(Label.CustmrParticipationCRPError);
                }
            }
        }
    }
    
    //method for updating Counts on the CRP record based on Customer Participation Status......
    public static void handleUpdateCRP(List<Customer_Participation__c> cpList){
        if(cpList.size()>0){
            Set<Id> setCRPtoUpdate = new Set<Id>();
            List<Customer_Reference_Program__c> lstCRPtoUpdate = new List<Customer_Reference_Program__c>();
            List<Customer_Reference_Program__c> updateCustmrRefProg = new List<Customer_Reference_Program__c>();
            Map<Id, Customer_Participation__c> custmrPartMap = new Map<Id, Customer_Participation__c>();
            
            for(Customer_Participation__c newCustomerParticipation : cpList){
                setCRPToUpdate.add(newCustomerParticipation.Customer_Reference_Id__c);
                custmrPartMap.put(newCustomerParticipation.Customer_Reference_Id__c, newCustomerParticipation );
            }
            if(setCRPToUpdate.size() > 0){
                lstCRPtoUpdate = [SELECT Id, CRP_Status__c,
                                  Activities_Asked_This_Year__c, Activities_Asked_Total__c,
                                  Activities_Accepted_This_Year__c, Activities_Accepted_Total__c,
                                  Activities_Declined_This_Year__c, Activities_Declined_Total__c,
                                  Most_Recent_Activity_Asked_Date__c
                                  FROM Customer_Reference_Program__c
                                  WHERE Id IN : setCRPToUpdate];
            }
            if(lstCRPtoUpdate.size() > 0){
                for(Customer_Reference_Program__c custmrRefProg : lstCRPtoUpdate) {
                    if(custmrRefProg.CRP_Status__c == 'Active') {
                        if(custmrPartMap.get(custmrRefProg.Id).status__c == 'Asked'){
                            custmrRefProg.Activities_Asked_This_Year__c = custmrRefProg.Activities_Asked_This_Year__c + 1;
                            custmrRefProg.Activities_Asked_Total__c = custmrRefProg.Activities_Asked_Total__c + 1;
                            custmrRefProg.Most_Recent_Activity_Asked_Date__c = system.Today();
                        }
                        else if(custmrPartMap.get(custmrRefProg.Id).status__c == 'Accepted'){ 
                            custmrRefProg.Activities_Accepted_This_Year__c = custmrRefProg.Activities_Accepted_This_Year__c + 1;
                            custmrRefProg.Activities_Accepted_Total__c = custmrRefProg.Activities_Accepted_Total__c + 1;
                        }    
                        else if(custmrPartMap.get(custmrRefProg.Id).status__c == 'Declined'){ 
                            custmrRefProg.Activities_Declined_This_Year__c = custmrRefProg.Activities_Declined_This_Year__c + 1;
                            custmrRefProg.Activities_Declined_Total__c = custmrRefProg.Activities_Declined_Total__c + 1;
                        }
                        updateCustmrRefProg.add(custmrRefProg);
                    }
                    if(updateCustmrRefProg.size() > 0) {
                        update updateCustmrRefProg;
                    }
                }    
            }      
        }    
    }
}