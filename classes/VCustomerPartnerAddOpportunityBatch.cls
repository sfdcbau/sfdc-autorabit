global class VCustomerPartnerAddOpportunityBatch implements Database.Batchable<sObject>
{
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
        String query = 'Select Id From CustomerPartner__c';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<CustomerPartner__c> scope) 
    {
        Set<Id> customerPartnerSet = new Set<Id>();
        
        // Collect contracts to update
        for(CustomerPartner__c cp : scope)
        {
            customerPartnerSet.add(cp.Id);
        }
        
        if(!customerPartnerSet.isEmpty())
            VCustomerPartnerUtil.addCustomerPartnerOpportunities(customerPartnerSet);
    }
    
    global void finish(Database.BatchableContext BC) 
    {
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems,CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()]; 
		System.debug('##### AsyncApexJob ID: ' + a.Id); 
    }
}