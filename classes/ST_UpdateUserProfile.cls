/*
#####################################################
# Created By....................: Tharun G
# Created Date................: 04/03/2019
# Description...................:  This class is used to update the Profile to system admin for some of the user
after the refreshing of sandboxes
# TestClass......................:ST_UpdateUserProfileTest
# Version .......................: V 1.0
#####################################################
*/
public class ST_UpdateUserProfile {
    
    public static void updateToSysAdmin(){
        
        list<User> usersListToUpdate = new List<user>();
        
        // get the list of user emails from the custom label  
        List<String> userProfilesToUpdate = Label.UserEmailsToUpdate.split(',');
        // get the sysadmin profileid
        Profile userManagementProfileId = [select id from Profile where Name='System Administrator'];
        // get the userslist to update
        List<User> usersList = [select id, ProfileId from user where Email in : userProfilesToUpdate ];
        // iterate through the list to update the profile
        if(usersList.size()>0){
            
            for(User us: usersList){
                
                us.ProfileId = userManagementProfileId.id;
                usersListToUpdate.add(us);
            }
        }
        
        if(usersListToUpdate.size()>0)
        {
            update usersListToUpdate;
        }
        
        
    }
    
}