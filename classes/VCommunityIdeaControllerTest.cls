@isTest(SeeAllData=true)
private class VCommunityIdeaControllerTest {
    
    static testMethod void unitTest(){
        //CommunityIdeaController.getLoginURL('/login');
        VCommunityIdeaController.getIdeaCategorySelectOptions();
        VCommunityIdeaController.getIdeaStatusSelectOptions();
        VCommunityIdeaController.getCurrentCommunityZones();
        VCommunityIdeaController.getCurrentCommunityZoneIDs();
        VCommunityIdeaController.getMyIdeas();
        VCommunityIdeaController.getTotalIdeas(5, 'Popular', 1, VCommunityIdeaController.getCurrentCommunityZoneIDs()[0],'','','');
        VCommunityIdeaController.getTotalIdeas(5, 'my', 1, VCommunityIdeaController.getCurrentCommunityZoneIDs()[0],'','','');
        VCommunityIdeaController.getIdeasList(5, 'Popular', 1, VCommunityIdeaController.getCurrentCommunityZoneIDs()[0],'','','');
        VCommunityIdeaController.getIdeasList(5, 'Recent', 1, VCommunityIdeaController.getCurrentCommunityZoneIDs()[0],'','','');
        VCommunityIdeaController.getIdeasList(5, 'my', 1, VCommunityIdeaController.getCurrentCommunityZoneIDs()[0],'','','');
        Community com = [select Id from Community Where IsActive = true limit 1];
        Idea newIdea = new Idea(Body='My Idea');
        newIdea.Title = 'Test Idea';
        newIdea.CommunityId = com.Id;
        insert newIdea;
        VCommunityIdeaController.getUserIdeas(newIdea.Id);
        VCommunityIdeaController.promoteIdea(newIdea.Id);
        VCommunityIdeaController.promoteFromIdeaDetails(newIdea.Id);
        VCommunityIdeaController.demoteIdea(newIdea.Id);
        VCommunityIdeaController.demoteFromIdeaDetails(newIdea.Id);
        VCommunityIdeaController.getIdea(newIdea.Id);
        
        IdeaComment newComment = new IdeaComment(IdeaId = newIdea.Id, CommentBody = 'Test Comment');
        insert newComment;
        
        VCommunityIdeaController.getComment(newIdea.Id);
        
        Vote newVote = new Vote(Type = 'Up', ParentId = newComment.Id);
        insert newVote;
        
        VCommunityIdeaController.subscribeIdea(newIdea.Id);
        VCommunityIdeaController.unlikeThisComment(newIdea.Id, newVote.Id);
        VCommunityIdeaController.likeThisComment(newIdea.Id, newComment.Id);
        VCommunityIdeaController.postComment(newIdea.Id, 'Test Comment');
        VCommunityIdeaController.deleteThisComment(newIdea.Id, newComment.Id);
        VCommunityIdeaController.setResponseComment(newIdea.Id, newComment.Id);
        VCommunityIdeaController.removeResponseComment(newIdea.Id);
        VCommunityIdeaController.isIdeaSubscribed(newIdea.Id);
        VCommunityIdeaController.postIdea('Idea Title', 'Idea body', VCommunityIdeaController.getCurrentCommunityZoneIDs()[0],'','');
        VCommunityIdeaController.findSimilarIdeas('Idea Title');
        VCommunityIdeaController.isGuestUser();
        VCommunityIdeaController.isAdminUser();
        VCommunityIdeaController.getUserType();
        VCommunityIdeaController.getUserId();
        VCommunityIdeaController.unsubscribeIdea(newIdea.Id);
    }
    
    static testMethod void testTopicIdeas(){
        List<Topic> topicList = [Select Id From Topic Limit 1];
        if(!topicList.isEmpty())
        {
	        VCommunityIdeaController.getTotalTopicIdeas(5, 1, topicList[0].Id);
	        VCommunityIdeaController.getTopicIdeasList(5, 1,topicList[0].Id);
        }
    }
}