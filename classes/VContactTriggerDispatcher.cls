public without sharing class VContactTriggerDispatcher 
{
    public void dispatch() 
    {
        if(Trigger.isAfter && Trigger.isInsert)
            setupCustomerPartnerContacts();
        
        if(Trigger.isAfter && Trigger.isUpdate)
            setupCustomerPartnerContacts();
    }
    
    private void setupCustomerPartnerContacts()
    {
        List<Customer_Partner_Contact__c> insertCustomerPartnerContactList = new List<Customer_Partner_Contact__c>();
        List<Customer_Partner_Contact__c> deleteCustomerPartnerContactList = new List<Customer_Partner_Contact__c>();
        Set<Id> accountSet = new Set<Id>();
        Map<Id, Set<Id>> insertAccountContactSetMap = new Map<Id, Set<Id>>(); // Map<AccountId, Set<ContactId>>
        Map<Id, Set<Id>> deleteAccountContactSetMap = new Map<Id, Set<Id>>(); // Map<AccountId, Set<ContactId>>
        
        if(Trigger.isInsert)
        {
            for(Contact con : (List<Contact>)Trigger.new)
                //&& con.Primary_Contact__c == true
            {
                if(con.AccountId != null && con.Primary_Contact__c == true)
                {
                    if(!accountSet.contains(con.AccountId))
                        accountSet.add(con.AccountId);
                    
                    if(!insertAccountContactSetMap.containsKey(con.AccountId))
                        insertAccountContactSetMap.put(con.AccountId, new Set<Id>());
                    
                    if(!insertAccountContactSetMap.get(con.AccountId).contains(con.Id))
                        insertAccountContactSetMap.get(con.AccountId).add(con.Id);
                }
            }
        }
        else if(Trigger.isUpdate)
        {
            for(Contact con : (List<Contact>)Trigger.new)
            {
                Contact old = (Contact)Trigger.oldMap.get(con.Id);
                
                if(con.AccountId != old.AccountId )
                {
                    if(con.AccountId != null && con.Primary_Contact__c == true)
                    {
                        if(!accountSet.contains(con.AccountId))
                            accountSet.add(con.AccountId);
                        
                        if(!insertAccountContactSetMap.containsKey(con.AccountId))
                            insertAccountContactSetMap.put(con.AccountId, new Set<Id>());
                        
                        if(!insertAccountContactSetMap.get(con.AccountId).contains(con.Id))
                            insertAccountContactSetMap.get(con.AccountId).add(con.Id);
                    }
                    
                    if(old.AccountId != null)
                    {
                        if(!accountSet.contains(old.AccountId))
                            accountSet.add(old.AccountId);
                        
                        if(!deleteAccountContactSetMap.containsKey(old.AccountId))
                            deleteAccountContactSetMap.put(old.AccountId, new Set<Id>());
                        
                        if(!deleteAccountContactSetMap.get(old.AccountId).contains(old.Id))
                            deleteAccountContactSetMap.get(old.AccountId).add(old.Id);
                    }
                }
            }
        }
        
        if(!insertAccountContactSetMap.isEmpty() || !deleteAccountContactSetMap.isEmpty())
        {
            // Collect partner contacts to delete
            // AND Contact__r.Primary_Contact__c =True 
            for(Customer_Partner_Contact__c cpc : [Select Id, Customer_Partner__c, Customer_Partner__r.Customer__c, Customer_Partner__r.Partner__c, Contact__c 
                                                   From Customer_Partner_Contact__c Where (Customer_Partner__r.Customer__c In :accountSet Or Customer_Partner__r.Partner__c In :accountSet) AND Contact__r.Primary_Contact__c =True ])
            {
                if(deleteAccountContactSetMap.containsKey(cpc.Customer_Partner__r.Customer__c))
                {
                    if(deleteAccountContactSetMap.get(cpc.Customer_Partner__r.Customer__c).contains(cpc.Contact__c))
                        deleteCustomerPartnerContactList.add(cpc);
                }
                
                if(deleteAccountContactSetMap.containsKey(cpc.Customer_Partner__r.Partner__c))
                {
                    if(deleteAccountContactSetMap.get(cpc.Customer_Partner__r.Partner__c).contains(cpc.Contact__c))
                        deleteCustomerPartnerContactList.add(cpc);
                }
            }
            
            // Collect new partner contacts
            for(CustomerPartner__c cp : [Select Id, Customer__c, Partner__c From CustomerPartner__c Where (Customer__c In :accountSet Or Partner__c In :accountSet)])
            {
                
                if(insertAccountContactSetMap.containsKey(cp.Customer__c))
                {
                    for(Id contactId : insertAccountContactSetMap.get(cp.Customer__c))
                    {
                        Customer_Partner_Contact__c newcpc = new Customer_Partner_Contact__c();
                        newcpc.Customer_Partner__c = cp.Id;
                        newcpc.Contact__c = contactId;
                        newcpc.Is_Partner_Contact__c = false;
                        insertCustomerPartnerContactList.add(newcpc);
                    }
                }
                
                if(insertAccountContactSetMap.containsKey(cp.Partner__c))
                {
                    for(Id contactId : insertAccountContactSetMap.get(cp.Partner__c))
                    {
                        Customer_Partner_Contact__c newcpc = new Customer_Partner_Contact__c();
                        newcpc.Customer_Partner__c = cp.Id;
                        newcpc.Contact__c = contactId;
                        newcpc.Is_Partner_Contact__c = true;
                        insertCustomerPartnerContactList.add(newcpc);
                    }
                }
            }
        }
        
        if(!deleteCustomerPartnerContactList.isEmpty())
            delete deleteCustomerPartnerContactList;
        
        if(!insertCustomerPartnerContactList.isEmpty())
            insert insertCustomerPartnerContactList;
    }
}