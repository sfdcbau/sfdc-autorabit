/***********************************************
 * @author    : Sanjeev Kaurav
 * @testclass : ST_UpdatePhoneDiscoveryCallTest
 * @PB Name   : Calendly_Event_Updates_Onboarder_Field
 * @Jira UserStroy   SE-1053
   @Description : This class is updating Phone discovery call field on account from ActivityDateTime on event whenever event subject is "Telecom Calendly".
 * *********************************************/
public class ST_UpdatePhoneDiscoveryCall {
    @InvocableMethod
    public static void populatePhoneDiscoveryCall(list<Event> eventlist){
        
        set<Id> whoIdlist = new set<Id>();
        set<Id> accId = new set<Id>();
        Set<Id> contactId = new Set<Id>();
        String EventSubject = label.ST_Phone_Discovery_Call;
        
        list<Account> updateAccount = new list<Account>();
       
        for(Event evt : eventlist) {
            whoIdlist.add(evt.WhoId);
        }
        //Get all contact which is related to event.
        
        for(Contact con : [Select Id, AccountId, Account.Phone_Discovery_Call__c from contact where Id IN : whoIdlist]) {
            if(con.Accountid!=null){
                accId.add(con.Accountid);
            }   
        }
       
        for(Contact con : [Select id,lastname,accountid from contact where accountid IN : accId]){
            contactId.add(con.id);
        }
        
        Date activityDate;
        Datetime dT;
        if(accId.size() > 0) {
            //Get latest ActivityDateTime from event. 
            for(Event eve :[Select id, ActivityDateTime, whoid, subject  from Event where whoid = :contactId AND Subject like :('%'+EventSubject+'%') ORDER BY ActivityDateTime desc limit 1 ]){
                
                if(eve.subject.contains(label.ST_Phone_Discovery_Call)){
                    dT= eve.ActivityDateTime;  // Store Activitydate into variable.
                    activityDate = date.newinstance(dT.year(), dT.month(), dT.day());
                    
                }
            }  
        }
        
        for(Account acc : [Select id,Phone_Discovery_Call__c from Account where id = : accId]){
            
            acc.Phone_Discovery_Call__c = activityDate;
            updateAccount.add(acc);
        }
        if(updateAccount.size()>0){
            update updateAccount;  //Updating Account's Phone discovery date fields.
        }
    }
}