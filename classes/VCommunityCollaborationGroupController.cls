public with sharing class VCommunityCollaborationGroupController {
    
    /*Get total Ideas*/
    @AuraEnabled
    public static Integer getTotalGroups(Integer recordsPerPage, Integer selPageNo, Boolean includePublic, Boolean includePrivate, Boolean includeUnlisted) 
    {
        Integer recPerPage = integer.valueof(recordsPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recPerPage;
        
        Integer totalRecords = 0;
        try 
        {
            String additionalFilter = '';
            String networkId = Network.getNetworkId();
            List<String> ctList = new List<String>();
            
            if(includePublic)
            {
                ctList.add('Public');
            }
            
            if(includePrivate)
            {
                ctList.add('Private');
            }
            
            if(includeUnlisted)
            {
                ctList.add('Unlisted');
            }
            
            if(ctList.isEmpty())
            {
                ctList.add('');
            }
            
            additionalFilter = additionalFilter + ' CollaborationType In :ctList And NetworkId = :networkId ';
            
            totalRecords = (Integer)Database.Query('SELECT count(Id) TotalCount FROM CollaborationGroup WHERE ' + additionalFilter)[0].get('TotalCount');
        } 
        catch (Exception e) 
        {
            System.debug(e);
        }
        return totalRecords;
    }
    
    /*Get Collaboration Group*/
    @AuraEnabled
    public static List<CollaborationGroup> getGroupList(Integer recordsPerPage, Integer selPageNo, Boolean includePublic, Boolean includePrivate, Boolean includeUnlisted) 
    {
        Integer recPerPage = integer.valueof(recordsPerPage);
        Integer pageNo = integer.valueof(selPageNo);
        Integer pageoffset = pageNo*recPerPage;
        List<CollaborationGroup> groupList = new List<CollaborationGroup>();
        
        try 
        {
            String additionalFilter = '';
            String networkId = Network.getNetworkId();
            List<String> ctList = new List<String>();
            
            if(includePublic)
            {
                ctList.add('Public');
            }
            
            if(includePrivate)
            {
                ctList.add('Private');
            }
            
            if(includeUnlisted)
            {
                ctList.add('Unlisted');
            }
            
            if(ctList.isEmpty())
            {
                ctList.add('');
            }
            
            additionalFilter = additionalFilter + ' CollaborationType In :ctList And NetworkId = :networkId ';
            
            groupList = Database.Query('Select Id, Name, CollaborationType, Description, MediumPhotoUrl, CreatedDate, LastFeedModifiedDate, ' +
                        ' MemberCount From CollaborationGroup WHERE ' + additionalFilter + 
                        ' Order By Name NULLS LAST LIMIT :recPerPage offset :pageoffset');
        } 
        catch (Exception e) 
        {
            System.debug(e);
        }
        return groupList;
    }
    
    /* Determines whether the current user has create group permission */
    @AuraEnabled
    public static boolean hasCreateGroupPermission() 
    {
        Boolean hasCreatePermission = false;
        
        List<User> userList = [Select Id, Profile.PermissionsChatterOwnGroups From User Where Id = :UserInfo.getUserId() Limit 1];
        if(!userList.isEmpty())
        {
            hasCreatePermission = userList[0].Profile.PermissionsChatterOwnGroups;
		
            // Check permission set if profile does not have that permission
            if(!hasCreatePermission)
            {
                List<PermissionSet> psList = [Select id From PermissionSet Where PermissionsChatterOwnGroups = true];
                if(!psList.isEmpty())
                {
                    List<PermissionSetAssignment> psaList = [Select id From PermissionSetAssignment Where assigneeId = :UserInfo.getUserId() And PermissionSetId In :psList];
                    if(!psaList.isEmpty())
                    {
                        hasCreatePermission = true;
                    }
                }
            }
        }
        
        return hasCreatePermission;
    }
    
    /*New Collaboration Group*/
    @AuraEnabled
    public static String saveNewCollaborationGroup(String newgroupName, String newgroupDescription, String newgroupCollaborationType) 
    {   
        String newId = '';
        
        Savepoint sp = Database.setSavepoint();
        
        try 
        {
            System.debug('Netword ID: '+Network.getNetworkId());
            System.debug('newgroupName: '+newgroupName);
            System.debug('newgroupDescription: '+newgroupDescription);
            System.debug('newgroupCollaborationType: '+newgroupCollaborationType);
            
            CollaborationGroup newcg = new CollaborationGroup();
            newcg.NetworkId = Network.getNetworkId();
            newcg.Name = newgroupName;
            newcg.Description = newgroupDescription;
            newcg.CollaborationType = newgroupCollaborationType;
            insert newcg;
            
            newId = newcg.Id;
        } 
        catch (Exception e) 
        {
            Database.rollback(sp);
            System.debug(e);
        }
        
        return newId;
    }
}