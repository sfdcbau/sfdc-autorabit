/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 18 Feb, 2019
 # Last Modified By..........: vishal Labh
 # Last Modified Date......: 16 APR, 2019
 # Description...................: This is test class for ST_MRRTrackingTrigger trigger, ST_MRRTrackingEventHandler and ST_MRRTrackingUtilityHandler class.
 #####################################################
*/
@isTest
private class ST_MRRTrackingTriggerTest {
    /**
     * @author : Aman Gupta
     * @date : 18 Feb, 2019
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        // insert Product2
        Product2 prodObj1 = utilityHelperTest.createProduct();
        prodObj1.name = 'Platform Payments-Check';
        prodObj1.Core_Product__c = false;
        
        Product2 prodObj2 = utilityHelperTest.createProduct();
        prodObj2.name = 'Platform Payments-Credit Card';
        prodObj2.Core_Product__c = false;
        
        insert new List<Product2> {prodObj1, prodObj2};
            
        // insert Pricebook2
        Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
        insert new List<Pricebook2> {custPBObj};
            
        // insert PricebookEntry
        PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
        insert new List<PricebookEntry> {pbeObj1};
        
        // insert Account
        Account accObj = utilityHelperTest.createAccount();
        insert new List<Account> {accObj};
            
        // insert Contact
        Contact conObj = utilityHelperTest.createContact(accObj.Id);
        conObj.Role__c = 'Co-Owner';
        insert new List<Contact> {conObj};
            
        // insert Opportunity
        Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj1.Name = accObj.Name + '- Payments - ' + System.today();
        oppObj1.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        oppObj1.StageName='Prospect';
        oppObj1.Pending_Boarding_Date__c= System.today().addDays(5);
        oppObj1.Qualified_Rate__c = 10;
        oppObj1.Mid_Qualified_Rate__c = 10;
        oppObj1.Non_Qualified_Rate__c  = 10;
        oppObj1.Per_Item__c = 10;
        oppObj1.Monthly_Service_Fee__c = 10;
        oppObj1.Pricing_Type__c = 'Interchange';
        oppObj1.Passthrough_Dues_and_Assessments__c = 'Yes';
        oppObj1.STP_CC_Monthly_Volume__c = 10;
        oppObj1.STP_CC_MRR__c = 10;
        oppObj1.Basis_Points_BPS__c= 10;
        oppObj1.Negotiation_Stage_Date__c  = System.today().addDays(2);
        oppObj1.Previous_Processor_BPS__c  = 10;
        oppObj1.Delayed_Implementation__c = true;
        oppObj1.Onboarding_POC__c=conObj.Id;
        
        Opportunity oppObj2 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj2.Name = accObj.Name + '- Payments - ' + System.today();
        oppObj2.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        oppObj2.StageName='Negotiation/Pending';
        oppObj2.Pending_Boarding_Date__c= System.today().addDays(10);
        oppObj2.Qualified_Rate__c = 20;
        oppObj2.Mid_Qualified_Rate__c = 20;
        oppObj2.Non_Qualified_Rate__c  = 20;
        oppObj2.Per_Item__c = 20;
        oppObj2.Monthly_Service_Fee__c = 20;
        oppObj2.Pricing_Type__c = 'Interchange';
        oppObj2.Passthrough_Dues_and_Assessments__c = 'Yes';
        oppObj2.STP_CC_Monthly_Volume__c = 20;
        oppObj2.STP_CC_MRR__c = 20;
        oppObj2.Basis_Points_BPS__c= 20;
        oppObj2.Negotiation_Stage_Date__c  = System.today().addDays(2);
        oppObj2.Previous_Processor_BPS__c  = 20;
        oppObj2.Delayed_Implementation__c = true;
        oppObj2.Onboarding_POC__c=conObj.Id;
        insert new List<Opportunity> {oppObj1, oppObj2};
        
        OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
        OpportunityLineItem oppLIObj2 = utilityHelperTest.createOppLineItem(oppObj2.Id, prodObj1.Id, 45.00, 26);
        insert oppLIObj1;
        insert oppLIObj2;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 18 Feb, 2019
     * @Updated by : Vishal
     * @Last updated : 16/04/2019
     * @description : This method is to test ST_MRRTrackingUtilityHandler countSumofMRR function.
    */
    @isTest static void testCountSumofMRRPositive() {
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1, userObj2};
        
        System.runAs (userObj1) {
            Account accObj = [Select Id, Name from Account LIMIT 1];
            
            List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY Name];
            List<Product2> prodObjList = [Select Id, Name from Product2 ORDER BY Name];
            
            // inserting Payment Assets
            Asset assObj1 = utilityHelperTest.createAsset(oppObjList[0].Id, accObj.Id);
            assObj1.Name = 'PayAsset1' + Label.ST_Payment_Credit_Card;
            assObj1.Status = 'Live';
            assObj1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj1.Product2Id = prodObjList[1].Id;
            
            Asset assObj2 = utilityHelperTest.createAsset(oppObjList[0].Id, accObj.Id);
            assObj2.Name = 'PayAsset2' + Label.ST_Payment_Check;
            assObj2.Status = 'Live';
            assObj2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj2.Product2Id = prodObjList[0].Id;
            
            List<Asset> assObjList = new List<Asset> {assObj1, assObj2};
            insert assObjList;
            
            // inserting Payment MRR_Tracking__c
            Id recTypeId = Schema.SObjectType.MRR_Tracking__c.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            MRR_Tracking__c mrrObj1 = utilityHelperTest.createMRRTracking(assObj1.Id, 2540,recTypeId);
            mrrObj1.Year__c = String.valueOf(System.today().year()-1);
            mrrObj1.Month__c = 'JAN';
            
            MRR_Tracking__c mrrObj2 = utilityHelperTest.createMRRTracking(assObj1.Id, 5650,recTypeId);
            mrrObj2.Year__c = String.valueOf(System.today().year()-1);
            mrrObj2.Month__c = 'FEB';
            
            MRR_Tracking__c mrrObj3 = utilityHelperTest.createMRRTracking(assObj1.Id, 5650,recTypeId);
            mrrObj3.Year__c = String.valueOf(System.today().year()-1);
            mrrObj3.Month__c = 'FEB';
            
            MRR_Tracking__c mrrObj4 = utilityHelperTest.createMRRTracking(assObj1.Id, 677,recTypeId);
            mrrObj4.Year__c = String.valueOf(System.today().year());
            mrrObj4.Month__c = 'MAR';
            
            MRR_Tracking__c mrrObj5 = utilityHelperTest.createMRRTracking(assObj1.Id, 677,recTypeId);
            mrrObj5.Year__c = String.valueOf(System.today().year());
            mrrObj5.Month__c = 'AUG';
            
            MRR_Tracking__c mrrObj6 = utilityHelperTest.createMRRTracking(assObj1.Id, 677,recTypeId);
            mrrObj6.Year__c = String.valueOf(System.today().year());
            mrrObj6.Month__c = 'AUG';
            
            MRR_Tracking__c mrrObj7 = utilityHelperTest.createMRRTracking(assObj2.Id, 2540,recTypeId);
            mrrObj7.Year__c = String.valueOf(System.today().year()-1);
            mrrObj7.Month__c = 'DEC';
            
            MRR_Tracking__c mrrObj8 = utilityHelperTest.createMRRTracking(assObj2.Id, 5650,recTypeId);
            mrrObj8.Year__c = String.valueOf(System.today().year()-1);
            mrrObj8.Month__c = 'NOV';
            
            MRR_Tracking__c mrrObj9 = utilityHelperTest.createMRRTracking(assObj2.Id, 5650,recTypeId);
            mrrObj9.Year__c = String.valueOf(System.today().year()-1);
            mrrObj9.Month__c = 'NOV';
            
            MRR_Tracking__c mrrObj10 = utilityHelperTest.createMRRTracking(assObj2.Id, 677,recTypeId);
            mrrObj10.Year__c = String.valueOf(System.today().year());
            mrrObj10.Month__c = 'JAN';
            
            MRR_Tracking__c mrrObj11 = utilityHelperTest.createMRRTracking(assObj2.Id, 677,recTypeId);
            mrrObj11.Year__c = String.valueOf(System.today().year());
            mrrObj11.Month__c = 'FEB';
            
            MRR_Tracking__c mrrObj12 = utilityHelperTest.createMRRTracking(assObj2.Id, 677,recTypeId);
            mrrObj12.Year__c = String.valueOf(System.today().year());
            mrrObj12.Month__c = 'FEB';
            
            List<MRR_Tracking__c> mrrObjList = new List<MRR_Tracking__c> {mrrObj1, mrrObj2, mrrObj3, mrrObj4, mrrObj5, mrrObj6, mrrObj7, mrrObj8, mrrObj9, mrrObj10, mrrObj11, mrrObj12};
            insert mrrObjList;
            
            // fetch Account records
            List<Account> accObjList = [Select Id, Name, CC_Payment_MRR__c, Check_Payment_MRR__c from Account Where Id= :accObj.Id];
            System.assertEquals(mrrObj5.MRR__c + mrrObj6.MRR__c, accObjList[0].CC_Payment_MRR__c);
            System.assertEquals(mrrObj11.MRR__c + mrrObj12.MRR__c, accObjList[0].Check_Payment_MRR__c);
            
            // again updating MRR_Tracking__c records with same value
            ST_MRRTrackingEventHandler.isAccMRRUpdated = false;
            mrrObjList[0].MRR__c = 2540;
            mrrObjList[1].MRR__c = 5650;
            mrrObjList[2].MRR__c = 6670;
            mrrObjList[3].MRR__c = 677;
            mrrObjList[4].MRR__c = 20;
            mrrObjList[5].MRR__c = 3454;
            mrrObjList[6].MRR__c = 2540;
            mrrObjList[7].MRR__c = 5650;
            mrrObjList[8].MRR__c = 6670;
            mrrObjList[9].MRR__c = 677;
            mrrObjList[10].MRR__c = 20;
            mrrObjList[11].MRR__c = 3454;
            update mrrObjList;
            
            // fetch Account records
            accObjList = [Select Id, Name, CC_Payment_MRR__c, Check_Payment_MRR__c from Account Where Id= :accObj.Id];
            // negative scenario: Account record is not updated as MRR__c old value is same as new value.
            System.assertEquals(mrrObjList[4].MRR__c + mrrObjList[5].MRR__c , accObjList  [0].CC_Payment_MRR__c);
            System.assertEquals(mrrObjList[10].MRR__c + mrrObjList[11].MRR__c, accObjList[0].Check_Payment_MRR__c);
            
            //deleting MRR_Tracking__c Records
            ST_MRRTrackingEventHandler.isAccMRRUpdated = false;
            delete mrrObjList;
        }
    }
}