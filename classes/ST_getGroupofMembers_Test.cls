/*--------------------------------------------------------------------------------------------------------------------------------
Test class for ST_getGroupofMembers class. 
@Method :-  createMapsOfGroups
@Jira User Story  SC-1 
----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class ST_getGroupofMembers_Test {
 
    @istest
    static void testcreateMapsOfGroups(){
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
          System.runAs (thisUser) 
          {        
            
            Group grpId = [SELECT Id FROM Group WHERE Name =:Label.CRP_CRM_GroupName];
            
            Group gp = new Group();
            gp.Name = 'Test Group';
            //gp.RelatedId = grpId.Id;
            insert gp;
              
              
            GroupMember GM1 = new GroupMember();
            GM1.GroupId = grpId.id;
            GM1.UserOrGroupId = thisUser.Id;
            insert GM1;
              
            GroupMember GM2 = new GroupMember();
            GM2.GroupId = grpId.id;
            GM2.UserOrGroupId = gp.Id;
            insert GM2;
                
            ST_getGroupofMembers objST_getGroupofMembers = new ST_getGroupofMembers();
            objST_getGroupofMembers.getAllPublicGroupUsers(grpId.Id);  
        }
    } 
}