@isTest
private class ST_QueueManagerTriggerHandlerTest {
	
	@isTest static void test_method_one() {
		
		//create user
		
		Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
       
        UserRole ur = new UserRole(Name = 'Finance Approver');
        insert ur;

        User usr = ST_CreateTestUserUtil.createTestUser(ur.Id, pf.Id, 'Test FirstName', 'Test LastName');
        try
	        {
	            insert usr;
	         }
         Catch(DMLException e)
	         {
	         
	         }
	    system.runAs(usr){
	    	//Create Account
			Account ac= new Account();
			ac.Name ='Test Account';
			insert ac;

			//Create Opp

			Opportunity opp = new Opportunity();
			opp.recordtypeId =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
			opp.Name='Test Opp';
			Opp.AccountId= ac.id;
			opp.stageName ='Prospect';
			opp.CloseDate =date.today();
			insert opp;

			// create QM record
			QueueManager__c qc = new QueueManager__c();
			qc.Opportunity__c = Opp.id;
			insert qc;
	
		test.startTest();

		qc.ownerId=usr.id;
		update qc; 
		test.stopTest();
	}
	}
	
	
}