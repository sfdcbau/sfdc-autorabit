/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Sanjeev Kaurav  <skaurav@servicetitan.com>
* @version        1.1
* @created        12-05-2018
* @ClassName      ST_UpdateObDiscoveryCall
* @TestClass      ST_UpdateObDiscoveryCallTest
@Jira UserStroy 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

@isTest
public class ST_UpdateObDiscoveryCallTest {
    
     static testmethod void  TestObDiscoveryCalldate(){
        
        Product2 product = utilityHelperTest.createProduct();
        product.name = 'Platform Payments-Check';
        PRODUCT.Core_Product__c = TRUE;
        insert product;
        
        PricebookEntry standPB = utilityHelperTest.craeteStandardPBE2(product.id);
        insert standPB;
        
        Pricebook2 custPB = utilityHelperTest.craeteCustomPB2();
        insert custPB; 
               
        PricebookEntry pbe = utilityHelperTest.createPricebookEntry(custPB, product);
        insert pbe;
        
        Account acc = utilityHelperTest.createAccount();
        insert acc;
        
        contact con = utilityHelperTest.createContact(acc.id);
        insert con;
        
        list<Opportunity> optyList  = new list<Opportunity>();
        
        Id oppSalesRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity opp = utilityHelperTest.createopportunity(acc.id,custPB.id,con.Id);
        
        insert opp;
        
        OpportunityLineItem ol = utilityHelperTest.createOppProduct(opp.Id, pbe.Id, product.id);
        insert ol;
        
        test.startTest();
 
        Opp.StageName = 'Closed Won';
        Opp.DispositionCode__c = 'Demo went well'; 
        update Opp;
        System.assertEquals('Closed Won', Opp.StageName);
        
        Onboarding__c obSales = utilityHelperTest.createonboarding(acc.id,opp.id);
        obSales.OB_Discovery_Call_Date__c = Opp.OBDiscoveryCallDate__c;
        insert obsales;
        
        opp.OBDiscoveryCallDate__c = system.today() + 1;
        opp.Delayed_Implementation__c = true;
        update opp;
        
        System.assertEquals(System.today(), obSales.OB_Discovery_Call_Date__c);
        test.stopTest();
        
        
    } 

}