/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a handler class for opportunityTrigger.
*
* This class automatically creates an QM records for the Payment-CC/Credit or Finance for Platform.
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jay Prasad   <jprasd@servicetitan.com>
* @version        1.0
* @created        05-29-2018
* @Name           CreateQMonOppHandler
* @Method         handleCreateQM()
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public  class CreateQMonOppTriggerHandler {
    
    private static Boolean bool=false;
    
    public static void handleCreateQM( List<Opportunity> opps){
         //Check if Trigger is switched ON
         List<Trigger_Settings__mdt> TS =  new List<Trigger_Settings__mdt>();
        
        TS = [SELECT Label,isActive__c FROM Trigger_Settings__mdt where Label='ST_CreateQMonOppTrigger' ];
        
        for (Trigger_Settings__mdt sett : TS){
          if(sett.isActive__c == TRUE){
            bool = true;
          }
    }
    system.debug('=====>'+bool);

    if(bool==true){
        list<QueueManager__c> QMlist= new list<QueueManager__c>();
        Group grp = new Group();
        grp=[Select Name, ID from Group where Type = 'Queue' AND Name ='Platform Payments Opps' limit 1];
        for(Opportunity op: opps) {
          if(op.RecordTypeId ==Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId()) {

            QueueManager__c q = new QueueManager__c();
            q.OwnerId = grp.id;
            q.opportunity__c = op.id;

            QMlist.add(q);
          }
            
        }
        system.debug('===>' +QMlist);

        if(!QMlist.isEmpty()){
          insert QMlist;
        }
        
    }
}
}