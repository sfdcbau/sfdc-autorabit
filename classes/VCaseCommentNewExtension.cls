/*
	Extension to add new case comment
*/
public without sharing class VCaseCommentNewExtension 
{
    public Case caseRec {get;set;}
    public CaseComment comment {get; set;}
    
	public VCaseCommentNewExtension(ApexPages.StandardController controller)
    {
        this.loadCase(((Case)controller.getRecord()).Id);
        
        // Set status to none
        this.caseRec.Status = null;
        
        // Initialize new comment
        this.initializeNewComment();
        
    }
    
    public PageReference savePrivateComment() 
    {
        // Insert case comment
        comment.IsPublished = false;
        insert comment;
        
        // Update case
        
        // Update case
        if(this.caseRec.Status != null)
        {
            update caseRec;
        }
        
        // Reset
        this.initializeNewComment();
        
        return null;
    }  
    
    public PageReference savePublicComment() 
    {
        // Insert case comment
        comment.IsPublished = true;
        insert comment;
        
        // Update case
        if(this.caseRec.Status != null)
        {
            update caseRec;
        }
        
        // Reset
        this.initializeNewComment();
        
        return null;
    }  
    
    private void loadCase(Id caseId)
    {
    	List<Case> caseList = [Select Id, Status From Case Where Id = :caseId];
    	if(!caseList.isEmpty())
    	{
    		this.caseRec = caseList[0];
    	}
    }
    
    private void initializeNewComment()
    {
        comment = new CaseComment();
        comment.ParentId = caseRec.id;
    }
}