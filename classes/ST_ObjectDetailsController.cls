/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 11 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 11 Mar, 2019
 # Description...................: This class is used by ST_ObjectDetails Lightning Component.
 # Test Class...................: ST_ObjectDetailsControllerTest
 #####################################################
*/
public with sharing class ST_ObjectDetailsController {
    /**
     * @author : Aman Gupta
     * @date : 11 Mar, 2019
     * @description : This method is called by ST_ObjectDetails Lightning Component.
               [SE-2511] --> Need to show OB Live Date and OB Discovery Call Date on Opportunity.
    */
    @AuraEnabled
    public static Map<String, String> getObBDDetails(String recordId) {
        try {
            // fetch Opportunity
            List<Opportunity> oppObjList = [SELECT 
                                            Id, AccountId 
                                            FROM Opportunity 
                                            WHERE Id = :recordId];
            Map<String, String> response = new Map<String, String> ();
            if(!oppObjList.isEmpty()) {
              List<String> onBDStatusList = new List<String> {'Pre-Implementation', 'Implementation', 'Live', 'Success'};
              // fetch Onboarding__c having RecordType.DeveloperName = 'ST_Onboarding'
                List<Onboarding__c> obBDObjList = [SELECT 
                                                   Id, Name, Live_Date__c, OB_Discovery_Call_Date__c 
                                                   FROM Onboarding__c 
                                                   WHERE Account__c = :oppObjList[0].AccountId AND RecordType.DeveloperName = 'ST_Onboarding' AND Onboarding_Status__c IN :onBDStatusList ORDER BY CreatedDate DESC];
                if(!obBDObjList.isEmpty()) {
                    if(obBDObjList[0].Live_Date__c != null)
                        response.put('OB Live Date', String.valueOf(obBDObjList[0].Live_Date__c)); 
                        //response.put('OB Live Date', obBDObjList[0].Live_Date__c.month() + '/' + obBDObjList[0].Live_Date__c.day() + '/' + obBDObjList[0].Live_Date__c.year()); 
                    if(obBDObjList[0].OB_Discovery_Call_Date__c != null)
                        response.put('OB Discovery Call Date', String.valueOf(obBDObjList[0].OB_Discovery_Call_Date__c)); 
                        //response.put('OB Discovery Call Date', obBDObjList[0].OB_Discovery_Call_Date__c.month() + '/' + obBDObjList[0].OB_Discovery_Call_Date__c.day() + '/' + obBDObjList[0].OB_Discovery_Call_Date__c.year()); 
                }
            }
            return response;
        }catch(Exception ex) { throw new AuraHandledException(ex.getMessage() + '. ' + ex.getStackTraceString()); }
    }
}