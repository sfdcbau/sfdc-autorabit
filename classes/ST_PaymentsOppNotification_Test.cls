/*
Test class for field update in Opportunity based on Onboarding. 
@Method :- ST_PaymentsOppNotification_Test
        Creating all the required data for the test class   
@Method :- testMethod1
*/

@isTest
private class ST_PaymentsOppNotification_Test {
		
    
    @testSetup static void setup() {
    	Account acc = new Account();
    	acc.name = 'test acc';
    	insert acc;
        
        Id OppRecordTypeId = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('ServiceTitan Payments').getRecordTypeId();
        Id ObRecordTypeId = Schema.SObjectType.Onboarding__c.RecordTypeInfosByName.get('ST Onboarding').getRecordTypeId();  
        
        Opportunity opp =new opportunity();
        opp.Name = 'test Opp';
        opp.AccountID = acc.id;
        opp.StageName = 'Prospect';
        opp.recordtypeid = OppRecordTypeId;
        opp.CloseDate = System.today();            
        insert opp;
        
        Onboarding__c ob =new Onboarding__c();
        ob.Name = 'test ob';
        ob.Account__c = acc.id;
        ob.recordtypeid = ObRecordTypeId;
        ob.Projected_Go_Live_Date__c = System.today();            
        insert ob;
    }
    
    @isTest static void testMethod1() {
        Onboarding__c ob1 = [SELECT Id, Account__c, Projected_Go_Live_Date__c FROM Onboarding__c WHERE Name = 'test ob'];
        ob1.Projected_Go_Live_Date__c = System.today() + 15;
        
        Test.startTest();
        update ob1;
        Test.stopTest();
        
        //Opportunity op = [SELECT Id, Name, Sales_OB_Projected_Go_Live_Date__c FROM Opportunity WHERE Name = 'test Opp'];
        //System.assertEquals(System.today() + 15, op.Sales_OB_Projected_Go_Live_Date__c);
    }
}