/*--------------------------------------------------------------------------------------------------------------------------------
Test class for ST_ProductRequestController class. 
@Class :- ST_ProductRequestController
@Method :- submit
@Jira User Story  SC-138
@author  : Kamal
----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class ST_ProductRequestController_Test {
    
    @istest
    //test method for request creation......... 
    static void testProductRequestInsert(){ 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.ProductRequestRecordType).getRecordTypeId();
        ST_ProductRequestController productController = new ST_ProductRequestController();
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.Product_Squad__c = 'CRM';
        refRequest.Request_Type__c = 'Demo';
        refRequest.References_Desired__c = 5;
        refRequest.Matching_Comments__c = 'test comments';
        refRequest.Matching_Criteria__c = 'Vertical';
        refRequest.Request_Description__c = 'test description'; 
        refRequest.Feature_Name__c = 'test feature';
        refRequest.Desired_Engagement_Date__c = System.today()+1 ;
        refRequest.Desired_Completion_Date__c = System.today()+2;
        refRequest.Time_Commitment__c = 'test commitment';
        refRequest.Target_Role_Persona__c = 'test role';
        
        List<Proposed_Account__c> listofProAcc = new List<Proposed_Account__c>();
        
        Proposed_Account__c objProAcc = new Proposed_Account__c();
        objProAcc.Account__c = testAccount.Id;
        insert objProAcc;
        listofProAcc.add(objProAcc);
        productController.lstAcctoShow = listofProAcc;
        productController.recRefRequest = refRequest;
        productController.Submit();   //Invoking submit method.....
        productController.Cancel();   //Invoking cancel method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Request_Type__c, Request_Description__c, Time_Commitment__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].Request_Type__c, 'Demo');
            System.assertEquals(refRequests[0].Request_Description__c, 'test description');
            System.assertEquals(refRequests[0].Time_Commitment__c, 'test commitment');
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId);
            Proposed_Account__c[] proAccounts = [SELECT Id, Name, Request__c FROM Proposed_Account__c WHERE Request__c =:refRequests[0].Id];
            System.assertNotEquals(proAccounts.size(), 0); 
        }   
    }
    
    @istest
    //test method for request creation with invalid status and without proposed account......... 
    static void testProductRequestInsertInvalidStatus(){ 
        
        ST_ProductRequestController productController = new ST_ProductRequestController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'Confirmed'; 
        refRequest.Product_Squad__c = 'CRM';
        refRequest.Request_Type__c = 'Demo';
        refRequest.References_Desired__c = 5;
        refRequest.Matching_Comments__c = 'test comments';
        refRequest.Matching_Criteria__c = 'Vertical';
        refRequest.Request_Description__c = 'test description'; 
        refRequest.Feature_Name__c = 'test feature';
        refRequest.Desired_Engagement_Date__c = System.today()+1 ;
        refRequest.Desired_Completion_Date__c = System.today()+2;
        refRequest.Time_Commitment__c = 'test commitment';
        refRequest.Target_Role_Persona__c = 'test role';
        
        productController.recRefRequest = refRequest;
        productController.Submit();   //Invoking submit method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Reference_Status__c FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertNotEquals(refRequests[0].Reference_Status__c, 'Confirmed');
            Proposed_Account__c[] proAccounts = [SELECT Id, Name, Request__c FROM Proposed_Account__c WHERE Request__c =:refRequests[0].Id];
            System.assertEquals(proAccounts.size(), 0); 
        }   
    }
    //test method for proposed account.......
    @istest
    static void proposedAccountTest(){
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        System.assertEquals(testAccount.Customer_Status_picklist__c, 'Onboarding');
        
        test.startTest();
        Set<Id> setAccId = new Set<Id>();
        List<Proposed_Account__c> lstProAcc = new List<Proposed_Account__c>();
        
        Proposed_Account__c objProAcc = new Proposed_Account__c();
        objProAcc.Account__c = testAccount.Id;
        
        system.assertEquals(objProAcc.Account__c , testAccount.Id);
        system.assertEquals(objProAcc.Id , null);
        
        Proposed_Account__c objProAcc2 = new Proposed_Account__c();
        objProAcc2.Account__c = testAccount.Id;
        
        system.assertEquals(objProAcc.Account__c , testAccount.Id);
        system.assertEquals(objProAcc.Id , null);
        
        setAccId.add(objProAcc2.Account__c);
        
        system.assertEquals(setAccId.size(), 1);
        
        PageReference pageRef = Page.ST_ProductRequest;
        Test.setCurrentPage(pageRef);
        
        ST_ProductRequestController productController = new ST_ProductRequestController();
        productController.acc = objProAcc;
        productController.SaveMultipleAccounts();
         
        productController.acc = objProAcc2;
        productController.SaveMultipleAccounts();
        
        Proposed_Account__c objProAcc1 = new Proposed_Account__c();
        objProAcc1.Account__c = testAccount.Id;
        insert objProAcc1;
        
        lstProAcc.add(objProAcc1);
        system.assertEquals(lstProAcc.size(), 1);
        
        setAccId.add(objProAcc1.Account__c);
        system.assertEquals(setAccId.size(), 1);
        
        productController.acc = objProAcc1;
        productController.setAccProId = setAccId;
        productController.lstAcctoShow = lstProAcc;
        productController.SaveMultipleAccounts();
        
        productController.rowToRemove = 1;
        system.assertEquals(productController.rowToRemove, 1);
        
        productController.removeRowFromAccList();
        
        lstProAcc.clear();
        system.assertEquals(lstProAcc.size(), 0);
        
        productController.lstAcctoShow = lstProAcc;
        productController.removeRowFromAccList();
        
        test.stopTest();
        
    }
    
}