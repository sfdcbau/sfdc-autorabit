/****************************************
* @author   : Mukul Kumar
* @JIRA     : SPR-7
* @Trigger  : ST_OpportunityTrigger
* @testcls  : ST_PaymentOpportunityHandlerTest
* @Description: When ever the Opportunity Stage is changed to Pending Onboarding, system will check the related
* Onboarding records and if any of these Onbording Status is Pending ST Live, it will Close the Opportuniy 
* and create the Asset record related to the Onboarding.
* @Last Modified By: Tharun G
* @Last Modified Date: 02/14/2019
* @Version: V.1.1 
* @Jira User Story: SE-1915(Added the Description in Dev Task, what we updated)
* **************************************/
public class ST_PaymentOpportunityHandler {
    public static void updateOnboardingAndOpportunity(list<Opportunity> listOpportunity, Map<Id, Opportunity> oldMap) {
        Set<Id> opportunityId = new Set<Id>();
        list<Opportunity> updateOpplist = new list<Opportunity>();
        List<Asset> insertAssetList = new List<Asset>();
        Set<id> OnboardingIds = new Set<Id>();
        Set<Id> SetOfOppsToUpdate = new Set<Id>();
        Map<Id, Id> assetMap = new Map<Id, Id>();
        Map<Id,Onboarding__c> mapOfIdAndOnboarding = new Map<Id,Onboarding__c>();
        list<OpportunityLineItem> updateOLIlist = new list<OpportunityLineItem>();
        list<Opportunity> opportunityList = new list<Opportunity>();
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        
        Trigger_Settings__mdt TriggerFlag =  [SELECT 
                                              Label, isActive__c 
                                              FROM Trigger_Settings__mdt 
                                              where Label = 'ST_PaymentOpportunityHandler' limit 1];
        
        // Activate or deactivate this code in production from Meta Data Type by just making isActive = false.
        if(TriggerFlag.isActive__c) {
            for(Opportunity opp : listOpportunity) {
                
                if((oldMap == null || opp.StageName != oldMap.get(opp.Id).StageName) && opp.StageName == 'Pending Boarding' && opp.RecordTypeId == OppRecordTypeId) {
                    opportunityId.add(opp.Id);
                }
            }
            if(opportunityId != null) {
                opportunityList = [Select 
                                   Id, Name, Stagename, (Select Id, name, Primary_Contact__c, Onboarding_Status__c, Opportunity__c, MerchantID__c , Swipers_Shipped__c, Date_of_First_Transaction__c from Onboarding__r)
                                   From Opportunity
                                   Where Id IN : opportunityId AND RecordTypeId = :OppRecordTypeId];
            }
            user currentUser = [Select 
                                Id, By_Pass_Validation__c 
                                From User 
                                Where Id = :userInfo.getuserId()];
            currentUser.By_Pass_Validation__c = true;
            update currentUser;  

            // Adding value inside Map onboarding Id againts onboarding
            if(opportunityList.size() > 0) {
                for(Opportunity op : opportunityList) {      
                    for(Onboarding__c onbrd : op.Onboarding__r) {    
                        
                        if(onbrd.Onboarding_Status__c == 'Pending ST Live') {
             
                            SetOfOppsToUpdate.add(Onbrd.Opportunity__c);
                            OnboardingIds.add(onbrd.Id);
                            mapOfIdAndOnboarding.put(onbrd.id,onbrd);
                        }
                    }
                }
            }
            
            if(SetOfOppsToUpdate.size()>0){
                for(Opportunity Opp: [select id, StageName from Opportunity where id in : SetOfOppsToUpdate]){  
                    Opp.StageName = 'Closed Won';
                    updateOpplist.add(Opp);
                }  
            }
            
            // Updating opportunity list
            if(updateOpplist.size() > 0) {
                update updateOpplist;
            } 
          
            currentUser.By_Pass_Validation__c = false;
            update currentUser;
            
            List<OpportunityLineItem> oliList = [Select id, 
                                                 Product2Id,
                                                 Product2.Name,
                                                 Name,                                               
                                                 OpportunityId, 
                                                 Asset__c,
                                                 Onboarding__c,                                              
                                                 Opportunity.AccountId,
                                                 Opportunity.Account.Name,
                                                 opportunity.Name,
                                                 Opportunity.Primary_Contact__c
                                                 FROM OpportunityLineItem 
                                                 WHERE OpportunityId IN :opportunityId 
                                                 AND Opportunity.hasOpportunitylineitem = true
                                                 AND  Onboarding__c In : OnboardingIds ];   
            
            // Creating Assets related to Account and opportunity with Status equal to onboarding.
            if(oliList.size()>0){
                for(OpportunityLineItem OLI : oliList){
                    
                    DateTime dTt = System.now();
                    String dayString = dTt.format('MM/dd/yyyy');
                    Asset asset_Obj = new Asset();
                    asset_Obj.Name = oli.Opportunity.Account.Name +' '+'-'+' '+ OLI.Product2.Name +' '+ '-'+ ' '+dayString;  
                    asset_Obj.Product2Id = OLI.Product2Id;
                    asset_Obj.AccountId = OLI.Opportunity.AccountId;
                    asset_Obj.Opportunity__c = OLI.OpportunityId;
                    asset_Obj.Onboarding__c = OLI.Onboarding__c;
                    asset_Obj.ContactId = mapOfIdAndOnboarding.get(OLI.Onboarding__c).Primary_Contact__c;
                    asset_Obj.Status = 'Onboarding';
                    asset_Obj.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();   
                    asset_Obj.Number_of_Swipers_Shipped_Asset__c = mapOfIdAndOnboarding.get(OLI.Onboarding__c).Swipers_Shipped__c;
                    asset_Obj.Asset_Merchant_ID__c =   mapOfIdAndOnboarding.get(OLI.Onboarding__c).MerchantID__c;
                    //Addded by vishal for SE-1677
                    asset_Obj.Date_of_First_Transaction__c =   mapOfIdAndOnboarding.get(OLI.Onboarding__c).Date_of_First_Transaction__c;
                    insertAssetList.add(asset_Obj);
                }
            }
            if(insertAssetList.size() > 0) {
                insert insertAssetList;
            }
            // Updating Assets to OLI
            if(insertAssetList.size() > 0) {
                for(Asset ast : insertAssetList) {
                    assetMap.put(ast.Product2Id, ast.Id);
                }
            }
            if(oliList.size() > 0) {
                for(OpportunityLineItem oli : oliList){ 
                    oli.Asset__c = assetMap.get(oli.Product2Id);
                    updateOLIlist.add(oli);
                }
            }  
            if(updateOLIlist.size() > 0) {
                update updateOLIlist;
            }
        } 
    }
}