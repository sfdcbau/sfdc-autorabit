/*
#####################################################
# Created By....................: Tharun G
# Created Date................: 04/03/2019
# Description...................:  This Test class is used to update the Profile to system admin for some of the users
# ApexClass......................:ST_UpdateUserProfile
# version .......................: V 1.0
#####################################################
*/

@isTest
public class ST_UpdateUserProfileTest {
    
    @isTest static void testUserProfile(){
        
        List<Profile> sysAdminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<Profile> SalesProfile= [SELECT Id FROM Profile WHERE Name = 'Standard Sales'];
        
        
        User userObj = new User(ProfileId = SalesProfile[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', 
                                Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', 
                                Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', 
                                EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert userObj;
        test.startTest();
        userobj.ProfileId = sysAdminProfile[0].id;
        update userobj;
        test.stopTest();
        //system.assertEquals('System Administrator', userobj.Profile.Name);
    }
    
    
    
    
}