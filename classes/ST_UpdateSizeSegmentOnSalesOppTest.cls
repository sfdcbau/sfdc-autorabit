/*------------------------------------------------------------------------------
* Test Class:  ST_UpdateSizeSegmentOnSalesOppTest
* Class Name:  ST_UpdateSizeSegmentOnSalesOpp 
* Version: 1
* Coverage:    78%
* Developer :  Rakesh Kanugu
* CreatedDate: 1/10/2019
* Version: 2
* Coverage:    94%
* Developer :  Rakesh Kanugu
* CreatedDate: 1/31/2019
--------------------------------------------------------------------------------*/

@isTest
Public class ST_UpdateSizeSegmentOnSalesOppTest{
    @isTest
    static void Testmeth1(){
        Test.startTest();
        Account A = new Account();
        A.name='test';
        A.Engagement_Cohort__c = 'Test';
        A.Franchise_Strategic__c= true;
        insert A;
        
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.AccountId = A.id;
        op.name='test';
        op.StageName = 'Stage Zero';
        op.CloseDate=system.today();
        op.recordTypeId = recTypeId;
        
        insert op;
        
        A.Franchise_Strategic__c = true;
        update A;
        
        product2 objproduct = utilityHelperTest.createProduct();
        insert objproduct;
        
        PricebookEntry objPricebookEntry = utilityHelperTest.craeteStandardPBE2(objproduct.Id);
        insert objPricebookEntry;
        
        OpportunityLineItem objOpportunityLineItem = utilityHelperTest.createOppProduct(op.Id,objPricebookEntry.Id,objproduct.Id);
        insert objOpportunityLineItem;
        
        A.Franchise_Strategic__c = false;
        A.Estimated_Total_Potential_Technicians__c = null;
        //  update A;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest2 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper2 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper2.accountId = A.id;
        objRequestWrapper2.sizeSegment = 'Enterprise';
        objRequestWrapper2.flagValue = '3';
        lstRequest2.add(objRequestWrapper2);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest2);
        system.assertNotEquals(A.Estimated_Total_Potential_Technicians__c,24);
        
        A.Estimated_Total_Potential_Technicians__c = 27;
        // update A;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest3 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper3 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper3.accountId = A.id;
        objRequestWrapper3.sizeSegment = 'Enterprise';
        objRequestWrapper3.flagValue = '3';
        lstRequest3.add(objRequestWrapper3);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest3);
        system.assertNotEquals(A.Estimated_Total_Potential_Technicians__c,24); 
        
        Test.stopTest();
    }
    @isTest
    static void Testmeth2(){
        Test.startTest();
        Account A = new Account();
        A.name='test';
        A.Franchise_Strategic__c = false;
        insert A;
        
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.AccountId = A.id;
        op.name='test';
        op.StageName = 'Stage Zero';
        op.CloseDate=system.today();
        op.recordTypeId = recTypeId;
        
        insert op;
        
        product2 objproduct = utilityHelperTest.createProduct();
        insert objproduct;
        
        PricebookEntry objPricebookEntry = utilityHelperTest.craeteStandardPBE2(objproduct.Id);
        insert objPricebookEntry;
        
        OpportunityLineItem oppLineItem = new  OpportunityLineItem();
        oppLineItem.Quantity = 10;
        oppLineItem.OpportunityId = Op.id;
        oppLineItem.Product2id = objproduct.id;
        oppLineItem.UnitPrice = 1200; 
        oppLineItem.PricebookEntryId = objPricebookEntry.id;
        oppLineItem.TotalPrice = null;
        oppLineItem.Recurring_Revenue__c = true;
        oppLineItem.Projected_Quantity__c = 3;
        
        insert oppLineItem;
        
        A.Franchise_Strategic__c = false;
        A.Estimated_Total_Potential_Technicians__c = 22;
        
        update A;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest2 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper2 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper2.accountId = A.id;
        objRequestWrapper2.sizeSegment = 'Enterprise';
        objRequestWrapper2.flagValue = '3';
        lstRequest2.add(objRequestWrapper2);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest2);
        system.assertNotEquals(A.Estimated_Total_Potential_Technicians__c,26);
        
        oppLineItem.Quantity =26;
        Update oppLineItem;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest3 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper3 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper3.accountId = A.id;
        objRequestWrapper3.sizeSegment = 'Enterprise';
        objRequestWrapper3.flagValue = '4';
        lstRequest3.add(objRequestWrapper3);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest3);
        system.assertNotEquals(oppLineItem.Quantity,24);
        
        A.Franchise_Strategic__c = true;
        update A;
        
        Test.stopTest();
    }
    
    @isTest
    static void Testmeth3(){
        Test.startTest();
        Account A = new Account();
        A.name='test';
        A.Franchise_Strategic__c = false;
        insert A;
        
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.AccountId = A.id;
        op.name='test';
        op.StageName = 'Stage Zero';
        op.CloseDate=system.today();
        op.recordTypeId = recTypeId;
        
        insert op;
        
        product2 objproduct = utilityHelperTest.createProduct();
        insert objproduct;
        
        PricebookEntry objPricebookEntry = utilityHelperTest.craeteStandardPBE2(objproduct.Id);
        insert objPricebookEntry;
        
        OpportunityLineItem oppLineItem = new  OpportunityLineItem();
        oppLineItem.Quantity = 20;
        oppLineItem.OpportunityId = Op.id;
        oppLineItem.Product2id = objproduct.id;
        oppLineItem.UnitPrice = 1200; 
        oppLineItem.PricebookEntryId = objPricebookEntry.id;
        oppLineItem.TotalPrice      = null;
        oppLineItem.Recurring_Revenue__c = true;
        oppLineItem.Projected_Quantity__c = 3;
        
        insert oppLineItem;
        oppLineItem.Quantity = 30;
        oppLineItem.Managed_tech__c = true;
        update oppLineItem;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest2 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper2 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper2.accountId = A.id;
        objRequestWrapper2.sizeSegment = 'Enterprise';
        objRequestWrapper2.flagValue = '4';
        lstRequest2.add(objRequestWrapper2);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest2);
        system.assertNotEquals( oppLineItem.Quantity,20);
        
        oppLineItem.Quantity = 20;
        oppLineItem.Managed_tech__c = true;
        update oppLineItem;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest5 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper5 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper5.accountId = A.id;
        objRequestWrapper5.sizeSegment = 'Corporate';
        objRequestWrapper5.flagValue = '4';
        lstRequest5.add(objRequestWrapper5);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest5);
        system.assertNotEquals( oppLineItem.Quantity,30);
        
        oppLineItem.Quantity = 28;
        oppLineItem.Managed_tech__c = true;
        update oppLineItem;
        
        Test.stopTest();
    }
    @isTest
    static void Testmeth4(){ 
        
        Test.startTest();
        
        Account A = new Account();
        A.name='test';
        A.Franchise_Strategic__c = false;
        insert A;
        
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.AccountId = A.id;
        op.name='test';
        op.StageName = 'Stage Zero';
        op.CloseDate=system.today();
        op.recordTypeId = recTypeId;
        
        insert op;
        
        product2 objproduct = utilityHelperTest.createProduct();
        insert objproduct;
        
        PricebookEntry objPricebookEntry = utilityHelperTest.craeteStandardPBE2(objproduct.Id);
        insert objPricebookEntry;
        
        OpportunityLineItem oppLineItem = new  OpportunityLineItem();
        oppLineItem.Quantity = 20;
        oppLineItem.OpportunityId = Op.id;
        oppLineItem.Product2id = objproduct.id;
        oppLineItem.UnitPrice = 1200; 
        oppLineItem.PricebookEntryId = objPricebookEntry.id;
        oppLineItem.TotalPrice      = null;
        oppLineItem.Recurring_Revenue__c = true;
        oppLineItem.Projected_Quantity__c = 3;
        
        insert oppLineItem;
        oppLineItem.Quantity = 30;
        oppLineItem.Managed_tech__c = true;
        update oppLineItem;
        
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest3 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper3 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper3.accountId = A.id;
        objRequestWrapper3.sizeSegment = 'Enterprise';
        objRequestWrapper3.flagValue = '3';
        lstRequest3.add(objRequestWrapper3);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest3);
        system.assertNotEquals( oppLineItem.Quantity,20);
        
        oppLineItem.Quantity = 20;
        oppLineItem.Managed_tech__c = true;
        update oppLineItem;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest4 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper4 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper4.accountId = A.id;
        objRequestWrapper4.sizeSegment = 'Corporate';
        objRequestWrapper4.flagValue = '3';
        lstRequest4.add(objRequestWrapper4);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest4);
        system.assertNotEquals( oppLineItem.Quantity,30);
        
        Test.stopTest();
    }
    @isTest
    static void Testmeth5(){ 
        Test.startTest();
        Account A = new Account();
        A.name='test';
        A.Engagement_Cohort__c = 'Test';
        A.Franchise_Strategic__c= true;
        insert A;
        
        Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity op = new Opportunity();
        op.AccountId = A.id;
        op.name='test';
        op.StageName = 'Stage Zero';
        op.CloseDate=system.today();
        op.recordTypeId = recTypeId;
        
        insert op;
        
        A.Franchise_Strategic__c = true;
        update A;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper.accountId = A.id;
        objRequestWrapper.sizeSegment = 'Enterprise';
        objRequestWrapper.flagValue = '1';
        lstRequest.add(objRequestWrapper);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest);
        system.assertNotEquals(A.Franchise_Strategic__c,false);
        
        A.Franchise_Strategic__c = true;
        A.Estimated_Total_Potential_Technicians__c =26;
        update A;
        
        List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper> lstRequest1 = new List<ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper>();
        ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper objRequestWrapper1 = new ST_UpdateSizeSegmentOnSalesOpp.RequestWrapper();
        objRequestWrapper1.accountId = A.id;
        objRequestWrapper1.sizeSegment = 'Enterprise';
        objRequestWrapper1.flagValue = '2';
        lstRequest1.add(objRequestWrapper1);
        ST_UpdateSizeSegmentOnSalesOpp.updateSizeSegmentFromAcc(lstRequest1);
        system.assertNotEquals(A.Estimated_Total_Potential_Technicians__c,24);
        
        product2 objproduct = utilityHelperTest.createProduct();
        insert objproduct;
        
        PricebookEntry objPricebookEntry = utilityHelperTest.craeteStandardPBE2(objproduct.Id);
        insert objPricebookEntry;
        
        OpportunityLineItem objOpportunityLineItem = utilityHelperTest.createOppProduct(op.Id,objPricebookEntry.Id,objproduct.Id);
        insert objOpportunityLineItem;
        
        A.Franchise_Strategic__c = false;
        A.Estimated_Total_Potential_Technicians__c = null;
        update A;
        
    }
}