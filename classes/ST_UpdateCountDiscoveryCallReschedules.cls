/***********************************************
 * @author    : Sanjeev Kaurav
 * @testclass : ST_UpdateCountDiscoveryCallTest
 * @PB Name   : UpdateDispAndSubDispCodes v10
 * @Description: This class is updating Count of discovery call date time and initial discovery call date time whenever changes discovery call date time field and record type is Sales

 * *********************************************/
public class ST_UpdateCountDiscoveryCallReschedules {
    
    @InvocableMethod
    Public static void ST_UpdateResult(list<Opportunity> oppValues){
        
        Map<Id,Opportunity> mapOfIdAndOpportunity = new Map<Id,Opportunity>();   // Map of Opportunity Id and Opportunity 
        list<Opportunity> updateOpportunity = new list<Opportunity>();
        for(Opportunity opp : oppValues){
            if(opp.Discovery_Call_Date_Time__c != null){
                mapOfIdAndOpportunity.put(opp.id,opp);  // Storing value of Opportunity Id and Opportunity
            }
        }
        if(mapOfIdAndOpportunity.keyset().Size()>0){
        for(Opportunity opp : [Select id, Discovery_Call_Date_Time__c, Initial_Discovery_Call_Date_Time__c, Count_of_Discovery_Call_Reschedules__c 
                                    from Opportunity where id IN : mapOfIdAndOpportunity.keyset()]){
                                    if(opp.Count_of_Discovery_Call_Reschedules__c!= null){
                                        opp.Count_of_Discovery_Call_Reschedules__c = opp.Count_of_Discovery_Call_Reschedules__c+1;  // Update Count of Discovery Call Reschedules
                                       // opp.Initial_Discovery_Call_Date_Time__c = opp.Discovery_Call_Date_Time__c;   //// Update Initial Discovery Call Date Time
                                        updateOpportunity.add(opp);        
                                    }
                            
        }
        }
        if(updateOpportunity.size()>0){
            Update updateOpportunity;  //Update Opportunity 
        }
    }
}