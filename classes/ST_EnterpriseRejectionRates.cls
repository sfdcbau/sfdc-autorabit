/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 31 Jan, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 14 Mar, 2019
 # Description...................: This class is used by ST_EnterpriseRejectionRates Lightning Component.
 # Test Class...................: ST_EnterpriseRejectionRatesTest
 #####################################################
*/
public with sharing class ST_EnterpriseRejectionRates {
    /**
     * @author : Aman Gupta
     * @date : 31 Jan, 2019
     * @description : This method is called by ST_EnterpriseRejectionRates Lightning Component.
    */
    @AuraEnabled
    public static List<ST_EnterpriseRejectionRates.TeamMemberWrapper> getTeamMembers(String sDate, String eDate, String onBDStatus, String teamrole, Integer managTechQuantity) {
        try {
            Date startDate = Date.valueOf(sDate);
            Date endDate = Date.valueOf(eDate);
            
            Set<Id> rejOnBDOppId = new Set<Id> ();
            // query Rejected Onboarding records between date filter
            for(Onboarding__c onBDObj: [SELECT 
                                        Id, Opportunity__c
                                        FROM Onboarding__c 
                                        WHERE Opportunity__r.StageName = 'Closed Won' AND Opportunity__r.RecordType.Name = 'Sales' AND Opportunity__r.Managed_Tech_Quantity__c > :managTechQuantity AND RecordType.Name = 'ST Onboarding' AND Onboarding_Status__c = 'Rejected' AND (Rejection_Date__c >= :startDate AND Rejection_Date__c <= :endDate)]) {
                rejOnBDOppId.add(onBDObj.Opportunity__c);
            }
            
            // query Opportunity records
            List<Opportunity> oppObjList = [Select 
                                            Id, Name, Total_Projected_MRR__c, Rejected_boolean__c, StageName, OwnerId, Owner.Name, CloseDate, 
                                            (SELECT Id, UserId, User.Name, TeamMemberRole FROM OpportunityTeamMembers WHERE TeamMemberRole = :teamrole ORDER BY User.Name),
                                            (SELECT Id, Name, Onboarding_Status__c, Rejection_Date__c FROM Onboarding__r WHERE RecordType.Name = 'ST Onboarding')
                                            FROM Opportunity 
                                            WHERE ((StageName = 'Closed Won' AND RecordType.Name = 'Sales' AND Managed_Tech_Quantity__c > :managTechQuantity AND (CloseDate >= :startDate AND CloseDate <= :endDate)) OR (Id IN :rejOnBDOppId)) ORDER BY Name];
            
            Map<Id, OpportunityTeamMember> teamMemObjMap = new Map<Id, OpportunityTeamMember> ();
            Map<Id, List<Opportunity>> teamMemOppMap = new Map<Id, List<Opportunity>> ();
            
            // iterate oppObjList List
            for(Opportunity oppObj : oppObjList) {
                // iterate OpportunityTeamMember records
                for(OpportunityTeamMember oppTMObj : oppObj.OpportunityTeamMembers) {
                    if(!teamMemOppMap.containsKey(oppTMObj.UserId)) {
                        teamMemObjMap.put(oppTMObj.UserId, oppTMObj);
                        teamMemOppMap.put(oppTMObj.UserId, new List<Opportunity> {oppObj});
                    }
                    else {
                        teamMemOppMap.get(oppTMObj.UserId).add(oppObj);        
                    }
                }
            }
            
            List<ST_EnterpriseRejectionRates.TeamMemberWrapper> teamMemWrapObjList = new List<ST_EnterpriseRejectionRates.TeamMemberWrapper> ();
            // iterate teamMemOppMap Map
            for(Id teamMemUserId : teamMemOppMap.keySet()) {
                ST_EnterpriseRejectionRates.TeamMemberWrapper teamMemWrapObj = new ST_EnterpriseRejectionRates.TeamMemberWrapper();
                teamMemWrapObj.oppTMObj = teamMemObjMap.get(teamMemUserId);
                teamMemWrapObj.oppFieldWrapList = new List<ST_EnterpriseRejectionRates.OppFieldWrapper> ();
                
                Decimal noOfRejOpp = 0, totalNoOfOpp = 0, totalRegCMRR = 0, totalCMRR = 0;
                // iterate Opportunity records
                for(Opportunity oppObj : teamMemOppMap.get(teamMemUserId)) {
                    String obStatus = '';
                    Date obRejDate = null;
                    Boolean showOnlyRejOpp = false;
                    // calculating Rejection Rate and Rejection CMRR
                    if(!oppObj.Onboarding__r.isEmpty()) {
                        obStatus = oppObj.Onboarding__r[0].Onboarding_Status__c;
                        obRejDate = oppObj.Onboarding__r[0].Rejection_Date__c;
                        // checking criteria for Onboarding records
                        //if(onBDStatus.equalsIgnoreCase('Rejected') && String.isNotBlank(obStatus) && obStatus.equalsIgnoreCase(onBDStatus) && obRejDate != null && (obRejDate >= startDate && obRejDate <= endDate)) {
                        if(String.isNotBlank(obStatus) && obStatus.equalsIgnoreCase(onBDStatus)) {
                            noOfRejOpp = noOfRejOpp + 1;
                            totalRegCMRR = totalRegCMRR + (Decimal)oppObj.Total_Projected_MRR__c;
                        }
                    }
                    totalNoOfOpp= totalNoOfOpp + 1;
                    totalCMRR = totalCMRR + (Decimal)oppObj.Total_Projected_MRR__c;
                    
                    // populating TeamMemberWrapper
                    String oppCloseDateStr = '';
                    if(oppObj.CloseDate != null)
                        oppCloseDateStr = DateTime.newInstance(oppObj.CloseDate.year(), oppObj.CloseDate.month(), oppObj.CloseDate.day()).format('MMM dd, YYYY');
                    
                    String obRejDateStr = '';
                    if(obRejDate != null)
                        obRejDateStr = DateTime.newInstance(obRejDate.year(), obRejDate.month(), obRejDate.day()).format('MMM dd, YYYY');
                    
                    teamMemWrapObj.oppFieldWrapList.add(new ST_EnterpriseRejectionRates.OppFieldWrapper(oppObj.Id, oppObj.Name, (Integer)oppObj.Rejected_boolean__c, oppObj.StageName, oppObj.OwnerId, oppObj.Owner.Name, oppCloseDateStr, String.valueOf(oppObj.Total_Projected_MRR__c), obStatus, obRejDateStr));
                }
                teamMemWrapObj.rejRate = (totalNoOfOpp != 0) ? ((noOfRejOpp/totalNoOfOpp) * 100).setScale(0) : 0;
                teamMemWrapObj.rejRateOnCMRR = (totalCMRR != 0) ? ((totalRegCMRR/totalCMRR) * 100).setScale(0) : 0;
                teamMemWrapObj.totalRejCMRR = totalRegCMRR;
                teamMemWrapObj.totalCMRR = totalCMRR;
                teamMemWrapObjList.add(teamMemWrapObj);
            }
            return teamMemWrapObjList;
        }catch(Exception ex) {
              throw new AuraHandledException(ex.getStackTraceString());  
        }
    }
    
    /**
     * @author : Aman Gupta
     * @date : 31 Jan, 2019
     * @description : This wrapper class is used by ST_EnterpriseRejectionRates Lightning Component.
    */
    public class TeamMemberWrapper {
        @AuraEnabled
        public OpportunityTeamMember oppTMObj;
        
        @AuraEnabled
        public List<OppFieldWrapper> oppFieldWrapList;
        
        @AuraEnabled
        public Decimal rejRate;
        
        @AuraEnabled
        public Decimal rejRateOnCMRR;
        
        @AuraEnabled
        public Decimal totalRejCMRR;
        
        @AuraEnabled
        public Decimal totalCMRR;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 31 Jan, 2019
     * @description : This wrapper class is used by ST_EnterpriseRejectionRates Lightning Component.
    */
    public class OppFieldWrapper {
        @AuraEnabled
        public String oppId;
        
        @AuraEnabled
        public String oppName;
        
        @AuraEnabled
        public Integer rejBoolean;
        
        @AuraEnabled
        public String stageName;
        
        @AuraEnabled
        public String ownerId;
        
        @AuraEnabled
        public String ownerName;
        
        @AuraEnabled
        public String closeDate;
        
        @AuraEnabled
        public String totalCMRR;
        
        @AuraEnabled
        public String onBDStatus;

        @AuraEnabled
        public String onBDRejDate;
        
        public OppFieldWrapper(String oppId, String oppName, Integer rejBoolean, String stageName, String ownerId, String ownerName, String closeDate, String totalCMRR, String onBDStatus, String onBDRejDate) {
            this.oppId = oppId;
            this.oppName = oppName;
            this.rejBoolean = rejBoolean;
            this.stageName = stageName;
            this.ownerId = ownerId;
            this.ownerName = ownerName;
            this.closeDate = closeDate;
            this.totalCMRR = totalCMRR;
            this.onBDStatus = onBDStatus;
            this.onBDRejDate = onBDRejDate;
        }
    }
}