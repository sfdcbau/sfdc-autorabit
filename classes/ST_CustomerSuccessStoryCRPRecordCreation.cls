/**
* ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐

* This is called from 'CustomerSuccessStory: Approval process and Email Alerts' process on customer Success story object
* This class creates a Customer Reference program record when customer success story is apporoved by all approvers
* ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          12-17-2018
* @Jira User Story  SC-26
* ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘*/

public class ST_CustomerSuccessStoryCRPRecordCreation {
    
    @InvocableMethod
    // method is called from process 'CustomerSuccessStory: Approval process and Email Alerts'
    public static void insertNewCRPRecord(List<Id> customerSuccessStory){
        Customer_Reference_Program__c newCRP = new  Customer_Reference_Program__c();
        
        List<Customer_Reference_Program__c> lstCRPtoInsert = new List<Customer_Reference_Program__c>();
        List<Customer_Reference_Details__c> lstCRDtoInsert = new List<Customer_Reference_Details__c>();
        Map<Id,List<Success_Story__c>> mapIdtoCSS = new Map<Id,List<Success_Story__c>>();
        List<Customer_Reference_Program__c> lstCRPtoUpdate = new List<Customer_Reference_Program__c>();
        List<Success_Story__c> lstCSStoUpdate = new List<Success_Story__c>();
        List<Success_Story__c> lstnewCSStoUpdate = new List<Success_Story__c>(); 
        Set<Id> setContactId = new Set<Id>();
        List<Customer_Reference_Program__c> lstCRPtoUpdateCRDId = new List<Customer_Reference_Program__c>();
        Map<Id,Success_Story__c> mapIdtoCSStoUpdate = new Map<Id,Success_Story__c>();
        
        //query the success story and creating map of contacts to story....
        for(Success_Story__c eachStory : [SELECT Id, Account__c, Contact__c,  Status__c, Public_Use_of_Name__c, Torch_Network__c 
                                          FROM Success_Story__c WHERE Id = :customerSuccessStory]){
                                              if(!mapIdtoCSS.containsKey(eachStory.Contact__c))
                                                  mapIdtoCSS.put(eachStory.Contact__c, new List<Success_Story__c>()); 
                                              mapIdtoCSS.get(eachStory.Contact__c).add(eachStory);
                                              lstnewCSStoUpdate.add(eachStory); 
                                              setContactId.add(eachStory.Contact__c);
                                          }
        
        //query and updating CRP if a CRP exists for the contact......
        for(Customer_Reference_Program__c eachCRP : [SELECT Id, Contact__c, Is_Customer_Success__c 
                                                     FROM Customer_Reference_Program__c WHERE Contact__c in :setContactId]){ 
                                                         if(mapIdtoCSS.containsKey(eachCRP.Contact__c) && mapIdtoCSS.get(eachCRP.Contact__c) != null){ 
                                                             eachCRP.Is_Customer_Success__c = true;
                                                             lstCRPtoUpdate.add(eachCRP);
                                                             for(Success_Story__c eachStory : mapIdtoCSS.get(eachCRP.Contact__c)){
                                                                 if(customerSuccessStory.contains(eachStory.Id)){
                                                                     eachStory.Customer_Reference_Program_Id__c = eachCRP.Id;
                                                                     system.debug('hello 1' + lstCSStoUpdate);
                                                                     mapIdtoCSStoUpdate.put(eachStory.Id,eachStory);
                                                                     //lstCSStoUpdate.add(eachStory);
                                                                 }
                                                             } 
                                                         }
                                                     }
        
        //creating new CRP and CRD if CRP does not exist for the contact.........
        if(lstnewCSStoUpdate != null && lstnewCSStoUpdate.size() > 0 && mapIdtoCSStoUpdate.size() == 0){
            for(Success_Story__c eachNewCSS : lstnewCSStoUpdate){
                Customer_Reference_Details__c newCRD = new  Customer_Reference_Details__c();
                newCRD.Activity_Types__c = 'Prospect Calls';
                lstCRDtoInsert.add(newCRD); 
                
                newCRP.Account__c = eachNewCSS.Account__c;
                newCRP.Contact__c = eachNewCSS.Contact__c;
                newCRP.CRP_Status__c = eachNewCSS.Status__c;
                newCRP.Approved_Date__c = System.today();
                newCRP.Torch_Network__c = eachNewCSS.Torch_Network__c;
                newCRP.Is_Customer_Success__c = true;
                lstCRPtoInsert.add(newCRP); 
            }
        } 
        
        try{
            if(lstCRPtoInsert != null && lstCRPtoInsert.size() > 0 && lstCRDtoInsert != null && lstCRDtoInsert.size() > 0){
                insert lstCRDtoInsert;
                insert lstCRPtoInsert;
            }
            else{
                update lstCRPtoUpdate;
            }
            if(mapIdtoCSStoUpdate != null && mapIdtoCSStoUpdate.size() > 0){
                update mapIdtoCSStoUpdate.values();
            }   
            
            //putting CRD Id in CRP.....
            for(Integer i = 0 ; i< lstCRPtoInsert.size(); i++){
                lstCRPtoInsert[i].Customer_Reference_Detail_Id__c = lstCRDtoInsert[i].Id;
            }
            
            lstCRPtoUpdateCRDId.addAll(lstCRPtoInsert);
            
            if(lstCRPtoUpdateCRDId != null && lstCRPtoUpdateCRDId.size() > 0){
                update lstCRPtoUpdateCRDId;
            }
        }
        catch(DmlException ex) {
            System.debug('An unexpected error has occurred: ' + ex.getMessage());
        }         
        
        //maping new CRP id in Customer Success Story record......
        if(lstCRPtoInsert != null && lstCRPtoInsert.size() > 0){
            for(Customer_Reference_Program__c eachCSStoUpdate : lstCRPtoInsert){
                if(mapIdtoCSS.containsKey(eachCSStoUpdate.Contact__c) && mapIdtoCSS.get(eachCSStoUpdate.Contact__c) != null){ 
                    for(Success_Story__c eachStory : mapIdtoCSS.get(eachCSStoUpdate.Contact__c)){
                        if(customerSuccessStory.contains(eachStory.Id)){
                            eachStory.Customer_Reference_Program_Id__c = eachCSStoUpdate.Id;
                            lstCSStoUpdate.add(eachStory);
                        }
                    }
                }
            }
            if(lstCSStoUpdate != null && lstCSStoUpdate.size() > 0){
                update lstCSStoUpdate;
            } 
        }
    }
}