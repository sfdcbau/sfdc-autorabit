public class VTopicAssignmentTriggerDispatcher 
{
	
	public void dispatch() 
	{
		if(Trigger.isBefore && Trigger.isDelete)
			validateFeedItemTopicAssignment();
        
		if(Trigger.isAfter && Trigger.isInsert)
			validateFeedItemTopicAssignment();
        
    }
    
    private void validateFeedItemTopicAssignment()
    {
        Set<String> canEditAnyTopicAssignmentProfileSet = new Set<String>();
        Boolean canEditAnyTopicAssignment = false;
        
        // Collect profiles that can edit any topic assignment
        for(Edit_All_Topic_Assignment_Profile__c cs : Edit_All_Topic_Assignment_Profile__c.getAll().values())
        {
            if(!canEditAnyTopicAssignmentProfileSet.contains(cs.Profile_ID__c))
                canEditAnyTopicAssignmentProfileSet.add(cs.Profile_ID__c);
        }
        
        // Check if current user profile can edit any topic assignment
        if(!canEditAnyTopicAssignmentProfileSet.isEmpty())
        {
            List<User> userList = [Select Id From User Where Id = :UserInfo.getUserId() And ProfileId In :canEditAnyTopicAssignmentProfileSet Limit 1];
            if(!userList.isEmpty())
            	canEditAnyTopicAssignment = true;
        }
        
        System.debug('##### canEditAnyTopicAssignmentProfileSet: '+canEditAnyTopicAssignmentProfileSet);
        System.debug('##### canEditAnyTopicAssignment: '+canEditAnyTopicAssignment);
        
        // Validate non-self topic assignment
        If(!canEditAnyTopicAssignment)
        {
            List<TopicAssignment> topicAssignmentList = new List<TopicAssignment>();
            Set<Id> entitySet = new Set<Id>();
            
            if(Trigger.isDelete)
            {
                for(TopicAssignment ta : (List<TopicAssignment>)Trigger.old)
                {
                    if(ta.EntityType == 'FeedItem')
                    {
                        topicAssignmentList.add(ta);
                        
                        if(!entitySet.contains(ta.EntityId))
                            entitySet.add(ta.EntityId);
                    }
                }
            }
            else if(Trigger.isInsert)
            {
                for(TopicAssignment ta : (List<TopicAssignment>)Trigger.new)
                {
                    if(ta.EntityType == 'FeedItem')
                    {
                        topicAssignmentList.add(ta);
                        
                        if(!entitySet.contains(ta.EntityId))
                            entitySet.add(ta.EntityId);
                    }
                }
            }
            
            System.debug('##### topicAssignmentList: '+topicAssignmentList);
            System.debug('##### entitySet: '+entitySet);
            
            if(!topicAssignmentList.isEmpty())
            {
                Map<Id, FeedItem> feedItemMap = new Map<Id, FeedItem>([Select Id, CreatedById From FeedItem Where Id In :entitySet]);
                
                for(TopicAssignment ta : topicAssignmentList)
                {
                    if(feedItemMap.containsKey(ta.EntityId))
                    {
                        FeedItem fi = feedItemMap.get(ta.EntityId);
                        
                        if(fi.CreatedById != UserInfo.getUserId())
                            ta.addError('You do not have permission to assign topic to this post');
                    }
                }
            }
        }
        
    }
    
}