/*--------------------------------------------------------------------------------------------------------------------------------
Test class for ST_AccSalesRequestController class. 
        Creating all the required data for the test class   
@Method :- submit(), getContacts()
@Jira User Story  SC-12

----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class ST_AccSalesRequestController_Test {
    
    @istest
    //test method for request creation......... 
    static void testRefRequestInsert(){ 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.SalesRequestRecordType).getRecordTypeId();
         
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Role__c = 'Owner';
        insert testContact; 
         
        List<Contact> listofContacts = new List<Contact>();
        List<SelectOption> options = new List<SelectOption>(); 
        listofContacts = [SELECT Id, Name FROM Contact WHERE AccountId = :testAccount.Id];
        If(listofContacts.size() != null && listofContacts.size()>0){
            for(Contact con : listofContacts){
              options.add(new SelectOption(con.Id, String.valueOf(con.Name))); 
            }
        }
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.Product_Areas__c = 'Dispatch';
        refRequest.Request_Type__c = 'Call';
        refRequest.References_Desired__c = 3;
        refRequest.Matching_Comments__c = 'test comments';
        refRequest.Matching_Criteria__c = 'Vertical';
        refRequest.Request_Description__c = 'test description';  
        refRequest.Desired_Completion_Date__c = System.today()+2; 
        
        List<Proposed_Account__c> listofProAcc = new List<Proposed_Account__c>();
        
        Proposed_Account__c objProAcc = new Proposed_Account__c();
        objProAcc.Account__c = testAccount.Id;
        insert objProAcc;
        listofProAcc.add(objProAcc);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        PageReference pageRef = Page.ST_Accsalesrequest;
        test.setCurrentPage(pageRef);
  		pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        ST_AccSalesRequestController requestController = new ST_AccSalesRequestController(sc);
        requestController.lstAcctoShow = listofProAcc;
        requestController.recRefRequest = refRequest;
        requestController.Submit();   //Invoking submit method.....
        requestController.getContacts();   //Invoking getContacts method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Request_Type__c, Request_Description__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].Request_Type__c, 'Call');
            System.assertEquals(refRequests[0].Request_Description__c, 'test description'); 
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId);
            Proposed_Account__c[] proAccounts = [SELECT Id, Name, Request__c FROM Proposed_Account__c WHERE Request__c =:refRequests[0].Id];
            System.assertNotEquals(proAccounts.size(), 0); 
        }   
    }
    
    @istest
    //test method for request creation with invalid status and without proposed account......... 
    static void testRefRequestInsertInvalidStatus(){
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.SalesRequestRecordType).getRecordTypeId();
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
          
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        PageReference pageRef = Page.ST_Accsalesrequest;
        test.setCurrentPage(pageRef);
  		pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        ST_AccSalesRequestController requestController = new ST_AccSalesRequestController(sc);
         
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'Completed'; 
        refRequest.Product_Areas__c = 'Dispatch';
        refRequest.Request_Type__c = 'Call';
        refRequest.References_Desired__c = 3;
        refRequest.Matching_Comments__c = 'test comments';
        refRequest.Matching_Criteria__c = 'Vertical';
        refRequest.Request_Description__c = 'test description';  
        refRequest.Desired_Completion_Date__c = System.today()+2; 
         
        requestController.recRefRequest = refRequest;
        requestController.Submit();   //Invoking submit method..... 
        
        Reference_Request__c[] refRequests = [SELECT Id, Reference_Status__c FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertNotEquals(refRequests[0].Reference_Status__c, 'Completed');
            Proposed_Account__c[] proAccounts = [SELECT Id, Name, Request__c FROM Proposed_Account__c WHERE Request__c =:refRequests[0].Id];
            System.assertEquals(proAccounts.size(), 0); 
        }   
    }
    //test method for proposed account.......
    @istest
    static void proposedAccountTest(){
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        System.assertEquals(testAccount.Customer_Status_picklist__c, 'Onboarding');
        
        test.startTest();
        Set<Id> setAccId = new Set<Id>();
        List<Proposed_Account__c> lstProAcc = new List<Proposed_Account__c>();
        
        Proposed_Account__c objProAcc = new Proposed_Account__c();
        objProAcc.Account__c = testAccount.Id;
        
        system.assertEquals(objProAcc.Account__c , testAccount.Id);
        system.assertEquals(objProAcc.Id , null);
        
        Proposed_Account__c objProAcc2 = new Proposed_Account__c();
        objProAcc2.Account__c = testAccount.Id;
        
        system.assertEquals(objProAcc.Account__c , testAccount.Id);
        system.assertEquals(objProAcc.Id , null);
        
        setAccId.add(objProAcc2.Account__c);
        
        system.assertEquals(setAccId.size(), 1);
        
        

        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        PageReference pageRef = Page.ST_Accsalesrequest;
        test.setCurrentPage(pageRef);
  		pageRef.getParameters().put('id', String.valueOf(testAccount.Id));
        ST_AccSalesRequestController requestController = new ST_AccSalesRequestController(sc);
        requestController.acc = objProAcc;
        requestController.SaveMultipleAccounts();
         
        requestController.acc = objProAcc2;
        requestController.SaveMultipleAccounts();
        
        Proposed_Account__c objProAcc1 = new Proposed_Account__c();
        objProAcc1.Account__c = testAccount.Id;
        insert objProAcc1;
        
        lstProAcc.add(objProAcc1);
        system.assertEquals(lstProAcc.size(), 1);
        
        setAccId.add(objProAcc1.Account__c);
        system.assertEquals(setAccId.size(), 1);
        
        requestController.acc = objProAcc1;
        requestController.setAccProId = setAccId;
        requestController.lstAcctoShow = lstProAcc;
        requestController.SaveMultipleAccounts();
        
        requestController.rowToRemove = 1;
        system.assertEquals(requestController.rowToRemove, 1);
        
        requestController.removeRowFromAccList();
        
        lstProAcc.clear();
        system.assertEquals(lstProAcc.size(), 0);
        
        requestController.lstAcctoShow = lstProAcc;
        requestController.removeRowFromAccList();
        
        test.stopTest();
        
    } 
}