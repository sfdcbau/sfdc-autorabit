public without sharing class VIdeaCommentTriggerDispatcher 
{
	
	public void dispatch() 
	{
		if(Trigger.isAfter && Trigger.isInsert)
			executeAfterInsert();
    }
    
    private void executeAfterInsert()
    {
		sendEmailNotifications();
    }
    
    /*
		Notify Idea Owner and Subscribers when there are new comments.

		Updates:
			- [20160714 SC]: Only notify subscribers on new comments when they subscribed for 'AllUpdates'.
	*/
    private void sendEmailNotifications()
    {
        if(Email_Template_Setting__c.getInstance('NewIdeaComment') != null)
        {
            List<EmailTemplate> emailtemplateList = [Select Id, Subject, HtmlValue, Body From EmailTemplate Where Id = :Email_Template_Setting__c.getInstance('NewIdeaComment').EmailTemplateID__c Limit 1];
            if(!emailtemplateList.isEmpty())
            {
                List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
                Map<Id, Set<Id>> ideaRecipientSetMap = new Map<Id, Set<Id>>(); // Map<Idea Id, Set<User Id>>
                Map<Id, Set<Id>> ideaSubscriberSetMap = new Map<Id, Set<Id>>(); // Map<Idea Id, Set<User Id>>
                Map<Id, Idea> ideaMap = new Map<Id, Idea>();
                Map<String, String> stringReplaceMap = new Map<String, String>();
                Set<Id> ideaSet = new Set<Id>();
                
                // Collect ideas
                for(IdeaComment ic : (List<IdeaComment>)Trigger.new)
                {
                    if(!ideaSet.contains(ic.IdeaId))
                    {
                        ideaSet.add(ic.IdeaId);
                    }
                }
                
                // Collect subscribers
                for(Idea_Subscription__c es : [Select Idea__c, Subscriber__c From Idea_Subscription__c 
                                               	Where Idea__c In :ideaSet And (Notify_of_Update_Type__c = 'AllUpdates' Or Notify_of_Update_Type__c = null)])
                {
                    if(!ideaSubscriberSetMap.containsKey(es.Idea__c))
                    {
                        ideaSubscriberSetMap.put(es.Idea__c, new Set<Id>());
                    }
                    
                    if(!ideaSubscriberSetMap.get(es.Idea__c).contains(es.Subscriber__c))
                    {
                        ideaSubscriberSetMap.get(es.Idea__c).add(es.Subscriber__c);
                    }
                }
                
                // Collect recipients
                for(IdeaComment ic : [Select Id, CreatedById, IdeaId, Idea.CreatedById From IdeaComment Where Id In :(List<IdeaComment>)Trigger.new])
                {
                    if(!ideaRecipientSetMap.containsKey(ic.IdeaId))
                    {
                        ideaRecipientSetMap.put(ic.IdeaId, new Set<Id>());
                    }
                    
                    // Add owner
                    if(!ideaRecipientSetMap.get(ic.IdeaId).contains(ic.Idea.CreatedById))
                    {
                        ideaRecipientSetMap.get(ic.IdeaId).add(ic.Idea.CreatedById);
                    }
                    
                    // Add subscribers
                    if(ideaSubscriberSetMap.containsKey(ic.IdeaId))
                    {
                        for(Id subscriberId : ideaSubscriberSetMap.get(ic.IdeaId))
                        {
                            if(subscriberId != ic.CreatedById && !ideaRecipientSetMap.get(ic.IdeaId).contains(subscriberId))
                            {
                                ideaRecipientSetMap.get(ic.IdeaId).add(subscriberId);
                            }
                        }           
                    }
                }
                
                // Collect emails
                if(!ideaRecipientSetMap.isEmpty())
                {
                    ideaMap = new Map<Id, Idea>([Select Id, Title From Idea Where Id In :ideaSet]);
                    stringReplaceMap = new Map<String, String>();
                    
                    for(Id ideaId : ideaRecipientSetMap.keySet())
                    {   
                        Idea myidea = ideaMap.get(ideaId);
                        
                        stringReplaceMap.put('{!Idea.Id}', myidea.Id);
                        stringReplaceMap.put('{!Idea.Title}', myidea.Title);
                        
                        for(Id userId : ideaRecipientSetMap.get(ideaId))
                        {	
                            String subject = emailtemplateList[0].Subject;
                            String textbody = emailtemplateList[0].Body;
                            String htmlbody = emailtemplateList[0].HtmlValue;
                            
                            for(String k : stringReplaceMap.keySet())
                            {
                                if(stringReplaceMap.get(k) != null)
                                {
                                    subject = subject.replace(k, stringReplaceMap.get(k));
                                    textbody = textbody.replace(k, stringReplaceMap.get(k));
                                    htmlbody = htmlbody.replace(k, stringReplaceMap.get(k));
                                }
                            }
                            
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            if(Email_Template_Setting__c.getInstance('NewIdeaComment').Organization_Wide_Email_Address_Id__c != null)
                            {
                                email.setOrgWideEmailAddressId(Email_Template_Setting__c.getInstance('NewIdeaComment').Organization_Wide_Email_Address_Id__c);
                            }
                            email.setTargetObjectId(userId);
                            email.setSaveAsActivity(false);
                            email.setTemplateId(emailtemplateList[0].Id);
                            email.setSubject(subject);
                            email.setPlainTextBody(textbody);
                            email.setHtmlBody(htmlbody);
                            emailList.add(email);
                        }
                    }
                }
                
                System.debug('#### ideaSet: '+ideaSet.size());
                System.debug('#### ideaRecipientSetMap SIZE: '+ideaRecipientSetMap.size());
                System.debug('#### ideaRecipientSetMap: '+ideaRecipientSetMap);
                System.debug('#### ideaSubscriberSetMap SIZE: '+ideaSubscriberSetMap.size());
                System.debug('#### ideaSubscriberSetMap: '+ideaSubscriberSetMap);
                System.debug('#### emailList: '+emailList.size());
                
                // Send emails
                if(!emailList.isEmpty())
                {
                    try
                    {
                        Messaging.sendEmail(emailList);
                    }
                    catch(Exception ex)
                    {
                        System.debug('#### Email Error: ' + ex);
                    }
                }  
            }
        }          
    }

}