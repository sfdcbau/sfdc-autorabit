/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This Class is used to clone the Opportunity for Payments RecordType once the Opportunity is ClosedWon.
*
* This class automatically creates Products and Onboarding records by using Closed C/W Oppty or Add Products Ligntning Actions
* It assign backs the created OB to the OLI.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Rakesh Kanugu
* @version        1.1
* @created        12-26-2018
@Updated        10/10/2018
* @Name           ST_ClonedOpportunityForPaymentsContrl
* @TestClass      ST_ClonedOpptyForPaymentsContrlTest
*         
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class ST_ClonedOpportunityForPaymentsContrl {
    
    /**
* This method is used to clone the opportunity with selected products and creating onboarding record.
* */
    @AuraEnabled
    public static String cloneOpportunity(Id oppId,String action,String productIds){
        String resp = '';
        try{
            if(String.isNotBlank(oppId)){
                if(action == 'clone'){
                    Opportunity objOpp = [SELECT ID,Name,Account.Name,AccountId,CloseDate,StageName,type,CampaignId,recordTypeId,Last_Contact_Date__c,Promotion_Offered__c,Description,Vendor_Used_Today__c FROM Opportunity WHERE Id =:oppId];
                    Opportunity objOpportunity = new Opportunity();
                    objOpportunity = objOpp.clone(false, false, false, false);
                    objOpportunity.Name = objOpportunity.Name+' Cloned';
                    objOpportunity.StageName = 'Prospect';
                    objOpportunity.CloseDate = system.today() + 30;
                    objOpportunity.Parent_Opportunity__c = oppId;
                    insert objOpportunity;
                    resp = 'Opprotunity Cloned Successfully with Name: '+objOpportunity.Name;
                    addProductAndOnboarding(objOpportunity.Id,objOpp.Account.Name,objOpp.accountId,productIds,null);
                    resp = objOpportunity.Id;
                }else if(action == 'add'){
                    Opportunity objOpp = [SELECT ID,Name,Account.Name,AccountId,CloseDate,StageName,type,CampaignId,recordTypeId,Last_Contact_Date__c,Promotion_Offered__c,Description,Vendor_Used_Today__c FROM Opportunity WHERE Id =:oppId];
                    addProductAndOnboarding(objOpp.Id,objOpp.Account.Name,objOpp.accountId,productIds,objOpp.stageName);
                    resp = objOpp.Id;
                }
            }
        }catch(Exception ex){
            resp = 'ERROR: '+ex.getMessage();
        }
        return resp;
    }
    
    /**
* This method is used to add products and creating onboarding record for the selected product.
* */
    public static void addProductAndOnboarding(String opportunity_ID,String accountName,String accountID,String productIds,String stageName){
        if(String.isNotBlank(productIds)){
            List<String> lstProdIds = (List<String>)JSON.deserialize(productIds, List<String>.class);
            Set<String> setProdIds = new Set<String>();
            setProdIds.addAll(lstProdIds);
            String payments_Product = Label.Payments_Product;
            Set<String> setProdNames = null;
            if(String.isNotBlank(payments_Product))
                setProdNames = getProdNames(payments_Product);
            list<PriceBookEntry> lstPriceBookEntry = null;
            if(!Test.isRunningTest())
                lstPriceBookEntry = [SELECT Id, Product2Id, Product2.Id, Product2.Name FROM PriceBookEntry WHERE Product2.Name IN:setProdNames AND PriceBook2.isStandard=true];
            else if(Test.isRunningTest())
                lstPriceBookEntry = [SELECT Id, Product2Id, Product2.Id, Product2.Name FROM PriceBookEntry WHERE Product2.Name IN('Platform Payments-Credit Card')];
            Map<String,String> mapProdIdAndPriceBookId = new Map<String,String>();
            if(lstPriceBookEntry != null && !lstPriceBookEntry.isEmpty()){
                for(PriceBookEntry objPriceBookEntry : lstPriceBookEntry){
                    mapProdIdAndPriceBookId.put(objPriceBookEntry.Product2Id,objPriceBookEntry.Id);
                }
            }
            List<OpportunityLineItem> lstOpportunityLineItem = new List<OpportunityLineItem>();
            //List<onboarding__c> lstOnboarding = new List<onboarding__c>();
            Id recTypeId = Schema.SObjectType.onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            List<Product2> lst_Product = [SELECT Id,Name FROM Product2 WHERE Id IN:setProdIds];
            QueueSObject QueueID = [Select Queue.Id, Queue.Name, Queue.Type from QueueSObject WHERE Queue.Type ='Queue' AND Queue.Name = 'Payment Onboarding' Limit 1];
            Map<String,onboarding__c> mapOnboarding = new Map<String,onboarding__c>();
            
            for(Product2 ObjProd : lst_Product){
                OpportunityLineItem oli = new OpportunityLineItem();
                oli.OpportunityId = opportunity_ID; 
                if(mapProdIdAndPriceBookId != null && mapProdIdAndPriceBookId.get(ObjProd.Id) != null)
                    oli.PricebookEntryId=mapProdIdAndPriceBookId.get(ObjProd.Id);
                oli.Quantity = 1;
                oli.TotalPrice = 0;
                oli.Product2Id = ObjProd.Id;
                lstOpportunityLineItem.add(oli);
                
                 String accName;
                
                Integer AccLength = accountName.length();
                if(AccLength>=31){
                  String splitedAccName = accountName.substring(0, 30);
                    accName = splitedAccName;
                }
                else{
                    accName = accountName;
                }

                
                
                
                onboarding__c objOnboarding = new onboarding__c();
                objOnboarding.Name = accName+'-'+ObjProd.Name+'-'+system.today().format();
                // Ravi- SE-2900: Put "0" as a value in Swiper Information section for Check OB records
                if(ObjProd.Name.containsIgnoreCase('Check')) {
                    objOnboarding.of_Swipers_to_Bill__c = 0;
                    objOnboarding.Swipers_Needed__c	= 0;
                }
                objOnboarding.Account__c = accountId;
                objOnboarding.Opportunity__c = opportunity_ID;
                objOnboarding.RecordTypeId = recTypeId;
                objOnboarding.Onboarding_Status__c = 'Pending Typeform Submission';
                objOnboarding.ownerId = QueueID.Queue.Id;
                if(String.isNotBlank(stageName) && stageName == 'Closed Won')
                   objOnboarding.OnboardingAfterSale__c = true;
                //lstOnboarding.add(objOnboarding);
                mapOnboarding.put(ObjProd.Id,objOnboarding);
            }
            /*if(String.isNotBlank(stageName) && stageName == 'Closed Won'){
                Opportunity objOpportunity = new Opportunity(Id=opportunity_ID,BypassValidation__c=true);
                update objOpportunity;
            }*/
            //system.debug('--stageName--'+stageName);
            List<User> lstUser = null;
            if(String.isNotBlank(stageName) && stageName == 'Closed Won'){
                lstUser = [SELECT Id,By_Pass_Validation__c FROM USER WHERE Id=:UserInfo.getUserId()];
                //system.debug('--lstUser--'+lstUser);
                if(lstUser != null && !lstUser.isEmpty()){
                    lstUser[0].By_Pass_Validation__c = true;
                    update lstUser;
                }
                //system.debug('--lstUser1--'+lstUser);
            }
            insert lstOpportunityLineItem;
            /*if(String.isNotBlank(stageName) && stageName == 'Closed Won'){
                Opportunity objOpportunity = new Opportunity(Id=opportunity_ID,BypassValidation__c=false);
                update objOpportunity;
            }*/
            
            if(mapOnboarding != null && !mapOnboarding.isEmpty()){
                insert mapOnboarding.values();
                for(OpportunityLineItem objOppLineItem : lstOpportunityLineItem){
                    if(mapOnboarding.get(objOppLineItem.Product2Id) != null){
                        objOppLineItem.Onboarding__c = mapOnboarding.get(objOppLineItem.Product2Id).Id;
                    }
                }
                update lstOpportunityLineItem;
            }
            if(String.isNotBlank(stageName) && stageName == 'Closed Won'){
                lstUser = [SELECT Id,By_Pass_Validation__c FROM USER WHERE Id=:UserInfo.getUserId()];
                if(lstUser != null && !lstUser.isEmpty()){
                    lstUser[0].By_Pass_Validation__c = false;
                    update lstUser;
                }
            }
        }
    }
    
/**
  This method is used to get the products using custom label.
**/
    @AuraEnabled
    public static List<Product2> getProducts(){
        String payments_Product = Label.Payments_Product;
        List<Product2> lstProduct = null;
        if(String.isNotBlank(payments_Product)){
            Set<String> setProdNames = getProdNames(payments_Product);
            lstProduct = [SELECT Id,Name FROM Product2 WHERE Name IN:setProdNames];// ('Platform Payments-Check','Platform Payments-Credit Card')
        }
        return lstProduct;
    }   
    
    @AuraEnabled
    public static String getOppStageName(Id oppId){
        String stageName = '';
        List<Opportunity> lstOpp = [SELECT ID,Name,Account.Name,AccountId,CloseDate,StageName FROM Opportunity WHERE Id =:oppId AND recordtype.name = 'ServiceTitan Payments' and stageName ='Closed Won'];
        if(lstOpp != null && !lstOpp.isEmpty()){
            if(String.isNotBlank(lstOpp[0].stageName))
                stageName = lstOpp[0].stageName;
        }
        return stageName;
    }
    
    /**
* This method is used to get the products names using custom label.
**/
    public static Set<String> getProdNames(String payments_Product){
        Set<String> setProdNames = new Set<String>();
        List<String> lstProdNames = payments_Product.split(',');
        setProdNames.addAll(lstProdNames);
        return setProdNames;
    }
}