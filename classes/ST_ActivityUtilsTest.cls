@isTest
private class ST_ActivityUtilsTest {
	

	@isTest static void test_method_one() {
		Account a = new Account();
        a.Name = 'Test Account';
		a.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
		insert a;

		Contact c = new Contact();
        c.FirstName = 'Joe';
        c.LastName = 'Smith';
        c.AccountId = a.Id;
        c.Email = 'test@test.com';
        insert c;
        
        //Inserting Tasks
        Task[] tList = new list<task>();
        Event[] eList = new list<event>();

        //Inserting 4 tasks
        List<Account> acclist = new List<Account>();
         acclist.add(a);
        for(Account acc : acclist) {
        	for(Integer i=0; i<4; i++) {
            Task t = new Task();
            t.Status = 'Completed';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            t.Activity_Type_NEW__c= 'DM Connect';
            t.subject = 'Call';
            t.WhatId = acc.id;
            t.OwnerId = UserInfo.getUserId();
            //t.WhatId = acc.Id;
            //if(i==1) t.WhatId = c.Id;
            //if(i==2) t.WhoId = c.Id;
            //if(i==3) t.WhoId = l.Id;
            tList.add(t);
            System.debug( '====>' +i+ '===>' +tList) ;
        	}
           
        }    
            //test.starttest();
           //ST_ActivityUtils au = new ST_ActivityUtils(acclist);
			//au.updateAccountActivityCount();
			//system.assertEquals(4, [SELECT id, Calls__c from ACCOUNT where Id = a.id] );
			insert tList;
			//test.stoptest();
	}
    
    @isTest static void test_method_Two() {
        Account a = new Account();
        a.Name = 'Test Account';
        a.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        insert a;
        Contact c = new Contact();
        c.FirstName = 'Joe';
        c.LastName = 'Smith';
        c.AccountId = a.Id;
        c.Email = 'test@test.com';
        insert c;
        
        //Inserting Tasks
        Task[] tList = new list<task>();
        Event[] eList = new list<event>();
        for(Integer i=0; i<4; i++) {
            Task t = new Task();
            t.Status = 'Completed';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            t.Activity_Type_NEW__c= 'DM Connect';
            t.subject = 'Call';
            t.WhoId = c.id;
            t.OwnerId = UserInfo.getUserId();
            tList.add(t);
            System.debug( '====>' +i+ '===>' +tList) ;
       }
        insert tList;
    }
    
    @isTest static void test_method_Three() {
        Account a = new Account();
        a.Name = 'Test Account';
        a.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        insert a;
        Opportunity O = new Opportunity();
        O.name= 'Jeo';
        O.accountid = a.Id;
        O.stageName='Stage Zero';
        o.closeDate= system.today();
        insert O;
        
        //Inserting Tasks
        Task[] tList = new list<task>();
        Event[] eList = new list<event>();
        for(Integer i=0; i<4; i++) {
            Task t = new Task();
            t.Status = 'Completed';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            t.Activity_Type_NEW__c= 'DM Connect';
            t.subject = 'Call';
            t.Whatid = O.id;
            t.OwnerId = UserInfo.getUserId();
            tList.add(t);
            System.debug( '====>' +i+ '===>' +tList) ;
       }
        insert tList;
    }
    @isTest static void test_method_four() {
        Account a = new Account();
        a.Name = 'Test Account';
        a.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        insert a;
        Lead L = new Lead();
        L.LastName = 'Smith';
        //L.AccountId = a.Id;
        L.status = 'Lead';
        L.company= 'ABC';
        L.Leadsource='BD-Other';
        insert L;
        
        //Inserting Tasks
        Task[] tList = new list<task>();
        Event[] eList = new list<event>();
        for(Integer i=0; i<4; i++) {
            Task t = new Task();
            t.Status = 'Completed';
            t.Priority = 'Normal';
            t.Type = 'Scheduled Call Back';
            t.Activity_Type_NEW__c= 'DM Connect';
            t.subject = 'Call';
            t.WhoId = L.id;
            t.OwnerId = UserInfo.getUserId();
            tList.add(t);
            System.debug( '====>' +i+ '===>' +tList) ;
       }
        insert tList;
    }
    
	
}