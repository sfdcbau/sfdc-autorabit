public with sharing class VCommunityCaseController 
{
  
    /*Get current user*/
    @AuraEnabled
    public static User getCurrentUser()
    {
        return [Select Id, Name, UserType From User Where Id = :UserInfo.getUserId() Limit 1];
    }
    
    /*Get case*/
    @AuraEnabled
    public static Case getCase(String caseId) {
        try {
            List<Case> caseList = [Select Id, CaseNumber, isClosed, Status, IsEscalated, Escalation_Details__c, CreatedDate, ClosedDate, Subject, Priority 
                    From Case Where Id = :caseId Limit 1];
            if(!caseList.isEmpty())
            {
                system.debug('#### Case: '+caseList[0]);
                return caseList[0];
            }
        } catch (Exception e) {
            System.debug(e);
        }
        
        return null;
    }
    
    
    /*Open Close case*/
    @AuraEnabled
    public static Case openCloseCase(String caseId, String statusName) {
        try {
            List<Case> caseList = [Select Id, Status, CreatedDate, ClosedDate From Case Where Id = :caseId Limit 1];
            if(!caseList.isEmpty())
            {
                system.debug('#### Open Close Case: '+caseList[0]);
                
                caseList[0].Status = statusName;
                update caseList;
                
                return VCommunityCaseController.getCase(caseId);
            }
        } catch (Exception e) {
            System.debug(e);
        }
        
        return null;
    }
    
    /*Escalate case*/
    @AuraEnabled
    public static Case escalateCase(String caseId, String escalationDetails) {
        System.debug('#### IN - escalationDetails: '+escalationDetails);
    
        try {
            List<Case> caseList = [Select Id, IsEscalated, Escalation_Details__c, IsClosed, Escalate_Case__c From Case Where Id = :caseId Limit 1];
            if(!caseList.isEmpty())
            {
                system.debug('#### Escalate Case: '+caseList[0]);
                
                caseList[0].IsEscalated = true;
                caseList[0].Escalate_Case__c = true;
                caseList[0].Escalation_Details__c = escalationDetails;
                update caseList;
                
                return VCommunityCaseController.getCase(caseId);
            }
        } catch (Exception e) {
            System.debug(e);
        }
        
        return null;
    }
    
    /*Get case comments*/
    @AuraEnabled
    public static List<CaseCommentObj> getCaseComments(String caseId, Integer noOfComments) {
        List<CaseCommentObj> caseCommentList = new List<CaseCommentObj>();
        try {
            Set<id> cuset = new Set<id>();
            
            System.debug('caseId: '+caseId);
            System.debug('noOfComments: '+noOfComments);
            
            for(CaseComment cc : [Select id, commentBody, createdById, createdBy.name, createdDate
                            From CaseComment
                            Where parentId = :caseId
                                And IsDeleted = false 
                        Order By createdDate Desc
                        Limit :Integer.valueOf(noOfComments)]){
                
                cuset.add(cc.createdById);
                
                CaseCommentObj cco = new CaseCommentObj(cc);
                caseCommentList.add(cco);
            }
            
            // Collect creator photo url
            Map<id,User> cumap = new Map<id,User>([Select id, fullPhotoUrl, smallPhotoUrl From User Where id In :cuset]);
            
            // setup the collection
            for(CaseCommentObj cco : caseCommentList)
            {
                if(cumap.containsKey(cco.caseComment.createdById))
                {
                    User u = cumap.get(cco.caseComment.createdById);
                    cco.createdByFullPhotoUrl = u.fullPhotoUrl;
                    cco.createdBySmallPhotoUrl = u.smallPhotoUrl;
                }
            }
        } catch (Exception e) {
            System.debug(e);
        }
        return caseCommentList;
    }
    
    /*Get total case comments*/
    @AuraEnabled
    public static Integer getTotalCaseComments(String caseId) {
        Integer totalComments = 0;
        try {
            totalComments = [SELECT count() FROM CaseComment WHERE IsDeleted = false AND parentId = :caseId];
        } catch (Exception e) {
            System.debug(e);
        }
        return totalComments;
    }
    
    /*Check if the profile is allowed to access cases in community*/
    @AuraEnabled
    public static Boolean hasCaseAccess() 
    {
        Boolean hasAccess = true;
        List<Profile> profileList = [SELECT Id, Name From Profile Where Id=:Userinfo.getProfileId() Limit 1];
        
        for(Community_No_Case_Access_Profiles__c cs : Community_No_Case_Access_Profiles__c.getAll().values())
        {
            if(profileList[0].Name.containsIgnoreCase(cs.Name_Contains__c))
            {
                hasAccess = false;
                break;
            }
        }
        
        return hasAccess;
    }
    
    /* 
        Wrapper class to include creator photo url
    */
    public class CaseCommentObj
    {
        @AuraEnabled
        public CaseComment caseComment {get;set;}
        
        @AuraEnabled
        public String createdBySmallPhotoUrl {get;set;}
        
        @AuraEnabled
        public String createdByFullPhotoUrl {get;set;}
        
        public caseCommentObj(CaseComment cc)
        {
            this.caseComment = cc;
        }
    }
}