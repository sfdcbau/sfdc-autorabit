/**********************************************************
*@author             : Mukul Kumar
*@Description        : All Onboarding Trigger event will be managed here. 
*                      Code should be written in the respective handlers and call method from required event method.
*@NewUtility handler : Create new method for sales functionality in "ST_SalesOpportunityTriggerUtilityHandler" class  and call it from this class..
*                      Create new method for Payment functionalities in "ST_PaymentOppTriggerUtilityHandler" class and call it from this class.
*@JIRA               : SDM-136
**********************************************************/

public class ST_OnboardingTriggerEventHandler {
    
    // All before insert event logic should be called from beforeInsert method
    public static void beforeInsert(List<Onboarding__c> triggerNew) {
        
    }
    // All after insert Event logic should be called from afterInsert method
    public static void afterInsert(List<Onboarding__c> triggerNew, Map<Id, Onboarding__c> newMap) {
        ST_CreateMavenLinkProject.CreateMavenLink(triggerNew);  
        OnboardingTriggerhandler.handleInsertAsset(triggerNew);
        OnboardingMRRTriggerHandler.handleInsertUpdateMRR(triggerNew);
        ST_PaymentsOnboardingHandler.onAfterInsert(triggerNew, newMap);
    }
    // All after insert Event logic should be called from beforeUpdate method
    public static void beforeUpdate(List<Onboarding__c> triggerNew, List<Onboarding__c> triggerOld, Map<Id, Onboarding__c> newMap, Map<Id, Onboarding__c> oldMap) {
        ST_PaymentsOnboardingHandler.onBeforeInsert(triggerNew, oldMap);
        
        // To handle the OB status between Pre-implementation and Implementation.
        ST_SalesOnboardingHandler.updateOnboardingStatus(triggerNew, oldMap);
    }
    // All after insert Event logic should be called from afterUpdate method
    public static void afterUpdate(List<Onboarding__c> triggerNew, List<Onboarding__c> triggerOld, Map<Id, Onboarding__c> newMap, Map<Id, Onboarding__c> oldMap) {
        ST_PaymentsOnboardingHandler.updateDisposCodeandSubDisposCode(triggerNew, oldMap);
        ST_PaymentsOnboardingHandler.CreationAsset(triggerNew, oldMap);
        OnboardingMRRTriggerHandler.handleInsertUpdateMRR(triggerNew);
        ST_PaymentsOppNotification.updateGoLiveDateField(triggerNew, oldMap);
        ST_PaymentsOnboardingHandler.onAfterUpdate(triggerOld, oldMap, triggerNew, newMap);
        ST_PaymentsOnboardingHandler.updateAsset(triggerNew, OldMap);
    }
    // All after insert Event logic should be called from beforeDelete method
    public static void beforeDelete(List<Onboarding__c> triggerOld, Map<Id, Onboarding__c> oldMap) {
        
    }
    // All after insert Event logic should be called from afterDelete method
    public static void afterDelete(List<Onboarding__c> triggerOld, Map<Id, Onboarding__c> oldMap) {
        
    }
}