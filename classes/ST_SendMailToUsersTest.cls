@isTest
public with sharing class ST_SendMailToUsersTest {
    public static testMethod void testRecordsCreation1(){
        
        Account__c acctNew = new Account__c(name = '1 - Code Red',Current_Health_Score__c = '1 - Code Red');
        insert acctNew;
        
        Account__c acctOld = new Account__c(name = '3',Current_Health_Score__c = '3');
        insert acctOld;
        
        Account acNew = new Account(Time_Zone__c = 'Pacific (UTC-8:00)',Software_newpicklist__c = 'Acowin',Pricing_Model__c = 'New Pricing (Managed Techs)',Residential__c = 10,Initial_Username__c = 'ska',Tenant_ID__c  = '23456789',Tenant_Name__c = 'Tenant',Estimated_Total_Potential_Technicians__c = 2,Estimated_Total_Potential_Office_Users__c = 1,Name = 'Test Account', BillingCity='Gwl', BillingState = 'Uttar pradesh', BillingCountry='india', Industry__c = 'Plumbing', No_Known_Active_Partners__c = true, Influencer__c = 'Unknown',Health_Score__c = acctNew.Current_Health_Score__c);
        insert acNew;
        
        
        Contact con =new Contact(Lastname ='Tets',accountid=acNew.id,Role__c = 'Owner');
        insert con;
        List<Account> newAccList = new list<Account>();
        newAccList.add(acNew);     
        acNew.Health_Score__c = '3';
        update acNew;
        
        //get standard pricebook
        Id pricebookId = Test.getStandardPricebookId();

        Product2 prd1 = new Product2 (Name='Managed Tech',Description='Test Product Entry 1',productCode = 'ABC', isActive = true,core_product__c = true);
        insert prd1;
        
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
        insert pbe1;
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        //ServiceTitan Payments
        Id oppSalesRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Opportunity opp1 = new Opportunity (Competing_Software__c = 'AppointmentPlus',recordtypeid = OppRT,ImplementationDate__c = system.today(),Name='Opp1',StageName='Stage Zero',OBDiscoveryCallDate__c =System.today()+55,Onboarding_POC__c=con.id, CloseDate=Date.today(),Pricebook2Id = pbe1.Pricebook2Id, AccountId = acNew.id);
        insert opp1;
         
        list<Opportunity> oppNewList = new list<Opportunity>();
        oppNewList.add(opp1);
         
        OpportunityLineItem lineItem1 = new OpportunityLineItem (OpportunityID=opp1.id,PriceBookEntryID=pbe1.id , quantity=35, totalprice=200 ,Product2id = prd1.id);
        insert lineItem1;
        
        
                 
        list<opportunity> opptest = [SELECT Id, StageName FROM opportunity WHERE Id =: opp1.Id limit 1 ];
        
         opptest[0].StageName='Closed Won';
         opptest[0].recordtypeid = oppSalesRT;
         opptest[0].Address_Industry_Verified__c = true;
         opptest[0].AE_Discovery_POC__c = con.id;
         //opptest[0].Legacy_Opportunity_Id__c = '1245678';
         update opptest;
         
         Onboarding__c obSales = new Onboarding__c();
         obSales.Account__c = acNew.id;
         obsales.name= 'Managed';
         obSales.Opportunity__c = opptest[0].id ;
         obSales.Live_Date__c = system.today() - 90;
         obSales.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId(); 
         insert obSales;
         ST_SendMailToUsers.ST_SendMail(newAccList);
        
    }
     //Added by Vishal Labh SE-2856
     public static testMethod void testRecordsCreation2() {
           Account accObj = utilityHelperTest.createAccount();
           accObj.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Strategic').getRecordTypeId();
           insert accObj;
           
          Onboarding__c obObj = new Onboarding__c();
          obObj.Account__c = accObj.id;
          obObj.name= 'New Onbaording';
          obObj.Live_Date__c = system.today() - 90;
          obObj.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId(); 
          insert obObj;
        
    }
  
}