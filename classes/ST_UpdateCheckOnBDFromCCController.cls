/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 13 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 14 Mar, 2019
 # Description...................: This class is used by ST_UpdateCheckOnBDFromCC Lightning Component.
 # Test Class...................: ST_UpdateCheckOnBDFromCCControllerTest
 #####################################################
*/
public with sharing class ST_UpdateCheckOnBDFromCCController {
    /**
     * @author : Aman Gupta
     * @date : 14 Mar, 2019
     * @description : This method is called by ST_UpdateCheckOnBDFromCC Lightning Component.
					  [SE-1890] --> Button to copy over CC onboarding fields onto Check based on DBA.
    */
    @AuraEnabled
    public static String updateCheckOnBD(String oppRecId, String ccDBA, Map<String, String> dataMap) {
        try {
            // fetch Opportunity
            List<Opportunity> oppObjList = [SELECT 
                                            Id,
                                            (SELECT Id, Name, Onboarding_Status__c FROM Onboarding__r WHERE RecordType.Name = 'Payments Onboarding' AND Name like '%Check%' AND DBA__c = :ccDBA AND Onboarding_Status__c != 'Live')
                                            FROM Opportunity 
                                            WHERE Id = :oppRecId];
            
            List<Onboarding__c> onBDObjList = new List<Onboarding__c> ();
            
            if(!oppObjList.isEmpty() && !oppObjList[0].Onboarding__r.isEmpty()) {
                // iterate all Check Onboarding's
                for(Onboarding__c onBDObj : oppObjList[0].Onboarding__r) {
                    onBDObj.Typeform_Recieved_Date__c = (String.isNotBlank(dataMap.get('Typeform_Recieved_Date__c'))) ? Date.valueOf(dataMap.get('Typeform_Recieved_Date__c')) : null;
                    onBDObj.Primary_Contact__c = dataMap.get('Primary_Contact__c');
                    onBDObj.Supporting_Docs__c = dataMap.get('Supporting_Docs__c');
                    onBDObj.Business_License_Received__c = (String.isNotBlank(dataMap.get('Business_License_Received__c'))) ? Date.valueOf(dataMap.get('Business_License_Received__c')) : null;
                    onBDObj.Driver_s_License_Received__c = (String.isNotBlank(dataMap.get('Driver_s_License_Received__c'))) ? Date.valueOf(dataMap.get('Driver_s_License_Received__c')) : null;
                    onBDObj.SSN_Received__c = (String.isNotBlank(dataMap.get('SSN_Received__c'))) ? Date.valueOf(dataMap.get('SSN_Received__c')) : null;
                    onBDObj.Voided_Check_Received__c = (String.isNotBlank(dataMap.get('Voided_Check_Received__c'))) ? Date.valueOf(dataMap.get('Voided_Check_Received__c')) : null;
                    onBDObj.CC_Statement_Received__c = (String.isNotBlank(dataMap.get('CC_Statement_Received__c'))) ? Date.valueOf(dataMap.get('CC_Statement_Received__c')) : null;
                    onBDObj.Bank_Statements_Received__c = (String.isNotBlank(dataMap.get('Bank_Statements_Received__c'))) ? Date.valueOf(dataMap.get('Bank_Statements_Received__c')) : null;
                    onBDObj.Financial_Statements_Received__c = (String.isNotBlank(dataMap.get('Financial_Statements_Received__c'))) ? Date.valueOf(dataMap.get('Financial_Statements_Received__c')) : null;
                    onBDObjList.add(onBDObj);
                }
            }
            else {
               return 'DBA doesn\'t match with Check Onboarding\'s!'; 
            }
            
            // update if onBDObjList not empty
            if(!onBDObjList.isEmpty())
                update onBDObjList;
            
            return 'Check Onboarding\'s Updated Successfully!';
        } catch(Exception ex) { throw new AuraHandledException(ex.getMessage() + '. ' + ex.getStackTraceString()); }
    }
}