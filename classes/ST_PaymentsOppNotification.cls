/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This class automatically executed when 'Projected Go Live Date field' of an 'ST Onboarding' record type On Onboarding Object updates the Sales OB Projected Go Live Date.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          11-22-2018
* @Jira User Story  SE-915, SE-1223  
* @Author           Sanjeev
* @TestClass		ST_PaymentsOppNotificationTest	
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘\
* 
*/
public class ST_PaymentsOppNotification {
    
    public static void updateGoLiveDateField(List<Onboarding__c> obList, Map<Id, Onboarding__c> oldMap ){
        
        // Fetching Opportunity RecordType ID...
        Id OppRecordTypeId = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('ServiceTitan Payments').getRecordTypeId();
        Id ST_OnboardingRecordTypeId = Schema.SObjectType.Onboarding__c.RecordTypeInfosByName.get('ST Onboarding').getRecordTypeId();
        Map<Id, Onboarding__c> mapOfIdAndOnboarding = new Map<Id, Onboarding__c>();
        List<Opportunity> updateOpportunity = new List<Opportunity>();
        
        for(Onboarding__c Onb : obList){ 
            if(Onb.RecordtypeId == ST_OnboardingRecordTypeId && Onb.Projected_Go_Live_Date__c != oldMap.get(Onb.Id).Projected_Go_Live_Date__c && Onb.Projected_Go_Live_Date__c != null && oldMap.get(Onb.Id).Projected_Go_Live_Date__c != null){
                mapOfIdAndOnboarding.put(Onb.Account__c, Onb);     //Fetching Account Id from Onboarding and Onboarding
            }
        }
        
        user currentUser = [Select 
                            Id, By_Pass_Validation__c 
                            From User 
                            Where Id = :userInfo.getuserId()];
        currentUser.By_Pass_Validation__c = true;
        update currentUser;
        
        if(mapOfIdAndOnboarding.keyset().size() > 0) {
            //Querying on ServiceTitan Payments Opportunity related to Onboarding's account
            list<opportunity> oppList = [SELECT Id, Name, Sales_OB_Projected_Go_Live_Date__c, AccountId FROM Opportunity WHERE RecordTypeId = :OppRecordTypeId AND AccountId = :mapOfIdAndOnboarding.keyset() ORDER BY createdDate DESC LIMIT 1];
            
            for(Opportunity opp : oppList){
                if(mapOfIdAndOnboarding.containskey(opp.accountId)){
                    opp.Sales_OB_Projected_Go_Live_Date__c = mapOfIdAndOnboarding.get(opp.accountId).Projected_Go_Live_Date__c;  //Store Projected go live date value into Sales ob projected go live date.
                    updateOpportunity.add(opp);
                }
            }
            
            if(updateOpportunity.size()>0){
                Update updateOpportunity;   //Updating the Opportunity
            }
        }  
        currentUser.By_Pass_Validation__c = false;
        update currentUser;  
    }
    
}