/*
#####################################################
# Created By....................: Vishal Labh
# Created Date................: 01-04-2019
# Last Modified By..........: Vishal Labh
# Description...................: This is test class for ST_BatchEmailNotification.
#####################################################
*/
@isTest
private class ST_BatchEmailNotificationTest {
    
    @testSetup static void setup() {
        // insert Product2
        Product2 prodObj1 = utilityHelperTest.createProduct();
        prodObj1.name = 'Platform Payments-Check';
        prodObj1.Core_Product__c = false;
        
        Product2 prodObj2 = utilityHelperTest.createProduct();
        prodObj2.name = 'Platform Payments-Credit Card';
        prodObj2.Core_Product__c = false;
        
        insert new List<Product2> {prodObj1, prodObj2};
            
            // insert Pricebook2
            Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
        insert new List<Pricebook2> {custPBObj};
            
            // insert PricebookEntry
            PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
        insert new List<PricebookEntry> {pbeObj1};
            
            // insert Account
            Account accObj = utilityHelperTest.createAccount();
        accObj.New_Markets__c = False;
        insert new List<Account> {accObj};
            
            // insert Contact
            Contact conObj = utilityHelperTest.createContact(accObj.Id);
        conObj.Role__c = 'Co-Owner';
        insert new List<Contact> {conObj};
            
            Date discDate = System.today().addDays(-1);
        Datetime discDateTime = Datetime.newInstance(discDate.year(), discDate.month(), discDate.day());
        
        // insert Opportunity
        Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj1.Name = oppObj1.Name + '1';
        oppObj1.Primary_Channel__c = 'MKTG';
        oppObj1.Initial_Discovery_Call_Date_Time__c = discDateTime;
        oppObj1.Discovery_Call_Date_Time__c = null;
        
        Opportunity oppObj2 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj2.Name = oppObj2.Name + '2';
        oppObj2.Primary_Channel__c = 'SALES - Warm Outbound';
        oppObj2.Initial_Discovery_Call_Date_Time__c = discDateTime;
        oppObj2.Discovery_Call_Date_Time__c = null;
        
        Opportunity oppObj3 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj3.Name = oppObj2.Name + '3';
        oppObj3.Primary_Channel__c = 'SALES - Cold Outbound';
        oppObj3.Initial_Discovery_Call_Date_Time__c = discDateTime;
        oppObj3.Discovery_Call_Date_Time__c = null;
        
        Opportunity oppObj4 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj4.Name = oppObj2.Name + '4';
        oppObj4.Primary_Channel__c = 'EVENTS';
        oppObj4.Initial_Discovery_Call_Date_Time__c = discDateTime;
        oppObj4.Discovery_Call_Date_Time__c = null;
        
        insert new List<Opportunity> {oppObj1, oppObj2, oppObj3, oppObj4};
            }
    @isTest static void testST_BatchEmailNotificationBatch() {
        
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<UserRole> rObj = [SELECT Id, Name FROM UserRole where name = 'Sales Manager'];
        
        // inserting User
        User userObj1 = new User(ProfileId = pObj[0].Id, UserRoleId = rObj[0].Id , LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1};
            
            System.runAs (userObj1) {
                test.startTest();
                List<Opportunity> oppObjList = [Select Id from Opportunity];
                System.assertEquals(4, oppObjList.size());
                
                for(Opportunity oppObj : oppObjList) {
                    oppObj.Size_Segment__c = 'Corporate';
                    oppObj.OwnerId = userObj1.Id;
                }
                update oppObjList;  
                test.stopTest();  
                
                ST_BatchEmailNotification batch = new ST_BatchEmailNotification();
                Database.executeBatch(batch);
                
                String sch = '20 30 8 10 2 ?';
                String jobID = system.schedule('Merge Job', sch, batch);
            }
    }   
}