@isTest
private class ST_DataExportPolicyConditionTest{
    static testmethod void test1() {
        
        User u = new User(    
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser11111000@amamama.com',
             Username = 'puse11111r000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'    
        );
        insert u;
          
        Map<String, String> eventData = new Map<String, String>();
        eventData.put('NumberOfRecords', '19');
        String orgId = '123456789';
        Lead Lead = utilityHelperTest.createLead();
        insert Lead;
        //Creating Data Export event
        TxnSecurity.Event e = new TxnSecurity.Event(  
            orgId /* organizationId */,
            u.Id /* userId */,
            'Lead' /* 'Lead' */,
             'DataExport' /* Action */,
             'Lead' /*resourceType */,  
              '1234567890' /* entityId */,
              Datetime.newInstance(2017, 5, 23) /* timeStamp */,
              eventData /* data */ 
        );  
        
        ST_DataExportPolicyCondition condition = new ST_DataExportPolicyCondition();
        condition.evaluate(e);
        System.assertEquals(false, condition.evaluate(e));
  
    }  
}