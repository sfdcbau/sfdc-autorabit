public without sharing class VCaseEmailFeedbackCommentController 
{
	private static final String QUERYNAME_CASEID = 'id';
	
    public Case mycase {get;set;}
    public String errorMessage {get;set;}
    public Boolean hasError {get;set;}
    public Boolean isCompleted {get;set;}
    public CaseComment mycasecomment {get;set;}
	
	public VCaseEmailFeedbackCommentController()
	{
		this.resetView();
        this.loadCase(this.getQueryRequest(QUERYNAME_CASEID));
        
        if(this.mycase != null)
        {
            mycasecomment = new CaseComment();
            mycasecomment.ParentId = this.mycase.Id;
        }
        else
        {
            this.errorMessage = 'Case not found.';
            this.hasError = true;
        }
	}
	
    /*
    	Get the requested query
    */
    private String getQueryRequest(String queryname)
    {
    	if(ApexPages.currentPage().getParameters().get(queryname) != null)
    	{
    		return ApexPages.currentPage().getParameters().get(queryname).trim();
    	}
    	return '';
    }
    
    /*
        Reset variables
    */
    private void resetView()
    {
        this.hasError = false;
        this.isCompleted = false;
        this.errorMessage = '';
    }
    
    public PageReference submit()
    {
        this.resetView();
        
        if(mycasecomment.CommentBody == null)
        {
            this.hasError = true;
            this.errorMessage += 'Please complete your feedback<br/>';
        }
        
        if(!this.hasError)
        {
            Savepoint sp = Database.setSavepoint();
            
            try
            {
                insert mycasecomment;
                
                this.isCompleted = true;
            }
            catch(Exception ex)
            {
                Database.rollback(sp);
                
                this.errorMessage = ex.getMessage();
                this.hasError = true;
                
                System.debug('#### Email Error: ' + ex);
                    
                List<String> adminemailaddressList = new List<String>();
                adminemailaddressList.add('sulung.chandra@veltig.com');
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setToAddresses(adminemailaddressList);
                email.setSubject('Unable to save email comment feedback');
                email.setPlainTextBody(ex.getMessage());
                email.setSaveAsActivity(false);
                
                List<Messaging.SingleEmailMessage> erroremailList = new List<Messaging.SingleEmailMessage>();
                erroremailList.add(email);
                
                try
                {
                    Messaging.sendEmail(erroremailList);
                }
                catch(Exception ex2){}  
            }
            
            System.debug('##### Error: ' + errorMessage);
        }
        
        return null;
    }
    private void loadCase(String caseId)
    {
        List<Case> caseList = [Select Id, CaseNumber, Rate__c, Final_Resolution_Rate__c From Case Where id = :caseId Limit 1];
        if(!caseList.isEmpty())
        {
            this.mycase = caseList[0];
        }
    }
    
}