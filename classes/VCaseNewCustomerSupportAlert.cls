global without sharing class VCaseNewCustomerSupportAlert 
{
	
	global class Request
	{
		@InvocableVariable(label='Mininum Number of Cases' required=true)
		public Integer minNumberOfCases;
        
		@InvocableVariable(label='Recipient Contact ID' required=true)
		public String recipientContactId;
        
		@InvocableVariable(label='Case Record Type Developer Name' required=true)
		public String caseRecordTypeName;
        
		@InvocableVariable(label='Case Status' required=true)
		public String caseStatus;
		
		@InvocableVariable(label='Case ID' required=true)
		public Id caseId;
	}
	
    @InvocableMethod(label='Send New Customer Support Alert')
	global static void sendNewCustomerSupportAlert(List<Request> requests)
    {
        List<EmailTemplate> emailtemplateList = new List<EmailTemplate>();
        String customSettingName = 'NewCustomerSupportCase';
        String orgWideEmailAddressId = null;
        
        // Get email template setting
        if(Email_Template_Setting__c.getInstance(customSettingName) != null)
        {
            emailtemplateList = [Select Id, Subject, HtmlValue, Body From EmailTemplate Where Id = :Email_Template_Setting__c.getInstance(customSettingName).EmailTemplateID__c Limit 1];
        	
            if(Email_Template_Setting__c.getInstance(customSettingName).Organization_Wide_Email_Address_Id__c != null)
            {
                orgWideEmailAddressId = Email_Template_Setting__c.getInstance(customSettingName).Organization_Wide_Email_Address_Id__c;
            }
        }
        
        if(!emailtemplateList.isEmpty() && !requests.isEmpty())
        {
            try
            {
                List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            
                // For this purpose, we just need to use the first request in the requests list
                List<RecordType> recordtypeList = [Select Id From RecordType Where SobjectType = 'Case' And DeveloperName = :requests[0].caseRecordTypeName Limit 1];
                if(!recordtypeList.isEmpty())
                {
                    List<Case> caseList = [Select Id From Case Where RecordTypeId = :recordtypeList[0].Id And Status = :requests[0].caseStatus Limit :requests[0].minNumberOfCases];
                    if(!caseList.isEmpty() && !recordtypeList.isEmpty())
                    {
                        if(caseList.size() >= requests[0].minNumberOfCases)
                        {
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            if(orgWideEmailAddressId != null){email.setOrgWideEmailAddressId(orgWideEmailAddressId);}
                            email.setTemplateId(emailtemplateList[0].Id);
                            email.setTargetObjectId(requests[0].recipientContactId);
                            email.setWhatId(requests[0].caseId);
                            email.setSaveAsActivity(false);
                            emailList.add(email);
                        }
                    }
                }
                
                if(!emailList.isEmpty())
                {
                    Messaging.sendEmail(emailList);
                }
            }
            catch(Exception ex)
            {
                System.debug('#### Email Error: ' + ex);
                    
                List<String> adminemailaddressList = new List<String>();
                adminemailaddressList.add('sulung.chandra@veltig.com');
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                if(orgWideEmailAddressId != null){email.setOrgWideEmailAddressId(orgWideEmailAddressId);}
                email.setToAddresses(adminemailaddressList);
                email.setSubject('Unable to send New Customer Support Alert');
                email.setPlainTextBody(ex.getMessage());
                email.setSaveAsActivity(false);
                
                List<Messaging.SingleEmailMessage> erroremailList = new List<Messaging.SingleEmailMessage>();
                erroremailList.add(email);
                
                try
                {
                    Messaging.sendEmail(erroremailList);
                }
                catch(Exception ex2){}  
            }
            
        }
        
    }
    
}