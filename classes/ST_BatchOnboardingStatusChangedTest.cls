/****************************************
* @author   : Ravi Pureti
* @JIRA     : SE-2509
* @Parentcls: ST_BatchOnboardingStatusChanged
* *****************************************/
@istest
private class ST_BatchOnboardingStatusChangedTest {
    
    static testmethod void testAccStatusUpdate(){
        // inserting Account
        Account accObj1 = new Account(Name = 'TestAcc1',Residential__c = 10, Industry__c = 'Air Quality', Time_Zone__c = 'Alaska (UTC-09:00)', Pricing_Model__c = 'New Pricing (Managed Techs)', No_Known_Active_Partners__c = true, Tenant_Name__c = 'TestTenName1', Tenant_ID__c = 'TestTenName1', Initial_Username__c = 'TestTenName1', Software_newpicklist__c = 'Acowin', Estimated_Total_Potential_Office_Users__c = 10, Estimated_Total_Potential_Technicians__c = 20, Influencer__c = 'Yes', mAsset__c = 'Awards');
        accObj1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Account accObj2 = new Account(Name = 'TestAcc2',Residential__c = 10, Industry__c = 'Air Quality', Time_Zone__c = 'Alaska (UTC-09:00)', Pricing_Model__c = 'New Pricing (Managed Techs)', No_Known_Active_Partners__c = true, Tenant_Name__c = 'TestTenName2', Tenant_ID__c = 'TestTenName2', Initial_Username__c = 'TestTenName2', Software_newpicklist__c = 'Acowin', Estimated_Total_Potential_Office_Users__c = 10, Estimated_Total_Potential_Technicians__c = 30, Influencer__c = 'Yes', mAsset__c = 'Awards');
        accObj2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        insert new List<Account> {accObj1, accObj2};
            
        // inserting Contact
        Contact conObj1 = new Contact(FirstName='testFN1', LastName='testLN1', Email='test1@test.com', AccountId = accObj1.Id, Role__c = 'Consultant');
        Contact conObj2 = new Contact(FirstName='testFN2', LastName='testLN2', Email='test2@test.com', AccountId = accObj2.Id, Role__c = 'Consultant');
        insert conObj1;
        
        // inserting Product
        Product2 addOnProdObj1 = new Product2(Name = 'AddOnProd', Product_Type__c = 'Add-on', Core_Product__c = false);
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert new List<Product2> {coreProdObj1, addOnProdObj1};
            
        // inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        PricebookEntry pbOj2 = new PricebookEntry (Product2ID = addOnProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=100, isActive=true);
        insert new List<PricebookEntry> {pbOj1, pbOj2};
            
        // inserting Opportunity with Stage = 'Stage Zero'
        Opportunity oppObj1 = new Opportunity(Name = 'TestAcc1Opp1', AccountId = accObj1.Id, StageName = 'Stage Zero', CloseDate = System.today().addDays(2), Address_Industry_Verified__c = true, ImplementationDate__c = System.today().addDays(20), OBDiscoveryCallDate__c = System.today().addDays(20), AE_Discovery_POC__c = conObj1.Id, Onboarding_POC__c = conObj1.Id, AE_Confidence__c = 'High Probability', NextStep = 'Demo', Pricebook2Id = pbOj1.Pricebook2Id, isEstimatedPTech__c = true, Competing_Software__c = 'Acowin');
        oppObj1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        insert oppObj1;
        
        // inserting OpportunityLineItem for add-on product
        OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, pbOj1.Id, 30.00, 30);
        insert oppLIObj1;
        
        // inserting Onboarding
        Onboarding__c obObj = new Onboarding__c(Name = 'Onboarding -Premier Package-3/27/2019', Onboarding_Status__c = 'Pre-Implementation', OB_Account_Level__c = 'MidMarket', OB_Discovery_Call_Date__c = System.Today());
        obObj.Account__c = accObj1.Id;
        obObj.Opportunity__c = oppObj1.Id;
        obObj.RecordTypeId = Schema.SObjectType.Onboarding__c.RecordTypeInfosByName.get('ST Onboarding').getRecordTypeId();
        insert obObj; 
        
        Test.startTest();
        update obObj;
        ST_BatchOnboardingStatusChanged batch = new ST_BatchOnboardingStatusChanged();
        Database.executeBatch(batch, 100);
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Merge Job', sch, batch);
        Test.stopTest();
        // query Onboarding__c records
        List<Onboarding__c> onBDObj = [Select Id, Onboarding_Status__c from Onboarding__c];
        System.assertEquals(1, onBDObj.size());
        System.assertEquals('Implementation', onBDObj[0].Onboarding_Status__c);
    }
}