/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* This is an utility class to check user have By Pass access to Trigger
* This is invoked from Apex Trigger 
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @Version          1.0
* @Created          02-05-2019
* @Created By       Manoj Kumar S
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* Test Class: TriggerHelper_Test
*/

Public Class TriggerHelper{
    
    //Method to check whether Current user have By Pass trigger flag or not
    Public Static boolean isBypassTrigger(){
       return [Select Bypass_Trigger__c from User WHERE Id = :UserInfo.getUserId()][0].Bypass_Trigger__c;
    }
}