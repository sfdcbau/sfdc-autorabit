/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is an controller extension class used for Visualforce page 'ST_AccSalesRequestController' with Account standard controller
* This is called from Sales Request button on Account record detail page
* This class creates a Reference Request record when user enter all required details
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          12-02-2018
* @Jira User Story  SC-12
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘\
* Test Class: ST_AccSalesRequestContraoller_Test
*/

public class ST_AccSalesRequestController {
    
    public String recAccountId{get;set;}
    public Account recAccount{get;set;}
    public Reference_Request__c recRefRequest{get;set;}
    public Boolean IsSaveSuccessfull {get;set;}
    public String successMessage{get;set;}
    public List<Reference_Request__c> lstRequest = new List<Reference_Request__c>();
    public List<Contact> listofContacts = new List<Contact>();
    public string contactName{get;set;}
    
    //newchange starts...
    public Boolean IsDuplicateAccount {get;set;}
    public Boolean IsProAccSaveSuccessfull {get;set;} 
    public Proposed_Account__c acc {get;set;}
    public Set<Id> setAccId = new Set<Id>();
    public List<Proposed_Account__c > lstAcctoShow {get;set;}
    public Boolean display{get;set;}
    public Boolean display2{get;set;}
    //public Boolean IsRefAcc{get;set;}
    public Integer rowToRemove {get;set;} 
    public Set<Id> setAccProId = new Set<Id>();
    public Set<Id> setreqProId = new Set<Id>();
    //newchange end.....
    
    // Constructer declaration....
    public ST_AccSalesRequestController(ApexPages.StandardController std ){  
        successMessage = '';
        recAccountId= ApexPages.currentPage().getparameters().get('id');
        recRefRequest= new Reference_Request__c();
        recAccount = [SELECT Id, Name FROM Account WHERE Id = :recAccountId]; 
        setreqProId.add(recAccount.Id);
        setAccProId.add(recAccount.Id);
        recRefRequest.RecordTypeID = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.SalesRequestRecordType).getRecordTypeId();
        acc = new Proposed_Account__c();
    }
    //getting contacts for this account...
    public List<SelectOption> getContacts(){
        List<SelectOption> options = new List<SelectOption>(); 
        listofContacts = [SELECT Id, Name FROM Contact WHERE AccountId = :recAccountId];
        If(listofContacts.size() != null && listofContacts.size()>0){
            for(Contact con : listofContacts){
                options.add(new SelectOption(con.Id, String.valueOf(con.Name))); 
            } 
        }
        return options;
    } 
    //Standard submit funcion....
    public PageReference submit() {
        Id requestRecordTypeId;
        if(Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.SalesRequestRecordType) != null){
            //getting recordtype for Reference Request custom object
            requestRecordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.SalesRequestRecordType).getRecordTypeId();  
            IsSaveSuccessfull = false;
        }
        // inserting Reference Request record......
        recRefRequest.RecordTypeId = requestRecordTypeId;
        recRefRequest.Reference_Status__c = 'New';
        recRefRequest.Account__c = recAccountId;
        if(!String.isBlank(contactName)) {
            recRefRequest.Contact__c = contactName;    
        }
        
        // inserting Request record......
        try{
            lstRequest.add(recRefRequest);
            if(lstRequest != null && lstRequest.size() > 0 ){ 
                insert lstRequest;
                IsSaveSuccessfull = true;
                successMessage = label.Request_Submit_Message;        
            }
            if(lstAcctoShow != null && lstAcctoShow.size() > 0){
                for(Proposed_Account__c eachProposedAcc : lstAcctoShow){
                    eachProposedAcc.Request__c =  lstRequest[0].Id; 
                }
                update lstAcctoShow;
                
            }
        }     
        catch(DMLException ex){
            IsSaveSuccessfull = false;
            lstRequest.clear(); 
            System.debug('The following exception has occurred: ' + ex.getMessage());
        }
        
        return null; 
    }
    
    public PageReference SaveMultipleAccounts(){
        try{ 
            if(acc != null){
                if(acc.Id == null && acc.Account__c != null){
                    if(setAccProId.size() > 0 && setAccProId.contains(acc.Account__c)){
                        
                            //IsRefAcc = true;
                            IsDuplicateAccount = true;
                            IsProAccSaveSuccessfull = false;
                        
                    }
                    else{
                        IsProAccSaveSuccessfull = false;
                        //IsDuplicateAccount = false;
                        //IsRefAcc = false;
                        insert acc;    
                    }   
                }
                else{
                    IsProAccSaveSuccessfull = true;
                    IsDuplicateAccount = false;
                    //IsRefAcc = false;
                }
            }
            if(acc.Id != null){
                setAccId.add(acc.Id);
                lstAcctoShow = savedRecord(setAccId);
                acc = new Proposed_Account__c();
            }
            
        }
        catch(Exception ex){    
        }
        return null;
    }
    
    //new change starts....
    public List<Proposed_Account__c> savedRecord(set<Id> setAccId){
        
        List<Proposed_Account__c> lstAccToShow = new List<Proposed_Account__c>();
        for(Proposed_Account__c eachAcc : [Select Id,Account__c,Selected_Account_Name__c from Proposed_Account__c where Id IN :setAccId]){
            lstAccToShow.add(eachAcc);
            setAccProId.add(eachAcc.Account__c);
        }
        
        display = true; 
        display2 = true;
        return lstAccToShow;
        
    }
    
    public void removeRowFromAccList(){
        if(lstAcctoShow != null && lstAcctoShow.size()> 0){
            Proposed_Account__c eachAcc = lstAcctoShow[rowToRemove -1];
            lstAcctoShow.remove(rowToRemove -1);
            setAccProId.remove(eachAcc.Account__c);
            try{
                delete eachAcc;
                IsProAccSaveSuccessfull = false;
                IsDuplicateAccount = false;
                acc = new Proposed_Account__c();
            }
            catch(Exception ex){ 
            }
        }
        if(lstAcctoShow.size() == 0 ){ 
            display = false; 
            
        }
        
    }
}