/*-----------------------------------------------------------------------------------------
Test class for ST_NominationInternalController class.  
           Creating all the required data for the test class   
@Method :- submit, save
@Jira User Story  SC-3

-------------------------------------------------------------------------------------------*/

@isTest
public class ST_NominationInternalController_Test {
 
   //Creating test data....
   @testSetup static void setup() { 
       
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Role__c = 'Owner';
        insert testContact;  
   } 
    
    //test method for request creation.........
    @istest
    static void testNominationInsert(){
        Id RecordTypeReqId = Schema.SObjectType.Reference_Request__c.getRecordTypeInfosByName().get(Label.NominationReferenceInternalRecordType).getRecordTypeId();
        Account[] eachAcc = [SELECT Id FROM Account LIMIT 1];
        Contact[] eachCon = [SELECT Id,Account.Customer_Status_picklist__c FROM Contact LIMIT 1];
        
        if(eachCon.size() > 0){
            System.assertEquals(eachCon[0].Account.Customer_Status_picklist__c, 'Onboarding');
        }
        
        Customer_Reference_Details__c objCRD = new Customer_Reference_Details__c();
           objCRD.Nomination_Comments__c = 'test';
           objCRD.Activity_Types__c = ' Prospect Calls';
           objCRD.Goals_Realized__c = 'Increased Efficiency / Streamlined Operations';
           objCRD.Goals_Comments__c = 'test';
           objCRD.High_Product_Usage__c = ' Mobile App';
           objCRD.Positive_Product_Experience__c = 'Mobile App';
           objCRD.Product_Comments__c = 'test';
           objCRD.Accounting_Software__c = 'Quickbooks Online';
           objCRD.Pricebook__c = 'Custom';
           objCRD.GPS_Integration__c = 'Native';
           objCRD.Third_Party_Integrations__c = 'AsktheSeal';
           objCRD.Competitor_Comments__c =  'test';
           insert objCRD;
        
        Reference_Request__c testRequest = new Reference_Request__c(); 
        testRequest.RecordTypeId = RecordTypeReqId;
        testRequest.Account__c = eachAcc[0].Id;
        testRequest.Contact__c = eachCon[0].Id;
        testRequest.Reference_Status__c = 'New';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(eachCon[0]);
        PageReference pageRef = Page.ST_NominationInternal;
        test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(eachCon[0].Id));
        ST_NominationInternalController objNominationInternalController = new ST_NominationInternalController(sc); 
        objNominationInternalController.recRefRequest = testRequest;
        objNominationInternalController.Submit();   //Invoking submit method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Reference_Status__c, RecordTypeId FROM Reference_Request__c];
        if(refRequests.size()>0){
            System.assertEquals(refRequests[0].RecordTypeId, RecordTypeReqId);
            System.assertEquals(refRequests[0].Account__c, eachAcc[0].Id);
            System.assertEquals(refRequests[0].Contact__c, eachCon[0].Id);
            System.assertEquals(refRequests[0].Reference_Status__c, 'New');
        }
         
        objNominationInternalController.isNomination = true;
        objNominationInternalController.isSuccessStory = true;
        objNominationInternalController.isAccountStatusValid = true;
        
         if(objNominationInternalController != null){
            System.assertEquals(objNominationInternalController.isNomination, true);
            System.assertEquals(objNominationInternalController.isSuccessStory, true);
            System.assertEquals(objNominationInternalController.isAccountStatusValid, true);
        }
           
        objNominationInternalController.recRefDetails = objCRD;
        objNominationInternalController.selectedValue = '1';
        objNominationInternalController.Submit();
        objNominationInternalController.save();
        objNominationInternalController.closePopup();
        objNominationInternalController.showPopup();
    }
    
    //test method for request creation with wrong status.........
    @istest
    static void testNominationInsertNegative(){
        Id RecordTypeReqId = Schema.SObjectType.Reference_Request__c.getRecordTypeInfosByName().get(Label.NominationReferenceInternalRecordType).getRecordTypeId();
        Account[] eachAcc = [SELECT Id FROM Account LIMIT 1];
        Contact[] eachCon = [SELECT Id,Account.Customer_Status_picklist__c FROM Contact LIMIT 1];
        
        if(eachCon.size() > 0){
            System.assertEquals(eachCon[0].Account.Customer_Status_picklist__c, 'Onboarding');
        }
        
        Customer_Reference_Details__c objCRD = new Customer_Reference_Details__c();
           objCRD.Nomination_Comments__c = 'test';
           objCRD.Activity_Types__c = ' Prospect Calls';
           objCRD.Goals_Realized__c = 'Increased Efficiency / Streamlined Operations';
           objCRD.Goals_Comments__c = 'test';
           objCRD.High_Product_Usage__c = ' Mobile App';
           objCRD.Positive_Product_Experience__c = 'Mobile App';
           objCRD.Product_Comments__c = 'test';
           objCRD.Accounting_Software__c = 'Quickbooks Online';
           objCRD.Pricebook__c = 'Custom';
           objCRD.GPS_Integration__c = 'Native';
           objCRD.Third_Party_Integrations__c = 'AsktheSeal';
           objCRD.Competitor_Comments__c =  'test';
           insert objCRD;
        
        Reference_Request__c testRequest = new Reference_Request__c(); 
        testRequest.RecordTypeId = RecordTypeReqId;
        testRequest.Account__c = eachAcc[0].Id;
        testRequest.Contact__c = eachCon[0].Id;
        testRequest.Reference_Status__c = 'Approved';
        
        ApexPages.StandardController sc = new ApexPages.StandardController(eachCon[0]);
        PageReference pageRef = Page.ST_NominationInternal;
        test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(eachCon[0].Id));
        ST_NominationInternalController objNominationInternalController = new ST_NominationInternalController(sc); 
        objNominationInternalController.recRefRequest = testRequest;
        objNominationInternalController.Submit();   //Invoking submit method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Reference_Status__c,RecordTypeId FROM Reference_Request__c];
        if(refRequests.size()>0){
            System.assertEquals(refRequests[0].RecordTypeId, RecordTypeReqId);
            System.assertEquals(refRequests[0].Account__c, eachAcc[0].Id);
            System.assertEquals(refRequests[0].Contact__c, eachCon[0].Id);
            System.assertNotEquals(refRequests[0].Reference_Status__c, 'Approved');
        }
         
        objNominationInternalController.isNomination = true;
        objNominationInternalController.isSuccessStory = true;
        objNominationInternalController.isAccountStatusValid = true;
        
         if(objNominationInternalController != null){
            System.assertEquals(objNominationInternalController.isNomination, true);
            System.assertEquals(objNominationInternalController.isSuccessStory, true);
            System.assertEquals(objNominationInternalController.isAccountStatusValid, true);
        }
           
        objNominationInternalController.recRefDetails = objCRD;
        objNominationInternalController.selectedValue = '1';
        objNominationInternalController.Submit();
        objNominationInternalController.save();
        objNominationInternalController.closePopup();
        objNominationInternalController.showPopup();
    }
    //test method to check account customer status.....
    @istest
    static void testCustomerStatusNotValid(){
        Account testAccount1 = new Account();
        testAccount1.name = 'test account';
        testAccount1.Customer_Status_picklist__c = 'Stage Zero';
        insert testAccount1;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount1.Id;
        testContact.Role__c = 'Owner';
        insert testContact; 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testContact);
        PageReference pageRef = Page.ST_NominationInternal;
        test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(testContact.Id));
        ST_NominationInternalController objNominationInternalController = new ST_NominationInternalController(sc);
         
        objNominationInternalController.selectedValue = '2';
        objNominationInternalController.Submit();
        objNominationInternalController.save();
        
        System.assertEquals(objNominationInternalController.isAccountStatusValid, false);
    }
    //test method for success story creation....
    @istest
    static void testStoryInsert(){
        Account eachAcc = [SELECT Id FROM Account LIMIT 1];
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        
        Success_Story__c teststory = new Success_Story__c(); 
        teststory.Activity_Types__c = 'Peer Calls';
        teststory.Lessons_Learned__c = 'test Lessons';
        teststory.Status__c = 'Draft';
        List<Success_Story__c> lstofStory = new List<Success_Story__c>();
        lstofStory.add(teststory);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(eachCon);
        PageReference pageRef = Page.ST_NominationInternal;
        test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(eachCon.Id));
        ST_NominationInternalController objNominationInternalController = new ST_NominationInternalController(sc);
         
        objNominationInternalController.recCustomerSuccessStory = teststory;
        objNominationInternalController.selectedValue = '2';
        objNominationInternalController.Submit();
        objNominationInternalController.save();
        
         Success_Story__c[] successStories = [SELECT Id, Status__c, Activity_Types__c, Lessons_Learned__c FROM Success_Story__c];
        if(successStories.size()>0){ 
            System.assertEquals(successStories[0].Activity_Types__c, 'Peer Calls');
            System.assertEquals(successStories[0].Lessons_Learned__c, 'test Lessons');
            System.assertEquals(successStories[0].Status__c, 'Draft');
        } 
    }
    
    //test method for success story creation....
    @istest
    static void testStoryInsertNegative(){
        Account eachAcc = [SELECT Id FROM Account LIMIT 1];
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        
        Success_Story__c teststory = new Success_Story__c(); 
        teststory.Activity_Types__c = 'Peer Calls';
        teststory.Lessons_Learned__c = 'test Lessons';
        teststory.Status__c = 'New';
         
        
        ApexPages.StandardController sc = new ApexPages.StandardController(eachCon);
        PageReference pageRef = Page.ST_NominationInternal;
        test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(eachCon.Id));
        ST_NominationInternalController objNominationInternalController = new ST_NominationInternalController(sc);
         
        objNominationInternalController.recCustomerSuccessStory = teststory;
        objNominationInternalController.selectedValue = '2';
        objNominationInternalController.Submit();
        objNominationInternalController.save();
        
         Success_Story__c[] successStories = [SELECT Id, Status__c, Activity_Types__c, Lessons_Learned__c FROM Success_Story__c];
        if(successStories.size()>0){ 
            System.assertEquals(successStories[0].Activity_Types__c, 'Peer Calls'); 
            System.assertNotEquals(successStories[0].Status__c, 'New');
        } 
    }
}