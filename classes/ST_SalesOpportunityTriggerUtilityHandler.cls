/**********************************************************
    *@author             : Sanjeev
    *@Version            : V 1.0
    *@Description        : The purpose of this class is to remove all the dml from Process Builder and write it on before event
    *@Test class         : ST_SalesOpportunityTriggerUtilityTest
    *@JIRA               : SDM-135
    *@Version            : V 1.1
    *@ModifiedDate       : 03/26/2019
    *@ModifiedBy         : Tharun G
    *Jira                : SE-2911
    *Version             : V 1.2
    *Modified Date       : 04/14/2019
    *Jira                : SE-2960
    **********************************************************/
    public class ST_SalesOpportunityTriggerUtilityHandler {
    
    public static Id financeRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Integrated Financing').getRecordTypeId();
    public static Id paymentRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
    public static Id salesRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
    
    /**********************************************************
    *@author             : Sanjeev Kaurav
    *@Description        : This method is updating Primary_Contact__c,Software_new__c and Initial_Tech_Count__c based on some criteria
    Process Builder - Opportunity Status Changed, Update Account Customer Status merging
    //Process Builder - Opportunity Fill Primary Contact v2
    //Push Software from Account to Opp - on Opp creation
    *@JIRA               : SDM-142
    **********************************************************/
    Public static void updatePrimaryContact(List<Opportunity> opplist) {
        
        Set<Id> accountIds = new Set<Id>();
        for(Opportunity opp : opplist){
            if(opp.accountId != null && (opp.RecordTypeid != paymentRTId || opp.RecordTypeid != financeRTId  || opp.RecordTypeid == salesRTId)){
                accountIds.add(opp.accountId);  //Adding Account Id 
            }
        }
        
        Map<Id,Account> mapAccountIdAndAccount = new Map<Id,Account>();
        for(Account acc : [Select Id, Primary_Contact__c, Software_newpicklist__c, Estimated_Total_Potential_Technicians__c from Account 
                           where id IN : accountIds ]){   //Querying on related Account 
                               mapAccountIdAndAccount.put(acc.id,acc);  //Putting value of Account Id and Account
                           }
        
        for(Opportunity opp : opplist){
            if(opp.accountId != null && (opp.RecordTypeId != paymentRTId || opp.recordtypeid != financeRTId)){
                if(mapAccountIdAndAccount != null)
                    opp.Primary_Contact__c = mapAccountIdAndAccount.get(opp.accountId).Primary_Contact__c;  //Updating Primary Contact from Account's Primary Contact
            }
            
            if(opp.accountId != null && opp.RecordTypeId == salesRTId){
                if(mapAccountIdAndAccount != null)
                    opp.Software_new__c = mapAccountIdAndAccount.get(opp.accountId).Software_newpicklist__c;   //Updating Software from Account's Software
            }
            
            if(mapAccountIdAndAccount.get(opp.accountId).Estimated_Total_Potential_Technicians__c == null && opp.RecordTypeId == salesRTId){
                if(mapAccountIdAndAccount != null)
                    opp.Initial_Tech_Count__c = 'No Tech Count at Opp Creation';   //Updating Initial Tech Count
            }
            
            if(mapAccountIdAndAccount.get(opp.accountId).Estimated_Total_Potential_Technicians__c != null && opp.RecordTypeId == salesRTId){
                if(mapAccountIdAndAccount != null){
                    Decimal totalTech = mapAccountIdAndAccount.get(opp.accountId).Estimated_Total_Potential_Technicians__c;
                    opp.Initial_Tech_Count__c = String.valueOf(totalTech);   //Updating Initial Tech Count from Account's Estimated Total Potential Technicians
                }
            }
        }
        
    }
    
    
    /**********************************************************
    *@author             : Sanjeev Kaurav
    *@Description        : This method is updating Opportunity's fields based on StageName field
    Process Builder - Opportunity Status Changed, Update Account Customer Status merging
    //Backdate Opp Stage Dates when skipped v8
    *@JIRA               : SDM-142
    **********************************************************/
    
    // Same Label is used in both the Conditions -- OppStageName
    
    Public static void UpdateOpportunityRecords(List<Opportunity> opplist, Map<Id, Opportunity> oldMap) {
        
        for(Opportunity opp : opplist){
            //Whenever Opportunity's stagename is Closed Won and Opportunity's Recordtype is Sales then Updating the Discovery Call Stage Date,Negotiation Stage Date,Pricing Stage Date and QA Stage Date from Won Stage Date
            if(opp.stagename == label.OppStageName && opp.stagename != oldMap.get(opp.Id).Stagename && (opp.Negotiation_Stage_Date__c == null ) && (opp.Pricing_Stage_Date__c == null) && (opp.Q_A_Stage_Date__c == null ) &&            
               (opp.Discovery_Call_Stage_Date__c == null ) && opp.RecordTypeId == salesRTId){
                   opp.Discovery_Call_Stage_Date__c = system.today();
                   opp.Negotiation_Stage_Date__c = system.today();
                   opp.Pricing_Stage_Date__c = system.today();
                   opp.Q_A_Stage_Date__c = system.today();
                   
                   //Whenever Opportunity's stagename is Negotitation and Opportunity's Recordtype is Sales then Updating the Discovery Call Stage Date,Pricing Stage Date and QA Stage Date from Negotiation Stage Date
               }else if(opp.stagename == label.ST_OppStageNegotiation && opp.stagename != oldMap.get(opp.Id).Stagename && opp.Pricing_Stage_Date__c == null && opp.Q_A_Stage_Date__c == null &&             
                        opp.Discovery_Call_Stage_Date__c == null && opp.recordtypeid == salesRTId){
                            opp.Discovery_Call_Stage_Date__c = system.today();
                            opp.Pricing_Stage_Date__c = system.today();
                            opp.Q_A_Stage_Date__c = system.today();
                            
                            //Whenever Opportunity's stagename is Pricing and Opportunity's Recordtype is Sales then Updating the Discovery Call Stage Date and QA Stage Date from Pricing Stage Date
                        }else if(opp.stagename == label.ST_OppStagePricing && opp.stagename != oldMap.get(opp.Id).Stagename && opp.Q_A_Stage_Date__c == null &&              
                                 opp.Discovery_Call_Stage_Date__c == null && opp.RecordTypeId == salesRTId){
                                     opp.Discovery_Call_Stage_Date__c = system.today();
                                     opp.Q_A_Stage_Date__c = system.today();
                                     
                                     
                                     //Whenever Opportunity's stagename is QA and Opportunity's Recordtype is Sales then Updating the Discovery Call Stage Date from QA Stage Date
                                 }else if(opp.stagename == label.ST_OppStageQA && opp.stagename != oldMap.get(opp.Id).Stagename && opp.Discovery_Call_Stage_Date__c == null && opp.RecordTypeId == salesRTId){
                                     opp.Discovery_Call_Stage_Date__c = system.today();
                                     
                                     //Whenever Opportunity's stagename is Closed Lost, Opportunity's Sellable_vs_Unsellable is Sellable and Opportunity's Recordtype is Sales then Updating the Discovery Call Stage Date from Today's Date
                                 }else if(opp.stagename == label.ST_OppStageLost && opp.stagename != oldMap.get(opp.Id).Stagename && opp.Discovery_Call_Stage_Date__c == null && opp.Sellable_vs_Unsellable__c == 'Sellable' && 
                                          opp.RecordTypeId == salesRTId && opp.stagename != oldMap.get(opp.Id).Stagename){
                                              
                                              opp.Discovery_Call_Stage_Date__c = system.today();
                                              
                                          }
            
            else if(opp.stagename == label.ST_OppStageCC && opp.Negotiation_Stage_Date__c == null && opp.Pricing_Stage_Date__c == null && opp.Q_A_Stage_Date__c == null &&            
                    opp.Discovery_Call_Stage_Date__c == null && opp.RecordTypeId == salesRTId && opp.stagename != oldMap.get(opp.Id).Stagename){
                        
                        opp.Discovery_Call_Stage_Date__c = system.today();
                        opp.Negotiation_Stage_Date__c = system.today();
                        opp.Pricing_Stage_Date__c = system.today();
                        opp.Q_A_Stage_Date__c = system.today();
                    }
                    // update the CC Received Stage Date when Opp stage is CC Received 
                    else if(opp.stagename == label.ST_OppStageCC && opp.RecordTypeId == salesRTId && opp.stagename != oldMap.get(opp.Id).Stagename){
                        
                        opp.CC_Processed_Stage_Date__c = system.today();
                    }  
            
        }
        }
        
    
    /**********************************************************
    *@author             : Himanshu Sharma
    *@Description        : This method is updating AE_Confidence__c,OB_Discovery_Call_Date_at_Close__c,Delayed_Implementation__c based on StageName field
    Process Builder - Opportunity Status Changed, Update Account Customer Status merging
    *@JIRA               : SDM-141
    **********************************************************/
    public static void updateAEConfidence(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap){
        
        Date ImplementationDate = System.today();
        ImplementationDate = ImplementationDate.addDays(21);
        for(Opportunity eachOpp : triggerNew){
            if(String.valueOf(eachOpp.StageName) == Label.OppStageClosedWon && eachOpp.RecordTypeId == salesRTId &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                   
                   if(String.valueOf(eachOpp.AE_Confidence__c) == Label.OppAEConfidence || String.valueOf(eachOpp.AE_Confidence__c) == null || String.valueOf(eachOpp.AE_Confidence__c) == ''){
                       eachOpp.AE_Confidence__c = 'High Probability';
                   }
                   if(eachOpp.OB_Discovery_Call_Date_at_Close__c == false){
                       eachOpp.OB_Discovery_Call_Date_at_Close__c = true;
                   }
                   if(eachOpp.Delayed_Implementation__c == false && eachOpp.ImplementationDate__c != null && eachOpp.ImplementationDate__c > ImplementationDate){
                       eachOpp.Delayed_Implementation__c = true;
                   } 
               }
        }
    }
    
    
    /**********************************************************
    *@author             : Himanshu Sharma
    *@Description        : This method is updating Discovery_Call_Stage_Date__c,CloseDate,Negotiation_Stage_Date__c,Pending_Boarding_Date__c based on StageName field
    Process Builder - Previous stages and DQ Rationale Required
    *@JIRA               : SDM-141
    **********************************************************/
    public static void updateDiscoveryCallStageDate(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap){
        
        List<String> lstOppStage = Label.OppStagePending.split(',');
        for(Opportunity eachOpp : triggerNew){
            if(lstOppStage.contains(eachOpp.StageName) && (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                
                if(String.valueOf(eachOpp.StageName) == Label.OppStageDiscoveryCall && eachOpp.RecordTypeId == paymentRTId && eachOpp.Delayed_Implementation__c == false){
                    eachOpp.Discovery_Call_Stage_Date__c = System.today();
                }
                if(String.valueOf(eachOpp.StageName) == Label.OppStageClosedLost){
                    eachOpp.CloseDate = System.today();
                }
                if(String.valueOf(eachOpp.StageName) == Label.OppStageNegotiationPending && (eachOpp.RecordTypeId == paymentRTId || eachOpp.RecordTypeId == salesRTId)){
                    eachOpp.Negotiation_Stage_Date__c = System.today();
                }
                if(String.valueOf(eachOpp.StageName) == Label.OppStagePendingBoarding && eachOpp.RecordTypeId == paymentRTId){
                    eachOpp.Pending_Boarding_Date__c = System.today();
                }
            }
        }
    }
    
    
    /**********************************************************
    *@author             : Himanshu Sharma
    *@Description        : This method is updating Discovery_Call_Meeting_Result__c based on Discovery_Call_Date_Time__c field
    Process Builder - Push Marketing Tracking Fields When CLOSED WON
    *@JIRA               : SDM-141
    **********************************************************/
    public static void updateMeetingYettoRun(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap){
        
        for(Opportunity eachOpp : triggerNew){
            DateTime dt;
            Date dt2;
            if(eachOpp.Discovery_Call_Date_Time__c != null)
                dt = eachOpp.Discovery_Call_Date_Time__c;
            if(dt != null)
                dt2 = dt.date();
            List<String> lstOppStages = Label.OppStagesDiscoverycalldate.split(',');
            
            if( eachOpp.Discovery_Call_Date_Time__c == null && (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c == eachOpp.Discovery_Call_Date_Time__c)){
                eachOpp.Discovery_Call_Meeting_Result__c = 'Meeting Yet to Run';
            }
            if(eachOpp.Discovery_Call_Date_Time__c == null && (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c != eachOpp.Discovery_Call_Date_Time__c)){
                eachOpp.Discovery_Call_Meeting_Result__c = 'No Show';
            }
            
            
            if(eachOpp.RecordTypeId == salesRTId && String.valueOf(eachOpp.StageName) == Label.OppStageZero &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c != eachOpp.Discovery_Call_Date_Time__c) &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                   
                   if(eachOpp.Discovery_Call_Date_Time__c >= System.today()){
                       eachOpp.Discovery_Call_Meeting_Result__c = 'Meeting Yet to Run';
                   }
                   else if(eachOpp.Discovery_Call_Date_Time__c < System.today()){
                       eachOpp.Discovery_Call_Meeting_Result__c = 'Non-Compliant';
                   }
                   
               }
            
            if(eachOpp.RecordTypeId == salesRTId && String.valueOf(eachOpp.StageName) == Label.OppStageClosedLost && 
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c != eachOpp.Discovery_Call_Date_Time__c) &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                   
                   system.debug('dt2::' + dt2 );
                   system.debug('eachOpp.CloseDate::' + eachOpp.CloseDate );
                   if(dt2 != null && eachOpp.CloseDate < dt2 && (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).CloseDate != eachOpp.CloseDate)){
                       eachOpp.Discovery_Call_Meeting_Result__c = 'No Show';
                   }
                   if(String.valueOf(eachOpp.DispositionCode__c) == Label.OppDPCodeUnreachable && (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).DispositionCode__c != eachOpp.DispositionCode__c)){
                       eachOpp.Discovery_Call_Meeting_Result__c = 'No Show';
                   }
                   
               }
            
            if(eachOpp.RecordTypeId == salesRTId && (lstOppStages.contains(String.valueOf(eachOpp.StageName)) || String.valueOf(eachOpp.StageName) == Label.OppStageClosedLost) && 
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c != eachOpp.Discovery_Call_Date_Time__c) &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                   system.debug('eachOpp.DispositionCode__c::' + eachOpp.DispositionCode__c);
                   if(eachOpp.DispositionCode__c != Label.OppDPCodeUnreachable && (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).DispositionCode__c != eachOpp.DispositionCode__c)){
                       eachOpp.Discovery_Call_Meeting_Result__c = 'Meeting Yet to Run';
                   }
                   else if(lstOppStages.contains(String.valueOf(eachOpp.StageName))){
                       eachOpp.Discovery_Call_Meeting_Result__c = 'Meeting Ran';
                   }
               }
            
        }
    }
    
    
    /**********************************************************
    *@author             : Himanshu Sharma
    *@Description        : This method is updating Size_Segment__c based on Managed_Tech_Quantity__c field
    Process Builder - Last DQ Date and Reason Update_ClosedLost v3
    *@JIRA               : SDM-141
    **********************************************************/
    public static void updateManageTechQuantityCheck(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap){
        
        for(Opportunity eachOpp : triggerNew){
            if(eachOpp.RecordTypeId == salesRTId &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Managed_Tech_Quantity__c != eachOpp.Managed_Tech_Quantity__c)){
                   
                   if(eachOpp.Managed_Tech_Quantity__c >= 30){
                       eachOpp.Size_Segment__c = 'Enterprise';
                   }
                   if(eachOpp.Managed_Tech_Quantity__c < 30 && eachOpp.Account.Franchise_Strategic__c == false && eachOpp.Managed_Tech_Quantity__c != null && eachOpp.Managed_Tech_Quantity__c != 0){
                       eachOpp.Size_Segment__c = 'Corporate';
                   }
                   //system.debug('eachOpp.Managed_Tech_Quantity__c::' + eachOpp.Managed_Tech_Quantity__c);
                   if(eachOpp.Account.Franchise_Strategic__c == true && eachOpp.Managed_Tech_Quantity__c == null){
                       eachOpp.Size_Segment__c = 'Enterprise';
                   }
                   if(eachOpp.Account.Franchise_Strategic__c == false && eachOpp.Account.Estimated_Total_Potential_Technicians__c >= 30 && eachOpp.Managed_Tech_Quantity__c == null){
                       eachOpp.Size_Segment__c = 'Enterprise';
                   }
                   if(eachOpp.Account.Franchise_Strategic__c == false && eachOpp.Account.Estimated_Total_Potential_Technicians__c < 30 &&
                      eachOpp.Account.Estimated_Total_Potential_Technicians__c != 0 && eachOpp.Account.Estimated_Total_Potential_Technicians__c != null &&
                      eachOpp.Managed_Tech_Quantity__c == null){
                          eachOpp.Size_Segment__c = 'Corporate';
                      }
                   if(eachOpp.Account.Franchise_Strategic__c == false && eachOpp.Account.Estimated_Total_Potential_Technicians__c == null &&
                      eachOpp.Account.Estimated_Total_Potential_Technicians__c == 0  &&
                      (eachOpp.Managed_Tech_Quantity__c == 0 || eachOpp.Managed_Tech_Quantity__c == null)){
                          eachOpp.Size_Segment__c = null;
                      }
               }
        }
    }
    
    
    /**********************************************************
    *@author             : Himanshu Sharma
    *@Description        : This method is updating Initial_Discovery_Call_Date_Time__c field Discovery_Call_Date_Time__c is getting changed
    Process Builder : UpdateDispAndSubDispCodes v10
    *@JIRA               : SDM-141
    **********************************************************/
    public static void CountReschedulesforNewRecords(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap){
        
        for(Opportunity eachOpp : triggerNew){
            if(eachOpp.RecordTypeId == salesRTId && eachOpp.Discovery_Call_Date_Time__c  != null && triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c == null &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Date_Time__c != eachOpp.Discovery_Call_Date_Time__c)){
                   eachOpp.Count_of_Discovery_Call_Reschedules__c = 0;
                   eachOpp.Initial_Discovery_Call_Date_Time__c = eachOpp.Discovery_Call_Date_Time__c;
               }
        }
    }
    
    /**********************************************************
    *@author             : Himanshu Sharma
    *@Description        : This method is updating Discovery_Meeting_Ran__c field Discovery_Call_Meeting_Result_New__c is Meeting Ran
    Process Builder : UpdateDispAndSubDispCodes v10
    *@JIRA               : SDM-141
    **********************************************************/
    public static void UpdateDiscoveryMeetingRan(List<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap){
        for(Opportunity eachOpp : triggerNew){
            if(eachOpp.Discovery_Call_Meeting_Result_New__c == 'Meeting Ran' && eachOpp.RecordTypeId == salesRTId &&
               (triggerOldMap == null || triggerOldMap.get(eachOpp.Id).Discovery_Call_Meeting_Result_New__c != eachOpp.Discovery_Call_Meeting_Result_New__c)){
                   eachOpp.Discovery_Meeting_Ran__c = true;
               }
        }
    }
    
    
    /*
    ********************************************************
    *@author             : Tharun Gaddam
    *@Description        : This Method is to update Stage Dates when Users Skips the Stages and Move directly to CCProcess Stage on Sales Opp                           
    *@JIRA               : SE-2583
    *@TestClass          : ST_SalesOpportunityTriggerUtilityTest
    *@TestMethods        : AutoPopulateStageDatesforCCProcessedTest,AutoPopulateStageDatesforQAToCCProsTest,AutoPopulateStageDatesforPricingToCCProsTest
    *********************************************************
    */
    
    public static void AutoPopulateStageDatesforCCProcessed(List<Opportunity> OppList, Map<Id,Opportunity> oldMap){
        for(Opportunity opp : opplist){
            //Check if Opp Stage is Disocvery Call and User is moving to CC Processed Stage and all other dates are null
            //system.debug('===opp.stagename=>'+opp.stagename);
            //system.debug('==oldMap.get(opp.Id).Stagename==>'+oldMap.get(opp.Id).Stagename);
            //system.debug('==opp.Negotiation_Stage_Date__c==>'+opp.Negotiation_Stage_Date__c);
            if(opp.stagename == label.ST_OppStageCC && opp.stagename != oldMap.get(opp.Id).Stagename && 
               oldMap.get(opp.Id).Stagename == label.DiscoveryCallStage && (opp.Negotiation_Stage_Date__c == null) 
               && (opp.Pricing_Stage_Date__c == null) && (opp.Q_A_Stage_Date__c == null) &&            
               opp.RecordTypeId == salesRTId){
                   
                   opp.Negotiation_Stage_Date__c = system.today();
                   opp.Pricing_Stage_Date__c = system.today();
                   opp.Q_A_Stage_Date__c = system.today();
                  // system.debug('==opp.Negotiation_Stage_Date__cUpdate==>'+opp.Negotiation_Stage_Date__c);
               }
            // Check if Opp Stage is Q&A/Demo and user is moving to CC Processed Stage along with all the other date fields are null then Populte 
            //   the Back Stage Dates with today's date
            if(opp.stagename == label.ST_OppStageCC && opp.stagename != oldMap.get(opp.Id).Stagename  && 
               oldMap.get(opp.Id).Stagename == label.ST_OppStageQA && (opp.Negotiation_Stage_Date__c == null ) 
               && (opp.Pricing_Stage_Date__c == null) && opp.RecordTypeId == salesRTId){  
                   opp.Negotiation_Stage_Date__c = system.today();
                   opp.Pricing_Stage_Date__c = system.today();
                   
               }
            
            // Check if Opp Stage is Pricing/ROI and user is moving to CC Processed Stage along with all the back date fields are null then Populte 
            //   the Back Stage Dates with today's date
            if(opp.stagename == label.ST_OppStageCC && opp.stagename != oldMap.get(opp.Id).Stagename  && 
               oldMap.get(opp.Id).Stagename == label.ST_OppStagePricing && (opp.Negotiation_Stage_Date__c == null ) 
               &&  opp.RecordTypeId == salesRTId){  
                   opp.Negotiation_Stage_Date__c = system.today();
               }
        } 
        
    } 
    
    

/*
*********************************************************
*@author             : Tharun Gaddam
*@Description        : This Method is to Update the SE-Impact field when ever SE-Owner field is populated                           
*@JIRA               : SE-2911
*@TestClass          : ST_SalesOpportunityTriggerUtilityTest
*@TestMethods        : 
*********************************************************
*/
 
public static void updateSEImpactField( List<Opportunity> OppList, Map<Id,Opportunity> oldMap){

   
   // itterate through the new Opp List 
    for(Opportunity Opp: OppList){
    
    
        if(Opp.SEOwner__c != oldMap.get(Opp.Id).SEOwner__c && Opp.SEOwner__c !=null ){  // Check SE Owner has changed and not equals to null
        
         Opp.SE_Impact__c = 1; // Assign the value as 1 
        
        }
        
        if(Opp.SEOwner__c != oldMap.get(Opp.Id).SEOwner__c && Opp.SEOwner__c == null  
            && Opp.SE_Impact__c != NULL && Opp.SE_Impact__c != 0 && 
                oldMap.get(Opp.Id).SEOwner__c != NULL ){ // Check if the user is trying to remove the SE Owner, if so then decrese the SE Impact Count
        
        Opp.SE_Impact__c =  Opp.SE_Impact__c - 1;
    
    
        } 
    
        
        
        }
    
    } 
    
    
}