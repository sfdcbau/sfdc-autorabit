public without sharing class VCaseRatingTriggerDispatcher 
{
	
	public void dispatch() 
	{
		if(Trigger.isAfter && Trigger.isInsert)
			executeAfterInsert();
		
		if(Trigger.isAfter && Trigger.isUpdate)
			executeAfterUpdate();
    }
    
    private void executeAfterInsert()
    {
		updateCase();
    }
    
    private void executeAfterUpdate()
    {
		updateCase();
    }
	
	/*
		Update the case based on the rating selected
		
		Notes:
			- The Case Email Feedback is public and does not require login.
			- The public profile uses Guest User which does not have access to update Case.
	*/
	private void updateCase()
	{
		List<Case> caseToUpdateList = new List<Case>();
		Set<Id> caseSet = new Set<Id>();
		Map<Id, Case_Rating__c> caseCrMap = new Map<Id, Case_Rating__c>();
		
		for(Case_Rating__c cr : (List<Case_Rating__c>)Trigger.new)
		{
			caseCrMap.put(cr.Case__c, cr);
			
			if(!caseSet.contains(cr.Case__c))
			{
				caseSet.add(cr.Case__c);
			}
		}
		
		// Get cases
		for(Case c : [Select Id, Rate__c, Final_Resolution_Rate__c From Case Where Id In :caseSet])
		{
			if(caseCrMap.containsKey(c.Id))
			{
				Case_Rating__c cr = caseCrMap.get(c.Id);
				
				c.Rate__c = cr.Rate__c;
				c.Final_Resolution_Rate__c = cr.Final_Resolution_Rate__c;
				
				caseToUpdateList.add(c);
			}
		}
		
		if(!caseToUpdateList.isEmpty())
		{
			update caseToUpdateList;
		}
	}
	
}