/**
	Test
 */
@IsTest 
private class VCaseTriggerTest 
{

	/*
		Test Onboarding member
	*/
    @IsTest(SeeAllData=false) 
    public static void testAddOnboardingTeamMember() 
    {
    	List<User> userList = [Select Id From User Where IsActive=true Limit 2];
        
        Account acc = new Account();
    	acc.Name = 'Test Account 3123103811';
        acc.Customer_Status_picklist__c = 'Onboarding ';
        if(userList.size()>1)
        {
            acc.Onboarder__c = userList[1].Id;
        }
    	insert acc;
    	
    	Contact con = new Contact();
    	con.AccountId = acc.Id;
    	con.Firstname = 'Test';
    	con.Lastname = 'Test 123123113123';
    	insert con;
    	
        test.startTest();
    	
    	// Insert case
    	Case c1 = new Case();
    	c1.AccountId = acc.Id;
    	c1.ContactId = con.Id;
    	c1.Status = 'New';
    	c1.Origin = 'Web';
        c1.Description = 'Test';
        c1.Subject = 'Test';
    	insert c1;
    	
    	test.stopTest();
    }
    
	/*
		Test Onboarding member
	*/
    @IsTest(SeeAllData=false) 
    public static void testAddSuccessTeamMember() 
    {
    	List<User> userList = [Select Id From User Where IsActive=true Limit 2];
        
        Account acc = new Account();
    	acc.Name = 'Test Account 3123103811';
        acc.Customer_Status_picklist__c = 'Success';
        if(userList.size()>1)
        {
            acc.Success_Rep__c = userList[1].Id;
        }
    	insert acc;
    	
    	Contact con = new Contact();
    	con.AccountId = acc.Id;
    	con.Firstname = 'Test';
    	con.Lastname = 'Test 123123113123';
    	insert con;
    	
        test.startTest();
    	
    	// Insert case
    	Case c1 = new Case();
    	c1.AccountId = acc.Id;
    	c1.ContactId = con.Id;
    	c1.Status = 'New';
    	c1.Origin = 'Web';
        c1.Description = 'Test';
        c1.Subject = 'Test';
    	insert c1;
    	
    	test.stopTest();
    }
    
    @IsTest(SeeAllData=true) 
    public static void testProcessBatch()
    {
        List<User> userList = [Select Id From User Where IsActive=true Limit 2];
        
        Test.startTest();
        
        Account acc = new Account();
    	acc.Name = 'Test Account 3123103811';
        acc.Customer_Status_picklist__c = 'Success';
        if(userList.size()>1)
        {
            acc.Success_Rep__c = userList[1].Id;
        }
    	insert acc;
    	
    	Contact con = new Contact();
    	con.AccountId = acc.Id;
    	con.Firstname = 'Test';
    	con.Lastname = 'Test 123123113123';
    	insert con;
    	
    	// Insert case
    	Case c1 = new Case();
    	c1.AccountId = acc.Id;
    	c1.ContactId = con.Id;
    	c1.Status = 'New';
    	c1.Origin = 'Web';
        c1.Description = 'Test';
        c1.Subject = 'Test';
    	insert c1;
        
        // Insert Case Team Member Batch
        Case_Team_Member_Batch__c ctmb = new Case_Team_Member_Batch__c();
        ctmb.Case__c = c1.Id;
        insert ctmb;
        
        try
        {
            VCaseTeamMemberBatchProcess bc = new VCaseTeamMemberBatchProcess();
			Database.executeBatch(bc, 200);
        }
        catch(Exception de)
        {
            
        }
        
        Test.stopTest();
    }
    
}