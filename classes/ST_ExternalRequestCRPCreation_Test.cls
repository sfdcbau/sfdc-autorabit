/*
Test class for new CRP record creation after external request is approved. 
@class :- ST_ExternalRequestCRPCreation
        Creating all the required data for the test class   
@Method :-  checkandInsertCRP
@Jira User Story  SC-13
*/
//---------------------------------------------------------------------------------------------------

@isTest
public class ST_ExternalRequestCRPCreation_Test {
     @testSetup static void setup() {
    	Account testAccount = new Account();
    	testAccount.name = 'test account';
    	insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Role__c = 'Owner';
        insert testContact;
         
        Customer_Reference_Details__c testCRD = new Customer_Reference_Details__c();
           testCRD.Nomination_Comments__c = 'test';
           testCRD.Activity_Types__c = ' Prospect Calls';
           testCRD.Goals_Realized__c = 'Increased Efficiency / Streamlined Operations';
           testCRD.Goals_Comments__c = 'test';
           testCRD.High_Product_Usage__c = ' Mobile App';
           testCRD.Positive_Product_Experience__c = 'Mobile App';
           testCRD.Product_Comments__c = 'test';
           testCRD.Accounting_Software__c = 'Quickbooks Online';
           testCRD.Pricebook__c = 'Custom';
           testCRD.GPS_Integration__c = 'Native';
           testCRD.Third_Party_Integrations__c = 'AsktheSeal';
           testCRD.Competitor_Comments__c =  'test';
           insert testCRD;
        
        Reference_Request__c testRequest =new Reference_Request__c();
         testRequest.RecordTypeId = Schema.SObjectType.Reference_Request__c.getRecordTypeInfosByName().get(Label.NominationReferenceInternalRecordType).getRecordTypeId();
         testRequest.Account__c = testAccount.Id;
         testRequest.Contact__c = testContact.Id;
         testRequest.Reference_Status__c = 'New';
         testRequest.Customer_Reference_Detail_Id__c = testCRD.Id;
         insert testRequest;
      
             
    }
    
    //test method if CRP record exists for the nominator contact.......
    @isTest static void testCRPExists() {
        
        Account eachAcc = [SELECT Id FROM Account LIMIT 1];
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        Reference_Request__c eachRequest = [SELECT Id FROM Reference_Request__c LIMIT 1];
        
        Customer_Reference_Program__c  testcrp = new Customer_Reference_Program__c(); 
        testcrp.CRP_Status__c = 'Approved';
        testcrp.Account__c = eachAcc.Id;
        testcrp.Contact__c = eachCon.Id;
        insert testcrp;
        
        Reference_Request__c requesttoUpdate = [SELECT Id, Contact__c, Account__c,  Reference_Status__c, Customer_Reference_Detail_Id__c
                                                FROM Reference_Request__c WHERE Id =: eachRequest.Id ];
        requesttoUpdate.Reference_Status__c = 'Approved';
        
        List<Id> lstRequestId = new List<Id>();
        lstRequestId.add(requesttoUpdate.Id);
         ST_ExternalRequestCRPCreation.checkandInsertCRP(lstRequestId);
        Test.startTest();
        update requesttoUpdate ;
        Test.stopTest();
        
        Customer_Reference_Program__c crpCreated = [SELECT Id, Request_Id__c FROM Customer_Reference_Program__c LIMIT 1];
         System.assertEquals(crpCreated.Request_Id__c, requesttoUpdate.Id);
      
    }
     //testmethod if new CRP record created for request....
     @isTest static void testCRPCreated() {
        Reference_Request__c eachRequest = [SELECT Id FROM Reference_Request__c LIMIT 1]; 
        Reference_Request__c requesttoUpdate = [SELECT Id, Contact__c, Contact__r.AccountId,  Reference_Status__c, Customer_Reference_Detail_Id__c
                                                FROM Reference_Request__c WHERE Id =: eachRequest.Id ];
        requesttoUpdate.Reference_Status__c = 'Approved';
         List<Id> lstRequestId = new List<Id>();
        lstRequestId.add(requesttoUpdate.Id);
         ST_ExternalRequestCRPCreation.checkandInsertCRP(lstRequestId);
        Test.startTest();
        update requesttoUpdate ;
        Test.stopTest();
         
         Customer_Reference_Program__c crpCreated = [SELECT Id, Request_Id__c FROM Customer_Reference_Program__c LIMIT 1];
         System.assertNotEquals(crpCreated.Id, null);
    }

}