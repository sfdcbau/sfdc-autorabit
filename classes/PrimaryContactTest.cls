/**
	Test set Primary Contact on Lead Conversion and Contact set primary
 */
@isTest
private class PrimaryContactTest 
{

	/*
		Test set Primary Contact on lead conversion
		
		Notes:
			- Due to the Validation Rule, we need to use 'Sales Development Representative' Role to avoid enterring information into Referred_By__c field.
	*/
    @IsTest(SeeAllData=false) 
    public static void testLeadConversion() 
    {
        List<User> userList = [Select Id From User Where UserRole.Name='Sales Development Representative' And IsActive=true Limit 1];
        
        if(!userList.isEmpty())
        {
        	System.runAs(userList[0])
        	{
        		List<Lead> leadList = new List<Lead>();
				List<Database.LeadConvert> leadconvertList = new List<Database.LeadConvert>();
				List<LeadStatus> leadconvertedstatusList = [Select MasterLabel From LeadStatus Where IsConverted = true Limit 1];
				
				// Add leads
				Lead l1 = new Lead();
				l1.Company = 'Test Company 1';
				l1.Firstname = 'User';
				l1.Lastname = 'Test1';
				l1.Status = 'Open';
				l1.Estimated_Total_Potential_Office_Users__c = 1;
				l1.No_of_Technicians__c = 1;
				l1.City = 'Test City';
				l1.State = 'California';
				l1.Country = 'United States';
				l1.Title = 'Administrator';
				leadList.add(l1);
				
				insert leadList;
				
				// Convert leads
				Database.LeadConvert lc1 = new Database.LeadConvert();
				lc1.setLeadId(l1.Id);
				lc1.setConvertedStatus(leadconvertedstatusList[0].MasterLabel);
				leadconvertList.add(lc1);
				
				List<Database.LeadConvertResult>lcrList = Database.convertLead(leadconvertList);
				
				Test.startTest();
				
				System.assertEquals(lcrList.size(),1);
				
				// Insert a new Primary contact
				Contact newcon = new Contact();
				newcon.AccountId = lcrList[0].getAccountId();
				newcon.Firstname = 'User';
				newcon.Lastname = 'Test2';
				newcon.Title = 'Administrator';
				newcon.Primary_Contact__c = true;
				insert newcon;
				
				// Set the previously converted contact to Primary
				List<Contact> conList = [Select Id, Primary_Contact__c From Contact Where Id = :lcrList[0].getContactId()];
				if(!conList.isEmpty())
				{
					conList[0].Primary_Contact__c = true;
					//update conList; // [20160616 SC]: There is an issue with the deployment
					//System.DmlException: Update failed. First exception on row 0 with id 0031a00000RwoT9AAJ; first error: CANNOT_EXECUTE_FLOW_TRIGGER, The record couldn’t be saved because it failed to trigger a flow. A flow trigger failed to execute the flow with version ID 3011a000000U3Y9. Contact your administrator for help.: [] 
					//Stack Trace: Class.PrimaryContactTest.testLeadConversion: line 69, column 1
				}
				
				Test.stopTest();
        	}
        }
    }
    
}