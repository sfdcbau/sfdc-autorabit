/**
  Test
 */
@IsTest 
private class VUserTriggerTest 
{

  /*
    Test associate user
  */
    @IsTest(SeeAllData=false) 
    public static void testAssociateUser() 
    {
      UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User();
        portalAccountOwner1.UserRoleId = portalRole.Id;
        portalAccountOwner1.ProfileId = profile1.Id;
        portalAccountOwner1.Username = System.now().millisecond() + 'test2@test0184210834.com';
        portalAccountOwner1.Alias = 'test1214';
        portalAccountOwner1.Email='sulung.chandra@test740742071.com';
        portalAccountOwner1.EmailEncodingKey='UTF-8';
        portalAccountOwner1.Firstname='Test';
        portalAccountOwner1.Lastname='1213';
        portalAccountOwner1.LanguageLocaleKey='en_US';
        portalAccountOwner1.LocaleSidKey='en_US';
        portalAccountOwner1.TimeZoneSidKey='America/Chicago';
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
            //Create account
            Account portalAccount1 = new Account();
            portalAccount1.Name = 'TestAccount';
            portalAccount1.OwnerId = portalAccountOwner1.Id;
            insert portalAccount1;
            
            //Create contact
            Contact contact1 = new Contact();
            contact1.FirstName = 'Test';
            contact1.Lastname = 'SC';
            contact1.AccountId = portalAccount1.Id;
            contact1.Email = System.now().millisecond() + 'test@test1214.com';
            insert contact1;
            
            //Create user
            List<Profile> portalProfile = [SELECT Id FROM Profile Where UserType In ('PowerPartner','CustomerSuccess','PowerCustomerSuccess') Limit 1];
            if(!portalProfile.isEmpty())
            {
                User user1 = new User();
                user1.Username = System.now().millisecond() + 'test12345@test1214.com';
                user1.ContactId = contact1.Id;
                user1.ProfileId = portalProfile[0].Id;
                user1.Alias = 'test123';
                user1.Email = 'test12345@test.com';
                user1.EmailEncodingKey = 'UTF-8';
                user1.LastName = 'SC';
                user1.CommunityNickname = 'test12345';
                user1.TimeZoneSidKey = 'America/Los_Angeles';
                user1.LocaleSidKey = 'en_US';
                user1.LanguageLocaleKey = 'en_US';
                insert user1;
                
                List<User> userList = [Select Id, ContactId From User Where ContactId <> null Limit 1];
          
                if(!userList.isEmpty())
                {
                    test.startTest();
                
                    update userList;
                    
                    test.stopTest();
                }
            }
        }
    }
    
}