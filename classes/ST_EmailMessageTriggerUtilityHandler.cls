/*
 #####################################################
 # Created By...................: Aman Gupta
 # Created Date.................: 29 Mar, 2019
 # Last Modified By.............: Aman Gupta
 # Last Modified Date...........: 29 Mar, 2019
 # Description..................: Contain methods used by event handler class.
 # Test Class...................: ST_EmailMessageTriggerTest
 #####################################################
*/
public class ST_EmailMessageTriggerUtilityHandler {
    /**
     * @author : Aman Gupta
     * @date : 29 Mar, 2019
     * @description : SE-2450 --> Route specific emails into a new "Unassigned Premium Cases" case view.
    */
    public static void updatePremiumCases(List<EmailMessage> emailMsgObjList) {
        List<String> premEmailsList = new List<String> ();
        // Custom Label Premium_Case_Emails containing all Premium Case Emails.
        if(String.isNotBlank(System.Label.Premium_Case_Emails))
            premEmailsList = System.Label.Premium_Case_Emails.split(',');
        
        // if premEmailsList not empty
        if(!premEmailsList.isEmpty()) {
            Set<Id> premCaseIdSet = new Set<Id> ();
            // itearate emailMsgObjList
            for(EmailMessage emailMsgObj : emailMsgObjList) {
                if(emailMsgObj.ParentId.getSObjectType() == Schema.Case.getSObjectType()) {
                    String allEmails = emailMsgObj.ToAddress + ',' +emailMsgObj.CcAddress + ',' + emailMsgObj.BccAddress;
                    for(String premEmail : premEmailsList) {
                        if(allEmails.contains(premEmail.trim())) {
                            premCaseIdSet.add(emailMsgObj.ParentId);
                            break;
                        }
                    }
                }
            }
            
            
            // if premCaseIdSet not empty
            if(!premCaseIdSet.isEmpty()) {
                // query only Cases where RecordType.Name = 'Customer Support'
                List<Case> caseObjList = [SELECT Id FROM Case WHERE RecordType.Name = 'Customer Support' AND Id IN :premCaseIdSet];
                // iterate caseObjList and Premium_Case__c = true
                for(Case caseObj : caseObjList) {
                    caseObj.Premium_Case__c = true;
                   
                }
                update caseObjList;
            }
        }
    }
}