/**********************************************************
*@author             : Mukul Kumar
*@Description        : This test class is for ST_SalesOpportunityTriggerUtilityHandler
**********************************************************/



@isTest
private class ST_SalesOpportunityTriggerUtilityTest {
    
    @testsetup
    static void createData(){
        
        Contact con = New Contact(lastname = 'Test');
        Insert con;
        
        Account acc = utilityHelperTest.createAccount();
        acc.Primary_Contact__c = con.id; 
        Insert acc;
        
        Contact con2 = New Contact(lastname = 'Test',AccountId = acc.Id, Role__c = 'Accountant');
        Insert con2;
        
        // inserting Product
        Product2 addOnProdObj1 = new Product2(Name = 'AddOnProd', Product_Type__c = 'Add-on', Core_Product__c = false);
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert new List<Product2> {coreProdObj1, addOnProdObj1};
            
            // inserting PricebookEntry
            Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        PricebookEntry pbOj2 = new PricebookEntry (Product2ID = addOnProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=100, isActive=true);
        insert new List<PricebookEntry> {pbOj1, pbOj2};
            
            Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
          Opportunity opp = new Opportunity (Name='Opp1',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId, CloseDate=System.today() -2 ,AccountId = acc.id, 
                                           ImplementationDate__c= System.today()+2, Primary_Contact__c  = acc.Primary_Contact__c, Opportunity_Notes__c = 'Test Notes', 
                                           Software_new__c  = acc.Software_newpicklist__c, Initial_Tech_Count__c = 'No Tech Count at Opp Creation',
                                          Onboarding_POC__c  = con2.Id, AE_Discovery_POC__c = con2.Id, AE_Confidence__c = '  Low Probability');
        
        insert opp;
        
        Id opportunityPaymentRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        Opportunity oppPayment = new Opportunity (Name='Opp1',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityPaymentRecordTypeId, CloseDate=Date.today() - 2 ,AccountId = acc.id, 
                                                  ImplementationDate__c= System.today()+2, Primary_Contact__c  = acc.Primary_Contact__c, Software_new__c  = acc.Software_newpicklist__c, Initial_Tech_Count__c = 'No Tech Count at Opp Creation');
        insert oppPayment;
        
        Opportunity oppManagedTech = new Opportunity (Name='OppManagedtech',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId, CloseDate=System.today() -2 ,AccountId = acc.id, 
                                                      ImplementationDate__c= System.today()+2, Primary_Contact__c  = acc.Primary_Contact__c, Software_new__c  = acc.Software_newpicklist__c, Initial_Tech_Count__c = 'No Tech Count at Opp Creation');
        insert oppManagedTech;
        
        // inserting OpportunityLineItem for add-on product
        OpportunityLineItem oppLIObj1 = new OpportunityLineItem(OpportunityId = opp.Id, Product2Id = pbOj1.Id, UnitPrice = 50.00, Quantity = 30);
        OpportunityLineItem oppLIObj3 = new OpportunityLineItem(OpportunityId = opp.Id, Product2Id = pbOj2.Id, UnitPrice = 40.00, Quantity = 50);
        
        //insert new List<OpportunityLineItem> {oppLIObj1, oppLIObj2, oppLIObj3};
        insert oppLIObj1;
        insert oppLIObj3;
    }
    
    
   
    @isTest
    static void UpdateDiscoveryMeetingRanTest(){
    
    
        Contact con = New Contact(lastname = 'Test');
        Insert con;
        
        Account acc = utilityHelperTest.createAccount();
        acc.Primary_Contact__c = con.id; 
        acc.Tenant_ID__c = '12344564566788';
        acc.Tenant_Name__c = 'salesforcetetststs';
        Insert acc;
        
        Contact con2 = New Contact(lastname = 'Test',AccountId = acc.Id, Role__c = 'Accountant');
        Insert con2;
        
         //inserting Product;
        Product2 addOnProdObj1 = new Product2(Name = 'AddOnProd', Product_Type__c = 'Add-on', Core_Product__c = false);
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert new List<Product2> {coreProdObj1};
        
         //inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
         PricebookEntry pbOj2 = new PricebookEntry (Product2ID = addOnProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=100, isActive=true);
        insert new List<PricebookEntry> {pbOj1};
        
        //Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        //Opportunity opp = new Opportunity (Name='Opp1',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId, CloseDate=System.today() -2 ,AccountId = acc.id, 
        //ImplementationDate__c= System.today()+2, Primary_Contact__c  = acc.Primary_Contact__c, Software_new__c  = acc.Software_newpicklist__c, Initial_Tech_Count__c = 'No Tech Count at Opp Creation');
        //insert opp;
        
        //OpportunityLineItem oppLIObj1 = new OpportunityLineItem(OpportunityId = opp.Id, Product2Id = pbOj1.Id, UnitPrice = 50.00, Quantity = 30);
        test.starttest();
        Opportunity eachOpp = [Select Id, RecordType.DeveloperName,Discovery_Meeting_Ran__c, Stagename, Discovery_Call_Date_Time__c, AE_Opportunity_Notes__c, AE_Confidence__c   from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        system.assertEquals(eachOpp.RecordType.DeveloperName , 'Sales');
        eachOpp.StageName = 'stage zero';
        eachOpp.Discovery_Call_Date_Time__c = System.today() -1 ;
        eachOpp.AE_Opportunity_Notes__c = 'Test';
        eachOpp.AE_Confidence__c = 'Low Probability';
        update eachOpp;
        
        OpportunityLineItem OppL = [select id from OpportunityLineItem Limit 1];
        //OppL.OpportunityId = eachOpp.id;
        //update Oppl;
        
        eachOpp.stagename = 'Discovery call';
        update eachOpp; 
   
        system.assertEquals(eachOpp.StageName, 'Discovery call');
  
        test.stoptest();
    }
    
    @isTest
    static void updateAEConfidenceTest(){
        Test.startTest();
        Opportunity eachOpp = [Select id,stageName,DQ_Rationale__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        system.assertEquals(eachOpp.RecordType.DeveloperName , 'Sales');
        
        Contact eachCon = [Select id,AccountId from Contact where AccountId != null];
        system.assertNotEquals(eachCon.AccountId , null);
        
        Product2 p2 = [Select id,Core_Product__c from Product2 WHERE Core_Product__c = false LIMIT 1];
        system.assertEquals(p2.Core_Product__c , false);
        
        OpportunityContactRole objRole = new OpportunityContactRole();
        objRole.OpportunityId = eachOpp.Id;
        objRole.Role = 'Owner';
        objRole.ContactId = eachCon.Id;
        insert objRole;
        
        system.assertEquals(objRole.Role , 'Owner');
        
        eachOpp.DQ_Rationale__c = 'test';
        eachOpp.StageName ='Closed Won';
        eachOpp.Address_Industry_Verified__c = true;
        eachOpp.OBDiscoveryCallDate__c = system.today();
        eachOpp.ImplementationDate__c = system.today() + 22;
        eachOpp.Onboarding_POC__c = eachCon.Id;
        eachOpp.AE_Discovery_POC__c  = eachCon.Id;
        eachOpp.Competing_Software__c = 'Acowin';
        eachOpp.OB_Discovery_Call_Date_at_Close__c = false;
        eachOpp.AE_Confidence__c = 'Low Probability' ;
        eachOpp.Delayed_Implementation__c = false;
        try{
            update eachOpp;   
            system.assertEquals(eachOpp.StageName, 'Closed Won');
        }
        catch(Exception ex){
            system.debug('Exception is ' + ex.getMessage());
        }
        test.stopTest();
    }
    
    @isTest
    static void updateDiscoveryCallStageDate(){
        test.startTest();
        Opportunity eachOpp = [Select id,stageName,DQ_Rationale__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'ServiceTitan_Payments'];
        system.assertEquals(eachOpp.RecordType.DeveloperName , 'ServiceTitan_Payments');
        
        Contact eachCon = [Select id,AccountId from Contact where AccountId != null];
        system.assertNotEquals(eachCon.AccountId , null);
        
        Product2 p2 = [Select id,Core_Product__c from Product2 WHERE Core_Product__c = false LIMIT 1];
        system.assertEquals(p2.Core_Product__c , false);
        
        OpportunityContactRole objRole = new OpportunityContactRole();
        objRole.OpportunityId = eachOpp.Id;
        objRole.Role = 'Owner';
        objRole.ContactId = eachCon.Id;
        insert objRole;
        system.assertEquals(objRole.Role , 'Owner');
        
       
        eachOpp.DQ_Rationale__c = 'test';
        eachOpp.StageName ='Discovery Call';
        eachOpp.Address_Industry_Verified__c = true;
        eachOpp.OBDiscoveryCallDate__c = system.today();
        eachOpp.ImplementationDate__c = system.today() + 22;
        eachOpp.Onboarding_POC__c = eachCon.Id;
        eachOpp.AE_Discovery_POC__c  = eachCon.Id;
        eachOpp.Competing_Software__c = 'Acowin';
        eachOpp.OB_Discovery_Call_Date_at_Close__c = false;
        eachOpp.AE_Confidence__c = 'Low Probability' ;
        eachOpp.Delayed_Implementation__c = false;
        try{
            update eachOpp;    
            system.assertEquals(eachOpp.StageName, 'Discovery Call');
        }
        catch(Exception ex){
            system.debug('Exception is ' + ex.getMessage());
        }
        eachOpp.StageName ='Negotiation / Pending';
        eachOpp.Platform_Discovery_Call_Date__c = System.today();
        update eachOpp;
        system.assertEquals(eachOpp.StageName, 'Negotiation / Pending');
        system.assertEquals(eachOpp.Platform_Discovery_Call_Date__c, System.today());
        
        eachOpp.StageName ='Pending Boarding';
        eachOpp.Qualified_Rate__c = 2;
        eachOpp.Mid_Qualified_Rate__c = 2;
        eachOpp.Non_Qualified_Rate__c = 2;
        eachOpp.Per_Item__c = 2;
        eachOpp.Monthly_Service_Fee__c = 3;
        eachOpp.Pricing_Type__c = 'Interchange';
        eachOpp.Passthrough_Dues_and_Assessments__c = 'No';
        eachOpp.STP_CC_Monthly_Volume__c = 10;
        eachOpp.STP_CC_MRR__c = 10;
        eachOpp.Basis_Points_BPS__c = 10;
        eachOpp.Previous_Processor_BPS__c = 10;
        update eachOpp;
        system.assertEquals(eachOpp.StageName, 'Pending Boarding');
        
        eachOpp.StageName ='Closed Lost';
        eachOpp.Closed_Lost_Detail__c = 'Not required';
        eachOpp.Closed_Lost_Reason__c = 'Pricing';
        eachOpp.STP_CC_MRR__c = 10;
        update eachOpp;
        system.assertEquals(eachOpp.StageName, 'Closed Lost');
        
        test.stopTest();
    }
    
    @isTest
    static void updateMeetingYettoRun(){
        Test.startTest();
        Opportunity eachOpp = [Select Id,Account.Competing_Software__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        system.assertEquals(eachOpp.RecordType.DeveloperName , 'Sales');
        
        eachOpp.StageName ='Closed Lost';
        eachOpp.Closed_Lost_Detail__c = 'Not required';
        eachOpp.Closed_Lost_Reason__c = 'Pricing';
        eachOpp.STP_CC_MRR__c = 10;
        eachOpp.Discovery_Call_Date_Time__c = System.today();
        eachOpp.Competing_Software__c = 'Acowin';
        eachOpp.Account.Competing_Software__c = 'Acowin';
        eachOpp.CloseDate= System.today() - 3;
        eachOpp.DispositionCode__c = 'Unreachable';
        update eachOpp;
        system.assertEquals(eachOpp.StageName, 'Closed Lost');
        
        eachOpp.DispositionCode__c = 'Authority';
        update eachOpp;
        system.assertEquals(eachOpp.DispositionCode__c, 'Authority');
        
        eachOpp.StageName ='Stage Zero';
        eachOpp.DispositionCode__c = null;
        eachOpp.Discovery_Call_Date_Time__c = System.today() + 2;
        eachOpp.Closed_Lost_Reason__c = null;
        update eachOpp;
        system.assertEquals(eachOpp.DispositionCode__c, null);
        system.assertEquals(eachOpp.StageName, 'Stage Zero');
        system.assertEquals(eachOpp.Discovery_Call_Date_Time__c, System.today() + 2);
        system.assertEquals(eachOpp.Closed_Lost_Reason__c, null);
        
  
        
        eachOpp.Discovery_Call_Date_Time__c = System.today() - 2;
        eachOpp.StageName = 'Stage Zero';
        update eachOpp;
        system.assertEquals(eachOpp.Discovery_Call_Date_Time__c, System.today() - 2);
        system.assertEquals(eachOpp.StageName, 'Stage Zero');
        
        OpportunityLineItem oli = [Select Id,Quantity,Opportunity.Account.Franchise_Strategic__c,Opportunity.Account.Estimated_Total_Potential_Technicians__c from OpportunityLineItem where Quantity = 30];
        oli.Quantity = 24;
        update oli;
        system.assertEquals(oli.Quantity, 24);
        
        test.stopTest();
        
    }
    
    
    
    
    // This method id used to test for PositiveTest when Opp Stage is moved to CC Prossed from Discovery Stage
    @isTest
    Static void AutoPopulateStageDatesforCCProcessedTest(){
        
   
        // inserting Product 
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert coreProdObj1; 
        
        // inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        insert pbOj1;
         Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
         
        test.startTest();
        Opportunity eachOpp = [Select Id,Pricing_Stage_Date__c, Negotiation_Stage_Date__c,Account.Competing_Software__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        
        eachOpp.StageName = label.DiscoveryCallStage;
        //eachOpp.RecordTypeId = opportunityRecordTypeId;
        update eachOpp;
        system.debug('@@beforeEachopp'+eachOpp);
        
        
        //OpportunityLineItem oppLIObj1 = new OpportunityLineItem(OpportunityId = eachOpp.Id, Product2Id = pbOj1.Id, UnitPrice = 50.00, Quantity = 30);  
        eachOpp.StageName = label.ST_OppStageCC;
        update eachOpp;  
        
        eachOpp = [Select Id,Pricing_Stage_Date__c, Negotiation_Stage_Date__c,Q_A_Stage_Date__c,Account.Competing_Software__c from Opportunity where id = :eachOpp.id];
        System.assertEquals(eachOpp.Negotiation_Stage_Date__c, system.today());
        System.assertEquals(eachOpp.Pricing_Stage_Date__c, system.today());
        System.assertEquals(eachOpp.Q_A_Stage_Date__c, system.today());
        test.stopTest();
        
        
        
        
    }
    // This method id used to test for PositiveTest when Opp Stage is moved to CC Prossed from Q&A/Demo
    @isTest
    Static void AutoPopulateStageDatesforQAToCCProsTest(){
        
        // inserting Product 
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert coreProdObj1; 
        
        // inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        insert pbOj1;
         Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
         Test.startTest();
        Opportunity eachOpp = [Select Id,Account.Competing_Software__c,Negotiation_Stage_Date__c,Pricing_Stage_Date__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        eachOpp.StageName = label.ST_OppStageQA;  
        eachOpp.RecordTypeId = opportunityRecordTypeId;
        update eachOpp;
       
        //OpportunityLineItem oppLIObj1 = new OpportunityLineItem(OpportunityId = eachOpp.Id, Product2Id = pbOj1.Id, UnitPrice = 50.00, Quantity = 30);  
        eachOpp.StageName = label.ST_OppStageCC;
        update eachOpp;
        eachOpp = [Select Id,Pricing_Stage_Date__c, Negotiation_Stage_Date__c,Q_A_Stage_Date__c,Account.Competing_Software__c from Opportunity where id = :eachOpp.id];
        System.assertEquals(eachOpp.Negotiation_Stage_Date__c, system.today());
        System.assertEquals(eachOpp.Pricing_Stage_Date__c, system.today());
        System.assertEquals(eachOpp.Q_A_Stage_Date__c, system.today());
        
        Test.stopTest();
         
    }
    
   
    // This method id used to test for PositiveTest when Opp Stage is moved to CC Prossed from Pricing/ROI
    @isTest
    Static void AutoPopulateStageDatesforPricingToCCProsTest(){
        
        // inserting Product 
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert coreProdObj1; 
        
        // inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        insert pbOj1;
         Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Test.startTest();
        Opportunity eachOpp = [Select Id,Account.Competing_Software__c,Negotiation_Stage_Date__c,Pricing_Stage_Date__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        eachOpp.StageName = label.ST_OppStagePricing;
        eachOpp.RecordTypeId = opportunityRecordTypeId;
        update eachOpp;
        
        
        //OpportunityLineItem oppLIObj1 = new OpportunityLineItem(OpportunityId = eachOpp.Id, Product2Id = pbOj1.Id, UnitPrice = 50.00, Quantity = 30);  
        eachOpp.StageName = label.ST_OppStageCC;
        update eachOpp;
        eachOpp = [Select Id,Pricing_Stage_Date__c, Negotiation_Stage_Date__c,Q_A_Stage_Date__c,Account.Competing_Software__c from Opportunity where id = :eachOpp.id];
       
        System.assertEquals(eachOpp.Pricing_Stage_Date__c, system.today());
        
        
        Test.stopTest();
         
    }
     
    
    // This method id used to test for When SE Owner is added SE Impact should show as 1  functionality
    @isTest
    Static void updateSEImpactPositive(){
    
     test.starttest();
     List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
     List<Profile> pObj1= [SELECT Id FROM Profile WHERE Name = 'Standard Sales'];
     List<UserRole> roles = [select id from userrole where Name = 'Sales Manager'];
     
     User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
     insert userObj1;   
     
     User userObj3 = new User(ProfileId = pObj1[0].Id,UserRoleId=roles[0].id, LastName = 'TestUser1113', Email = 'testuser1132@test.com', Username = 'Test.User1132@test.com', CompanyName = 'TestCom3', Title = 'TestTitle3', Alias = 'TU1312', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
         
     insert userObj3;
     System.runAs(userObj1){
     Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
     
           
        Opportunity eachOpp = [Select Id,SEOwner__c, SE_Impact__c, Account.Competing_Software__c,Negotiation_Stage_Date__c,Pricing_Stage_Date__c,RecordType.DeveloperName from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
        eachOpp.StageName = label.ST_OppStagePricing;
        eachOpp.RecordTypeId = opportunityRecordTypeId;
        eachOpp.SEOwner__c = userObj3.Id;
        update eachOpp;
        //system.debug('@@'+eachOpp);
        Opportunity eachOppL = [Select id, SE_Impact__c from Opportunity where RecordType.DeveloperName = 'Sales' LIMIT 1];
     System.assertEquals(1, eachOppL.SE_Impact__c);
     test.stoptest();
    
    }
    }
     
}