/*
#####################################################
# Created By....................: Aman Gupta
# Created Date................: 18 Jan, 2019
# Last Modified By..........: Aman Gupta
# Last Modified Date......: 18 Jan, 2019
# Description...................: This is trigger handler class for ST_OpportunityTrigger trigger.
# Test Class...................: ST_OpportunityTriggerHandlerTest
#####################################################
*/
public class ST_SalesAddOnProductHandler {
    
    private Boolean isTriggerContext = false;
    private Integer batchSize = 0;
    private static Boolean isAssetCreated = false;
    public static Boolean isAccSTPayCusStatusUpdated = false;
    
    /**
    * @author : Aman Gupta
    * @date : 18 Jan, 2019
    * @description : Parameterized Constructor
    */
    public ST_SalesAddOnProductHandler(Boolean isExecuting, Integer size) {
        isTriggerContext = isExecuting;
        batchSize = size;
    }
    
    //*************************** Trigger Event Handler Start ********************************//
    /**
    * @author : Aman Gupta
    * @date : 18 Jan, 2019
    * @description : This method is to handle onAfterUpdate event of Opportunity trigger.
    */
    public static void onBeforeUpdate(List<Opportunity> triggerold, Map<Id,Opportunity> triggeroldMap, List<Opportunity> triggernew, Map<Id,Opportunity> triggernewMap) {
        oppPusher(triggernew, triggeroldMap);
    }
    
    /**
    * @author : Aman Gupta
    * @date : 12 Feb, 2019
    * @description : This method is to handle onAfterInsert event of Opportunity trigger.
    */
    public static void onAfterInsert(List<Opportunity> triggernew, Map<Id,Opportunity> triggernewMap) {
        //Update Account (field: ST_Payments_Customer_Status__c) for Payment Opportunities on the basis of Opportunity Stage whenever Opportunity record is created, updated.
        if(!isAccSTPayCusStatusUpdated) {
            updateAccSTPayCusStatus(triggernew);
            isAccSTPayCusStatusUpdated = true;
        }
    }
    
    /**
    * @author : Aman Gupta
    * @date : 18 Jan, 2019
    * @description : This method is to handle onAfterUpdate event of Opportunity trigger.
    */
    public static void onAfterUpdate(List<Opportunity> triggerold, Map<Id,Opportunity> triggeroldMap, List<Opportunity> triggernew, Map<Id,Opportunity> triggernewMap) {
        //create Asset record for Add-on Product when Opportunity RecordType in ('ServiceTitan_Payments', 'Sales', 'Integrated_Financing') and Stage = 'Closed Won'.
        if(!isAssetCreated) {
            // update record if old value != new value
            List<Opportunity> oppObjList = new List<Opportunity> ();
            for(Id oppId : triggernewMap.keySet()) {
                if(triggeroldMap.get(oppId).StageName != triggernewMap.get(oppId).StageName
                   && (triggernewMap.get(oppId).RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId()
                      || triggernewMap.get(oppId).RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId())
                   && triggernewMap.get(oppId).StageName == 'Closed Won')
                    oppObjList.add(triggernewMap.get(oppId));
            }
            if(!oppObjList.isEmpty()) {
                createAssetForAddOnProduct(triggernew, triggernewMap, new List<String> {'Sales', 'Upsell'});
                isAssetCreated = true;
            }
        }
        
        //Update Account (field: ST_Payments_Customer_Status__c) for Payment Opportunities on the basis of Opportunity Stage whenever Opportunity record is created, updated.
        if(!isAccSTPayCusStatusUpdated) {
            // update record if old value != new value
            List<Opportunity> oppObjList = new List<Opportunity> ();
            for(Id oppId : triggernewMap.keySet()) {
                if(triggeroldMap.get(oppId).StageName != triggernewMap.get(oppId).StageName
                   && triggernewMap.get(oppId).RecordTypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId())
                    oppObjList.add(triggernewMap.get(oppId));
            }
            if(!oppObjList.isEmpty()) {
                updateAccSTPayCusStatus(oppObjList);
                isAccSTPayCusStatusUpdated = true;
            }
        }
    }
    //*************************** Trigger Event Handler End ********************************//
    
    //*************************** Methods invoked by Event Handler methods Starts ********************************//
    /**
    * @author : Chris Lee
    * @date : 26 Apr, 2016
    * @description : 
    */  
    public static void oppPusher(List<Opportunity> newOppObjList, Map<Id,Opportunity> oldOppObjMap) {
        Date dNewCloseDate;
        Date dOldCloseDate;
        Boolean bPushed=false;
        
        for (Opportunity oIterator : newOppObjList) { //Bulk trigger handler so that you can mass update opportunities and this fires for all'
            // gets new values for updated rows
            dNewCloseDate = oIterator.CloseDate; // get the new closedate 
            dOldCloseDate = oldOppObjMap.get(oIterator.Id).CloseDate; //get the old closedate for this opportunity
            if (dOldCloseDate<dNewCloseDate) { //if the new date is after the old one, look if the month numbers are different
                if (dOldCloseDate.month()<dNewCloseDate.month()) { // the month number is higher, it's been pushed out
                    bPushed=true;
                }
                else {
                    if (dOldCloseDate.year()<dNewCloseDate.year()) { // the month wasn't higher, but the year was, pushed!
                        bPushed=true;
                    }
                }
                
            }
            if (bPushed==true) { // let's go make them sorry
                if (oIterator.PushCount__c==null) {
                    oIterator.PushCount__c=1;
                }
                else {
                    oIterator.PushCount__c++;           
                }
            }
        }
    }
    
    /**
    * @author : Aman Gupta
    * @date : 18 Jan, 2019
    * @description : [SE-1019] --> Create Asset record for Add-on Product when Opportunity RecordType in oppRecTypeNameList list and Stage = 'Closed Won'.
    */  
    public static void createAssetForAddOnProduct(List<Opportunity> oppObjList, Map<Id,Opportunity> oppObjMap, List<String> oppRecTypeNameList) {
        // check whether to run createAssetForAddOnProduct or not.
        List<Trigger_Settings__mdt> trgSetObjList = [SELECT 
                                                     Label, isActive__c 
                                                     FROM Trigger_Settings__mdt 
                                                     WHERE Label = 'ST_CreateAssetForAddOnProduct' LIMIT 1];
        if(!trgSetObjList.isEmpty() && trgSetObjList[0].isActive__c) {
            List<OpportunityLineItem> oppLIObjList = new List<OpportunityLineItem> ();
            List<Asset> assetObjList = new List<Asset> ();
            // populating assetObjList list with Asset records.
            for(Opportunity oppObj : [SELECT Id, AccountId, RecordType.DeveloperName, Managed_Tech_Quantity__c, Total_MRR__c,
                                      (SELECT Id, Name, Product2Id, Quantity, UnitPrice from OpportunityLineItems WHERE Product2.Core_Product__c = false AND Product2.Product_Type__c = 'Add-on' AND Product2.Asset_Required__c = True) 
                                      from Opportunity WHERE Id IN : oppObjList AND StageName = 'Closed Won' AND RecordType.DeveloperName IN : oppRecTypeNameList]) {
                for(OpportunityLineItem oppLIObj : oppObj.OpportunityLineItems) {
                    Asset assetObj = new Asset();
                    assetObj.Name = oppLIObj.Name;
                    assetObj.Product2Id = oppLIObj.Product2Id;
                    assetObj.AccountId = oppObj.AccountId;
                    assetObj.Opportunity__c = oppObj.Id;
                    assetObj.Managed_Tech_Count__c = oppObj.Managed_Tech_Quantity__c;
                    assetObj.Price = oppLIObj.UnitPrice;
                    assetObj.Quantity = oppLIObj.Quantity;
                    assetObj.MRR_Asset__c = oppObj.Total_MRR__c;
                    if(oppObj.RecordType.DeveloperName.equalsIgnoreCase('Sales')) {
                        assetObj.Status = System.label.SalesAssetStatus;
                        assetObj.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
                    }
                    if(oppObj.RecordType.DeveloperName.equalsIgnoreCase('Upsell')) {
                        assetObj.Status = 'Pending';
                        assetObj.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
                    }
                    oppLIObjList.add(oppLIObj);
                    assetObjList.add(assetObj);
                }
            }
            // inserting Asset records
            if(!assetObjList.isEmpty()) {
                insert assetObjList;
                
                // updating OpportunityLineItem Asset__c's field
                Integer i = 0;
                for(OpportunityLineItem oppLIObj : oppLIObjList) {
                    oppLIObj.Asset__c = assetObjList[i].Id;
                    i++;
                }
                update oppLIObjList;
            }
        }
    }
    
    /**
    * @author : Aman Gupta
    * @date : 11 Feb, 2019
    * @description : SE-1678 --> Update Account (field: ST_Payments_Customer_Status__c) for Payment Opportunities on the basis of Opportunity Stage whenever Opportunity record is created, updated.
    */
    public static void updateAccSTPayCusStatus(List<Opportunity> oppObjList) {
        Set<Id> accIdSet = new Set<Id> ();
        Set<Id> closedLostAccIdSet = new Set<Id> ();
        Map<Id, String> accIdStatusmap = new Map<Id, String> ();
        List<Account> accObjList = new List<Account> ();
        
        Set<String> oppStageSet = new Set<String> {'Prospect', 'Discovery Call', 'Negotiation / Pending', 'Pending Boarding'};
        // filter Payment Opportunities
        for(Opportunity oppObj : oppObjList) {
            if(oppStageSet.contains(oppObj.StageName)) {
                String payCusStatus = (oppObj.StageName.equalsIgnoreCase('Negotiation / Pending')) ? 'Negotiation Pending' : oppObj.StageName;
                accIdStatusmap.put(oppObj.AccountId, payCusStatus);
            }
            else if(oppObj.StageName.equalsIgnoreCase('Closed Lost'))
                closedLostAccIdSet.add(oppObj.AccountId);
        }
        
        // populating accObjList with records to update
        for(Id accObjId : accIdStatusmap.keySet()) {
            Account accObj = new Account(Id = accObjId);
            accObj.ST_Payments_Customer_Status__c = accIdStatusmap.get(accObjId);
            accObjList.add(accObj);
            if(closedLostAccIdSet.contains(accObjId))
                closedLostAccIdSet.remove(accObjId);
        }
        
        // checking closed lost Opp's if any!
        if(!closedLostAccIdSet.isEmpty()) {
            // fetch Accounts having Payment Opportunities WHERE Id IN :closedLostAccIdSet
            for(Account accObj : [SELECT Id, Name, (SELECT Id, StageName FROM Opportunities WHERE RecordType.DeveloperName = 'ServiceTitan_Payments') FROM Account WHERE Id IN :closedLostAccIdSet]) {
                Boolean isOppClosedLost = true;
                if(!accObj.Opportunities.isEmpty()) {
                    for(Opportunity oppObj : accObj.Opportunities) {
                        if(!oppObj.StageName.equalsIgnoreCase('Closed Lost')) {
                            isOppClosedLost = false;
                            break;
                        }
                    }
                    // update Account if all Opp's are closed lost
                    if(isOppClosedLost) {
                        accObj.ST_Payments_Customer_Status__c = 'Opted Out';
                        accObjList.add(accObj);
                    }
                }
            }    
        }
        
        // update accObjList if not empty
        if(!accObjList.isEmpty())
            update accObjList;
    }
    
    //*************************** Methods invoked by Event Handler methods Ends ********************************//
}