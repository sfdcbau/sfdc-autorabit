public class VPostChatController 
{
    public Case mycase {get;set;}
    public Case_Rating__c mycaserating {get;set;}
    public CaseComment mycasecomment {get;set;}
    public String attachedRecords {get;set;}
    public String errorMessage {get;set;}
    public Boolean hasError {get;set;}
    public Boolean isCompleted {get;set;}
    public Case_Rating__c caseRating {get;set;}
    
    public vPostChatController()
    {
        this.resetView();
        this.attachedRecords = ApexPages.currentPage().getParameters().get('attachedRecords');
        
        try
        {
            String caseId = VPostChatJson2Apex.parse(this.attachedRecords).Case_Z;
        
            if(caseId != null)
            {
                this.loadCase(caseId);
                
                if(this.mycase != null)
                {
                    mycaserating = new Case_Rating__c();
                    mycaserating.Case__c = this.mycase.Id;
                    mycaserating.Case_ID__c = this.mycase.Id;
                    
                    mycasecomment = new CaseComment();
                    mycasecomment.ParentId = mycase.Id;
                }
            }
        }
        catch(Exception ex)
        {}
    }
    
    public List<SelectOption> getRateOptions() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        
        options.add(new SelectOption('-3', 'Very Dissatisfied'));
        options.add(new SelectOption('-2', 'Dissatisfied'));
        options.add(new SelectOption('-1', 'Slightly Dissatisfied'));
        options.add(new SelectOption('0', 'Neutral'));
        options.add(new SelectOption('1', 'Somewhat Satisfied'));
        options.add(new SelectOption('2', 'Satisfied'));
        options.add(new SelectOption('3', 'Extremely Satisfied'));
        
        return options; 
    }
    
    /*
        Reset variables
    */
    private void resetView()
    {
        this.hasError = false;
        this.isCompleted = false;
        this.errorMessage = '';
    }
    
    public PageReference submit()
    {
        System.debug('##### Case: ' + mycase);
            
        this.resetView();
        
        if(this.mycaserating.Rate__c == null || this.mycaserating.Final_Resolution_Rate__c == null)
        {
            this.hasError = true;
            this.errorMessage += 'Please complete your feedback<br/>';
        }
        
        if(!this.hasError)
        {
            Savepoint sp = Database.setSavepoint();
            
            try
            {
                insert this.mycaserating;
                    
                if(mycasecomment.CommentBody != null)
                {
                    insert mycasecomment;
                }
                
                this.isCompleted = true;
            }
            catch(Exception ex)
            {
                Database.rollback(sp);
                
                this.errorMessage = ex.getMessage();
                this.hasError = true;
                
                System.debug('#### Email Error: ' + ex);
                    
                List<String> adminemailaddressList = new List<String>();
                adminemailaddressList.add('sulung.chandra@veltig.com');
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setToAddresses(adminemailaddressList);
                email.setSubject('Unable to save chat feedback');
                email.setPlainTextBody(ex.getMessage());
                email.setSaveAsActivity(false);
                
                List<Messaging.SingleEmailMessage> erroremailList = new List<Messaging.SingleEmailMessage>();
                erroremailList.add(email);
                
                try
                {
                    Messaging.sendEmail(erroremailList);
                }
                catch(Exception ex2){}  
            }
            
            System.debug('##### Error: ' + errorMessage);
        }
        
        return null;
    }
    
    private void loadCase(String caseId)
    {
        List<Case> caseList = [Select Id, CaseNumber, Rate__c, Final_Resolution_Rate__c From Case Where id = :caseId Limit 1];
        if(!caseList.isEmpty())
        {
            this.mycase = caseList[0];
        }
    }
}