/*
	Util class for Customer-Partner process
*/
public with sharing class VCustomerPartnerUtil 
{
	public static boolean canTriggerLeadConvertCustomerPartner = true;
	public static Set<Id> losingRecordCustomerPartnerSet = new Set<Id>();


	/*
		Future method to delete customer-partners.
		
		Params:
			- customerPartnerList: List of customer-partner Ids
	*/
	@future
	public static void deleteCustomerPartners(List<Id> customerPartnerList)
	{
		List<CustomerPartner__c> deleteCustomerPartnerList = [Select Id From CustomerPartner__c Where Id In :customerPartnerList];
		
		if(!deleteCustomerPartnerList.isEmpty())
		{
			delete deleteCustomerPartnerList;
		}
	} 
    
    /*
		Add Customer-Partner Contact records
		
		Params:
			- customerPartnerSet: Set of customer-partner Ids
	*/
    public static void addCustomerPartnerContacts(Set<Id> customerPartnerSet)
    {
        List<CustomerPartner__c> customerPartnerList = [Select Id, Customer__c, Partner__c From CustomerPartner__c Where Id In :customerPartnerSet And (Customer__c != null Or Partner__c != null)];
        List<Customer_Partner_Contact__c> insertCustomerPartnerContactList = new List<Customer_Partner_Contact__c>();
        List<Customer_Partner_Contact__c> deleteCustomerPartnerContactList = new List<Customer_Partner_Contact__c>();
        Set<Id> accountSet = new Set<Id>();
        Map<Id, Set<Id>> accountContactSetMap = new Map<Id, Set<Id>>(); // Map<AccountId, Set<ContactId>>
        
        for(CustomerPartner__c cp : customerPartnerList)
        {
            if(!accountSet.contains(cp.Customer__c))
                accountSet.add(cp.Customer__c);
            
            if(!accountSet.contains(cp.Partner__c))
                accountSet.add(cp.Partner__c);
        }
        
        if(!customerPartnerList.isEmpty())
        {
            // Collect contact accounts
            for(Contact con : [Select AccountId, Id From Contact Where AccountId In :accountSet])
            {
                if(!accountContactSetMap.containsKey(con.AccountId))
                    accountContactSetMap.put(con.AccountId, new Set<Id>());
                
                if(!accountContactSetMap.get(con.AccountId).contains(con.Id))
                    accountContactSetMap.get(con.AccountId).add(con.Id);
            }
            
            // Collect contacts to delete
            for(Customer_Partner_Contact__c cpc : [Select Id From Customer_Partner_Contact__c Where Customer_Partner__c In :customerPartnerSet])
            {
                deleteCustomerPartnerContactList.add(cpc);
            }
            
            // Collect contacts to insert
            for(CustomerPartner__c cp : customerPartnerList)
            {
                if(accountContactSetMap.containsKey(cp.Customer__c))
                {
                    for(Id contactId : accountContactSetMap.get(cp.Customer__c))
                    {
                        Customer_Partner_Contact__c newcpc = new Customer_Partner_Contact__c();
                        newcpc.Customer_Partner__c = cp.Id;
                        newcpc.Contact__c = contactId;
                        newcpc.Is_Partner_Contact__c = false;
                        insertCustomerPartnerContactList.add(newcpc);
                    }
                }
                
                if(accountContactSetMap.containsKey(cp.Partner__c))
                {
                    for(Id contactId : accountContactSetMap.get(cp.Partner__c))
                    {
                        Customer_Partner_Contact__c newcpc = new Customer_Partner_Contact__c();
                        newcpc.Customer_Partner__c = cp.Id;
                        newcpc.Contact__c = contactId;
                        newcpc.Is_Partner_Contact__c = true;
                        insertCustomerPartnerContactList.add(newcpc);
                    }
                }
            }
        }
        
        if(!deleteCustomerPartnerContactList.isEmpty())
            delete deleteCustomerPartnerContactList;
        
        if(!insertCustomerPartnerContactList.isEmpty())
            insert insertCustomerPartnerContactList;
    }
    
    /*
		Add Customer-Partner Opportunity records
		
		Params:
			- customerPartnerSet: Set of customer-partner Ids
	*/
    public static void addCustomerPartnerOpportunities(Set<Id> customerPartnerSet)
    {
        List<CustomerPartner__c> customerPartnerList = [Select Id, Customer__c, Partner__c From CustomerPartner__c Where Id In :customerPartnerSet And (Customer__c != null Or Partner__c != null)];
        List<Customer_Partner_Opportunity__c> insertCustomerPartnerOpportunityList = new List<Customer_Partner_Opportunity__c>();
        List<Customer_Partner_Opportunity__c> deleteCustomerPartnerOpportunityList = new List<Customer_Partner_Opportunity__c>();
        Set<Id> accountSet = new Set<Id>();
        Map<Id, Set<Id>> accountOpportunitySetMap = new Map<Id, Set<Id>>(); // Map<AccountId, Set<OpportunityId>>

        for(CustomerPartner__c cp : customerPartnerList)
        {
            if(!accountSet.contains(cp.Customer__c))
                accountSet.add(cp.Customer__c);
            
            if(!accountSet.contains(cp.Partner__c))
                accountSet.add(cp.Partner__c);
        }
        
        if(!customerPartnerList.isEmpty())
        {
            // Collect opportunity accounts
            for(Opportunity opp : [Select AccountId, Id From Opportunity Where AccountId In :accountSet])
            {
                if(!accountOpportunitySetMap.containsKey(opp.AccountId))
                    accountOpportunitySetMap.put(opp.AccountId, new Set<Id>());
                
                if(!accountOpportunitySetMap.get(opp.AccountId).contains(opp.Id))
                    accountOpportunitySetMap.get(opp.AccountId).add(opp.Id);
            }
            
            // Collect opportunities to delete
            for(Customer_Partner_Opportunity__c cpo : [Select Id From Customer_Partner_Opportunity__c Where Customer_Partner__c In :customerPartnerSet])
            {
                deleteCustomerPartnerOpportunityList.add(cpo);
            }
            
            // Collect opportunities to insert
            for(CustomerPartner__c cp : customerPartnerList)
            {
                if(accountOpportunitySetMap.containsKey(cp.Customer__c))
                {
                    for(Id opportunityId : accountOpportunitySetMap.get(cp.Customer__c))
                    {
                        Customer_Partner_Opportunity__c newcpo = new Customer_Partner_Opportunity__c();
                        newcpo.Customer_Partner__c = cp.Id;
                        newcpo.Opportunity__c = opportunityId;
                        newcpo.Is_Partner_Opportunity__c = false;
                        insertCustomerPartnerOpportunityList.add(newcpo);
                    }
                }
                
                if(accountOpportunitySetMap.containsKey(cp.Partner__c))
                {
                    for(Id opportunityId : accountOpportunitySetMap.get(cp.Partner__c))
                    {
                        Customer_Partner_Opportunity__c newcpo = new Customer_Partner_Opportunity__c();
                        newcpo.Customer_Partner__c = cp.Id;
                        newcpo.Opportunity__c = opportunityId;
                        newcpo.Is_Partner_Opportunity__c = true;
                        insertCustomerPartnerOpportunityList.add(newcpo);
                    }
                }
            }
        }
        
        if(!deleteCustomerPartnerOpportunityList.isEmpty())
            delete deleteCustomerPartnerOpportunityList;
        
        if(!insertCustomerPartnerOpportunityList.isEmpty())
            insert insertCustomerPartnerOpportunityList;
    }
}