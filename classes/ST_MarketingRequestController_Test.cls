/*--------------------------------------------------------------------------------------------------------------------------------
Test class for ST_MarketingSalesRequestController class. 
@Class :- ST_MarketingSalesRequestController
@Method :- submit
@Jira User Story  SC-8
@author  : Ravi
----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class ST_MarketingRequestController_Test {
    
    @istest
    //test method for request creation......... 
    static void testMarketingRequestInsert(){ 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.MarketingRequestRecordType).getRecordTypeId();
        ST_MarketingRequestController marketingController = new ST_MarketingRequestController();
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.Product_Areas__c = 'Fintech';
        refRequest.Request_Type__c = 'Success Story';
        refRequest.Request_SubType__c = 'Blog Post';
        refRequest.References_Desired__c = 3;
        refRequest.Matching_Comments__c = 'test comments';
        refRequest.Matching_Criteria__c = 'Vertical';
        refRequest.Request_Description__c = 'test description';  
        refRequest.Desired_Completion_Date__c = System.today()+2;  
        
        List<Proposed_Account__c> listofProAcc = new List<Proposed_Account__c>();
        Proposed_Account__c objProAcc = new Proposed_Account__c();
        objProAcc.Account__c = testAccount.Id;
        insert objProAcc;
        listofProAcc.add(objProAcc);
        marketingController.lstAcctoShow = listofProAcc;
        marketingController.recRefRequest = refRequest;
        marketingController.Submit();   //Invoking submit method.....
        marketingController.Cancel();   //Invoking cancel method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Request_Type__c, Request_Description__c, Matching_Criteria__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].Request_Type__c, 'Success Story');
            System.assertEquals(refRequests[0].Request_Description__c, 'test description');
            System.assertEquals(refRequests[0].Matching_Criteria__c, 'Vertical');
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId);
            Proposed_Account__c[] proAccounts = [SELECT Id, Name, Request__c FROM Proposed_Account__c WHERE Request__c =:refRequests[0].Id];
            System.assertNotEquals(proAccounts.size(), 0);
        }   
    }
    
    @istest
    //test method for request creation with invalid status without proposed account......... 
    static void testProductRequestInsertInvalidStatus(){ 
        
        ST_MarketingRequestController marketingController = new ST_MarketingRequestController();
        
        Reference_Request__c refRequest = new Reference_Request__c();
        refRequest.Reference_Status__c = 'Completed'; 
        refRequest.Product_Areas__c = 'Fintech';
        refRequest.Request_Type__c = 'Success Story';
        refRequest.Request_SubType__c = 'Blog Post';
        refRequest.References_Desired__c = 3;
        refRequest.Matching_Comments__c = 'test comments';
        refRequest.Matching_Criteria__c = 'Vertical';
        refRequest.Request_Description__c = 'test description';  
        refRequest.Desired_Completion_Date__c = System.today()+2;  
        
        marketingController.recRefRequest = refRequest;
        marketingController.Submit();   //Invoking submit method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Reference_Status__c, References_Desired__c FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].References_Desired__c, 3);
            System.assertNotEquals(refRequests[0].Reference_Status__c, 'Completed');
            Proposed_Account__c[] proAccounts = [SELECT Id, Name, Request__c FROM Proposed_Account__c WHERE Request__c =:refRequests[0].Id];
            System.assertEquals(proAccounts.size(), 0);
        }   
    } 
    
    //test method for proposed account.......
    @istest
    static void proposedAccountTest(){
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        System.assertEquals(testAccount.Customer_Status_picklist__c, 'Onboarding');
        
        test.startTest();
        Set<Id> setAccId = new Set<Id>();
        List<Proposed_Account__c> lstProAcc = new List<Proposed_Account__c>();
        
        Proposed_Account__c objProAcc = new Proposed_Account__c();
        objProAcc.Account__c = testAccount.Id;
        
        system.assertEquals(objProAcc.Account__c , testAccount.Id);
        system.assertEquals(objProAcc.Id , null);
        
        Proposed_Account__c objProAcc2 = new Proposed_Account__c();
        objProAcc2.Account__c = testAccount.Id;
        
        system.assertEquals(objProAcc.Account__c , testAccount.Id);
        system.assertEquals(objProAcc.Id , null);
        
        setAccId.add(objProAcc2.Account__c);
        
        system.assertEquals(setAccId.size(), 1);
        
        PageReference pageRef = Page.ST_MarketingRequest;
        Test.setCurrentPage(pageRef);
        
        ST_MarketingRequestController marketingController = new ST_MarketingRequestController();
        marketingController.acc = objProAcc;
        marketingController.SaveMultipleAccounts();
        
        //ST_MarketingRequestController marketingController = new ST_MarketingRequestController();
        marketingController.acc = objProAcc2;
        marketingController.SaveMultipleAccounts();
        
        Proposed_Account__c objProAcc1 = new Proposed_Account__c();
        objProAcc1.Account__c = testAccount.Id;
        insert objProAcc1;
        
        lstProAcc.add(objProAcc1);
        system.assertEquals(lstProAcc.size(), 1);
        
        setAccId.add(objProAcc1.Account__c);
        system.assertEquals(setAccId.size(), 1);
        
        marketingController.acc = objProAcc1;
        marketingController.setAccProId = setAccId;
        marketingController.lstAcctoShow = lstProAcc;
        marketingController.SaveMultipleAccounts();
        
        marketingController.rowToRemove = 1;
        system.assertEquals(marketingController.rowToRemove, 1);
        
        marketingController.removeRowFromAccList();
        
        lstProAcc.clear();
        system.assertEquals(lstProAcc.size(), 0);
        
        marketingController.lstAcctoShow = lstProAcc;
        marketingController.removeRowFromAccList();
        
        test.stopTest();
        
    }
}