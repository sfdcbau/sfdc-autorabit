/*
	Controller for the current user ajax responder
*/
public with sharing class VCurrentUserAjaxResponderController
{
	
	public String jsonString {get;set;}
    
    public vCurrentUserAjaxResponderController()
    {
		UserObj u = new UserObj(VUserUtil.getCurrentUser());
		
		this.jsonString = JSON.serialize(u);
	}
    
    /* 
    	Wrapper class
    */
    public class UserObj
    {
    	String id {get;set;}
    	String name {get;set;}
    	String usertype {get;set;}
        String profileid {get;set;}
        String profilename {get;set;}
        Boolean hascommunitycaseaccess {get;set;}
    	
    	public userObj(User u)
    	{
    		this.id =  u.id;
    		this.name =  u.name;
    		this.usertype = u.usertype;
    		this.profileid = u.ProfileId;
    		this.profilename = u.Profile.Name;
            
            this.hasCommunityCaseAccess = VCommunityCaseController.hasCaseAccess();
    	}
    }
    
}