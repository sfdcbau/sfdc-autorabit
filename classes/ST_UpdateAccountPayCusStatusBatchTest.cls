/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 04 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 04 Mar, 2019
 # Description...................: This is test class for ST_UpdateAccountPayCusStatusBatch apex batch.
 #####################################################
*/
@isTest
public class ST_UpdateAccountPayCusStatusBatchTest {
    
    /**
     * @author : Aman Gupta
     * @date : 04 Mar, 2019
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        // insert Product2
        Product2 prodObj1 = utilityHelperTest.createProduct();
        prodObj1.name = 'Managed Tech';
        prodObj1.Core_Product__c = true;
        
        insert new List<Product2> {prodObj1};
            
		// insert Pricebook2
        Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
        insert new List<Pricebook2> {custPBObj};
            
        // insert PricebookEntry
        PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
        insert new List<PricebookEntry> {pbeObj1};
            
        // insert Account
        Account accObj = utilityHelperTest.createAccount();
        insert new List<Account> {accObj};
            
        // insert Contact
        Contact conObj = utilityHelperTest.createContact(accObj.Id);
        conObj.Role__c = 'Co-Owner';
        insert new List<Contact> {conObj};
            
        // insert Opportunity
        Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj1.Name = accObj.Name + '- Payments - ' + System.today();
        oppObj1.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        oppObj1.StageName='Prospect';
        oppObj1.Pending_Boarding_Date__c= System.today().addDays(5);
        oppObj1.Qualified_Rate__c = 10;
        oppObj1.Mid_Qualified_Rate__c = 10;
        oppObj1.Non_Qualified_Rate__c  = 10;
        oppObj1.Per_Item__c = 10;
        oppObj1.Monthly_Service_Fee__c = 10;
        oppObj1.Pricing_Type__c = 'Interchange';
        oppObj1.Passthrough_Dues_and_Assessments__c = 'Yes';
        oppObj1.STP_CC_Monthly_Volume__c = 10;
        oppObj1.STP_CC_MRR__c = 10;
        oppObj1.Basis_Points_BPS__c= 10;
        oppObj1.Negotiation_Stage_Date__c  = System.today().addDays(2);
        oppObj1.Previous_Processor_BPS__c  = 10;
        oppObj1.Delayed_Implementation__c = true;
        oppObj1.Onboarding_POC__c=conObj.Id;
        
        
        Opportunity oppObj2 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj2.Name = accObj.Name + '- Payments - ' + System.today();
        oppObj2.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        oppObj2.StageName='Negotiation/Pending';
        oppObj2.Pending_Boarding_Date__c= System.today().addDays(10);
        oppObj2.Qualified_Rate__c = 20;
        oppObj2.Mid_Qualified_Rate__c = 20;
        oppObj2.Non_Qualified_Rate__c  = 20;
        oppObj2.Per_Item__c = 20;
        oppObj2.Monthly_Service_Fee__c = 20;
        oppObj2.Pricing_Type__c = 'Interchange';
        oppObj2.Passthrough_Dues_and_Assessments__c = 'Yes';
        oppObj2.STP_CC_Monthly_Volume__c = 20;
        oppObj2.STP_CC_MRR__c = 20;
        oppObj2.Basis_Points_BPS__c= 20;
        oppObj2.Negotiation_Stage_Date__c  = System.today().addDays(2);
        oppObj2.Previous_Processor_BPS__c  = 20;
        oppObj2.Delayed_Implementation__c = true;
        oppObj2.Onboarding_POC__c=conObj.Id;
        
        insert new List<Opportunity> {oppObj1, oppObj2};
            
        // inserting OpportunityLineItem for add-on product
        OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
        insert oppLIObj1;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 04 Mar, 2019
     * @description : This method is to test ST_UpdateAccountPayCusStatusBatch apex batch.
    */
    @isTest static void testUpdateAccPayCusStatusOpp() {
        List<Profile> pObj1= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		List<Profile> pObj2= [SELECT Id FROM Profile WHERE Name = 'Standard Platform'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj1[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj2[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1, userObj2};
		
        System.runAs (userObj1) {
            List<Product2> prodObjList = [SELECT Id, Name FROM Product2];
            List<Account> accObjList = [SELECT Id, Name FROM Account];
			List<Opportunity> oppObjList = [SELECT Id, Name FROM Opportunity ORDER By StageName DESC];
            
            
			test.startTest();
			// inserting Payment Onboarding__c
			Onboarding__c onBDObj1 = utilityHelperTest.createonboarding(accObjList[0].Id, oppObjList[0].Id);
			onBDObj1.Name = oppObjList[0].Name + Label.ST_Payment_Credit_Card;
			onBDObj1.Onboarding_Status__c = 'Pending Typeform Submission';
			onBDObj1.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
			
			Onboarding__c onBDObj2 = utilityHelperTest.createonboarding(accObjList[0].Id, oppObjList[0].Id);
			onBDObj2.Name = oppObjList[0].Name + Label.ST_Payment_Check;
			onBDObj2.Onboarding_Status__c = 'Pending Typeform Submission';
			onBDObj2.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
			
			List<Onboarding__c> onBDObjList = new List<Onboarding__c> {onBDObj1, onBDObj2};
			insert onBDObjList;
			
			// updating Onboarding_Status__c = 'Pending ST Live'
			for(Onboarding__c onBDObj: onBDObjList) {
				onBDObj.Onboarding_Status__c = 'Pending ST Live';
                onBDObj.Date_of_First_Transaction__c = System.today();
				onBDObj.Portal_Username__c = 'testUN';
				onBDObj.MerchantID__c = 'testId';
				onBDObj.Gateway_Created_By__c = userObj2.Id;
				onBDObj.Gateway_Creation_Date__c = System.today();
				onBDObj.Swipers_Shipped__c = 5;
				onBDObj.Date_Swipers_Shipped__c = System.today();
				//onBDObj.Date_of_First_Transaction__c = System.today();
			}
			update onBDObjList;
			
            // creating Asset records
            Asset assObj1 = new Asset(Name = 'TestAsset1' + Label.ST_Payment_Credit_Card, AccountId = accObjList[0].Id, Product2Id = prodObjList[0].Id);
            assObj1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj1.Status = 'Live';
            Asset assObj2 = new Asset(Name = 'TestAsset2' + Label.ST_Payment_Check, AccountId = accObjList[0].Id, Product2Id = prodObjList[0].Id);
            assObj2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj2.Status = 'Live';
            Asset assObj3 = new Asset(Name = 'TestAsset3' + Label.ST_Payment_Check, AccountId = accObjList[0].Id, Product2Id = prodObjList[0].Id);
            assObj3.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj3.Status = 'Inactive';
            
            List<Asset> assObjList = new List<Asset> {assObj1, assObj2, assObj3};
            insert assObjList;
                
            //assObjList[0].Status = 'Inactive';
            //assObjList[1].Status = 'Inactive';
            //update assObjList;
            
            accObjList[0].ST_Payments_Customer_Status__c = '';
            update accObjList[0];
                
            ST_UpdateAccountPayCusStatusBatch batch = new ST_UpdateAccountPayCusStatusBatch();
            Database.executeBatch(batch, 100);
            test.stopTest();
            batch = new ST_UpdateAccountPayCusStatusBatch('Select Id, Name from Account');
            
            /*
            // updating Opportunity StageName = 'Closed Lost'
			oppObj1.StageName='Closed Lost';
			oppObj1.STP_CC_MRR__c = 50;
			oppObj1.Closed_Lost_Detail__c = 'High Price';
			oppObj1.Closed_Lost_Reason__c = 'Pricing';
			oppObj2.StageName='Closed Lost';
			oppObj2.STP_CC_MRR__c = 50;
			oppObj2.Closed_Lost_Detail__c = 'High Price';
			oppObj2.Closed_Lost_Reason__c = 'Pricing';
			//update new List<Opportunity> {oppObj1, oppObj2};
            */
        }
    }
}