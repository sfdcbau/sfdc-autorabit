/****************************************
* @author   : Sanjeev Kaurav
* @JIRA     : SPR-11
* @Trigger  : ST_OnboardingTrigger
* @Class  : ST_PaymentsOnboardingHandler
* **************************************/

@isTest
public class ST_PaymentsOnboardingHandlerTest{
    
    static testmethod void  TestInsertopp(){
    
         User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard Platform'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'payhuser@h.com',
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
             //UserRoleId = r.Id
        );
        insert u;
        system.assertNotEquals(null, u.Id);
        
        Account acNew = new Account(Initial_Username__c = 'ska',Tenant_ID__c  = '23456789',Tenant_Name__c = 'Tenant',Estimated_Total_Potential_Technicians__c = 2,Estimated_Total_Potential_Office_Users__c = 1,Name = 'Test Account', BillingCity='Gwl', BillingState = 'Uttar pradesh', BillingCountry='india', Industry__c = 'Plumbing', No_Known_Active_Partners__c = true, Influencer__c = 'Unknown');
        insert acNew;
        
        Contact con =new Contact(Lastname ='Tets',accountid=acNew.id,Role__c = 'Owner');
        insert con;
       
        //get standard pricebook
        Id pricebookId = Test.getStandardPricebookId();

        Product2 prd1 = new Product2 (Name='Managed Tech',Description='Test Product Entry 1',productCode = 'ABC', isActive = true,core_product__c = true);
        insert prd1;
        
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
        insert pbe1;
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        //ServiceTitan Payments
        //Id oppSalesRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Opportunity opp1 = new Opportunity (Pending_Boarding_Date__c= system.today(),Qualified_Rate__c = 10,Mid_Qualified_Rate__c = 10,Non_Qualified_Rate__c  = 10,Per_Item__c = 10,Monthly_Service_Fee__c = 10,Pricing_Type__c = 'Interchange',Passthrough_Dues_and_Assessments__c = 'Yes',STP_CC_Monthly_Volume__c = 10,STP_CC_MRR__c = 10,Basis_Points_BPS__c= 10,Negotiation_Stage_Date__c  = system.today(),Previous_Processor_BPS__c  = 10,Delayed_Implementation__c = true,recordtypeid = OppRT,ImplementationDate__c = system.today(),Name='Opp1',StageName='Pending Boarding',Onboarding_POC__c=con.id, CloseDate=Date.today(),Pricebook2Id = pbe1.Pricebook2Id, AccountId = acNew.id);
        insert opp1;
        system.assertNotEquals(null, opp1.id);
         
        list<Opportunity> oppNewList = new list<Opportunity>();
        oppNewList.add(opp1);
         
        
         Onboarding__c obSales = new Onboarding__c(Gateway_Creation_Date__c= system.today(),Gateway_Created_By__c =u.id,ApprovedDate__c= system.today(),Portal_Username__c= 'pay',MerchantID__c='34578',Swipers_Shipped__c=10,Date_Swipers_Shipped__c = system.today());
         obSales.Account__c = acNew.id;
         obsales.OB_Discovery_Call_Date__c = system.today()+7;
         obsales.name= 'Check Payments';
         obSales.Opportunity__c = opp1.id ;
         obSales.Live_Date__c = system.today() - 90;
         obSales.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId(); 
         obSales.Onboarding_Status__c = 'Pending ST Live';
         insert obSales; 
         system.assertNotEquals(null, obSales.Id);
         //obSales.id = obSales.id;
         //update obSales;
         
         OpportunityLineItem lineItem1 = new OpportunityLineItem (Onboarding__c = obSales.id,OpportunityID=opp1.id,PriceBookEntryID=pbe1.id , quantity=25, totalprice=200 ,Product2id = prd1.id);
        insert lineItem1; 
        
         list<Onboarding__c> onbList = new list<Onboarding__c>();
         onbList.add(obSales);
         //ST_PaymentsOnboardingHandler.CreationAsset(onbList);
         system.assertNotEquals(null, onbList);
    }
    
    /**
     * @author : Aman Gupta
     * @date : 14 Feb, 2019
     * @description : This method is to test ST_PaymentsOnboardingHandler updateAccSTPayCusStatus function.
    */
    @isTest static void testUpdateAccSTPayCusStatus() {
        List<Profile> pObj1= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<Profile> pObj2= [SELECT Id FROM Profile WHERE Name = 'Standard Platform'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj1[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj2[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1, userObj2};
        
        System.runAs (userObj1) {
            // insert Product2
            Product2 prodObj1 = utilityHelperTest.createProduct();
            prodObj1.name = 'Managed Tech';
            prodObj1.Core_Product__c = true;
            
            insert new List<Product2> {prodObj1};
            
            // insert Pricebook2
            Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
            insert new List<Pricebook2> {custPBObj};
                
            // insert PricebookEntry
            PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
            insert new List<PricebookEntry> {pbeObj1};
            
            // insert Account
            Account accObj = utilityHelperTest.createAccount();
            insert new List<Account> {accObj};
                
            // insert Contact
            Contact conObj = utilityHelperTest.createContact(accObj.Id);
            conObj.Role__c = 'Co-Owner';
            insert new List<Contact> {conObj};
                
            // insert Opportunity
            Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
            oppObj1.Name = accObj.Name + '- Payments - ' + System.today();
            oppObj1.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
            oppObj1.StageName='Prospect';
            oppObj1.Pending_Boarding_Date__c= System.today().addDays(5);
            oppObj1.Qualified_Rate__c = 10;
            oppObj1.Mid_Qualified_Rate__c = 10;
            oppObj1.Non_Qualified_Rate__c  = 10;
            oppObj1.Per_Item__c = 10;
            oppObj1.Monthly_Service_Fee__c = 10;
            oppObj1.Pricing_Type__c = 'Interchange';
            oppObj1.Passthrough_Dues_and_Assessments__c = 'Yes';
            oppObj1.STP_CC_Monthly_Volume__c = 10;
            oppObj1.STP_CC_MRR__c = 10;
            oppObj1.Basis_Points_BPS__c= 10;
            oppObj1.Negotiation_Stage_Date__c  = System.today().addDays(2);
            oppObj1.Previous_Processor_BPS__c  = 10;
            oppObj1.Delayed_Implementation__c = true;
            oppObj1.Onboarding_POC__c=conObj.Id;
            
            
            Opportunity oppObj2 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
            oppObj2.Name = accObj.Name + '- Payments - ' + System.today();
            oppObj2.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
            oppObj2.StageName='Negotiation/Pending';
            oppObj2.Pending_Boarding_Date__c= System.today().addDays(10);
            oppObj2.Qualified_Rate__c = 20;
            oppObj2.Mid_Qualified_Rate__c = 20;
            oppObj2.Non_Qualified_Rate__c  = 20;
            oppObj2.Per_Item__c = 20;
            oppObj2.Monthly_Service_Fee__c = 20;
            oppObj2.Pricing_Type__c = 'Interchange';
            oppObj2.Passthrough_Dues_and_Assessments__c = 'Yes';
            oppObj2.STP_CC_Monthly_Volume__c = 20;
            oppObj2.STP_CC_MRR__c = 20;
            oppObj2.Basis_Points_BPS__c= 20;
            oppObj2.Negotiation_Stage_Date__c  = System.today().addDays(2);
            oppObj2.Previous_Processor_BPS__c  = 20;
            oppObj2.Delayed_Implementation__c = true;
            oppObj2.Onboarding_POC__c=conObj.Id;
            
            insert new List<Opportunity> {oppObj1, oppObj2};
            
            // inserting OpportunityLineItem for add-on product
            OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
            OpportunityLineItem oppLIObj2 = utilityHelperTest.createOppLineItem(oppObj2.Id, prodObj1.Id, 45.00, 26);
            insert oppLIObj1;
            insert oppLIObj2;
            
            test.startTest();
            // fetch Account and check ST_Payments_Customer_Status__c = 'Prospect' or 'Negotiation/Pending'
            List<Account> accObjList = [Select Id, Name, ST_Payments_Customer_Status__c , Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c from Account];
            
            System.assert(accObjList[0].ST_Payments_Customer_Status__c == 'Prospect' || accObjList[0].ST_Payments_Customer_Status__c == 'Negotiation/Pending');
            
            // inserting Payment Onboarding__c
            Onboarding__c onBDObj1 = utilityHelperTest.createonboarding(accObj.Id, oppObj1.Id);
            onBDObj1.Name = oppObj1.Name + Label.ST_Payment_Credit_Card;
            onBDObj1.Onboarding_Status__c = 'Pending Typeform Submission';
            onBDObj1.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            
            Onboarding__c onBDObj2 = utilityHelperTest.createonboarding(accObj.Id, oppObj1.Id);
            onBDObj2.Name = oppObj1.Name + Label.ST_Payment_Check;
            onBDObj2.Onboarding_Status__c = 'Pending Typeform Submission';
            onBDObj2.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            
            Onboarding__c onBDObj3 = utilityHelperTest.createonboarding(accObj.Id, oppObj2.Id);
            onBDObj3.Name = oppObj2.Name + Label.ST_Payment_Credit_Card;
            onBDObj3.Onboarding_Status__c = 'Pending Typeform Submission';
            onBDObj3.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            
            Onboarding__c onBDObj4 = utilityHelperTest.createonboarding(accObj.Id, oppObj2.Id);
            onBDObj4.Name = oppObj2.Name + Label.ST_Payment_Check;
            onBDObj4.Onboarding_Status__c = 'Pending Typeform Submission';
            onBDObj4.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            
            List<Onboarding__c> onBDObjList = new List<Onboarding__c> {onBDObj1, onBDObj2, onBDObj3, onBDObj4};
            insert onBDObjList;
            
            // updating Onboarding_Status__c = 'Pending ST Live'
            ST_PaymentsOnboardingHandler.isAccSTPayCusStatusUpdated = false;
            for(Onboarding__c onBDObj: onBDObjList) {
                onBDObj.Onboarding_Status__c = 'Pending ST Live';
                onBDObj.Portal_Username__c = 'testUN';
                onBDObj.MerchantID__c = 'testId';
                onBDObj.Gateway_Created_By__c = userObj2.Id;
                onBDObj.Gateway_Creation_Date__c = System.today();
                onBDObj.Swipers_Shipped__c = 5;
                onBDObj.Date_Swipers_Shipped__c = System.today();
                onBDObj.Date_of_First_Transaction__c = System.today();
            }
            update onBDObjList;
            
            // positive scenario: fetch Account and check ST_Payments_Customer_Status__c = 'Pending ST Live' and First Transaction dates not null
            accObjList = [Select Id, Name, ST_Payments_Customer_Status__c , Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c from Account];
            System.assertEquals('Pending ST Live', accObjList[0].ST_Payments_Customer_Status__c);
            System.assertNotEquals(null, accObjList[0].Credit_Card_Date_of_First_Transaction__c);
            System.assertNotEquals(null, accObjList[0].Check_Date_of_First_Transaction__c);
            
            test.stopTest();
            
            // creating Asset records
            Asset assObj1 = new Asset(Name = 'TestAsset1' + Label.ST_Payment_Credit_Card, AccountId = accObj.Id, Product2Id = prodObj1.Id);
            assObj1.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj1.Status = 'Live';
            Asset assObj2 = new Asset(Name = 'TestAsset2' + Label.ST_Payment_Check, AccountId = accObj.Id, Product2Id = prodObj1.Id);
            assObj2.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
            assObj2.Status = 'Live';
            
            List<Asset> assObjList = new List<Asset> {assObj1, assObj2};
            insert assObjList;
                
            ST_UpdateCustomerStatusFromAssets.updateAccSTPayCusStatus(assObjList);
            // scenario: 
            accObjList = [Select Id, Name, ST_Payments_Customer_Status__c , Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c from Account];
            System.assertEquals('Live (Credit Cards & Checks)', accObjList[0].ST_Payments_Customer_Status__c);
            
            assObjList[0].Status = 'Inactive';
            assObjList[1].Status = 'Inactive';
            update assObjList;
            
            ST_UpdateCustomerStatusFromAssets.updateAccSTPayCusStatus(assObjList);
            // scenario: 
            accObjList = [Select Id, Name, ST_Payments_Customer_Status__c , Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c from Account];
            System.assertEquals('Churned', accObjList[0].ST_Payments_Customer_Status__c);

            ST_SalesAddOnProductHandler.isAccSTPayCusStatusUpdated = false;            
            // updating Opportunity StageName = 'Closed Lost'
            oppObj1.StageName='Closed Lost';
            oppObj1.STP_CC_MRR__c = 50;
            oppObj1.Closed_Lost_Detail__c = 'High Price';
            oppObj1.Closed_Lost_Reason__c = 'Pricing';
            oppObj2.StageName='Closed Lost';
            oppObj2.STP_CC_MRR__c = 50;
            oppObj2.Closed_Lost_Detail__c = 'High Price';
            oppObj2.Closed_Lost_Reason__c = 'Pricing';
            update new List<Opportunity> {oppObj1, oppObj2};

            // fetch Account and check ST_Payments_Customer_Status__c = 'Churned'
            accObjList = [Select Id, Name, ST_Payments_Customer_Status__c , Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c from Account];
            System.assertEquals('Opted Out', accObjList[0].ST_Payments_Customer_Status__c);
        }
    }
    
    /**
     * @author : Aman Gupta
     * @date : 10 Apr, 2019
     * @description : This method is to test ST_PaymentsOnboardingHandler CreationAsset function.
    */
    @isTest static void testDupPayAssetCreation() {
        List<Profile> pObj1= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<Profile> pObj2= [SELECT Id FROM Profile WHERE Name = 'Standard Platform'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj1[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj2[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1, userObj2};
        
        System.runAs (userObj1) {
            // insert Product2
            Product2 prodObj1 = utilityHelperTest.createProduct();
            prodObj1.name = 'Managed Tech';
            prodObj1.Core_Product__c = true;
            
            insert new List<Product2> {prodObj1};
                
            // insert Pricebook2
            Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
            insert new List<Pricebook2> {custPBObj};
                
            // insert PricebookEntry
            PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
            insert new List<PricebookEntry> {pbeObj1};
                
            // insert Account
            Account accObj = utilityHelperTest.createAccount();
            insert new List<Account> {accObj};
                
            // insert Contact
            Contact conObj = utilityHelperTest.createContact(accObj.Id);
            conObj.Role__c = 'Co-Owner';
            insert new List<Contact> {conObj};
                
            // insert Opportunity
            Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
            oppObj1.Name = accObj.Name + '- Payments - ' + System.today();
            oppObj1.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
            oppObj1.StageName='Pending Boarding';
            oppObj1.Pending_Boarding_Date__c= System.today().addDays(5);
            oppObj1.Qualified_Rate__c = 10;
            oppObj1.Mid_Qualified_Rate__c = 10;
            oppObj1.Non_Qualified_Rate__c  = 10;
            oppObj1.Per_Item__c = 10;
            oppObj1.Monthly_Service_Fee__c = 10;
            oppObj1.Pricing_Type__c = 'Interchange';
            oppObj1.Passthrough_Dues_and_Assessments__c = 'Yes';
            oppObj1.STP_CC_Monthly_Volume__c = 10;
            oppObj1.STP_CC_MRR__c = 10;
            oppObj1.Basis_Points_BPS__c= 10;
            oppObj1.Negotiation_Stage_Date__c  = System.today().addDays(2);
            oppObj1.Previous_Processor_BPS__c  = 10;
            oppObj1.Delayed_Implementation__c = true;
            oppObj1.Onboarding_POC__c=conObj.Id;
            
            insert new List<Opportunity> {oppObj1};
                
			test.startTest();
            // inserting Payment Onboarding__c
            Onboarding__c onBDObj1 = utilityHelperTest.createonboarding(accObj.Id, oppObj1.Id);
            onBDObj1.Name = oppObj1.Name + Label.ST_Payment_Credit_Card;
            onBDObj1.Onboarding_Status__c = 'Pending Typeform Submission';
            onBDObj1.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            
            Onboarding__c onBDObj2 = utilityHelperTest.createonboarding(accObj.Id, oppObj1.Id);
            onBDObj2.Name = oppObj1.Name + Label.ST_Payment_Check;
            onBDObj2.Onboarding_Status__c = 'Pending Typeform Submission';
            onBDObj2.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();

            List<Onboarding__c> onBDObjList = new List<Onboarding__c> {onBDObj1, onBDObj2};
            insert onBDObjList;
            
            // inserting OpportunityLineItem for add-on product
            OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
            oppLIObj1.Onboarding__c = onBDObj1.Id;
            insert oppLIObj1;
            
            OpportunityLineItem oppLIObj2 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
            oppLIObj2.Onboarding__c = onBDObj2.Id;
            insert oppLIObj2;
            
            // updating Onboarding_Status__c = 'Pending ST Live'
            for(Onboarding__c onBDObj: onBDObjList) {
                onBDObj.Onboarding_Status__c = 'Pending ST Live';
                onBDObj.Portal_Username__c = 'testUN';
                onBDObj.MerchantID__c = 'testId';
                onBDObj.Gateway_Created_By__c = userObj2.Id;
                onBDObj.Gateway_Creation_Date__c = System.today();
                onBDObj.Swipers_Shipped__c = 5;
                onBDObj.Date_Swipers_Shipped__c = System.today();
                onBDObj.Date_of_First_Transaction__c = System.today();
            }
            update onBDObjList;
            
            // fetching Assets created for Onboarding's
            List<Asset> assetObjList = [Select Id, Status from Asset];
            System.assertEquals(2, assetObjList.size());            
            System.assertEquals('Onboarding', assetObjList[0].Status);
            System.assertEquals('Onboarding', assetObjList[1].Status);
        }
    }
}