/*
	Controller for Customer-Partner web services
*/
global with sharing class CustomerPartnerServiceController 
{
	/*
		Create lead customer partner
	*/
	webservice static String createLeadCustomerPartner(Id leadId)
	{
		List<RecordType> rtList = [Select Id, Name From RecordType Where SObjectType = 'CustomerPartner__c' And Name='Lead' Limit 1];
		if(!rtList.isEmpty())
		{
			return insertCustomerPartner(rtList[0].Id, leadId, Static_ID__c.getInstance('Account_Lead_Customer').RecordID__c);
		}
		
		return '';
	}
	
	/*
		Create account customer partner
	*/
	webservice static String createAccountCustomerPartner(Id accountId)
	{
		List<RecordType> rtList = [Select Id, Name From RecordType Where SObjectType = 'CustomerPartner__c' And Name='Customer' Limit 1];
		if(!rtList.isEmpty())
		{
			return insertCustomerPartner(rtList[0].Id, null, accountId);
		}
		
		return '';
	}
	
	/*
		Insert customer partner
	*/
	public static String insertCustomerPartner(Id recordtypeId, Id leadId, Id customerId)
	{
		String cpId = '';
		
		CustomerPartner__c newcp = new CustomerPartner__c();
		newcp.RecordTypeId = recordtypeId;
		newcp.Lead__c = leadId;
		newcp.Customer__c = customerId;
		
		Savepoint sp = Database.setSavepoint();
			
		try
		{
			insert newcp;
			cpId = newcp.Id;
		}
		catch(Exception ex)
		{
			Database.rollback(sp);
		}
		
		return cpId;
	}
}