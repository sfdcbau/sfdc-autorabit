/*************************************************
* 
* @Parent Class : ST_PaymentOpportunityHandler
* ***********************************************/
@isTest
public class ST_PaymentOpportunityHandlerTest {
    static testmethod void  TestInsertopp(){
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard Platform'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'payhuser@h.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert u;
        system.assertNotEquals(null, u.Id);
        
        Product2 product = utilityHelperTest.createProduct();
        product.name = 'Platform Payments-Check';
        PRODUCT.Core_Product__c = TRUE;
        insert product;
        
        PricebookEntry standPB = utilityHelperTest.craeteStandardPBE2(product.id);
        insert standPB;
        
        Pricebook2 custPB = utilityHelperTest.craeteCustomPB2();
        insert custPB; 
               
        PricebookEntry pbe = utilityHelperTest.createPricebookEntry(custPB, product);
        insert pbe;
        
        Account acc = utilityHelperTest.createAccount();
        insert acc;
        
        contact con = utilityHelperTest.createContact(acc.id);
        insert con;
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        
        //ServiceTitan Payments
        
        list<Opportunity> oppNewList = new list<Opportunity>();
        Opportunity opp1 = new Opportunity (Pending_Boarding_Date__c= system.today(),Qualified_Rate__c = 10,Mid_Qualified_Rate__c = 10,Non_Qualified_Rate__c  = 10,Per_Item__c = 10,Monthly_Service_Fee__c = 10,Pricing_Type__c = 'Interchange',Passthrough_Dues_and_Assessments__c = 'Yes',STP_CC_Monthly_Volume__c = 10,STP_CC_MRR__c = 10,Basis_Points_BPS__c= 10,Negotiation_Stage_Date__c  = system.today(),Previous_Processor_BPS__c  = 10,Delayed_Implementation__c = true, recordtypeid = OppRT, ImplementationDate__c = system.today(), Name='Opp1', StageName='Stage Zero', Onboarding_POC__c=con.id, CloseDate=Date.today(),Pricebook2Id = pbe.Pricebook2Id, AccountId = acc.id);
        insert opp1;
          
        Onboarding__c obSales = utilityHelperTest.createonboarding(acc.id,opp1.id);
        obSales.OB_Discovery_Call_Date__c = Opp1.OBDiscoveryCallDate__c;
        obSales.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId(); 
        obSales.Onboarding_Status__c = 'Pending ST Live';
        insert obsales;
        
        OpportunityLineItem lineItem1 = new OpportunityLineItem ( OpportunityID = opp1.id, PriceBookEntryID=pbe.id, quantity=25, totalprice=200 ,Product2id = product.id);
        lineItem1.Onboarding__c = obsales.id;
        insert lineItem1;   
        system.assertNotEquals(null, lineItem1.Id);
        test.startTest();
        opp1.StageName='Pending Boarding';
        update opp1;
        
        system.assertEquals('Pending ST Live', obSales.Onboarding_Status__c);
        system.assertEquals('Pending Boarding', opp1.StageName);
        system.assertEquals('Pending Boarding', opp1.StageName);
        
        opp1.StageName='Closed Won';
        update opp1;
        system.assertEquals('Closed Won', opp1.StageName);  
        test.stopTest();
    }
}