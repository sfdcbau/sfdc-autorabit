/*
 #####################################################
 # Created By...................: Aman Gupta
 # Created Date.................: 29 Mar, 2019
 # Last Modified By.............: Aman Gupta
 # Last Modified Date...........: 29 Mar, 2019
 # Description..................: All EmailMessage Trigger event will be managed here. 
                      			  Code should be written in the respective utility handlers and call method from required event method.
 # Test Class...................: ST_EmailMessageTriggerTest
 #####################################################
*/
public class ST_EmailMessageTriggerEventHandler {
    
    // All before insert event logic should be called from beforeInsert method
    public static void beforeInsert(List<EmailMessage> triggerNew) {
        
    }
    
    // All after insert Event logic should be called from afterInsert method
    public static void afterInsert(List<EmailMessage> triggerNew, Map<Id, EmailMessage> newMap) {
        // SE-2450 --> Route specific emails into a new "Unassigned Premium Cases" case view.
        ST_EmailMessageTriggerUtilityHandler.updatePremiumCases(triggerNew);
    }
    
    // All after insert Event logic should be called from beforeUpdate method
    public static void beforeUpdate(List<EmailMessage> triggerNew, List<EmailMessage> triggerOld, Map<Id, EmailMessage> newMap, Map<Id, EmailMessage> oldMap) {
    
    }
    
    // All after insert Event logic should be called from afterUpdate method
    public static void afterUpdate(List<EmailMessage> triggerNew, List<EmailMessage> triggerOld, Map<Id, EmailMessage> newMap, Map<Id, EmailMessage> oldMap) {
    
    }
    
    // All after insert Event logic should be called from beforeDelete method
    public static void beforeDelete(List<EmailMessage> triggerOld, Map<Id, EmailMessage> oldMap) {
        
    }
    
    // All after insert Event logic should be called from afterDelete method
    public static void afterDelete(List<EmailMessage> triggerOld, Map<Id, EmailMessage> oldMap) {
        
    }
}