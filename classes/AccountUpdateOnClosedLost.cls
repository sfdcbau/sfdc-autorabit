public class AccountUpdateOnClosedLost {
    @InvocableMethod(label='Update Account Records' description='update the account records with public group useres.')
    public static void updateAccountRecords(List<ID> accountidlist) {
        /*List<String> accountNames = new List<String>();
        List<Account> accounts = [SELECT Name,Customer_Status_picklist__c,Assoc_Segment_Ranking__c FROM Account WHERE Id in :accountidlist];
        //query public groups and group member and make two list per group
        List<Group> gm = [Select Id,DeveloperName From Group where Type = 'regular'];
        List<GroupMember> BDRUSERLIST=[select Id,UserOrGroupId from GroupMember where Group.type='Regular' AND Group.DeveloperName='BDRs'];
        List<GroupMember> SDRUSERLIST=[select Id,UserOrGroupId from GroupMember where Group.type='Regular' AND Group.DeveloperName='SDRs'];
        
        //store the updataded account in this list
        List<Account> UpdatedAccount =new List<Account>();
        
        //get the custom Setting REcord values type values 
        List<Account_Setting__c> accountSetting=[Select Id,BDRs_Index__c,SDRs_Index__c from Account_Setting__c];
        //get the next indexed user of the appropriate public group and store its index to get the next user of public group   
        Integer BDRs_Index;
        Integer SDRs_Index;
        if(accountSetting == null && accountSetting.size()==0){
            BDRs_Index=0;
            SDRs_Index=0;
            Account_Setting__c accountset= new Account_Setting__c(Name='Index',BDRs_Index__c=0,SDRs_Index__c=0);
            insert accountset;
        }
        
        //loop through all accounts and assign next user and update account list and custom setting record index
        for(Account newacc: accounts){
            if(accountSetting != null && accountSetting.size()>0){
                if(newacc.Assoc_Segment_Ranking__c != null){
                    //get the index of next user from the public group based on the last index stored into custom metadata for BDRs Public group
                    //And if the last user of list then get the first user of public group
                    
                    if(accountSetting[0].BDRs_Index__c +1 <= BDRUSERLIST.size()){
                        BDRs_Index=(Integer)((accountSetting[0].BDRs_Index__c +1 == BDRUSERLIST.size())?0:accountSetting[0].BDRs_Index__c +1);
                        accountSetting[0].BDRs_Index__c=accountSetting[0].BDRs_Index__c+1;
                    }else{
                        BDRs_Index=0;
                        accountSetting[0].BDRs_Index__c=0;
                    }
                    
                    
                    newacc.OwnerId=BDRUSERLIST[BDRs_Index].UserOrGroupId;
                    newacc.SDR_MDR_Owner__c=BDRUSERLIST[BDRs_Index].UserOrGroupId;
                }else{
                    //get the index of next user from the public group based on the last index stored into custom metadata for SDRs Public group 
                    //And if the last user of list then get the first user of public group
                    //if(accountSetting != null && accountSetting.size()>0){
                    if(accountSetting[0].SDRs_Index__c +1 <= SDRUSERLIST.size()){
                        SDRs_Index=(Integer)((accountSetting[0].SDRs_Index__c +1 == SDRUSERLIST.size())?0:accountSetting[0].SDRs_Index__c+1);
                        accountSetting[0].SDRs_Index__c=accountSetting[0].SDRs_Index__c+1;
                    }else{
                        SDRs_Index=0;
                        accountSetting[0].SDRs_Index__c=0;
                    }   
                    //}
                    newacc.OwnerId=SDRUSERLIST[SDRs_Index].UserOrGroupId;
                    newacc.SDR_MDR_Owner__c=SDRUSERLIST[SDRs_Index].UserOrGroupId;
                }
            }
            newacc.Customer_Status_picklist__c='SDR Assigned';
            newacc.DispositionCode__c='';
            newacc.SubDisposition__c='';
            newacc.Engagement_Cohort__c='Closed Lost Re-Engagement';
            UpdatedAccount.add(newacc);
        }
        if(accountSetting.size()>0){
        update accountSetting;    
        }
        
        update UpdatedAccount;*/
    }
}