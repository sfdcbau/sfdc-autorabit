/***********************************************
 * @author    : Sanjeev Kaurav
 * @testclass : ST_MeetingTrackingTest
 * @PB Name   : Meeting_Tracking_on_Opportunity
 * 
 * *********************************************/
public class ST_MeetingTracking {
    
    @InvocableMethod
    Public static void ST_UpdateMeetingResult(list<Event> eventValues){
        
        list<Opportunity> updateOpportunityList = new list<Opportunity>();
        Map<Id,Event> mapOfIdAndEvent = new Map<Id,Event>();  // Map of Opportunity Id and Event 
        for(Event eve:eventValues){
            mapOfIdAndEvent.put(eve.whatid,eve); // Storing value of Opportunity Id and Event 
        }
        
        for(Opportunity opp : [Select id,Discovery_Call_Date_Time__c,Initial_Discovery_Call_Date_Time__c,Count_of_Discovery_Call_Reschedules__c from Opportunity where id IN :mapOfIdAndEvent.keyset()]){
              
            opp.Discovery_Call_Date_Time__c = mapOfIdAndEvent.get(opp.id).ActivityDateTime;  // Update Discovery Call date time from Event's ActivityDateTime
            updateOpportunityList.add(opp);
        }
        if(updateOpportunityList.size()>0){
            update updateOpportunityList;   //Updating Opportunity 
        }
    }
}