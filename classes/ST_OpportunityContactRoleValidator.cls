public class ST_OpportunityContactRoleValidator{

/** ------------------------------------------------------------------------------------------------
* @author         Himanshu Sharma  
* @version        1.0
* @UserStory      SE-1118
* @Name           checkContactRoleonOpp 
* @Purpose        method to check if there is no ContactRole on Opportunity with Role Owner/POC and Opportunity is getting closed
* @test class     ST_OpportunityTriggerHandlertest
* -------------------------------------------------------------------------------------------------+
*/
    public static void checkContactRoleonOpp(List<Opportunity> triggerNew, Map<Id,Opportunity> triggerOldMap){
        Set<Id> setAccId = new Set<Id>();
        Map<Id,List<Contact>> mapAccidtoContact = new Map<Id,List<Contact>>(); 
        Map<Id,List<Opportunity>> mapAccIdtoOpp = new Map<Id,List<Opportunity>>();
        String allRoles = Label.OppContactRole;
        Id SalesRecTypeId;
        
            if(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RecordTypeForOppStage) != null){
                SalesRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RecordTypeForOppStage).getRecordTypeId();
            }
        
        for(Opportunity eachOpp : triggerNew){
            if(SalesRecTypeId != null && eachOpp.RecordTypeId == SalesRecTypeId && string.valueOf(eachOpp.StageName)  == Label.OppStageName && (triggerOldMap == null ||
               triggerOldMap.get(eachOpp.Id).StageName != eachOpp.StageName)){
                
                setAccId.add(eachOpp.AccountId);
                if(!mapAccIdtoOpp.containsKey(eachOpp.AccountId))
                        mapAccIdtoOpp.put(eachOpp.AccountId,new List<Opportunity>());
                        mapAccIdtoOpp.get(eachOpp.AccountId).add(eachOpp);
            }
        }
        
        if(setAccId != null && setAccId.size() > 0){
            for(Contact  eachContact : [Select Id,AccountId,OwnerPOC__c from Contact where AccountId IN : setAccId and OwnerPOC__c = :Integer.valueOf(Label.OppContactRole)]){
                    if(!mapAccidtoContact.containsKey(eachContact.AccountId))
                        mapAccidtoContact.put(eachContact.AccountId,new List<Contact>());
                        mapAccidtoContact.get(eachContact.AccountId).add(eachContact);
            
            }
        }
         for(Opportunity eachOpp : triggerNew){
            if(mapAccIdtoOpp.containsKey(eachOpp.AccountId) && mapAccIdtoOpp.get(eachOpp.AccountId) != null){
                if(mapAccidtoContact.containsKey(eachOpp.AccountId) && mapAccidtoContact.get(eachOpp.AccountId) != null){
                    //Do Nothing
                }
                else{
                     if(SalesRecTypeId != null && eachOpp.RecordTypeId == SalesRecTypeId && String.valueOf(eachOpp.StageName) == Label.OppStageName)
                     eachOpp.addError(Label.OppClosedWonError);
                }
            }
         
         }
    }
}