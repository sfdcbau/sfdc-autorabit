public without sharing class VCaseUtil 
{
	public static void addCaseTeamMember(Set<Id> caseSet)
    {
		List<CaseTeamMember> insertCaseTeamMemberList = new List<CaseTeamMember>();
        List<Case> caseList = new List<Case>();
		Map<String, CaseTeamRole> caseTeamRoleMap = new Map<String, CaseTeamRole>();
		Map<Id, Account> accountMap = new Map<Id, Account>();
		Set<Id> accountSet = new Set<Id>();
        
        for(Case c : [Select Id, AccountId From Case Where Id In :caseSet])
		{
			if(c.AccountId != null)
			{
				if(!accountSet.contains(c.AccountId))
				{
					accountSet.add(c.AccountId);
				}
                
                caseList.add(c);
			}
		}
        
        if(!accountSet.isEmpty())
        {
            // Collect team role
            for(CaseTeamRole ctr : [Select id, Name, AccessLevel From CaseTeamRole])
            {
                if(!caseTeamRoleMap.containsKey(ctr.Name))
                {
                    caseTeamRoleMap.put(ctr.Name, ctr);
                }
            }
            
            // Collect account
            accountMap = new Map<Id, Account>([Select Id, Onboarder__c, Onboarder__r.ContactId, Onboarder__r.IsActive, Success_Rep__c, Success_Rep__r.ContactId, Success_Rep__r.IsActive, Customer_Status_picklist__c
                                               From Account Where Id In :accountSet]);
            
            System.debug('#### caseTeamRoleMap :'+ caseTeamRoleMap);
            System.debug('#### accountMap :'+ accountMap);
            
            // Collect team member to insert
            for(Case c : caseList)
            {
                String roleOnboarder = 'Onboarder';
                String roleSuccessRep = 'Success Rep';
                
                if(accountMap.containsKey(c.AccountId))
                {
                    Account acc = accountMap.get(c.AccountId);
                    
                    if(caseTeamRoleMap.containsKey(roleOnboarder))
                    {	
                        if(acc.Customer_Status_picklist__c == 'Onboarding' && acc.Onboarder__c != null && acc.Onboarder__r.IsActive)
                        {
                            CaseTeamRole ctr = caseTeamRoleMap.get(roleOnboarder);
                            
                            CaseTeamMember ctm = new CaseTeamMember();
                            ctm.ParentId = c.Id;
                            
                            if(acc.Onboarder__r.ContactId != null)
                                ctm.MemberId = acc.Onboarder__r.ContactId;
                            else
                                ctm.MemberId = acc.Onboarder__c;
                            
                            ctm.TeamRoleId = ctr.Id;
                            insertCaseTeamMemberList.add(ctm);
                        }
                    }
                    
                    if(caseTeamRoleMap.containsKey(roleSuccessRep))
                    {
                        if(acc.Customer_Status_picklist__c == 'Success' && acc.Success_Rep__c  != null && acc.Success_Rep__r.IsActive)
                        {
                            CaseTeamRole ctr = caseTeamRoleMap.get(roleSuccessRep);
                            
                            CaseTeamMember ctm = new CaseTeamMember();
                            ctm.ParentId = c.Id;
                            
                            if(acc.Success_Rep__r.ContactId != null)
                                ctm.MemberId = acc.Success_Rep__r.ContactId;
                            else
                                ctm.MemberId = acc.Success_Rep__c;
                            
                            ctm.TeamRoleId = ctr.Id;
                            insertCaseTeamMemberList.add(ctm);
                        }
                    }
                }
            }
        }
        
        if(!insertCaseTeamMemberList.isEmpty())
        {
            insert insertCaseTeamMemberList;
        }
    }
}