/*------------------------------------------------------------------------------
 * Class Name:  ST_UpdateSizeSegmentOnSalesOpp
 * Test Class:  ST_UpdateSizeSegmentOnSalesOppTest & UtilityHelperTest
 * Subject:     To Update the Size Segment field on Sales Opp when there is a change on Account Fields (Engagement Cohort, Estimated Total Potential Tech).
 * Description: When Engagement cohort or Estimated Total potential Tech change on Account, 'First Success Call' - process builder will invoke this class in order to update Size segment on Sales Opp.
 * Version: 1
 * Developer :  Rakesh Kanugu
 * CreatedDate: 1/10/2019
 * Version: 2
 * Developer :  Rakesh Kanugu
 * CreatedDate: 1/31/2019
 * Version: 3
 * Developer :  Sanjeev Karuv
 * CreatedDate: 03/04/2019
--------------------------------------------------------------------------------*/
public class ST_UpdateSizeSegmentOnSalesOpp {
    @InvocableMethod(label='Update size segment from account' description='Update size segment from account')
    public static void updateSizeSegmentFromAcc(List<RequestWrapper> requestWrapper) {
        if(requestWrapper != null && !requestWrapper.isEmpty()){
            List<Opportunity> lstOpportunity = [select id,Size_Segment__c,Managed_Tech_Quantity__c,account.Estimated_Total_Potential_Technicians__c,account.Engagement_Cohort__c,account.Franchise_Strategic__c from Opportunity where accountid =:requestWrapper[0].accountId and recordtype.Name = 'Sales' and Isclosed = false order by createddate desc limit 1];
            if(lstOpportunity != null && !lstOpportunity.isEmpty()){
                //system.debug('--lstOpportunity[0].account.Engagement_Cohort__c--'+lstOpportunity[0].account.Engagement_Cohort__c);
                if(requestWrapper[0].flagValue == '1'){
                    lstOpportunity[0].Size_Segment__c = requestWrapper[0].sizeSegment;
                    update lstOpportunity[0];
                }else if(requestWrapper[0].flagValue == '2'){
                    if(lstOpportunity[0].Managed_Tech_Quantity__c == null){
                        lstOpportunity[0].Size_Segment__c = requestWrapper[0].sizeSegment;
                        update lstOpportunity[0];   
                    }
                }else if(requestWrapper[0].flagValue == '3'){
                    if(lstOpportunity[0].Managed_Tech_Quantity__c == null){
                        if(lstOpportunity[0].account.Estimated_Total_Potential_Technicians__c == null){
                            lstOpportunity[0].Size_Segment__c = null;
                        }else if(lstOpportunity[0].account.Estimated_Total_Potential_Technicians__c >= 30){
                            lstOpportunity[0].Size_Segment__c = 'Enterprise';
                        }else if(lstOpportunity[0].account.Estimated_Total_Potential_Technicians__c < 30){
                            lstOpportunity[0].Size_Segment__c = 'Corporate';
                        }
                    }else if(lstOpportunity[0].Managed_Tech_Quantity__c != null && lstOpportunity[0].Managed_Tech_Quantity__c >= 30){
                        lstOpportunity[0].Size_Segment__c = 'Enterprise';
                    }else if(lstOpportunity[0].Managed_Tech_Quantity__c != null && lstOpportunity[0].Managed_Tech_Quantity__c < 30){
                        lstOpportunity[0].Size_Segment__c = 'Corporate';
                    }
                    update lstOpportunity[0];   
                }else if(requestWrapper[0].flagValue == '4'){
                    //system.debug('--lstOpportunity[0].account.Franchise_Strategic__c--'+lstOpportunity[0].account.Franchise_Strategic__c);
                    if(lstOpportunity[0].account.Franchise_Strategic__c == false && lstOpportunity[0].Managed_Tech_Quantity__c == null){
                        lstOpportunity[0].Size_Segment__c = null;
                        //else if(String.isNotBlank(lstOpportunity[0].account.Engagement_Cohort__c) && (lstOpportunity[0].account.Engagement_Cohort__c.toLowerCase().contains('direct energy') || lstOpportunity[0].account.Engagement_Cohort__c.toLowerCase().contains('American Leak Detection'))){
                    }else if(lstOpportunity[0].account.Franchise_Strategic__c  == true){
                        lstOpportunity[0].Size_Segment__c = 'Enterprise';
                    }else if(lstOpportunity[0].account.Franchise_Strategic__c  == false){
                       // system.debug('--lstOpportunity[0].Managed_Tech_Quantity__c--'+lstOpportunity[0].Managed_Tech_Quantity__c);
                        if(lstOpportunity[0].Managed_Tech_Quantity__c != null && lstOpportunity[0].Managed_Tech_Quantity__c >= 30){
                            lstOpportunity[0].Size_Segment__c = 'Enterprise';
                        }else if(lstOpportunity[0].Managed_Tech_Quantity__c != null && lstOpportunity[0].Managed_Tech_Quantity__c < 30){
                            lstOpportunity[0].Size_Segment__c = 'Corporate';
                        }else if(lstOpportunity[0].Managed_Tech_Quantity__c == null){
                            lstOpportunity[0].Size_Segment__c = null;
                        }
                    }      
                    update lstOpportunity[0];   
                }
            }
        }
    }
/*------------------------------------------------------------------------------
 * Method Name: RequestWrapper
 * Subject:     Passing the Variables( Account id, Size Segment and Flag value) in process builder ('First Success Call')  .
 * Description: By Using these variables we are assigning the values to Size Segment on Sales Opp.
--------------------------------------------------------------------------------*/
    
    public class RequestWrapper {
        @InvocableVariable(required=true)
        public String accountId;
    
        @InvocableVariable(required=true)
        public String sizeSegment;
        
        @InvocableVariable(required=true)
        public String flagValue;
    }
}