public class EditOpptyFromAccount{
  @InvocableMethod(label='Edit Oppty from Acc' description='updates oppty')
  public static void updateOppty(List<ID> ids) {
    //List<String> accountNames = new List<String>();
    List<Opportunity> opptysToEdit= [SELECT Id,OpptyEmailUpdate__c FROM Opportunity WHERE account.Id in :ids];
    for (Opportunity opp: opptysToEdit) {
      opp.OpptyEmailUpdate__c = true;
    }
    update opptystoedit;
  }
}