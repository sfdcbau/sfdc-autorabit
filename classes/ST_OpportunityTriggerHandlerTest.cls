/*
 #####################################################
 # Created By....................: Himanshu Sharma  
 # Created Date................: 28 Dec, 2018
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 22 Jan, 2019
 # Description...................: This is test class for ST_OpportunityTriggerHandler, OpportunityTriggerHandler class.
 # Version.......................: V 1.1
 # ModifiedDate.....................: 04/16/2019 
 #####################################################
*/
@istest
private class ST_OpportunityTriggerHandlerTest {

   @testsetup
    static void createData(){
        // inserting Account
        Account accObj1 = new Account(Name = 'TestAcc1',Residential__c = 10, Industry__c = 'Air Quality', Time_Zone__c = 'Alaska (UTC-09:00)', Pricing_Model__c = 'New Pricing (Managed Techs)', No_Known_Active_Partners__c = true, Tenant_Name__c = 'TestTenName1', Tenant_ID__c = 'TestTenName1', Initial_Username__c = 'TestTenName1', Software_newpicklist__c = 'Acowin', Estimated_Total_Potential_Office_Users__c = 10, Estimated_Total_Potential_Technicians__c = 20, Influencer__c = 'Yes', mAsset__c = 'Awards');
        accObj1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        Account accObj2 = new Account(Name = 'TestAcc2',Residential__c = 10, Industry__c = 'Air Quality', Time_Zone__c = 'Alaska (UTC-09:00)', Pricing_Model__c = 'New Pricing (Managed Techs)', No_Known_Active_Partners__c = true, Tenant_Name__c = 'TestTenName2', Tenant_ID__c = 'TestTenName2', Initial_Username__c = 'TestTenName2', Software_newpicklist__c = 'Acowin', Estimated_Total_Potential_Office_Users__c = 10, Estimated_Total_Potential_Technicians__c = 30, Influencer__c = 'Yes', mAsset__c = 'Awards');
        accObj2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        insert new List<Account> {accObj1, accObj2};
        
        // inserting Contact
        Contact conObj1 = new Contact(FirstName='testFN1', LastName='testLN1', Email='test1@test.com', AccountId = accObj1.Id, Role__c = 'Consultant');
        Contact conObj2 = new Contact(FirstName='testFN2', LastName='testLN2', Email='test2@test.com', AccountId = accObj2.Id, Role__c = 'Consultant');
        insert new List<Contact> {conObj1, conObj2};

        // inserting Product
        Product2 addOnProdObj1 = new Product2(Name = 'AddOnProd', Product_Type__c = 'Add-on', Core_Product__c = false);
        Product2 coreProdObj1 = new Product2(Name = 'Premier Package', Core_Product__c = true);
        insert new List<Product2> {coreProdObj1, addOnProdObj1};
        
        // inserting PricebookEntry
        Id stdPBId = Test.getStandardPricebookId();
        PricebookEntry pbOj1 = new PricebookEntry (Product2ID = coreProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=50, isActive=true);
        PricebookEntry pbOj2 = new PricebookEntry (Product2ID = addOnProdObj1.Id,Pricebook2ID = stdPBId, UnitPrice=100, isActive=true);
        insert new List<PricebookEntry> {pbOj1, pbOj2};

        // inserting Opportunity with Stage = 'Stage Zero'
        Opportunity oppObj1 = new Opportunity(Name = 'TestAcc1Opp1', AccountId = accObj1.Id, StageName = 'Stage Zero', CloseDate = System.today().addDays(2), Address_Industry_Verified__c = true, ImplementationDate__c = System.today().addDays(20), OBDiscoveryCallDate__c = System.today().addDays(20), AE_Discovery_POC__c = conObj1.Id, Onboarding_POC__c = conObj1.Id, AE_Confidence__c = 'High Probability', AE_Opportunity_Notes__c = 'Demo', Pricebook2Id = pbOj1.Pricebook2Id, isEstimatedPTech__c = true, Competing_Software__c = 'Acowin');
        oppObj1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity oppObj2 = new Opportunity(Name = 'TestAcc2Opp2', AccountId = accObj2.Id, StageName = 'Stage Zero', CloseDate = System.today().addDays(2), Address_Industry_Verified__c = true, ImplementationDate__c = System.today().addDays(20), OBDiscoveryCallDate__c = System.today().addDays(20), AE_Discovery_POC__c = conObj2.Id, Onboarding_POC__c = conObj2.Id, AE_Confidence__c = 'High Probability', AE_Opportunity_Notes__c = 'Demo', Pricebook2Id = pbOj1.Pricebook2Id, isEstimatedPTech__c = true, Competing_Software__c = 'Acowin');
        oppObj2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        Opportunity opp1 = new Opportunity (Pending_Boarding_Date__c= system.today(),Qualified_Rate__c = 10,Mid_Qualified_Rate__c = 10,Non_Qualified_Rate__c  = 10,Per_Item__c = 10,Monthly_Service_Fee__c = 10,Pricing_Type__c = 'Interchange',Passthrough_Dues_and_Assessments__c = 'Yes',STP_CC_Monthly_Volume__c = 10,STP_CC_MRR__c = 10,Basis_Points_BPS__c= 10,Negotiation_Stage_Date__c  = system.today(),Previous_Processor_BPS__c  = 10,Delayed_Implementation__c = true, recordtypeid = OppRT, ImplementationDate__c = system.today(), Name='Opp1', StageName='Stage Zero', Onboarding_POC__c=conObj1.id, CloseDate=Date.today(), Pricebook2Id = pbOj1.Pricebook2Id, AccountId = accObj1.id);
       // insert opp1;
        
        insert new List<Opportunity> {oppObj1, oppObj2};
            
            // inserting OpportunityContactRole
        OpportunityContactRole oppConRoleObj1 = new OpportunityContactRole(OpportunityId = oppObj1.Id, Role = 'Owner', ContactId = conObj1.Id);
        OpportunityContactRole oppConRoleObj2 = new OpportunityContactRole(OpportunityId = oppObj2.Id, Role = 'Owner', ContactId = conObj2.Id);
        
        insert new List<OpportunityContactRole> {oppConRoleObj1, oppConRoleObj2};
        
        // inserting OpportunityLineItem for add-on product
        OpportunityLineItem oppLIObj1 = new OpportunityLineItem(OpportunityId = oppObj1.Id, Product2Id = pbOj1.Id, UnitPrice = 50.00, Quantity = 30);
        //OpportunityLineItem oppLIObj2 = new OpportunityLineItem(OpportunityId = oppObj2.Id, Product2Id = pbOj1.Id, UnitPrice = 40.00, Quantity = 50);
        OpportunityLineItem oppLIObj3 = new OpportunityLineItem(OpportunityId = oppObj2.Id, Product2Id = pbOj2.Id, UnitPrice = 40.00, Quantity = 50);
        
        //insert new List<OpportunityLineItem> {oppLIObj1, oppLIObj2, oppLIObj3};
        insert oppLIObj1;
       // insert oppLIObj2;
        insert oppLIObj3;
    }
    
    @istest
    static void testcheckContactRoleonOpp(){
        Opportunity eachOpp = [Select id,stageName from Opportunity LIMIT 1];
        test.startTest();
        eachOpp.StageName ='SE Vetting';
        update eachOpp;
        
        try{
            eachOpp.DQ_Rationale__c = 'test';
            eachOpp.StageName ='Closed Won';
            update eachOpp;    
        }
        catch(Exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('At least one Owner or POC contact must exist on this Account for the Opportunity to be set to Closed Won. Please go to the appropriate Contact record on this account and update the "Role" field to identify either the Owner or POC.') ? true : false;
            System.assertEquals(expectedExceptionThrown, true);
        }
        test.stopTest();
    }
    
    @istest
    static void checkContactRoleonOppPositiveTest(){
        
        Opportunity eachOpp = [Select id,stageName,DQ_Rationale__c from Opportunity LIMIT 1];
        Contact eachCon = [Select id from Contact LIMIT 1];
        Product2 p2 = [Select id from Product2 WHERE Core_Product__c = false LIMIT 1];
        
        OpportunityContactRole objRole = new OpportunityContactRole();
        objRole.OpportunityId = eachOpp.Id;
        objRole.Role = 'Owner';
        objRole.ContactId = eachCon.Id;
        insert objRole;
        
        
       // eachOpp.StageName ='SE Vetting';
       // update eachOpp; 
        
        opportunitylineitem oli = new opportunitylineitem();
        oli.OpportunityId = eachOpp.id;
        oli.product2Id = p2.id;
        oli.opportunity = eachOpp;
        oli.ServiceDate = system.today();
        oli.Quantity = 10;
        oli.unitprice = 10;
        insert oli;
        
        test.startTest();
        eachOpp.DQ_Rationale__c = 'test';
        eachOpp.StageName ='Closed Won';
        eachOpp.Address_Industry_Verified__c = true;
        eachOpp.OBDiscoveryCallDate__c = system.today();
        eachOpp.ImplementationDate__c = system.today();
        eachOpp.Onboarding_POC__c = eachCon.Id;
        eachOpp.AE_Discovery_POC__c  = eachCon.Id;
        eachOpp.Competing_Software__c = 'Acowin';
        try{
            update eachOpp;    
        }
        catch(Exception ex){
            system.debug('Exception is ' + ex.getMessage());
        }
        test.stopTest();
    }
    
    /**
     * @author : Aman Gupta
     * @date : 21 Jan, 2019
     * @description : This method is to test ST_OpportunityTriggerHandler validateAddOnProductOnOppty function and OpportunityTriggerHandler handleInsertAsset function.
    */
    @isTest static void testValidateAddOnProductOnOppty() {
        test.startTest();
        List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY NAME ASC];
        List<Product2> prodObjList = [Select Id, Name from Product2 ORDER BY NAME ASC];
        List<Contact> conObjList = [Select Id, Name from Contact ORDER BY NAME ASC];
        
        for(Contact conObj : conObjList) {
            conObj.Role__c = 'Co-Owner';
        }
        update conObjList;
            
        // updating Opportunity records with Stage = 'Closed Won'
        oppObjList[0].StageName = 'Closed Won';
        oppObjList[1].StageName = 'Closed Won';
        
        update new List<Opportunity> (oppObjList);
        
        System.assertEquals(2, oppObjList.size());
        System.assertEquals('Closed Won', oppObjList[0].StageName);
        System.assertEquals('Closed Won', oppObjList[1].StageName);
        
        List<Asset> assetObjList = [Select Id, Name from Asset];
        test.stopTest();
    }
    
    /**
     * @author : Aman Gupta
     * @date : 22 Jan, 2019
     * @description : This method is to test ST_UpdateCodeRedStartandEndDate updateDateMethod function.
    */
    @isTest static void testUpdateDateMethod() {
        test.startTest();
        
        
        List<Account> accObjList = [Select Id, Name from Account ORDER BY NAME ASC];
        List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY NAME ASC];
        
        List<Product2> prodObjList = [Select Id, Name from Product2 where Core_Product__c=True ORDER BY NAME ASC];
        List<Contact> conObjList = [Select Id, Name from Contact ORDER BY NAME ASC];
        
        for(Contact conObj : conObjList) {
            conObj.Role__c = 'Co-Owner';
        }
        update conObjList;
            
        // updating Opportunity records with Stage = 'Closed Won'
        oppObjList[0].StageName = 'Closed Won';
        oppObjList[1].StageName = 'Closed Won';
        
        update new List<Opportunity> (oppObjList);
        
        System.assertEquals(2, oppObjList.size());
        System.assertEquals('Closed Won', oppObjList[0].StageName);
        System.assertEquals('Closed Won', oppObjList[1].StageName);
        
        accObjList[0].Health_Score__c = '1 - Code Red';
        accObjList[0].HealthScoreDisposition1__c = 'Product Gap';
        accObjList[0].HealthScoreSubDisposition1__c = 'Back Office - Memberships';
        accObjList[0].HealthScoreDisp1Comments__c = 'Test';
        accObjList[1].Health_Score__c = '1 - Code Red';
        accObjList[1].HealthScoreDisposition1__c = 'Product Gap';
        accObjList[1].HealthScoreSubDisposition1__c = 'Back Office - Memberships';
        accObjList[1].HealthScoreDisp1Comments__c = 'Test';
        update accObjList;
        
        List<Onboarding__c> onBDObjList = [Select Id, Code_Red_Start_Date__c, Code_Red_End_Date__c from Onboarding__c  ORDER BY NAME ASC];
        System.assertEquals(1, onBDObjList.size());
        
        Boolean isTriggerContext = false;
        Integer batchSize = 0;
        
        ST_SalesAddOnProductHandler testSalesaddon = new ST_SalesAddOnProductHandler(isTriggerContext, batchSize);

        test.stopTest();
    }
    
    /**
     * @author : Aman Gupta
     * @date : 22 Jan, 2019
     * @description : This method is to test ST_UpdateObDiscoveryCall ST_UpdateDiscoCall function.
    */
    @isTest static void testST_UpdateDiscoCall() {
        test.startTest();
        List<Account> accObjList = [Select Id, Name from Account ORDER BY NAME ASC];
        List<Opportunity> oppObjList = [Select Id, Name, StageName, OBDiscoveryCallDate__c, Delayed_Implementation__c from Opportunity ORDER BY NAME ASC];
        List<Product2> prodObjList = [Select Id, Name from Product2 ORDER BY NAME ASC];
        List<Contact> conObjList = [Select Id, Name from Contact ORDER BY NAME ASC];
        
        for(Contact conObj : conObjList) {
            conObj.Role__c = 'Co-Owner';
        }
        update conObjList;
            
        // updating Opportunity records with Stage = 'Closed Won'
        oppObjList[0].StageName = 'Closed Won';
        oppObjList[0].Delayed_Implementation__c = true;
        oppObjList[1].StageName = 'Closed Won';
        oppObjList[1].Delayed_Implementation__c = true;
        
        update new List<Opportunity> (oppObjList);
        
        System.assertEquals(2, oppObjList.size());
        System.assertEquals('Closed Won', oppObjList[0].StageName);
        System.assertEquals('Closed Won', oppObjList[1].StageName);
        
        
        List<Onboarding__c> onBDObjList = [Select Id, OB_Discovery_Call_Date__c from Onboarding__c  ORDER BY NAME ASC];
        System.assertEquals(1, onBDObjList.size());
        System.assertEquals(oppObjList[0].OBDiscoveryCallDate__c, onBDObjList[0].OB_Discovery_Call_Date__c);
        test.stopTest();
    }
}