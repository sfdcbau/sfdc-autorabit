/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is the handler class for Automating asset creation for the platform onboarding check 
*
* This class automatically creates an asset for the Payment-Check for Platform when an onboarding 
check object is created through quick action
* It also assigns backs the created Onboarding asset to the OLI.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Andrew Huynh   <ahuynh@servicetitan.com>
* @version        1.0
* @created        04-30-2018
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘\
* Test Class: OnboardingTriggerhandlerTest
*/

public with sharing class OnboardingTriggerhandler {
    
    private static Boolean bool=false;
    
    public static void handleInsertAsset(List<Onboarding__c> onboarding){
        List<Trigger_Settings__mdt> TS =  new List<Trigger_Settings__mdt>();
        
        TS = [SELECT Label,isActive__c FROM Trigger_Settings__mdt where Label='ST_OnboardingTriggerhandler' ];
        
        for (Trigger_Settings__mdt sett : TS){
            if(sett.isActive__c == TRUE){
                bool = true;
            }
        }
        if(bool == true){
            if(onboarding.size()>0){
                
                map<id,id> oppAssetId = new map<id,id>();
                id onboardingId = null;
                id opportunityId = null;
                list<onboarding__c> onboardingChecksList = new list<onboarding__c>();
                list<onboarding__c> onboardingChecksToUpdate = new list<onboarding__c>();
                list<asset> assetsToInsert = new list<asset>();
                
                //loop through newly created onboarding records and if the manual check add on field is marked as true, then we create new assets for these onboarding records
                for(onboarding__c o: onboarding){
                    if(o.Manual_Check_Add_On__c == true){
                        
                        onboardingId = o.id;
                        opportunityId = o.Opportunity__c;
                        //Using this list later again to update these same onboarding records to relate the newly created assets
                        onboardingChecksList.add(o);
                        
                        //Seting up the new assets
                        asset a = new asset();
                        
                        a.name = o.name;
                        a.accountId = o.Account__c;
                        a.opportunity__c = o.Opportunity__c;
                        a.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();  
                        
                        assetsToInsert.add(a);
                    }
                }
                
                //Inserting the new assets
                if(assetsToInsert.size()>0){
                    insert assetsToInsert;
                }
                
                //mapping newly created onboarding record to the newly created asset 
                if(assetsToInsert.size()> 0){
                    onboarding__c onboardingUpdate = new onboarding__c();
                    
                    onboardingUpdate.id = onboardingId;
                    onboardingUpdate.Asset__c = AssetsToInsert[0].id;
                    onboardingChecksToUpdate.add(OnboardingUpdate);           
                }
                
                //updating the sames onboarding records to relate the newly created assets. 
                if(onboardingChecksToUpdate.size()>0){
                    update onboardingChecksToUpdate; 
                }
            }
        }
        
    }
    
}