@isTest
public class AccUpdateOppContactTriggerhandlerTest{

    public static testMethod void testCopyPrimaryContact(){
        
        Account a = new Account(Name = 'Test Account');
        insert a;
        
        Contact con = new contact();
        
        con.LastName='TestContact';
        con.Email='testContact@test.com'; 
        con.AccountId = a.Id; 
        
        insert con;
        
        opportunity o = new Opportunity();
        o.name = 'Test Opp';
        o.StageName = 'Stage Zero';
        o.CloseDate = date.today().addDays(50);
        o.AccountId = a.id;
        insert o;
        
        Test.StartTest();
        
        a.Primary_Contact__c = con.id;
        update a;
        
        Test.StopTest();
    }

    /**
     * @author : Aman Gupta
     * @date : 30 Mar, 2019
     * @description : This method is to test AccUpdateOppContactTriggerhandler updateAccPayLiveDate function.
    */
    @isTest static void testUpdateAccPayLiveDate() {
        // updating Account records
        Account accObj1 = utilityHelperTest.createAccount();
        accObj1.Name = 'TestAcc1';
        accObj1.ST_Payments_Customer_Status__c = '';
        accObj1.Tenant_ID__c = '100001';
        accObj1.Tenant_Name__c = 'testtenant1';
        accObj1.Initial_Username__c = 'testtenant1';
        
        Account accObj2 = utilityHelperTest.createAccount();
        accObj2.Name = 'TestAcc2';
        accObj2.ST_Payments_Customer_Status__c = 'Pending ST Live';
        accObj2.Tenant_ID__c = '100002';
        accObj2.Tenant_Name__c = 'testtenant2';
        accObj2.Initial_Username__c = 'testtenant2';
        
        Account accObj3 = utilityHelperTest.createAccount();
        accObj3.Name = 'TestAcc3';
        accObj3.ST_Payments_Customer_Status__c = 'Live (Credit Cards)';
        accObj3.Tenant_ID__c = '100003';
        accObj3.Tenant_Name__c = 'testtenant3';
        accObj3.Initial_Username__c = 'testtenant3';
        
        Account accObj4 = utilityHelperTest.createAccount();
        accObj4.Name = 'TestAcc4';
        accObj4.ST_Payments_Customer_Status__c = 'Pending ST Live';
        accObj4.Payments_Live_Date__c = System.today().addDays(-90);
        accObj4.Tenant_ID__c = '100004';
        accObj4.Tenant_Name__c = 'testtenant4';
        accObj4.Initial_Username__c = 'testtenant4';
        
        List<Account> accObjList = new List<Account> {accObj1, accObj2, accObj3, accObj4};
        insert accObjList;
        
        // updating Account records
        accObj1.ST_Payments_Customer_Status__c = 'Live (Credit Cards)'; 
        accObj2.ST_Payments_Customer_Status__c = 'Live (Checks)';
        accObj3.ST_Payments_Customer_Status__c = 'Live (Credit Cards & Checks)';
        accObj4.ST_Payments_Customer_Status__c = 'Live (Credit Cards)';
            
        update accObjList;
        
        // query Accunt records
        accObjList = [Select Id, Payments_Live_Date__c from Account ORDER BY Name];
        System.assertEquals(4, accObjList.size());
        System.assertEquals(System.today(), accObjList[0].Payments_Live_Date__c);
        System.assertEquals(System.today(), accObjList[1].Payments_Live_Date__c);
        System.assertEquals(null, accObjList[2].Payments_Live_Date__c);
        System.assertNotEquals(System.today(), accObjList[3].Payments_Live_Date__c);
    }
}