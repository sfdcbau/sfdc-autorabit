/***********************************************
 * @author    : Sanjeev Kaurav
 * @testclass : ST_UpdateCountDiscoveryCallTest
 * @PB Name   : UpdateDispAndSubDispCodes v10
 * @Description: This is a Test Class for ST_UpdateCountDiscoveryCallReschedules
 * *********************************************/

@isTest
private class ST_UpdateCountDiscoveryCallTest{

    static testmethod void  TestUpdateCountPositive(){
    
        //Create Account Data
        Account acc = new Account();
        acc.Name='Test Account' ;
        insert acc;
        
        //Opportunity's Sales Record Type
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        //Create Opportunity Data
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', Discovery_Call_Date_Time__c = system.today(),Initial_Discovery_Call_Date_Time__c = null,Count_of_Discovery_Call_Reschedules__c  = 1,OBDiscoveryCallDate__c = system.today(),RecordTypeId= OppRT, CloseDate=Date.today() ,AccountId = acc.id, ImplementationDate__c= System.today()+2);
        insert opp1;
        opp1.Discovery_Call_Date_Time__c =  System.today() + 1;
        opp1.Count_of_Discovery_Call_Reschedules__c  = 2;  
        opp1.Initial_Discovery_Call_Date_Time__c = opp1.Discovery_Call_Date_Time__c;
        Update opp1;
        
        System.assertEquals(opp1.Discovery_Call_Date_Time__c, opp1.Initial_Discovery_Call_Date_Time__c);  //Asserting Discovery Call date Time and Initital Discovery Call date Time
        
    }
    
    static testmethod void  TestUpdateCountNegative(){
    
        //Create Account Data
        Account acc = new Account();
        acc.Name='Test Account' ;
        insert acc;
        
        //Opportunity's Sales Record Type
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        //Create Opportunity Data
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', Discovery_Call_Date_Time__c = system.today(),Initial_Discovery_Call_Date_Time__c = null,Count_of_Discovery_Call_Reschedules__c  = 1,OBDiscoveryCallDate__c = system.today(),RecordTypeId= OppRT, CloseDate=Date.today() ,AccountId = acc.id, ImplementationDate__c= System.today()+2);
        insert opp1;
          
        opp1.Initial_Discovery_Call_Date_Time__c = opp1.Discovery_Call_Date_Time__c;
        Update opp1;
        
        Opportunity opp = [Select id, Initial_Discovery_Call_Date_Time__c, Discovery_Call_Date_Time__c from Opportunity where id = :opp1.id];
        
        System.assertNotEquals(opp1.Discovery_Call_Date_Time__c + 1, opp1.Initial_Discovery_Call_Date_Time__c); 
    }
}