@isTest
public class AccountHistoryTrackerHandlerTest{

    public static testMethod void testCreateAccountHistoryRecord(){
        
        Account a = new Account();
        a.Name = 'Test Account';
        a.Account_Level__c = 'OneToMany';
        insert a;
        
        
        Test.StartTest();
        
        a.Account_Level__c = 'Emerging';
        update a;
        
        Test.StopTest();
    }

}