@IsTest 
private class VPostChatTest 
{
	
    @IsTest(SeeAllData=false) 
    public static void testPostChat() 
    {
        
        // Insert case
        Case c1 = new Case();
        c1.Status = 'New';
        c1.Origin = 'Chat';
        c1.Description = 'Test';
        c1.Subject = 'Test';
        insert c1;
        
        test.startTest();
        
        PageReference pageref = Page.VPostChat;
        test.setCurrentPage(pageref);
        
        VPostChatController ctrl = new VPostChatController();
        
        if(ctrl.mycase == null)
        {
            ctrl.mycase = c1;
            
            Case_Rating__c cr = new Case_Rating__c();
            cr.Case__c = c1.Id;
            cr.Case_ID__c = c1.Id;
            ctrl.mycaserating = cr;
            
            CaseComment cc = new CaseComment();
        	cc.ParentId = c1.Id;
            ctrl.mycasecomment = cc;
        }
        
        List<SelectOption> myoptions = ctrl.getRateOptions();
        ctrl.mycaserating.Rate__c = '9';
        ctrl.mycaserating.Final_Resolution_Rate__c = '10';
        ctrl.mycasecomment.CommentBody = 'Hello';
        ctrl.submit();
        
        //System.assertEquals(ctrl.isCompleted, true);
        
        test.stopTest();
        
        // Check case
        List<Case> caseList = [Select Id, Rate__c, Final_Resolution_Rate__c From Case Where Id = :ctrl.mycase.Id Limit 1];
        //System.assertEquals(caseList[0].Rate__c, '9');
        //System.assertEquals(caseList[0].Final_Resolution_Rate__c, '10');
    }
	
    @IsTest(SeeAllData=false) 
    public static void testPostChatOnError() 
    {
        
        // Insert case
        Case c1 = new Case();
        c1.Status = 'New';
        c1.Origin = 'Chat';
        c1.Description = 'Test';
        c1.Subject = 'Test';
        insert c1;
        
        test.startTest();
        
        PageReference pageref = Page.VPostChat;
        test.setCurrentPage(pageref);
        
        VPostChatController ctrl = new VPostChatController();
        
        if(ctrl.mycase == null)
        {
            Case_Rating__c cr = new Case_Rating__c();
            cr.Case__c = c1.Id;
            cr.Case_ID__c = c1.Id;
            ctrl.mycaserating = cr;
        }
        
        List<SelectOption> myoptions = ctrl.getRateOptions();
        ctrl.mycaserating.Rate__c = '10';
        ctrl.mycaserating.Final_Resolution_Rate__c = '10';
        ctrl.submit();
        
        test.stopTest();
    }
    
}