@isTest
public class OnboardingTriggerhandlerTest{
 
    public static testMethod void testAssetCreation(){
        
        Id AccountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account a = new Account(Name = 'Test Account', RecordTypeId = AccountRT);
        insert a;
        
        Id AssetRT = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        Asset ast = new Asset();
        ast.Name = 'Test Assets';
        ast.Status = 'Pending';
        ast.AccountId = a.Id;
        ast.RecordTypeId = AssetRT;
        
        insert ast;

        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prd1 = new Product2 (Name='Platform Payments-Credit Card',Description='Test Product Entry 1',productCode = 'ABC', isActive = true);
        insert prd1;
    
        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=pricebookId,UnitPrice=50, isActive=true);
        insert pbe1;
    
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();

        Opportunity o = new Opportunity (Name='Opp1',StageName='Prospect', RecordTypeId= OppRT, CloseDate=Date.today(),Pricebook2Id = pbe1.Pricebook2Id, AccountId = a.id);
        insert o;
   
        Contact con = new contact();
        
        con.LastName='TestContact';
        con.Email='testContact@test.com'; 
        con.AccountId = a.Id; 
        
        insert con;
        
        Test.startTest();
        
        Onboarding__c ob = new Onboarding__c (Name = 'Onboarding Test',
                                              Account__c = a.id,
                                              Opportunity__c = o.id,
                                              Manual_Check_Add_On__c = true, 
                                              Number_of_Techs__c = 2,
                                              Onboarding_Status__c = 'Data Entry',
                                              RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId() 
                                             );
        insert ob; 
        
        Test.stopTest();

                                          
        
    }
}