/**
* ──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐

* This is called from 'Request: Notification Alerts to CRM and Requester when Request status changes' process on request object
* This class creates a Customer Reference program record for request or link request to CRP when external request is approved by CRM
* ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          2-6-2019
* @Jira User Story  SC-13
* ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘*/
public class ST_ExternalRequestCRPCreation {
    
    @InvocableMethod
    public static void checkandInsertCRP(List<Id> lstExtRefReqId){
        
        List<Reference_Request__c> lstRefRequest = new List<Reference_Request__c>();
        Set<Id> setReqId = new Set<Id>();
        Map<Id, List<Customer_Reference_Program__c>> mapConIdtoCRP = new Map<Id, List<Customer_Reference_Program__c>>();
        List<Customer_Reference_Program__c> lstCRPtoUpdate = new List<Customer_Reference_Program__c>();
        List<Customer_Reference_Program__c> lstCRPtoInsert = new List<Customer_Reference_Program__c>();
        List<Customer_Reference_Details__c> lstCRDtoInsert = new List<Customer_Reference_Details__c>();
        
        if(lstExtRefReqId != null && lstExtRefReqId.size() > 0){
            for(Reference_Request__c eachReq : [Select Id,Contact__c,Account__c,OwnerId from Reference_Request__c where Id IN :lstExtRefReqId]){
                lstRefRequest.add(eachReq);
                setReqId.add(eachReq.Contact__c);
            }
        }
        
        if(setReqId != null && setReqId.size() > 0){
            for(Customer_Reference_Program__c eachProg : [Select Id,Contact__c,Request_Id__c from Customer_Reference_Program__c where Contact__c IN :setReqId]){
                if(!mapConIdtoCRP.containsKey(eachProg.Contact__c))
                    mapConIdtoCRP.put(eachProg.Contact__c,new List<Customer_Reference_Program__c>());
                 mapConIdtoCRP.get(eachProg.Contact__c).add(eachProg);
            }
        }
        
        if(lstRefRequest != null && lstRefRequest.size() > 0){
            for(Reference_Request__c eachReq : lstRefRequest){
                if(mapConIdtoCRP.containsKey(eachReq.Contact__c) && mapConIdtoCRP.get(eachReq.Contact__c) != null){
                    for(Customer_Reference_Program__c eachProg : mapConIdtoCRP.get(eachReq.Contact__c)){
                        eachProg.Request_Id__c = eachReq.Id;
                        lstCRPtoUpdate.add(eachProg);
                    }
                }
                else{
                    Customer_Reference_Program__c eachProg = new Customer_Reference_Program__c();
                    eachProg.Contact__c = eachReq.Contact__c;
                    eachProg.Account__c = eachReq.Account__c;
                    eachProg.Request_Id__c = eachReq.Id;
                    eachProg.CRP_Status__c = 'Approved';
                    eachProg.Approved_Date__c = System.today();
                    eachProg.Max_Activities_Per_Year__c = 12;
                    eachProg.ownerId = eachReq.ownerId;
                    lstCRPtoInsert.add(eachProg);
                }
            }
        }
        if(lstCRPtoUpdate.size() > 0 && lstCRPtoUpdate != null){
            update lstCRPtoUpdate;
        }
        if(lstCRPtoInsert.size() > 0 && lstCRPtoInsert != null){
            insert lstCRPtoInsert;
        }
        
        if(lstCRPtoInsert != null && lstCRPtoInsert.size() > 0){
            for(Integer i =0 ; i < lstCRPtoInsert.size(); i++){
                Customer_Reference_Details__c eachDetails = new Customer_Reference_Details__c();
                eachDetails.Activity_Types__c = 'Prospect Calls';
                lstCRDtoInsert.add(eachDetails);
            }
        }
        if(lstCRDtoInsert != null && lstCRDtoInsert.size() > 0){
            insert lstCRDtoInsert;
        }
        if(lstCRDtoInsert != null && lstCRDtoInsert.size() > 0){
            lstCRPtoUpdate.clear();
            for(Integer i =0 ; i < lstCRDtoInsert.size(); i++){
                lstCRPtoInsert[i].Customer_Reference_Detail_Id__c = lstCRDtoInsert[i].Id;
                lstCRPtoUpdate.add(lstCRPtoInsert[i]);
            }
        }
        if(lstCRPtoUpdate != null && lstCRPtoUpdate.size() > 0){
            update lstCRPtoUpdate;
        }
    }
    
}