/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 11 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 11 Mar, 2019
 # Description...................: This is test class for ST_ObjectDetailsController class.
 #####################################################
*/
@isTest
private class ST_ObjectDetailsControllerTest {
    /**
     * @author : Aman Gupta
     * @date : 11 Mar, 2019
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        // insert Product2
        Product2 prodObj1 = utilityHelperTest.createProduct();
        prodObj1.name = 'Premier Package';
        prodObj1.Core_Product__c = true;
        
        Product2 prodObj2 = utilityHelperTest.createProduct();
        prodObj2.name = 'PriceBook';
        prodObj2.Product_Type__c = 'Add-on';
        insert new List<Product2> {prodObj1, prodObj2};
        
        // insert Pricebook2
        Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
        insert new List<Pricebook2> {custPBObj};
            
        // insert PricebookEntry
        PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
        PricebookEntry pbeObj2 = utilityHelperTest.craeteStandardPBE2(prodObj2.Id);
        insert new List<PricebookEntry> {pbeObj1, pbeObj2};
        
        // insert Account
        Account accObj = utilityHelperTest.createAccount();
        insert new List<Account> {accObj};
            
        // insert Contact
        Contact conObj = utilityHelperTest.createContact(accObj.Id);
        conObj.Role__c = 'Co-Owner';
        insert new List<Contact> {conObj};
            
        // insert Opportunity
        Opportunity salesOppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        salesOppObj1.Name = 'Test Opportunity Sales';
        Opportunity payOppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        payOppObj1.RecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        payOppObj1.Name = 'Test Opportunity Pay';
        insert new List<Opportunity> {salesOppObj1, payOppObj1};
    }
    
    /**
     * @author : Aman Gupta
     * @date : 05 Feb, 2019
     * @description : This method is to test ST_ObjectDetailsController getObBDDetails function.
    */
    @isTest static void testGetObBDDetailsPositive() {
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1};
        
        System.runAs (userObj1) {
            List<Account> accObjList = [Select Id, Name from Account];
            List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY Name];
            
            Onboarding__c onBDObj1 = utilityHelperTest.createonboarding(accObjList[0].Id, oppObjList[0].Id);
            onBDObj1.Live_Date__c = System.today();
            onBDObj1.OB_Discovery_Call_Date__c = System.today();
            insert new List<Onboarding__c> {onBDObj1};
            
            test.startTest();
            Map<String, String> response = ST_ObjectDetailsController.getObBDDetails(oppObjList[0].Id);
            // string OBDate = onBDObj1.OB_Discovery_Call_Date__c.month() + '/' + onBDObj1.OB_Discovery_Call_Date__c.day() + '/' + onBDObj1.OB_Discovery_Call_Date__c.year();
            //onBDObj1.OB_Discovery_Call_Date__c.month() + '/' + onBDObj1.OB_Discovery_Call_Date__c.Live_Date__c.day() + '/' + onBDObj1.OB_Discovery_Call_Date__c.Live_Date__c.year()
            //string OBLiveDate = onBDObj1.Live_Date__c.month() + '/' + onBDObj1.Live_Date__c.day() + '/' + onBDObj1.Live_Date__c.year();
            System.assertEquals(String.valueOf(System.today()), response.get('OB Discovery Call Date'));
            //System.assertEquals(String.valueOf(onBDObj1.Live_Date__c), response.get('OB Live Date'));
            System.assertEquals(String.valueOf(System.today()), response.get('OB Discovery Call Date'));
            test.stopTest();
        }
    }
    
    @isTest static void testGetObBDDetailsNegative() {
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1};
        
        System.runAs (userObj1) {
            List<Account> accObjList = [Select Id, Name from Account];
            List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY Name];
            
            test.startTest();
            Map<String, String> response = ST_ObjectDetailsController.getObBDDetails('recordId');
            System.assertEquals(null, response.get('OB Discovery Call Date'));
            System.assertEquals(null, response.get('OB Live Date'));
            test.stopTest();
        }
    }
}