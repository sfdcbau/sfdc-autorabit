/*
Test class for new CRP record creation after Customer Success Story is approved. 
@class :- ST_CustomerSuccessStoryCRPCreation
Creating all the required data for the test class   
@Method :- insertNewCRPRecord
@Jira User Story  SC-26
*/
//---------------------------------------------------------------------------------------------------

@isTest
public class ST_CustomerSuccessStoryCRPCreation_Test {
    
    @testSetup static void setup() {
        Account testAccount = new Account();
        testAccount.name = 'test account';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Role__c = 'Owner';
        insert testContact;
        
        Success_Story__c css =new Success_Story__c();
        css.Account__c = testAccount.Id;
        css.Contact__c = testContact.Id;
        css.Status__c = 'New';
        css.Business_Problem_Compelling_Event__c = 'test events';
        css.Business_Outcomes_Metrics_Achieved__c = 'test outcomes';
        css.Adoption_Journey__c = 'test journey';
        css.Lessons_Learned__c = 'test lessons';
     //   css.Solution_Overview__c = 'test solutions';
        insert css;
        
    }
    //test method if CRP record exists for the nominator contact.......
    @isTest static void testContactExist() {
        
        Account eachAcc = [SELECT Id FROM Account LIMIT 1];
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        Success_Story__c eachcss = [SELECT Id FROM Success_Story__c LIMIT 1];
        
        Customer_Reference_Program__c  testcrp = new Customer_Reference_Program__c(); 
        testcrp.CRP_Status__c = 'Approved';
        testcrp.Account__c = eachAcc.Id;
        testcrp.Contact__c = eachCon.Id;
        insert testcrp;
        
        Success_Story__c csstoUpdate = [SELECT Id, Account__c, Contact__c,  Status__c, Adoption_Journey__c, Torch_Network__c FROM Success_Story__c WHERE Id =: eachcss.Id ];
        csstoUpdate.Status__c = 'Approved';
        
        Test.startTest();
        update csstoUpdate ;
        Test.stopTest();
        
        Success_Story__c cssUpdated = [SELECT Id, Account__c, Contact__c,  Status__c, Adoption_Journey__c, Torch_Network__c, Customer_Reference_Program_Id__c FROM Success_Story__c WHERE Id =: eachcss.Id ];
        //System.assertEquals(cssUpdated.Customer_Reference_Program_Id__c, testcrp.Id);
        
    }
    //testmethod if new CRP record created for success story....
    @isTest static void testInsertNewCRP() {
        Success_Story__c eachcss = [SELECT Id FROM Success_Story__c LIMIT 1]; 
        Success_Story__c csstoUpdate = [SELECT Account__c, Contact__c,  Status__c, Adoption_Journey__c, Torch_Network__c FROM Success_Story__c WHERE Id =: eachcss.Id ];
        csstoUpdate.Status__c = 'Approved';
        
        Test.startTest();
        update csstoUpdate ;
        Test.stopTest();
        Success_Story__c cssUpdated = [SELECT Account__c, Contact__c,  Status__c, Adoption_Journey__c, Torch_Network__c, Customer_Reference_Program_Id__c FROM Success_Story__c WHERE Id =: eachcss.Id ];
        //System.assertNotEquals(cssUpdated.Customer_Reference_Program_Id__c, null);
    }
}