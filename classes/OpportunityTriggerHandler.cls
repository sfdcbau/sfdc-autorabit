/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* This is a handler class for opportunityTrigger.
*
* This class automatically creates Onboarding and asset records when Opportunity Stage is Closed-Won 
* It assign backs the created asset to the OLI.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jay Prasad   <jprasd@servicetitan.com>
* @version        1.2
* @created        04-30-2018
* @Updated-V.1.1  10/10/2018
* @Updated-V.1.2  12/27/2018
* @Name           OpportunityTriggerHandler
* @Method         handleInsertAsset()
* @TestClass      OpportunityTriggerHandlerTest
* @Trigger  
* @Version        1.3
* @Modified Date  03/17/2019
* @Modified Jira  SE-1952     
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class OpportunityTriggerHandler  {
    
    public static Boolean bool=false;     
    // 
    public static boolean firstRun = true;
    public static boolean isFirstRun(){
        if(firstRun){   
            firstRun = false;
            return true;
        }else{
            return firstRun;
        }
    }
    //If OPP RT = Payments : create Asset- Payments
    public static void handleInsertAsset( List<Opportunity> opps, Map<Id, Opportunity> oldMap){
        //Check if Trigger is switched ON
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Set<Id> opportunityIds = new Set<Id>();
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        for(Opportunity opp : opps){
            if(opp.stagename != oldMap.get(opp.Id).stagename && opp.RecordTypeId == OppRecordTypeId) {
                opportunityIds.add(opp.id);
            }
        }
        List<Trigger_Settings__mdt> TS =  new List<Trigger_Settings__mdt>();
        
        TS = [SELECT Label,isActive__c FROM Trigger_Settings__mdt where Label='ST_OpportunityTrigger' ];
        
        for (Trigger_Settings__mdt sett : TS){
            if(sett.isActive__c == TRUE){
                bool = true;
            }
        } 
        if(bool==true){
            
            // get the OLI list  for an OPPid along with RT detail, AccountId, 
            if(opportunityIds.size() > 0) {
                oliList = [SELECT Id, 
                           Product2Id,
                           Product2.Name,
                           Product2.Core_Product__c,
                           Name,
                           Tech_Count__c,
                           Tenant_Name__c,
                           OpportunityId, 
                           Asset__c,
                           Quantity,
                           Opportunity.AccountId,
                           opportunity.Name,
                           Opportunity.description,
                           Opportunity.StageName, 
                           Opportunity.recordtype.developername,
                           Opportunity.Managed_Tech_Quantity__c,
                           Opportunity.Primary_Contact__c,
                           Opportunity.OBDiscoveryCallDate__c,
                           Opportunity.ImplementationDate__c,
                           Opportunity.Onboarding_POC__c,
                           Opportunity.Account.Account_Level__c
                           FROM OpportunityLineItem 
                           WHERE OpportunityId IN :opportunityIds
                           AND Opportunity.hasOpportunitylineitem = true ];
            }
            
            
            List<Asset> astList = new list<Asset>();
            astList.clear();
            
            try{
                if(oliList.size()>0){
                    for(OpportunityLineItem oppLineItem : oliList){
                        
                        if((oppLineItem.Opportunity.StageName == 'Closed Won') && (((oppLineItem.Opportunity.RecordTypeId==Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId())&& oppLineItem.Product2.Core_Product__c== True) )){
                            
                            Asset asst = new Asset();
                            asst.Name = oppLineItem.Name;
                            asst.Product2Id = oppLineItem.Product2Id;
                            asst.AccountId = oppLineItem.Opportunity.AccountId;
                            asst.Opportunity__c = oppLineItem.OpportunityId;
                            if(oppLineItem.Opportunity.recordtype.developername == 'Sales' && oppLineItem.Product2.Core_Product__C == True ){
                                asst.Status = system.label.SalesAssetStatus;
                                asst.Managed_Tech_Count__c = oppLineItem.Opportunity.Managed_Tech_Quantity__c;
                                asst.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
                            }
                            astList.add(asst);
                        }
                    }
                }
                
                if(astList.Size()>0){
                    insert astList;  
                }  
                
            }
            catch(DMLException e){
                
            }
            
            //Update OLI with the asset created 
            
            if(astList.size()>0){
                for(Integer i=0; i<oliList.size();i++){
                    if(i<astList.size())
                        oliList[i].Asset__c =astList[i].id;
                }
            } 
            if(oliList.Size()>0){
                update oliList;
            }
            
            //Create OB objects    
            List<Onboarding__c> obList = new List<Onboarding__c>();
            Group QueueID = [Select ID from Group where Type = 'Queue' and name = 'ST_Onboarding_Queue'];
            
            for(OpportunityLineItem olsSales : oliList){
                if(olsSales.Opportunity.StageName == 'Closed Won' && olsSales.Opportunity.recordtype.developername == 'Sales' && olsSales.Product2.Core_Product__C==True ){
                    
                    Onboarding__c obSales = new Onboarding__c();
                    obSales.Account__c = olsSales.Opportunity.AccountId;
                    obSales.Opportunity__c = olsSales.OpportunityId ;
                    ObSales.Primary_Contact__c = olsSales.Opportunity.Onboarding_POC__c;
                    obSales.Name = System.label.OnboardingName +olsSales.Product2.Name +'-'+DATE.today().format();
                    obSales.Onboarding_Status__c = System.label.OnboardingStatus;               
                    obSales.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId(); 
                    obSales.OwnerId = QueueID.id;
                    obSales.Implementation_Start_Date__c = olsSales.Opportunity.ImplementationDate__c;
                    obSales.OB_Discovery_Call_Date__c = olsSales.Opportunity.OBDiscoveryCallDate__c;
                    obSales.Managed_Tech__c = olsSales.Tech_Count__c;
                    // SE-1952: New Field and automation on Onboarding object to capture CX Account Level during onboarding
                    obSales.OB_Account_Level__c = olsSales.Opportunity.Account.Account_Level__c;
                    obList.add(obSales);
                }
            }
            //System.debug('@@OBList'+ obList.Size());
            
            if(obList.Size()>0){
                insert obList;
            }
            if(astList.size()>0){
                for(Integer j=0; j<obList.size();j++){
                    if(!Test.isRunningTest())
                        obList[j].Asset__c =astList[j].id;
                }
            } 
            if(obList.size()>0){
                update obList;
            }
            if(obList.size()>0){
                for(Integer K=0; K<astList.size(); K++){
                    if(K<obList.Size())
                        astList[K].Onboarding__c =obList[K].id;
                }
            } 
            if(astList.Size()>0){
                if(!Test.isRunningTest())
                update astList;
            }
            
        }
        
    }
    
    
}