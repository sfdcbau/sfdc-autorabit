@isTest
private class ST_AddPaymentOpportunityControllerTest
{
    @isTest
    static void TestisPaymentOpportunityExistNegative()
    {
        Account account = new Account();
        account.Name='Test Account' ;
        insert account;
        
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity opportunity = new Opportunity (Name='Opp1',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId , CloseDate=Date.today() ,AccountId = account.id, ImplementationDate__c= System.today()+2);
        insert opportunity;
        
        boolean isPaymentOpportunityExist = ST_AddPaymentOpportunityController.isPaymentOpportunityExist(account.Id);
        System.assertEquals(false, isPaymentOpportunityExist );
    }
    
    @isTest
    static void TestisPaymentOpportunityExist()
    {
        Account account = new Account();
        account.Name='Test Account' ;
        insert account;
        
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        Opportunity opportunity = new Opportunity (Name='Opp1',StageName='Closed Lost', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId , CloseDate=Date.today() ,AccountId = account.id, ImplementationDate__c= System.today()+2);
        insert opportunity;
        
        boolean isPaymentOpportunityExist = ST_AddPaymentOpportunityController.isPaymentOpportunityExist(account.Id);
        System.assertEquals(false, isPaymentOpportunityExist );
    }
    
    @isTest
    static void TestisPaymentOpportunityExistClosedWon()
    {
        Account account = new Account();
        account.Name='Test Account' ;
        insert account;
        
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        Opportunity opportunity = new Opportunity (Name='Opp1',StageName='Prospect', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId , CloseDate=Date.today() ,AccountId = account.id, ImplementationDate__c= System.today()+2);
        insert opportunity;
        
        boolean isPaymentOpportunityExist = ST_AddPaymentOpportunityController.isPaymentOpportunityExist(account.Id);
        System.assertEquals(true, isPaymentOpportunityExist );
    }
    
    @isTest
    static void TestisPaymentOpportunityExistClosedWonMultiple()
    {
        Account account = new Account();
        account.Name='Test Account' ;
        insert account;
        
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ServiceTitan Payments').getRecordTypeId();
        Opportunity opportunity = new Opportunity (Name='Opp1',StageName='Closed Lost', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId , CloseDate=Date.today() ,AccountId = account.id, ImplementationDate__c= System.today()+2);
        Opportunity opportunity1 = new Opportunity (Name='Opp2',StageName='Closed Lost', OBDiscoveryCallDate__c = system.today(),RecordTypeId= opportunityRecordTypeId , CloseDate=Date.today() ,AccountId = account.id, ImplementationDate__c= System.today()+2);

        insert opportunity;
        
        boolean isPaymentOpportunityExist = ST_AddPaymentOpportunityController.isPaymentOpportunityExist(account.Id);
        System.assertEquals(false, isPaymentOpportunityExist );
    }
}