/*--------------------------------------------------------------------------------------------------------------------------------
Test class for CRP_ExternalNominationController class. 
@Class :- CRP_ExternalNominationController
@Method :- testExternalRequestInsert
@Jira User Story  SC-1
@author  : Kamal

----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CRP_ExternalNominationControllerTest {
    
    @istest
    //test method for request if request alrady created before......... 
    static void testExternalRequest(){ 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();
        
        Reference_Request__c refRequestOld = new Reference_Request__c(); 
        refRequestOld.Reference_Status__c = 'New'; 
        refRequestOld.First_Name__c = 'test';
        refRequestOld.Last_Name__c = 'CRP';
        refRequestOld.Email__c = 'testemail@mail.com';
        refRequestOld.RecordTypeId = recordTypeId;
        insert refRequestOld;
        
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.First_Name__c = 'External';
        refRequest.Last_Name__c = 'CRP';
        refRequest.Email__c = 'testemail@mail.com';
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
         
        
        Reference_Request__c[] refRequests = [SELECT Id, First_Name__c, Last_Name__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests.size(), 1); 
        }   
    }
    
    @istest
    //test method for request creation without contact match......... 
    static void testExternalRequestInsert(){ 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.First_Name__c = 'External';
        refRequest.Last_Name__c = 'CRP';
        refRequest.Email__c = 'testemail@mail.com';
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
         
        
        Reference_Request__c[] refRequests = [SELECT Id, First_Name__c, Last_Name__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].First_Name__c, 'External');
            System.assertEquals(refRequests[0].Last_Name__c, 'CRP');
            System.assertEquals(refRequests[0].Email__c, 'testemail@mail.com');
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId); 
        }   
    }
    
     @istest
    //test method for request creation when a contact matches......... 
    static void testRequestInsertwithContact(){ 
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Email = 'testemail@mail.com';
        testContact.Role__c = 'Owner';
        insert testContact; 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.First_Name__c = 'External';
        refRequest.Last_Name__c = 'CRP';
        refRequest.Email__c = 'testemail@mail.com';
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
         
        
        Reference_Request__c[] refRequests = [SELECT Id, First_Name__c, Last_Name__c, Contact__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].Contact__c, testContact.Id); 
            System.assertEquals(refRequests[0].Email__c, 'testemail@mail.com');
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId); 
        }   
    }
    
    @istest
    //test method for request creation when a contact matches and CRP exists......... 
    static void testRequestInsertwithExistingCRP(){ 
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Email = 'testemail@mail.com';
        testContact.Role__c = 'Owner';
        insert testContact; 
        
        Customer_Reference_Program__c  testcrp = new Customer_Reference_Program__c(); 
        testcrp.CRP_Status__c = 'Approved';
        testcrp.Account__c = testAccount.Id;
        testcrp.Contact__c = testContact.Id;
        insert testcrp;
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.First_Name__c = 'External';
        refRequest.Last_Name__c = 'CRP';
        refRequest.Email__c = 'testemail@mail.com';
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
         
        
        Reference_Request__c[] refRequests = [SELECT Id, First_Name__c, Last_Name__c, Contact__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].Contact__c, testContact.Id); 
            System.assertEquals(refRequests[0].Email__c, 'testemail@mail.com');
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId); 
        }   
    }
    
    @istest
    //test method for request creation when multiple contact matches......... 
    static void testRequestwithMultipleCons(){ 
        
        Account testAccount = new Account();
        testAccount.name = 'test account';
        testAccount.Customer_Status_picklist__c = 'Onboarding';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Email = 'testemail@mail.com';
        testContact.Role__c = 'Owner';
        insert testContact;
        
        Contact testContactDupe = new Contact();
        testContactDupe.LastName = 'test contactDupe';
        testContactDupe.AccountId = testAccount.Id;
        testContactDupe.Email = 'testemail@mail.com';
        testContactDupe.Role__c = 'Owner';
        insert testContactDupe;
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.First_Name__c = 'External';
        refRequest.Last_Name__c = 'CRP';
        refRequest.Email__c = 'testemail@mail.com';
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
         
        
        Reference_Request__c[] refRequests = [SELECT Id, First_Name__c, Last_Name__c, Contact__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests[0].Contact__c, null); 
            System.assertEquals(refRequests[0].Email__c, 'testemail@mail.com');
            System.assertEquals(refRequests[0].RecordTypeId , recordTypeId);
        }   
    }
    
    @istest
    //test method for request creation with invalid status......... 
    static void testExternalRequestInsertInvalidStatus(){ 
        
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'Completed'; 
        refRequest.First_Name__c = 'External';
        refRequest.Last_Name__c = 'CRP';
        refRequest.Email__c = 'testemail@mail.com';
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
        
        Reference_Request__c[] refRequests = [SELECT Id, Reference_Status__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertNotEquals(refRequests[0].Reference_Status__c, 'Completed'); 
            System.assertEquals(refRequests[0].Email__c, 'testemail@mail.com'); 
        }   
    }
    @istest
     static void negativeTesting(){ 
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();
        
        
        CRP_ExternalNominationController externalController = new CRP_ExternalNominationController();
        
        Reference_Request__c refRequest = new Reference_Request__c(); 
        refRequest.Reference_Status__c = 'New'; 
        refRequest.Company_Name__c = 'Test Company';
                
        externalController.recRefRequest = refRequest;
        externalController.save();   //Invoking save method.....
        
        externalController.isPhoneNumberInvalid = false;
        externalController.requestNumberLength = false;
        
        Reference_Request__c[] refRequests = [SELECT Id, First_Name__c, Last_Name__c, Email__c, RecordTypeId FROM Reference_Request__c];
        
        if(refRequests.size()>0){
            //To verify inserted values......
            System.assertEquals(refRequests.size(), 1); 
        }   
    }
     
}