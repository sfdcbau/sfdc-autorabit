/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* This is an controller extension class used for Visualforce page 'ST_NominationInternal' with Contact standard controller
* This is called from Nominate button on contact record detail page
* This class creates a Reference Request record when user enter all required details
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* @version          1.0
* @created          12-02-2018
* @Jira User Story  SC-3
* ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
* Test Class: ST_NominationInternalController_Test
*/
public class ST_NominationInternalController {
    public String selectedValue{get;set;} 
    public Boolean isRadioBtnSelected{get;set;}
    public Boolean isNomination{get;set;}
    public Boolean isSuccessStory{get;set;} 
    public Boolean isAccountStatusValid{get;set;}
    public boolean displayPopup {get; set;} 
    public String successMessage{get;set;} 
    public String recContactId{get;set;}
    public Contact recContact{get;set;}
    public List<Reference_Request__c> listofContactHasRequest{get;set;}
    public Customer_Reference_Details__c recRefDetails{get;set;}
    public List<Customer_Reference_Details__c> lstDetails = new List<Customer_Reference_Details__c>();
    public List<Success_Story__c> lstStory = new List<Success_Story__c>();
    public Boolean isSubmittedBefore{get;set;}
    public Reference_Request__c recRefRequest{get;set;}
    public Success_Story__c recCustomerSuccessStory{get;set;}
    public String requestStatus{get;set;}
    public String customerSuccessStoryStatus{get;set;}
    public Boolean IsSaveSuccessfull {get;set;}  
    
    // Constructer declaration....
    public ST_NominationInternalController(ApexPages.StandardController std ){
        selectedValue = '';
        customerSuccessStoryStatus = 'Draft';
        requestStatus = 'New';
        recContactId = ApexPages.currentPage().getparameters().get('id');
        recRefDetails = new Customer_Reference_Details__c();
        recRefRequest = new Reference_Request__c();
        recCustomerSuccessStory = new Success_Story__c();
        Id reqRecordTypeId;
        reqRecordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceInternalRecordType).getRecordTypeId();
        
        String accCustomerStatus = Label.Account_Customer_Status;
        List<String> lstAccCustomerStatus = accCustomerStatus.split(',');
        
        recContact = [SELECT Id, FirstName, LastName, Name, Email, AccountId, Account.Name, Account.Customer_Status_picklist__c FROM Contact WHERE Id = :recContactId];
        
        listofContactHasRequest = [SELECT Id, Contact__c FROM Reference_Request__c WHERE Contact__c =:recContactId AND RecordTypeId =:reqRecordTypeId];
        
        //to check if contact-account status is onboarding, live or success....
        if((recContact.Account.Customer_Status_picklist__c != null || recContact.Account.Customer_Status_picklist__c !='') &&
           (lstAccCustomerStatus.contains(String.valueOf(recContact.Account.Customer_Status_picklist__c))  )){
               
               isAccountStatusValid = true;
               //to check if contact already nominated...
               if(listofContactHasRequest !=null && !listofContactHasRequest.isEmpty()){                                  
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Nomination Request for this contact has been submitted before, Please check Reference Requests. Please select Success Story or close the window!'));
                   isSubmittedBefore = true;
               }
           }
        else{
            isAccountStatusValid = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'To nominate the contact for Customer Reference Program, Contact-Account status should be Onboarding, Live or Success.'));  
        } 
    }
    public void Submit(){         
        if(selectedValue == '1'){
            isNomination = true;
            isSuccessStory = false;
        }else if(selectedValue == '2'){
            isNomination = false;
            isSuccessStory = true; 
        }
        isRadioBtnSelected = true;   
    }
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
    //Standard save funcion....
    public void save() {  
        Id requestRecordTypeId;
        IsSaveSuccessfull = true;
        if(Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceInternalRecordType) != null ){
            //getting recordtype for Reference Request custom object
            requestRecordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceInternalRecordType).getRecordTypeId();   
        }
        // nomination object recRefDetails......
        
        if(selectedValue == '1'){
            // inserting Customer Reference Details record......
            if(recRefDetails.Nomination_Comments__c != Null && recRefDetails.Goals_Realized__c != null) {
                try{
                    lstDetails.add(recRefDetails);
                    if(lstDetails != null && lstDetails.size() > 0 ){
                        insert lstDetails;
                        successMessage = label.Request_Submit_Message;
                    }
                    
                    System.debug('Request submitted successfully.');
                }     
                catch(DMLException ex){
                    System.debug('The following exception has occurred: ' + ex.getMessage());
                    IsSaveSuccessfull = false;
                } 
                
                if(requestRecordTypeId != null || requestRecordTypeId != ''){
                    recRefRequest.RecordTypeId = requestRecordTypeId;
                }
                // inserting Reference Request record......
                recRefRequest.Account__c = recContact.AccountId;
                recRefRequest.First_Name__c = recContact.FirstName;
                recRefRequest.Last_Name__c = recContact.LastName;
                recRefRequest.Email__c = recContact.Email;
                recRefRequest.Reference_Status__c = 'New';
                recRefRequest.Contact__c = recContactId;        
                recRefRequest.Customer_Reference_Detail_Id__c = recRefDetails.Id;
                if(recRefRequest != null){
                    try{
                        insert recRefRequest;
                        IsSaveSuccessfull = true;
                    }
                    catch(Exception ex){
                        system.debug('Exception is ::' + ex.getMessage());
                    }
                }    
                else{ IsSaveSuccessfull = false;
                    }
            }
        }
        // inserting Customer Success Story record......
        else if(selectedValue == '2'){ 
            recCustomerSuccessStory.Contact__c = recContactId;
            recCustomerSuccessStory.Account__c = recContact.AccountId;
            recCustomerSuccessStory.Status__c = 'Draft';
            lstStory.add(recCustomerSuccessStory); 
            if(lstStory != null && lstStory.size() > 0){
                
                try{ 
                    insert lstStory;
                    IsSaveSuccessfull = true;
                    successMessage = label.Request_Submit_Message;
                }
                catch(Exception ex){
                    system.debug('Exception is :' + ex.getMessage());
                }
            } else { IsSaveSuccessfull = false;
                   }
        }
        else {
            
        }
    }
}