/***********************************************
* @author       : Mukul Kumar
* @testclass    : ST_OBDiscoveryCallDateTest
* @Description  : This class is to update the OB Discovery Call Date on OPP when ever an event is created with Subject as Onboarding Discovery
*                 Call Calendy. This class will be called by an Process Builder.
* @PB Name      : Calendly_Event_Updates_Onboarder_Field
* @Version      : V 1.0
* @ModifiedDate : 03/27/2019
* @ModifiedBy   : Tharun G
* @Version      : V 1.1
* @Jira         : SE-2925
* @ModifiedDate : 04/11/2019
* @ModifiedBy   : Tharun G
* @Version      : V 1.2
* @Jira         : SE-2865
* 
* *********************************************/
public class ST_OBDiscoveryCallDate {
    @InvocableMethod
    public static void PopulateOBDiscoveryCall(list<Event> eventlist){
        set<Id> whoIdlist = new set<Id>();
        set<Id> AccId = new set<Id>();
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        list<Opportunity> updateopportunity = new list<Opportunity>();
        map<Id, Opportunity> OpportunityMap = new map<Id, Opportunity>();
        string Subjectlabel = label.MSAErrorEmailSubject;
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply_salesforce@servicetitan.com'];
        List<Messaging.SingleEmailMessage> listofEmailsToSend = new List<Messaging.SingleEmailMessage>();
        
        
        Date updateEventdate ;
        string whoid;
        for(Event evt : eventlist) {
            whoIdlist.add(evt.WhoId);
            updateEventdate = evt.ActivityDate;
            whoid = evt.id;
        }
        list<Contact> contactlist = [Select Id,Name, AccountId, Account.Name, Account.customer_status_picklist__c from contact where Id IN : whoIdlist];
        //System.debug('@@contactList'+contactlist);
        
        for(Contact acc : contactlist) {
            // Check for Account Status
            if(acc.Account.customer_status_picklist__c == 'Stage Zero' || acc.Account.customer_status_picklist__c == 'CC Processed' || acc.Account.customer_status_picklist__c == 'SE Vetting' || acc.Account.customer_status_picklist__c == 'Discovery Call'
               || acc.Account.customer_status_picklist__c == 'Q&A / DEMO' || acc.Account.customer_status_picklist__c == 'Pricing / ROI Assessment' || acc.Account.customer_status_picklist__c == 'Negotiation / Pending' || acc.Account.customer_status_picklist__c == 'Onboarding'){
                   AccId.add(acc.Accountid);
               }
            else{
                // store all the varaibles and url's in strings to use the email body
                string accountName = acc.Account.Name;
                string contactName = acc.Name;
                string accountUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+ acc.AccountId;
                String contactUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+ acc.Id;
                string eventUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/'+ whoId; 
                string customerStatus = acc.Account.customer_status_picklist__c;   
                String htmlBody= label.MSADescription +'\n'+'\n' +label.AccountName+ ' ' + accountName + '\n'+label.CustomerStatus +' '+ customerStatus  + '\n' +  label.EventLink +' '+eventUrl +'\n'+ label.ContactName + ' '+ contactName + '\n'+ label.MSAContactLink + ' '+ contactUrl;
                
                
                // create the email meesgae object to send the email
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                List<String> emailsToSend = label.MSAErrorNotfEmails.split(',');
                mail.setToAddresses(emailsToSend);
                mail.setUseSignature(false); 
                mail.setBccSender(false);
                if(owea.size()>0)
                {
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);
                } 
                mail.setSaveAsActivity(false);
                mail.setSubject(Subjectlabel);
                mail.setPlainTextBody(htmlBody);
                listofEmailsToSend.add(mail);
            }
            
        }
        //system.debug('@@accId'+accId);
        if(AccId.size() > 0) {
            Opportunity opplist = [Select id, name, AccountId, OBDiscoveryCallDate__c from Opportunity where AccountId IN :AccId AND RecordTypeId = : OppRecordTypeId order by createdDate desc limit 1];
            opplist.OBDiscoveryCallDate__c = updateEventdate;
            update opplist;
        }
        // adding isRunningTest beacause to skip the deplyement error "NO MASS EMAIL PERMISSION"
        if(listofEmailsToSend.size()>0 && !(Test.isRunningTest()) ){
            Messaging.sendEmail(listofEmailsToSend);    
        }
    }
    
}