/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 05 Feb, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 05 Feb, 2019
 # Description...................: This is test class for ST_EnterpriseRejectionRates class.
 #####################################################
*/
@isTest
private class ST_EnterpriseRejectionRatesTest {
    /**
     * @author : Aman Gupta
     * @date : 05 Feb, 2019
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        // insert Product2
        Product2 prodObj1 = utilityHelperTest.createProduct();
        prodObj1.name = 'Premier Package';
        prodObj1.Core_Product__c = true;
        
        Product2 prodObj2 = utilityHelperTest.createProduct();
        prodObj2.name = 'PriceBook';
        prodObj2.Product_Type__c = 'Add-on';
        insert new List<Product2> {prodObj1, prodObj2};
        
        // insert Pricebook2
        Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
        insert new List<Pricebook2> {custPBObj};
            
        // insert PricebookEntry
        PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
        PricebookEntry pbeObj2 = utilityHelperTest.craeteStandardPBE2(prodObj2.Id);
        insert new List<PricebookEntry> {pbeObj1, pbeObj2};
        
        // insert Account
        Account accObj = utilityHelperTest.createAccount();
		accObj.Customer_Status_picklist__c ='Stage Zero';
        insert new List<Account> {accObj};
            
        // insert Contact
        Contact conObj = utilityHelperTest.createContact(accObj.Id);
        conObj.Role__c = 'Co-Owner';
        insert new List<Contact> {conObj};
            
        // insert Opportunity
        Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj1.Name = 'Test Opportunity OB Rej1'; 
        Opportunity oppObj2 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj1.Name = 'Test Opportunity OB Rej2';
        insert new List<Opportunity> {oppObj1, oppObj2};
        
        // inserting OpportunityContactRole
        OpportunityContactRole oppConRoleObj1 = utilityHelperTest.createOppContactRole(oppObj1.Id, 'Co-Owner', conObj.Id);
        OpportunityContactRole oppConRoleObj2 = utilityHelperTest.createOppContactRole(oppObj2.Id, 'Co-Owner', conObj.Id);
        insert new List<OpportunityContactRole> {oppConRoleObj1, oppConRoleObj2};
        
        // inserting OpportunityLineItem for add-on product
        OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
        OpportunityLineItem oppLIObj2 = utilityHelperTest.createOppLineItem(oppObj2.Id, prodObj1.Id, 45.00, 36);
        insert oppLIObj1;
        insert oppLIObj2;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 05 Feb, 2019
     * @description : This method is to test ST_EnterpriseRejectionRate getTeamMembers function.
    */
    @isTest static void testGetTeamMembersPositive() {
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        List<UserRole> rObj= [SELECT Id FROM UserRole WHERE Name = 'Sales Manager'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj3 = new User(ProfileId = pObj[0].Id, UserRoleId = rObj[0].Id, LastName = 'TestUser1113', Email = 'testuser1113@test.com', Username = 'Test.User1113@test.com', CompanyName = 'TestCom3', Title = 'TestTitle3', Alias = 'TU1113', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1, userObj2, userObj3};
        
        System.runAs (userObj1) {
            Account accObj = [Select Id, Name from Account LIMIT 1];
            
            List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY Name];
            oppObjList[0].SEOwner__C = userObj3.Id;
            oppObjList[1].SEOwner__C = userObj3.Id;
            update oppObjList;
            
            OpportunityTeamMember oppTMObj1 = utilityHelperTest.createOppTeamMem(oppObjList[0].Id, userObj1.Id, 'Sales Engineer');
            OpportunityTeamMember oppTMObj2 = utilityHelperTest.createOppTeamMem(oppObjList[0].Id, userObj2.Id, 'Sales Engineer');
            OpportunityTeamMember oppTMObj3 = utilityHelperTest.createOppTeamMem(oppObjList[1].Id, userObj1.Id, 'Sales Engineer');
            OpportunityTeamMember oppTMObj4 = utilityHelperTest.createOppTeamMem(oppObjList[1].Id, userObj2.Id, 'Sales Engineer');
            // insert OpportunityTeamMember
            insert new List<OpportunityTeamMember> {oppTMObj1, oppTMObj2, oppTMObj3, oppTMObj4};
                
            // updating Opportunity records with Stage = 'Closed Won'
            oppObjList[0].StageName = 'Closed Won';
            oppObjList[1].StageName = 'Closed Won';
            
            update new List<Opportunity> (oppObjList);
            
            test.startTest();
            // inserting ST_Onboarding Onboarding__c
            Onboarding__c onBDObj1 = utilityHelperTest.createonboarding(accObj.Id, oppObjList[0].Id);
            onBDObj1.Onboarding_Status__c = 'Rejected';
            onBDObj1.Rejection_Date__c = System.today().addDays(30);
                
            Onboarding__c onBDObj2 = utilityHelperTest.createonboarding(accObj.Id, oppObjList[1].Id);
            onBDObj2.Onboarding_Status__c = 'Rejected';
            onBDObj2.Rejection_Date__c = System.today().addDays(30);
            
            List<Onboarding__c> onBDObjList = new List<Onboarding__c> {onBDObj1, onBDObj2};
            insert onBDObjList;
            
            Date startDate = System.today().addDays(-30);
            Date endDate = System.today().addDays(60);
            List<ST_EnterpriseRejectionRates.TeamMemberWrapper> teamMemWrapObjList = ST_EnterpriseRejectionRates.getTeamMembers(String.valueOf(startDate), String.valueOf(endDate), 'Rejected', 'Sales Engineer', 24);
            
            
            List<OpportunityLineItem> oppLIObjList = new List<OpportunityLineItem> ();
            for(OpportunityLineItem oppLIObj : [Select Id, Name from OpportunityLineItem]) {
                oppLIObj.Recurring_Revenue__c = true;
                oppLIObj.Projected_Quantity__c = 35;
                oppLIObjList.add(oppLIObj);
            }
            update oppLIObjList[0];
            update oppLIObjList[1];
            
            teamMemWrapObjList = ST_EnterpriseRejectionRates.getTeamMembers(String.valueOf(startDate), String.valueOf(endDate), 'Rejected', 'Sales Engineer', 24);
            
            test.stopTest();
        }
    }
    
    @isTest static void testGetTeamMembersNegative() {
        Date startDate = System.today().addDays(-30);
        Date endDate = System.today().addDays(60);
            
        List<ST_EnterpriseRejectionRates.TeamMemberWrapper> teamMemWrapObjList = ST_EnterpriseRejectionRates.getTeamMembers(String.valueOf(startDate), String.valueOf(endDate), 'Rejected', 'Sales Engineer', 24);
        System.assertEquals(0, teamMemWrapObjList.size());
    }
}