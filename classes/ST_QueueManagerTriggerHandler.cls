/* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Jay Prasad   <jprasd@servicetitan.com>
* @version        1.0
* @created        05-29-2018
* @Name           ST_QueueManagerTriggerHandler
* @Method         handleInsertQM()
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class ST_QueueManagerTriggerHandler{

private static Boolean bool=false;

	public static void handleUpdateOppsOwner( List<QueueManager__c> qm){
		
		List<Trigger_Settings__mdt> TS =  new List<Trigger_Settings__mdt>();
      	
      	TS = [SELECT Label,isActive__c FROM Trigger_Settings__mdt where Label='ST_QueueManagerTrigger' ];
       	
       	for (Trigger_Settings__mdt sett : TS){
        	if(sett.isActive__c == TRUE){
        		bool = true;
        	}
		}

		
		if(bool==true){
			//create Opp list for update
			List<Opportunity> OppUpdateList = new List<Opportunity>();
			List<Opportunity> OppTempList = new List<Opportunity>();

			//get Opp ids for each QM records.
			for(QueueManager__c q: qm){
				OppTempList =[SELECT id from Opportunity where Id =:q.Opportunity__c];
				for(Opportunity o :OppTempList){
					o.OwnerId = q.OwnerId;
					OppUpdateList.add(o);
				}
			}
			

			if(!OppUpdateList.isEmpty()) {
				update OppUpdateList;
			}

			system.debug('=====>'+OppUpdateList);

		}
	}




}