/***********************************************
 * @testclass : TalkdeskGainsightLogActivityTest

 * *********************************************/
 
public class TalkdeskGainsightLogActivity {
    @Future(callout=true)
    public static void gsCreateActivity(id a){
        List<Talkdesk_Gainsight_Settings__mdt > tcs = new List<Talkdesk_Gainsight_Settings__mdt >([SELECT gsServiceURL__c, activityTypeId__c, appOrgId__c, appSessionID__c, appUserID__c, contentType__c, csOrgURL__c, Gainsight_URL_Base__c  
                                                                                   FROM Talkdesk_Gainsight_Settings__mdt 
                                                                                   Limit 1]);
        
        List<talkdesk__Talkdesk_Activity__c> activity = new List<talkdesk__Talkdesk_Activity__c>([SELECT Name, Talkdesk__User__r.Name, Talkdesk__User__r.email, talkdesk__User__c, Talkdesk__Start_Time__c, Talkdesk__Notes__c, Talkdesk__Disposition_Code__c, Talkdesk__Talkdesk_id__c, Talkdesk__Account__r.Name, Talkdesk__Account__c
                                                                                                  FROM talkdesk__Talkdesk_Activity__c
                                                                                                  WHERE ID=:a                                                                                            
                                                                                                  Limit 1
                                                                                                 ]);
        if(activity.size()>0){ 
            system.debug('Custom Settings: '+tcs);
            system.debug('Activity: '+a);
            if(tcs.size()>0){
                HttpRequest req = new HttpRequest();
                req.setEndpoint(tcs[0].gsServiceURL__c);
                req.setMethod('POST');
                JSONGenerator gen = JSON.createGenerator(true);                                                     
                // Custom Settings
                String contentType = tcs[0].contentType__c ;
                String appOrgId = tcs[0].appOrgId__c;
                String appSessionID = tcs[0].appSessionID__c;
                String appUserID = tcs[0].appUserID__c;
                String csOrgURL = tcs[0].csOrgURL__c ;
                String activityTypeId = tcs[0].activityTypeId__c ;
                String antCallType;
                
                List<Talkdesk_GS_Call_Types__c> callType = new List<Talkdesk_GS_Call_Types__c>([SELECT Name, Gainsight_ID__c 
                                                                                                FROM Talkdesk_GS_Call_Types__c
                                                                                                WHERE Name=:activity[0].Talkdesk__Disposition_Code__c                                                                                            
                                                                                                Limit 1
                                                                                               ]);
                if(callType.size()>0){
                    antCallType = callType[0].Gainsight_ID__c;
                }
                else{
                    for(Talkdesk_GS_Call_Types__c gsc:[SELECT Name, Gainsight_ID__c 
                                                       FROM Talkdesk_GS_Call_Types__c
                                                       WHERE Name=:'Success Call'                                                                                            
                                                       Limit 1]){
 
                        antCallType = gsc.Gainsight_ID__c;
                    }
                }
                req.setHeader('Content-Type', contentType);
                req.setHeader('appOrgId', appOrgId);
                req.setHeader('Authorization', appSessionID);
                req.setHeader('appUserID', appUserID);
                
                // Create a new http object to send the request object 
                gen.writeStartObject();
                gen.writeFieldName('note');
                gen.writeStartObject();
                gen.writeFieldName('customFields');
                gen.writeStartObject();
                gen.writeFieldName('internalAttendees');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('id', '');
                gen.writeStringField('obj', '');
                gen.writeStringField('name', activity[0].Talkdesk__User__r.name);
                gen.writeStringField('email', activity[0].Talkdesk__User__r.email);
                gen.writeStringField('eid', activity[0].Talkdesk__User__c);
                gen.writeStringField('eobj', 'User');
                gen.writeStringField('epp', csOrgURL);
                gen.writeStringField('esys', 'SALESFORCE');
                gen.writeStringField('sys', 'GAINSIGHT');
                gen.writeStringField('pp', '');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeFieldName('externalAttendees');
                gen.writeStartArray();
                gen.writeEndArray();
                gen.writeStringField('ant__Call_Type', antCallType);
                gen.writeEndObject();
                gen.writeStringField('subject', activity[0].Name);
                gen.writeStringField('activityDate', string.valueof(datetime.valueOf(activity[0].Talkdesk__start_time__c).getTime()));
                gen.writeStringField('plainText', activity[0].Talkdesk__notes__c);
                gen.writeStringField('content', activity[0].Talkdesk__notes__c);
                gen.writeStringField('type', 'CALL');
                gen.writeEndObject();
                gen.writeFieldName('meta');
                gen.writeStartObject();
                gen.writeStringField('activityTypeId', activityTypeId);
                gen.writeStringField('ctaId', '');
                gen.writeStringField('source', 'C360');
                gen.writeEndObject();
                gen.writeFieldName('author');
                gen.writeStartObject();
                gen.writeStringField('id', '');
                gen.writeStringField('obj', '');
                gen.writeStringField('name', activity[0].Talkdesk__User__r.Name);
                gen.writeStringField('email', activity[0].Talkdesk__User__r.Email);
                gen.writeStringField('eid', activity[0].Talkdesk__User__c);
                gen.writeStringField('eobj', 'User');
                gen.writeStringField('epp', csOrgURL);
                gen.writeStringField('esys', 'SALESFORCE');
                gen.writeStringField('sys', 'GAINSIGHT');
                gen.writeStringField('pp', '');             
                gen.writeEndObject();          
                gen.writeStringField('syncedToSFDC', 'false'); 
                gen.writeStringField('id', activity[0].Talkdesk__Talkdesk_id__c); 
                gen.writeFieldName('tasks');
                gen.writeStartArray();
                gen.writeEndArray();
                gen.writeFieldName('attachments');
                gen.writeStartArray();
                gen.writeEndArray();
                gen.writeFieldName('contexts');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('id', ''); 
                gen.writeStringField('obj', 'Company'); 
                gen.writeStringField('eobj', 'Account');
                gen.writeStringField('eid', activity[0].Talkdesk__Account__c); 
                gen.writeStringField('esys', 'SALESFORCE');
                gen.writeStringField('lbl', activity[0].Talkdesk__Account__r.Name); 
                gen.writeStringField('dsp', 'true');
                gen.writeStringField('base', 'true'); 
                gen.writeEndObject(); 
                gen.writeEndArray();
                gen.writeEndObject();
                
                // Get the JSON string.
                String pretty = gen.getAsString();
                  
                // Send Body
                req.setBody(pretty);
                
                Http http = new Http();
                HttpResponse res = new HttpResponse();
                try{
                    //if(!Test.isRunningTest()){
                        res = http.send(req);
                        System.debug(res.toString());
                    //}
                }
                catch(System.CalloutException e) {
                    System.debug('GS error: '+ e);
                    System.debug(res.toString());
                }
             }
        }
    }
}