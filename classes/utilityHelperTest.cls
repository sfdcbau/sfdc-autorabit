public class utilityHelperTest{
    
    public static Lead createLead(){
        Lead leadObj1 = new Lead(Company = 'TestCom1',Firstname = 'FN1', Lastname = 'LN1', Status = 'Open');
        leadObj1.Estimated_Total_Potential_Office_Users__c = 1;
        leadObj1.No_of_Technicians__c = 1;
        
        return leadObj1;
    } 
        
        
    public static product2 createProduct(){
        Product2 prod = new Product2();
        prod.name = 'Test Product';
        prod.IsActive = true;
           
        return prod;
    } 
    
    public static PricebookEntry craeteStandardPBE2(String prodId){
        Id stdPriceBookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = stdPriceBookId, Product2Id =prodId,
            UnitPrice = 10000, IsActive = true);
        return standardPrice;
    }
    
    public static Pricebook2  craeteCustomPB2(){        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        return customPB ;   
    }    
    
    public static PricebookEntry createPricebookEntry ( Pricebook2 newPricebook, Product2 prod) {
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = newPricebook.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        return customPrice;  
    }
    
    public static account createAccount(){
        Account acc= new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        acc.name ='Service Titan Account';
        acc.Customer_Status_picklist__c = 'Success';
        acc.Estimated_Total_Potential_Technicians__c = 35;
        acc.BillingCity = 'Noida';
        acc.BillingCountry = 'India';
        acc.BillingState = 'Uttar pradesh';
        acc.CMRR_Total__c = 14400;
        acc.Industry__c ='Electrical';
        acc.Software_newpicklist__c = 'Acowin';
        acc.No_Known_Active_Partners__c = True;
        acc.Estimated_Total_Potential_Office_Users__c = 35;
        acc.Influencer__c  = 'Unknown';
        acc.CountofOwnerPOCContacts__c = 1;
        acc.Residential__c  = 50;
        acc.Pricing_Model__c ='New Pricing (Managed Techs)';
        acc.Tenant_ID__c = '233445';
        acc.Tenant_Name__c = 'testTenent';
        acc.Initial_Username__c = 'tenent123012';
        acc.Time_Zone__c = 'Pacific (UTC-8:00)';
        return acc;    
        
        
     }
    
     public static contact createContact(String accid){
        Contact con= new Contact();
        con.lastname = 'test';
        con.AccountId = accid;
        con.Role__c   ='POC';
        
        return con;    
    }
    
    public static opportunity createopportunity(String accid,String customPBid,String conId){
        Id OppRecordTypeId = Schema.SObjectType.opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity Opp = new Opportunity();
        
        Opp.Name = 'Test Opportunity ';
        Opp.accountid = accid;
        Opp.CloseDate = system.today()+30;          
        Opp.StageName = 'Stage Zero';
        Opp.RecordTypeId= OppRecordTypeId;
        Opp.Pricebook2id =customPBid;
        Opp.Address_Industry_Verified__c = true;
        Opp.DispositionCode__c = '';
        Opp.SubDisposition__c  ='';
        Opp.Competing_Software__c = 'AppointmentPlus';
        Opp.OBDiscoveryCallDate__c =date.today();
        Opp.ImplementationDate__c = date.today();
        Opp.Onboarding_POC__c     = conId;
        Opp.AE_Discovery_POC__c = conId;
        Opp.DataMigrationFee__c = 'Basic';
        Opp.TravelFee__c = 'Basic';
        Opp.AccountingMigrationFee__c = 'Basic';
        Opp.Trade_Organization_Network__c = 'IDA';
        Opp.Tracking_Inventory__c = 'Do not track inventory';
        Opp.Storing_and_Charge_Credit_Cards__c = 'Yes';
        Opp.Primary_Software__c = 'Acowin';
        Opp.Charging_Customers__c = 'Technicians always call the office and ask for pricing';
        Opp.Sales_Tax__c = 'No, does not charge sales tax to customers';
        Opp.Accepting_Credit_Cards__c = 'No, technicians do not accept any form of credit card payment';
        Opp.Technician_Compensation__c = 'Hourly pay';
        Opp.Quickbooks_Sync__c = 'No, using a different accounting software';
        Opp.Quickbooks_Version__c = 'Quickbooks Enterprise Desktop for PC';
        Opp.Memberships_and_Agreements__c = 'No';
        Opp.Customer_Invoicing__c = 'Technicians fill out paper invoices out in the field';
        Opp.GSPCredit__c  = 760;
        Opp.Special_Notes__c = 'Test';
        Opp.Trade_Organization_Network__c  = 'BDR';
        return Opp;             
    }
    
    public static OpportunityLineItem createOppProduct(string oppId,string pbeid,string prodID){
        OpportunityLineItem oppLineItem = new  OpportunityLineItem();
        oppLineItem.Quantity = 37;
        oppLineItem.OpportunityId = Oppid;
        oppLineItem.Product2id = prodID;
        oppLineItem.UnitPrice = 1200; 
        oppLineItem.PricebookEntryId = pbeid;
        oppLineItem.TotalPrice      = null;
        oppLineItem.Recurring_Revenue__c = true;
        oppLineItem.Projected_Quantity__c = 37;
        
        return oppLineItem;  
    }   
    
    
    public static asset createAsset(String oppid, string accid){  
        Id AsstRecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        asset Asst= new asset();
        Asst.name='test';
        Asst.Status = 'Inactive';
        Asst.RecordTypeId = AsstRecordTypeId;
        Asst.Opportunity__c  = Oppid;
        Asst.Accountid = accid;
        Asst.Managed_Tech_Count__c = 20;
        Asst.Asset_Live_Date__c = system.today();
        return Asst;
    }   
   
    public static Onboarding__c createonboarding(String AccountId, String opportunityId) {
        Id AsstRecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
        Onboarding__c onbrding = new Onboarding__c();
        onbrding.name = 'Test Onboarding';
        onbrding.Account__c = AccountId;
        onbrding.Opportunity__c = opportunityId;
        onbrding.Onboarding_Status__c = 'Live';
        onbrding.RecordTypeId = AsstRecordTypeId;
        return onbrding;
    }
    
    public static OpportunityContactRole createOppContactRole(String oppId, String role, String contId){
        OpportunityContactRole oppConRoleObj = new OpportunityContactRole();
        oppConRoleObj.OpportunityId = oppId;
        oppConRoleObj.Role = role;
        oppConRoleObj.ContactId = contId;
        return oppConRoleObj;
    } 

    public static OpportunityLineItem createOppLineItem(String oppId, String prodId, Decimal unitPrice, Integer quantity){
        OpportunityLineItem oppLIObj = new  OpportunityLineItem();
        oppLIObj.OpportunityId = oppId;
        oppLIObj.Product2Id  = prodId;
        oppLIObj.UnitPrice = unitPrice;
        oppLIObj.Quantity = quantity; 
        oppLIObj.Recurring_Revenue__c = true;
        oppLIObj.Projected_Quantity__c = quantity;
        return oppLIObj;  
    }  
    
    public static OpportunityTeamMember createOppTeamMem(String oppId, String userId, String teamMemRole){
        OpportunityTeamMember oppTMObj = new OpportunityTeamMember();
        oppTMObj.OpportunityId = oppId;
        oppTMObj.UserId = userId;
        oppTMObj.TeamMemberRole = teamMemRole;
        return oppTMObj;
    }     
    
    public static MRR_Tracking__c createMRRTracking(String assId, Double mrr, Id recTypeId) {
        MRR_Tracking__c mrrTrackObj = new MRR_Tracking__c();
        mrrTrackObj.RecordTypeId = Schema.SObjectType.MRR_Tracking__c.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
        mrrTrackObj.MRR__c = mrr;
        mrrTrackObj.Asset__c = assId;
        mrrTrackObj.BPS__c = 10;
        mrrTrackObj.Month__c = 'JAN';
        mrrTrackObj.Year__c = '2019';
        mrrTrackObj.Transaction_Count__c = 2;
        mrrTrackObj.Volume__c = 20;    
        return mrrTrackObj;
    }
   public static Case createCase(String accid, String conId) {
         Case CasObj= new Case();
         CasObj.Subject= 'testsubject';
         CasObj.recordtypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
         CasObj.AccountId = accid;
         CasObj.ContactId = conId;
         return CasObj;
   }
    
}