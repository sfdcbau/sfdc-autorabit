/**
	Test Customer-Partner creation
 */
@isTest
private class CampaignAddMemberTest 
{
	
    @IsTest(SeeAllData=false) 
    public static void testOpportuntyAddAccountContactToCampaign() 
    {
		// Add account
		Account acc = new Account();
		acc.Name = 'Test Account 0928342';
		acc.BillingCountry = 'United States';
        acc.Estimated_Total_Potential_Office_Users__c = 1;
        acc.Estimated_Total_Potential_Technicians__c = 1;
		insert acc;
		
		// Add contact
		Contact con = new Contact();
		con.AccountId = acc.Id;
		con.Firstname = 'Test';
		con.Lastname = '9823131';
		con.Role__c = 'Owner';
		insert con;
		
		// Add Campaign
		Campaign cam = new Campaign();
		cam.Name = 'Test Campaign 1098231';
		insert cam;
		
		// Add Campaign Member
		CampaignMember cm = new CampaignMember();
		cm.CampaignId = cam.Id;
		cm.ContactId = con.Id;
		insert cm;
		
		// Add Opportunity
		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.Name = 'Test Opportunity 8919841';
		opp.CloseDate = Date.today();
		opp.StageName = 'Stage Zero';
		insert opp;
		
		Test.startTest();
		
		// Assign primary campaign
		opp.CampaignId = cam.Id;
		update opp;
		
		Test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testCampaignMemberAddAccountOpportunityToCampaign() 
    {
		// Add account
		Account acc = new Account();
		acc.Name = 'Test Account 0928342';
		acc.BillingCountry = 'United States';
        acc.Estimated_Total_Potential_Office_Users__c = 1;
        acc.Estimated_Total_Potential_Technicians__c = 1;
		insert acc;
		
		// Add contact
		Contact con = new Contact();
		con.AccountId = acc.Id;
		con.Firstname = 'Test';
		con.Lastname = '9823131';
		con.Role__c = 'Owner';
		insert con;
		
		// Add Campaign
		Campaign cam = new Campaign();
		cam.Name = 'Test Campaign 1098231';
		insert cam;
		
		// Add Opportunity
		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.Name = 'Test Opportunity 8919841';
		opp.CloseDate = Date.today();
		opp.StageName = 'Stage Zero';
		insert opp;
		
		Test.startTest();
		
		// Add Campaign Member
		CampaignMember cm = new CampaignMember();
		cm.CampaignId = cam.Id;
		cm.ContactId = con.Id;
		insert cm;
		
		Test.stopTest();
    }
    
}