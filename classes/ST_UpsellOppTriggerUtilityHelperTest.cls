@istest
public class ST_UpsellOppTriggerUtilityHelperTest {

    @istest
    static void updateCallDatesonRespStagesTest(){
        Contact con = New Contact(lastname = 'Test');
        Insert con;
        
        Account acc = utilityHelperTest.createAccount();
        acc.Primary_Contact__c = con.id; 
        Insert acc;
        
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Upsell').getRecordTypeId();
        Opportunity opp = new Opportunity (Name='Opp1',StageName='Prospect', RecordTypeId= opportunityRecordTypeId, CloseDate=System.today() -2 ,AccountId = acc.id);
        insert opp;
        
        test.startTest();
        opp.StageName = 'Discovery Call';
        update opp;
        
        opp.StageName = 'Pricing / Negotiation';
        update opp;
        
        opp.StageName = 'Proposal Sent';
        update opp;
        test.stopTest();
    }
}