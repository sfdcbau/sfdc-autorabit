@isTest
private class ST_OBDiscoveryCallDateTest {

    static testmethod void  TestOBDiscoveryCallDate(){
        Account acc = new Account();
        acc.Name='Test Account' ;
        insert acc;
        Id OppRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        Opportunity opp1 = new Opportunity (Name='Opp1',StageName='Stage Zero', OBDiscoveryCallDate__c = system.today(),RecordTypeId= OppRT, CloseDate=Date.today() ,AccountId = acc.id, ImplementationDate__c= System.today()+2);
        insert opp1;
        
        Contact con = new contact();
        con.accountid = acc.id;
        con.lastname ='Test Contcat';
        insert con;
        
        Event eve = new Event();
        eve.Subject = 'Test Event';
        eve.ActivityDateTime = System.today() ; 
        eve.ActivityDate = System.today() ; 
        eve.DurationInMinutes = 30;
        eve.whoId= con.id;
        insert eve;
        List<event> lsteve =new List<Event>();
        lsteve.add(eve);
        Test.startTest();
            ST_OBDiscoveryCallDate.PopulateOBDiscoveryCall(lsteve);
        Test.stopTest();
        
    }
    
    static testmethod void  TestSendEmail(){
        Account acc = new Account();
        acc.Name='Test Account' ;
        acc.customer_status_picklist__c = 'Source and Enrich';
        insert acc;
        
        Contact con = new contact();
        con.accountid = acc.id;
        con.lastname ='Test Contcat';
        insert con;
        
        Event eve = new Event();
        eve.Subject = 'Test Event';
        eve.ActivityDateTime = System.today() ; 
        eve.ActivityDate = System.today() ; 
        eve.DurationInMinutes = 30;
        eve.whoId= con.id;
        insert eve;
        List<event> lsteve =new List<Event>();
        lsteve.add(eve);
        Test.startTest();
            ST_OBDiscoveryCallDate.PopulateOBDiscoveryCall(lsteve);
        Test.stopTest();
        
    }

}