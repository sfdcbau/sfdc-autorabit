/**
	Test
 */
@IsTest 
public class VCaseNewCustomerSupportAlertTest 
{
	/*
		Test Onboarding member
	*/
    @IsTest(SeeAllData=false) 
    public static void testNotification() 
    {
        List<User> userList = [Select Id From User Where IsActive=true And Profile.Name = 'System Administrator' And Id != :UserInfo.getUserId() Limit 1];
    	List<RecordType> recordtypeList = [Select Id From RecordType Where SobjectType='Case' And DeveloperName='Customer_Support' Limit 1];
        
        if(!userList.isEmpty() && !recordtypeList.isEmpty())
        {
            System.runAs(userList[0])
            {
               EmailTemplate et = new EmailTemplate();
                et.Name = 'Test Template 1231231';
                et.DeveloperName = 'Test_Template_1231231';
                et.Subject = 'Test';
                et.Body = 'Test';
                et.TemplateType = 'text';
                et.FolderId = UserInfo.getUserId();
                insert et; 
            }
            
            Email_Template_Setting__c cs = new Email_Template_Setting__c();
            cs.Name = 'NewCustomerSupportCase ';
            cs.EmailTemplateID__c = ([Select Id From EmailTemplate Where DeveloperName='Test_Template_1231231' Limit 1]).Id;
            insert cs;
            
            Account acc = new Account();
            acc.Name = 'Test Account 3123103811';
            insert acc;
            
            Contact con = new Contact();
            con.AccountId = acc.Id;
            con.Firstname = 'Test';
            con.Lastname = 'Test 123123113123';
            con.Email = 'test173712312@veltig.com';
            insert con;
            
            // Insert case
            Case c1 = new Case();
            c1.RecordTypeId = recordtypeList[0].Id;
            c1.Status = 'New';
            c1.Origin = 'Web';
            c1.Description = 'Test';
            insert c1;
            
            test.startTest();
            
            VCaseNewCustomerSupportAlert.Request req = new VCaseNewCustomerSupportAlert.Request();
            req.caseId = c1.Id;
            req.caseRecordTypeName = 'Customer_Support';
            req.caseStatus = 'New';
            req.minNumberOfCases = 1;
            req.recipientContactId = con.Id;
            
            List<VCaseNewCustomerSupportAlert.Request> requests = new List<VCaseNewCustomerSupportAlert.Request>();
            requests.add(req);
            
            VCaseNewCustomerSupportAlert.sendNewCustomerSupportAlert(requests);
            
            test.stopTest();
        }
        
        
    }
	/*
		Test Onboarding member
	*/
    @IsTest(SeeAllData=false) 
    public static void testNotificationInvalidContact() 
    {
        List<User> userList = [Select Id From User Where IsActive=true And Profile.Name = 'System Administrator' And Id != :UserInfo.getUserId() Limit 1];
    	List<RecordType> recordtypeList = [Select Id From RecordType Where SobjectType='Case' And DeveloperName='Customer_Support' Limit 1];
        
        if(!userList.isEmpty() && !recordtypeList.isEmpty())
        {   
            System.runAs(userList[0])
            {
               EmailTemplate et = new EmailTemplate();
                et.Name = 'Test Template 1231231';
                et.DeveloperName = 'Test_Template_1231231';
                et.Subject = 'Test';
                et.Body = 'Test';
                et.TemplateType = 'text';
                et.FolderId = UserInfo.getUserId();
                insert et; 
            }
            
            Email_Template_Setting__c cs = new Email_Template_Setting__c();
            cs.Name = 'NewCustomerSupportCase ';
            cs.EmailTemplateID__c = ([Select Id From EmailTemplate Where DeveloperName='Test_Template_1231231' Limit 1]).Id;
            insert cs;
            
            // Insert case
            Case c1 = new Case();
            c1.RecordTypeId = recordtypeList[0].Id;
            c1.Status = 'New';
            c1.Origin = 'Web';
            c1.Description = 'Test';
            insert c1;
            
            test.startTest();
            
            VCaseNewCustomerSupportAlert.Request req = new VCaseNewCustomerSupportAlert.Request();
            req.caseId = c1.Id;
            req.caseRecordTypeName = 'Customer_Support';
            req.caseStatus = 'New';
            req.minNumberOfCases = 1;
            req.recipientContactId = '1234567890';
            
            List<VCaseNewCustomerSupportAlert.Request> requests = new List<VCaseNewCustomerSupportAlert.Request>();
            requests.add(req);
            
            VCaseNewCustomerSupportAlert.sendNewCustomerSupportAlert(requests);
            
            test.stopTest();
        }
        
        
    }
}