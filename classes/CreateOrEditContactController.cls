/*--------------------------------------------------------------------------------
Class Name: CreateOrEditContactController
Created By: Rakesh Kanugu
Subject: Add or Update XDR/AE/Onboarding POC On Sales Opportunity
Summary: Controls 'CreateOrEditContact' VF page and 'Add POC button' on Opportunity
@Updated on 11/02/2019 
@Version : V 1.1 
@Updated by Sanjeev 
@Jira Story : (SE-1514)
@Code Coverage : 93%
----------------------------------------------------------------------------------*/
public class CreateOrEditContactController {
        public String accId;
        public String oppId {get; set;}
        public List<Contact> contactList {get; set;}
        public List<Wrapper> listWrapper {get; set;}
        public Account acc {get; set;}
        public Contact singleCon {get; set;}
        public ContactWrapper objContactWrapper{get; set;}
        public OppWrapper objOppWrapper {get; set;}
        public CreateOrEditContactController(){
            listWrapper = new List<Wrapper>();
            objOppWrapper = new OppWrapper();
            objContactWrapper = new ContactWrapper();
            if(apexpages.currentpage().getparameters().get('accid') != null)
                accId = String.valueOf(apexpages.currentpage().getparameters().get('accid'));
            if(apexpages.currentpage().getparameters().get('oppId') != null)
                oppId = String.valueOf(apexpages.currentpage().getparameters().get('oppId'));
            system.debug('--oppId--'+oppId);
            contactList = [SELECT Id, Name,Role__c FROM Contact WHERE AccountId =:accId];
            if(contactList != null && !contactList.isEmpty()){
                for(Contact objContact : contactList){
                    Wrapper objWrapper = new Wrapper();
                    objWrapper.checked = false;
                    objWrapper.recordId = objContact.Id;
                    objWrapper.contactName = objContact.Name;
                    objWrapper.role = objContact.Role__c;
                    //objWrapper.con = objContact;
                    listWrapper.add(objWrapper);
                }
            }
            singleCon = new Contact();
        }
        
        public void addContact(){
            //insert singleCon;
            system.debug('--objContactWrapper--'+objContactWrapper);
            if(objContactWrapper != null){
                singleCon.accountId = accId;
                singleCon.firstName = objContactWrapper.firstName;
                singleCon.lastName = objContactWrapper.lastName;
                singleCon.email = objContactWrapper.email;
                singleCon.phone = objContactWrapper.phone;
                singleCon.Role__c = objContactWrapper.role;
                
                insert singleCon;
                Wrapper obj_Wrapper = new Wrapper();
                obj_Wrapper.checked = false;
                obj_Wrapper.recordId = singleCon.Id;
                String name = singleCon.firstName;
                if(String.isNotBlank(name))
                    name += ' '+ singleCon.lastName;
                else
                    name = singleCon.lastName;
                obj_Wrapper.contactName = name;
                obj_Wrapper.role = singleCon.Role__c;
                listWrapper.add(obj_Wrapper);
            }
            //contactList.add(singleCon);        
            singleCon = new Contact();
        }
        
        public void updateOpp(){
            system.debug('--listWrapper--'+listWrapper);
            system.debug('--objOppWrapper--'+objOppWrapper);
            Contact objContact = null;
            if(listWrapper != null && !listWrapper.isEmpty()){
                for(Wrapper objWrapper : listWrapper){
                    if(objWrapper.checked != null && objWrapper.checked){
                        objContact = new Contact(id=objWrapper.recordId,Role__c=objWrapper.role);
                        break;
                    }
                }
            }
            if(objContact != null){
                update objContact;
                
                Opportunity opp = null;
                if(objOppWrapper != null && (objOppWrapper.XDR_Discovery_POC != null && objOppWrapper.XDR_Discovery_POC)){
                    opp = new Opportunity(XDR_Discovery_POC__c=objContact.Id);//XDR_Discovery_POC__c
                }else if(objOppWrapper != null && (objOppWrapper.AE_Discovery_POC != null && objOppWrapper.AE_Discovery_POC)){
                    opp = new Opportunity(AE_Discovery_POC__c=objContact.Id);//AE_Discovery_POC__c
                }
                else if(objOppWrapper != null && (objOppWrapper.Onboarding_POC != null && objOppWrapper.Onboarding_POC)){
                    opp = new Opportunity(Onboarding_POC__c=objContact.Id);//Onboarding_POC__c
                }
                if(opp != null){
                    opp.Id = oppId; 
                    update opp;
                }
            }
        }
        
        public void updateContact(){
            Wrapper obj = null;
            if(listWrapper != null && !listWrapper.isEmpty()){
                for(Wrapper objWrapper : listWrapper){
                    if(objWrapper.checked == true){
                        obj = objWrapper;
                        break;
                    }
                }
            }
            system.debug('--obj--'+obj);
            //if(obj != null && String.isBlank(obj.con.Role__c))
            //    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Contact must have role inorder to save POC'));
        }
        
        public List<SelectOption> getRoles(){
          List<SelectOption> options = new List<SelectOption>();
           //options.add(new SelectOption('--None--', ''));     
           Schema.DescribeFieldResult fieldResult = Contact.Role__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
           //Added by Sanjeev
           
           String contactRoles = System.label.ST_ContactRoles;
           List<String> choosingPickList = new List<String>();     
           choosingPickList = contactRoles.split(',');
           Set<String> removeValue = new Set<String>();
           for(String pickListString: choosingPickList){      
               removeValue.add(pickListString);
           }   
           
           for( Schema.PicklistEntry f : ple){   
              if(!removeValue.contains(f.getLabel()))                
              options.add(new SelectOption(f.getLabel(), f.getValue()));       
           }     
           return options;
        }
        
        public class Wrapper{
            public Boolean checked {get;set;}
            public String recordId {get;set;}
            public String contactName {get;set;}
            public String role {get;set;}
            public Wrapper() {
                
            }
        }
        
        public class ContactWrapper{
            public String firstName {get;set;}
            public String lastName {get;set;}
            public String email {get;set;}
            public String phone {get;set;}
            public String role {get;set;}
            public ContactWrapper() {
                firstName = '';
                lastName = '';
                email = '';
                phone = '';
                role = '';
            }
        }
        
        public class OppWrapper{
            public Boolean XDR_Discovery_POC {get;set;}
            public Boolean AE_Discovery_POC {get;set;}
            public Boolean Onboarding_POC {get;set;}
            
            public OppWrapper(){
                XDR_Discovery_POC = false;
                AE_Discovery_POC = false;
                Onboarding_POC = false;
            }
        }
    }