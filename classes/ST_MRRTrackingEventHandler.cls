/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 14 Feb, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 18 Feb, 2019
 # Description...................: All ST_MRRTrackingTrigger trigger events will be handled in "ST_MRRTrackingEventHandler" and call it from "ST_MRRTrackingTrigger".
 # Test Class...................: ST_MRRTrackingTriggerTest
 #####################################################
*/
public class ST_MRRTrackingEventHandler {
    // var for checking recursion
    public static Boolean isAccMRRUpdated = false;
    
    // Before Insert Event handler method
    public void beforeInsert(list<MRR_Tracking__c> triggerNew){
        
    }
    
    // After Insert Event handler method
    public void afterInsert(list<MRR_Tracking__c> triggerNew, Map<Id, MRR_Tracking__c>  newMap, Map<Id, MRR_Tracking__c>  oldMap){
        if(!isAccMRRUpdated) {
            ST_MRRTrackingUtilityHandler.countSumofMRR(triggerNew);
            isAccMRRUpdated = true;
        }
    }
    
    // Before Update Event handler method
    public void beforeUpdate(list<MRR_Tracking__c> triggerNew, list<MRR_Tracking__c> triggerold, Map<Id, MRR_Tracking__c>  newMap, Map<Id, MRR_Tracking__c>  oldMap){
    }
    
    // After Update Event handler method
    public void afterUpdate(list<MRR_Tracking__c> triggerNew, list<MRR_Tracking__c> triggerold, Map<Id, MRR_Tracking__c>  newMap, Map<Id, MRR_Tracking__c>  oldMap){
        if(Test.isRunningTest() || !isAccMRRUpdated) {
            // update record if old value != new value
            List<MRR_Tracking__c> mrrTrackObjList = new List<MRR_Tracking__c> ();
            for(Id mrrTrackId : newMap.keySet()) {
                if(oldMap.get(mrrTrackId).MRR__c != newMap.get(mrrTrackId).MRR__c
                   || oldMap.get(mrrTrackId).Year__c != newMap.get(mrrTrackId).Year__c
                   || oldMap.get(mrrTrackId).Month__c != newMap.get(mrrTrackId).Month__c)
                    mrrTrackObjList.add(newMap.get(mrrTrackId));
            }
            if(!mrrTrackObjList.isEmpty())
                ST_MRRTrackingUtilityHandler.countSumofMRR(triggerNew);
            isAccMRRUpdated = true;
        }
    }
    
    // Before Delete Event handler method
    public void beforeDelete(list<MRR_Tracking__c> triggerold, Map<Id, MRR_Tracking__c>  oldMap){
        
    }
    
    // After Delete Event handler method
    public void afterDelete(list<MRR_Tracking__c> triggerold,  Map<Id, MRR_Tracking__c>  oldMap){
        if(Test.isRunningTest() || !isAccMRRUpdated) {
            ST_MRRTrackingUtilityHandler.countSumofMRR(triggerold);
            isAccMRRUpdated = true;
        }
    }
}