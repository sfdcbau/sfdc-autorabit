/****************************************
* @author   : Sanjeev Kaurav
* @JIRA     : SPR-11
* @Trigger  : ST_OnboardingTrigger
* @testcls  : ST_PaymentsOnboardingHandlerTest
* **************************************/
Public class ST_PaymentsOnboardingHandler{
    
    public static Boolean isAccSTPayCusStatusUpdated = false;
    private static Boolean runonce = false;
    
    public static void onBeforeInsert(List<Onboarding__c> triggernew, Map<Id,Onboarding__c> triggerOldMap) {
        if(!runonce) {
            ST_PaymentsOnboardingHandler.noChangesAllowedonLiveOnboarding(triggernew, triggerOldMap);
            runonce = true;
        }
    }
    /**
    * @author : Aman Gupta
    * @date : 12 Feb, 2019
    * @description : This method is to handle onAfterInsert event of Opportunity trigger.
    */
    public static void onAfterInsert(List<Onboarding__c> triggernew, Map<Id, Onboarding__c> triggernewMap) {
        //Update Account (field: Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c) for Payment Onboardings whenever Onboarding__c record is created, updated.
        if(!isAccSTPayCusStatusUpdated) {
            ST_PaymentsOnboardingHandler.updateAccSTPayCusStatus(triggernew);
        }
    }
    /**
    * @author : Aman Gupta
    * @date : 18 Jan, 2019
    * @description : This method is to handle onAfterUpdate event of Opportunity trigger.
    */
    public static void onAfterUpdate(List<Onboarding__c> triggerold, Map<Id,Onboarding__c> triggeroldMap, List<Onboarding__c> triggernew, Map<Id,Onboarding__c> triggernewMap) {
        //Update Account (field: ST_Payments_Customer_Status__c, Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c) for Payment Onboardings whenever Onboarding__c record is created, updated.
        if(!isAccSTPayCusStatusUpdated) {
            // update record if old value != new value
            List<Onboarding__c> onBDObjList = new List<Onboarding__c> ();
            for(Id onBDId : triggernewMap.keySet()) {
                if(triggeroldMap.get(onBDId).Onboarding_Status__c != triggernewMap.get(onBDId).Onboarding_Status__c)
                    onBDObjList.add(triggernewMap.get(onBDId));
            }
            if(!onBDObjList.isEmpty()) {
                ST_PaymentsOnboardingHandler.updateAccSTPayCusStatus(onBDObjList);
                isAccSTPayCusStatusUpdated = true;
            }
        }
    }
    public static void CreationAsset(list<Onboarding__c> onbList, Map<Id, Onboarding__c> oldMap){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> selectedOppIds = new Set<Id>();
        Set<id> OnboardingIds = new Set<Id>();
        List<Asset> insertAssetList = new List<Asset>();
        List<Opportunity> updateOpportunity = new List<Opportunity>();
        Map<Id,Onboarding__c> mapOfIdAndOnboarding = new Map<Id,Onboarding__c>();
        
        Id onbRT = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
        
        Trigger_Settings__mdt TriggerFlag =  [SELECT 
                                              Label, isActive__c 
                                              FROM Trigger_Settings__mdt 
                                              where Label = 'ST_PaymentOpportunityHandler' limit 1];
        
        // Activate or deactivate this code in production from Meta Data Type by just making isActive = false.
        if(TriggerFlag.isActive__c) {
            // ST-3003: Prevent Duplicate Payment Asset Creation on Onboarding's
            // query all Assets related to Onboardings
            List<Asset> assetObjList = [SELECT
                                        Id, Onboarding__c
                                        FROM Asset 
                                        WHERE Onboarding__c IN :onbList];
            // Onboarding's Id set
            Set<Id> onBdIdSet = new Set<Id> ();
            for(Asset assetObj : assetObjList) {
                onBdIdSet.add(assetObj.Onboarding__c);
            }
            
            for(Onboarding__c onb : onbList){
                if(onb.Onboarding_Status__c == 'Pending ST Live' && onb.recordtypeid == onbRT && onb.Opportunity__c != null && onb.Onboarding_Status__c!= oldMap.get(Onb.Id).Onboarding_Status__c){
                    // adding only those Onboarding's that don't have any existing Asset's
                    if(!onBdIdSet.contains(onb.Id)) {
                        oppIds.add(onb.Opportunity__c);
                        OnboardingIds.add(onb.id);
                        mapOfIdAndOnboarding.put(onb.id,onb);   
                    }
                }
            }
            
            user currentUser = [Select 
                                Id, By_Pass_Validation__c 
                                From User 
                                Where Id = :userInfo.getuserId()];
            currentUser.By_Pass_Validation__c = true;
            update currentUser;
            
            for(Opportunity opp : [Select id, stagename from Opportunity where id IN :oppIds]){
                if(opp.stagename == 'Pending Boarding'){
                    opp.stagename = 'Closed Won';
                    updateOpportunity.add(opp);
                    selectedOppIds.add(opp.id);
                }
                if(opp.stagename == 'Closed Won'){
                    
                    selectedOppIds.add(opp.id);
                }
            }
            if(updateOpportunity.size()>0){
                update updateOpportunity;
            }
            currentUser.By_Pass_Validation__c = false;
            update currentUser;
            
            List<OpportunityLineItem> oliList = [Select id, 
                                                 Product2Id,
                                                 Product2.Name,
                                                 Name,                                               
                                                 OpportunityId, 
                                                 Asset__c,
                                                 Onboarding__c,                                              
                                                 Opportunity.AccountId,
                                                 Opportunity.Account.Name,
                                                 opportunity.Name,
                                                 Opportunity.Primary_Contact__c                                            
                                                 FROM OpportunityLineItem 
                                                 WHERE OpportunityId IN :selectedOppIds 
                                                 AND Opportunity.hasOpportunitylineitem = true
                                                 AND  Onboarding__c In:OnboardingIds ];      
            // Creating Assets related to Account and opportunity with Status equal to onboarding.
            if(oliList.size()>0){
                for(OpportunityLineItem OLI : oliList){
                    
                    //DateTime dT = System.now();
                    DateTime dTt = System.now();
                    //date dTt = date.newInstance(, 3, 21);
                    String dayString = dTt.format('MM/dd/yyyy');
                    //d.format('dd/MM/yyyy')
                    //Date myDate = date.newinstance(dT.day(),dT.month(), dT.year());
                    
                    //String dateStr = String.valueof(myDate);
                    
                    Asset asset_Obj = new Asset();
                    asset_Obj.Name = oli.Opportunity.Account.Name +' '+'-'+' '+ OLI.Product2.Name+' '+'-'+' '+dayString; 
                    
                    asset_Obj.Product2Id = OLI.Product2Id;
                    asset_Obj.AccountId = OLI.Opportunity.AccountId;
                    asset_Obj.Opportunity__c = OLI.OpportunityId;
                    asset_Obj.Onboarding__c = OLI.Onboarding__c;
                    asset_Obj.ContactId = mapOfIdAndOnboarding.get(OLI.Onboarding__c).Primary_Contact__c;
                    asset_Obj.Status = 'Onboarding';
                    asset_Obj.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('Payments').getRecordTypeId();     
                    asset_Obj.Number_of_Swipers_Shipped_Asset__c = mapOfIdAndOnboarding.get(OLI.Onboarding__c).Swipers_Shipped__c;
                    asset_Obj.Asset_Merchant_ID__c =   mapOfIdAndOnboarding.get(OLI.Onboarding__c).MerchantID__c;
                    asset_Obj.Date_of_First_Transaction__c =   mapOfIdAndOnboarding.get(OLI.Onboarding__c).Date_of_First_Transaction__c;
                    insertAssetList.add(asset_Obj);
                    
                }
            }
            
            if(insertAssetList.size()>0){
                insert insertAssetList;
            }
            // Updating Assets to OLI
            if(insertAssetList.size()>0){
                for(Integer i=0; i<oliList.size();i++){
                    if(i<insertAssetList.size())
                        oliList[i].Asset__c =insertAssetList[i].id;
                }
            } 
            if(oliList.Size()>0){
                update oliList;
            }
        }
    }
    
    /**
    * @author : Aman Gupta
    * @date : 11 Feb, 2019
    * @description : SE-1678 --> Update Account (field: ST_Payments_Customer_Status__c, Credit_Card_Date_of_First_Transaction__c, Check_Date_of_First_Transaction__c) for Payment Onboardings whenever Onboarding__c record is created, updated.
    */
    public static void updateAccSTPayCusStatus(List<Onboarding__c> onBDObjList) {
        Set<Id> accIdSet = new Set<Id> ();
        List<Account> accObjList = new List<Account> ();
        
        // filter Payment Onboardings
        for(Onboarding__c onBDObj : onBDObjList) {
            if(onBDObj.RecordTypeId == Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId()) {
                accIdSet.add(onBDObj.Account__c);
            }
        }
        
        // fetch Accounts if accIdSet not empty
        if(!accIdSet.isEmpty()) {
            for(Account accObj : [SELECT Id, Name, ST_Payments_Customer_Status__c, (SELECT Id, Name, Opportunity__r.StageName, Onboarding_Status__c, Date_of_First_Transaction__c FROM Onboarding__r WHERE RecordType.DeveloperName = 'Payments_Onboarding' AND Opportunity__r.StageName != 'Closed Lost' ORDER BY Date_of_First_Transaction__c) FROM Account WHERE Id IN :accIdSet]) {
                if(!accObj.Onboarding__r.isEmpty()) {
                    
                    Boolean isAllOnBDPendSTLive = true;
                    Date ccDateOfFirstTrans = null;
                    Date cDateOfFirstTrans = null;
                    for(Onboarding__c onBDObj : accObj.Onboarding__r) {
                        if(onBDObj.Onboarding_Status__c.equalsIgnoreCase('Pending ST Live')) {
                            if(ccDateOfFirstTrans == null && onBDObj.Name.containsIgnoreCase(Label.ST_Payment_Credit_Card))
                                ccDateOfFirstTrans = onBDObj.Date_of_First_Transaction__c;
                            
                            if(cDateOfFirstTrans == null && onBDObj.Name.containsIgnoreCase(Label.ST_Payment_Check))
                                cDateOfFirstTrans = onBDObj.Date_of_First_Transaction__c;    
                        }
                        else {
                            isAllOnBDPendSTLive = false;
                            break;
                        }
                    }
                    
                    Set<String> assetStatusSet = new Set<String> {'Live (Credit Cards)', 'Live (Checks)', 'Live (Credit Cards & Checks)'};
                    Set<String> oppStageSet = new Set<String> {'Prospect', 'Discovery Call', 'Negotiation / Pending', 'Pending Boarding'};
                    if(!assetStatusSet.contains(accObj.ST_Payments_Customer_Status__c)) {
                        // update Account
                        if(isAllOnBDPendSTLive) {
                            // update Account if all Onboarding's are 'Pending ST Live'
                            accObj.Credit_Card_Date_of_First_Transaction__c = ccDateOfFirstTrans;
                            accObj.Check_Date_of_First_Transaction__c = cDateOfFirstTrans;
                            accObj.ST_Payments_Customer_Status__c = 'Pending ST Live';
                            accObjList.add(accObj);
                        }
                        else {
                            Boolean isAnyOnBDStatusLive = false;
                            for(Onboarding__c onBDObj : accObj.Onboarding__r) {
                                if(onBDObj.Onboarding_Status__c == 'Live' || oppStageSet.contains(onBDObj.Opportunity__r.StageName)) {
                                    isAnyOnBDStatusLive = true;
                                    break;
                                }
                            }
                            // update Account if all Onboarding's are not 'Pending ST Live' and any Onboarding not 'Live' and Opportunity StageName not in ('Prospect', 'Discovery Call', 'Negotiation / Pending', 'Pending Boarding')
                            if(!isAnyOnBDStatusLive) {
                                accObj.ST_Payments_Customer_Status__c = 'Pending Boarding';
                                accObjList.add(accObj);
                            }
                        }  
                    }
                }
            }
        }
        
        // update accObjList if not empty
        if(!accObjList.isEmpty())
            update accObjList;
    }
    // Updating Disposition code and sub disposition code on Account from Onboarding 
    public static void updateDisposCodeandSubDisposCode(list<Onboarding__c> onboardinglist, Map<Id, Onboarding__c> oldMap) {
        Id onboardingRT = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('ST Onboarding').getRecordTypeId();
        Set<Id> accountId = new Set<Id>();
        Map<Id, Onboarding__c> onboardingMap = new Map<Id, Onboarding__c>();
        list<Account> updateAccountlist = new list<Account>();
        
        for(Onboarding__c onbrd : onboardinglist) {
            if((onbrd.DispositionCode__c != oldMap.get(onbrd.Id).DispositionCode__c || onbrd.SubDispositionCode__c != oldMap.get(onbrd.Id).SubDispositionCode__c) && 
               (onbrd.Onboarding_Status__c == 'Live' || onbrd.Onboarding_Status__c == 'Success') && onbrd.RecordTypeId == onboardingRT) {
                   accountId.add(onbrd.Account__c);
                   onboardingMap.put(onbrd.Account__c, onbrd);
               }
        }
        for(Account acc : [Select Id, DispositionCode__c, SubDisposition__c from Account Where Id IN : accountId]) {
            acc.DispositionCode__c = onboardingMap.get(acc.Id).DispositionCode__c;
            acc.SubDisposition__c  = onboardingMap.get(acc.Id).SubDispositionCode__c;
            updateAccountlist.add(acc);
        }
        if(updateAccountlist.size() > 0) {
            update updateAccountlist;
        }
    } 
    // Validation Rule : We can not change Payment onboarding when onboarding status is live.
    public static void noChangesAllowedonLiveOnboarding(list<Onboarding__c> onboardinglist, Map<Id, Onboarding__c> oldMap){
        Id SysAdminprofileId = UserInfo.getProfileId();
        Profile ProfId = [Select Id, name from profile where name = 'System Administrator'];
        Id onboardingRT = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
        for(Onboarding__c onbrd : onboardinglist) {
            if(oldMap.get(onbrd.Id).Onboarding_Status__c == 'Live' && onbrd.RecordTypeId == onboardingRT && ProfId.Id != SysAdminprofileId ) {
                onbrd.addError(label.RecordLockedOnboardingLiveError);
            }
        }
    }
     /**
    * @author : Vishal Labh
    * @date : 04 Mrach, 2019
    * @description : This method is to handle onAfterUpdate event of ST_OnboardingTriggerEventHandler.
    */
    Public static void updateAsset(list<Onboarding__c> onbrdlist, Map<Id, Onboarding__c> oldMap){
        Id onboardingRT = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
        Set<Id> onbordId = new Set<Id>();
        list<Asset> updateAssetlist = new list<Asset>();
        Map<Id, Onboarding__c> onboardingMap = new Map<Id, Onboarding__c>();
        
        for(Onboarding__c onb : onbrdlist){
            if(onb.Date_of_First_Transaction__c != oldMap.get(onb.Id).Date_of_First_Transaction__c && onb.RecordTypeId == onboardingRT) {
                onbordId.add(onb.Id);  
                onboardingMap.put(onb.Id, onb);  
            }
        }
        list<Asset> astList = [Select Id, name, Date_of_First_Transaction__c, Onboarding__c from Asset where Onboarding__c = :onbordId];
        if(astList.size() > 0) {
            for(Asset ast : astList) {
                ast.Date_of_First_Transaction__c = onboardingMap.get(ast.Onboarding__c).Date_of_First_Transaction__c;
                updateAssetlist.add(ast);
            }
        }
        if(updateAssetlist.size() > 0) {
            update updateAssetlist;
        }
    }
}