/*--------------------------------------------------------------------------------------------------------------------------------
Test class for RequestTriggerhandler class. 
@Method :- handleUpdateRequest
@author  : Kamal
----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class RequestTriggerhandler_Test {
    //creating test data......
    @testSetup static void setup() {
        Account testAccount = new Account();
        testAccount.name = 'test Account';
        insert testAccount;
        
        Contact testContact = new Contact();
        testContact.LastName = 'test contact';
        testContact.AccountId = testAccount.Id;
        testContact.Role__c = 'Owner';
        insert testContact;
        
        Reference_Request__c testRequest = new Reference_Request__c(); 
        testRequest.RecordTypeId = Schema.SObjectType.Reference_Request__c.getRecordTypeInfosByName().get(Label.SalesRequestRecordType).getRecordTypeId();
        testRequest.Request_Type__c = 'Call';
        testRequest.Matching_Criteria__c ='Size';
        testRequest.Matching_Comments__c = 'test comments';
        testRequest.Request_Description__c = 'test';
        testRequest.References_Desired__c = 3;
        testRequest.Desired_Completion_Date__c = system.today();
        insert testRequest;
        
        Customer_Reference_Program__c  testcrp = new Customer_Reference_Program__c(); 
        testcrp.CRP_Status__c = 'Active';
        testcrp.Account__c = testAccount.Id;
        insert testcrp;
        
        Customer_Participation__c testcustmrparticipation = new Customer_Participation__c();
        testcustmrparticipation.Request_Id__c = testRequest.id;
        testcustmrparticipation.Customer_Reference_Id__c = testcrp.id;
        insert testcustmrparticipation;
    }
    //positive test method.....
    static testMethod void testCRPupdatePositive(){
        
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        Reference_Request__c eachReq = [SELECT Id, Reference_Status__c, RecordTypeId FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name, Activities_Completed_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        Customer_Participation__c eachcp = [SELECT Id, Status__c FROM Customer_Participation__c LIMIT 1];
        eachcp.Status__c = 'Accepted';
        update eachcp;
        eachReq.Reference_Status__c = 'Completed';
        update eachReq;
        List<Reference_Request__c> lstofReq = new List<Reference_Request__c>();
        Map<Id, Reference_Request__c> mapofRequest = new Map<Id, Reference_Request__c>();
        lstofReq.add(eachReq);
        update lstofReq;
        mapofRequest.put(eachReq.Id, eachReq);
        
        Test.startTest();
        RequestTriggerhandler.handleUpdateRequest(lstofReq, mapofRequest);  
        Test.stopTest();
        Customer_Reference_Program__c updatedCRP = [SELECT Id, Name, Activities_Completed_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        System.assertEquals(updatedCRP.Activities_Completed_This_Year__c, 1);
    } 
    
    //negative test method.....
    static testMethod void testCRPupdateNegative(){
        
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        Reference_Request__c eachReq = [SELECT Id, Reference_Status__c, RecordTypeId FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name, Activities_Completed_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        Customer_Participation__c eachcp = [SELECT Id, Status__c FROM Customer_Participation__c LIMIT 1];
        eachcp.Status__c = 'Accepted';
        update eachcp;
        
        List<Reference_Request__c> lstofReq = new List<Reference_Request__c>();
        Map<Id, Reference_Request__c> mapofRequest = new Map<Id, Reference_Request__c>();
        lstofReq.add(eachReq);
        update lstofReq;
        mapofRequest.put(eachReq.Id, eachReq);
        
        Test.startTest();
        RequestTriggerhandler.handleUpdateRequest(lstofReq, mapofRequest);  
        Test.stopTest();
        Customer_Reference_Program__c updatedCRP = [SELECT Id, Name, Activities_Completed_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        System.assertEquals(updatedCRP.Activities_Completed_This_Year__c, 0);
    }
    
    //positive test method.....
    static testMethod void testCRPStatusupdate(){
        
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        Reference_Request__c eachReq = [SELECT Id, Reference_Status__c, RecordTypeId FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name, Activities_Completed_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        Customer_Participation__c eachcp = [SELECT Id, Status__c FROM Customer_Participation__c LIMIT 1]; 
        eachReq.Reference_Status__c = 'Completed';
        update eachReq;
        
        List<Reference_Request__c> lstofReq = new List<Reference_Request__c>();
        Map<Id, Reference_Request__c> mapofRequest = new Map<Id, Reference_Request__c>();
        lstofReq.add(eachReq); 
        mapofRequest.put(eachReq.Id, eachReq);
        
        Test.startTest();
        RequestTriggerhandler.handleRequestStatusUpdate(lstofReq, mapofRequest);  
        Test.stopTest();
        Reference_Request__c updatedReq = [SELECT Id, Reference_Status__c FROM Reference_Request__c LIMIT 1];
        System.assertEquals(updatedReq.Reference_Status__c, 'Completed');
    }
    
    //negative test method.....
    static testMethod void testCRPStatusupdateNegative(){
        
        Contact eachCon = [SELECT Id FROM Contact LIMIT 1];
        
        Reference_Request__c testReq  = new Reference_Request__c(); 
        testReq.RecordTypeId = Schema.SObjectType.Reference_Request__c.getRecordTypeInfosByName().get(Label.SalesRequestRecordType).getRecordTypeId();
        testReq.Request_Type__c = 'Call';
        testReq.Matching_Criteria__c ='Size';
        testReq.Matching_Comments__c = 'test comments';
        testReq.Request_Description__c = 'test';
        testReq.References_Desired__c = 3;
        testReq.Desired_Completion_Date__c = system.today();
        insert testReq;
        testReq.Reference_Status__c = 'Completed'; 
        
        List<Reference_Request__c> lstofReq = new List<Reference_Request__c>();
        Map<Id, Reference_Request__c> mapofRequest = new Map<Id, Reference_Request__c>();
        lstofReq.add(testReq);
        mapofRequest.put(testReq.Id, testReq);
        
        Test.startTest();
        RequestTriggerhandler.handleRequestStatusUpdate(lstofReq, mapofRequest);  
        Test.stopTest();
        Reference_Request__c updatedReq = [SELECT Id, Reference_Status__c FROM Reference_Request__c LIMIT 1];
        System.assertNotEquals(updatedReq.Reference_Status__c, 'Completed');
    }
}