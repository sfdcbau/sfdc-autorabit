/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 15 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 15 Mar, 2019
 # Description...................: This is test class for ST_UpdateCheckOnBDFromCCController class.
 #####################################################
*/
@isTest
private class ST_UpdateCheckOnBDFromCCControllerTest {
    /**
     * @author : Aman Gupta
     * @date : 15 Mar, 2019
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        // insert Product2
        Product2 prodObj1 = utilityHelperTest.createProduct();
        prodObj1.name = 'Premier Package';
        prodObj1.Core_Product__c = true;
        
        Product2 prodObj2 = utilityHelperTest.createProduct();
        prodObj2.name = 'PriceBook';
        prodObj2.Product_Type__c = 'Add-on';
        insert new List<Product2> {prodObj1, prodObj2};
        
        // insert Pricebook2
        Pricebook2 custPBObj = utilityHelperTest.craeteCustomPB2();
        insert new List<Pricebook2> {custPBObj};
            
        // insert PricebookEntry
        PricebookEntry pbeObj1 = utilityHelperTest.craeteStandardPBE2(prodObj1.Id);
        PricebookEntry pbeObj2 = utilityHelperTest.craeteStandardPBE2(prodObj2.Id);
        insert new List<PricebookEntry> {pbeObj1, pbeObj2};
        
        // insert Account
        Account accObj = utilityHelperTest.createAccount();
        insert new List<Account> {accObj};
            
        // insert Contact
        Contact conObj = utilityHelperTest.createContact(accObj.Id);
        conObj.Role__c = 'Co-Owner';
        insert new List<Contact> {conObj};
            
        // insert Opportunity
        Opportunity oppObj1 = utilityHelperTest.createopportunity(accObj.Id, Test.getStandardPricebookId(), conObj.Id);
        oppObj1.Name = 'Test Opportunity OB Rej1';
        insert new List<Opportunity> {oppObj1};
        
        // inserting OpportunityContactRole
		OpportunityContactRole oppConRoleObj1 = utilityHelperTest.createOppContactRole(oppObj1.Id, 'Co-Owner', conObj.Id);
        insert new List<OpportunityContactRole> {oppConRoleObj1};
		
		// inserting OpportunityLineItem for add-on product
        OpportunityLineItem oppLIObj1 = utilityHelperTest.createOppLineItem(oppObj1.Id, prodObj1.Id, 30.00, 30);
        insert oppLIObj1;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 15 Mar, 2019
     * @description : This method is to test ST_UpdateCheckOnBDFromCCController updateCheckOnBD function.
    */
    @isTest static void testUpdateCheckOnBD() {
        List<Profile> pObj= [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        // inserting User
        User userObj1 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1111', Email = 'testuser1111@test.com', Username = 'Test.User1111@test.com', CompanyName = 'TestCom1', Title = 'TestTitle1', Alias = 'TU1111', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        User userObj2 = new User(ProfileId = pObj[0].Id, LastName = 'TestUser1112', Email = 'testuser1112@test.com', Username = 'Test.User1112@test.com', CompanyName = 'TestCom2', Title = 'TestTitle2', Alias = 'TU1112', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        insert new List<User> {userObj1, userObj2};
        
        System.runAs (userObj1) {
            Account accObj = [Select Id, Name from Account LIMIT 1];
            Contact conObj = [Select Id, Name from Contact LIMIT 1];
            List<Opportunity> oppObjList = [Select Id, Name from Opportunity ORDER BY Name];
            
            String message = ST_UpdateCheckOnBDFromCCController.updateCheckOnBD(oppObjList[0].Id, '', null);
            System.assertEquals('DBA doesn\'t match with Check Onboarding\'s!', message);
            
            test.startTest();
            // inserting Payment Onboarding__c
			Onboarding__c onBDObj1 = utilityHelperTest.createonboarding(accObj.Id, oppObjList[0].Id);
			onBDObj1.Name = oppObjList[0].Name + Label.ST_Payment_Credit_Card;
			onBDObj1.Onboarding_Status__c = 'Pending Typeform Submission';
			onBDObj1.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            onBDObj1.DBA__c = 'TestDBA';
            onBDObj1.Typeform_Recieved_Date__c = System.today();
            onBDObj1.Supporting_Docs__c = 'TestSuppDocs';
            onBDObj1.Primary_Contact__c = conObj.Id;
			
			Onboarding__c onBDObj2 = utilityHelperTest.createonboarding(accObj.Id, oppObjList[0].Id);
			onBDObj2.Name = oppObjList[0].Name + Label.ST_Payment_Check;
			onBDObj2.Onboarding_Status__c = 'Pending Typeform Submission';
			onBDObj2.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            onBDObj2.DBA__c = 'TestDBA';
			
			Onboarding__c onBDObj3 = utilityHelperTest.createonboarding(accObj.Id, oppObjList[0].Id);
			onBDObj3.Name = oppObjList[0].Name + Label.ST_Payment_Check;
			onBDObj3.Onboarding_Status__c = 'Pending Typeform Submission';
			onBDObj3.RecordTypeId = Schema.SObjectType.Onboarding__c.getRecordTypeInfosByName().get('Payments Onboarding').getRecordTypeId();
            onBDObj3.DBA__c = 'TestDBA';
			
			insert new List<Onboarding__c> {onBDObj1, onBDObj2, onBDObj3};
			
            Map<String, String> dataMap = new Map<String, String> ();
            dataMap.put('Typeform_Recieved_Date__c', String.valueOf(onBDObj1.Typeform_Recieved_Date__c));
            dataMap.put('Primary_Contact__c', onBDObj1.Primary_Contact__c);
            dataMap.put('Supporting_Docs__c', onBDObj1.Supporting_Docs__c);
            dataMap.put('Business_License_Received__c', '');
            dataMap.put('Driver_s_License_Received__c', '');
            dataMap.put('SSN_Received__c', '');
            dataMap.put('Voided_Check_Received__c', '');
            dataMap.put('CC_Statement_Received__c', '');
            dataMap.put('Bank_Statements_Received__c', '');
            dataMap.put('Financial_Statements_Received__c', '');

            message = ST_UpdateCheckOnBDFromCCController.updateCheckOnBD(oppObjList[0].Id, onBDObj1.DBA__c, dataMap);
            System.assertEquals('Check Onboarding\'s Updated Successfully!', message);
                
            // Query Check Onboarding records
            List<Onboarding__c> onBDObjList = [SELECT Id, Name, Typeform_Recieved_Date__c, Supporting_Docs__c, Primary_Contact__c FROM Onboarding__c WHERE Name like '%Check%'];
            System.assertEquals(2, onBDObjList.size());
            System.assertEquals(true, (onBDObjList[0].Typeform_Recieved_Date__c != null && onBDObjList[0].Supporting_Docs__c != null && onBDObjList[0].Primary_Contact__c != null));
            System.assertEquals(true, (onBDObjList[1].Typeform_Recieved_Date__c != null && onBDObjList[1].Supporting_Docs__c != null && onBDObjList[1].Primary_Contact__c != null));
            
            try {
                dataMap.put('Typeform_Recieved_Date__c', '2019-12-33');
                message = ST_UpdateCheckOnBDFromCCController.updateCheckOnBD(oppObjList[0].Id, onBDObj1.DBA__c, dataMap);
            } catch(Exception ex) {}
        }
    }
}