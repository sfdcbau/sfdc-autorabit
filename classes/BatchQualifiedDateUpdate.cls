global class BatchQualifiedDateUpdate implements Database.Batchable<sObject>{

   global Database.QueryLocator start(Database.BatchableContext BC){
       Set<String> accIds = new Set<String>();
       List<Opportunity> lstOpportunity = null;
       if(Test.isrunningTest())
       		lstOpportunity = [SELECT Id,AccountId FROM Opportunity WHERE recordtype.name = 'Sales' and closeDate!=null and Discovery_Call_Stage_Date__c != null and AccountId != null];
       else
           lstOpportunity = [SELECT Id,AccountId FROM Opportunity WHERE recordtype.name = 'Sales' and StageName != 'Stage Zero' and closeDate!=null and Discovery_Call_Stage_Date__c != null and AccountId != null];
       if(lstOpportunity != null && !lstOpportunity.isEmpty()){
           for(Opportunity objOpportunity : lstOpportunity){
               if(String.isNotBlank(objOpportunity.AccountId))
                   accIds.add(objOpportunity.AccountId);
           }
       }
       String query = '';
       if(Test.isrunningTest())
           query = 'select id,Qualified_Date__c,(select id,name,CloseDate,Discovery_Call_Stage_Date__c from Opportunities where recordtype.name = \'Sales\' and Discovery_Call_Stage_Date__c != null order by Discovery_Call_Stage_Date__c asc limit 1) from account where Qualified_Date__c = null and Id IN:accIds';
       else
           query = 'select id,Qualified_Date__c,(select id,name,CloseDate,Discovery_Call_Stage_Date__c from Opportunities where recordtype.name = \'Sales\' and StageName != \'Stage Zero\' and Discovery_Call_Stage_Date__c != null order by Discovery_Call_Stage_Date__c asc limit 1) from account where Qualified_Date__c = null and Id IN:accIds';
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Account> scope){
        
       if(scope != null && !scope.isEmpty()){
           List<Account> accList = new List<Account>();
            for(Account objAccount : scope){
                if(objAccount.Opportunities != null && !objAccount.Opportunities.isEmpty()){
                    Opportunity objOpportunity = objAccount.Opportunities[0];
                    if(objOpportunity != null){
                        if(objOpportunity.Discovery_Call_Stage_Date__c != null){
                            objAccount.Qualified_Date__c = objOpportunity.Discovery_Call_Stage_Date__c;
                            accList.add(objAccount);
                            }
                            
                    }
                }
            }
           if(accList != null && !accList.isEmpty()){
               update accList;
               system.debug('----accList--'+accList);
           }
        }
    }

   global void finish(Database.BatchableContext BC){
   }
}