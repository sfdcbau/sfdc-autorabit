@isTest

public class VContactTriggerDispatcherTest{

    
    @isTest
    static void positiveCreationOfCustomerPartnerContactRecords(){
        
        Test.startTest();
        // Add accounts
        List<Account> accountList = new List<Account>();
        
        Account acc0 = new Account();
        acc0.Name = 'Lead Customer 28731983';
        acc0.BillingCountry = 'United States';
        accountList.add(acc0);
        
        Account acc1 = new Account();
        acc1.Name = 'Test Customer 827312313';
        acc1.BillingCountry = 'United States';
        accountList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Test Partner 92834023';
        acc2.BillingCountry = 'United States';
        accountList.add(acc2);
        
        Account acc3 = new Account();
        acc3.Name = 'Test Partner 059875906784';
        acc3.BillingCountry = 'United States';
        accountList.add(acc3);
        
        insert accountList;
    
        // Insert opportunity
        List<Contact> contactList = new List<Contact>();
        
        Contact con1 = new Contact();
        con1.FirstName = 'Test';
        con1.LastName = 'Contact017239018231';
        con1.AccountId = accountList[2].Id;
        con1.Role__c = 'POC';
        con1.Primary_Contact__c = true;
        contactList.add(con1);
        
        Contact con2 = new Contact();
        con2.FirstName = 'Test';
        con2.LastName = 'Contact091823';
        con2.AccountId = accountList[2].Id;
        con2.Role__c = 'POC';
  
        contactList.add(con2);
        
         insert contactList;
    
        Static_ID__c csStatic = new Static_ID__c();
        csStatic.Name = 'Account_Lead_Customer';
        csStatic.RecordName__c = acc0.Name;
        csStatic.RecordID__c = acc0.Id;
        insert csStatic;
        
        // Add customer partners
        List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
        
        CustomerPartner__c cp1 = new CustomerPartner__c();
        cp1.Customer__c = accountList[1].Id;
        cp1.Partner__c = accountList[2].Id;
        customerpartnerList.add(cp1);
        
        insert customerpartnerList;  
        
        System.assertEquals(2, ([Select Id From Customer_Partner_Contact__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        Contact con3 = new Contact();
        con3.FirstName = 'Test';
        con3.LastName = 'Contact040456840';
        con3.AccountId = accountList[2].Id;
        con3.Role__c = 'POC';
        con3.Primary_Contact__c = true;
        insert con3;      
        
        con2.Primary_Contact__c = true;
        con2.AccountId = accountList[0].Id;
        update con2;
        
        cp1.Partner__c = accountList[3].Id;
        update customerpartnerList;
        
        System.assertEquals(0, ([Select Id From Customer_Partner_Contact__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        Test.stopTest();
   }
    
    
    
  
}