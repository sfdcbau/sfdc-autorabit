/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 29 Mar, 2019
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 29 Mar, 2019
 # Description...................: This is test class for ST_EmailMessageTrigger trigger, ST_EmailMessageTriggerEventHandler and ST_EmailMessageTriggerUtilityHandler class.
 #####################################################
*/
@isTest
private class ST_EmailMessageTriggerTest {
    /**
     * @author : Aman Gupta
     * @date : 29 Mar, 2019
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        // insert Case records
        Case caseObj1 = new Case();
        caseObj1.Subject = 'TestSubject1';
        caseObj1.RecordTypeId =   Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        caseObj1.Description = 'Test Desc';
        caseObj1.SuppliedEmail = 'test1@test.com';
        caseObj1.SuppliedName = 'Test1';
        
        Case caseObj2 = new Case();
        caseObj2.Subject = 'TestSubject2';
        caseObj2.RecordTypeId =   Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        caseObj2.Description = 'Test Desc';
        caseObj2.SuppliedEmail = 'test2@test.com';
        caseObj2.SuppliedName = 'Test2';
        
        Case caseObj3 = new Case();
        caseObj3.Subject = 'TestSubject3';
        caseObj3.RecordTypeId =   Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        caseObj3.Description = 'Test Desc';
        caseObj3.SuppliedEmail = 'test3@test.com';
        caseObj3.SuppliedName = 'Test3';
        
        Case caseObj4 = new Case();
        caseObj4.Subject = 'TestSubject4';
        caseObj4.RecordTypeId =   Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        caseObj4.Description = 'Test Desc';
        caseObj4.SuppliedEmail = 'test4@test.com';
        caseObj4.SuppliedName = 'Test4';
        
        Case caseObj5 = new Case();
        caseObj5.Subject = 'TestSubject5';
        caseObj5.RecordTypeId =   Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Support').getRecordTypeId();
        caseObj5.Description = 'Test Desc';
        caseObj5.SuppliedEmail = 'test5@test.com';
        caseObj5.SuppliedName = 'Test5';
        
        insert new List<Case> {caseObj1, caseObj2, caseObj3, caseObj4, caseObj5};
        
        // insert EmailMessage records
        EmailMessage emailMsgObj1 = new EmailMessage(ParentId = caseObj1.Id);
        emailMsgObj1.ToAddress = 'support+aladdin@servicetitan.com';
        emailMsgObj1.CcAddress = '';
        emailMsgObj1.BccAddress = '';
        
        EmailMessage emailMsgObj2 = new EmailMessage(ParentId = caseObj2.Id);
        emailMsgObj2.ToAddress = 'test@test.com';
        emailMsgObj2.CcAddress = 'support+premium@servicetitan.com';
        emailMsgObj2.BccAddress = '';
        
        EmailMessage emailMsgObj3 = new EmailMessage(ParentId = caseObj3.Id);
        emailMsgObj3.ToAddress = 'test@test.com';
        emailMsgObj3.CcAddress = '';
        emailMsgObj3.BccAddress = 'support+servicechampions@servicetitan.com';
        
        EmailMessage emailMsgObj4 = new EmailMessage(ParentId = caseObj4.Id);
        emailMsgObj4.ToAddress = 'support+temperaturepro@servicetitan.com';
        emailMsgObj4.CcAddress = 'support+horizonservicesinc@servicetitan.com';
        emailMsgObj4.BccAddress = '';
        
        EmailMessage emailMsgObj5 = new EmailMessage(ParentId = caseObj5.Id);
        emailMsgObj5.ToAddress = 'test@test.com';
        emailMsgObj5.CcAddress = '';
        emailMsgObj5.BccAddress = '';
        
        EmailMessage emailMsgObj6 = new EmailMessage(ParentId = caseObj1.Id);
        emailMsgObj6.ToAddress = 'test@test.com';
        emailMsgObj6.CcAddress = 'test@test.com';
        emailMsgObj6.BccAddress = '';
        
        insert new List<EmailMessage> {emailMsgObj1, emailMsgObj2, emailMsgObj3, emailMsgObj4, emailMsgObj5, emailMsgObj6};
    }
    
    /**
     * @author : Aman Gupta
     * @date : 29 Mar, 2019
     * @description : This method is to test ST_EmailMessageTriggerUtilityHandler updatePremiumCases function.
    */
    @isTest static void testUpdatePremiumCases() {
        List<Case> caseObjList = [Select Id, Premium_Case__c from Case ORDER BY Subject];
        
        System.assertEquals(5, caseObjList.size());
        System.assertEquals(true, caseObjList[0].Premium_Case__c);
        System.assertEquals(true, caseObjList[1].Premium_Case__c);
        System.assertEquals(true, caseObjList[2].Premium_Case__c);
        System.assertEquals(true, caseObjList[3].Premium_Case__c);
        System.assertEquals(false, caseObjList[4].Premium_Case__c);
        
        update caseObjList;
        delete caseObjList;
    }
}