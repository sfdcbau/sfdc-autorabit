/**
    Tests
*/
@IsTest 
public with sharing class VCommunityCaseControllerTest 
{
    
    @IsTest(SeeAllData=false) 
    public static void testCaseController() 
    {
        Account acc = new Account();        
        acc.name = 'Test Account 109273012983';
        insert acc;
        
        Contact con = new Contact();
        con.LastName = 'Test 91083013';
        con.FirstName = 'User';
        con.Phone='925-111-2222';
        con.Accountid = acc.id;
        insert con;
        
        Case newcase = new Case();
        newcase.accountId = acc.id;
        newcase.contactId = con.id;
        newcase.Status = 'New';
        newcase.Subject = 'Test';
        insert newcase;
        
        CaseComment newcasecomment = new CaseComment();
        newcasecomment.ParentId = newcase.Id;
        newcasecomment.CommentBody = 'Test';
        newcasecomment.isPublished = true;
        insert newcasecomment;
        
        test.startTest();
        
        Case mycase = VCommunityCaseController.getCase(newcase.Id);
        mycase = VCommunityCaseController.escalateCase(newcase.Id, 'Test');
        mycase = VCommunityCaseController.openCloseCase(newcase.Id, 'Closed');
        
        List<VCommunityCaseController.CaseCommentObj> casecommentList = VCommunityCaseController.getCaseComments(newcase.Id, 5);
        Integer totalCaseComments = VCommunityCaseController.getTotalCaseComments(newcase.Id);
        
        User myuser = VCommunityCaseController.getCurrentUser();
        
        test.stopTest();
    }
    
}