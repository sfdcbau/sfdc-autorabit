/**
* --------------------------------------------------------------------------------------------  
* This is an controller extension class used for Visualforce page 'CRP_ExternalNomination'. 
* This class creates a Reference Request record when user enter all required details
* ------------------------------------------------------------------------------------------- 
* @version          1.0
* @created          01-23-2019 
* @Jira User Story  SC-1
* ------------------------------------------------------------------------------------------- 
* Test Class: CRP_ExternalNominationControllerTest
*/
public class CRP_ExternalNominationController {
    
    public Boolean IsSaveSuccessfull {get;set;}
    public Boolean isError {get;set;}
    public Boolean isFirstNameBlank {get;set;}
    public Boolean isLastNameBlank {get;set;}
    public Boolean isEmailBlank {get;set;}
    public Boolean isEmailValid {get;set;}
    public Boolean isPhoneNumberInvalid{get;set;}
    public String successMessage{get;set;}
    public String requestEmail{get;set;}
    public String phoneNumber{get;set;}
    public Boolean requestNumberLength{get;set;}
    public Reference_Request__c recRefRequest{get;set;}
    public List<Reference_Request__c> lstofOldRequests = new List<Reference_Request__c>();
    public List<Reference_Request__c> lstofNewRequests = new List<Reference_Request__c>();
    public List<Contact> listofContacts = new List<Contact>();
    public List<Customer_Reference_Program__c> listofCRP = new List<Customer_Reference_Program__c>();
    public String emailBody;
    public String emailSubject;
    
    // Constructer declaration....
    public CRP_ExternalNominationController(){
        successMessage = '';
        recRefRequest= new Reference_Request__c(); 
        //IsSaveSuccessfull = false;
        Id requestRecordTypeId;
        recRefRequest.RecordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId(); 
        
    }
    //Standard save funcion....
    public PageReference save() {
        
        If(recRefRequest.Phone_Number__c != null) {
            phoneNumber = recRefRequest.Phone_Number__c;
        }else{
            phoneNumber = '';
        }
        Boolean isEmailValid = validEmailCheck();
        if(String.isBlank(recRefRequest.Email__c) || String.isBlank(recRefRequest.First_Name__c)  || String.isBlank(recRefRequest.Last_Name__c) || isEmailValid == false){
            isError = true;
            
            if(String.isBlank(recRefRequest.First_Name__c)){
                isFirstNameBlank = true;                               
            }
            else{
                isFirstNameBlank = false; 
            }
            if(String.isBlank(recRefRequest.Last_Name__c)){
                isLastNameBlank = true;                               
            }
            else{
                isLastNameBlank = false; 
            }
        }
        
        else{
            isError = false;
        }
        
        Id requestRecordTypeId; 
        requestEmail = recRefRequest.Email__c;
        if(Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType) != null ){
            //getting recordtype for Reference Request custom object
            requestRecordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.NominationReferenceExternalRecordType).getRecordTypeId();   
        }
        If(requestEmail != ''){ 
            lstofOldRequests = [SELECT Id, Name, Email__c FROM Reference_Request__c WHERE Email__c = :requestEmail AND RecordTypeId = :requestRecordTypeId];
            
            listofContacts = [SELECT Id, Name, Email, AccountId FROM Contact WHERE Email = :requestEmail];
            
            listofCRP = [SELECT Id, Name, Contact__c FROM Customer_Reference_Program__c WHERE Contact__c IN :listofContacts];
        }
        if(isError==false){
            //checking if request exists for entered email.....
            if(lstofOldRequests.size() != null && lstofOldRequests.size()>0){ 
                //sending email to CRM......  
                IsSaveSuccessfull = true;
                successMessage = label.External_Nomination_Submit_Message; 
            }else{
                //checking if contact exists for entered email..... 
                if(listofContacts.size() != null && listofContacts.size()==1){
                    //creating Reference Request record mapped with contact Id.......
                    recRefRequest.RecordTypeId = requestRecordTypeId;
                    recRefRequest.Reference_Status__c = 'New';
                    recRefRequest.Account__c = listofContacts[0].AccountId;
                    recRefRequest.Contact__c = listofContacts[0].Id;
                    lstofNewRequests.add(recRefRequest);
                    
                    //checking if multiple contacts exist for entered email.....
                }else if(listofContacts.size() != null && listofContacts.size() > 1){
                    //creating Reference Request record mapped without contact Id.......
                    recRefRequest.RecordTypeId = requestRecordTypeId;
                    recRefRequest.Reference_Status__c = 'New';
                    lstofNewRequests.add(recRefRequest); 
                    
                }else {
                    //creating Reference Request record mapped without contact Id.......
                    recRefRequest.RecordTypeId = requestRecordTypeId;
                    recRefRequest.Reference_Status__c = 'New';
                    lstofNewRequests.add(recRefRequest);   
                }
            }
            // inserting Request record......
            try{ 
                if(lstofNewRequests != null && lstofNewRequests.size() > 0 && isError == false){ 
                    insert lstofNewRequests;  
                    IsSaveSuccessfull = true;
                    successMessage = label.External_Nomination_Submit_Message;
                    
                    //setting subject and body for email..... 
                    
                    //checking if contact exists for entered email..... 
                    if(listofContacts.size() != null && listofContacts.size()==1){ 
                        if(listofCRP.size() != null && listofCRP.size()>0){
                            emailSubject = 'New Torch Network Reference Nomination Request: Existing CRP';
                            emailBody= 'Submission Details:'+
                                '\nFirst Name: '+recRefRequest.First_Name__c+
                                '\nLast Name: '+recRefRequest.Last_Name__c+
                                '\nEmail: '+recRefRequest.Email__c+
                                '\nPhone: '+phoneNumber+
                                '\n'+
                                '\nMatching Contacts:'+
                                '\nContact: '+listofContacts[0].Name+' '+Label.Server_Address+''+listofContacts[0].Id+
                                '\nCRP: '+listofCRP[0].Name+' '+Label.Server_Address+''+listofCRP[0].Id+
                                '\n'+
                                '\nRequest: '+ Label.Server_Address+''+lstofNewRequests[0].Id;
                        }else{
                            emailSubject = 'New Torch Network Reference Nomination Request';
                            emailBody =   '\nFirst Name: '+recRefRequest.First_Name__c+
                                '\nLast Name: '+recRefRequest.Last_Name__c+
                                '\nEmail: '+recRefRequest.Email__c+
                                '\nPhone: '+phoneNumber+ 
                                '\n';
                            
                            for(Contact eachContact : listofContacts){
                                emailBody += '\nContact: '+Label.Server_Address+''+eachContact.Id;
                                
                            }
                            emailBody +='\n'+
                                '\nRequest: '+ Label.Server_Address+''+lstofNewRequests[0].Id;
                        }
                        
                        //checking if multiple contacts exist for entered email.....
                    }else if(listofContacts.size() != null && listofContacts.size() > 1){ 
                        emailSubject = 'New Torch Network Reference Nomination Request: Multiple Contacts Found';
                        emailBody = 'Submission Details:'+
                            '\nFirst Name: '+recRefRequest.First_Name__c+
                            '\nLast Name: '+recRefRequest.Last_Name__c+
                            '\nEmail: '+recRefRequest.Email__c+
                            '\nPhone: '+phoneNumber+
                            '\n'+
                            '\nMatching Contacts:';
                        
                        for(Contact eachContact : listofContacts){
                            emailBody += '\nContact: '+Label.Server_Address+''+eachContact.Id; 
                        } 
                        emailBody +='\n'+
                            '\nRequest: '+ Label.Server_Address+''+lstofNewRequests[0].Id;
                    }else {
                        //creating Reference Request record mapped without contact Id.......
                        emailSubject = 'New Torch Network Reference Nomination Request: No Contact Matched';
                        emailBody = '\nFirst Name: '+recRefRequest.First_Name__c+
                            '\nLast Name: '+recRefRequest.Last_Name__c+
                            '\nEmail: '+recRefRequest.Email__c+
                            '\nPhone: '+phoneNumber+
                            '\n'+
                            '\nRequest: '+ Label.Server_Address+''+lstofNewRequests[0].Id;
                        
                    }
                    sendEmail();
                }
                else if(lstofOldRequests.size() != null && lstofOldRequests.size()>0){ 
                    String requestEmailId = recRefRequest.Email__c;
                    emailSubject = 'New Torch Network Reference Nomination Request: Existing Request';
                    emailBody = Label.CRP_ExternalExistingRequestMessage+' '+requestEmailId+'\n Existing Request: '+ Label.Server_Address+''+lstofOldRequests[0].Id;
                    sendEmail();   
                }
            }     
            catch(DMLException ex){
                IsSaveSuccessfull = false; 
                lstofNewRequests.clear(); 
            }
            
        }
        return null; 
    }
    
    //method to send email.....
    public void sendEmail(){ 
        group eachGroupId = [SELECT Id FROM Group Where Name = :Label.CRP_CRM_GroupName];
        ST_getGroupofMembers objGroupMember = new ST_getGroupofMembers();
        Set <Id> setofUserId = objGroupMember.getAllPublicGroupUsers(eachGroupId.Id);
        List<String> toAddresses = new List<String>();
        for(User eachUser : [Select Id,email from user where Id IN :setofUserId]){
            toAddresses.add(eachUser.Email);
        } 
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        Messaging.SendEmailResult[] results;
        // Set recipients email IDs.....
        message.toAddresses =  toAddresses; 
        message.subject = emailSubject; 
        message.plainTextBody = emailBody;
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            try{
                results = Messaging.sendEmail(messages);
            }
        catch(Exception ex){
            system.debug('Exception is ::' + ex.getMessage());
        }
        if(results != null && results.size() > 0){
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: '
                             + results[0].errors[0].message);
            }
        }
        
    }
    
    public Boolean validEmailCheck(){ 
        Boolean res = true;
        if(String.isBlank(recRefRequest.Email__c) || recRefRequest.Email__c == ''){
            isEmailBlank = true; 
            isEmailValid = false; 
            res = false;
        } 
        else{ 
            
            String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
            Pattern MyPattern = Pattern.compile(emailRegex);
            Matcher MyMatcher = MyPattern.matcher(recRefRequest.Email__c);
            if (!MyMatcher.matches()) 
                res = false;
            if(res == false){
                isEmailValid = true;
                isEmailBlank = false; 
            }
            
        }
        return res;
    }
    
}