@isTest
public class TalkdeskAPIControllerTest{
    
    static testMethod void testDoPost(){
        // create Account record
        Account acc = utilityHelperTest.createAccount();
        insert acc;
        
        RestRequest request = new RestRequest();
        request.httpMethod = 'POST';
        
        RestResponse response = new RestResponse();
        
        // setting request
        RestContext.request = request;
        // setting response
        RestContext.response = response;
        
        TalkdeskAPIController.gsCreateActivity('testAddress',acc.id);
        response= RestContext.response;
        
        
        // creating JSONGenerator
        JSONGenerator gen = JSON.createGenerator(true);
        String gsURL = 'Error creating URL';
        String status = 'failure';
        List<Talkdesk_Gainsight_Settings__mdt> tcs = new List<Talkdesk_Gainsight_Settings__mdt>([SELECT Gainsight_URL_Base__c 
                                                                                                 FROM Talkdesk_Gainsight_Settings__mdt
                                                                                                 WHERE Gainsight_URL_Base__c!=NULL]);
        if(tcs.size()>0){
            status = 'success';
            gsURL = tcs[0].Gainsight_URL_Base__c + acc.Id;
        }
        // Write JSON Body
        gen.writeStartObject();
        gen.writeStringField('status', 'testAddress');
        gen.writeStringField('accountURL', gsURL);
        gen.writeEndObject();
        
        String pretty = gen.getAsString();
        
        system.assertEquals( Blob.valueOf(pretty) , response.responseBody);
        //system.assertEquals(tcs[0].Gainsight_URL_Base__c + acc.id, response.status);
    }     
}