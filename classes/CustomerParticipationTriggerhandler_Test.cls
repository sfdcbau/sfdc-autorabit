/*--------------------------------------------------------------------------------------------------------------------------------
Test class for CustomerParticipationTriggerhandler class. 
@Method :- handleInsertCustomerParticipation, handleUpdateCRP
@author  : Kamal
----------------------------------------------------------------------------------------------------------------------------------*/
@isTest
public class CustomerParticipationTriggerhandler_Test {
    
    //creating test data.....
    @testSetup static void setup() {
        Account testAccount = new Account();
        testAccount.name = 'test Account';
        insert testAccount;
        
        Id recordTypeId = Schema.SObjectType.Reference_Request__c.RecordTypeInfosByName.get(Label.SalesRequestRecordType).getRecordTypeId();
        Reference_Request__c testRequest = new Reference_Request__c();
        testRequest.RecordTypeId = recordTypeId;
        testRequest.Request_Type__c = 'Call';
        testRequest.Matching_Criteria__c ='Size';
        testRequest.Matching_Comments__c = 'test comments';
        testRequest.Request_Description__c = 'test';
        testRequest.References_Desired__c = 3;
        testRequest.Desired_Completion_Date__c = system.today();
        insert testRequest;
        
        Customer_Reference_Program__c  testcrp = new Customer_Reference_Program__c(); 
        testcrp.CRP_Status__c = 'Active';
        testcrp.Account__c = testAccount.Id;
        insert testcrp;
        
        Customer_Participation__c testcustmrparticipation = new Customer_Participation__c();
        testcustmrparticipation.Request_Id__c = testRequest.id;
        testcustmrparticipation.Customer_Reference_Id__c = testcrp.id;
        insert testcustmrparticipation;
        
    }
    //testing for already mapped CRP......
    @isTest static void testCPInsertFail() {
        Reference_Request__c eachReq = [SELECT Id, Name FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name,Activities_Asked_This_Year__c, Activities_Accepted_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        
        Customer_Participation__c cptoAdd = [SELECT Id, Request_Id__c, Customer_Reference_Id__c, Status__c
                                             FROM Customer_Participation__c 
                                             WHERE Request_Id__c =:eachReq.Id];
        
        list<Customer_Participation__c> listofCustmrParticipation = new list<Customer_Participation__c>();
        listofCustmrParticipation.add(cptoAdd);
        
        Test.startTest();
        CustomerParticipationTriggerhandler.handleInsertCustomerParticipation(listofCustmrParticipation); 
        System.assertEquals(listofCustmrParticipation.size(), 1);
        Test.stopTest();
        
    }
    //testing for new CRP to insert......
    @isTest static void testCPInsertPass() {
        Reference_Request__c eachReq = [SELECT Id, Name FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name,Activities_Asked_This_Year__c, Activities_Accepted_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        
        Customer_Participation__c cptoAdd = [SELECT Id, Request_Id__c, Customer_Reference_Id__c, Status__c
                                             FROM Customer_Participation__c 
                                             WHERE Customer_Reference_Id__c =:eachCRP.Id];
        
        list<Customer_Participation__c> listofCustmrParticipation = new list<Customer_Participation__c>();
        listofCustmrParticipation.add(cptoAdd);
        
        Account testAccount = new Account();
        testAccount.name = 'test Account';
        insert testAccount;
        
        Customer_Reference_Program__c  testcrp1 = new Customer_Reference_Program__c(); 
        testcrp1.CRP_Status__c = 'Active';
        testcrp1.Account__c = testAccount.Id;
        insert testcrp1;
        
        Customer_Participation__c testcustmrparticipation = new Customer_Participation__c();
        testcustmrparticipation.Request_Id__c = eachReq.id;
        testcustmrparticipation.Customer_Reference_Id__c = testcrp1.id;
        insert testcustmrparticipation;
        
        Customer_Participation__c cptoAdd1 = [SELECT Id, Request_Id__c, Customer_Reference_Id__c, Status__c
                                              FROM Customer_Participation__c 
                                              WHERE Customer_Reference_Id__c =:eachCRP.Id];
        listofCustmrParticipation.add(cptoAdd1);
        
        Test.startTest();
        CustomerParticipationTriggerhandler.handleInsertCustomerParticipation(listofCustmrParticipation); 
        System.assertEquals(listofCustmrParticipation.size(), 2); 
        Test.stopTest();
        
    }
    //testing for CRP count update when reference accepted......
    @isTest static void testCPUpdateAccepted() {
        Reference_Request__c eachReq = [SELECT Id, Name FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name,Activities_Asked_This_Year__c, Activities_Accepted_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        System.assertEquals(eachCRP.Activities_Asked_This_Year__c, 1); 
        Customer_Participation__c cptoAdd = [SELECT Id, Request_Id__c, Customer_Reference_Id__c, Status__c
                                             FROM Customer_Participation__c 
                                             WHERE Customer_Reference_Id__c =:eachCRP.Id];
        
        list<Customer_Participation__c> listofCustmrParticipation = new list<Customer_Participation__c>();
        listofCustmrParticipation.add(cptoAdd); 
        for(Customer_Participation__c cp : listofCustmrParticipation){
            cp.Status__c = 'Accepted';
        }
        update listofCustmrParticipation; 
        
        Test.startTest();
        CustomerParticipationTriggerhandler.handleUpdateCRP(listofCustmrParticipation); 
        Test.stopTest();
        Customer_Reference_Program__c eachCRPUpdated = [SELECT Id, Name, Activities_Asked_This_Year__c, Activities_Accepted_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        System.assertEquals(eachCRPUpdated.Activities_Accepted_This_Year__c, 2); 
    }
    
    //testing for CRP count update when reference declined......
    @isTest static void testCPUpdateDeclined() {
        Reference_Request__c eachReq = [SELECT Id, Name FROM Reference_Request__c LIMIT 1];
        Customer_Reference_Program__c eachCRP = [SELECT Id, Name,Activities_Asked_This_Year__c, Activities_Accepted_This_Year__c FROM Customer_Reference_Program__c LIMIT 1];
        System.assertEquals(eachCRP.Activities_Asked_This_Year__c, 1);
        System.assertEquals(eachCRP.Activities_Accepted_This_Year__c, 0);
        Customer_Participation__c cptoAdd = [SELECT Id, Request_Id__c, Customer_Reference_Id__c, Status__c
                                             FROM Customer_Participation__c 
                                             WHERE Customer_Reference_Id__c =:eachCRP.Id];
        
        list<Customer_Participation__c> listofCustmrParticipation = new list<Customer_Participation__c>();
        listofCustmrParticipation.add(cptoAdd);
        
        for(Customer_Participation__c cp : listofCustmrParticipation){
            cp.Status__c = 'Declined';
        }
        update listofCustmrParticipation;
        
        Test.startTest(); 
        CustomerParticipationTriggerhandler.handleUpdateCRP(listofCustmrParticipation);
        System.assertEquals(eachCRP.Activities_Asked_This_Year__c, 1);
        System.assertEquals(eachCRP.Activities_Accepted_This_Year__c, 0);
        Test.stopTest();
        
    }
}