public class ClonedOpportunityForPaymentsContrl {

     @AuraEnabled
    public static String cloneOpportunity(Id oppId){
        String resp = '';
        try{
            if(String.isNotBlank(oppId)){
                Opportunity objOpp = [SELECT ID,Name,AccountId,CloseDate,StageName,type,CampaignId,recordTypeId,Last_Contact_Date__c,Promotion_Offered__c,Description,Vendor_Used_Today__c FROM Opportunity WHERE Id =:oppId];
                Opportunity objOpportunity = new Opportunity();
                objOpportunity = objOpp.clone(false, false, false, false);
                /*objOpportunity.Name = objOpp.Name+' Clonned';//'Test 234';
                objOpportunity.CloseDate = objOpp.CloseDate;//system.today();
                objOpportunity.StageName = 'Stage Zero';
                objOpportunity.AccountId = objOpp.AccountId;//'0011800000ZeoyB';*/
                objOpportunity.Name = objOpportunity.Name+' Cloned';
                objOpportunity.StageName = 'Stage Zero';
                objOpportunity.Parent_Opportunity__c = oppId;
                objOpportunity.No_AE__c = true;
                insert objOpportunity;
                resp = 'Opprotunity Cloned Successfully with Name: '+objOpportunity.Name;
            }
        }catch(Exception ex){
            resp = 'ERROR: '+ex.getMessage();
        }
        return resp;
    }
}