/*
 #####################################################
 # Created By....................: Aman Gupta
 # Created Date................: 27 Dec, 2018
 # Last Modified By..........: Aman Gupta
 # Last Modified Date......: 27 Dec, 2018
 # Description...................: This is test class for ST_CustomerPartnerTrigger trigger and ST_CustomerPartnerTriggerHandler class.
 #####################################################
*/
@isTest
private class ST_CustomerPartnerTriggerTest {
    /**
     * @author : Aman Gupta
     * @date : 27 Dec, 2018
     * @description : This method create test data.
    */
    @testSetup static void setup() {
        Lead leadObj1 = new Lead(Company = 'TestCom1',Firstname = 'FN1', Lastname = 'LN1', Status = 'Open');
        leadObj1.Estimated_Total_Potential_Office_Users__c = 1;
        leadObj1.No_of_Technicians__c = 1;
        insert leadObj1;
        
        Account cAccObj1 = new Account(Name = 'TestCAcc1');
        cAccObj1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account cAccObj2 = new Account(Name = 'TestCAcc2');
        cAccObj2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account pAccObj1 = new Account(Name = 'TestPAcc1');
        pAccObj1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        pAccObj1.Assoc_Segment_Partner_Rank__c = 10;
        
        Account pAccObj2 = new Account(Name = 'TestPAcc2');
        pAccObj2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        pAccObj2.Assoc_Segment_Partner_Rank__c = 20;
        
        Account pAccObj3 = new Account(Name = 'TestPAcc3');
        pAccObj3.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        pAccObj3.Assoc_Segment_Partner_Rank__c = 5;
        
        Account pAccObj4 = new Account(Name = 'TestPAcc4');
        pAccObj4.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        pAccObj4.Assoc_Segment_Partner_Rank__c = 26;
        
        insert new List<Account> {cAccObj1, cAccObj2, pAccObj1, pAccObj2, pAccObj3, pAccObj4};
            
        Static_ID__c stObj1 = new Static_ID__c(Name = 'Account_Lead_Customer', RecordName__c = cAccObj1.Name, RecordID__c = cAccObj1.Id);
        
        insert stObj1;
    }
    
    /**
     * @author : Aman Gupta
     * @date : 27 Dec, 2018
     * @description : This method is to test ST_CustomerPartnerTriggerHandler updateAccountRanking function.
    */
    @isTest static void testUpdateAccountRanking() {
        Lead leadObj1 = [Select Id, Name from Lead where Name = 'FN1 LN1' LIMIT 1];
        List<Account> custAccObjList = [Select Id, Name from Account Where RecordType.Name = 'Customer' ORDER BY Name];
        List<Account> partAccObjList = [Select Id, Name from Account Where RecordType.Name = 'Partner' ORDER BY Name];
        
        CustomerPartner__c cpObj1 = new CustomerPartner__c(Customer__c = custAccObjList[0].Id, Partner__c = partAccObjList[0].Id, Status__c = 'Current', Lead__c = leadObj1.Id);
        CustomerPartner__c cpObj2 = new CustomerPartner__c(Customer__c = custAccObjList[0].Id, Partner__c = partAccObjList[1].Id, Status__c = 'Former',Lead__c = leadObj1.Id);
        CustomerPartner__c cpObj3 = new CustomerPartner__c(Customer__c = custAccObjList[1].Id, Partner__c = partAccObjList[2].Id, Status__c = 'None',Lead__c = leadObj1.Id);
        CustomerPartner__c cpObj4 = new CustomerPartner__c(Customer__c = custAccObjList[1].Id, Partner__c = partAccObjList[3].Id, Status__c = 'Current', Lead__c = leadObj1.Id);
        
        // insert CustomerPartner__c records
        insert new List<CustomerPartner__c> {cpObj1, cpObj2, cpObj3, cpObj4};
        
        // Query Account records
        custAccObjList = [Select Id, Name, Assoc_Segment_Ranking__c from Account ORDER BY Name];
        System.assertEquals(10, custAccObjList[0].Assoc_Segment_Ranking__c);
        System.assertEquals(5, custAccObjList[1].Assoc_Segment_Ranking__c);
        
        test.startTest();
        //update CustomerPartner__c records
        update new List<CustomerPartner__c> {cpObj1, cpObj2, cpObj3, cpObj4};
            
        // Query Account records
        custAccObjList = [Select Id, Name, Assoc_Segment_Ranking__c from Account ORDER BY Name];
        System.assertEquals(10, custAccObjList[0].Assoc_Segment_Ranking__c);
        System.assertEquals(5, custAccObjList[1].Assoc_Segment_Ranking__c);
        
        // delete CustomerPartner__c
        delete new List<CustomerPartner__c> {cpObj1, cpObj2, cpObj3, cpObj4};
        
        // Query Account records
        custAccObjList = [Select Id, Name, Assoc_Segment_Ranking__c from Account ORDER BY Name];
        System.assertEquals(null, custAccObjList[0].Assoc_Segment_Ranking__c);
        System.assertEquals(null, custAccObjList[1].Assoc_Segment_Ranking__c);
        test.stopTest();
    }
}