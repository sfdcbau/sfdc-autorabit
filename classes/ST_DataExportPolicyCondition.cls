global class ST_DataExportPolicyCondition implements TxnSecurity.PolicyCondition {
  public boolean evaluate(TxnSecurity.Event e) {
  
    Integer numberOfRecords = Integer.valueOf(e.data.get('NumberOfRecords'));
    String entityName = e.data.get('EntityName');
  
    if ('Lead'.equals(entityName) && numberOfRecords > Integer.valueof(label.ST_numberOfRecords)) {return true;}
    
    if ('Account'.equals(entityName) && numberOfRecords > Integer.valueof(label.ST_numberOfRecords)) {return true;}
    
    if ('Contact'.equals(entityName) && numberOfRecords > Integer.valueof(label.ST_numberOfRecords)) {return true;}
    
    if ('Opportunity'.equals(entityName) && numberOfRecords > Integer.valueof(label.ST_numberOfRecords)) {return true;}
  
    return false; 
  }
}