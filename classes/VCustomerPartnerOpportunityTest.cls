@isTest
private class VCustomerPartnerOpportunityTest 
{
    
    @IsTest(SeeAllData=false) 
    public static void testAssignNewOpportunityToCustomerPartner() 
    {
        // Add accounts
        List<Account> accountList = new List<Account>();
        
        Account acc0 = new Account();
        acc0.Name = 'Lead Customer 28731983';
        acc0.BillingCountry = 'United States';
        accountList.add(acc0);
        
        Account acc1 = new Account();
        acc1.Name = 'Test Customer 827312313';
        acc1.BillingCountry = 'United States';
        accountList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Test Customer 92834023';
        acc2.BillingCountry = 'United States';
        accountList.add(acc2);
        
        Account acc3 = new Account();
        acc3.Name = 'Test Partner 28731983';
        acc3.BillingCountry = 'United States';
        accountList.add(acc3);
        
        insert accountList;
        
        Static_ID__c csStatic = new Static_ID__c();
        csStatic.Name = 'Account_Lead_Customer';
        csStatic.RecordName__c = acc0.Name;
        csStatic.RecordID__c = acc0.Id;
        insert csStatic;
        
        // Add customer partners
        List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
        
        CustomerPartner__c cp1 = new CustomerPartner__c();
        cp1.Customer__c = accountList[1].Id;
        cp1.Partner__c = accountList[2].Id;
        customerpartnerList.add(cp1);
        
        CustomerPartner__c cp2 = new CustomerPartner__c();
        cp2.Customer__c = accountList[1].Id;
        cp2.Partner__c = accountList[3].Id;
        customerpartnerList.add(cp2);
        
        insert customerpartnerList;
        
        Test.startTest();
        
        System.assertEquals(0, ([Select Id From CustomerPartner__c Where Opportunity__c != null]).size());        
        
        // Insert opportunity
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp 12810281';
        opp.AccountId = accountList[1].Id;
        opp.StageName = 'Stage Zero';
        opp.CloseDate = Date.today();
        insert opp;
        
        System.assertEquals(2, ([Select Id From CustomerPartner__c Where Opportunity__c != null]).size());
        
        Test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testCustomerPartnerOpportunityClosedLost() 
    {
        // Add accounts
        List<Account> accountList = new List<Account>();
        
        Account acc0 = new Account();
        acc0.Name = 'Lead Customer 28731983';
        acc0.BillingCountry = 'United States';
        accountList.add(acc0);
        
        Account acc1 = new Account();
        acc1.Name = 'Test Customer 827312313';
        acc1.BillingCountry = 'United States';
        acc1.Software_newpicklist__c = 'Acowin';
        accountList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Test Customer 92834023';
        acc2.BillingCountry = 'United States';
        acc2.Software_newpicklist__c = 'Acowin';
        accountList.add(acc2);
        
        insert accountList;
        
        Static_ID__c csStatic = new Static_ID__c();
        csStatic.Name = 'Account_Lead_Customer';
        csStatic.RecordName__c = acc0.Name;
        csStatic.RecordID__c = acc0.Id;
        insert csStatic;
        
        // Add customer partners
        List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
        
        CustomerPartner__c cp1 = new CustomerPartner__c();
        cp1.Customer__c = accountList[1].Id;
        cp1.Partner__c = accountList[2].Id;
        customerpartnerList.add(cp1);
        
        insert customerpartnerList;     
        
        // Insert opportunity
        List<Opportunity> oppportunityList = new List<Opportunity>();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp 12810281';
        opp1.AccountId = accountList[1].Id;
        opp1.StageName = 'Stage Zero';
        opp1.CloseDate = Date.today();
        oppportunityList.add(opp1);
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test Opp 74654675';
        opp2.AccountId = accountList[1].Id;
        opp2.StageName = 'Stage Zero';
        opp2.CloseDate = Date.today();
        opp2.Competing_Software__c = 'AppointmentPlus';
        oppportunityList.add(opp2);
        
        insert oppportunityList;
        
        Test.startTest();
        
        List<CustomerPartner__c> cpList = [Select Id, Opportunity__c From CustomerPartner__c Where Opportunity__c != null];
        
        if(!cpList.isEmpty())
        {
            List<Opportunity> oppList = [Select Id, StageName From Opportunity Where Id = :cpList[0].Opportunity__c];
            
            if(!oppList.isEmpty())
            {
                oppList[0].StageName = 'Closed Lost';
                //oppList[0].Disposition_Code__c = 'Grace Period';
                //oppList[0].Legacy_Opportunity_Id__c = '12312329791';
                oppList[0].Competing_Software__c = 'Other';
                update oppList;
            }
        }
        
        Test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testCopyOpportunities() 
    {   
        
        Test.startTest();
        
        // Add accounts
        List<Account> accountList = new List<Account>();
        
        Account acc0 = new Account();
        acc0.Name = 'Lead Customer 28731983';
        acc0.BillingCountry = 'United States';
        accountList.add(acc0);
        
        Account acc1 = new Account();
        acc1.Name = 'Test Customer 827312313';
        acc1.BillingCountry = 'United States';
        accountList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Test Partner 92834023';
        acc2.BillingCountry = 'United States';
        accountList.add(acc2);
        
        Account acc3 = new Account();
        acc3.Name = 'Test Partner 059875906784';
        acc3.BillingCountry = 'United States';
        accountList.add(acc3);
        
        insert accountList;
        
        // Insert opportunity
        List<Opportunity> oppportunityList = new List<Opportunity>();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp 12810281';
        opp1.AccountId = accountList[2].Id;
        opp1.StageName = 'Stage Zero';
        opp1.CloseDate = Date.today();
        oppportunityList.add(opp1);
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test Opp 74654675';
        opp2.AccountId = accountList[2].Id;
        opp2.StageName = 'Stage Zero';
        opp2.CloseDate = Date.today();
        oppportunityList.add(opp2);
        
        insert oppportunityList;
        
        Static_ID__c csStatic = new Static_ID__c();
        csStatic.Name = 'Account_Lead_Customer';
        csStatic.RecordName__c = acc0.Name;
        csStatic.RecordID__c = acc0.Id;
        insert csStatic;
        
        // Add customer partners
        List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
        
        CustomerPartner__c cp1 = new CustomerPartner__c();
        cp1.Customer__c = accountList[1].Id;
        cp1.Partner__c = accountList[2].Id;
        customerpartnerList.add(cp1);
        
        insert customerpartnerList;  
        
        System.assertEquals(2, ([Select Id From Customer_Partner_Opportunity__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        Opportunity opp3 = new Opportunity();
        opp3.Name = 'Test Opp 20394852';
        opp3.AccountId = accountList[2].Id;
        opp3.StageName = 'Stage Zero';
        opp3.CloseDate = Date.today();
        insert opp3;
        
        System.assertEquals(3, ([Select Id From Customer_Partner_Opportunity__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        opp2.AccountId = accountList[0].Id;
        update opp2;
        
        System.assertEquals(2, ([Select Id From Customer_Partner_Opportunity__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        cp1.Partner__c = accountList[3].Id;
        update customerpartnerList;
        
        System.assertEquals(0, ([Select Id From Customer_Partner_Opportunity__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        Test.stopTest();
    }
    
    @IsTest(SeeAllData=false) 
    public static void testCopyOpportunitiesBatch() 
    {   
        Test.startTest();
        
        // Add accounts
        List<Account> accountList = new List<Account>();
        
        Account acc0 = new Account();
        acc0.Name = 'Lead Customer 28731983';
        acc0.BillingCountry = 'United States';
        accountList.add(acc0);
        
        Account acc1 = new Account();
        acc1.Name = 'Test Customer 827312313';
        acc1.BillingCountry = 'United States';
        accountList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Test Partner 92834023';
        acc2.BillingCountry = 'United States';
        accountList.add(acc2);
        
        Account acc3 = new Account();
        acc3.Name = 'Test Partner 059875906784';
        acc3.BillingCountry = 'United States';
        accountList.add(acc3);
        
        insert accountList;
        
        List<Opportunity> oppportunityList = new List<Opportunity>();
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp 12810281';
        opp1.AccountId = accountList[2].Id;
        opp1.StageName = 'Stage Zero';
        opp1.CloseDate = Date.today();
        oppportunityList.add(opp1);
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test Opp 74654675';
        opp2.AccountId = accountList[2].Id;
        opp2.StageName = 'Stage Zero';
        opp2.CloseDate = Date.today();
        oppportunityList.add(opp2);
        
        insert oppportunityList;
        
        Static_ID__c csStatic = new Static_ID__c();
        csStatic.Name = 'Account_Lead_Customer';
        csStatic.RecordName__c = acc0.Name;
        csStatic.RecordID__c = acc0.Id;
        insert csStatic;
        
        // Add customer partners
        List<CustomerPartner__c> customerpartnerList = new List<CustomerPartner__c>();
        
        CustomerPartner__c cp1 = new CustomerPartner__c();
        cp1.Customer__c = accountList[1].Id;
        cp1.Partner__c = accountList[2].Id;
        customerpartnerList.add(cp1);
        
        insert customerpartnerList;  
        
        System.assertEquals(2, ([Select Id From Customer_Partner_Opportunity__c Where Customer_Partner__c = :customerpartnerList[0].Id]).size());
        
        Opportunity opp3 = new Opportunity();
        opp3.Name = 'Test Opp 20394852';
        opp3.AccountId = accountList[2].Id;
        opp3.StageName = 'Stage Zero';
        opp3.CloseDate = Date.today();
        insert opp3;
        
        VCustomerPartnerAddOpportunityBatch bc2 = new VCustomerPartnerAddOpportunityBatch();
        Database.executeBatch(bc2, 200);
        
        Test.stopTest();
    }
}