global class VScheduleCaseTeamMemberBatch implements Schedulable 
{
	public VScheduleCaseTeamMemberBatch()
    {
    	
    }
    
    public Interface IScheduleDispatched 
    { 
    	void execute(SchedulableContext sc); 
    } 

    global void execute(SchedulableContext sc) 
    {
        Type targettype = Type.forName('VScheduleCaseTeamMemberBatchHandler');   
        if(targettype != null) 
        {
            IScheduleDispatched obj = (IScheduleDispatched)targettype.NewInstance();
            obj.execute(sc);  
        } 
    } 
}