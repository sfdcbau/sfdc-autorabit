public without sharing class VCaseTriggerDispatcher 
{
	
	public void dispatch() 
	{
		if(Trigger.isBefore && Trigger.isInsert)
			executeBeforeInsert();
        
		if(Trigger.isAfter && Trigger.isInsert)
			executeAfterInsert();
    }
    
    private void executeBeforeInsert()
    {
		setVictorOps();
    }
    
    private void executeAfterInsert()
    {
		addCaseTeamMember();
    }
	
	private void addCaseTeamMember()
	{
        Set<Id> caseSet = new Set<Id>();
        
		for(Case c : (List<Case>)Trigger.new)
        {
            caseSet.add(c.Id);
        }
        
        try
        {
            VCaseUtil.addCaseTeamMember(caseSet);
        }
        catch(DmlException de)
        {
            List<Case_Team_Member_Batch__c> ctmbList = new List<Case_Team_Member_Batch__c>();
            
            for(Id caseId : caseSet)
            {
                Case_Team_Member_Batch__c ctmb = new Case_Team_Member_Batch__c();
                ctmb.Case__c = caseId;
                ctmbList.add(ctmb);
            }
            
            if(!ctmbList.isEmpty())
                insert ctmbList;
        }
	}
    
    /*
		Set VictorOps for based on Support Hours
		
	*/
    private void setVictorOps()
    {
        List<BusinessHours> businessHoursList = [Select Id,Name From BusinessHours Where Name='Support Hours' Limit 1];
        if(!businessHoursList.isEmpty())
        {
            for(Case c : (List<Case>)Trigger.new)
            {
                c.VictorOps__c = BusinessHours.isWithin(businessHoursList[0].Id, Datetime.now());
            }
        }
    }
}