({
    doInit : function(component, event) {
        // Get Current User
        var getCurrentUserAction = component.get("c.getCurrentUser");
    	getCurrentUserAction.setCallback(this, function(a){
    		component.set("v.currentUser", a.getReturnValue());
        });
		$A.enqueueAction(getCurrentUserAction);
        
        // Get Case
        var getCaseAction = component.get("c.getCase");
        getCaseAction.setParams({
        	caseId : component.get("v.CaseId")
    	});
        getCaseAction.setCallback(this, function(a) {
            component.set("v.thisCase", a.getReturnValue());
            
            var escalateable = false;
            
            if(!a.getReturnValue().IsEscalated && !(a.getReturnValue().Status == 'Closed' || a.getReturnValue().Status == 'Solved')){
                escalateable = true;
            }
            
        	component.set("v.isEscalateable", escalateable);
        });
        $A.enqueueAction(getCaseAction);
    },
    showModal : function(component, event, helper) {
        $("#backGroundSectionId").show();
        $("#modalEscalateCase").show();
    },
    showModalBox : function(component, event, helper) {
        $("#backGroundSectionId").hide();
        $("#modalEscalateCase").hide();
    },
    escalateCaseSave : function(component, event, helper){
        $(".escalateError").html("");
        $(".escalateError").css('display', 'none');
        
        var escalationDetails = component.find("escalationDetails").get("v.value");
        
        if(escalationDetails !== undefined && escalationDetails != ''){
            var getCaseAction = component.get("c.escalateCase");
            getCaseAction.setParams({
                caseId : component.get("v.CaseId"),
                escalationDetails : escalationDetails
            });
           
            getCaseAction.setCallback(this, function(a) {
                component.set("v.thisCase", a.getReturnValue());
                
                var escalateable = false;
                
                if(!a.getReturnValue().IsEscalated && !(a.getReturnValue().Status == 'Closed' || a.getReturnValue().Status == 'Solved')){
                    escalateable = true;
                }
                
                component.set("v.isEscalateable", escalateable);
                
                console.log("Escalate State: "+a.getState());
                if (a.getState() === "SUCCESS") {
                    $("#backGroundSectionId").hide();
                    $("#modalEscalateCase").hide();
                } else if (a.getState() === "ERROR") {
                    $(".escalateError").html("Error: " + a.getError());
        			$(".escalateError").css('display', 'block');
                }
            });
            $A.enqueueAction(getCaseAction);
        }
        else{
            $(".escalateError").html("Escalation Details is required");
        	$(".escalateError").css('display', 'block');
        }
    }
})