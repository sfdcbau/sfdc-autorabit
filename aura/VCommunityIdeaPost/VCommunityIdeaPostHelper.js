({
    GetGuestUser : function(component, event) {
        var isGuestUserMethod = component.get("c.isGuestUser");
        isGuestUserMethod.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.isGuestUser", a.getReturnValue());
        });
        $A.enqueueAction(isGuestUserMethod);
    },
    GetCurrentUserType : function(component, event) {
        var CurrentUserTypeAction = component.get("c.getUserType");
        CurrentUserTypeAction.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.currentUserType", a.getReturnValue());
        });
        $A.enqueueAction(CurrentUserTypeAction);
    },
	GetZone : function(component, event){
        var getZones = component.get("c.getCurrentCommunityZones");
        getZones.setCallback(this, function(a){
            console.log('got zones');
            component.set("v.zoneIdNames", a.getReturnValue());
        });
        $A.enqueueAction(getZones);
    },
	GetCategories : function(component, event){
        var getCategories = component.get("c.getIdeaCategorySelectOptions");
        getCategories.setCallback(this, function(a){
            console.log('got categories');
            component.set("v.categorySelectOptions", a.getReturnValue());
        });
        $A.enqueueAction(getCategories);
    },
	GetStatuses : function(component, event){
        var getStatuses = component.get("c.getIdeaStatusSelectOptions");
        getStatuses.setCallback(this, function(a){
            console.log('got statuses');
            component.set("v.statusSelectOptions", a.getReturnValue());
        });
        $A.enqueueAction(getStatuses);
    },
    GetTopicName : function(component, event){
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === 'topic') {
                component.set("v.topic", sParameterName[1]);
                component.set("v.selectedCategories", sParameterName[1]);
            }
        }
    }
})