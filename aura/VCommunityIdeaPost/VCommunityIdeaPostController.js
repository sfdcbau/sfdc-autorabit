({
	doInit : function(component, event, helper) {
        helper.GetZone(component, event);
        helper.GetGuestUser(component, event);
        helper.GetCurrentUserType(component, event);
        helper.GetCategories(component, event);
        helper.GetStatuses(component, event);
        helper.GetTopicName(component, event);
    },
    checkForSimilarIdeas : function(component, event, helper){
        var title = component.find("title").get("v.value");
        if(typeof(title) != 'undefined' && title != ''){
        	$('.loadingSimilarIdeas').css('display', 'block');
            $('.titleError').css('display', 'none');
            var action = component.get("c.findSimilarIdeas");
            action.setParams({"title" : title});
            action.setCallback(this, function(a) {
                component.set("v.similarIdeas", a.getReturnValue());
                $('.loadingSimilarIdeas').css('display', 'none');
            });
            $A.enqueueAction(action);
        }
    },
    submitPostIdea : function(component, event, helper) {
        var title = component.find("title").get("v.value");
        var description = component.find("description").get("v.value");
        var categories = component.get("v.selectedCategories");
        
        var status = '';
        
        if(typeof(component.find("status")) != 'undefined'){
            status = component.find("status").get("v.value");
        }
        
        console.log('description: ' + description);
        console.log('cat: ' + categories);
        
        //var zone = component.find("zone").get("v.value");
        var zones = component.get("v.zoneIdNames");
        var zone = zones[0].Id;
        console.log('Zone: ' +zone);
        if(zone != ''){
            $('.zoneError').css('display', 'none');
            if(typeof(title) != 'undefined' && title != ''){
                console.log('Enter');
                
                $('.titleError').css('display', 'none');
                var action = component.get("c.postIdea");
                action.setParams({"title" : title, 
                                  "description" : description, 
                                  "zone" : zone, 
                                  "categories" : categories,
                                  "status" : status});
                action.setCallback(this, function(a) {
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/idea/"+a.getReturnValue()
                    });
                    urlEvent.fire();
                });
                $A.enqueueAction(action);
            }else{
                $('.titleError').css('display', 'block');
            }
        }else{
            $('.zoneError').css('display', 'block');
        }
    },
    cancelPostIdea : function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/ideas"
        });
        urlEvent.fire();
    },
    onCategorySelectChange : function(component, event, helper){
        component.set("v.selectedCategories", component.find("categories").get("v.value"));
    },
    onStatusSelectChange : function(component, event, helper){
        component.set("v.selectedStatus", component.find("status").get("v.value"));
    }
})