({
    doInit : function(component, event, helper) {
        component.set("v.nextPage", 0);
        helper.LoadGroupList(component, event);
        helper.LoadTotalIdeas(component, event);
        helper.LoadHasGroupPermission(component, event);
    },
    firstPage : function(component, event, helper){
        component.set("v.nextPage", 0);
        helper.LoadGroupList(component, event);
    },
    previousPage : function(component, event, helper){
        var nextPage = component.get("v.nextPage");
        if(component.get("v.pageUpdated")){
            nextPage = nextPage-1;
        }
        component.set("v.nextPage", nextPage);
        helper.LoadGroupList(component, event);
    },
    nextPage : function(component, event, helper){
        var nextPage = component.get("v.nextPage");
        if(component.get("v.pageUpdated")){
            nextPage = nextPage+1;
        }
        component.set("v.nextPage", nextPage);
        helper.LoadGroupList(component, event);
    },
    lastPage : function(component, event, helper){
        var recordsPerPage = component.get("v.recordsPerPage");
        component.set("v.nextPage", Math.floor(component.get("v.totalGroups")/recordsPerPage));
        helper.LoadGroupList(component, event);
    },
    showModal : function(component, event, helper) {
        $("#backGroundSectionId").show();
        $("#modalNewGroup").show();
    },
    showModalBox : function(component, event, helper) {
        $("#backGroundSectionId").hide();
        $("#modalNewGroup").hide();
    },
    saveNewGroup : function(component, event, helper){
        $(".modalError").html("");
        $(".modalError").css('display', 'none');
        
        var newgroupName = component.find("newgroupName").get("v.value");
        var newgroupDescription = component.find("newgroupDescription").get("v.value");
        var newgroupCollaborationType = component.find("newgroupCollaborationType").get("v.value");
        
        if(newgroupName !== undefined && newgroupName != ''){
            var newAction = component.get("c.saveNewCollaborationGroup");
            newAction.setParams({
                newgroupName : newgroupName,
                newgroupDescription : newgroupDescription,
                newgroupCollaborationType : newgroupCollaborationType
            });
           
            newAction.setCallback(this, function(a) {
                console.log("State: "+a.getState());
                if (a.getState() === "SUCCESS") {
                    $("#backGroundSectionId").hide();
                    $("#modalNewGroup").hide();
                    
                    var groupDetailPath = component.get("v.groupDetailPath");
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/" + groupDetailPath + a.getReturnValue()
                    });
                    urlEvent.fire();
                } else if (a.getState() === "ERROR") {
                    $(".modalError").html("Error: " + a.getError());
        			$(".modalError").css('display', 'block');
                }
            });
            $A.enqueueAction(newAction);
        }
        else{
            $(".modalError").html("Group Name is required");
        	$(".modalError").css('display', 'block');
        }
    }
})