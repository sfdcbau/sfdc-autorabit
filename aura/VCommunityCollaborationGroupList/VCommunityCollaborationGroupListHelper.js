({
	LoadHasGroupPermission : function(component, event) {
        var newAction = component.get("c.hasCreateGroupPermission");
        newAction.setCallback(this, function(a) {
            component.set("v.hasCreatePermission", a.getReturnValue());
        });
        $A.enqueueAction(newAction);
	},
	LoadGroupList : function(component, event) {
        var newAction = component.get("c.getGroupList");
        newAction.setParams({
            recordsPerPage : component.get("v.recordsPerPage"),
            selPageNo : component.get("v.nextPage"),
            includePublic : component.get("v.includePublic"),
            includePrivate : component.get("v.includePrivate"),
            includeUnlisted : component.get("v.includeUnlisted")
        });
        newAction.setCallback(this, function(a) {
            component.set("v.collaborationGroupList", a.getReturnValue());
            component.set("v.pageUpdated", true);
            if(a.getReturnValue().length != 0){
                
            }else{
                component.set("v.nextPage", component.get("v.nextPage")-1);
            }
            //console.log(a.getReturnValue());
        });
        $A.enqueueAction(newAction);
	},
    LoadTotalIdeas : function(component, event){
        var newAction = component.get("c.getTotalGroups");
        newAction.setParams({
            recordsPerPage : component.get("v.recordsPerPage"),
            selPageNo : component.get("v.nextPage"),
            includePublic : component.get("v.includePublic"),
            includePrivate : component.get("v.includePrivate"),
            includeUnlisted : component.get("v.includeUnlisted")
        });
        newAction.setCallback(this, function(a) {
            var recordsPerPage = component.get("v.recordsPerPage");
            var totalGroups = a.getReturnValue();
            var totalGroupPages = Math.ceil(totalGroups/recordsPerPage);
            component.set("v.totalGroups", a.getReturnValue());
            component.set("v.totalGroupPages", totalGroupPages);
        });
        $A.enqueueAction(newAction);
        
    }
})