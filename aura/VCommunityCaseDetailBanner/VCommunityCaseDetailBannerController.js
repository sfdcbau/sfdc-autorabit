({
    doInit : function(component, event) {
        // Get Current User
        var getCurrentUserAction = component.get("c.getCurrentUser");
    	getCurrentUserAction.setCallback(this, function(a){
    		component.set("v.currentUser", a.getReturnValue());
        });
		$A.enqueueAction(getCurrentUserAction);
        
        // Get Case
        var getCaseAction = component.get("c.getCase");
        getCaseAction.setParams({
        	caseId : component.get("v.CaseId")
    	});
        getCaseAction.setCallback(this, function(a) {
            component.set("v.thisCase", a.getReturnValue());
        });
        $A.enqueueAction(getCaseAction);
    }
})