({
    doInit : function(component, event, helper) {
        helper.GetAnnouncementList(component, event);
        helper.GetTotalAnnouncement(component, event);
    },
    firstPage : function(component, event, helper){
        component.set("v.nextPage", 0);
        helper.GetAnnouncementList(component, event);
    },
    previousPage : function(component, event, helper){
        var nextPage = component.get("v.nextPage");
        if(component.get("v.announcementPageUpdated")){
            nextPage = nextPage-1;
        }
        component.set("v.nextPage", nextPage);
        helper.GetAnnouncementList(component, event);
    },
    nextPage : function(component, event, helper){
        var nextPage = component.get("v.nextPage");
        if(component.get("v.announcementPageUpdated")){
            nextPage = nextPage+1;
        }
        component.set("v.nextPage", nextPage);
        helper.GetAnnouncementList(component, event);
    },
    lastPage : function(component, event, helper){
        var announcementsPerPage = component.get("v.announcementsPerPage");
        component.set("v.nextPage", Math.floor(component.get("v.totalAnnouncements")/announcementsPerPage));
        helper.GetAnnouncementList(component, event);
    }
})