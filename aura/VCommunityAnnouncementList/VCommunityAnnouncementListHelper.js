({
    GetAnnouncementList : function(component, event) {
        var AnnouncementListAction = component.get("c.getAnnouncements");
        AnnouncementListAction.setParams({
            announcementsPerPage : component.get("v.announcementsPerPage"),
            selPageNo : component.get("v.nextPage")
        });
        AnnouncementListAction.setCallback(this, function(a) {
            component.set("v.announcementList", a.getReturnValue());
            component.set("v.announcementPageUpdated", true);
            if(a.getReturnValue().length != 0){
                
            }else{
                //component.set("v.announcementPageUpdated", false);
                component.set("v.nextPage", component.get("v.nextPage")-1);
            }
            console.log(a.getReturnValue());
        });
        $A.enqueueAction(AnnouncementListAction);
	},
    GetTotalAnnouncement : function(component, event){
        var totalAnnouncementAction = component.get("c.getTotalAnnouncements");
        totalAnnouncementAction.setCallback(this, function(a) {
            var announcementsPerPage = component.get("v.announcementsPerPage");
            var totalAnnouncements = a.getReturnValue();
            var totalAnnouncementPages = Math.ceil(totalAnnouncements/announcementsPerPage);
            component.set("v.totalAnnouncements", a.getReturnValue());
            component.set("v.announcementsPerPage", announcementsPerPage);
            console.log('totalAnnouncements: '+a.getReturnValue());
            console.log('totalAnnouncementPages: '+totalAnnouncementPages);
        });
        $A.enqueueAction(totalAnnouncementAction);
        
    }
})