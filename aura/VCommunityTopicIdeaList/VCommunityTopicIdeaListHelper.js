({
    GetGuestUser : function(component, event) {
        var isGuestUserMethod = component.get("c.isGuestUser");
        isGuestUserMethod.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.isGuest", a.getReturnValue());
        });
        $A.enqueueAction(isGuestUserMethod);
    },
	IdeasList : function(component, event) {
        var newAction = component.get("c.getTopicIdeasList");
        newAction.setParams({
            ideasPerPage : component.get("v.ideasPerPage"),
            selPageNo : component.get("v.nextPage"),
            topicId : component.get("v.topicId")
        });
        newAction.setCallback(this, function(a) {
            component.set("v.IdeasList", a.getReturnValue());
            component.set("v.ideasPageUpdated", true);
            if(a.getReturnValue().length != 0){
                
            }else{
                //component.set("v.ideasPageUpdated", false);
                component.set("v.nextPage", component.get("v.nextPage")-1);
            }
            console.log(a.getReturnValue());
        });
        $A.enqueueAction(newAction);
	},
    TotalIdeas : function(component, event){
        var totalIdeasAction = component.get("c.getTotalTopicIdeas");
        totalIdeasAction.setParams({
            ideasPerPage : component.get("v.ideasPerPage"),
            selPageNo : component.get("v.nextPage"),
            topicId : component.get("v.topicId")
        });
        totalIdeasAction.setCallback(this, function(a) {
            var ideasPerPage = component.get("v.ideasPerPage");
            var totalIdeas = a.getReturnValue();
            var totalIdeaPages = Math.ceil(totalIdeas/ideasPerPage);
            component.set("v.TotalIdeas", a.getReturnValue());
            component.set("v.totalIdeaPages", totalIdeaPages);
            console.log('TotalIdeas: '+a.getReturnValue());
            console.log('totalIdeaPages: '+totalIdeaPages);
        });
        $A.enqueueAction(totalIdeasAction);
        
    }
})