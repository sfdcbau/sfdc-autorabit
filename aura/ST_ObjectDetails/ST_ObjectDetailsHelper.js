({
    doInitHelper: function(component, helper) {
        try {
            // calling apex controller
            var action = component.get("c.getObBDDetails");
            action.setParams({
                "recordId": component.get("v.recordId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //console.log('response = '  + response.getReturnValue());
                    helper.createJSON(component, helper, response.getReturnValue());
                }
                else {
                    let message = 'Error: ';
                    let errors = response.getError();
                    // Retrieve the error message sent by the server
                    if (errors && errors.length > 0)
                        message = message + errors[0].message;
                    console.log("Error in loading Lightning Component: ST_ObjectDetails. " + message);
                    component.set("v.recordError", "Error in loading Lightning Component: ST_ObjectDetails. " + message);
                    component.set("v.spinnervisible",false);
                }
            });
            $A.enqueueAction(action);
        }catch (ex) {
            console.log("Error in loading Lightning Component: ST_ObjectDetails. " + ex);
            component.set("v.recordError", "Error in loading Lightning Component: ST_ObjectDetails. " + ex);
            component.set("v.spinnervisible",false);
        }
    },
    
    createJSON: function(component, helper, responseObj) {
        var fieldJson = [
            {
                "sObjectName": "Onboarding__c",
                "sections": [
                    {
                        "sectionName": "Account Details",
                        "iconName" : "standard:account",
                        "fields": [
                            
                            {"fieldlabel": "Customer Status", "fieldtype": "Text", "fieldName": "Account__r.Customer_Status_picklist__c"},
                            {"fieldlabel": "Tenant Name", "fieldtype": "Text", "fieldName": "Account__r.Tenant_Name__c"},
                            {"fieldlabel": "Onboarder", "fieldtype": "Url", "fieldName": "Account__r.Onboarder__r.Name", "fieldId": "Account__r.Onboarder__c"},
                            {"fieldlabel": "Primary Contact", "fieldtype": "Url", "fieldName": "Account__r.Primary_Contact__r.Name", "fieldId": "Account__r.Primary_Contact__c"},
                            {"fieldlabel": "Success Rep", "fieldtype": "Url", "fieldName": "Account__r.Success_Rep__r.Name", "fieldId": "Account__r.Success_Rep__c"},
                            {"fieldlabel": "Managed Tech Count", "fieldtype": "Text", "fieldName": "Account__r.Managed_Tech_Count__c"},
                            {"fieldlabel": "OB Live Date", "fieldtype": "Date", "fieldName": "Account__r.OB_Live_Date__c"},
                            {"fieldlabel": "OB Projected Go Live Date", "fieldtype": "Date", "fieldName": "Account__r.OB_Projected_Go_Live_Date__c"}
                        ]
                    },
                    {
                        "sectionName": "Related Opportunity Details", 
                        "iconName" : "standard:opportunity",
                        "fields": [
                            {"fieldlabel": "Opportunity Name", "fieldtype": "Url", "fieldName": "Opportunity__r.Name", "fieldId": "Opportunity__c"},
                            {"fieldlabel": "Opportunity Stage", "fieldtype": "Text", "fieldName": "Opportunity__r.StageName"},
                            {"fieldlabel": "Opportunity Record Type", "fieldtype": "Text", "fieldName": "Opportunity__r.RecordType.Name"},
                            {"fieldlabel": "Opportunity Owner", "fieldtype": "Url", "fieldName": "Opportunity__r.Owner.Name", "fieldId": "Opportunity__r.OwnerId"},
                            {"fieldlabel": "Primary Channel", "fieldtype": "Text", "fieldName": "Opportunity__r.Primary_Channel__c"},
                            {"fieldlabel": "Close Date", "fieldtype": "Date", "fieldName": "Opportunity__r.CloseDate"},
                            {"fieldlabel": "Closed/Lost Reason", "fieldtype": "Text", "fieldName": "Opportunity__r.Closed_Lost_Reason__c"},
                            {"fieldlabel": "Closed Lost Detail", "fieldtype": "Text", "fieldName": "Opportunity__r.Closed_Lost_Detail__c"}
                        ]
                    },
                    {
                        "sectionName": "Financials",
                        "iconName" : "standard:opportunity",
                        "fields": [
                            {"fieldlabel": "STP CC Monthly Volume", "fieldtype": "Text", "fieldName": "Opportunity__r.STP_CC_Monthly_Volume__c"},
                            {"fieldlabel": "Basis Points (BPS)", "fieldtype": "Text", "fieldName": "Opportunity__r.Basis_Points_BPS__c"},
                            {"fieldlabel": "STP CC MRR", "fieldtype": "Text", "fieldName": "Opportunity__r.STP_CC_MRR__c"},
                            {"fieldlabel": "Previous Processor BPS", "fieldtype": "Text", "fieldName": "Opportunity__r.Previous_Processor_BPS__c"},
                            {"fieldlabel": "# of Free Swipers", "fieldtype": "Text", "fieldName": "Opportunity__r.of_Free_Swipers__c"},
                            {"fieldlabel": "Qualified Rate", "fieldtype": "Text", "fieldName": "Opportunity__r.Qualified_Rate__c"},
                            {"fieldlabel": "Pricing Type", "fieldtype": "Text", "fieldName": "Opportunity__r.Pricing_Type__c"},
                            {"fieldlabel": "Mid Qualified Rate", "fieldtype": "Text", "fieldName": "Opportunity__r.Mid_Qualified_Rate__c"},
                            {"fieldlabel": "Passthrough Dues and Assessments?", "fieldtype": "Text", "fieldName": "Opportunity__r.Passthrough_Dues_and_Assessments__c"},
                            {"fieldlabel": "Non Qualified Rate", "fieldtype": "Text", "fieldName": "Opportunity__r.Non_Qualified_Rate__c"},
                            {"fieldlabel": "Monthly Service Fee", "fieldtype": "Text", "fieldName": "Opportunity__r.Monthly_Service_Fee__c"},
                            {"fieldlabel": "Per Item", "fieldtype": "Text", "fieldName": "Opportunity__r.Per_Item__c"}
                        ]
                    }
                ]
			},
			{
				"sObjectName": "Opportunity",
				"sections": [
                    {
                    	"sectionName": "Account Details",
                    	"iconName" : "standard:account",
                    	"fields": [
                    		{"fieldlabel": "Customer Status", "fieldtype": "Text", "fieldName": "Account.Customer_Status_picklist__c"},
                    		{"fieldlabel": "Tenant Name", "fieldtype": "Text", "fieldName": "Account.Tenant_Name__c"},
                    		{"fieldlabel": "Onboarder Name", "fieldtype": "Url", "fieldName": "Account.Onboarder__r.Name", "fieldId": "Account.Onboarder__c"},
                    		{"fieldlabel": "Primary Contact", "fieldtype": "Url", "fieldName": "Account.Primary_Contact__r.Name", "fieldId": "Account.Primary_Contact__c"},
                    		{"fieldlabel": "Success Rep", "fieldtype": "Url", "fieldName": "Account.Success_Rep__r.Name", "fieldId": "Account.Success_Rep__c"},
                    		{"fieldlabel": "Managed Tech Count", "fieldtype": "Text", "fieldName": "Account.Managed_Tech_Count__c"},
                    		{"fieldlabel": "Onboarding OB Discovery Call Date", "fieldtype": "Date", "fieldName": "", "fieldvalue": (!$A.util.isUndefinedOrNull(responseObj)) ? responseObj["OB Discovery Call Date"] : ""},
                    		{"fieldlabel": "Onboarding OB Live Date", "fieldtype": "Date", "fieldName": "", "fieldvalue": (!$A.util.isUndefinedOrNull(responseObj)) ? responseObj["OB Live Date"] : ""},
                            {"fieldlabel": "OB Live Date", "fieldtype": "Date", "fieldName": "Account.OB_Live_Date__c"},
                            {"fieldlabel": "OB Projected Go Live Date", "fieldtype": "Date", "fieldName": "Account.OB_Projected_Go_Live_Date__c"}
                		]
            		}
            	]
            }
        ];
        
        var isCompInit = false;
        var fetchFields = [];
        for(var i = 0; i < fieldJson.length; i++) {
            if(component.get("v.sObjectName") == fieldJson[i].sObjectName) {
                for(var j = 0; j < fieldJson[i].sections.length; j++) {
                    for(var k = 0; k < fieldJson[i].sections[j].fields.length; k++) {
                        if(!$A.util.isEmpty(fieldJson[i].sections[j].fields[k].fieldName))
                            fetchFields.push(fieldJson[i].sections[j].fields[k].fieldName);
                        if(!$A.util.isEmpty(fieldJson[i].sections[j].fields[k].fieldId))		
                            fetchFields.push(fieldJson[i].sections[j].fields[k].fieldId);
                    }
                }
                isCompInit = true;
                break;
            }
        }
        
        if(isCompInit) {
            component.set("v.fieldJson", fieldJson);
            //console.log("fetchFields = " + fetchFields);
            component.set("v.fields", fetchFields);
            component.set("v.isCompInit", isCompInit);
        }else {
            console.log("Error in loading Lightning Component: ST_ObjectDetails for " + component.get("v.sObjectName") + " object.");
            component.set("v.recordError", "Error in loading Lightning Component: ST_ObjectDetails for " + component.get("v.sObjectName") + " object.");
            component.set("v.spinnervisible",false);
        }
    }
})