({
	doInit: function(component, event, helper) {
        //console.log('doInit');
        component.set("v.spinnervisible",true);
        
        if(component.get("v.sObjectName") == "Opportunity" && !$A.util.isEmpty(component.get("v.recordId"))) {
            helper.doInitHelper(component, helper);
        }
        else {
            helper.createJSON(component, helper);
        }
    },
    
    handleRecordUpdated: function(component, event, helper) {
        //console.log('handleRecordUpdated');
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            //console.log("Record is loaded successfully.");
            
            var fieldJson = component.get("v.fieldJson");
            var fetchFields = [];
            var sections = [];
            for(var i = 0; i < fieldJson.length; i++) {
                if(component.get("v.sObjectName") == fieldJson[i].sObjectName) {
                    for(var j = 0; j < fieldJson[i].sections.length; j++) {
                        var fields = [];
                        for(var k = 0; k < fieldJson[i].sections[j].fields.length; k++) {
                            fields.push(
                                {"fieldlabel": fieldJson[i].sections[j].fields[k].fieldlabel,
                                 "fieldtype": fieldJson[i].sections[j].fields[k].fieldtype,
                                 "fieldvalue": !$A.util.isEmpty(fieldJson[i].sections[j].fields[k].fieldName) ? component.get("v.simpleRecord." + fieldJson[i].sections[j].fields[k].fieldName) : fieldJson[i].sections[j].fields[k].fieldvalue,
                                 "fieldId": !$A.util.isEmpty(fieldJson[i].sections[j].fields[k].fieldId) ? component.get("v.simpleRecord." + fieldJson[i].sections[j].fields[k].fieldId) : "",
                                 //"fieldValue": component.get("v.simpleRecord." + fieldJson[i].sections[j].fields[k].fieldName),
                                 //"fieldId": component.get("v.simpleRecord." + fieldJson[i].sections[j].fields[k].fieldId)
                                })
                            ;
                        }
                        sections.push({"sectionName": fieldJson[i].sections[j].sectionName, "iconName": fieldJson[i].sections[j].iconName, "fields": fields});
                    }
                    break;
                }
            }
            component.set("v.dataWrap", {"sections": sections});
        }else if(eventParams.changeType === "CHANGED") {
        }else if(eventParams.changeType === "REMOVED") {
        }else if(eventParams.changeType === "ERROR") {
            console.log("Error: " + component.get("v.recordError"));
            component.set("v.recordError", "Error: " + component.get("v.recordError"));
        }
        component.set("v.spinnervisible",false);
    },
    
	navigateToRecord: function(component, event, helper) {
        var recordId = event.target.getAttribute("id");
        //console.log("recordId = " + recordId);
        var navEvt = $A.get("e.force:navigateToSObject");
        if(!$A.util.isEmpty(recordId) && navEvt) {
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    }
})