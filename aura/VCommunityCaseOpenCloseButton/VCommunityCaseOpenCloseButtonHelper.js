({
	loadCurrentUser : function(component, event){
        var getCurrentUserAction = component.get("c.getCurrentUser");
    	getCurrentUserAction.setCallback(this, function(a){
    		component.set("v.currentUser", a.getReturnValue());
        });
		$A.enqueueAction(getCurrentUserAction);
    },
    loadCase : function(component, event){
        var getCaseAction = component.get("c.getCase");
        getCaseAction.setParams({
        	caseId : component.get("v.CaseId")
    	});
        
        getCaseAction.setCallback(this, function(a) {
            component.set("v.thisCase", a.getReturnValue());
            this.loadIsSolved(component, event);
        });
        $A.enqueueAction(getCaseAction);
    },
    loadIsSolved : function(component, event){
        var mycase = component.get("v.thisCase");
        var isSolved = false;
        
        if(mycase.IsClosed || (mycase.Status.toLowerCase() == 'solved') ){
            isSolved = true;
        }
        
        component.set("v.isSolved", isSolved);
    }
})