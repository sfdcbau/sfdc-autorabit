({
    doInit : function(component, event, helper) {
        helper.loadCurrentUser(component, event);
        helper.loadCase(component, event);
    },
    closeCase : function(component, event, helper){
        var getCaseAction = component.get("c.openCloseCase");
        getCaseAction.setParams({
        	caseId : component.get("v.CaseId"),
            statusName : component.get("v.CloseStatusName")
    	});
       
        getCaseAction.setCallback(this, function(a) {
            component.set("v.thisCase", a.getReturnValue());
            helper.loadIsSolved(component, event);
        });
        $A.enqueueAction(getCaseAction);
    },
    openCase : function(component, event, helper){
        var getCaseAction = component.get("c.openCloseCase");
        getCaseAction.setParams({
        	caseId : component.get("v.CaseId"),
            statusName : component.get("v.OpenStatusName")
    	});
       
        getCaseAction.setCallback(this, function(a) {
            component.set("v.thisCase", a.getReturnValue());
            helper.loadIsSolved(component, event);
        });
        $A.enqueueAction(getCaseAction);
    }
})