({
    doInit : function(component, event, helper) {
        component.set("v.saveBtnFlag",false);
        var action = component.get("c.getProducts");
        // Register the callback function
        action.setCallback(this, function(response) {
            component.set("v.listOfProducts",response.getReturnValue());
        });
        // Invoke the service
        $A.enqueueAction(action);
        
        var action1 = component.get("c.getOppStageName");
        action1.setParams({
            "oppId": component.get("v.recordId")
        });
        // Register the callback function
        action1.setCallback(this, function(response) {
            var resp = response.getReturnValue();
            if(typeof(resp) !== 'undefined' && resp !== null && resp !== '' && resp === 'Closed Won'){
                component.set("v.saveBtnFlag",false);
            }else{
                component.set("v.saveBtnFlag",true);
            }
        });
        // Invoke the service
        $A.enqueueAction(action1);
    },
    
    selectProduct : function(component, event, helper) {
        var chkVal = event.getSource().get("v.value");
        var index = event.getSource().get("v.text");
        var prodIds = component.get("v.productIds");
        if(chkVal == true)
            prodIds.push(index);
        else if(chkVal == false)
            prodIds.pop(index);
    },
    
    save : function(component, event, helper) {
        var prodIds = component.get("v.productIds");
        var action = component.get("c.cloneOpportunity");
        action.setParams({
            "oppId": component.get("v.recordId"),
            "action": "clone",
            "productIds" : JSON.stringify(prodIds)
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var resp = response.getReturnValue();
            if(typeof(resp) !== 'undefined' && resp !== null && resp !== '' && !resp.includes('ERROR:')){
                window.location.href = '/'+resp;
            }else{
                component.set("v.displayMessage",resp);   
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    }
})