({
    doInit: function(component, event, helper) {
        //console.log('doInit');
        
        // fetch Custom Labels
        var onboardstatus = (!$A.util.isEmpty($A.get("$Label.c.ST_On_Boarding_Status")) ? $A.get("$Label.c.ST_On_Boarding_Status") : "Rejected");
        var onboardstatusList = ["Rejected"];
        if(!onboardstatusList.includes(onboardstatus))
            onboardstatus = onboardstatusList[0];
        
        var teamrole = (!$A.util.isEmpty($A.get("$Label.c.ST_Team_Role")) ? $A.get("$Label.c.ST_Team_Role") : "Sales Engineer");
        var teamroleList = ["Sales Engineer", "Sales Rep"];
		if(!teamroleList.includes(teamrole))
            teamrole = teamroleList[0];
        
        var mgtechqt = (!$A.util.isEmpty($A.get("$Label.c.ST_Managed_Tech_Quantity")) ? $A.get("$Label.c.ST_Managed_Tech_Quantity") : 0);
        
        //set Managed Tech Quantity
        component.find("mgtechqt").set("v.value" , mgtechqt)
        
        // Initialize input select options
        var optsOB = [
            { "class": "optionClass", label: "Rejected", value: "Rejected", selected: ("Rejected" == onboardstatus ? "true" : "false") },
            //{ "class": "optionClass", label: "Churn", value: "Churn", selected: ("Churn" == onboardstatus ? "true" : "false") }
        ];
        
        var optsTR = [
            { "class": "optionClass", label: "Sales Engineer", value: "Sales Engineer", selected: ("Sales Engineer" == teamrole ? "true" : "false") },
            { "class": "optionClass", label: "Sales Rep", value: "Sales Rep", selected: ("Sales Rep" == teamrole ? "true" : "false")  }
        ];
        
        component.set("v.optionsOB", optsOB);
        component.set("v.optionsTR", optsTR);
        
        //set enddate date
        var todaydate = new Date();
        var enddate = todaydate.getFullYear() + "-" + (todaydate.getMonth() + 1 < 10 ? "0" : "") + (todaydate.getMonth() + 1)  + "-" + (todaydate.getDate() < 10 ? "0" : "") + todaydate.getDate();
        
        //set startdate date
        todaydate.setDate(todaydate.getDate() - 89);
        var startdate = todaydate.getFullYear() + "-" + (todaydate.getMonth() + 1 < 10 ? "0" : "") + (todaydate.getMonth() + 1)  + "-" + (todaydate.getDate() < 10 ? "0" : "") + todaydate.getDate();
        
        component.find("startdate").set("v.value", startdate);
        component.find("enddate").set("v.value", enddate);
        
        helper.fetchRecordsHelper(component, helper, startdate, enddate, onboardstatus, teamrole, mgtechqt);
        //helper.fetchRecordsHelper(component, helper, component.find("startdate").get("v.value"), component.find("enddate").get("v.value"), component.find("onboardstatus").get("v.value"), component.find("teamrole").get("v.value"));
    },
    
    fetchRecords: function(component, event, helper) {
        var validRequest = true;
        
        //validate startdate
        if(component.find("startdate").get("v.value") == '' || component.find("startdate").get("v.value") == null) {
            //console.log('empty startdate');
            //$A.util.addClass(component.find("startdate"), "text-border-color_red");
            $A.util.removeClass(component.find("errorText1"), "slds-hide");
            validRequest = false;
        }
        //validate enddate
        if(component.find("enddate").get("v.value") == '' || component.find("enddate").get("v.value") == null) {
            //console.log('empty enddate');
            //$A.util.addClass(component.find("enddate"), "text-border-color_red");
            $A.util.removeClass(component.find("errorText2"), "slds-hide");
            validRequest = false;
        }
        //validate enddate not less than startdate
        if(validRequest && (component.find("startdate").get("v.value") > component.find("enddate").get("v.value"))) {
            $A.util.removeClass(component.find("errorText3"), "slds-hide");
            validRequest = false;
        }
        //validate Managed Tech Quantity
        if(validRequest) {
            var validity = component.find("mgtechqt").get("v.validity");
            //console.log("check validity = " + validity.valid);
            if(!validity.valid)
                validRequest = false;
        }
        
        if(validRequest) {
            component.set("v.teamMem", null);
    		helper.fetchRecordsHelper(component, helper, component.find("startdate").get("v.value"), component.find("enddate").get("v.value"), component.find("onboardstatus").get("v.value"), component.find("teamrole").get("v.value"), component.find("mgtechqt").get("v.value"));
        }
    },
    
    onDateChange: function(component, event, helper) {
        if(event.getSource().get("v.name") == "startdate") {
            if(component.find("startdate").get("v.value") == '' || component.find("startdate").get("v.value") == null)
                $A.util.removeClass(component.find("errorText1"), "slds-hide");
            else
                $A.util.addClass(component.find("errorText1"), "slds-hide");    
        }
        if(event.getSource().get("v.name") == "enddate") {
            if(component.find("enddate").get("v.value") == '' || component.find("enddate").get("v.value") == null)
                $A.util.removeClass(component.find("errorText2"), "slds-hide");
            else
                $A.util.addClass(component.find("errorText2"), "slds-hide");
        }
        if(!$A.util.isEmpty(component.find("startdate").get("v.value")) && !$A.util.isEmpty(component.find("enddate").get("v.value")) && (component.find("startdate").get("v.value") > component.find("enddate").get("v.value")))
            $A.util.removeClass(component.find("errorText3"), "slds-hide");
        else
            $A.util.addClass(component.find("errorText3"), "slds-hide");
    },
    
})