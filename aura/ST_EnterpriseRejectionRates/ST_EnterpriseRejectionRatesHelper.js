({
    fetchRecordsHelper: function(component, helper, startdate, enddate, onBDStatus, teamrole, mgtechqt) {
        component.set("v.spinnervisible",true);
        var action = component.get("c.getTeamMembers");
        action.setParams({
            "sDate": startdate,
            "eDate": enddate,
            "onBDStatus": onBDStatus,
            "teamrole": teamrole,
            "managTechQuantity" : (!$A.util.isEmpty(mgtechqt) ? mgtechqt : 0)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log('response = '  + response.getReturnValue());
                component.set("v.teamMem", response.getReturnValue());
                helper.createComponent(component, helper);
            }
            else {
                let message = 'Error: ';
                let errors = response.getError();
                // Retrieve the error message sent by the server
                if (errors && errors.length > 0)
                    message = message + errors[0].message;
				console.log(message);
            }
            component.set("v.spinnervisible",false);
        });
        $A.enqueueAction(action);
    },
    
    createComponent : function(component, helper) {
        // dynamically creating components
        $A.createComponent(
            "c:ST_EnterpriseRejectionRatesTable",
            {
                "aura:id": "datatable",
                "totalteamMem": component.get("v.teamMem")
            },
            function(newComponent, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    component.set("v.body", newComponent);
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    }
})