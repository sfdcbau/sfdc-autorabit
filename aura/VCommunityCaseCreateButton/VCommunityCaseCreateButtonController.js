({
	doInit : function(component, event, helper){
    	var action = component.get("c.getCurrentUser");
    	action.setCallback(this, function(data){
    		component.set("v.currentUser", data.getReturnValue());
        });
		$A.enqueueAction(action);
	}
})