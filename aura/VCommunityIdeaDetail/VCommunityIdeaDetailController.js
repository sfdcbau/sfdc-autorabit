({
    doInit : function(component, event, helper) {
        helper.GetUserId(component, event);
		helper.GetIdea(component, event);
        helper.GetIdeaComments(component, event);
        helper.GetIsGuestUser(component, event);
        helper.GetIsAdminUser(component, event);
        helper.GetSubscription(component, event);
        helper.GetResponseComment(component, event);
    },
    saveComment : function(component, event){
        var yourComment = component.find("IdeaComment").get("v.value");
		console.log(yourComment);
        
        var getIdeaAction = component.get("c.postComment");
        getIdeaAction.setParams({
        	IdeaId : component.get("v.ideaId"),
            CommentBody : yourComment
    	});
       
        getIdeaAction.setCallback(this, function(a) {
            component.set("v.comments", a.getReturnValue());
            component.find("IdeaComment").set("v.value", "");
            var IdeaList = component.get("v.thisIdea");
            IdeaList.NumComments = a.getReturnValue().length;
            component.set("v.thisIdea", IdeaList);
        });
        $A.enqueueAction(getIdeaAction);
    },
    unlikeComment : function(component, event){
        var VoteId = event.target.getAttribute('data-idValue');

        var action = component.get("c.unlikeThisComment");
        action.setParams({IdeaId : component.get("v.ideaId"),"VoteId" : VoteId});
        action.setCallback(this, function(a) {
            component.set("v.comments", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    likeComment : function(component, event){
        var commentId = event.target.getAttribute('data-idValue');

        var action = component.get("c.likeThisComment");
        action.setParams({IdeaId : component.get("v.ideaId"),"commentId" : commentId});
        action.setCallback(this, function(a) {
            component.set("v.comments", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    deleteComment : function(component, event){
        var commentId = event.target.getAttribute('data-idValue');

        var action = component.get("c.deleteThisComment");
        action.setParams({IdeaId : component.get("v.ideaId"),"commentId" : commentId});
        action.setCallback(this, function(a) {
            component.set("v.comments", a.getReturnValue());
            var IdeaList = component.get("v.thisIdea");
            IdeaList.NumComments = a.getReturnValue().length;
            component.set("v.thisIdea", IdeaList);
        });
        $A.enqueueAction(action);
    },
    promoteAnIdea : function(component, event){
        var IdeaId = event.target.getAttribute('data-idValue');
        console.log('Idea Id: '+IdeaId);
        
        var action = component.get("c.promoteFromIdeaDetails");
        action.setParams({"IdeaId" : IdeaId});
        action.setCallback(this, function(a) {
            component.set("v.thisIdea", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    demoteAnIdea : function(component, event){
        var IdeaId = event.target.getAttribute('data-idValue');
        console.log('Idea Id: '+IdeaId);
        
        var action = component.get("c.demoteFromIdeaDetails");
        action.setParams({"IdeaId" : IdeaId});
        action.setCallback(this, function(a) {
            component.set("v.thisIdea", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    subscribeAnIdea : function(component, event, helper){
        var getIdeaAction = component.get("c.subscribeIdea");
        getIdeaAction.setParams({
        	ideaId : component.get("v.ideaId")
    	});
        getIdeaAction.setCallback(this, function(a) {
            helper.GetSubscription(component, event);
        });
        $A.enqueueAction(getIdeaAction);
    },
    subscribeAnIdeaOnProductTeamUpdates : function(component, event, helper){
        var getIdeaAction = component.get("c.subscribeIdeaOnProductTeamUpdates");
        getIdeaAction.setParams({
        	ideaId : component.get("v.ideaId")
    	});
        getIdeaAction.setCallback(this, function(a) {
            helper.GetSubscription(component, event);
        });
        $A.enqueueAction(getIdeaAction);
    },
    unsubscribeAnIdea : function(component, event, helper){
        var getIdeaAction = component.get("c.unsubscribeIdea");
        getIdeaAction.setParams({
        	ideaId : component.get("v.ideaId")
    	});
        getIdeaAction.setCallback(this, function(a) {
            helper.GetSubscription(component, event);
        });
        $A.enqueueAction(getIdeaAction);
    },
    setIdeaResponseComment : function(component, event, helper){
        var commentId = event.target.getAttribute('data-idValue');
        var getIdeaAction = component.get("c.setResponseComment");
        getIdeaAction.setParams({
            IdeaId : component.get("v.ideaId"),
            "commentId" : commentId
        });
        getIdeaAction.setCallback(this, function(a) {
            component.set("v.responseComment", a.getReturnValue());
            helper.GetIdeaComments(component, event);
        });
        $A.enqueueAction(getIdeaAction);
    },
    removeIdeaResponseComment : function(component, event, helper){
        var getIdeaAction = component.get("c.removeResponseComment");
        getIdeaAction.setParams({
            IdeaId : component.get("v.ideaId")
        });
        getIdeaAction.setCallback(this, function(a) {
            component.set("v.responseComment", a.getReturnValue());
            helper.GetIdeaComments(component, event);
        });
        $A.enqueueAction(getIdeaAction);
    }
})