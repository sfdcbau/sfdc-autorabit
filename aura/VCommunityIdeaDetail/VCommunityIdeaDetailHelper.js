({
	GetIsGuestUser : function(component, event) {
        var isGuestUserMethod = component.get("c.isGuestUser");
        isGuestUserMethod.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.isGuest", a.getReturnValue());
        });
        $A.enqueueAction(isGuestUserMethod);
    },
    GetIsAdminUser : function(component, event) {
        var isAdminUserMethod = component.get("c.isAdminUser");
        isAdminUserMethod.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.isAdmin", a.getReturnValue());
        });
        $A.enqueueAction(isAdminUserMethod);
    },
    GetUserId : function(component, event) {
        var UserIdAction = component.get("c.getUserId");
        UserIdAction.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.userId", a.getReturnValue());
        });
        $A.enqueueAction(UserIdAction);
    },
    GetSubscription : function(component, event) {
        var SubscriptionAction = component.get("c.getIdeaSubscription");
        SubscriptionAction.setParams({
        	ideaId : component.get("v.ideaId")
    	});
        SubscriptionAction.setCallback(this, function(a) {
            component.set("v.ideaSubscription", a.getReturnValue());
        });
        $A.enqueueAction(SubscriptionAction);
    },
    GetIdea : function(component, event) {
        var getIdeaAction = component.get("c.getIdea");
        getIdeaAction.setParams({
        	IdeaId : component.get("v.ideaId")
    	});
        getIdeaAction.setCallback(this, function(a) {
            component.set("v.thisIdea", a.getReturnValue());
        });
        $A.enqueueAction(getIdeaAction);
    },
    GetResponseComment : function(component, event) {
        var getResponseCommentAction = component.get("c.getResponseComment");
        getResponseCommentAction.setParams({
        	IdeaId : component.get("v.ideaId")
    	});
        getResponseCommentAction.setCallback(this, function(a) {
            component.set("v.responseComment", a.getReturnValue());
        });
        $A.enqueueAction(getResponseCommentAction);
    },
    GetIdeaComments : function(component, event) {
        var getCommentsAction = component.get("c.getComment");
        getCommentsAction.setParams({
        	IdeaId : component.get("v.ideaId")
    	});
        getCommentsAction.setCallback(this, function(a) {
            component.set("v.comments", a.getReturnValue());
        });
        $A.enqueueAction(getCommentsAction);
    }
})