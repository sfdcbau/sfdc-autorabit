({
	doInit: function(component, event, helper) {
        //console.log('doInit');
        component.set("v.spinnervisible",true);
    },
    
    handleRecordUpdated: function(component, event, helper) {
        //console.log('handleRecordUpdated');
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            //console.log("Record is loaded successfully.");
        }else if(eventParams.changeType === "CHANGED") {
        }else if(eventParams.changeType === "REMOVED") {
        }else if(eventParams.changeType === "ERROR") {
            console.log("Error: " + component.get("v.recordError"));
            component.set("v.recordError", "Error: " + component.get("v.recordError"));
        }
        component.set("v.spinnervisible",false);
    },
    
    copyCCData: function(component, event, helper) {
        //console.log('copyCCData');
        if(!$A.util.isEmpty(component.get("v.simpleRecord.Opportunity__c"))) {
            if(!$A.util.isUndefinedOrNull(component.get("v.simpleRecord.DBA__c"))) {
                component.set("v.showCCCopyBtn", false);
            }
            else {
                //Display the warning message
                helper.showToastMessage(component, helper, 'DBA is empty for this Onboarding!', 'warning');
            }
        }
        else {
            console.log("Error: " + "Opportunity record doesn't exist for this Credit Card Onboarding");
            component.set("v.recordError", "Error: " + "Opportunity record doesn't exist for this Credit Card Onboarding");
        }
    },
    continueBtn: function(component, event, helper) {
        if (event.getSource().get("v.label") == "Yes") {
            helper.doInitHelper(component, helper);
        }
        else {
            component.set("v.showCCCopyBtn", true);
        }
    }
})