({
	doInitHelper: function(component, helper) {
        try {
            component.set("v.spinnervisible",true);
            
            // calling apex controller
            var action = component.get("c.updateCheckOnBD");
            action.setParams({
                "oppRecId": component.get("v.simpleRecord.Opportunity__c"),
                "ccDBA": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.DBA__c")) ? component.get("v.simpleRecord.DBA__c") : '',
                "dataMap": {
                    "Typeform_Recieved_Date__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Typeform_Recieved_Date__c")) ? component.get("v.simpleRecord.Typeform_Recieved_Date__c") : null,
                    "Primary_Contact__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Primary_Contact__c")) ? component.get("v.simpleRecord.Primary_Contact__c") : null,
                    "Supporting_Docs__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Supporting_Docs__c")) ? component.get("v.simpleRecord.Supporting_Docs__c") : '',
                    "Business_License_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Business_License_Received__c")) ? component.get("v.simpleRecord.Business_License_Received__c") : null,
                    "Driver_s_License_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Driver_s_License_Received__c")) ? component.get("v.simpleRecord.Driver_s_License_Received__c") : null,
                    "SSN_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.SSN_Received__c")) ? component.get("v.simpleRecord.SSN_Received__c") : null,
                    "Voided_Check_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Voided_Check_Received__c")) ? component.get("v.simpleRecord.Voided_Check_Received__c") : null,
                    "CC_Statement_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.CC_Statement_Received__c")) ? component.get("v.simpleRecord.CC_Statement_Received__c") : null,
                    "Bank_Statements_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Bank_Statements_Received__c")) ? component.get("v.simpleRecord.Bank_Statements_Received__c") : null,
                    "Financial_Statements_Received__c": !$A.util.isUndefinedOrNull(component.get("v.simpleRecord.Financial_Statements_Received__c")) ? component.get("v.simpleRecord.Financial_Statements_Received__c") : null
                }
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //console.log('response = '  + response.getReturnValue());
                    if(response.getReturnValue() == "Check Onboarding's Updated Successfully!") {
                        //Display the success message
                        helper.showToastMessage(component, helper, "Check Onboarding's Updated Successfully!", 'success');
                    } else if(response.getReturnValue() == "DBA doesn\'t match with Check Onboarding\'s!") {
                        //Display the error message
                        helper.showToastMessage(component, helper, "DBA doesn\'t match with Check Onboarding\'s!", 'error');
                    }
                }
                else {
                    let message = 'Error: ';
                    let errors = response.getError();
                    // Retrieve the error message sent by the server
                    if (errors && errors.length > 0)
                        message = message + errors[0].message;
                    console.log("Error in loading Lightning Component: ST_UpdateCheckonBDFromCC. " + message);
                    component.set("v.recordError", "Error in loading Lightning Component: ST_UpdateCheckonBDFromCC. " + message);
                }
                component.set("v.spinnervisible",false);
                component.set("v.showCCCopyBtn", true);
            });
            $A.enqueueAction(action);
        }catch (ex) {
            console.log("Error in loading Lightning Component: ST_UpdateCheckonBDFromCC. " + ex);
            component.set("v.recordError", "Error in loading Lightning Component: ST_UpdateCheckonBDFromCC. " + ex);
            component.set("v.spinnervisible",false);
            component.set("v.showCCCopyBtn", true);
        }
	},
    
    showToastMessage: function(component, helper, message, type) {
        var toastEvent = $A.get("event.force:showToast");
        toastEvent.setParams({
            title : '',
            message: message,
            duration:'5000',
            key: 'info_alt',
            type: type,
            mode: 'pester'
        });
        toastEvent.fire();
    }
})