({  //This function is invoked during the page load
	doInit : function(component, event, helper) {
		var action = component.get("c.isPaymentOpportunityExist");
        console.log("v.Name")
        action.setParams({
            "accountId": component.get("v.recordId")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            //Getting the response from Apex controller whether Payment Opportunity exist or not
            if(response.getReturnValue() == true)
            {
                var changeElement = component.find("ligntningCard");
                // by using $A.util.toggleClass add-remove slds-hide class
                $A.util.toggleClass(changeElement, "slds-hide");
            }
            else
            {               
                component.set("v.AddOpportunityMessage","Please click the button to add Payment Opportunity");
            }
        });
        // Invoke the controller
        $A.enqueueAction(action);
	},
    
    handleClick : function(cmp, event) {
        var attributeValue = cmp.get("v.AddOpportunityMessage");
        console.log("current text: " + attributeValue);
		var flow = cmp.find("flowId");
         var inputVariables = [
            { name : "vAccountID", type : "String", value: cmp.get("v.recordId")},
            { name : "vAcctName", type : "String", value: cmp.get("v.record").Name}  
        ];
        // Invoke the below flow to create Oportunity and OB records
        flow.startFlow("Create_Platform_Opportunities",inputVariables);
        var target = event.getSource();
        let button = event.getSource();
        button.set('v.disabled',true); // Disable the button once the flow is invoked
    },
    //This method will be invoked when flow is completed
    handleStatusChange : function (component, event) {
    if(event.getParam("status") === "FINISHED_SCREEN") {
        component.set("v.AddOpportunityMessage", 'Payment Opportunity Added Successfully'); 

        var changeElement = component.find("flowId");
        var flowElement = component.find("btnAddPaymentOpportunity");

        // by using $A.util.toggleClass add-remove slds-hide class
        $A.util.toggleClass(changeElement, "slds-hide");
        $A.util.toggleClass(flowElement, "slds-hide");
        //Display the success message
        var toastEvent = $A.get("event.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Payment Opportunity Added Successfully!',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        $A.get('e.force:refreshView').fire();
    }else{        
        var toastEvent = $A.get("event.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message: 'Payment Opportunity is not created!',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    }
}
})