({
    GetCurrentUser : function(component, event) {
        var getCurrentUserAction = component.get("c.getCurrentUser");
    	getCurrentUserAction.setCallback(this, function(a){
    		component.set("v.currentUser", a.getReturnValue());
        });
		$A.enqueueAction(getCurrentUserAction);
    },
	CaseCommentList : function(component, event) {
        var getCaseCommentsAction = component.get("c.getCaseComments");
        
        console.log('caseId: ' + component.get("v.caseId"));
        console.log('noOfComments: ' + component.get("v.defaultNumberOfCommentsShown"));
        
        getCaseCommentsAction.setParams({
            caseId : component.get("v.caseId"),
            noOfComments : component.get("v.defaultNumberOfCommentsShown")
        });
        getCaseCommentsAction.setCallback(this, function(a) {
            if(a.getReturnValue().length != 0){
                component.set("v.caseCommentList", a.getReturnValue());
            }
            console.log(a.getReturnValue());
        });
        $A.enqueueAction(getCaseCommentsAction);
	},
    TotalCaseComments : function(component, event){
        var totalCaseCommentsAction = component.get("c.getTotalCaseComments");
        totalCaseCommentsAction.setParams({
            caseId : component.get("v.caseId")
        });
        totalCaseCommentsAction.setCallback(this, function(a) {
            component.set("v.totalCaseComments", a.getReturnValue());
        });
        $A.enqueueAction(totalCaseCommentsAction);
        
    }
})