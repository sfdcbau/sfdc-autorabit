({
    doInit : function(component, event, helper) {
        helper.GetCurrentUser(component, event, helper);
        helper.CaseCommentList(component, event);
        helper.TotalCaseComments(component, event);
    },
    viewAll : function(component, event, helper){
        component.set("v.defaultNumberOfCommentsShown", 10000);
        helper.CaseCommentList(component, event);
        helper.TotalCaseComments(component, event);
    }
})