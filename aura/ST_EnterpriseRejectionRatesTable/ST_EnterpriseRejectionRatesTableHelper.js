({
    doInitHelper : function(component, helper) {
        var totalteamMem = component.get("v.totalteamMem");
        var totalPage = Math.ceil(totalteamMem.length/component.get("v.noOfDisRec"));
        component.set("v.totalPage", totalPage);
        component.set("v.currentPage", 1);
        
        var teamMem = [];
        var n = (component.get("v.noOfDisRec") > totalteamMem.length ? totalteamMem.length : component.get("v.noOfDisRec"));
        for(var i = 0; i < n; i++) {
            teamMem.push(totalteamMem[i]);
        }
        component.set("v.teamMem", teamMem);
    },
    
    doPGHelper : function(component, helper, start, end, currentPage) {
        var totalteamMem = component.get("v.totalteamMem");
        var teamMem = [];
        for(var i = start; i < end; i++) {
            teamMem.push(totalteamMem[i]);
        }
        component.set("v.teamMem", teamMem);
        component.set("v.currentPage", currentPage);
    }
})