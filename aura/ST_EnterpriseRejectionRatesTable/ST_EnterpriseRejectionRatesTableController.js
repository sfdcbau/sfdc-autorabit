({
	doInit: function(component, event, helper) {
        console.log('doInit in Table');
        if(!$A.util.isEmpty(component.get("v.totalteamMem"))) {
            // Initialize input select options
            var optsDR = [
                { "class": "optionClass", label: "10", value: "10", selected: "true" },
                { "class": "optionClass", label: "20", value: "20"},
                { "class": "optionClass", label: "50", value: "50"},
                { "class": "optionClass", label: "100", value: "100"},
                { "class": "optionClass", label: "200", value: "200"}
            ];
            
            component.set("v.optionsDR", optsDR);
            
            // pagination initilization
            component.set("v.noOfDisRec", "10");
            helper.doInitHelper(component, helper);
        }
    },
    
    disRecChange: function(component, event, helper) {
        component.set("v.noOfDisRec" , event.target.value)
        helper.doInitHelper(component, helper);
    },
    
    doPG: function(component, event, helper) {
        //console.log("event = " + event.target.getAttribute("id"));
        var totalteamMem = component.get("v.totalteamMem");
        var totalPage = component.get("v.totalPage");
        var currentPage = component.get("v.currentPage");
        var noOfDisRec = component.get("v.noOfDisRec");
        
        if(event.target.getAttribute("id") == "first") {
            helper.doPGHelper(component, helper, 0, noOfDisRec, 1);
        }
        else if(event.target.getAttribute("id") == "prev") {
            helper.doPGHelper(component, helper, (currentPage - 2) * noOfDisRec, (currentPage - 1) * noOfDisRec, currentPage - 1);
        }
        else if(event.target.getAttribute("id") == "next") {
            var end = (currentPage + 1) * noOfDisRec;
            // checking if lastpage
            if(currentPage + 1 == totalPage)
                end = (currentPage + 1) * noOfDisRec > totalteamMem.length ? totalteamMem.length : (totalPage * noOfDisRec);
           	helper.doPGHelper(component, helper, currentPage * noOfDisRec, end, currentPage + 1);
        }
        else if(event.target.getAttribute("id") == "last") {
            var end = (totalPage * noOfDisRec) > totalteamMem.length ? totalteamMem.length : (totalPage * noOfDisRec);
            helper.doPGHelper(component, helper, (totalPage - 1) * noOfDisRec, end, totalPage);
        }
    },
    
    navigateToRecord: function(component, event, helper) {
        var recordId = event.target.getAttribute("id");
        console.log("recordId = " + recordId);
        var navEvt = $A.get("e.force:navigateToSObject");
        if(!$A.util.isEmpty(recordId) && navEvt) {
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    }
})