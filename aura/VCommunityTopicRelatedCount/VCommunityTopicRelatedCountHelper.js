({
    TotalIdeas : function(component, event){
        var totalIdeasAction = component.get("c.getTotalTopicIdeas");
        totalIdeasAction.setParams({
            topicId : component.get("v.topicId")
        });
        totalIdeasAction.setCallback(this, function(a) {
            component.set("v.totalIdeas", a.getReturnValue());
        });
        $A.enqueueAction(totalIdeasAction);
        
    },
    TotalDiscussions : function(component, event){
        var totalDiscussionsAction = component.get("c.getTotalTopicDiscussions");
        totalDiscussionsAction.setParams({
            topicId : component.get("v.topicId")
        });
        totalDiscussionsAction.setCallback(this, function(a) {
            component.set("v.totalDiscussions", a.getReturnValue());
        });
        $A.enqueueAction(totalDiscussionsAction);
        
    }
})