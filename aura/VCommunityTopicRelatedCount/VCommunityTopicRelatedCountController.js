({
    doInit : function(component, event, helper) {
        helper.TotalIdeas(component, event);
        helper.TotalDiscussions(component, event);
    },
})