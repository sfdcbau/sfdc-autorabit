({
    GetGuestUser : function(component, event) {
        var isGuestUserMethod = component.get("c.isGuestUser");
        isGuestUserMethod.setCallback(this, function(a) {
            console.log(a.getReturnValue());
            component.set("v.isGuest", a.getReturnValue());
        });
        $A.enqueueAction(isGuestUserMethod);
    },
	IdeasList : function(component, event) {
        var newAction = component.get("c.getIdeasList");
        newAction.setParams({
            ideasPerPage : component.get("v.ideasPerPage"),
            selectedFilter : component.get("v.viewName"),
            selPageNo : component.get("v.nextPage"),
            selectedZone : component.get("v.selectedZoneId"),
            selectedCategory : component.get("v.selectedCategory"),
            selectedStatus : component.get("v.selectedStatus"),
            keyword : component.get("v.searchKeyword")
        });
        newAction.setCallback(this, function(a) {
            component.set("v.IdeasList", a.getReturnValue());
            component.set("v.ideasPageUpdated", true);
            if(a.getReturnValue().length != 0){
                
            }else{
                //component.set("v.ideasPageUpdated", false);
                component.set("v.nextPage", component.get("v.nextPage")-1);
            }
            //console.log(a.getReturnValue());
        });
        $A.enqueueAction(newAction);
	},
    TotalIdeas : function(component, event){
        var totalIdeasAction = component.get("c.getTotalIdeas");
        totalIdeasAction.setParams({
            ideasPerPage : component.get("v.ideasPerPage"),
            selectedFilter : component.get("v.viewName"),
            selPageNo : component.get("v.nextPage"),
            selectedZone : component.get("v.selectedZoneId"),
            selectedCategory : component.get("v.selectedCategory"),
            selectedStatus : component.get("v.selectedStatus"),
            keyword : component.get("v.searchKeyword")
        });
        totalIdeasAction.setCallback(this, function(a) {
            var ideasPerPage = component.get("v.ideasPerPage");
            var totalIdeas = a.getReturnValue();
            var totalIdeaPages = Math.ceil(totalIdeas/ideasPerPage);
            component.set("v.TotalIdeas", a.getReturnValue());
            component.set("v.totalIdeaPages", totalIdeaPages);
            console.log('TotalIdeas: '+a.getReturnValue());
            console.log('totalIdeaPages: '+totalIdeaPages);
        });
        $A.enqueueAction(totalIdeasAction);
        
    },
    GetZone : function(component, event, helper){
        var getZones = component.get("c.getCurrentCommunityZones");
        getZones.setCallback(this, function(a){
            console.log('got zones');
            component.set("v.zoneIdNames", a.getReturnValue());
            component.set("v.selectedZoneId", a.getReturnValue()[0].Id);
            console.log(a.getReturnValue()[0].Name);
            helper.IdeasList(component, event);
            helper.TotalIdeas(component, event);
        });
        $A.enqueueAction(getZones);
    },
	GetCategories : function(component, event){
        var getCategorries = component.get("c.getIdeaCategorySelectOptions");
        getCategorries.setCallback(this, function(a){
            component.set("v.categorySelectOptions", a.getReturnValue());
        });
        $A.enqueueAction(getCategorries);
    },
	GetStatuses : function(component, event){
        var getStatuses = component.get("c.getIdeaStatusSelectOptions");
        getStatuses.setCallback(this, function(a){
            component.set("v.statusSelectOptions", a.getReturnValue());
        });
        $A.enqueueAction(getStatuses);
    },
    GetFilters : function(component, event){
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === 'view') {
                component.set("v.viewName", sParameterName[1]);
            }
            if (sParameterName[0] === 'category') {
                component.set("v.selectedCategory", sParameterName[1]);
            }
            if (sParameterName[0] === 'status') {
                component.set("v.selectedStatus", sParameterName[1]);
            }
            if (sParameterName[0] === 'search') {
                component.set("v.searchKeyword", sParameterName[1]);
            }
        }
    }
})